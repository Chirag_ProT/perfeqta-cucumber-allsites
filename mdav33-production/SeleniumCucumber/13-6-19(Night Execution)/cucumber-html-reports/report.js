$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("featurefile/Attributes.feature");
formatter.feature({
  "line": 1,
  "name": "Info: Executed script on \"test1.beperfeqta.com/v32050\" link.",
  "description": "Module Name: Attribute",
  "id": "info:-executed-script-on-\"test1.beperfeqta.com/v32050\"-link.",
  "keyword": "Feature"
});
formatter.before({
  "duration": 22169904713,
  "status": "passed"
});
formatter.scenario({
  "line": 5,
  "name": "Verify the \"Attribute\" module title",
  "description": "",
  "id": "info:-executed-script-on-\"test1.beperfeqta.com/v32050\"-link.;verify-the-\"attribute\"-module-title",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 4,
      "name": "@chrome"
    }
  ]
});
formatter.step({
  "line": 6,
  "name": ": Navigate to the URL",
  "keyword": "Given "
});
formatter.step({
  "line": 7,
  "name": ": Enter valid credentials and Click on Login Button",
  "keyword": "And "
});
formatter.step({
  "line": 8,
  "name": ": Click on Administration Tile",
  "keyword": "And "
});
formatter.step({
  "line": 9,
  "name": ": Click on Attributes Tile",
  "keyword": "And "
});
formatter.step({
  "line": 10,
  "name": ": Verify the module name as \"Attributes\"",
  "keyword": "Then "
});
formatter.match({
  "location": "CommonFunctions.navigate_to_the_URL()"
});
formatter.result({
  "duration": 3122015005,
  "status": "passed"
});
formatter.match({
  "location": "CommonFunctions.enter_valid_credentials_and_Click_on_Login_Button()"
});
formatter.result({
  "duration": 7876034322,
  "status": "passed"
});
formatter.match({
  "location": "CommonFunctions.click_on_Administration_Tile()"
});
formatter.result({
  "duration": 1198742739,
  "status": "passed"
});
formatter.match({
  "location": "Attributes.click_on_Attributes_Tile()"
});
formatter.result({
  "duration": 299421680,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Attributes",
      "offset": 29
    }
  ],
  "location": "Attributes.verify_the_module_name_as(String)"
});
formatter.result({
  "duration": 376013666,
  "status": "passed"
});
formatter.embedding("image/png", "embedded0.png");
formatter.after({
  "duration": 4345360063,
  "status": "passed"
});
formatter.before({
  "duration": 1863091006,
  "status": "passed"
});
formatter.scenario({
  "line": 13,
  "name": "Verify the default \"Module drop-down\" values of Attributes module",
  "description": "",
  "id": "info:-executed-script-on-\"test1.beperfeqta.com/v32050\"-link.;verify-the-default-\"module-drop-down\"-values-of-attributes-module",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 12,
      "name": "@chrome"
    }
  ]
});
formatter.step({
  "line": 14,
  "name": ": Navigate to the URL",
  "keyword": "Given "
});
formatter.step({
  "line": 15,
  "name": ": Enter valid credentials and Click on Login Button",
  "keyword": "And "
});
formatter.step({
  "line": 16,
  "name": ": Click on Administration Tile",
  "keyword": "And "
});
formatter.step({
  "line": 17,
  "name": ": Click on Attributes Tile",
  "keyword": "And "
});
formatter.step({
  "line": 18,
  "name": ": Verify the default \"Module drop-down\" values of Attributes module",
  "keyword": "Then "
});
formatter.match({
  "location": "CommonFunctions.navigate_to_the_URL()"
});
formatter.result({
  "duration": 3392560119,
  "status": "passed"
});
formatter.match({
  "location": "CommonFunctions.enter_valid_credentials_and_Click_on_Login_Button()"
});
formatter.result({
  "duration": 6784412259,
  "status": "passed"
});
formatter.match({
  "location": "CommonFunctions.click_on_Administration_Tile()"
});
formatter.result({
  "duration": 1207922622,
  "status": "passed"
});
formatter.match({
  "location": "Attributes.click_on_Attributes_Tile()"
});
formatter.result({
  "duration": 269914664,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Module drop-down",
      "offset": 22
    }
  ],
  "location": "Attributes.verify_the_default_values_of_Attributes_module(String)"
});
formatter.result({
  "duration": 412474305,
  "status": "passed"
});
formatter.embedding("image/png", "embedded1.png");
formatter.after({
  "duration": 3508154422,
  "status": "passed"
});
formatter.before({
  "duration": 773492356,
  "status": "passed"
});
formatter.scenario({
  "line": 21,
  "name": "Verify the default module drop-down is enable or not",
  "description": "",
  "id": "info:-executed-script-on-\"test1.beperfeqta.com/v32050\"-link.;verify-the-default-module-drop-down-is-enable-or-not",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 20,
      "name": "@chrome"
    }
  ]
});
formatter.step({
  "line": 22,
  "name": ": Navigate to the URL",
  "keyword": "Given "
});
formatter.step({
  "line": 23,
  "name": ": Enter valid credentials and Click on Login Button",
  "keyword": "And "
});
formatter.step({
  "line": 24,
  "name": ": Click on Administration Tile",
  "keyword": "And "
});
formatter.step({
  "line": 25,
  "name": ": Click on Attributes Tile",
  "keyword": "And "
});
formatter.step({
  "line": 26,
  "name": ": Click on \"Module drop-down\" and verify that the drop down is enabled or not",
  "keyword": "Then "
});
formatter.match({
  "location": "CommonFunctions.navigate_to_the_URL()"
});
formatter.result({
  "duration": 3976887196,
  "status": "passed"
});
formatter.match({
  "location": "CommonFunctions.enter_valid_credentials_and_Click_on_Login_Button()"
});
formatter.result({
  "duration": 6775504881,
  "status": "passed"
});
formatter.match({
  "location": "CommonFunctions.click_on_Administration_Tile()"
});
formatter.result({
  "duration": 1216595928,
  "status": "passed"
});
formatter.match({
  "location": "Attributes.click_on_Attributes_Tile()"
});
formatter.result({
  "duration": 165550189,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Module drop-down",
      "offset": 12
    }
  ],
  "location": "Attributes.click_on_and_verify_that_the_drop_down_is_enabled_or_not(String)"
});
formatter.result({
  "duration": 401864289,
  "status": "passed"
});
formatter.embedding("image/png", "embedded2.png");
formatter.after({
  "duration": 3509910597,
  "status": "passed"
});
formatter.before({
  "duration": 740202483,
  "status": "passed"
});
formatter.scenario({
  "line": 29,
  "name": "Verify the search functionality of Attribute listing page",
  "description": "",
  "id": "info:-executed-script-on-\"test1.beperfeqta.com/v32050\"-link.;verify-the-search-functionality-of-attribute-listing-page",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 28,
      "name": "@chrome"
    }
  ]
});
formatter.step({
  "line": 30,
  "name": ": Navigate to the URL",
  "keyword": "Given "
});
formatter.step({
  "line": 31,
  "name": ": Enter valid credentials and Click on Login Button",
  "keyword": "And "
});
formatter.step({
  "line": 32,
  "name": ": Click on Administration Tile",
  "keyword": "And "
});
formatter.step({
  "line": 33,
  "name": ": Click on Attributes Tile",
  "keyword": "And "
});
formatter.step({
  "line": 34,
  "name": ": Select Module from Attribute listing Screen",
  "keyword": "And "
});
formatter.step({
  "line": 35,
  "name": ": Enter the data into search box, which user want to search",
  "keyword": "And "
});
formatter.step({
  "line": 36,
  "name": ": Verify the search results of Attribute listing page",
  "keyword": "Then "
});
formatter.match({
  "location": "CommonFunctions.navigate_to_the_URL()"
});
formatter.result({
  "duration": 3135625258,
  "status": "passed"
});
formatter.match({
  "location": "CommonFunctions.enter_valid_credentials_and_Click_on_Login_Button()"
});
formatter.result({
  "duration": 6736077014,
  "status": "passed"
});
formatter.match({
  "location": "CommonFunctions.click_on_Administration_Tile()"
});
formatter.result({
  "duration": 1216573413,
  "status": "passed"
});
formatter.match({
  "location": "Attributes.click_on_Attributes_Tile()"
});
formatter.result({
  "duration": 140253389,
  "status": "passed"
});
formatter.match({
  "location": "Attributes.select_Module_from_Attribute_listing_Screen()"
});
formatter.result({
  "duration": 527228130,
  "status": "passed"
});
formatter.match({
  "location": "Attributes.enter_the_data_into_search_box_which_user_want_to_search()"
});
formatter.result({
  "duration": 177158,
  "status": "passed"
});
formatter.match({
  "location": "Attributes.verify_the_search_results_of_Attribute_listing_page()"
});
formatter.result({
  "duration": 29036505281,
  "status": "passed"
});
formatter.embedding("image/png", "embedded3.png");
formatter.after({
  "duration": 3470262808,
  "status": "passed"
});
formatter.before({
  "duration": 752936963,
  "status": "passed"
});
formatter.scenario({
  "line": 39,
  "name": "Verify the Ascending order for the Attribute column under Attributes listing page",
  "description": "",
  "id": "info:-executed-script-on-\"test1.beperfeqta.com/v32050\"-link.;verify-the-ascending-order-for-the-attribute-column-under-attributes-listing-page",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 38,
      "name": "@chrome"
    }
  ]
});
formatter.step({
  "line": 40,
  "name": ": Navigate to the URL",
  "keyword": "Given "
});
formatter.step({
  "line": 41,
  "name": ": Enter valid credentials and Click on Login Button",
  "keyword": "And "
});
formatter.step({
  "line": 42,
  "name": ": Click on Administration Tile",
  "keyword": "And "
});
formatter.step({
  "line": 43,
  "name": ": Click on Attributes Tile",
  "keyword": "And "
});
formatter.step({
  "line": 44,
  "name": ": Click on Sorting icon of the Attribute column",
  "keyword": "And "
});
formatter.step({
  "line": 45,
  "name": ": Verify that all the records of Attribute column display in ascending order",
  "keyword": "Then "
});
formatter.match({
  "location": "CommonFunctions.navigate_to_the_URL()"
});
formatter.result({
  "duration": 2879519593,
  "status": "passed"
});
formatter.match({
  "location": "CommonFunctions.enter_valid_credentials_and_Click_on_Login_Button()"
});
formatter.result({
  "duration": 6704210598,
  "status": "passed"
});
formatter.match({
  "location": "CommonFunctions.click_on_Administration_Tile()"
});
formatter.result({
  "duration": 1216002285,
  "status": "passed"
});
formatter.match({
  "location": "Attributes.click_on_Attributes_Tile()"
});
formatter.result({
  "duration": 279612086,
  "status": "passed"
});
formatter.match({
  "location": "Attributes.click_on_Sorting_icon_of_the_Attribute_column()"
});
formatter.result({
  "duration": 21309401892,
  "status": "passed"
});
formatter.match({
  "location": "Attributes.verify_that_all_the_records_of_Attribute_column_display_in_ascending_order()"
});
formatter.result({
  "duration": 70297,
  "status": "passed"
});
formatter.embedding("image/png", "embedded4.png");
formatter.after({
  "duration": 3523443714,
  "status": "passed"
});
formatter.before({
  "duration": 800745828,
  "status": "passed"
});
formatter.scenario({
  "line": 48,
  "name": "Verify the navigation of Audit trail page of Attributes",
  "description": "",
  "id": "info:-executed-script-on-\"test1.beperfeqta.com/v32050\"-link.;verify-the-navigation-of-audit-trail-page-of-attributes",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 47,
      "name": "@chrome"
    }
  ]
});
formatter.step({
  "line": 49,
  "name": ": Navigate to the URL",
  "keyword": "Given "
});
formatter.step({
  "line": 50,
  "name": ": Enter valid credentials and Click on Login Button",
  "keyword": "And "
});
formatter.step({
  "line": 51,
  "name": ": Click on Administration Tile",
  "keyword": "And "
});
formatter.step({
  "line": 52,
  "name": ": Click on Attributes Tile",
  "keyword": "And "
});
formatter.step({
  "line": 53,
  "name": ": Click on \"View Audit Trail\" link of the first record of the listing page",
  "keyword": "And "
});
formatter.step({
  "line": 54,
  "name": ": Verify the user redirecting to Audit trail of record",
  "keyword": "Then "
});
formatter.match({
  "location": "CommonFunctions.navigate_to_the_URL()"
});
formatter.result({
  "duration": 2561571580,
  "status": "passed"
});
formatter.match({
  "location": "CommonFunctions.enter_valid_credentials_and_Click_on_Login_Button()"
});
formatter.result({
  "duration": 6729221374,
  "status": "passed"
});
formatter.match({
  "location": "CommonFunctions.click_on_Administration_Tile()"
});
formatter.result({
  "duration": 1204974794,
  "status": "passed"
});
formatter.match({
  "location": "Attributes.click_on_Attributes_Tile()"
});
formatter.result({
  "duration": 151807386,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "View Audit Trail",
      "offset": 12
    }
  ],
  "location": "Pagination.click_on_link_of_the_first_record_of_the_listing_page(String)"
});
formatter.result({
  "duration": 2559715994,
  "status": "passed"
});
formatter.match({
  "location": "Attributes.verify_the_user_redirecting_to_Audit_trail_of_record()"
});
formatter.result({
  "duration": 2039313447,
  "status": "passed"
});
formatter.embedding("image/png", "embedded5.png");
formatter.after({
  "duration": 3537015888,
  "status": "passed"
});
formatter.before({
  "duration": 1359619630,
  "status": "passed"
});
formatter.scenario({
  "line": 57,
  "name": "Verify the breadcrumbs of Audit trail page of Attributes module",
  "description": "",
  "id": "info:-executed-script-on-\"test1.beperfeqta.com/v32050\"-link.;verify-the-breadcrumbs-of-audit-trail-page-of-attributes-module",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 56,
      "name": "@chrome"
    }
  ]
});
formatter.step({
  "line": 58,
  "name": ": Navigate to the URL",
  "keyword": "Given "
});
formatter.step({
  "line": 59,
  "name": ": Enter valid credentials and Click on Login Button",
  "keyword": "And "
});
formatter.step({
  "line": 60,
  "name": ": Click on Administration Tile",
  "keyword": "And "
});
formatter.step({
  "line": 61,
  "name": ": Click on Attributes Tile",
  "keyword": "And "
});
formatter.step({
  "line": 62,
  "name": ": Click on \"View Audit Trail\" link of the first record of the listing page",
  "keyword": "And "
});
formatter.step({
  "line": 63,
  "name": ": Verify the Audit trail breadcrumbs as \"Home\u003eAdministration\u003eAttributes\u003eAudit Trail\"",
  "keyword": "Then "
});
formatter.match({
  "location": "CommonFunctions.navigate_to_the_URL()"
});
formatter.result({
  "duration": 3349122194,
  "status": "passed"
});
formatter.match({
  "location": "CommonFunctions.enter_valid_credentials_and_Click_on_Login_Button()"
});
formatter.result({
  "duration": 6752499704,
  "status": "passed"
});
formatter.match({
  "location": "CommonFunctions.click_on_Administration_Tile()"
});
formatter.result({
  "duration": 1229428643,
  "status": "passed"
});
formatter.match({
  "location": "Attributes.click_on_Attributes_Tile()"
});
formatter.result({
  "duration": 149382400,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "View Audit Trail",
      "offset": 12
    }
  ],
  "location": "Pagination.click_on_link_of_the_first_record_of_the_listing_page(String)"
});
formatter.result({
  "duration": 2558564155,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Home\u003eAdministration\u003eAttributes\u003eAudit Trail",
      "offset": 41
    }
  ],
  "location": "Attributes.verify_the_Audit_trail_breadcrumbs_as(String)"
});
formatter.result({
  "duration": 1048017864,
  "status": "passed"
});
formatter.embedding("image/png", "embedded6.png");
formatter.after({
  "duration": 3583872146,
  "status": "passed"
});
formatter.before({
  "duration": 688495449,
  "status": "passed"
});
formatter.scenario({
  "comments": [
    {
      "line": 65,
      "value": "# Pagination test cases for the Audit trail page of Attributes"
    }
  ],
  "line": 68,
  "name": "Verify pagination of Audit trail screen of Attribute module when user click on \"First\"",
  "description": "",
  "id": "info:-executed-script-on-\"test1.beperfeqta.com/v32050\"-link.;verify-pagination-of-audit-trail-screen-of-attribute-module-when-user-click-on-\"first\"",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 67,
      "name": "@chrome"
    }
  ]
});
formatter.step({
  "line": 69,
  "name": ": Navigate to the URL",
  "keyword": "Given "
});
formatter.step({
  "line": 70,
  "name": ": Enter valid credentials and Click on Login Button",
  "keyword": "And "
});
formatter.step({
  "line": 71,
  "name": ": Click on Administration Tile",
  "keyword": "And "
});
formatter.step({
  "line": 72,
  "name": ": Click on Attributes Tile",
  "keyword": "And "
});
formatter.step({
  "line": 73,
  "name": ": Click on \"View Audit Trail\" link of the first record of the listing page",
  "keyword": "And "
});
formatter.step({
  "line": 74,
  "name": ": Click on the Last button of Pagination",
  "keyword": "And "
});
formatter.step({
  "line": 75,
  "name": ": Click on the First button of Pagination",
  "keyword": "And "
});
formatter.step({
  "line": 76,
  "name": ": Verify that the system should display the first page of the Audit trail screen",
  "keyword": "Then "
});
formatter.match({
  "location": "CommonFunctions.navigate_to_the_URL()"
});
formatter.result({
  "duration": 2884706912,
  "status": "passed"
});
formatter.match({
  "location": "CommonFunctions.enter_valid_credentials_and_Click_on_Login_Button()"
});
formatter.result({
  "duration": 6719450615,
  "status": "passed"
});
formatter.match({
  "location": "CommonFunctions.click_on_Administration_Tile()"
});
formatter.result({
  "duration": 1206635267,
  "status": "passed"
});
formatter.match({
  "location": "Attributes.click_on_Attributes_Tile()"
});
formatter.result({
  "duration": 264255437,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "View Audit Trail",
      "offset": 12
    }
  ],
  "location": "Pagination.click_on_link_of_the_first_record_of_the_listing_page(String)"
});
formatter.result({
  "duration": 2448789867,
  "status": "passed"
});
formatter.match({
  "location": "Pagination.click_on_the_Last_button_of_Pagination()"
});
formatter.result({
  "duration": 133936085,
  "status": "passed"
});
formatter.match({
  "location": "Pagination.click_on_the_First_button_of_Pagination()"
});
formatter.result({
  "duration": 314455746,
  "status": "passed"
});
formatter.match({
  "location": "Pagination.verify_that_the_system_should_display_the_first_page_of_the_Audit_trail_screen()"
});
formatter.result({
  "duration": 440100449,
  "status": "passed"
});
formatter.embedding("image/png", "embedded7.png");
formatter.after({
  "duration": 3564295225,
  "status": "passed"
});
formatter.before({
  "duration": 770321384,
  "status": "passed"
});
formatter.scenario({
  "line": 79,
  "name": "",
  "description": "Verify pagination of Audit trail screen of Attribute module when user click on \"Next\"",
  "id": "info:-executed-script-on-\"test1.beperfeqta.com/v32050\"-link.;",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 78,
      "name": "@chrome"
    }
  ]
});
formatter.step({
  "line": 81,
  "name": ": Navigate to the URL",
  "keyword": "Given "
});
formatter.step({
  "line": 82,
  "name": ": Enter valid credentials and Click on Login Button",
  "keyword": "And "
});
formatter.step({
  "line": 83,
  "name": ": Click on Administration Tile",
  "keyword": "And "
});
formatter.step({
  "line": 84,
  "name": ": Click on Attributes Tile",
  "keyword": "And "
});
formatter.step({
  "line": 85,
  "name": ": Click on \"View Audit Trail\" link of the first record of the listing page",
  "keyword": "And "
});
formatter.step({
  "line": 86,
  "name": ": Click on Next button of Pagination",
  "keyword": "And "
});
formatter.step({
  "line": 87,
  "name": ": Verify that the system should display the second page of the Audit trail screen",
  "keyword": "Then "
});
formatter.match({
  "location": "CommonFunctions.navigate_to_the_URL()"
});
formatter.result({
  "duration": 2942249014,
  "status": "passed"
});
formatter.match({
  "location": "CommonFunctions.enter_valid_credentials_and_Click_on_Login_Button()"
});
formatter.result({
  "duration": 6735392989,
  "status": "passed"
});
formatter.match({
  "location": "CommonFunctions.click_on_Administration_Tile()"
});
formatter.result({
  "duration": 1109842216,
  "status": "passed"
});
formatter.match({
  "location": "Attributes.click_on_Attributes_Tile()"
});
formatter.result({
  "duration": 370280203,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "View Audit Trail",
      "offset": 12
    }
  ],
  "location": "Pagination.click_on_link_of_the_first_record_of_the_listing_page(String)"
});
formatter.result({
  "duration": 2474696034,
  "status": "passed"
});
formatter.match({
  "location": "Pagination.click_on_Next_button_of_Pagination()"
});
formatter.result({
  "duration": 133815304,
  "status": "passed"
});
formatter.match({
  "location": "Pagination.verify_that_the_system_should_display_the_second_page_of_the_Audit_trail_screen()"
});
formatter.result({
  "duration": 1307591765,
  "status": "passed"
});
formatter.embedding("image/png", "embedded8.png");
formatter.after({
  "duration": 3498469015,
  "status": "passed"
});
formatter.before({
  "duration": 762596062,
  "status": "passed"
});
formatter.scenario({
  "line": 90,
  "name": "",
  "description": "Verify pagination of Audit trail screen of Attribute module when user click on \"Previous\"",
  "id": "info:-executed-script-on-\"test1.beperfeqta.com/v32050\"-link.;",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 89,
      "name": "@chrome"
    }
  ]
});
formatter.step({
  "line": 92,
  "name": ": Navigate to the URL",
  "keyword": "Given "
});
formatter.step({
  "line": 93,
  "name": ": Enter valid credentials and Click on Login Button",
  "keyword": "And "
});
formatter.step({
  "line": 94,
  "name": ": Click on Administration Tile",
  "keyword": "And "
});
formatter.step({
  "line": 95,
  "name": ": Click on Attributes Tile",
  "keyword": "And "
});
formatter.step({
  "line": 96,
  "name": ": Click on \"View Audit Trail\" link of the first record of the listing page",
  "keyword": "And "
});
formatter.step({
  "line": 97,
  "name": ": Click on Next button of Pagination",
  "keyword": "And "
});
formatter.step({
  "line": 98,
  "name": ": Click on the Previous button of Pagination",
  "keyword": "And "
});
formatter.step({
  "line": 99,
  "name": ": Verify that the system should display the previous page of the Audit trail screen",
  "keyword": "Then "
});
formatter.match({
  "location": "CommonFunctions.navigate_to_the_URL()"
});
formatter.result({
  "duration": 5081576970,
  "status": "passed"
});
formatter.match({
  "location": "CommonFunctions.enter_valid_credentials_and_Click_on_Login_Button()"
});
formatter.result({
  "duration": 6704211682,
  "status": "passed"
});
formatter.match({
  "location": "CommonFunctions.click_on_Administration_Tile()"
});
formatter.result({
  "duration": 1233934008,
  "status": "passed"
});
formatter.match({
  "location": "Attributes.click_on_Attributes_Tile()"
});
formatter.result({
  "duration": 254430136,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "View Audit Trail",
      "offset": 12
    }
  ],
  "location": "Pagination.click_on_link_of_the_first_record_of_the_listing_page(String)"
});
formatter.result({
  "duration": 2435452333,
  "status": "passed"
});
formatter.match({
  "location": "Pagination.click_on_Next_button_of_Pagination()"
});
formatter.result({
  "duration": 165309544,
  "status": "passed"
});
formatter.match({
  "location": "Pagination.click_on_the_Previous_button_of_Pagination()"
});
formatter.result({
  "duration": 1474236786,
  "status": "passed"
});
formatter.match({
  "location": "Pagination.verify_that_the_system_should_display_the_previous_page_of_the_Audit_trail_screen()"
});
formatter.result({
  "duration": 1058421138,
  "status": "passed"
});
formatter.embedding("image/png", "embedded9.png");
formatter.after({
  "duration": 3462254415,
  "status": "passed"
});
formatter.before({
  "duration": 783508746,
  "status": "passed"
});
formatter.scenario({
  "line": 102,
  "name": "",
  "description": "Verify pagination of Audit trail screen of Attribute module when user click on \"Last\"",
  "id": "info:-executed-script-on-\"test1.beperfeqta.com/v32050\"-link.;",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 101,
      "name": "@chrome"
    }
  ]
});
formatter.step({
  "line": 104,
  "name": ": Navigate to the URL",
  "keyword": "Given "
});
formatter.step({
  "line": 105,
  "name": ": Enter valid credentials and Click on Login Button",
  "keyword": "And "
});
formatter.step({
  "line": 106,
  "name": ": Click on Administration Tile",
  "keyword": "And "
});
formatter.step({
  "line": 107,
  "name": ": Click on Attributes Tile",
  "keyword": "And "
});
formatter.step({
  "line": 108,
  "name": ": Click on \"View Audit Trail\" link of the first record of the listing page",
  "keyword": "And "
});
formatter.step({
  "line": 109,
  "name": ": Click on the Last button of Pagination",
  "keyword": "And "
});
formatter.step({
  "line": 110,
  "name": ": Verify that the system should display the last page of the Audit trail screen",
  "keyword": "Then "
});
formatter.match({
  "location": "CommonFunctions.navigate_to_the_URL()"
});
formatter.result({
  "duration": 3181077336,
  "status": "passed"
});
formatter.match({
  "location": "CommonFunctions.enter_valid_credentials_and_Click_on_Login_Button()"
});
formatter.result({
  "duration": 6777358017,
  "status": "passed"
});
formatter.match({
  "location": "CommonFunctions.click_on_Administration_Tile()"
});
formatter.result({
  "duration": 1201750597,
  "status": "passed"
});
formatter.match({
  "location": "Attributes.click_on_Attributes_Tile()"
});
formatter.result({
  "duration": 160921376,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "View Audit Trail",
      "offset": 12
    }
  ],
  "location": "Pagination.click_on_link_of_the_first_record_of_the_listing_page(String)"
});
formatter.result({
  "duration": 2582148594,
  "status": "passed"
});
formatter.match({
  "location": "Pagination.click_on_the_Last_button_of_Pagination()"
});
formatter.result({
  "duration": 152569330,
  "status": "passed"
});
formatter.match({
  "location": "Pagination.verify_that_the_system_should_display_the_last_page_of_the_Audit_trail_screen()"
});
formatter.result({
  "duration": 203365607,
  "status": "passed"
});
formatter.embedding("image/png", "embedded10.png");
formatter.after({
  "duration": 3460499163,
  "status": "passed"
});
formatter.before({
  "duration": 728183485,
  "status": "passed"
});
formatter.scenario({
  "line": 112,
  "name": "Verify the record count of Audit trail screen of Attribute module",
  "description": "",
  "id": "info:-executed-script-on-\"test1.beperfeqta.com/v32050\"-link.;verify-the-record-count-of-audit-trail-screen-of-attribute-module",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 111,
      "name": "@chrome"
    }
  ]
});
formatter.step({
  "line": 113,
  "name": ": Navigate to the URL",
  "keyword": "Given "
});
formatter.step({
  "line": 114,
  "name": ": Enter valid credentials and Click on Login Button",
  "keyword": "And "
});
formatter.step({
  "line": 115,
  "name": ": Click on Administration Tile",
  "keyword": "And "
});
formatter.step({
  "line": 116,
  "name": ": Click on Attributes Tile",
  "keyword": "And "
});
formatter.step({
  "line": 117,
  "name": ": Click on \"View Audit Trail\" link of the first record of the listing page",
  "keyword": "And "
});
formatter.step({
  "line": 118,
  "name": ": Verify the record count",
  "keyword": "Then "
});
formatter.match({
  "location": "CommonFunctions.navigate_to_the_URL()"
});
formatter.result({
  "duration": 2685149709,
  "status": "passed"
});
formatter.match({
  "location": "CommonFunctions.enter_valid_credentials_and_Click_on_Login_Button()"
});
formatter.result({
  "duration": 6708642554,
  "status": "passed"
});
formatter.match({
  "location": "CommonFunctions.click_on_Administration_Tile()"
});
formatter.result({
  "duration": 1114766935,
  "status": "passed"
});
formatter.match({
  "location": "Attributes.click_on_Attributes_Tile()"
});
formatter.result({
  "duration": 248132187,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "View Audit Trail",
      "offset": 12
    }
  ],
  "location": "Pagination.click_on_link_of_the_first_record_of_the_listing_page(String)"
});
formatter.result({
  "duration": 2558461110,
  "status": "passed"
});
formatter.match({
  "location": "Pagination.verify_the_record_count()"
});
formatter.result({
  "duration": 7485182606,
  "status": "passed"
});
formatter.embedding("image/png", "embedded11.png");
formatter.after({
  "duration": 3444290418,
  "status": "passed"
});
formatter.before({
  "duration": 815972583,
  "status": "passed"
});
formatter.scenario({
  "line": 121,
  "name": "Verify pagination of Attributes listing page when user click on \"First\"",
  "description": "",
  "id": "info:-executed-script-on-\"test1.beperfeqta.com/v32050\"-link.;verify-pagination-of-attributes-listing-page-when-user-click-on-\"first\"",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 120,
      "name": "@chrome"
    },
    {
      "line": 120,
      "name": "@demo"
    }
  ]
});
formatter.step({
  "line": 122,
  "name": ": Navigate to the URL",
  "keyword": "Given "
});
formatter.step({
  "line": 123,
  "name": ": Enter valid credentials and Click on Login Button",
  "keyword": "And "
});
formatter.step({
  "line": 124,
  "name": ": Click on Administration Tile",
  "keyword": "And "
});
formatter.step({
  "line": 125,
  "name": ": Click on Attributes Tile",
  "keyword": "And "
});
formatter.step({
  "line": 126,
  "name": ": Click on the Last button of Pagination",
  "keyword": "And "
});
formatter.step({
  "line": 127,
  "name": ": Click on the First button of Pagination",
  "keyword": "And "
});
formatter.step({
  "line": 128,
  "name": ": Verify that the system should display the first page of the listing screen",
  "keyword": "Then "
});
formatter.match({
  "location": "CommonFunctions.navigate_to_the_URL()"
});
formatter.result({
  "duration": 3093134961,
  "status": "passed"
});
formatter.match({
  "location": "CommonFunctions.enter_valid_credentials_and_Click_on_Login_Button()"
});
formatter.result({
  "duration": 6720613284,
  "status": "passed"
});
formatter.match({
  "location": "CommonFunctions.click_on_Administration_Tile()"
});
formatter.result({
  "duration": 1110307601,
  "status": "passed"
});
formatter.match({
  "location": "Attributes.click_on_Attributes_Tile()"
});
formatter.result({
  "duration": 384222922,
  "status": "passed"
});
formatter.match({
  "location": "Pagination.click_on_the_Last_button_of_Pagination()"
});
formatter.result({
  "duration": 31005246077,
  "error_message": "org.openqa.selenium.NoSuchElementException: Timed out after 10 seconds. Unable to locate the element\n\tat org.openqa.selenium.support.pagefactory.AjaxElementLocator.findElement(AjaxElementLocator.java:90)\n\tat org.openqa.selenium.support.pagefactory.internal.LocatingElementHandler.invoke(LocatingElementHandler.java:38)\n\tat com.sun.proxy.$Proxy21.click(Unknown Source)\n\tat com.cucumber.framework.helper.PageObject.PaginationPageObject.clickLastBtnPagination(PaginationPageObject.java:74)\n\tat com.cucumber.framework.stepdefinition.Pagination.click_on_the_Last_button_of_Pagination(Pagination.java:23)\n\tat ✽.And : Click on the Last button of Pagination(featurefile/Attributes.feature:126)\nCaused by: org.openqa.selenium.NoSuchElementException: no such element: Unable to locate element: {\"method\":\"xpath\",\"selector\":\"//a[@ng-click\u003d\u0027selectPage(totalPages, $event)\u0027]\"}\n  (Session info: chrome\u003d69.0.3497.81)\n  (Driver info: chromedriver\u003d2.40,platform\u003dLinux 4.4.0-134-generic x86_64) (WARNING: The server did not provide any stacktrace information)\nCommand duration or timeout: 0 milliseconds\nFor documentation on this error, please visit: http://seleniumhq.org/exceptions/no_such_element.html\nBuild info: version: \u00273.11.0\u0027, revision: \u0027e59cfb3\u0027, time: \u00272018-03-11T20:26:55.152Z\u0027\nSystem info: host: \u0027protl-vishal\u0027, ip: \u0027127.0.1.1\u0027, os.name: \u0027Linux\u0027, os.arch: \u0027amd64\u0027, os.version: \u00274.4.0-134-generic\u0027, java.version: \u00271.8.0_201\u0027\nDriver info: org.openqa.selenium.chrome.ChromeDriver\nCapabilities {acceptInsecureCerts: false, acceptSslCerts: false, applicationCacheEnabled: false, browserConnectionEnabled: false, browserName: chrome, chrome: {chromedriverVersion: 2.40, userDataDir: /tmp/.org.chromium.Chromium...}, cssSelectorsEnabled: true, databaseEnabled: false, goog:chromeOptions: {debuggerAddress: localhost:41536}, handlesAlerts: true, hasTouchScreen: false, javascriptEnabled: true, locationContextEnabled: true, mobileEmulationEnabled: false, nativeEvents: true, networkConnectionEnabled: false, pageLoadStrategy: normal, platform: LINUX, platformName: LINUX, rotatable: false, setWindowRect: true, takesHeapSnapshot: true, takesScreenshot: true, unexpectedAlertBehaviour: , unhandledPromptBehavior: , version: 69.0.3497.81, webStorageEnabled: true}\nSession ID: 18f0fa3cd9104e75e619035f290f60a8\n*** Element info: {Using\u003dxpath, value\u003d//a[@ng-click\u003d\u0027selectPage(totalPages, $event)\u0027]}\n\tat sun.reflect.GeneratedConstructorAccessor85.newInstance(Unknown Source)\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(DelegatingConstructorAccessorImpl.java:45)\n\tat java.lang.reflect.Constructor.newInstance(Constructor.java:423)\n\tat org.openqa.selenium.remote.ErrorHandler.createThrowable(ErrorHandler.java:214)\n\tat org.openqa.selenium.remote.ErrorHandler.throwIfResponseFailed(ErrorHandler.java:166)\n\tat org.openqa.selenium.remote.http.JsonHttpResponseCodec.reconstructValue(JsonHttpResponseCodec.java:40)\n\tat org.openqa.selenium.remote.http.AbstractHttpResponseCodec.decode(AbstractHttpResponseCodec.java:80)\n\tat org.openqa.selenium.remote.http.AbstractHttpResponseCodec.decode(AbstractHttpResponseCodec.java:44)\n\tat org.openqa.selenium.remote.HttpCommandExecutor.execute(HttpCommandExecutor.java:158)\n\tat org.openqa.selenium.remote.service.DriverCommandExecutor.execute(DriverCommandExecutor.java:83)\n\tat org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:545)\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:319)\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElementByXPath(RemoteWebDriver.java:421)\n\tat org.openqa.selenium.By$ByXPath.findElement(By.java:361)\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:311)\n\tat org.openqa.selenium.support.pagefactory.DefaultElementLocator.findElement(DefaultElementLocator.java:69)\n\tat org.openqa.selenium.support.pagefactory.AjaxElementLocator.access$001(AjaxElementLocator.java:40)\n\tat org.openqa.selenium.support.pagefactory.AjaxElementLocator$SlowLoadingElement.isLoaded(AjaxElementLocator.java:156)\n\tat org.openqa.selenium.support.ui.SlowLoadableComponent.get(SlowLoadableComponent.java:69)\n\tat org.openqa.selenium.support.pagefactory.AjaxElementLocator.findElement(AjaxElementLocator.java:86)\n\tat org.openqa.selenium.support.pagefactory.internal.LocatingElementHandler.invoke(LocatingElementHandler.java:38)\n\tat com.sun.proxy.$Proxy21.click(Unknown Source)\n\tat com.cucumber.framework.helper.PageObject.PaginationPageObject.clickLastBtnPagination(PaginationPageObject.java:74)\n\tat com.cucumber.framework.stepdefinition.Pagination.click_on_the_Last_button_of_Pagination(Pagination.java:23)\n\tat sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\n\tat sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\n\tat sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\n\tat java.lang.reflect.Method.invoke(Method.java:498)\n\tat cucumber.runtime.Utils$1.call(Utils.java:40)\n\tat cucumber.runtime.Timeout.timeout(Timeout.java:16)\n\tat cucumber.runtime.Utils.invoke(Utils.java:34)\n\tat cucumber.runtime.java.JavaStepDefinition.execute(JavaStepDefinition.java:38)\n\tat cucumber.runtime.StepDefinitionMatch.runStep(StepDefinitionMatch.java:37)\n\tat cucumber.runtime.Runtime.runStep(Runtime.java:300)\n\tat cucumber.runtime.model.StepContainer.runStep(StepContainer.java:44)\n\tat cucumber.runtime.model.StepContainer.runSteps(StepContainer.java:39)\n\tat cucumber.runtime.model.CucumberScenario.run(CucumberScenario.java:44)\n\tat cucumber.runtime.model.CucumberFeature.run(CucumberFeature.java:165)\n\tat cucumber.api.testng.TestNGCucumberRunner.runCucumber(TestNGCucumberRunner.java:63)\n\tat cucumber.api.testng.AbstractTestNGCucumberTests.feature(AbstractTestNGCucumberTests.java:21)\n\tat sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\n\tat sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\n\tat sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\n\tat java.lang.reflect.Method.invoke(Method.java:498)\n\tat org.testng.internal.MethodInvocationHelper.invokeMethod(MethodInvocationHelper.java:86)\n\tat org.testng.internal.Invoker.invokeMethod(Invoker.java:643)\n\tat org.testng.internal.Invoker.invokeTestMethod(Invoker.java:820)\n\tat org.testng.internal.Invoker.invokeTestMethods(Invoker.java:1128)\n\tat org.testng.internal.TestMethodWorker.invokeTestMethods(TestMethodWorker.java:129)\n\tat org.testng.internal.TestMethodWorker.run(TestMethodWorker.java:112)\n\tat org.testng.TestRunner.privateRun(TestRunner.java:782)\n\tat org.testng.TestRunner.run(TestRunner.java:632)\n\tat org.testng.SuiteRunner.runTest(SuiteRunner.java:366)\n\tat org.testng.SuiteRunner.runSequentially(SuiteRunner.java:361)\n\tat org.testng.SuiteRunner.privateRun(SuiteRunner.java:319)\n\tat org.testng.SuiteRunner.run(SuiteRunner.java:268)\n\tat org.testng.SuiteRunnerWorker.runSuite(SuiteRunnerWorker.java:52)\n\tat org.testng.SuiteRunnerWorker.run(SuiteRunnerWorker.java:86)\n\tat org.testng.TestNG.runSuitesSequentially(TestNG.java:1244)\n\tat org.testng.TestNG.runSuitesLocally(TestNG.java:1169)\n\tat org.testng.TestNG.run(TestNG.java:1064)\n\tat org.testng.remote.AbstractRemoteTestNG.run(AbstractRemoteTestNG.java:114)\n\tat org.testng.remote.RemoteTestNG.initAndRun(RemoteTestNG.java:251)\n\tat org.testng.remote.RemoteTestNG.main(RemoteTestNG.java:77)\n",
  "status": "failed"
});
formatter.match({
  "location": "Pagination.click_on_the_First_button_of_Pagination()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "Pagination.verify_that_the_system_should_display_the_first_page_of_the_listing_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.write("/home/nilay/Desktop/SeleniumCucumber/target/test-classes/screenshots/_2019-06-13/Verify pagination of Attributes listing page when user click on \"First\".png");
formatter.embedding("image/png", "embedded12.png");
formatter.after({
  "duration": 3847665217,
  "status": "passed"
});
formatter.before({
  "duration": 715982364,
  "status": "passed"
});
formatter.scenario({
  "line": 131,
  "name": "Verify pagination of Attributes listing page when user click on \"Next\"",
  "description": "",
  "id": "info:-executed-script-on-\"test1.beperfeqta.com/v32050\"-link.;verify-pagination-of-attributes-listing-page-when-user-click-on-\"next\"",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 130,
      "name": "@chrome"
    }
  ]
});
formatter.step({
  "line": 132,
  "name": ": Navigate to the URL",
  "keyword": "Given "
});
formatter.step({
  "line": 133,
  "name": ": Enter valid credentials and Click on Login Button",
  "keyword": "And "
});
formatter.step({
  "line": 134,
  "name": ": Click on Administration Tile",
  "keyword": "And "
});
formatter.step({
  "line": 135,
  "name": ": Click on Attributes Tile",
  "keyword": "And "
});
formatter.step({
  "line": 136,
  "name": ": Click on Next button of Pagination",
  "keyword": "And "
});
formatter.step({
  "line": 137,
  "name": ": Verify that the system should display the second page of the listing screen",
  "keyword": "Then "
});
formatter.match({
  "location": "CommonFunctions.navigate_to_the_URL()"
});
formatter.result({
  "duration": 2798601670,
  "status": "passed"
});
formatter.match({
  "location": "CommonFunctions.enter_valid_credentials_and_Click_on_Login_Button()"
});
formatter.result({
  "duration": 6694812380,
  "status": "passed"
});
formatter.match({
  "location": "CommonFunctions.click_on_Administration_Tile()"
});
formatter.result({
  "duration": 1194702097,
  "status": "passed"
});
formatter.match({
  "location": "Attributes.click_on_Attributes_Tile()"
});
formatter.result({
  "duration": 262414972,
  "status": "passed"
});
formatter.match({
  "location": "Pagination.click_on_Next_button_of_Pagination()"
});
formatter.result({
  "duration": 31068956935,
  "error_message": "org.openqa.selenium.NoSuchElementException: Timed out after 10 seconds. Unable to locate the element\n\tat org.openqa.selenium.support.pagefactory.AjaxElementLocator.findElement(AjaxElementLocator.java:90)\n\tat org.openqa.selenium.support.pagefactory.internal.LocatingElementHandler.invoke(LocatingElementHandler.java:38)\n\tat com.sun.proxy.$Proxy21.click(Unknown Source)\n\tat com.cucumber.framework.helper.PageObject.PaginationPageObject.clickNextBtnPagination(PaginationPageObject.java:90)\n\tat com.cucumber.framework.stepdefinition.Pagination.click_on_Next_button_of_Pagination(Pagination.java:38)\n\tat ✽.And : Click on Next button of Pagination(featurefile/Attributes.feature:136)\nCaused by: org.openqa.selenium.NoSuchElementException: no such element: Unable to locate element: {\"method\":\"xpath\",\"selector\":\"//li/a[@ng-click\u003d\u0027selectPage(page + 1, $event)\u0027]\"}\n  (Session info: chrome\u003d69.0.3497.81)\n  (Driver info: chromedriver\u003d2.40,platform\u003dLinux 4.4.0-134-generic x86_64) (WARNING: The server did not provide any stacktrace information)\nCommand duration or timeout: 0 milliseconds\nFor documentation on this error, please visit: http://seleniumhq.org/exceptions/no_such_element.html\nBuild info: version: \u00273.11.0\u0027, revision: \u0027e59cfb3\u0027, time: \u00272018-03-11T20:26:55.152Z\u0027\nSystem info: host: \u0027protl-vishal\u0027, ip: \u0027127.0.1.1\u0027, os.name: \u0027Linux\u0027, os.arch: \u0027amd64\u0027, os.version: \u00274.4.0-134-generic\u0027, java.version: \u00271.8.0_201\u0027\nDriver info: org.openqa.selenium.chrome.ChromeDriver\nCapabilities {acceptInsecureCerts: false, acceptSslCerts: false, applicationCacheEnabled: false, browserConnectionEnabled: false, browserName: chrome, chrome: {chromedriverVersion: 2.40, userDataDir: /tmp/.org.chromium.Chromium...}, cssSelectorsEnabled: true, databaseEnabled: false, goog:chromeOptions: {debuggerAddress: localhost:45193}, handlesAlerts: true, hasTouchScreen: false, javascriptEnabled: true, locationContextEnabled: true, mobileEmulationEnabled: false, nativeEvents: true, networkConnectionEnabled: false, pageLoadStrategy: normal, platform: LINUX, platformName: LINUX, rotatable: false, setWindowRect: true, takesHeapSnapshot: true, takesScreenshot: true, unexpectedAlertBehaviour: , unhandledPromptBehavior: , version: 69.0.3497.81, webStorageEnabled: true}\nSession ID: 59eea7eb99b4a2cabc10c06be3b1e134\n*** Element info: {Using\u003dxpath, value\u003d//li/a[@ng-click\u003d\u0027selectPage(page + 1, $event)\u0027]}\n\tat sun.reflect.GeneratedConstructorAccessor85.newInstance(Unknown Source)\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(DelegatingConstructorAccessorImpl.java:45)\n\tat java.lang.reflect.Constructor.newInstance(Constructor.java:423)\n\tat org.openqa.selenium.remote.ErrorHandler.createThrowable(ErrorHandler.java:214)\n\tat org.openqa.selenium.remote.ErrorHandler.throwIfResponseFailed(ErrorHandler.java:166)\n\tat org.openqa.selenium.remote.http.JsonHttpResponseCodec.reconstructValue(JsonHttpResponseCodec.java:40)\n\tat org.openqa.selenium.remote.http.AbstractHttpResponseCodec.decode(AbstractHttpResponseCodec.java:80)\n\tat org.openqa.selenium.remote.http.AbstractHttpResponseCodec.decode(AbstractHttpResponseCodec.java:44)\n\tat org.openqa.selenium.remote.HttpCommandExecutor.execute(HttpCommandExecutor.java:158)\n\tat org.openqa.selenium.remote.service.DriverCommandExecutor.execute(DriverCommandExecutor.java:83)\n\tat org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:545)\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:319)\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElementByXPath(RemoteWebDriver.java:421)\n\tat org.openqa.selenium.By$ByXPath.findElement(By.java:361)\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:311)\n\tat org.openqa.selenium.support.pagefactory.DefaultElementLocator.findElement(DefaultElementLocator.java:69)\n\tat org.openqa.selenium.support.pagefactory.AjaxElementLocator.access$001(AjaxElementLocator.java:40)\n\tat org.openqa.selenium.support.pagefactory.AjaxElementLocator$SlowLoadingElement.isLoaded(AjaxElementLocator.java:156)\n\tat org.openqa.selenium.support.ui.SlowLoadableComponent.get(SlowLoadableComponent.java:69)\n\tat org.openqa.selenium.support.pagefactory.AjaxElementLocator.findElement(AjaxElementLocator.java:86)\n\tat org.openqa.selenium.support.pagefactory.internal.LocatingElementHandler.invoke(LocatingElementHandler.java:38)\n\tat com.sun.proxy.$Proxy21.click(Unknown Source)\n\tat com.cucumber.framework.helper.PageObject.PaginationPageObject.clickNextBtnPagination(PaginationPageObject.java:90)\n\tat com.cucumber.framework.stepdefinition.Pagination.click_on_Next_button_of_Pagination(Pagination.java:38)\n\tat sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\n\tat sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\n\tat sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\n\tat java.lang.reflect.Method.invoke(Method.java:498)\n\tat cucumber.runtime.Utils$1.call(Utils.java:40)\n\tat cucumber.runtime.Timeout.timeout(Timeout.java:16)\n\tat cucumber.runtime.Utils.invoke(Utils.java:34)\n\tat cucumber.runtime.java.JavaStepDefinition.execute(JavaStepDefinition.java:38)\n\tat cucumber.runtime.StepDefinitionMatch.runStep(StepDefinitionMatch.java:37)\n\tat cucumber.runtime.Runtime.runStep(Runtime.java:300)\n\tat cucumber.runtime.model.StepContainer.runStep(StepContainer.java:44)\n\tat cucumber.runtime.model.StepContainer.runSteps(StepContainer.java:39)\n\tat cucumber.runtime.model.CucumberScenario.run(CucumberScenario.java:44)\n\tat cucumber.runtime.model.CucumberFeature.run(CucumberFeature.java:165)\n\tat cucumber.api.testng.TestNGCucumberRunner.runCucumber(TestNGCucumberRunner.java:63)\n\tat cucumber.api.testng.AbstractTestNGCucumberTests.feature(AbstractTestNGCucumberTests.java:21)\n\tat sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\n\tat sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\n\tat sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\n\tat java.lang.reflect.Method.invoke(Method.java:498)\n\tat org.testng.internal.MethodInvocationHelper.invokeMethod(MethodInvocationHelper.java:86)\n\tat org.testng.internal.Invoker.invokeMethod(Invoker.java:643)\n\tat org.testng.internal.Invoker.invokeTestMethod(Invoker.java:820)\n\tat org.testng.internal.Invoker.invokeTestMethods(Invoker.java:1128)\n\tat org.testng.internal.TestMethodWorker.invokeTestMethods(TestMethodWorker.java:129)\n\tat org.testng.internal.TestMethodWorker.run(TestMethodWorker.java:112)\n\tat org.testng.TestRunner.privateRun(TestRunner.java:782)\n\tat org.testng.TestRunner.run(TestRunner.java:632)\n\tat org.testng.SuiteRunner.runTest(SuiteRunner.java:366)\n\tat org.testng.SuiteRunner.runSequentially(SuiteRunner.java:361)\n\tat org.testng.SuiteRunner.privateRun(SuiteRunner.java:319)\n\tat org.testng.SuiteRunner.run(SuiteRunner.java:268)\n\tat org.testng.SuiteRunnerWorker.runSuite(SuiteRunnerWorker.java:52)\n\tat org.testng.SuiteRunnerWorker.run(SuiteRunnerWorker.java:86)\n\tat org.testng.TestNG.runSuitesSequentially(TestNG.java:1244)\n\tat org.testng.TestNG.runSuitesLocally(TestNG.java:1169)\n\tat org.testng.TestNG.run(TestNG.java:1064)\n\tat org.testng.remote.AbstractRemoteTestNG.run(AbstractRemoteTestNG.java:114)\n\tat org.testng.remote.RemoteTestNG.initAndRun(RemoteTestNG.java:251)\n\tat org.testng.remote.RemoteTestNG.main(RemoteTestNG.java:77)\n",
  "status": "failed"
});
formatter.match({
  "location": "Pagination.verify_that_the_system_should_display_the_second_page_of_the_listing_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.write("/home/nilay/Desktop/SeleniumCucumber/target/test-classes/screenshots/_2019-06-13/Verify pagination of Attributes listing page when user click on \"Next\".png");
formatter.embedding("image/png", "embedded13.png");
formatter.after({
  "duration": 3535370760,
  "status": "passed"
});
formatter.before({
  "duration": 746057982,
  "status": "passed"
});
formatter.scenario({
  "line": 140,
  "name": "",
  "description": "Verify pagination of Attributes listing page when user click on \"Previous\"",
  "id": "info:-executed-script-on-\"test1.beperfeqta.com/v32050\"-link.;",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 139,
      "name": "@chrome"
    }
  ]
});
formatter.step({
  "line": 142,
  "name": ": Navigate to the URL",
  "keyword": "Given "
});
formatter.step({
  "line": 143,
  "name": ": Enter valid credentials and Click on Login Button",
  "keyword": "And "
});
formatter.step({
  "line": 144,
  "name": ": Click on Administration Tile",
  "keyword": "And "
});
formatter.step({
  "line": 145,
  "name": ": Click on Attributes Tile",
  "keyword": "And "
});
formatter.step({
  "line": 146,
  "name": ": Click on Next button of Pagination",
  "keyword": "And "
});
formatter.step({
  "line": 147,
  "name": ": Click on the Previous button of Pagination",
  "keyword": "And "
});
formatter.step({
  "line": 148,
  "name": ": Verify that the system should display the first page of the listing screen",
  "keyword": "Then "
});
formatter.match({
  "location": "CommonFunctions.navigate_to_the_URL()"
});
formatter.result({
  "duration": 2573697120,
  "status": "passed"
});
formatter.match({
  "location": "CommonFunctions.enter_valid_credentials_and_Click_on_Login_Button()"
});
formatter.result({
  "duration": 6759529880,
  "status": "passed"
});
formatter.match({
  "location": "CommonFunctions.click_on_Administration_Tile()"
});
formatter.result({
  "duration": 1205125149,
  "status": "passed"
});
formatter.match({
  "location": "Attributes.click_on_Attributes_Tile()"
});
formatter.result({
  "duration": 262560373,
  "status": "passed"
});
formatter.match({
  "location": "Pagination.click_on_Next_button_of_Pagination()"
});
formatter.result({
  "duration": 30990913646,
  "error_message": "org.openqa.selenium.NoSuchElementException: Timed out after 10 seconds. Unable to locate the element\n\tat org.openqa.selenium.support.pagefactory.AjaxElementLocator.findElement(AjaxElementLocator.java:90)\n\tat org.openqa.selenium.support.pagefactory.internal.LocatingElementHandler.invoke(LocatingElementHandler.java:38)\n\tat com.sun.proxy.$Proxy21.click(Unknown Source)\n\tat com.cucumber.framework.helper.PageObject.PaginationPageObject.clickNextBtnPagination(PaginationPageObject.java:90)\n\tat com.cucumber.framework.stepdefinition.Pagination.click_on_Next_button_of_Pagination(Pagination.java:38)\n\tat ✽.And : Click on Next button of Pagination(featurefile/Attributes.feature:146)\nCaused by: org.openqa.selenium.NoSuchElementException: no such element: Unable to locate element: {\"method\":\"xpath\",\"selector\":\"//li/a[@ng-click\u003d\u0027selectPage(page + 1, $event)\u0027]\"}\n  (Session info: chrome\u003d69.0.3497.81)\n  (Driver info: chromedriver\u003d2.40,platform\u003dLinux 4.4.0-134-generic x86_64) (WARNING: The server did not provide any stacktrace information)\nCommand duration or timeout: 0 milliseconds\nFor documentation on this error, please visit: http://seleniumhq.org/exceptions/no_such_element.html\nBuild info: version: \u00273.11.0\u0027, revision: \u0027e59cfb3\u0027, time: \u00272018-03-11T20:26:55.152Z\u0027\nSystem info: host: \u0027protl-vishal\u0027, ip: \u0027127.0.1.1\u0027, os.name: \u0027Linux\u0027, os.arch: \u0027amd64\u0027, os.version: \u00274.4.0-134-generic\u0027, java.version: \u00271.8.0_201\u0027\nDriver info: org.openqa.selenium.chrome.ChromeDriver\nCapabilities {acceptInsecureCerts: false, acceptSslCerts: false, applicationCacheEnabled: false, browserConnectionEnabled: false, browserName: chrome, chrome: {chromedriverVersion: 2.40, userDataDir: /tmp/.org.chromium.Chromium...}, cssSelectorsEnabled: true, databaseEnabled: false, goog:chromeOptions: {debuggerAddress: localhost:36343}, handlesAlerts: true, hasTouchScreen: false, javascriptEnabled: true, locationContextEnabled: true, mobileEmulationEnabled: false, nativeEvents: true, networkConnectionEnabled: false, pageLoadStrategy: normal, platform: LINUX, platformName: LINUX, rotatable: false, setWindowRect: true, takesHeapSnapshot: true, takesScreenshot: true, unexpectedAlertBehaviour: , unhandledPromptBehavior: , version: 69.0.3497.81, webStorageEnabled: true}\nSession ID: 423a81001e74c92fcefd64765d18fa32\n*** Element info: {Using\u003dxpath, value\u003d//li/a[@ng-click\u003d\u0027selectPage(page + 1, $event)\u0027]}\n\tat sun.reflect.GeneratedConstructorAccessor85.newInstance(Unknown Source)\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(DelegatingConstructorAccessorImpl.java:45)\n\tat java.lang.reflect.Constructor.newInstance(Constructor.java:423)\n\tat org.openqa.selenium.remote.ErrorHandler.createThrowable(ErrorHandler.java:214)\n\tat org.openqa.selenium.remote.ErrorHandler.throwIfResponseFailed(ErrorHandler.java:166)\n\tat org.openqa.selenium.remote.http.JsonHttpResponseCodec.reconstructValue(JsonHttpResponseCodec.java:40)\n\tat org.openqa.selenium.remote.http.AbstractHttpResponseCodec.decode(AbstractHttpResponseCodec.java:80)\n\tat org.openqa.selenium.remote.http.AbstractHttpResponseCodec.decode(AbstractHttpResponseCodec.java:44)\n\tat org.openqa.selenium.remote.HttpCommandExecutor.execute(HttpCommandExecutor.java:158)\n\tat org.openqa.selenium.remote.service.DriverCommandExecutor.execute(DriverCommandExecutor.java:83)\n\tat org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:545)\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:319)\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElementByXPath(RemoteWebDriver.java:421)\n\tat org.openqa.selenium.By$ByXPath.findElement(By.java:361)\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:311)\n\tat org.openqa.selenium.support.pagefactory.DefaultElementLocator.findElement(DefaultElementLocator.java:69)\n\tat org.openqa.selenium.support.pagefactory.AjaxElementLocator.access$001(AjaxElementLocator.java:40)\n\tat org.openqa.selenium.support.pagefactory.AjaxElementLocator$SlowLoadingElement.isLoaded(AjaxElementLocator.java:156)\n\tat org.openqa.selenium.support.ui.SlowLoadableComponent.get(SlowLoadableComponent.java:69)\n\tat org.openqa.selenium.support.pagefactory.AjaxElementLocator.findElement(AjaxElementLocator.java:86)\n\tat org.openqa.selenium.support.pagefactory.internal.LocatingElementHandler.invoke(LocatingElementHandler.java:38)\n\tat com.sun.proxy.$Proxy21.click(Unknown Source)\n\tat com.cucumber.framework.helper.PageObject.PaginationPageObject.clickNextBtnPagination(PaginationPageObject.java:90)\n\tat com.cucumber.framework.stepdefinition.Pagination.click_on_Next_button_of_Pagination(Pagination.java:38)\n\tat sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\n\tat sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\n\tat sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\n\tat java.lang.reflect.Method.invoke(Method.java:498)\n\tat cucumber.runtime.Utils$1.call(Utils.java:40)\n\tat cucumber.runtime.Timeout.timeout(Timeout.java:16)\n\tat cucumber.runtime.Utils.invoke(Utils.java:34)\n\tat cucumber.runtime.java.JavaStepDefinition.execute(JavaStepDefinition.java:38)\n\tat cucumber.runtime.StepDefinitionMatch.runStep(StepDefinitionMatch.java:37)\n\tat cucumber.runtime.Runtime.runStep(Runtime.java:300)\n\tat cucumber.runtime.model.StepContainer.runStep(StepContainer.java:44)\n\tat cucumber.runtime.model.StepContainer.runSteps(StepContainer.java:39)\n\tat cucumber.runtime.model.CucumberScenario.run(CucumberScenario.java:44)\n\tat cucumber.runtime.model.CucumberFeature.run(CucumberFeature.java:165)\n\tat cucumber.api.testng.TestNGCucumberRunner.runCucumber(TestNGCucumberRunner.java:63)\n\tat cucumber.api.testng.AbstractTestNGCucumberTests.feature(AbstractTestNGCucumberTests.java:21)\n\tat sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\n\tat sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\n\tat sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\n\tat java.lang.reflect.Method.invoke(Method.java:498)\n\tat org.testng.internal.MethodInvocationHelper.invokeMethod(MethodInvocationHelper.java:86)\n\tat org.testng.internal.Invoker.invokeMethod(Invoker.java:643)\n\tat org.testng.internal.Invoker.invokeTestMethod(Invoker.java:820)\n\tat org.testng.internal.Invoker.invokeTestMethods(Invoker.java:1128)\n\tat org.testng.internal.TestMethodWorker.invokeTestMethods(TestMethodWorker.java:129)\n\tat org.testng.internal.TestMethodWorker.run(TestMethodWorker.java:112)\n\tat org.testng.TestRunner.privateRun(TestRunner.java:782)\n\tat org.testng.TestRunner.run(TestRunner.java:632)\n\tat org.testng.SuiteRunner.runTest(SuiteRunner.java:366)\n\tat org.testng.SuiteRunner.runSequentially(SuiteRunner.java:361)\n\tat org.testng.SuiteRunner.privateRun(SuiteRunner.java:319)\n\tat org.testng.SuiteRunner.run(SuiteRunner.java:268)\n\tat org.testng.SuiteRunnerWorker.runSuite(SuiteRunnerWorker.java:52)\n\tat org.testng.SuiteRunnerWorker.run(SuiteRunnerWorker.java:86)\n\tat org.testng.TestNG.runSuitesSequentially(TestNG.java:1244)\n\tat org.testng.TestNG.runSuitesLocally(TestNG.java:1169)\n\tat org.testng.TestNG.run(TestNG.java:1064)\n\tat org.testng.remote.AbstractRemoteTestNG.run(AbstractRemoteTestNG.java:114)\n\tat org.testng.remote.RemoteTestNG.initAndRun(RemoteTestNG.java:251)\n\tat org.testng.remote.RemoteTestNG.main(RemoteTestNG.java:77)\n",
  "status": "failed"
});
formatter.match({
  "location": "Pagination.click_on_the_Previous_button_of_Pagination()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "Pagination.verify_that_the_system_should_display_the_first_page_of_the_listing_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.write("/home/nilay/Desktop/SeleniumCucumber/target/test-classes/screenshots/_2019-06-13/.png");
formatter.embedding("image/png", "embedded14.png");
formatter.after({
  "duration": 3606847973,
  "status": "passed"
});
formatter.before({
  "duration": 1321364538,
  "status": "passed"
});
formatter.scenario({
  "line": 150,
  "name": "Verify pagination of Attributes listing page when user click on \"Last\"",
  "description": "",
  "id": "info:-executed-script-on-\"test1.beperfeqta.com/v32050\"-link.;verify-pagination-of-attributes-listing-page-when-user-click-on-\"last\"",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 149,
      "name": "@chrome"
    }
  ]
});
formatter.step({
  "line": 151,
  "name": ": Navigate to the URL",
  "keyword": "Given "
});
formatter.step({
  "line": 152,
  "name": ": Enter valid credentials and Click on Login Button",
  "keyword": "And "
});
formatter.step({
  "line": 153,
  "name": ": Click on Administration Tile",
  "keyword": "And "
});
formatter.step({
  "line": 154,
  "name": ": Click on Attributes Tile",
  "keyword": "And "
});
formatter.step({
  "line": 155,
  "name": ": Click on the Last button of Pagination",
  "keyword": "And "
});
formatter.step({
  "line": 156,
  "name": ": Verify that the system should display the last page of the listing screen",
  "keyword": "Then "
});
formatter.match({
  "location": "CommonFunctions.navigate_to_the_URL()"
});
formatter.result({
  "duration": 2951532249,
  "status": "passed"
});
formatter.match({
  "location": "CommonFunctions.enter_valid_credentials_and_Click_on_Login_Button()"
});
formatter.result({
  "duration": 6737267561,
  "status": "passed"
});
formatter.match({
  "location": "CommonFunctions.click_on_Administration_Tile()"
});
formatter.result({
  "duration": 1205104952,
  "status": "passed"
});
formatter.match({
  "location": "Attributes.click_on_Attributes_Tile()"
});
formatter.result({
  "duration": 283087399,
  "status": "passed"
});
formatter.match({
  "location": "Pagination.click_on_the_Last_button_of_Pagination()"
});
formatter.result({
  "duration": 31054556892,
  "error_message": "org.openqa.selenium.NoSuchElementException: Timed out after 10 seconds. Unable to locate the element\n\tat org.openqa.selenium.support.pagefactory.AjaxElementLocator.findElement(AjaxElementLocator.java:90)\n\tat org.openqa.selenium.support.pagefactory.internal.LocatingElementHandler.invoke(LocatingElementHandler.java:38)\n\tat com.sun.proxy.$Proxy21.click(Unknown Source)\n\tat com.cucumber.framework.helper.PageObject.PaginationPageObject.clickLastBtnPagination(PaginationPageObject.java:74)\n\tat com.cucumber.framework.stepdefinition.Pagination.click_on_the_Last_button_of_Pagination(Pagination.java:23)\n\tat ✽.And : Click on the Last button of Pagination(featurefile/Attributes.feature:155)\nCaused by: org.openqa.selenium.NoSuchElementException: no such element: Unable to locate element: {\"method\":\"xpath\",\"selector\":\"//a[@ng-click\u003d\u0027selectPage(totalPages, $event)\u0027]\"}\n  (Session info: chrome\u003d69.0.3497.81)\n  (Driver info: chromedriver\u003d2.40,platform\u003dLinux 4.4.0-134-generic x86_64) (WARNING: The server did not provide any stacktrace information)\nCommand duration or timeout: 0 milliseconds\nFor documentation on this error, please visit: http://seleniumhq.org/exceptions/no_such_element.html\nBuild info: version: \u00273.11.0\u0027, revision: \u0027e59cfb3\u0027, time: \u00272018-03-11T20:26:55.152Z\u0027\nSystem info: host: \u0027protl-vishal\u0027, ip: \u0027127.0.1.1\u0027, os.name: \u0027Linux\u0027, os.arch: \u0027amd64\u0027, os.version: \u00274.4.0-134-generic\u0027, java.version: \u00271.8.0_201\u0027\nDriver info: org.openqa.selenium.chrome.ChromeDriver\nCapabilities {acceptInsecureCerts: false, acceptSslCerts: false, applicationCacheEnabled: false, browserConnectionEnabled: false, browserName: chrome, chrome: {chromedriverVersion: 2.40, userDataDir: /tmp/.org.chromium.Chromium...}, cssSelectorsEnabled: true, databaseEnabled: false, goog:chromeOptions: {debuggerAddress: localhost:35597}, handlesAlerts: true, hasTouchScreen: false, javascriptEnabled: true, locationContextEnabled: true, mobileEmulationEnabled: false, nativeEvents: true, networkConnectionEnabled: false, pageLoadStrategy: normal, platform: LINUX, platformName: LINUX, rotatable: false, setWindowRect: true, takesHeapSnapshot: true, takesScreenshot: true, unexpectedAlertBehaviour: , unhandledPromptBehavior: , version: 69.0.3497.81, webStorageEnabled: true}\nSession ID: ae49e45eefb063e89ad993a9823bbf24\n*** Element info: {Using\u003dxpath, value\u003d//a[@ng-click\u003d\u0027selectPage(totalPages, $event)\u0027]}\n\tat sun.reflect.GeneratedConstructorAccessor85.newInstance(Unknown Source)\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(DelegatingConstructorAccessorImpl.java:45)\n\tat java.lang.reflect.Constructor.newInstance(Constructor.java:423)\n\tat org.openqa.selenium.remote.ErrorHandler.createThrowable(ErrorHandler.java:214)\n\tat org.openqa.selenium.remote.ErrorHandler.throwIfResponseFailed(ErrorHandler.java:166)\n\tat org.openqa.selenium.remote.http.JsonHttpResponseCodec.reconstructValue(JsonHttpResponseCodec.java:40)\n\tat org.openqa.selenium.remote.http.AbstractHttpResponseCodec.decode(AbstractHttpResponseCodec.java:80)\n\tat org.openqa.selenium.remote.http.AbstractHttpResponseCodec.decode(AbstractHttpResponseCodec.java:44)\n\tat org.openqa.selenium.remote.HttpCommandExecutor.execute(HttpCommandExecutor.java:158)\n\tat org.openqa.selenium.remote.service.DriverCommandExecutor.execute(DriverCommandExecutor.java:83)\n\tat org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:545)\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:319)\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElementByXPath(RemoteWebDriver.java:421)\n\tat org.openqa.selenium.By$ByXPath.findElement(By.java:361)\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:311)\n\tat org.openqa.selenium.support.pagefactory.DefaultElementLocator.findElement(DefaultElementLocator.java:69)\n\tat org.openqa.selenium.support.pagefactory.AjaxElementLocator.access$001(AjaxElementLocator.java:40)\n\tat org.openqa.selenium.support.pagefactory.AjaxElementLocator$SlowLoadingElement.isLoaded(AjaxElementLocator.java:156)\n\tat org.openqa.selenium.support.ui.SlowLoadableComponent.get(SlowLoadableComponent.java:69)\n\tat org.openqa.selenium.support.pagefactory.AjaxElementLocator.findElement(AjaxElementLocator.java:86)\n\tat org.openqa.selenium.support.pagefactory.internal.LocatingElementHandler.invoke(LocatingElementHandler.java:38)\n\tat com.sun.proxy.$Proxy21.click(Unknown Source)\n\tat com.cucumber.framework.helper.PageObject.PaginationPageObject.clickLastBtnPagination(PaginationPageObject.java:74)\n\tat com.cucumber.framework.stepdefinition.Pagination.click_on_the_Last_button_of_Pagination(Pagination.java:23)\n\tat sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\n\tat sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\n\tat sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\n\tat java.lang.reflect.Method.invoke(Method.java:498)\n\tat cucumber.runtime.Utils$1.call(Utils.java:40)\n\tat cucumber.runtime.Timeout.timeout(Timeout.java:16)\n\tat cucumber.runtime.Utils.invoke(Utils.java:34)\n\tat cucumber.runtime.java.JavaStepDefinition.execute(JavaStepDefinition.java:38)\n\tat cucumber.runtime.StepDefinitionMatch.runStep(StepDefinitionMatch.java:37)\n\tat cucumber.runtime.Runtime.runStep(Runtime.java:300)\n\tat cucumber.runtime.model.StepContainer.runStep(StepContainer.java:44)\n\tat cucumber.runtime.model.StepContainer.runSteps(StepContainer.java:39)\n\tat cucumber.runtime.model.CucumberScenario.run(CucumberScenario.java:44)\n\tat cucumber.runtime.model.CucumberFeature.run(CucumberFeature.java:165)\n\tat cucumber.api.testng.TestNGCucumberRunner.runCucumber(TestNGCucumberRunner.java:63)\n\tat cucumber.api.testng.AbstractTestNGCucumberTests.feature(AbstractTestNGCucumberTests.java:21)\n\tat sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\n\tat sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\n\tat sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\n\tat java.lang.reflect.Method.invoke(Method.java:498)\n\tat org.testng.internal.MethodInvocationHelper.invokeMethod(MethodInvocationHelper.java:86)\n\tat org.testng.internal.Invoker.invokeMethod(Invoker.java:643)\n\tat org.testng.internal.Invoker.invokeTestMethod(Invoker.java:820)\n\tat org.testng.internal.Invoker.invokeTestMethods(Invoker.java:1128)\n\tat org.testng.internal.TestMethodWorker.invokeTestMethods(TestMethodWorker.java:129)\n\tat org.testng.internal.TestMethodWorker.run(TestMethodWorker.java:112)\n\tat org.testng.TestRunner.privateRun(TestRunner.java:782)\n\tat org.testng.TestRunner.run(TestRunner.java:632)\n\tat org.testng.SuiteRunner.runTest(SuiteRunner.java:366)\n\tat org.testng.SuiteRunner.runSequentially(SuiteRunner.java:361)\n\tat org.testng.SuiteRunner.privateRun(SuiteRunner.java:319)\n\tat org.testng.SuiteRunner.run(SuiteRunner.java:268)\n\tat org.testng.SuiteRunnerWorker.runSuite(SuiteRunnerWorker.java:52)\n\tat org.testng.SuiteRunnerWorker.run(SuiteRunnerWorker.java:86)\n\tat org.testng.TestNG.runSuitesSequentially(TestNG.java:1244)\n\tat org.testng.TestNG.runSuitesLocally(TestNG.java:1169)\n\tat org.testng.TestNG.run(TestNG.java:1064)\n\tat org.testng.remote.AbstractRemoteTestNG.run(AbstractRemoteTestNG.java:114)\n\tat org.testng.remote.RemoteTestNG.initAndRun(RemoteTestNG.java:251)\n\tat org.testng.remote.RemoteTestNG.main(RemoteTestNG.java:77)\n",
  "status": "failed"
});
formatter.match({
  "location": "Pagination.verify_that_the_system_should_display_the_last_page_of_the_listing_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.write("/home/nilay/Desktop/SeleniumCucumber/target/test-classes/screenshots/_2019-06-13/Verify pagination of Attributes listing page when user click on \"Last\".png");
formatter.embedding("image/png", "embedded15.png");
formatter.after({
  "duration": 3585905482,
  "status": "passed"
});
formatter.before({
  "duration": 706542968,
  "status": "passed"
});
formatter.scenario({
  "line": 159,
  "name": "Verify the record count of Attribute listing screen",
  "description": "",
  "id": "info:-executed-script-on-\"test1.beperfeqta.com/v32050\"-link.;verify-the-record-count-of-attribute-listing-screen",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 158,
      "name": "@chrome"
    }
  ]
});
formatter.step({
  "line": 160,
  "name": ": Navigate to the URL",
  "keyword": "Given "
});
formatter.step({
  "line": 161,
  "name": ": Enter valid credentials and Click on Login Button",
  "keyword": "And "
});
formatter.step({
  "line": 162,
  "name": ": Click on Administration Tile",
  "keyword": "And "
});
formatter.step({
  "line": 163,
  "name": ": Click on Attributes Tile",
  "keyword": "And "
});
formatter.step({
  "line": 164,
  "name": ": Verify the record count",
  "keyword": "Then "
});
formatter.match({
  "location": "CommonFunctions.navigate_to_the_URL()"
});
formatter.result({
  "duration": 3116244515,
  "status": "passed"
});
formatter.match({
  "location": "CommonFunctions.enter_valid_credentials_and_Click_on_Login_Button()"
});
formatter.result({
  "duration": 6746572505,
  "status": "passed"
});
formatter.match({
  "location": "CommonFunctions.click_on_Administration_Tile()"
});
formatter.result({
  "duration": 1209670519,
  "status": "passed"
});
formatter.match({
  "location": "Attributes.click_on_Attributes_Tile()"
});
formatter.result({
  "duration": 275022485,
  "status": "passed"
});
formatter.match({
  "location": "Pagination.verify_the_record_count()"
});
formatter.result({
  "duration": 5461962141,
  "status": "passed"
});
formatter.embedding("image/png", "embedded16.png");
formatter.after({
  "duration": 3522694679,
  "status": "passed"
});
formatter.before({
  "duration": 1035263933,
  "status": "passed"
});
formatter.scenario({
  "line": 168,
  "name": "",
  "description": "Verify the navigation of \"View\" link of Linked Information column of Attribute listing screen",
  "id": "info:-executed-script-on-\"test1.beperfeqta.com/v32050\"-link.;",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 167,
      "name": "@chrome"
    }
  ]
});
formatter.step({
  "line": 170,
  "name": ": Navigate to the URL",
  "keyword": "Given "
});
formatter.step({
  "line": 171,
  "name": ": Enter valid credentials and Click on Login Button",
  "keyword": "And "
});
formatter.step({
  "line": 172,
  "name": ": Click on Administration Tile",
  "keyword": "And "
});
formatter.step({
  "line": 173,
  "name": ": Click on Attributes Tile",
  "keyword": "And "
});
formatter.step({
  "line": 174,
  "name": ": Click on View link of the first record of Attribute listing screen",
  "keyword": "And "
});
formatter.step({
  "line": 175,
  "name": ": Verify that popup should appear on the screen with the label \"Your current Attribute is used in these Apps:\"",
  "keyword": "Then "
});
formatter.match({
  "location": "CommonFunctions.navigate_to_the_URL()"
});
formatter.result({
  "duration": 3436520645,
  "status": "passed"
});
formatter.match({
  "location": "CommonFunctions.enter_valid_credentials_and_Click_on_Login_Button()"
});
formatter.result({
  "duration": 6704589928,
  "status": "passed"
});
formatter.match({
  "location": "CommonFunctions.click_on_Administration_Tile()"
});
formatter.result({
  "duration": 1121663546,
  "status": "passed"
});
formatter.match({
  "location": "Attributes.click_on_Attributes_Tile()"
});
formatter.result({
  "duration": 226763587,
  "status": "passed"
});
formatter.match({
  "location": "Attributes.click_on_View_link_of_the_first_record_of_Attribute_listing_screen()"
});
formatter.result({
  "duration": 542868967,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Your current Attribute is used in these Apps:",
      "offset": 64
    }
  ],
  "location": "Attributes.verify_that_popup_should_appear_on_the_screen_with_the_label(String)"
});
formatter.result({
  "duration": 1298707021,
  "status": "passed"
});
formatter.embedding("image/png", "embedded17.png");
formatter.after({
  "duration": 440340830,
  "error_message": "org.openqa.selenium.WebDriverException: unknown error: Element \u003ci class\u003d\"header-username-bx ng-binding\"\u003e...\u003c/i\u003e is not clickable at point (1210, 10). Other element would receive the click: \u003cdiv class\u003d\"modal-backdrop fade ng-animate ng-leave ng-leave-active\" uib-modal-animation-class\u003d\"fade\" modal-in-class\u003d\"in\" ng-style\u003d\"{\u0027z-index\u0027: 1040 + (index \u0026amp;\u0026amp; 1 || 0) + index*10}\" uib-modal-backdrop\u003d\"modal-backdrop\" modal-animation\u003d\"true\" style\u003d\"z-index: 1040;\" data-ng-animate\u003d\"2\"\u003e\u003c/div\u003e\n  (Session info: chrome\u003d69.0.3497.81)\n  (Driver info: chromedriver\u003d2.40,platform\u003dLinux 4.4.0-134-generic x86_64) (WARNING: The server did not provide any stacktrace information)\nCommand duration or timeout: 0 milliseconds\nBuild info: version: \u00273.11.0\u0027, revision: \u0027e59cfb3\u0027, time: \u00272018-03-11T20:26:55.152Z\u0027\nSystem info: host: \u0027protl-vishal\u0027, ip: \u0027127.0.1.1\u0027, os.name: \u0027Linux\u0027, os.arch: \u0027amd64\u0027, os.version: \u00274.4.0-134-generic\u0027, java.version: \u00271.8.0_201\u0027\nDriver info: org.openqa.selenium.chrome.ChromeDriver\nCapabilities {acceptInsecureCerts: false, acceptSslCerts: false, applicationCacheEnabled: false, browserConnectionEnabled: false, browserName: chrome, chrome: {chromedriverVersion: 2.40, userDataDir: /tmp/.org.chromium.Chromium...}, cssSelectorsEnabled: true, databaseEnabled: false, goog:chromeOptions: {debuggerAddress: localhost:33556}, handlesAlerts: true, hasTouchScreen: false, javascriptEnabled: true, locationContextEnabled: true, mobileEmulationEnabled: false, nativeEvents: true, networkConnectionEnabled: false, pageLoadStrategy: normal, platform: LINUX, platformName: LINUX, rotatable: false, setWindowRect: true, takesHeapSnapshot: true, takesScreenshot: true, unexpectedAlertBehaviour: , unhandledPromptBehavior: , version: 69.0.3497.81, webStorageEnabled: true}\nSession ID: 110bed3f8c48fe8f5d31e7de4108f963\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(NativeConstructorAccessorImpl.java:62)\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(DelegatingConstructorAccessorImpl.java:45)\n\tat java.lang.reflect.Constructor.newInstance(Constructor.java:423)\n\tat org.openqa.selenium.remote.ErrorHandler.createThrowable(ErrorHandler.java:214)\n\tat org.openqa.selenium.remote.ErrorHandler.throwIfResponseFailed(ErrorHandler.java:166)\n\tat org.openqa.selenium.remote.http.JsonHttpResponseCodec.reconstructValue(JsonHttpResponseCodec.java:40)\n\tat org.openqa.selenium.remote.http.AbstractHttpResponseCodec.decode(AbstractHttpResponseCodec.java:80)\n\tat org.openqa.selenium.remote.http.AbstractHttpResponseCodec.decode(AbstractHttpResponseCodec.java:44)\n\tat org.openqa.selenium.remote.HttpCommandExecutor.execute(HttpCommandExecutor.java:158)\n\tat org.openqa.selenium.remote.service.DriverCommandExecutor.execute(DriverCommandExecutor.java:83)\n\tat org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:545)\n\tat org.openqa.selenium.remote.RemoteWebElement.execute(RemoteWebElement.java:279)\n\tat org.openqa.selenium.remote.RemoteWebElement.click(RemoteWebElement.java:83)\n\tat com.cucumber.framework.helper.InitializeWebDrive.tearDownLogout(InitializeWebDrive.java:195)\n\tat com.cucumber.framework.helper.InitializeWebDrive.tearDownDriver(InitializeWebDrive.java:176)\n\tat com.cucumber.framework.helper.InitializeWebDrive.afterChrome(InitializeWebDrive.java:129)\n\tat sun.reflect.GeneratedMethodAccessor30.invoke(Unknown Source)\n\tat sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\n\tat java.lang.reflect.Method.invoke(Method.java:498)\n\tat cucumber.runtime.Utils$1.call(Utils.java:40)\n\tat cucumber.runtime.Timeout.timeout(Timeout.java:16)\n\tat cucumber.runtime.Utils.invoke(Utils.java:34)\n\tat cucumber.runtime.java.JavaHookDefinition.execute(JavaHookDefinition.java:60)\n\tat cucumber.runtime.Runtime.runHookIfTagsMatch(Runtime.java:224)\n\tat cucumber.runtime.Runtime.runHooks(Runtime.java:212)\n\tat cucumber.runtime.Runtime.runAfterHooks(Runtime.java:206)\n\tat cucumber.runtime.model.CucumberScenario.run(CucumberScenario.java:46)\n\tat cucumber.runtime.model.CucumberFeature.run(CucumberFeature.java:165)\n\tat cucumber.api.testng.TestNGCucumberRunner.runCucumber(TestNGCucumberRunner.java:63)\n\tat cucumber.api.testng.AbstractTestNGCucumberTests.feature(AbstractTestNGCucumberTests.java:21)\n\tat sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\n\tat sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\n\tat sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\n\tat java.lang.reflect.Method.invoke(Method.java:498)\n\tat org.testng.internal.MethodInvocationHelper.invokeMethod(MethodInvocationHelper.java:86)\n\tat org.testng.internal.Invoker.invokeMethod(Invoker.java:643)\n\tat org.testng.internal.Invoker.invokeTestMethod(Invoker.java:820)\n\tat org.testng.internal.Invoker.invokeTestMethods(Invoker.java:1128)\n\tat org.testng.internal.TestMethodWorker.invokeTestMethods(TestMethodWorker.java:129)\n\tat org.testng.internal.TestMethodWorker.run(TestMethodWorker.java:112)\n\tat org.testng.TestRunner.privateRun(TestRunner.java:782)\n\tat org.testng.TestRunner.run(TestRunner.java:632)\n\tat org.testng.SuiteRunner.runTest(SuiteRunner.java:366)\n\tat org.testng.SuiteRunner.runSequentially(SuiteRunner.java:361)\n\tat org.testng.SuiteRunner.privateRun(SuiteRunner.java:319)\n\tat org.testng.SuiteRunner.run(SuiteRunner.java:268)\n\tat org.testng.SuiteRunnerWorker.runSuite(SuiteRunnerWorker.java:52)\n\tat org.testng.SuiteRunnerWorker.run(SuiteRunnerWorker.java:86)\n\tat org.testng.TestNG.runSuitesSequentially(TestNG.java:1244)\n\tat org.testng.TestNG.runSuitesLocally(TestNG.java:1169)\n\tat org.testng.TestNG.run(TestNG.java:1064)\n\tat org.testng.remote.AbstractRemoteTestNG.run(AbstractRemoteTestNG.java:114)\n\tat org.testng.remote.RemoteTestNG.initAndRun(RemoteTestNG.java:251)\n\tat org.testng.remote.RemoteTestNG.main(RemoteTestNG.java:77)\n",
  "status": "failed"
});
formatter.before({
  "duration": 726520198,
  "status": "passed"
});
formatter.scenario({
  "line": 178,
  "name": "Verify Edit Attribute functionality",
  "description": "",
  "id": "info:-executed-script-on-\"test1.beperfeqta.com/v32050\"-link.;verify-edit-attribute-functionality",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 177,
      "name": "@chrome"
    }
  ]
});
formatter.step({
  "line": 179,
  "name": ": Navigate to the URL",
  "keyword": "Given "
});
formatter.step({
  "line": 180,
  "name": ": Enter valid credentials and Click on Login Button",
  "keyword": "And "
});
formatter.step({
  "line": 181,
  "name": ": Click on Administration Tile",
  "keyword": "And "
});
formatter.step({
  "line": 182,
  "name": ": Click on Attributes Tile",
  "keyword": "And "
});
formatter.step({
  "line": 183,
  "name": ": Click on the first record of \"Attributes\" column",
  "keyword": "And "
});
formatter.step({
  "line": 184,
  "name": ": Verify the name of the page as \"Edit Attribute\" and the current version",
  "keyword": "Then "
});
formatter.match({
  "location": "CommonFunctions.navigate_to_the_URL()"
});
formatter.result({
  "duration": 3307748485,
  "status": "passed"
});
formatter.match({
  "location": "CommonFunctions.enter_valid_credentials_and_Click_on_Login_Button()"
});
formatter.result({
  "duration": 9824119434,
  "status": "passed"
});
formatter.match({
  "location": "CommonFunctions.click_on_Administration_Tile()"
});
formatter.result({
  "duration": 1217084017,
  "status": "passed"
});
formatter.match({
  "location": "Attributes.click_on_Attributes_Tile()"
});
formatter.result({
  "duration": 271529528,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Attributes",
      "offset": 32
    }
  ],
  "location": "Attributes.click_on_the_first_record_of_column(String)"
});
formatter.result({
  "duration": 634690314,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Edit Attribute",
      "offset": 34
    }
  ],
  "location": "Attributes.verify_the_name_of_the_page_as_and_the_current_version(String)"
});
formatter.result({
  "duration": 1204019126,
  "status": "passed"
});
formatter.embedding("image/png", "embedded18.png");
formatter.after({
  "duration": 3447868465,
  "status": "passed"
});
formatter.before({
  "duration": 790082209,
  "status": "passed"
});
formatter.scenario({
  "line": 187,
  "name": "Verify Attribute update functionality",
  "description": "",
  "id": "info:-executed-script-on-\"test1.beperfeqta.com/v32050\"-link.;verify-attribute-update-functionality",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 186,
      "name": "@chrome"
    }
  ]
});
formatter.step({
  "line": 188,
  "name": ": Navigate to the URL",
  "keyword": "Given "
});
formatter.step({
  "line": 189,
  "name": ": Enter valid credentials and Click on Login Button",
  "keyword": "And "
});
formatter.step({
  "line": 190,
  "name": ": Click on Administration Tile",
  "keyword": "And "
});
formatter.step({
  "line": 191,
  "name": ": Click on Attributes Tile",
  "keyword": "And "
});
formatter.step({
  "line": 192,
  "name": ": Click on the first record of \"Attributes\" column",
  "keyword": "And "
});
formatter.step({
  "line": 193,
  "name": ": Update the data",
  "keyword": "And "
});
formatter.step({
  "line": 194,
  "name": ": Click on save button",
  "keyword": "And "
});
formatter.step({
  "line": 195,
  "name": ": Verify that the updated field should be saved and appear on the screen",
  "keyword": "Then "
});
formatter.match({
  "location": "CommonFunctions.navigate_to_the_URL()"
});
formatter.result({
  "duration": 2702428015,
  "status": "passed"
});
formatter.match({
  "location": "CommonFunctions.enter_valid_credentials_and_Click_on_Login_Button()"
});
formatter.result({
  "duration": 6768223233,
  "status": "passed"
});
formatter.match({
  "location": "CommonFunctions.click_on_Administration_Tile()"
});
formatter.result({
  "duration": 1220591210,
  "status": "passed"
});
formatter.match({
  "location": "Attributes.click_on_Attributes_Tile()"
});
formatter.result({
  "duration": 268199228,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Attributes",
      "offset": 32
    }
  ],
  "location": "Attributes.click_on_the_first_record_of_column(String)"
});
formatter.result({
  "duration": 615107167,
  "status": "passed"
});
formatter.match({
  "location": "Attributes.update_the_data()"
});
formatter.result({
  "duration": 116345,
  "status": "passed"
});
formatter.match({
  "location": "Attributes.click_on_save_button()"
});
formatter.result({
  "duration": 475394847,
  "status": "passed"
});
formatter.match({
  "location": "Attributes.verify_that_the_updated_field_should_be_saved_and_appear_on_the_screen()"
});
formatter.result({
  "duration": 1038849296,
  "status": "passed"
});
formatter.embedding("image/png", "embedded19.png");
formatter.after({
  "duration": 3468818358,
  "status": "passed"
});
formatter.before({
  "duration": 768910918,
  "status": "passed"
});
formatter.scenario({
  "line": 198,
  "name": "Verify the cancel button functionality of Edit Attribute screen",
  "description": "",
  "id": "info:-executed-script-on-\"test1.beperfeqta.com/v32050\"-link.;verify-the-cancel-button-functionality-of-edit-attribute-screen",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 197,
      "name": "@chrome"
    }
  ]
});
formatter.step({
  "line": 199,
  "name": ": Navigate to the URL",
  "keyword": "Given "
});
formatter.step({
  "line": 200,
  "name": ": Enter valid credentials and Click on Login Button",
  "keyword": "And "
});
formatter.step({
  "line": 201,
  "name": ": Click on Administration Tile",
  "keyword": "And "
});
formatter.step({
  "line": 202,
  "name": ": Click on Attributes Tile",
  "keyword": "And "
});
formatter.step({
  "line": 203,
  "name": ": Click on the first record of \"Attributes\" column",
  "keyword": "And "
});
formatter.step({
  "line": 204,
  "name": ": Click on cancel button",
  "keyword": "And "
});
formatter.step({
  "line": 205,
  "name": ": Verify that page should be redirected back to the Attribute listing screen",
  "keyword": "Then "
});
formatter.match({
  "location": "CommonFunctions.navigate_to_the_URL()"
});
formatter.result({
  "duration": 4132699669,
  "status": "passed"
});
formatter.match({
  "location": "CommonFunctions.enter_valid_credentials_and_Click_on_Login_Button()"
});
formatter.result({
  "duration": 6730263052,
  "status": "passed"
});
formatter.match({
  "location": "CommonFunctions.click_on_Administration_Tile()"
});
formatter.result({
  "duration": 1207866814,
  "status": "passed"
});
formatter.match({
  "location": "Attributes.click_on_Attributes_Tile()"
});
formatter.result({
  "duration": 122894755,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Attributes",
      "offset": 32
    }
  ],
  "location": "Attributes.click_on_the_first_record_of_column(String)"
});
formatter.result({
  "duration": 731198419,
  "status": "passed"
});
formatter.match({
  "location": "Attributes.click_on_cancel_button()"
});
formatter.result({
  "duration": 515561835,
  "status": "passed"
});
formatter.match({
  "location": "Attributes.verify_that_page_should_be_redirected_back_to_the_Attribute_listing_screen()"
});
formatter.result({
  "duration": 66922623,
  "status": "passed"
});
formatter.embedding("image/png", "embedded20.png");
formatter.after({
  "duration": 3536206967,
  "status": "passed"
});
formatter.before({
  "duration": 735446080,
  "status": "passed"
});
formatter.scenario({
  "line": 208,
  "name": "Verify the Cancel button color of Edit Attribute screen",
  "description": "",
  "id": "info:-executed-script-on-\"test1.beperfeqta.com/v32050\"-link.;verify-the-cancel-button-color-of-edit-attribute-screen",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 207,
      "name": "@chrome"
    }
  ]
});
formatter.step({
  "line": 209,
  "name": ": Navigate to the URL",
  "keyword": "Given "
});
formatter.step({
  "line": 210,
  "name": ": Enter valid credentials and Click on Login Button",
  "keyword": "And "
});
formatter.step({
  "line": 211,
  "name": ": Click on Administration Tile",
  "keyword": "And "
});
formatter.step({
  "line": 212,
  "name": ": Click on Attributes Tile",
  "keyword": "And "
});
formatter.step({
  "line": 213,
  "name": ": Click on the first record of \"Attributes\" column",
  "keyword": "And "
});
formatter.step({
  "line": 214,
  "name": ": Verify the cancel button color",
  "keyword": "Then "
});
formatter.match({
  "location": "CommonFunctions.navigate_to_the_URL()"
});
formatter.result({
  "duration": 3821098886,
  "status": "passed"
});
formatter.match({
  "location": "CommonFunctions.enter_valid_credentials_and_Click_on_Login_Button()"
});
formatter.result({
  "duration": 6715311147,
  "status": "passed"
});
formatter.match({
  "location": "CommonFunctions.click_on_Administration_Tile()"
});
formatter.result({
  "duration": 1106669894,
  "status": "passed"
});
formatter.match({
  "location": "Attributes.click_on_Attributes_Tile()"
});
formatter.result({
  "duration": 239911766,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Attributes",
      "offset": 32
    }
  ],
  "location": "Attributes.click_on_the_first_record_of_column(String)"
});
formatter.result({
  "duration": 740305790,
  "status": "passed"
});
formatter.match({
  "location": "Attributes.verify_the_cancel_button_color()"
});
formatter.result({
  "duration": 460457320,
  "status": "passed"
});
formatter.embedding("image/png", "embedded21.png");
formatter.after({
  "duration": 3445552615,
  "status": "passed"
});
formatter.before({
  "duration": 725762398,
  "status": "passed"
});
formatter.scenario({
  "line": 217,
  "name": "Verify the save button color of Edit Attribute screen",
  "description": "",
  "id": "info:-executed-script-on-\"test1.beperfeqta.com/v32050\"-link.;verify-the-save-button-color-of-edit-attribute-screen",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 216,
      "name": "@chrome"
    }
  ]
});
formatter.step({
  "line": 218,
  "name": ": Navigate to the URL",
  "keyword": "Given "
});
formatter.step({
  "line": 219,
  "name": ": Enter valid credentials and Click on Login Button",
  "keyword": "And "
});
formatter.step({
  "line": 220,
  "name": ": Click on Administration Tile",
  "keyword": "And "
});
formatter.step({
  "line": 221,
  "name": ": Click on Attributes Tile",
  "keyword": "And "
});
formatter.step({
  "line": 222,
  "name": ": Click on the first record of \"Attributes\" column",
  "keyword": "And "
});
formatter.step({
  "line": 223,
  "name": ": Verify the save button color",
  "keyword": "Then "
});
formatter.match({
  "location": "CommonFunctions.navigate_to_the_URL()"
});
formatter.result({
  "duration": 2340815400,
  "status": "passed"
});
formatter.match({
  "location": "CommonFunctions.enter_valid_credentials_and_Click_on_Login_Button()"
});
formatter.result({
  "duration": 6728088782,
  "status": "passed"
});
formatter.match({
  "location": "CommonFunctions.click_on_Administration_Tile()"
});
formatter.result({
  "duration": 1199719215,
  "status": "passed"
});
formatter.match({
  "location": "Attributes.click_on_Attributes_Tile()"
});
formatter.result({
  "duration": 276003076,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Attributes",
      "offset": 32
    }
  ],
  "location": "Attributes.click_on_the_first_record_of_column(String)"
});
formatter.result({
  "duration": 546399158,
  "status": "passed"
});
formatter.match({
  "location": "Attributes.verify_the_save_button_color()"
});
formatter.result({
  "duration": 374483330,
  "status": "passed"
});
formatter.embedding("image/png", "embedded22.png");
formatter.after({
  "duration": 3449262303,
  "status": "passed"
});
});