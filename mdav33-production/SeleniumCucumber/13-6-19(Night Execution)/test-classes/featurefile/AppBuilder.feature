Feature: Info: Executed script on "https://test1.beperfeqta.com/mdav33/#/login" link.
  Module Name: App Builder

  @chrome @done
  Scenario: Verify the module title when user click on "App Builder" module
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    Then : Verify module name as App Builder

  @chrome @common
  Scenario: Verify Last button of App Builder Page Pagination
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Last button of Pagination
    Then : Verify that system should display the Last page of the App Builder listing screen

  @chrome @common
  Scenario: Verify First button of App Builder Page Pagination
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on First button of Pagination
    Then : Verify that system should display the First page of the App Builder listing screen

  @chrome @common
  Scenario: Verify Pagination of App Builder Pag
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    Then : Verify total number of entries should be match with pagination of App Builder screen

  @chrome @common
  Scenario: Verify Search Functionality of App Builder Screen
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Enter the App name into search box of App Builder screen
    Then : Verify the search result

  @chrome @common
  Scenario: Verify the Ascending order functionality for Apps column of App Builder screen
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Sorting icon of the Apps column for ascending order sorting
    Then : Verify that all the records of Apps column display in ascending order

  @chrome @common
  Scenario: Verify the Descending order functionality for Apps column of App Builder screen
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Sorting icon of the Apps column for descending order sorting
    Then : Verify that all the records of Apps column display in descending order

  @chrome @common
  Scenario: Verify View Audit Trail Page redirection of App Builder
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on View Audit Trail link of first record from the Audit Trail column
    Then : Verify that system should be redirect to the Audit Trail screen when a user click on the View Audit Trail link of the first record

  @chrome @common
  Scenario: Verify Pagination for Audit Trail Page
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on View Audit Trail link of first record from the Audit Trail column
    Then : Verify total number of entries should be match with pagination of Audit Trail screen

  @chrome @common
  Scenario: Verify Back Button color for Audit Trail Page
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on View Audit Trail link of first record from the Audit Trail column
    Then : Verify that color of Back button should be black

  @chrome @common
  Scenario: Verify Audit Trail back button functionality of App Builder
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on View Audit Trail link of first record from the Audit Trail column
    Then : Verify that color of Back button should be black

  @chrome @common
  Scenario: Verify Audit Trail back button functionality of App Builder
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on View Audit Trail link of first record from the Audit Trail column
    And : Click on Back button
    Then : Verify that system should be redirect to the App Builder screen when a user click on the Back button

  #	In this scenario @After will throw an Exception which is not an issue.
  @chrome @done
  Scenario: Verify Copy pop-up
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Create Copy icon of first record from the Action column
    Then : Verify that system should display Copy pop-up when a user click on the Create Copy icon

  #	In this scenario @After will throw an Exception which is not an issue.
  @chrome @done
  Scenario: Verify Default value while creating Copy of App
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    Then : Verify that system should display Default App Title into Copy pop-up when a user create copy of a App

  @chrome @pending
  Scenario: Verify creating copy of App functionality
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Create Copy icon of first record from the Action column
    And : Click on OK button of Copy pop-up
    Then : Verify that a user is allow to create copy of app by click on Create Copy icon

  @chrome @done
  Scenario: Verify Color of Add New Button
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    Then : Verify that color of Add New button should be blue

  @chrome @done
  Scenario: Verify Add New Button functionality
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    Then : Verify that Build App page should be display when a user Click on Add New button of App Builder

  @chrome @done
  Scenario: Verify '1  BASIC' text in Basic tab
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    Then : Verify that 1  BASIC text should be displayed in Basic tab

  @chrome @done
  Scenario: Verify color of Basic tab
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    Then : Verify that Color of the Basic tab should be green

  @chrome @done
  Scenario: Verify App Details label in Basic Tab
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    Then : Verify that App Details label should be displayed in top left side of Basic Tab

  @chrome @done
  Scenario: Verify Current Version of App in Basic Tab
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    Then : Verify that a system should display the Current Version of the App in top right side of Basic Tab

  @chrome @done
  Scenario: Verify Current Version message color in Basic tab
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    Then : Verify that Color of Current Version message should blue

  @chrome @done
  Scenario: Verify Site Selections Section label in Basic tab
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    Then : Verify that Site Selections label should be displayed after App Details section in Basic Tab

  @chrome @done
  Scenario: Verify other tabs are disabled when User access Basic Tab
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    Then : Verify that all other tabs should be displayed in disable mode when a user access Basic Tab

  @chrome @done
  Scenario: Verify Required field validation for App Module
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Press "TAB" Key on Select App Module
    Then : Verify that system should display Required validation message as App Module is required.

  @chrome @done
  Scenario: Verify Color of Save Draft Button
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    Then : Verify that Color of Save Draft button should be blue

  @chrome @done
  Scenario: Verify Color of Continue Button
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    Then : Verify that Color of Continue button should be green

  @chrome @done
  Scenario: Verify Color of Cancel Button
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    Then : Verify that Color of App Builder Cancel button should be black

  #	In this scenario @After will throw an Exception which is not an issue.
  @chrome @done
  Scenario: Verify Alertbox appear on Click on Cancel button of App Builder
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Click on Cancel button of App Builder
    Then : Verify that system should display Alertbox when a user click on the Cancel button

  #	In this scenario @After will throw an Exception which is not an issue.
  @chrome @done
  Scenario: Verify Alertbox Message that appear on Click on Cancel button of App Builder
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Click on Cancel button of App Builder
    Then : Verify that system should display Alertbox message as Whoa! Are you sure? All changes will be lost.

  #	In this scenario @After will throw an Exception which is not an issue.
  @chrome @done
  Scenario: Verify Color of No Button in Alertbox
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Click on Cancel button of App Builder
    Then : Verify that color of No button in Alertbox should be red

  #	In this scenario @After will throw an Exception which is not an issue.
  @chrome @done
  Scenario: Verify Color of Yes Button in Alertbox
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Click on Cancel button of App Builder
    Then : Verify that color of Yes button in Alertbox should be green

  @chrome @done
  Scenario: Verify Alertbox disappear on Click on No Button
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Click on Cancel button of App Builder
    And : Click on No button of Alert Popup
    Then : Verify that Alertbox should be disappear when a user click on the No button

  @chrome @done
  Scenario: Verify system is redirected to Listing Page on click of Yes Button of Alertbox
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Click on Cancel button of App Builder
    And : Click on Yes button of Alert Popup
    Then : Verify that system should redirect to the App Listing page when a user click on the Yes button of Alertbox

  @chrome @done
  Scenario: Verify Required validations message for App Title field
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Click on the App Title textbox and Press "TAB" Key
    Then : Verify the Required validation message for App Title field as "App Title is required."

  @chrome @done
  Scenario: Verify color of App title Required validations message
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Click on the App Title textbox and Press "TAB" Key
    Then : Verify that color of Required validation for App Title field message should be red

  @chrome @done
  Scenario: Verify Minimum validation message for App title textbox
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Click on App title textbox and Enter App title as value less than 2 characters
    Then : Verify Minimum validation message of App title textbox asApp title must be at least 2 characters.

  @chrome @done
  Scenario: Verify color of Minimum validation message for App title field
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Click on App title textbox and Enter App title as value less than 2 characters
    Then : Verify that color of Minimum validation message for App Title field should be red

  @chrome @done
  Scenario: Verify Maximum validation message for App title textbox
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Click on App title textbox and Enter App title value more than 50 characters
    Then : Verify Maximum validation message of App title textbox as App title cannot be more than 50 characters.

  @chrome @done
  Scenario: Verify color of Maximum validation for App title field
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Click on App title textbox and Enter App title value more than 50 characters
    Then : Verify that color of Maximum validation message for App Title field should be red

  @chrome @done
  Scenario: Verify no validation message for App title field
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Click on App title textbox and Enter Valid Value in App title textbox
    Then : Verify that system should not display validation message when a user enter valid App Title

  @chrome @demo
  Scenario: Verify Minimum validation for App Description field
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Click on App Description textbox and Enter App Description as value less than 2 characters
    Then : Verify Minimum validation message of App Description textbox as App Description must be at least 2 characters.

  @chrome @demo
  Scenario: Verify Color of Minimum validation message for App Description field
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Click on App Description textbox and Enter App Description as value less than 2 characters
    Then : Verify that color of App Description textbox Minimum validation message should be red

  @chrome @demo
  Scenario: Verify Maximum validation message for App Description field
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Click on App Description textbox and Enter App Description as value more than 800 characters
    Then : Verify Maximum validation message of App Description textbox as App Description cannot be more than 800 characters.

  @chrome @demo
  Scenario: Verify Color of Maximum validation message for App Description field
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Click on App Description textbox and Enter App Description as value more than 800 characters
    Then : Verify that color of App Description textbox Maximum validation message should be red

  @chrome @demo
  Scenario: Verify no validation message for App Description field
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Enter Valid Value in App Description textbox
    Then : Verify that system should not display validation message when a user enter Valid App Description

  #46 #remove
  #This functionality is work different in current system, so need to remove this testcase
  @chrome
  Scenario: Verify that by selecting URL Radio button URL textbox appears

  @chrome
  Scenario: "Verify invalid Validation message for URL
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Select URL Radio button
    And : Click on the URL textbox
    And : Enter URL as "abc"
    Then : Verify that system display invalid validation for URL as "Not a valid URL, Don't forget to use http:// or https://"

  @chrome
  Scenario: Verify Color of Validation message for URL
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Select URL Radio button
    And : Click on the URL textbox
    And : Enter URL as "abc"
    Then : Verify that Color of Validation message color should be red

  @chrome
  Scenario: Verify No Validation message for Valid URL
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Select URL Radio button
    And : Click on the URL textbox
    And : Enter Valid URL into URL textbox
    Then : Verify that system should not display Validation message when a user enter valid URL

  @chrome
  Scenario: verify Remove validation Message for URL
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Select URL Radio button
    And : Click on the URL textbox
    And : Enter Valid URL into URL textbox
    And : Select Upload Document Radio button
    Then : Verify that system should display Remove Validation message as "Please remove the entered URL."

  @chrome
  Scenario: Verify that by selecting "Send Email Alert when Record result is a failure" checkbox Email textbox appears
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Select Send Email Alert when Record result is a failure checkbox
    Then : Verify that system should display Email Address textbox when a user select "Send Email Alert when Record result is a failure" checkbox

  @chrome
  Scenario: Verify that by Deselecting "Send Email Alert when Record result is a failure" checkbox Email textbox disappear.
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Select Send Email Alert when Record result is a failure checkbox
    And : Deselect Send Email Alert when Record result is a failure checkbox
    Then : Verify that system should not display Email textbox when a user deselect "Send Email Alert when Record result is a failure" checkbox`

  @chrome
  Scenario: Verify that by selecting "Send Email Alert when Record result is a failure" checkbox Email Information message appears
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Select Send Email Alert when Record result is a failure checkbox
    Then : Verify that system should display information message as "Separate multiple Email Addresses with a comma(,)." when a user select Send Email Alert when Record result is a failure checkbox

  @chrome
  Scenario: Verify color of Email Information message
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Select Send Email Alert when Record result is a failure checkbox
    Then : Verify that color of Email Information message should be blue

  @chrome
  Scenario: Verify Required message for Email Address
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Select Send Email Alert when Record result is a failure checkbox
    And : Click on the Email Address textbox
    And : Press "TAB" key
    Then : Verify that system should display required message for Email Address textbox as "Please enter at least one Email Address."

  @chrome
  Scenario: Verify Invalid validation message for Email Address
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Select Send Email Alert when Record result is a failure checkbox
    And : Click on the Email Address textbox
    And : Enter Email Address as "abc"
    Then : Verify that Invalid email Address validation message should display as "Invalid Email Address."

  @chrome
  Scenario: Verify No Validation message for Valid Email Address
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Select Send Email Alert when Record result is a failure checkbox
    And : Click on the Email Address textbox
    And : Enter Valid Email Address
    Then : Verify that system should not display validation message when a user enter valid email address

  @chrome
  Scenario: Verify Information message of Site Selection YES option
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    Then : Verify that Information message for Site selection should be display as "Please select at least one option from each site level." when Yes option is selected

  @chrome
  Scenario: Verify Color of Information message for Site Selection option YES
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    Then : Verify that Color of Information message should be blue for Yes option of Site Selection

  @chrome
  Scenario: Verify Information messages of Site Selection option NO
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Select No option form Does this app require site selections? dropdown
    Then : Verify that system should display Information Message for NO option as "Please select default option from each site level. They will be stored in the database for each app."

  @chrome
  Scenario: Verify Color of Information message for Site Selection option No
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Select No option form Does this app require site selections? dropdown
    Then : Verify that Color of Information message should be blue for No option of Site Selection

  @chrome
  Scenario: Verify Required Validation Message for Site Selection
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Select No option form Does this app require site selections? dropdown
    Then : Verify that Required validation Message should be display as "At least one site is required from each level." when a user select No option

  @chrome
  Scenario: Verify Color of Required Validation Message for Site Selection
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Select No option form Does this app require site selections? dropdown
    Then : Verify that Color of Required validation message should be red for No option of Site Selection

  @chrome
  Scenario: Verify Minimum validation of Header Message for Print textbox
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Click on Header Message for Print textbox
    And : Enter Header Message for Print as "T"
    Then : Verify that system should display Minimum validation of Header Message for Print as "Header Message for Print must be at least 2 characters."

  @chrome
  Scenario: Verify Color of Minimum validation of Header Message for Print textbox
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Click on Header Message for Print textbox
    And : Enter Header Message for Print as "T"
    Then : Verify that Color of Minimum validation message should be red for Header Message for Print textbox

  @chrome
  Scenario: Verify Maximum validation of "Header Message for Print" textbox
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Click on Header Message for Print textbox
    And : Enter Header Message for Print value more than 200 characters
    Then : Verify that system should display Maximum validation message as "Header Message for Print cannot be more than 200 characters."

  @chrome
  Scenario: Verify Color of Maximum validation of "Header Message for Print" textbox
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Click on Header Message for Print textbox
    And : Enter Header Message for Print value more than 200 characters
    Then : Verify that Color of Maximum validation message should be red for Header Message for Print textbox

  @chrome
  Scenario: Verify No validation message for "Header Message for Print" textbox
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Click on Header Message for Print textbox
    And : Enter Valid Value in Header Message for Print textbox
    Then : Verify that system should not display any validation message when a user enter Valid Value into Header Message for Print textbox

  @chrome
  Scenario: Verify Minimum validation of "Header Message for Print When App Status is Passed" textbox
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Click on Header Message for Print When App Status is Passed textbox
    And : Enter Header Message for Print When App Status is Passed as "T"
    Then : Verify that system should display Minimum validation of Header Message for Print When App Status is Passed as "Header Message for Print When App Status is Passed must be at least 2 characters."

  @chrome
  Scenario: verify Color of Minimum validation of "Header Message for Print When App Status is Passed" textbox
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Click on Header Message for Print When App Status is Passed textbox
    And : Enter Header Message for Print When App Status is Passed as "T"
    Then : Verify that Color of Minimum validation message should be red for Header Message for Print When App Status is Passed textbox

  @chrome
  Scenario: Verify Maximum validation of "Header Message for Print When App Status is Passed" textbox
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Click on Header Message for Print When App Status is Passed textbox
    And : Enter Header Message for Print When App Status is Passed value more than 200 characters
    Then : Verify that system should display Maximum validation message as "Header Message for Print When App Status is Passed cannot be more than 200 characters."

  @chrome
  Scenario: verify Color of Maximum validation of "Header Message for Print When App Status is Passed" textbox
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Click on Header Message for Print When App Status is Passed textbox
    And : Enter Header Message for Print When App Status is Passed value more than 200 characters
    Then : Verify that Color of Maximum validation message should be red for Header Message for Print When App Status is Passed textbox

  @chrome
  Scenario: Verify No validation message for "Header Message for Print When App Status is Passed" textbox
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Click on Header Message for Print When App Status is Passed textbox
    And : Enter Valid Value in Header Message for Print When App Status is Passed textbox
    Then : Verify that system should not display any validation message when a user enter Valid Value into Header Message for Print When App Status is Passed textbox

  @chrome
  Scenario: Verify Minimum validation of "Header Message for Print When App Status is Failed" textbox
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Click on Header Message for Print When App Status is Failed textbox
    And : Enter Header Message for Print When App Status is Failed as "T"
    Then : Verify that system should display Minimum validation of Header Message for Print When App Status is Failed as "Header Message for Print when App Status is Failed must be at least 2 characters."

  @chrome
  Scenario: verify Color of Minimum validation of "Header Message for Print When App Status is Failed" textbox
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Click on Header Message for Print When App Status is Failed textbox
    And : Enter Header Message for Print When App Status is Failed as "T"
    Then : Verify that Color of Minimum validation message should be red for Header Message for Print When App Status is Failed textbox

  @chrome
  Scenario: Verify Maximum validation of "Header Message for Print When App Status is Failed" textbox
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Click on Header Message for Print When App Status is Failed textbox
    And : Enter Header Message for Print When App Status is Failed value more than 200 characters
    Then : Verify that system should display Maximum validation message as "Header Message for Print When App Status is Failed cannot be more than 200 characters."

  @chrome
  Scenario: verify Color of Maximum validation of "Header Message for Print When App Status is Failed" textbox
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Click on Header Message for Print When App Status is Failed textbox
    And : Enter Header Message for Print When App Status is Failed value more than 200 characters
    Then : Verify that Color of Maximum validation message should be red for Header Message for Print When App Status is Failed textbox

  @chrome
  Scenario: Verify No validation message for "Header Message for Print When App Status is Failed" textbox
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Click on Header Message for Print When App Status is Failed textbox
    And : Enter Valid Value in Header Message for Print When App Status is Failed textbox
    Then : Verify that system should not display any validation message when a user enter Valid Value into Header Message for Print When App Status is Failed textbox

  @chrome
  Scenario: Verify that by selecting "Peer or Second Review" checkbox Select Role dropdown is displayed
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Select Peer or Second Review checkbox
    Then : Verify that system should display Select Role dropdown when a user select Peer or Second Review checkbox

  @chrome
  Scenario: Verify that by unselecting "Peer or Second Review" checkbox Select Role dropdown is disappear
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Select Peer or Second Review checkbox
    And : Deselect Peer or Second Review checkbox
    Then : Verify that system should not display Select Role dropdown when a user unselect the Peer or Second Review checkbox

  @chrome
  Scenario: Verify App Details in Basic Tab to Proceed in Attribute tab
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Add All Required App Details in Basic Tab
    Then : Verify that system should redirect to the Attribute tab when a user Adds All required details into Basic Tab

  @chrome
  Scenario: Verify '2  ATTRIBUTES' text in Attribute tab
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Add All Required App Details in Basic Tab
    And : Click on Continue button
    Then : Verify that '2 ATTRIBUTES' text should be display in Attribute Tab

  @chrome
  Scenario: Verify Color of Attribute tab
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Add All Required App Details in Basic Tab
    And : Click on Continue button
    Then : Verify that Color of Attribute tab should be green

  @chrome
  Scenario: Verify Attribute Details label in Attribute tab
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Add All Required App Details in Basic Tab
    And : Click on Continue button
    Then : Verify that Attribute Details label should be displayed in top left side of Attribute Tab

  @chrome
  Scenario: Verify Current Version of App in Attribute Tab
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Add All Required App Details in Basic Tab
    And : Click on Continue button
    Then : Verify that a system should display the Current Version of the App in top right side of Attribute Tab

  @chrome
  Scenario: Verify Current Version Message color in Attribute tab
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Add All Required App Details in Basic Tab
    And : Click on Continue button
    Then : Verify that Color of Current Version message should be blue in Attribute tab

  @chrome
  Scenario: Verify other tabs are Disabled when User access Attribute Tab
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Add All Required App Details in Basic Tab
    And : Click on Continue button
    Then : Verify that all next tabs should displayed in Disable mode when a user access Attribute tab

  @chrome
  Scenario: Verify Information Message for Attribute tab
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Add All Required App Details in Basic Tab
    And : Click on Continue button
    Then : Verify that system should display information message in Attribute tab as "Define Attributes for App. Minimum 2 Attributes and/or Entities are required."

  @chrome
  Scenario: Verify Color of Information Message in Attribute tab
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Add All Required App Details in Basic Tab
    And : Click on Continue button
    Then : Verify that Color of Information message in Attribute tab should be blue

  @chrome
  Scenario: Verify Add Attribute button is not displayed initially when the page is loaded
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Add All Required App Details in Basic Tab
    And : Click on Continue button
    Then : Verify that Add Attribute button should not displayed initially to the user when Attribute tab is loaded

  @chrome
  Scenario: Verify that Add Attribute button is displayed only when Attribute is selected in Dropdown
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Add All Required App Details in Basic Tab
    And : Click on Continue button
    And : Select Attribute from Select Attribute dropdown
    Then : Verify that system should display Add Attribute button only when a user select any Attribute from dropdown

  @chrome
  Scenario: Verify that Add Attribute is clickable
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Add All Required App Details in Basic Tab
    And : Click on Continue button
    And : Select Attribute from Select Attribute dropdown
    And : Click on Add Attribute button
    Then : Verify that Add Attribute button is clickable or not

  @chrome
  Scenario: Verify Color of  Add Attribute button
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Add All Required App Details in Basic Tab
    And : Click on Continue button
    And : Select Attribute from Select Attribute dropdown
    Then : Verify that Color of Add Attribute button should be blue

  @chrome
  Scenario: Verify that Remove button functionality
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Add All Required App Details in Basic Tab
    And : Click on Continue button
    And : Select Attribute from Select Attribute dropdown
    And : Click on Add Attribute button
    And : Click on Remove button
    Then : Verify that Selected Attribute should be removed when a user click on the Remove button

  @chrome
  Scenario: Verify Attribute Details in Attribute Tab to Proceed in Workflow tab
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Add All Required App Details in Basic Tab
    And : Click on Continue button
    And : Add All Required Attribute Details in Attribute tab
    And : Click on Continue button
    Then : Verify that system should redirect to the Workflow tab when a user Adds All required details into Attribute Tab

  @chrome
  Scenario: Verify text in '3  WORKFLOW' Workflow tab
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Add All Required App Details in Basic Tab
    And : Click on Continue button
    And : Add All Required Attribute Details in Attribute tab
    And : Click on Continue button
    Then : Verify that '3  WORKFLOW' text should be display in Workflow tab

  @chrome
  Scenario: Verify Color of Workflow tab
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Add All Required App Details in Basic Tab
    And : Click on Continue button
    And : Add All Required Attribute Details in Attribute tab
    And : Click on Continue button
    Then : Verify that Color of Workflow tab should be green

  @chrome
  Scenario: Verify Workflow Details label in Workflow tab
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Add All Required App Details in Basic Tab
    And : Click on Continue button
    And : Add All Required Attribute Details in Attribute tab
    And : Click on Continue button
    Then : Verify that Workflow Details label should be displayed in top left side of Workflow Tab

  @chrome
  Scenario: Verify Current Version of App in Workflow Tab
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Add All Required App Details in Basic Tab
    And : Click on Continue button
    And : Add All Required Attribute Details in Attribute tab
    And : Click on Continue button
    Then : Verify that a system should display the Current Version of the App in top right side of Workflow Tab

  @chrome
  Scenario: Verify Current Version Message color in Workflow tab
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Add All Required App Details in Basic Tab
    And : Click on Continue button
    And : Add All Required Attribute Details in Attribute tab
    And : Click on Continue button
    Then : Verify that Color of Current Version message should be blue in Workflow tab

  @chrome
  Scenario: Verify other tabs are disabled when User access Workflow Tab
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Add All Required App Details in Basic Tab
    And : Click on Continue button
    And : Add All Required Attribute Details in Attribute tab
    And : Click on Continue button
    Then : Verify that all next tabs should displayed in Disable mode when a user access Attribute tab

  @chrome
  Scenario: Verify Entity Workflow label in Workflow tab
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Add All Required App Details in Basic Tab
    And : Click on Continue button
    And : Add All Required Attribute Details in Attribute tab
    And : Click on Continue button
    Then : Verify that system should display Entity Workflow label below Workflow Details label

  @chrome
  Scenario: Verify Procedure Workflow label in Workflow tab
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Add All Required App Details in Basic Tab
    And : Click on Continue button
    And : Add All Required Attribute Details in Attribute tab
    And : Click on Continue button
    Then : Verify that system should display Procedure Workflow label below Entity Workflow section

  @chrome
  Scenario: Verify that Add Entity button is not displayed initially when the page is loaded
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Add All Required App Details in Basic Tab
    And : Click on Continue button
    And : Add All Required Attribute Details in Attribute tab
    And : Click on Continue button
    Then : Verify that system should not display Add Entity button initially when the page is loaded

  @chrome
  Scenario: Verify that Add Entity button is displayed when Entity name is typed in Search and Select Entity textbox
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Add All Required App Details in Basic Tab
    And : Click on Continue button
    And : Add All Required Attribute Details in Attribute tab
    And : Click on Continue button
    And : Type Entity name in Search and Select Entity dropdown
    Then : Verify that system should display Add Entity button when user Type Valid Entity name in Search and Select Entity dropdown

  @chrome
  Scenario: Verify that Add Entity button is clickable
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Add All Required App Details in Basic Tab
    And : Click on Continue button
    And : Add All Required Attribute Details in Attribute tab
    And : Click on Continue button
    And : Type Entity name in Search and Select Entity dropdown
    And : Click on Add Entity button
    Then : Verify that Add Entity button is clickable or not

  @chrome
  Scenario: Verify Color of Add Entity button
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Add All Required App Details in Basic Tab
    And : Click on Continue button
    And : Add All Required Attribute Details in Attribute tab
    And : Click on Continue button
    And : Type Entity name in Search and Select Entity dropdown
    Then : Verify that Color of Add Entity button should be soft cyan

  @chrome
  Scenario: Verify Filter button redirection
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Add All Required App Details in Basic Tab
    And : Click on Continue button
    And : Add All Required Attribute Details in Attribute tab
    And : Click on Continue button
    And : Type Entity name in Search and Select Entity dropdown
    And : Click on Add Entity button
    And : Click on Filter button
    Then : Verify that system should redirect to the Apply Entity Filter section when a user click on the Filter button

  @chrome
  Scenario: Verify Add Rule button redirection for Entity
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Add All Required App Details in Basic Tab
    And : Click on Continue button
    And : Add All Required Attribute Details in Attribute tab
    And : Click on the Continue button
    And : Type Entity name in Search and Select Entity dropdown
    And : Click on Add Entity button
    And : Click on Add Rule button
    Then : Verify that system should redirect to the Define Rule section when a user click on the Add Rule button

  @chrome
  Scenario: Verify Show/Hide details link is clickable
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Add All Required App Details in Basic Tab
    And : Click on Continue button
    And : Add All Required Attribute Details in Attribute tab
    And : Click on Continue button
    And : Type Entity name in Search and Select Entity dropdown
    And : Click on Add Entity button
    And : Click on Show link
    And : Click on Hide link
    Then : Verify that Show/Hide link is clickable or not

  @chrome
  Scenario: Verify Show details link functionality
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Add All Required App Details in Basic Tab
    And : Click on Continue button
    And : Add All Required Attribute Details in Attribute tab
    And : Click on Continue button
    And : Type Entity name in Search and Select Entity dropdown
    And : Click on Add Entity button
    And : Click on Show link
    Then : Verify that system should expand the Entity Details when a user click on the Show link

  @chrome
  Scenario: Verify Hide details link functionality
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Add All Required App Details in Basic Tab
    And : Click on Continue button
    And : Add All Required Attribute Details in Attribute tab
    And : Click on Continue button
    And : Type Entity name in Search and Select Entity dropdown
    And : Click on Add Entity button
    And : Click on Hide link
    Then : Verify that system should collapse the Entity Details when a user click on the Hide link

  @chrome
  Scenario: Verify that Add Procedure button is not displayed initially when the page is loaded
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Add All Required App Details in Basic Tab
    And : Click on Continue button
    And : Add All Required Attribute Details in Attribute tab
    And : Click on the Continue button
    Then : Verify that Add Procedure button should not displayed initially when the page is loaded

  @chrome
  Scenario: Verify that Add Procedure button is displayed when Procedure name is typed in Search and Select Procedure textbox
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Add All Required App Details in Basic Tab
    And : Click on Continue button
    And : Add All Required Attribute Details in Attribute tab
    And : Click on Continue button
    And : Type Procedure name in Search and Select Procedure dropdown
    And : Click on the Add Procedure button
    Then : Verify that system should display Add Procedure button when user Type Valid Procedure name in Search and Select Procedure dropdown

  @chrome
  Scenario: Verify that Add Procedure button is clickable
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Add All Required App Details in Basic Tab
    And : Click on Continue button
    And : Add All Required Attribute Details in Attribute tab
    And : Click on Continue button
    And : Type Procedure name in Search and Select Procedure dropdown
    And : Click on Add Procedure button
    Then : Verify that Add Procedure button is clickable or not

  @chrome
  Scenario: Verify Color of Add Procedure button
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Add All Required App Details in Basic Tab
    And : Click on Continue button
    And : Add All Required Attribute Details in Attribute tab
    And : Click on Continue button
    And : Type Procedure name in Search and Select Procedure dropdown
    Then : Verify that Color of Add Procedure button should be soft cyan

  @chrome
  Scenario: Verify Add Rule button redirection for Procedure
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Add All Required App Details in Basic Tab
    And : Click on Continue button
    And : Add All Required Attribute Details in Attribute tab
    And : Click on Continue button
    And : Type Procedure name in Search and Select Procedure dropdown
    And : Click on Add Procedure button
    And : Click on Add Rule button
    Then : Verify that system should redirect to the Define Rule section when a user click on the Add Rule button

  @chrome
  Scenario: Verify Validation Message for Workflow tab
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Add All Required App Details in Basic Tab
    And : Click on Continue button
    And : Add All Required Attribute Details in Attribute tab
    And : Click on Continue button
    And : Click on Continue button
    Then : Verify that system should display validation message as "Please add at least 2 Attributes and/or Entities."

  @chrome
  Scenario: Verify Workflow Details in Workflow Tab to Proceed in Acceptance tab
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Add All Required App Details in Basic Tab
    And : Click on Continue button
    And : Add All Required Attribute Details in Attribute tab
    And : Click on Continue button
    And : Add all Required Workflow Details in Workflow tab
    And : Click on Continue button
    Then : Verify that system should redirect to the Acceptance tab when a user Adds All required details into Workflow Tab

  @chrome
  Scenario: Verify text in '3   ACCEPTANCE' Acceptance tab
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Add All Required App Details in Basic Tab
    And : Click on Continue button
    And : Add All Required Attribute Details in Attribute tab
    And : Click on Continue button
    And : Add all Required Workflow Details in Workflow tab
    And : Click on Continue button
    Then : Verify that '3   ACCEPTANCE' text should be display in Acceptance tab

  @chrome
  Scenario: Verify Color of Acceptance tab
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Add All Required App Details in Basic Tab
    And : Click on Continue button
    And : Add All Required Attribute Details in Attribute tab
    And : Click on Continue button
    And : Add all Required Workflow Details in Workflow tab
    And : Click on Continue button
    Then : Verify that Color of Acceptance tab should be green

  @chrome
  Scenario: Verify  App Acceptance Criteria label in Acceptance tab
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Add All Required App Details in Basic Tab
    And : Click on Continue button
    And : Add All Required Attribute Details in Attribute tab
    And : Click on Continue button
    And : Add all Required Workflow Details in Workflow tab
    And : Click on Continue button
    Then : Verify that App Acceptance Criteria label should be displayed in top left side of Acceptance Tab

  @chrome
  Scenario: Verify Current Version of App in Acceptance Tab
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Add All Required App Details in Basic Tab
    And : Click on Continue button
    And : Add All Required Attribute Details in Attribute tab
    And : Click on Continue button
    And : Add all Required Workflow Details in Workflow tab
    And : Click on Continue button
    Then : Verify that a system should display the Current Version of the App in top right side of Acceptance Tab

  @chrome
  Scenario: Verify Current Version Message color in Acceptance tab
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Add All Required App Details in Basic Tab
    And : Click on Continue button
    And : Add All Required Attribute Details in Attribute tab
    And : Click on Continue button
    And : Add all Required Workflow Details in Workflow tab
    And : Click on Continue button
    Then : Verify that Color of Current Version message should be blue in Acceptance tab

  @chrome
  Scenario: Verify other tabs are disabled when User access Acceptance Tab
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Add All Required App Details in Basic Tab
    And : Click on Continue button
    And : Add All Required Attribute Details in Attribute tab
    And : Click on Continue button
    And : Add all Required Workflow Details in Workflow tab
    And : Click on Continue button
    Then : Verify that all next tabs should displayed in Disable mode when a user access Acceptance tab

  @chrome
  Scenario: Verify How do you want to track Unique Records?? label in Acceptance tab
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Add All Required App Details in Basic Tab
    And : Click on Continue button
    And : Add All Required Attribute Details in Attribute tab
    And : Click on Continue button
    And : Add all Required Workflow Details in Workflow tab
    And : Click on Continue button
    Then : Verify that "How do you want to track Unique Records??" label should be display below App Acceptance Criteria label

  @chrome
  Scenario: Verify Information Message for Acceptance tab
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Add All Required App Details in Basic Tab
    And : Click on Continue button
    And : Add All Required Attribute Details in Attribute tab
    And : Click on Continue button
    And : Add all Required Workflow Details in Workflow tab
    And : Click on Continue button
    Then : Verify that system should display information message as "Combination of selected 'Key Attributes' will be checked against any matching entry."

  @chrome
  Scenario: Verify Color of Information Message for Acceptance tab
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Add All Required App Details in Basic Tab
    And : Click on Continue button
    And : Add All Required Attribute Details in Attribute tab
    And : Click on Continue button
    And : Add all Required Workflow Details in Workflow tab
    And : Click on Continue button
    Then : Verify that Color of Acceptance Attribute information message should be blue

  @chrome
  Scenario: Verify "Define Pass Criteria(s)on App" label in Acceptance tab
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Add All Required App Details in Basic Tab
    And : Click on Continue button
    And : Add All Required Attribute Details in Attribute tab
    And : Click on Continue button
    And : Add all Required Workflow Details in Workflow tab
    And : Click on Continue button
    Then : Verify that system should display Define Pass Criteria(s) on App label below How do you want to track Unique Records? section

  @chrome
  Scenario: Verify Information Message to select Procedure of Acceptance tab
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Add All Required App Details in Basic Tab
    And : Click on Continue button
    And : Add All Required Attribute Details in Attribute tab
    And : Click on Continue button
    And : Add all Required Workflow Details in Workflow tab
    And : Click on Continue button
    Then : Verify that system should display Procedure information message as "Add Procedure(s) that will allow the App to Pass."

  @chrome
  Scenario: Verify  Color of Information Message for Procedure in Acceptance tab
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Add All Required App Details in Basic Tab
    And : Click on Continue button
    And : Add All Required Attribute Details in Attribute tab
    And : Click on Continue button
    And : Add all Required Workflow Details in Workflow tab
    And : Click on Continue button
    Then : Verify that Color of Procedure information message should be blue

  @chrome
  Scenario: Verify that Add Procedure button is displayed only when Procedure is selected in Dropdown
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Add All Required App Details in Basic Tab
    And : Click on Continue button
    And : Add All Required Attribute Details in Attribute tab
    And : Click on Continue button
    And : Add all Required Workflow Details in Workflow tab
    And : Click on Continue button
    And : Select Procedure from Select Procedure dropdown
    Then : Verify that system should display Add Procedure button when a user select Procedure from Select Procedure dropdown

  @chrome
  Scenario: Verify that Add Procedure button is clickable
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Add All Required App Details in Basic Tab
    And : Click on Continue button
    And : Add All Required Attribute Details in Attribute tab
    And : Click on Continue button
    And : Add all Required Workflow Details in Workflow tab
    And : Click on Continue button
    And : Select Procedure from Select Procedure dropdown
    And : Click on Add Procedure button
    Then : Verify that Add Procedure button is clickable or not

  @chrome
  Scenario: Verify Color of Add Procedure button
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Add All Required App Details in Basic Tab
    And : Click on Continue button
    And : Add All Required Attribute Details in Attribute tab
    And : Click on Continue button
    And : Add all Required Workflow Details in Workflow tab
    And : Click on Continue button
    And : Select Procedure from Select Procedure dropdown
    Then : Verify that Color of Add Procedure button should be blue

  @chrome
  Scenario: Verify that Remove button functionality
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Add All Required App Details in Basic Tab
    And : Click on Continue button
    And : Add All Required Attribute Details in Attribute tab
    And : Click on Continue button
    And : Add all Required Workflow Details in Workflow tab
    And : Click on Continue button
    And : Select Procedure from Select Procedure dropdown
    And : Click on Add Procedure button
    And : Click on Remove button
    Then : Verify that system should remove the Procedure when a user click on the Remove button

  @chrome
  Scenario: Verify App Acceptance Criteria in Acceptance tab to Proceed in Preview tab
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Add All Required App Details in Basic Tab
    And : Click on Continue button
    And : Add All Required Attribute Details in Attribute tab
    And : Click on Continue button
    And : Add all Required Workflow Details in Workflow tab
    And : Click on Continue button
    And : Add all required App Acceptance Criteria in Acceptance tab
    And : Click on Continue button
    Then : Verify that system should redirect to the Preview tab when a user Adds All required details into Acceptance Tab

  @chrome
  Scenario: Verify text in '6 PREVIEW' Preview tab
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Add All Required App Details in Basic Tab
    And : Click on Continue button
    And : Add All Required Attribute Details in Attribute tab
    And : Click on Continue button
    And : Add all Required Workflow Details in Workflow tab
    And : Click on Continue button
    And : Add all required App Acceptance Criteria in Acceptance tab
    And : Click on Continue button
    Then : Verify that system should display '6 PREVIEW' in Preview tab

  @chrome
  Scenario: Verify Color of Preview tab
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Add All Required App Details in Basic Tab
    And : Click on Continue button
    And : Add All Required Attribute Details in Attribute tab
    And : Click on Continue button
    And : Add all Required Workflow Details in Workflow tab
    And : Click on Continue button
    And : Add all required App Acceptance Criteria in Acceptance tab
    And : Click on Continue button
    Then : Verify that Color of Preview tab should be green

  @chrome
  Scenario: Verify other tabs are disabled when User access Preview Tab
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Add All Required App Details in Basic Tab
    And : Click on Continue button
    And : Add All Required Attribute Details in Attribute tab
    And : Click on Continue button
    And : Add all Required Workflow Details in Workflow tab
    And : Click on Continue button
    And : Add all required App Acceptance Criteria in Acceptance tab
    And : Click on Continue button
    Then : Verify that all next tabs should displayed in Disable mode when a user access Preview tab

  @chrome
  Scenario: Verify Information Message for Preview tab
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Add All Required App Details in Basic Tab
    And : Click on Continue button
    And : Add All Required Attribute Details in Attribute tab
    And : Click on Continue button
    And : Add all Required Workflow Details in Workflow tab
    And : Click on Continue button
    And : Add all required App Acceptance Criteria in Acceptance tab
    And : Click on Continue button
    Then : Verify that system should display Preview tab information message as "Preview the App before publishing."

  @chrome
  Scenario: Verify Color of Information message for Preview tab
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Add All Required App Details in Basic Tab
    And : Click on Continue button
    And : Add All Required Attribute Details in Attribute tab
    And : Click on Continue button
    And : Add all Required Workflow Details in Workflow tab
    And : Click on Continue button
    And : Add all required App Acceptance Criteria in Acceptance tab
    And : Click on Continue button
    Then : Verify that Color of information message for Preview tab should be blue

  @chrome
  Scenario: Verify Current Version of App in Preview Tab
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Add All Required App Details in Basic Tab
    And : Click on Continue button
    And : Add All Required Attribute Details in Attribute tab
    And : Click on Continue button
    And : Add all Required Workflow Details in Workflow tab
    And : Click on Continue button
    And : Add all required App Acceptance Criteria in Acceptance tab
    And : Click on Continue button
    Then : Verify that system should display the Current Version of the App in top right side of Preview Tab

  @chrome
  Scenario: Verify Current App name in Preview tab
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Add All Required App Details in Basic Tab
    And : Click on Continue button
    And : Add All Required Attribute Details in Attribute tab
    And : Click on Continue button
    And : Add all Required Workflow Details in Workflow tab
    And : Click on Continue button
    And : Add all required App Acceptance Criteria in Acceptance tab
    And : Click on Continue button
    Then : Verify that system should display Correct App name label in Preview tab

  @chrome
  Scenario: Verify Preview App in Preview tab to Proceed in Permissions tab
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Add All Required App Details in Basic Tab
    And : Click on Continue button
    And : Add All Required Attribute Details in Attribute tab
    And : Click on Continue button
    And : Add all Required Workflow Details in Workflow tab
    And : Click on Continue button
    And : Add all required App Acceptance Criteria in Acceptance tab
    And : Click on Continue button
    And : Click on Publish & Continue button
    Then : Verify that system should redirect to the Permissions tab when a user click on Publish & Continue button

  @chrome
  Scenario: Verify text in '6 Permission' in Permission tab
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Add All Required App Details in Basic Tab
    And : Click on Continue button
    And : Add All Required Attribute Details in Attribute tab
    And : Click on Continue button
    And : Add all Required Workflow Details in Workflow tab
    And : Click on Continue button
    And : Add all required App Acceptance Criteria in Acceptance tab
    And : Click on Continue button
    And : Click on Publish & Continue button
    Then : Verify that system should display '6  Permissions' text in Permissions tab

  @chrome
  Scenario: Verify Color of Permission tab
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Add All Required App Details in Basic Tab
    And : Click on Continue button
    And : Add All Required Attribute Details in Attribute tab
    And : Click on Continue button
    And : Add all Required Workflow Details in Workflow tab
    And : Click on Continue button
    And : Add all required App Acceptance Criteria in Acceptance tab
    And : Click on Continue button
    And : Click on Publish & Continue button
    Then : Verify that Color of Permissions tab should be green

  @chrome
  Scenario: Verify App Name in Permission Tab
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Add All Required App Details in Basic Tab
    And : Click on Continue button
    And : Add All Required Attribute Details in Attribute tab
    And : Click on Continue button
    And : Add all Required Workflow Details in Workflow tab
    And : Click on Continue button
    And : Add all required App Acceptance Criteria in Acceptance tab
    And : Click on Continue button
    And : Click on Publish & Continue button
    Then : Verify that system should display Correct App Name label in Permissions tab

  @chrome
  Scenario: Verify Information message 'Define Permissions for App.' in Permission tab
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Add All Required App Details in Basic Tab
    And : Click on Continue button
    And : Add All Required Attribute Details in Attribute tab
    And : Click on Continue button
    And : Add all Required Workflow Details in Workflow tab
    And : Click on Continue button
    And : Add all required App Acceptance Criteria in Acceptance tab
    And : Click on Continue button
    And : Click on Publish & Continue button
    Then : Verify that system should display information as "Define Permissions for App." in Permissions tab

  @chrome
  Scenario: Verify Information Message color in Permission tab
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Add All Required App Details in Basic Tab
    And : Click on Continue button
    And : Add All Required Attribute Details in Attribute tab
    And : Click on Continue button
    And : Add all Required Workflow Details in Workflow tab
    And : Click on Continue button
    And : Add all required App Acceptance Criteria in Acceptance tab
    And : Click on Continue button
    And : Click on Publish & Continue button
    Then : Verify that Color of information message in Permissions tab should be blue

  @chrome
  Scenario: Verify Color of Save Button
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Add All Required App Details in Basic Tab
    And : Click on Continue button
    And : Add All Required Attribute Details in Attribute tab
    And : Click on Continue button
    And : Add all Required Workflow Details in Workflow tab
    And : Click on Continue button
    And : Add all required App Acceptance Criteria in Acceptance tab
    And : Click on Continue button
    And : Click on Publish & Continue button
    Then : Verify that Color of Save button should be green

  @chrome
  Scenario: Verify Save Button Click
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Add All Required App Details in Basic Tab
    And : Click on Continue button
    And : Add All Required Attribute Details in Attribute tab
    And : Click on Continue button
    And : Add all Required Workflow Details in Workflow tab
    And : Click on Continue button
    And : Add all required App Acceptance Criteria in Acceptance tab
    And : Click on Continue button
    And : Click on Publish & Continue button
    And : Click on Save button
    Then : Verify that Save button is clickable or not

  @chrome
  Scenario: Verify Save Button Redirection
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Add All Required App Details in Basic Tab
    And : Click on Continue button
    And : Add All Required Attribute Details in Attribute tab
    And : Click on Continue button
    And : Add all Required Workflow Details in Workflow tab
    And : Click on Continue button
    And : Add all required App Acceptance Criteria in Acceptance tab
    And : Click on Continue button
    And : Click on Publish & Continue button
    And : Click on Save button
    Then : Verify that system should redirect to the Apps Listing Screen when a user click on the Save button

  @chrome
  Scenario: Verify Add New App functionality
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Add All Required App Details in Basic Tab
    And : Click on Continue button
    And : Add All Required Attribute Details in Attribute tab
    And : Click on Continue button
    And : Add all Required Workflow Details in Workflow tab
    And : Click on Continue button
    And : Add all required App Acceptance Criteria in Acceptance tab
    And : Click on Continue button
    And : Click on Publish & Continue button
    And : Click on Save button
    Then : Verify that user is allowed to Add New app
