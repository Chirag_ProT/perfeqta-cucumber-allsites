Feature: Info: Executed script on "test1.beperfeqta.com/v32050" link. 
	Module Name: Questions


@chrome 
Scenario: Verify the module title when user click on "Questions" module 
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
	And : Click on Administration Tile 
	And : Click on Questions Tile 
	Then : Verify the module name as Questions 
	
@chrome  
Scenario: Verify the "Filter By" functionality of Questions module listing screen 
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
	And : Click on Administration Tile 
	And : Click on Questions Tile 
	And : Click on Filter By dropdown 
	Then : Verify the Filter By dropdown should be clickable 
	
@chrome  
Scenario: Verify the "Question Type" functionality of Questions module listing screen 
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
	And : Click on Administration Tile 
	And : Click on Questions Tile 
	And : Click on Filter By dropdown and select Question Type option 
	And : Click on Question Type dropdown 
	Then : Verify the Question Type dropdown should be clickable 
	
@chrome  
Scenario: Verify pagination of Questions listing page when user click on "Last" 
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
	And : Click on Administration Tile 
	And : Click on Questions Tile 
	And : Click on the Last button of Pagination 
	Then : Verify that the system should display the last page of the listing screen 
	
@chrome  
Scenario: Verify pagination of Questions listing page when user click on "First" 
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
	And : Click on Administration Tile 
	And : Click on Questions Tile 
	And : Click on the Last button of Pagination 
	And : Click on the First button of Pagination 
	Then : Verify that the system should display the first page of the listing screen 
	
@chrome  
Scenario: Verify pagination of Questions listing page when user click on "Next" 
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
	And : Click on Administration Tile 
	And : Click on Questions Tile 
	And : Click on Next button of Pagination 
	Then : Verify that the system should display the second page of the listing screen 
	
@chrome  
Scenario: Verify pagination of Questions listing page when user click on "Previous" 
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
	And : Click on Administration Tile 
	And : Click on Questions Tile 
	And : Click on Next button of Pagination 
	And : Click on the Previous button of Pagination 
	Then : Verify that the system should display the first page of the listing screen 
	
@chrome @ignore
Scenario: Verify the record count of Questions listing screen 
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
	And : Click on Administration Tile 
	And : Click on Questions Tile 
	Then : Verify the record count 
	
@chrome  @ignore
Scenario: Verify the Ascending order for the Questions column under Questions listing page 
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
	And : Click on Administration Tile 
	And : Click on Questions Tile 
	And : Click on Sorting icon of the Questions column
	Then : Verify that all the records of Questions column display in ascending order when click on sorting icon 
	
	#not implement
@chrome  @ignore
Scenario: Verify the Descending order for the Questions column under Questions listing page 
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
	And : Click on Administration Tile 
	And : Click on Questions Tile 
	And : Click on Sorting icon of the Questions column 
	Then : Verify that all the records of Questions column display in descending order 
	
@chrome @ignore 
Scenario: Verify the Search functionality of Questions module listing screen 
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
	And : Click on Administration Tile 
	And : Click on Questions Tile 
	And : Enter the data into search box, which user want to search 
	Then : Verify the search results of Questions listing page 
	
@chrome  
Scenario: Verify Edit Questions Breadcrumb functionality 
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
	And : Click on Administration Tile 
	And : Click on Questions Tile 
	And : Click on the first record of Question listing grid 
	Then : Verify the Breadcrumb of Edit Question as "Home > Administration > Questions > Add / Edit Question" 
	
@chrome  
Scenario: Verify the "Save" button functionality 
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
	And : Click on Administration Tile 
	And : Click on Questions Tile 
	And : Click on the first record of Question listing grid
	And : Click on Save button of Questions screen
	Then : Verify the Save button should be clickable 
	
@chrome  
Scenario: Verify the "Cancel" button functionality 
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
	And : Click on Administration Tile 
	And : Click on Questions Tile 
	And : Click on the first record of Question listing grid
	And : Click on Cancel button of Questions screen
	Then : Verify the Cancel button should be clickable and redirect to Questions listing grid
	
@chrome  
Scenario: Verify the Color of Save button of Questions module 
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
	And : Click on Administration Tile 
	And : Click on Questions Tile 
	And : Click on the first record of Question listing grid 
	Then : Verify the Color of Save button 
	
@chrome  
Scenario: Verify the Color of Cancel button of Questions module 
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
	And : Click on Administration Tile 
	And : Click on Questions Tile 
	And : Click on the first record of Question listing grid 
	Then : Verify the Color of Cancel button 
	
@chrome  
Scenario: Verify the Copy pop-up of Questions module 
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
	And : Click on Administration Tile 
	And : Click on Questions Tile 
	And : Click on the Create Copy Icon of first record of Question listing grid 
	Then : Verify the Label of Copy pop-up should be as "Questions Name" 
	
		
@chrome  
Scenario: Verify the Default Value of Copy pop-up while Creating Copy 
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
	And : Click on Administration Tile 
	And : Click on Questions Tile 
	And : Click on the Create Copy Icon of first record of Question listing grid 
	Then : Verify the Default Value of Copy pop-up should be appended with "- Copy" 
	
@chrome  
Scenario: Verify the Copy functionality of Questions module 
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
	And : Click on Administration Tile 
	And : Click on Questions Tile 
	And : Click on the Create Copy Icon of first record of Question listing grid 
	And : Click on OK button of the Copy popup 
	Then : Verify that the system should Create Copy of Question 
	
	
@chrome   
Scenario: Verify Mandatory validation message for the Question name field of Questions module 
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
	And : Click on Administration Tile 
	And : Click on Questions Tile 
	And : Click on "Add New" button of Questions listing screen 
	And : Click on Question Name textbox and press the "TAB" Key 
	Then : Verify the validation message as Question Name is required. 
	
@chrome   
Scenario: Verify Minimum validation message of Question Name Textbox 
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
	And : Click on Administration Tile 
	And : Click on Questions Tile 
	And : Click on "Add New" button of Questions listing screen 
	And : Enter Question Name value less than 2 chracters 
	Then : Verify the validation message as Question Name must be at least 2 characters. 
	
@chrome  
Scenario: Verify Maximum validation message of Question Name Textbox 
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
	And : Click on Administration Tile 
	And : Click on Questions Tile 
	And : Click on "Add New" button of Questions listing screen 
	And : Enter Question Name value more than 200 characters 
	Then : Verify validation message Question Name cannot be more than 200 characters. 
	
@chrome @demo 
Scenario: Verify Question Name text box with valid data 
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
	And : Click on Administration Tile 
	And : Click on Questions Tile 
	And : Click on "Add New" button of Questions listing screen 
	And : Enter a valid Question Name 
	Then : Verify system should accept the Question name value without any validation message 
	
@chrome  
Scenario: Verify Unique validation message of Question Name Textbox 
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
	And : Click on Administration Tile 
	And : Click on Questions Tile 
	And : Click on "Add New" button of Questions listing screen 
	And : Enter duplicate Question Name 
	Then : Verify the Unique validation message for the Question Name Textbox as Question Name must be unique.
	
@chrome  
Scenario: Verify Mandatory validation message of Question Tag Textbox 
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
	And : Click on Administration Tile 
	And : Click on Questions Tile 
	And : Click on "Add New" button of Questions listing screen 
	And : Click on the Question Tag text box and press the "TAB" Key 
	Then : Verify validation message as Question Tag is required. 
	
@chrome   
Scenario: Verify Minimum validation message of Question Tag Textbox 
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
	And : Click on Administration Tile 
	And : Click on Questions Tile 
	And : Click on "Add New" button of Questions listing screen 
	And : Enter Question Tag value less than 2 characters	
	Then : Verify validation message as Question Tag must be at least 2 characters. 
	
@chrome  
Scenario: Verify Maximum validation message of Question Tag Textbox 
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
	And : Click on Administration Tile 
	And : Click on Questions Tile 
	And : Click on "Add New" button of Questions listing screen 
	And : Enter Question Tag value more than 20 characters 
	Then : Verify validation message as Question Tag cannot be more than 20 characters. 
	
@chrome  
Scenario: Verify Unique validation message of Question Tag Textbox 
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
	And : Click on Administration Tile 
	And : Click on Questions Tile 
	And : Click on "Add New" button of Questions listing screen 
	And : Enter the duplicate value into the Question Tag text box 
	Then : Verify message as Question Tag already exists 
	
@chrome   
Scenario: Verify Question Tag text box with valid data 
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
	And : Click on Administration Tile 
	And : Click on Questions Tile 
	And : Click on "Add New" button of Questions listing screen 
	And : Enter valid data into the Question Tag text box 
	Then : Verify the system should accept the Question tag value without any validation message 
	
@chrome  
Scenario: Verify Instructions for user TextBox is appear after checking "Instructions for user" 
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
	And : Click on Administration Tile 
	And : Click on Questions Tile 
	And : Click on "Add New" button of Questions listing screen 
	And : Click on Instructions for user checkbox 
	Then : Verify that Instructions for user checkbox appears on the screen 
	
@chrome  
Scenario: Verify Mandatory validation message of Instructions for user Textbox 
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
	And : Click on Administration Tile 
	And : Click on Questions Tile 
	And : Click on "Add New" button of Questions listing screen 
	And : Click on Instructions for user checkbox 
	And : Click on Instructions for user text box and press the "TAB" Key 
	Then : Verify mandatory validation message as User Instructions are required. 
	
@chorme  
Scenario: Verify Minimum validation message of Instructions for user Textbox 
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
	And : Click on Administration Tile 
	And : Click on Questions Tile 
	And : Click on "Add New" button of Questions listing screen 
	And : Click on Instructions for user checkbox 
	And : Enter Instructions For User textbox value less than 2 characters 
	Then : Verify that validation message as Instructions for user must be at least 2 characters. 
	
@chorme  
Scenario: Verify Maximum validation message of Instructions for user Textbox 
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
	And : Click on Administration Tile 
	And : Click on Questions Tile 
	And : Click on "Add New" button of Questions listing screen 
	And : Click on Instructions for user checkbox 
	And : Enter Instructions for user textbox value more than 800 characters 
	Then : Verify that validation message as Instructions for user cannot be more than 800 characters. 
	
@chrome  
Scenario: Verify Instructions for user Textbox with valid data 
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
	And : Click on Administration Tile 
	And : Click on Questions Tile 
	And : Click on "Add New" button of Questions listing screen 
	And : Click on Instructions for user checkbox 
	And : Enter valid data into Instructions for user textbox 
	Then : Veirfy the system should accept the Instructions for user textbox value wihtout any validation message 
	
@chrome  
Scenario: Verify the Add Question functionality 
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
	And : Click on Administration Tile 
	And : Click on Questions Tile 
	And : Click on "Add New" button of Questions listing screen 
	And : Add valid data for all fields and click on the save button 
	Then : Verify that the newly added Question should appear on the Questions listing screen and the page should be redirected to Questions listing screen 
	
@chrome  
Scenario: Verify the Navigation and Breadcrumb of Audit trail page of Questions module 
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
	And : Click on Administration Tile 
	And : Click on Questions Tile 
	And : Click on "View Audit Trail" link of the first record of the listing page 
	Then : Verify that system should display Audit Trail page and Breadcrumb as "Home > Administration > Questions > Audit Trail" when user click on "View Audit Trail" link of the Questions listing screen 
	
@chrome    
Scenario: Verify pagination of Audit Trail when user click on "Last" for Questions module 
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
	And : Click on Administration Tile 
	And : Click on Questions Tile 
	And : Click on "View Audit Trail" link of the first record of the listing page  
	And : Click on the Last button of Pagination
	Then : Verify that the system should display the last page of the Audit trail screen
	
@chrome  
Scenario: Verify pagination of Audit Trail when user click on "First" for Questions module 
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
	And : Click on Administration Tile 
	And : Click on Questions Tile 
	And : Click on "View Audit Trail" link of the first record of the listing page 
	And : Click on the Last button of Pagination 
	And : Click on the First button of Pagination 
   Then : Verify that the system should display the first page of the Audit trail screen	
   
@chrome  
Scenario: Verify pagination of Audit Trail when user click on "Next" for Questions module 
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
	And : Click on Administration Tile 
	And : Click on Questions Tile 
	And : Click on "View Audit Trail" link of the first record of the listing page
	And : Click on Next button of Pagination 
   Then : Verify that the system should display the second page of the Audit trail screen 
   
@chrome  
Scenario: Verify pagination of Audit Trail when user click on "Previous" for Questions module 
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
	And : Click on Administration Tile 
	And : Click on Questions Tile 
	And : Click on "View Audit Trail" link of the first record of the listing page 
	And : Click on the Previous button of Pagination 
	Then : Verify that the system should display the previous page of the Audit trail screen
	
@chrome  
Scenario: Verify Color of Back button in Audit Trail of Questions module 
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
	And : Click on Administration Tile 
	And : Click on Questions Tile 
	And : Click on "View Audit Trail" link of the first record of the listing page 
	Then : Verify the Color of Back button 
	
@chrome  
Scenario: Verify Back button functionality in Audit Trail of Questions module 
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
	And : Click on Administration Tile 
	And : Click on Questions Tile 
	And : Click on "View Audit Trail" link of the first record of the listing page
	And : Click on Back button of Question
	Then : Verify that the system should redirect to Questions listing screen and Breadcrumb display as "Questions" 
	
	
	
	
	
	
	
	
	