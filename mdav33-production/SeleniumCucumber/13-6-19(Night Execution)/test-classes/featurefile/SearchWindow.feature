Feature: Info: Executed script on "test1.beperfeqta.com/mdav33" link.
    Module Name: Search Window

  @chrome
  Scenario: Verify default selection of dropdown of Module field in Search Window
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Qualification Performance Tile
    And : Click on Search Window Tile
    Then : Verify that "--Please Select--" option should be selected bydefault in Module dropdown

  @chrome
  Scenario: Verify functionality of Search button before selecting any option
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Qualification Performance Tile
    And : Click on Search Window Tile
    And : Click on Module dropdown and Select value of Module dropdown of Search Window
    Then : Verify that Search button should be disabled before selecting any option

  @chrome
  Scenario: Verify functionality of Search button in Search Window
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Qualification Performance Tile
    And : Click on Search Window Tile
    And : Click on Module dropdown and Select value of Module dropdown of Search Window
    And : Click on App dropdown and Select value of App dropdown of Search Window
    And : Click on Search button
    Then : Verify that system should displayed searched value when user click on Search button after selcting module and app

  @chrome
  Scenario: Verify functionality of Search button after selecting all option
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Qualification Performance Tile
    And : Click on Search Window Tile
    And : Click on Module dropdown and Select value of Module dropdown of Search Window
    And : Click on App dropdown and Select value of App dropdown of Search Window
    Then : Verify that Search button should be enabled after selecting all option

  @chrome
  Scenario: Verify Label of Search Window screen
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Qualification Performance Tile
    And : Click on Search Window Tile
    Then : Verify that Label of Seach Window screen should be displayed as Search Window

  @chrome
  Scenario: Verify Breadcrumb of Seach Window screen
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Qualification Performance Tile
    And : Click on Search Window Tile
    Then : Verify that system should display Breadcrumb for Search Window as "Home > Qualification Performance > Search Window"

  @chrome
  Scenario: Verify functionality of Check All button in Window Type dropdown
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Qualification Performance Tile
    And : Click on Search Window Tile
    And : Click on Filter By button
    And : Click on Window Type dropdown
    And : Click on Check All button
    Then : Verify that All value of Window Type dropdown should be Checked when user Click on Check All button

  @chrome
  Scenario: Verify functionality of Uncheck All button in Window Type dropdown
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Qualification Performance Tile
    And : Click on Search Window Tile
    And : Click on Filter By button
    And : Click on Window Type dropdown
    And : Click on Check All button
    And : Click on Uncheck All button
    Then : Verify that All value of Window Type dropdown should be Unchecked when user Click on Uncheck All button

  @chrome
  Scenario: Verify Todays Date is display when user select Today option from Date option dropdown
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Qualification Performance Tile
    And : Click on Search Window Tile
    And : Click on Filter By button
    And : Click on Date Option and select Today option from Date Option dropdown
    Then : Verify that Todays Date should be displayed when user select Today option from Date Option dropdown

  @chrome
  Scenario: Verify Todays Date is display when user click on Today button from Datepicker
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Qualification Performance Tile
    And : Click on Search Window Tile
    And : Click on Filter By button
    And : Click on Datepicker
    And : Click on Today button from Datepicker
    Then : Verify that Syatem shoul be displayed Todays Date when user click Today button from Datepicker

  @chrome
  Scenario: Verify functionality of Clear link for Date option
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Qualification Performance Tile
    And : Click on Search Window Tile
    And : Click on Filter By button
    And :  Click on dropdown of Date Options and Select First value of Date option dropdown
    And : Click on Clear link of filter by option in search window
    Then : Verify that system should clear selected value of Date option dropdown when user click on Clear link

  @chrome
  Scenario: Verify Required validation message for Month dropdown
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Qualification Performance Tile
    And : Click on Search Window Tile
    And : Click on Filter By button
    And : Click of Month-Year radio button
    And : Click on Year dropdown and Select Year
    Then : Verify that Required validation message for Month dropdown should be displayed as Month is required.

  @chrome
  Scenario: Verify Required validation message for Year dropdown
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Qualification Performance Tile
    And : Click on Search Window Tile
    And : Click on Filter By button
    And : Click of Month-Year radio button
    And : Click on Year dropdown and Press "TAB" Key
    Then : Verify that Required validation message for Year dropdown should be displayed as Year is required.

  @chrome
  Scenario: Ensure that System Favorite tab Functionality
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Qualification Performance Tile
    And : Click on Search Window Tile
    And : Click on System Favorites
    Then : Verify that System Favorite tab should be displayed using Last Updated By

  @chrome
  Scenario: Verify label of Add To Favorite button in Search Window screen
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Qualification Performance Tile
    And : Click on Search Window Tile
    Then : Verify that label of Add To Favorite button should be displayed as Add To Favorite

  @chrome
  Scenario: Verify the functionality of Save button when user click on save button with valid data
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Qualification Performance Tile
    And : Click on Search Window Tile
    And : Click on Module dropdown and Select value of Module dropdown of Search Window
    And : Click on App dropdown and Select value of App dropdown of Search Window
    And : Click on Add To Favorite button in search window screen
    And : Click on textbox of Enter Favorite Name in search widnow pop up
    And : Enter valid data in Favorite name textbox of search window pop up
    And : Click on Save button of add to favorite pop up of search window pop up
    Then : Verify that system should save Add To Favorite when user click on Save button with valid data

  # in this scenario @After method will throw exception which will be not an issue.
  @chrome
  Scenario: Verify validation message for Duplicate Name of Add To Favorite
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Qualification Performance Tile
    And : Click on Search Window Tile
    And : Click on Module dropdown and Select value of Module dropdown of Search Window
    And : Click on App dropdown and Select value of App dropdown of Search Window
    And : Click on Add To Favorite button in search window screen
    And : Click on textbox of Enter Favorite Name in search widnow pop up
    And : Enter Name which is already added into Add To Favorite
    Then : Verify that validation message for Duplicate Name Should be displayed as Favorite Name must be unique.

  #in this scenario @After method will throw exception which will be not an issue.
  @chrome
  Scenario: Verify Required validation message for Enter Favorite Name textbox in Add To Favorite popup
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Qualification Performance Tile
    And : Click on Search Window Tile
    And : Click on Module dropdown and Select value of Module dropdown of Search Window
    And : Click on App dropdown and Select value of App dropdown of Search Window
    And : Click on Add To Favorite button in search window screen
    And : Click on textbox of Enter Favorite Name in search widnow pop up
    And : Press "TAB" key of Add to Favorite Name Pop Up text Box of Search Window
    Then : Verify that Required validation message should be displayed as Favorite Name is required.

  #in this scenario @After method will throw exception which will be not an issue.
  @chrome
  Scenario: Verify Minimum validation message for Enter Favorite Name textbox in Add To Favorite popup
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Qualification Performance Tile
    And : Click on Search Window Tile
    And : Click on Module dropdown and Select value of Module dropdown of Search Window
    And : Click on App dropdown and Select value of App dropdown of Search Window
    And : Click on Add To Favorite button in search window screen
    And : Click on textbox of Enter Favorite Name in search widnow pop up
    And : Enter less than 2 characters in Favorite Name in search window pop up
    Then : Verify that Minimum validation message should be displayed as Favorite Name must be at least 2 characters.

  #in this scenario @After method will throw exception which will be not an issue.
  @chrome
  Scenario: Verify Maximum validation message for Enter Favorite Name textbox in Add To Favorite popup
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Qualification Performance Tile
    And : Click on Search Window Tile
    And : Click on Module dropdown and Select value of Module dropdown of Search Window
    And : Click on App dropdown and Select value of App dropdown of Search Window
    And : Click on Add To Favorite button in search window screen
    And : Click on textbox of Enter Favorite Name in search widnow pop up
    And : Enter more than 50 characters in Favorite Name in search window pop up
    Then : Verify that Maximum validation message should be displayed as Favorite Name cannot be more than 50 characters.

  #in this scenario @After method will throw exception which will be not an issue.
  @chrome
  Scenario: Verify the color of Save button of Add To Favorite popup
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Qualification Performance Tile
    And : Click on Search Window Tile
    And : Click on Module dropdown and Select value of Module dropdown of Search Window
    And : Click on App dropdown and Select value of App dropdown of Search Window
    And : Click on Add To Favorite button in search window screen
    Then : Verify that the color of Save button should be Green

  #in this scenario @After method will throw exception which will be not an issue.
  @chrome
  Scenario: Verify the color of Cancel button of Add To Favorite popup
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Qualification Performance Tile
    And : Click on Search Window Tile
    And : Click on Module dropdown and Select value of Module dropdown of Search Window
    And : Click on App dropdown and Select value of App dropdown of Search Window
    And : Click on Add To Favorite button in search window screen
    Then : Verify that the color of Cancel button should be Black

  @chrome
  Scenario: Verify the color of Seach button in Seach Window screen
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Qualification Performance Tile
    And : Click on Search Window Tile
    Then : Verify that the color of Seach button should be Green

  @chrome
  Scenario: Verify the color of Clear All button in Seach Window screen
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Qualification Performance Tile
    And : Click on Search Window Tile
    Then : Verify that the color of Clear All button should be Black

  @chrome
  Scenario: Verify the color of Add To Favorite button in Seach Window screen
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Qualification Performance Tile
    And : Click on Search Window Tile
    Then : Verify that the color of Add To Favorite button should be Blue

  #in this scenario @After method will throw exception which will be not an issue.
  @chrome
  Scenario: Verify Confirmation message when user click on Delete icon from My Favorites list
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Qualification Performance Tile
    And : Click on Search Window Tile
    And : Click on Delete icon of first recod from My Favorites list
    Then : Verify that Confirmation message should be displayed as Are you sure you want to remove Example?

  #in this scenario @After method will throw exception which will be not an issue.
  @chrome
  Scenario: Verify the color of No button of Confirmation message popup of Delete icon
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Qualification Performance Tile
    And : Click on Search Window Tile
    And : Click on Delete icon of first recod from My Favorites list
    Then : Verify that the color of No button of Confirmation message popup should be Red

  @chrome
  Scenario: Verify the functionality of No button of Confirmation message popup
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Qualification Performance Tile
    And : Click on Search Window Tile
    And : Click on Delete icon of first recod from My Favorites list
    And : Click on No button of Confirmation message popup
    Then : Verify that system should not delete the column when user click on No button of Confirmation message popup

  #in this scenario @After method will throw exception which will be not an issue.
  @chrome
  Scenario: Verify the color of Yes button of Confirmation message popup of Delete icon
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Qualification Performance Tile
    And : Click on Search Window Tile
    And : Click on Delete icon of first recod from My Favorites list
    Then : Verify that the color of Yes button of Confirmation message popup should be Green

  @chrome
  Scenario: Verify the functionality of Yes button of Confirmation message popup
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Qualification Performance Tile
    And : Click on Search Window Tile
    And : Click on Delete icon of first recod from My Favorites list
    And : Click on Yes button of Confirmation message popup
    Then : Verify that system should be delete the column when user click on Yes button of Confirmation message popup

  @chrome
  Scenario: Verify functionality of Clear All button in Search Window screen
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Qualification Performance Tile
    And : Click on Search Window Tile
    And : Click on Module dropdown and Select value of Module dropdown of Search Window
    And : Click on App dropdown and Select value of App dropdown of Search Window
    And : Click on Search button
    And : Click on Clear All button of Search Window Screen
    Then : Verify that system should Clear all the selected data which is displayed on the screen when user click on Clear All button
