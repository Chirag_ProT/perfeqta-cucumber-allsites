Feature: Search feature of the web page

  @sanity
  Scenario: sanity
    Given : I am at the home page
    Then : print Sanity.

  @smoke
  Scenario: smoke
    Given : I am at the home page
    Then : print smoke.

  @regression
  Scenario: regression
    Given : I am at the home page
    Then : print regression.
