package com.cucumber.framework.helper;

import static java.time.DayOfWeek.SATURDAY;
import static java.time.DayOfWeek.SUNDAY;
import static java.time.temporal.TemporalAdjusters.nextOrSame;
import static java.time.temporal.TemporalAdjusters.previousOrSame;
import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.time.Duration;
import java.time.LocalDate;
import java.util.NoSuchElementException;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.Color;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cucumber.framework.settings.ObjectRepo;
import com.cucumber.framework.utility.Constant;
import com.google.common.base.Function;
import java.util.Random;
import java.time.format.DateTimeFormatter;
import java.time.LocalDateTime;

public class Helper {

	public WebDriverWait wait = new WebDriverWait(ObjectRepo.driver, 10);

	public WebElement element = null;

	public void browser_quit() {
		ObjectRepo.driver.quit();
	}

	// This function will get values from Clipboard.
	public String getClipboardContents(WebElement xPath) throws Exception {

		String copy = Keys.chord(Keys.CONTROL, Keys.chord("c"));
		xPath.sendKeys(Keys.CONTROL + "a");
		xPath.sendKeys(copy);
		Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
		Transferable contents = clipboard.getContents(null);
		String copiedValue = (String) contents.getTransferData(DataFlavor.stringFlavor);
		System.out.println(copiedValue);
		return copiedValue;
	}

	public String ColorCode(WebElement element, String ColorCodeHex) {
		String color = element.getCssValue("background-color");
		System.out.println(color);
		String hex = Color.fromString(color).asHex();
		System.out.println(hex);
		return hex;
	}

	public String randomNumberGeneration() {
		Random rg = new Random();
		int randomInt = rg.nextInt(1000000)+1;
		System.out.println("Generated : " + randomInt);
		return  Integer.toString(randomInt);
	}

	public String FontColorCode(WebElement element, String ColorCodeHex) {
		String color = element.getCssValue("color");
		System.out.println("font color: " + color);
		String hex = Color.fromString(color).asHex();
		System.out.println(hex);
		return hex;
	}

	public String BtnColor(WebElement element) {
		String color = element.getCssValue("background-color");
		System.out.println(color);
		String hex = Color.fromString(color).asHex();
		System.out.println(hex);
		return hex;
	}

	public String FontColor(WebElement element) {
		String color = element.getCssValue("color");
		System.out.println("font color: " + color);
		String hex = Color.fromString(color).asHex();
		System.out.println(hex);
		return hex;
	}

	// by santosh
	public String FontColorCodeHex(WebElement element) {
		String color = element.getCssValue("color");
		String hex = Color.fromString(color).asHex();
		System.out.println(hex);

		return hex;
	}

	public void scroll_down(WebDriver driver) {
		/*
		 * Function : This function is used to scroll down. Function Call :
		 * scroll_down(); Created By : Unnati Solanki Created Date : 12/21/2017
		 */
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("window.scrollTo(0, document.body.scrollHeight);");

	}

	public void scroll_element(String Element_location, WebDriver browser) {
		/*
		 * Function : This function is used to scroll to the element. Function Call :
		 * scroll_element(); Created By : Unnati Solanki Created Date : 04/22/2018
		 */
		WebElement element = browser.findElement(By.xpath(Element_location));
		((JavascriptExecutor) browser).executeScript("arguments[0].click();", element);
	}

	public void scroll_up(WebDriver browser) {
		/*
		 * Function : This function is used to scroll up. Function Call : scroll_up();
		 * Created By : Unnati Solanki Created Date : 12/21/2017
		 */

		JavascriptExecutor jse = (JavascriptExecutor) browser;
		jse.executeScript("window.scrollTo(0, -document.body.scrollHeight);");

	}

	public void takeSnapShot(String i, WebDriver browser) throws Exception {
		/*
		 * Function : This function is used to take snapshot. It will take snapshot of
		 * entire page. Function Call : takeSnapShot("Attribute_Edit_Screen"); Created
		 * By : Unnati Solanki Created Date : 12/21/2017
		 */
		//		Shutterbug.shootPage(browser, ScrollStrategy.BOTH_DIRECTIONS).save("D:\\PERFEQTA Script Screenshot\\" + i);
	}

	public void FluentWaitClickAction(final WebElement InputElement, WebDriver driver) throws Exception {
		/*
		 * Function : This function is used for Fluent Wait Click Action. By : Santosh
		 * Shinde Created Date : 21th june 208
		 */
		FluentWait<WebDriver> waitNew = new FluentWait<WebDriver>(driver).withTimeout(Duration.ofSeconds(30))
				.pollingEvery(Duration.ofSeconds(5)).ignoring(NoSuchElementException.class);

		WebElement foo = waitNew.until(new Function<WebDriver, WebElement>() {
			public WebElement apply(WebDriver driver) {
				WebElement foo = InputElement;

				wait.until(ExpectedConditions.visibilityOfAllElements(InputElement));
				wait.until(ExpectedConditions.elementToBeClickable(InputElement)).click();
				return foo;
			}
		});
	}

	public WebElement FluentWaitPage(final WebElement InputElement, WebDriver driver) throws Exception {
		/*
		 * Function : This function is used for Fluent Wait Page Element. By : Santosh
		 * Shinde Created Date : 21th june 208
		 */

		FluentWait<WebDriver> waitNew = new FluentWait<WebDriver>(driver).withTimeout(Duration.ofSeconds(30))
				.pollingEvery(Duration.ofSeconds(7)).ignoring(NoSuchElementException.class);

		WebElement foo = waitNew.until(new Function<WebDriver, WebElement>() {
			public WebElement apply(WebDriver driver) {
				return element = InputElement;
			}
		});
		return element;
	}

	public void insertValueInDisableTxtBox(WebElement WebElement, String Value) throws Exception {
		/*
		 * Function : This function is used to scroll up. Function Call : scroll_up();
		 * Created By : Unnati Solanki Created Date : 12/21/2017
		 */

		WebElement textbox = WebElement;
		((JavascriptExecutor) ObjectRepo.driver).executeScript("arguments[0].placeholder='" + Value + "'", textbox);
	}

	public String currentSystemDate() {
		/*
		 * Function : This function is used to display current system date : Created By
		 * : Santosh Shinde Created Date : 23th oct 2018
		 */
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("MM/dd/yyyy");
		LocalDateTime now = LocalDateTime.now();
		System.out.println(dtf.format(now));
		return dtf.format(now);
	}

	public String weekDateOfSunday() {
		/*
		 * Function : This function is used to display Sunday of current week: Created
		 * By : Santosh Shinde Created Date : 24th oct 2018
		 */
		LocalDate today = LocalDate.now();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/YYYY");
		LocalDate sunday = today.with(previousOrSame(SUNDAY));
		System.out.println("Today: " + formatter.format(today));
		System.out.println("Sunday of the Week: " + formatter.format(sunday));
		String weekSunday = formatter.format(sunday);
		return weekSunday;
	}

	public String weekDateOfSaturday() {
		/*
		 * Function : This function is used to display Saturday of current week: Created
		 * By : Santosh Shinde Created Date : 24th oct 2018
		 */
		LocalDate today = LocalDate.now();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/YYYY");
		LocalDate saturday = today.with(nextOrSame(SATURDAY));
		System.out.println("Today: " + formatter.format(today));
		System.out.println("Saturday of the Week: " + formatter.format(saturday));
		String weekSaturday = formatter.format(saturday);
		return weekSaturday;
	}

	public String thisMonthStartDate() {
		/*
		 * Function : This function is used to display first date of current month:
		 * Created By : Santosh Shinde Created Date : 24th oct 2018
		 */
		LocalDate today = LocalDate.now();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/YYYY");
		String thisMonthStartDate = formatter.format(today.withDayOfMonth(1));
		System.out.println("Start Date of this Month: " + thisMonthStartDate);
		return thisMonthStartDate;
	}

	public String thisMonthEndDate() {
		/*
		 * Function : This function is used to display end date of current month:
		 * Created By : Santosh Shinde Created Date : 24th oct 2018
		 */
		LocalDate today = LocalDate.now();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/YYYY");
		String thisMonthEndDate = formatter.format(today.withDayOfMonth(today.lengthOfMonth()));
		System.out.println("End Date of this Month: " + thisMonthEndDate);
		return thisMonthEndDate;
	}

	public void javascriptExecutorClick(WebElement element) {

		JavascriptExecutor executor = (JavascriptExecutor) ObjectRepo.driver;
		executor.executeScript("arguments[0].click();", element);
	}

	public void SelectDrpDwnValueByText(WebElement elementInput, String valueInput) throws Exception {
		/*
		 * Function : This function will select Dropdown option by visible text. Created
		 * By : Santosh Shinde Created Date : 27th nov 2018
		 */
		Select Drpdwn = new Select(elementInput);
		Drpdwn.selectByVisibleText(valueInput);
		Thread.sleep(1000);
	}

	public void SelectDrpDwnValueByIndex(WebElement inputElement, int indexNo) throws Exception {
		/*
		 * Function : This function will select Dropdown option by index number. Created
		 * By : Santosh Shinde Created Date : 27th nov 2018
		 */
		Select dateDrpdwn = new Select(inputElement);
		dateDrpdwn.selectByIndex(indexNo);
		Thread.sleep(1000);
	}

	public String getDrpDwnFirstSelectedOptn(WebElement element) throws Exception {
		/*
		 * Function : This function will return the first selected value of dropdown.
		 * Created By : Santosh Shinde Created Date : 27th nov 2018
		 */
		Select dateDrpdwn = new Select(element);
		WebElement option = dateDrpdwn.getFirstSelectedOption();
		String defaultItem = option.getText();
		System.out.println(defaultItem);
		Thread.sleep(1000);
		return defaultItem;
	}
	public Integer randomNum() throws Exception {
		/*
		 * Function : This function will give Random numbers between 1 and 100.
		 * Created By : Santosh Shinde Created Date : 29th Jan 2019
		 */
		Random random = new Random();
		int ranNum = random.nextInt(99)+1;
		return ranNum; 

	}

}
