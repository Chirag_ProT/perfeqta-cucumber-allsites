package com.cucumber.framework.helper.PageObject;

import java.lang.reflect.Array;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.Assert;
import com.cucumber.framework.helper.Helper;
import com.cucumber.framework.helper.Button.ButtonHelper;
import com.cucumber.framework.helper.Logger.LoggerHelper;
import com.cucumber.framework.settings.ObjectRepo;

public class AdvancedSearchPageObject extends PageBase {
	private WebDriver driver;
	public ButtonHelper btnHelper;
	private Helper helper = new Helper();
	private final Logger log = LoggerHelper.getLogger(AdvancedSearchPageObject.class);
	public String allModuleExpected;
	public String expectedAppName;

	public AdvancedSearchPageObject(WebDriver driver) {
		super(driver);
		this.driver = driver;
		btnHelper = new ButtonHelper(driver);
	}

	/** WebElements */

	@FindBy(how = How.XPATH, using = "//a[@ui-sref='secure.advancesearch.searchByApps']")
	public WebElement clickAdvSearchTile;

	@FindBy(how = How.XPATH, using = "//span[@class='currentStep ng-binding']")
	public WebElement advSearchModuleName;

	@FindBy(how = How.XPATH, using = "//span[@ng-bind-html='step.ncyBreadcrumbLabel']")
	public WebElement advSearchBreadcrumbs;

	@FindBy(how = How.XPATH, using = "//div[1]/form/div[1]/ul/li[1]/a")
	public WebElement advSearchLabel;

	@FindBy(how = How.XPATH, using = "//fieldset[@ng-disabled='vm.loadapp']")
	public WebElement displayModule;

	@FindBy(how = How.XPATH, using = "//input[@ng-click='vm.onModuleSelectChange()']")
	public WebElement uncheckAllModule;

	@FindBy(how = How.XPATH, using = "//input[@ng-model='$select.search']")
	public WebElement searchAppTextfield;

	@FindBy(how = How.XPATH, using = "//a[@ng-click='vm.clearApps()']")
	public WebElement clearLink;

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Enter one or multiple App names to search']")
	public WebElement clearFunctionality;

	@FindBy(how = How.XPATH, using = "//ul[@class='ui-select-no-choice dropdown-menu']")
	public WebElement NoAppsFoundDrpdwn;

	@FindBy(how = How.XPATH, using = "//span[@class='ui-select-choices-row-inner']")
	public WebElement selectApps;

	@FindBy(how = How.XPATH, using = "//label[@class='ng-binding']")
	public WebElement appNameVerify;

	@FindBy(how = How.XPATH, using = "//button[@class='btn btn-success ng-binding']")
	public WebElement goBtn;

	@FindBy(how = How.XPATH, using = "/html/body/div/div/ui-view/div/div[3]/div/div[1]/div/div[1]/div[1]/b")
	public WebElement clickAppToCollapse;

	@FindBy(how = How.XPATH, using = "//div[@class='col-md-10 col-md-offset-1 alert alert-warning no-records ng-binding']")
	public WebElement noRecordFoundListings;

	@FindBy(how = How.XPATH, using = "//div[1]/div/div/div/quick-search/div/div/table/tbody/tr[3]/td/div/span/a")
	public WebElement recordsPendingYourReview;

	@FindBy(how = How.XPATH, using = "//div[1]/div/div/div/quick-search/div/div/table/tbody/tr[1]/td/div/span/a")
	public WebElement recordsYouCreatedToday;

	@FindBy(how = How.XPATH, using = "//div[1]/div/div/div/quick-search/div/div/table/tbody/tr[4]/td/div/span/a")
	public WebElement recordsAssignedToYou;

	@FindBy(how = How.XPATH, using = "//div[1]/div/div/div/quick-search/div/div/table/tbody/tr[2]/td/div/span/a")
	public WebElement recordsYouAccessedToday;

	@FindBy(how = How.XPATH, using = "//div[4]/div/div[1]/div[1]/h4")
	public WebElement expandcollapseFilterBy;

	@FindBy(how = How.XPATH, using = "//label[@for='date']")
	public WebElement dateLabelFilterBy;

	@FindBy(how = How.XPATH, using = "//*[contains(text(), 'Batch')]")
	public WebElement verifyBatchLink;

	@FindBy(how = How.XPATH, using = "//button[@ng-click='vm.clear();advanceSearchForm.$setPristine();advanceSearchForm.$setUntouched();']")
	public WebElement clickClearBtn;

	@FindBy(how = How.XPATH, using = "//img[@title='Export as CSV']")
	public WebElement csvIconDisplay;

	@FindBy(how = How.XPATH, using = "//img[@title='Export as Excel']")
	public WebElement excelIconDisplay;

	@FindBy(how = How.XPATH, using = "//img[@title='Export as PDF']")
	public WebElement pdfIconDisplay;

	@FindBy(how = How.XPATH, using = "//button[@title='Click to save your search']")
	public WebElement addToFavoriteBtn;

	@FindBy(how = How.XPATH, using = "//button[@ng-click='vm.saveData(favoriteSearchForm)']")
	public WebElement addToFavoriteSaveBtn;

	@FindBy(how = How.XPATH, using = "//p[@translate='text.FAVORITENAMEISREQUIRED']")
	public WebElement addToFavoriteRequiredMsg;

	@FindBy(how = How.XPATH, using = "//button[@ng-click='vm.closePopup()']")
	public WebElement cancelBtnAddToFavorite;

	@FindBy(how = How.XPATH, using = "//input[@name='searchNameTextbox']")
	public WebElement addToFavoriteTextfield;

	@FindBy(how = How.XPATH, using = "//p[@translate='text.FAVORITENAMESHOULDBEAT2']")
	public WebElement addToFavoriteMinimumMsg;

	@FindBy(how = How.XPATH, using = "//p[@translate='text.FAVORITENAMESHOULDNOTMORE50']")
	public WebElement addToFavoriteMaximumMsg;

	@FindBy(how = How.XPATH, using = "//h4[@class='col-sm-11 ng-binding']")
	public WebElement addToFavoriteLabel;

	@FindBy(how = How.XPATH, using = "//i[@class='glyphicon glyphicon-remove pull-right critearia-01 ng-click-active']")
	public WebElement closeIconAddToFavorite;

	@FindBy(how = How.XPATH, using = "//input[@ng-value='false']")
	public WebElement addToFavoriteRadioBtn;

	@FindBy(how = How.XPATH, using = "//div/div[2]/div/div/div/table/tbody/tr/td[1]/div/span/a")
	public WebElement favoriteSavedSuccess;

	@FindBy(how = How.XPATH, using = "//p[@translate='text.FAVORITENAMEMUSTBEUNIQUE']")
	public WebElement addToFavoriteUniqueMsg;
	
	@FindBy(how = How.XPATH, using = "//div/div[2]/label/em")
	public WebElement clickMyFavoriteTable;

	@FindBy(how = How.XPATH, using = "//div[2]/div/div/div/table/tbody/tr/td[3]/a/i")
	public WebElement removeFromMyFavorite;

	@FindBy(how = How.XPATH, using = "//p[@style='display: block;']")
	public WebElement removeMsgMyFavorite;

	@FindBy(how = How.XPATH, using = "//button[@class='confirm']")
	public WebElement removeYesMsgMyFavorite;

	@FindBy(how = How.XPATH, using = "//button[@class='cancel']")
	public WebElement removeNoMsgMyFavorite;

	@FindBy(how = How.XPATH, using = "//div[2]/label/em")
	public WebElement myFavoritesLabel;

	@FindBy(how = How.XPATH, using = "//strong[@style='font-size: 15px;']")
	public WebElement filterByLabel;

	@FindBy(how = How.XPATH, using = "//div[@class='advance-search-main-icons-bx ng-scope']")
	public WebElement filterByIcons;

	@FindBy(how = How.XPATH, using = "//div[1]/ul/li[1]/a/em")
	public WebElement dateRangeSpelling;

	@FindBy(how = How.XPATH, using = "//div[1]/ul/li[2]/a/em")
	public WebElement attributes_EntitiesSpelling;

	@FindBy(how = How.XPATH, using = "//div[1]/ul/li[3]/a/em")
	public WebElement sitesSpelling;

	@FindBy(how = How.XPATH, using = "//div[1]/ul/li[4]/a/em")
	public WebElement batchEntriesSpelling;

	@FindBy(how = How.XPATH, using = "//div[1]/ul/li[5]/a/em")
	public WebElement assigneeSpelling;

	@FindBy(how = How.XPATH, using = "//div[1]/ul/li[6]/a/em")
	public WebElement recordStatusSpelling;

	@FindBy(how = How.XPATH, using = "//select[@name='qcform']")
	public WebElement dateDrpDwnFilterBy;

	@FindBy(how = How.XPATH, using = "//input[@ng-model='vm.advancesearch.date']")
	public WebElement getDateFilterBy;

	@FindBy(how = How.XPATH, using = "//input[@ng-model='vm.advancesearch.startDate']")
	public WebElement getStartDateFilterBy;

	@FindBy(how = How.XPATH, using = "//input[@ng-model='vm.advancesearch.endDate']")
	public WebElement getEndDateFilterBy;

	@FindBy(how = How.XPATH, using = "//input[@ng-change=\"vm.changeRadioButtonValue('dateRange')\"]")
	public WebElement dateRangeByDefault;

	@FindBy(how = How.XPATH, using = "//a[@ng-click=\"vm.onClearFilters('date')\"]")
	public WebElement clearLinkFilterBy;

	@FindBy(how = How.XPATH, using = "//span[@title='Remove Date Range Filter']")
	public WebElement removeIconFilterBy;

	@FindBy(how = How.XPATH, using = "//*[@for='test1']")
	public WebElement monthYearDrpDwn;

	@FindBy(how = How.XPATH, using = "//select[@ng-model='vm.advancesearch.month']")
	public WebElement selectMonthDrpDwn;

	@FindBy(how = How.XPATH, using = "//select[@ng-model='vm.advancesearch.year']")
	public WebElement selectYearDrpDwn;

	@FindBy(how = How.XPATH, using = "//td[@data-th='Created Date']")
	public WebElement createdDateGrid;

	@FindBy(how = How.XPATH, using = "//*[@ng-click='selectPage(page + 1, $event)']")
	public WebElement nextPagging;

	@FindBy(how = How.XPATH, using = "//button[@ng-click='vm.open1()']")
	public WebElement specificDateIcon;

	@FindBy(how = How.XPATH, using = "//button[@ng-click=\"select('today', $event)\"]")
	public WebElement selectSpecificDate;

	@FindBy(how = How.XPATH, using = "//p[@translate='text.monthisrequired']")
	public WebElement monthDrpDwnRequiredMsg;
	
	@FindBy(how = How.XPATH, using = "//p[@translate='text.yearisrequired']")
	public WebElement yearDrpDwnRequiredMsg;

	@FindBy(how = How.XPATH, using = "//select[@ng-model='vm.attribEtyFilter[$index].attrb']")
	public WebElement attributeDrpDwn;

	@FindBy(how = How.XPATH, using = "//p[@class='err-message ng-binding ng-scope']")
	public WebElement valueRequiredMsg;

	@FindBy(how = How.XPATH, using = "//button[@ng-click='vm.addAttribute($index)']")
	public WebElement addAttribute;

	@FindBy(how = How.XPATH, using = "//select[@name='attributeTextbox0']")
	public WebElement attributeValue;

	@FindBy(how = How.XPATH, using = "//span[@title='Remove Attributes/Entity Filter']")
	public WebElement attributesRemoveIcon;

	@FindBy(how = How.XPATH, using = "//a[@ng-click='vm.onClearFilters()']")
	public WebElement attributesClearLink;

	@FindBy(how = How.XPATH, using = "//a[@ng-click=\"vm.onClearFilters('sites')\"]")
	public WebElement sitesClearLink;

	@FindBy(how = How.XPATH, using = "//select[@title='Please select Region']")
	public WebElement sitesRegionDrpDwn;

	@FindBy(how = How.XPATH, using = "//span[@title='Remove Sites Filter']")
	public WebElement sitesRemoveIcon;

	@FindBy(how = How.XPATH, using = "//input[@name='batchEntryId']")
	public WebElement batchEntrytextfield;

	@FindBy(how = How.XPATH, using = "//p[@translate='secure.advancesearch.table.BATCHENTRYIDSHOULDNOT20']")
	public WebElement batchEntryMaximumMsg;

	@FindBy(how = How.XPATH, using = "//a[@ng-click=\"vm.onClearFilters('batchEntryId')\"]")
	public WebElement batchEntryClearLink;

	@FindBy(how = How.XPATH, using = "//span[@title='Remove Batch Entries Filter']")
	public WebElement batchEntryRemoveIcon;

	@FindBy(how = How.XPATH, using = "//label[@for='test11']")
	public WebElement assigneeRadioBtn;

	@FindBy(how = How.XPATH, using = "//label[@for='test12']")
	public WebElement assigneeToMeRadioBtn;

	@FindBy(how = How.XPATH, using = "//*[@id='Assignee-Clear-Roles']/div/button")
	public WebElement assigneeRoleDrpdwn;

	@FindBy(how = How.XPATH, using = "//*[@data-ng-click='selectAll()']")
	public WebElement checkAllRoleDrpdwn;

	@FindBy(how = How.XPATH, using = "//input[@checked='checked']")
	public WebElement checkAllFunctioning;

	@FindBy(how = How.XPATH, using = "//a[@data-ng-click='deselectAll();']")
	public WebElement unCheckAllRoleDrpdwn;

	@FindBy(how = How.XPATH, using = "//div[2]/div[2]/div[2]/div/ul/li[1]")
	public WebElement passRecordStatus;

	@FindBy(how = How.XPATH, using = "//div[2]/div[2]/div[2]/div/ul/li[2]")
	public WebElement failRecordStatus;

	@FindBy(how = How.XPATH, using = "//div[2]/div[2]/div[2]/div/ul/li[3]")
	public WebElement reviewPendingRecordStatus;

	@FindBy(how = How.XPATH, using = "//div[2]/div[2]/div[2]/div/ul/li[4]")
	public WebElement voidRecordStatus;

	@FindBy(how = How.XPATH, using = "//div[2]/div[2]/div[2]/div/ul/li[5]")
	public WebElement draftRecordStatus;

	@FindBy(how = How.XPATH, using = "//div[2]/div[2]/div[2]/div/ul/li[6]")
	public WebElement rejectedRecordStatus;

	@FindBy(how = How.XPATH, using = "//span[@title='Remove Record Status Filter']")
	public WebElement recordStatusRemoveIcon;

	@FindBy(how = How.XPATH, using = "//tbody/tr[1]/td[2]/div/a")
	public WebElement viewLinkApp;

	@FindBy(how = How.XPATH, using = "//div[@class='col-md-6 col-sm-6 col-xs-12 qcFormTitle']")
	public WebElement viewLinkAppTitle;






	// --------------------public methods--------------------

	public WebDriver getDriver() {
		return this.driver;
	}

	public void clickAdvancedSearchTile() throws Exception {
		Thread.sleep(1000);
		clickAdvSearchTile.click();
		Thread.sleep(1000);
	}

	public void verifyAdvSearchModuleName() throws Exception {
		log.info(advSearchModuleName);
		Assert.assertEquals(advSearchModuleName.getText(), "App Record Search");
	}

	public void verifyAdvSearchBreadcrumbs() throws Exception {
		log.info(advSearchBreadcrumbs);
		Assert.assertEquals(advSearchBreadcrumbs.getText(), "App Record Search");
	}

	public void verifyAdvSearchLabel() throws Exception {
		log.info(advSearchLabel);
		Assert.assertEquals(advSearchLabel.getText(), "App Record Search");
	}

	public void verifyAdvSearchLabelColor() throws Exception {
		log.info(advSearchLabel);
		Assert.assertEquals(helper.FontColor(advSearchLabel), "#1896bb");
	}

	public void verifyAllModuleIsDisplayed() throws Exception {

		log.info(displayModule);
		String expected = "", count = null, actual="";
		String arry[] = allModuleExpected.split(",");
		List<WebElement> links = driver.findElements(By.xpath("//fieldset[@ng-disabled='vm.loadapp']"));
		String []linkText =new String[links.size()];

		for(int i=0; i < links.size(); i++)
		{
			linkText[i] = links.get(i).getText();
			actual = actual + linkText[i];		   
		}

		for (int j=0; j < arry.length; j++)
		{
			count = arry[j];
			count = count.trim();
			expected = expected + "\n" + count;
			expected = expected.trim();
		}

		System.err.println(actual);
		System.out.println("=====================");
		System.err.println(expected);
		Assert.assertEquals(actual, expected);
	}

	public void verifyAllModuleSelected() throws Exception {
		log.info(advSearchLabel);
		if(driver.findElements(By.xpath("//input[@aria-checked='true']")).size() !=0)
		{
			Assert.assertEquals(true, true);
		}
		else
		{
			Assert.assertEquals(true, false);
		}
	}

	public void clickUncheckAllModule() throws Exception {
		log.info(uncheckAllModule);

		int count = driver.findElements(By.xpath("//*[@class='check--label-box']")).size();
		for (int i = 1; i <= count; i++)
		{
			driver.findElement(By.xpath("//*[@class='list']/li["+i+"]/label/span[1]")).click();
		}
	}

	public void clickCheckAllModule() throws Exception {
		log.info(uncheckAllModule);
		int count = driver.findElements(By.xpath("//*[@class='check--label-box']")).size();

		for (int i = 1; i <= count; i++)
		{
			driver.findElement(By.xpath("//*[@class='list']/li["+i+"]/label/span[1]")).click();
		}
	}

	public void verifyCheckAllModule() throws Exception {
		log.info(uncheckAllModule);
		WebElement element;
		int selectedModule = 1;
		int count = driver.findElements(By.xpath("//*[@class='check--label-box']")).size();

		for (int i = 1; i < count; i++) {
			element = driver.findElement(By.xpath("//*[@id='module" + i + "']"));
			if (element.isSelected()) {
				selectedModule++;
			}
		}
		System.out.println("Total selected module: " + selectedModule);
		Assert.assertEquals(selectedModule, count);
	}

	public void verifyUncheckAllModule() throws Exception {
		log.info(uncheckAllModule);
		WebElement element;
		int selectedModule = 1;
		int actual = 1;
		int count = driver.findElements(By.xpath("//*[@class='check--label-box']")).size();

		for (int i = 1; i < count; i++) {
			element = driver.findElement(By.xpath("//*[@id='module" + i + "']"));
			if (element.isSelected()) {
				selectedModule++;
			}
			else
			{
				actual++;
			}
		}
		System.out.println("Total selected module Actual: " + actual);
		System.out.println("Total selected module Old: " + selectedModule);
		Assert.assertEquals(actual, count);
	}

	public void clickSearchAppTextField() throws Exception {
		log.info(searchAppTextfield);
		searchAppTextfield.click();
	}

	public void searchAppValidAppName(String validAppName) throws Exception {
		searchAppTextfield.sendKeys(validAppName);
		Thread.sleep(500);
	}

	public void clickClearLink() throws Exception {
		log.info(clearLink);
		clearLink.click();
	}

	public void verifyClearFunctionality() throws Exception {
		log.info(clearLink);
		boolean isDisplayDrpDwn = driver.findElements(By.xpath("//*[@class='ui-select-choices ui-select-choices-content ui-select-dropdown dropdown-menu']")).size() != 0;
		Assert.assertEquals(isDisplayDrpDwn, false);
	}

	public void verifyNoAppsFoundMsg() throws Exception {
		log.info(NoAppsFoundDrpdwn);
		Assert.assertEquals(NoAppsFoundDrpdwn.getText(), "  No apps found.");
	}

	public void searchAppInvalidAppName(String invalidAppName) throws Exception {
		log.info(searchAppTextfield);
		searchAppTextfield.sendKeys(invalidAppName);
		Thread.sleep(500);
	}

	public void selectAppsFromDrpDwn(String invalidAppName) throws Exception {
		log.info(selectApps);
		selectApps.click();
		goBtn.click();
		String nowSplit = appNameVerify.getText();
		String actual[] = nowSplit.split(" ()");
		System.out.println("Expected Result: "+ actual[0]);
		Assert.assertEquals(actual[0], expectedAppName);
	}

	public void selectMultipleAppsFromDrpDwn(String mulApps1) throws Exception {
		log.info(selectApps);

		String[] splits = mulApps1.split(",");
		//		int sizeOfString = mulApps1.length();
		System.out.println("splits: "+ splits.toString()  + splits.length);
		for(int i=0; i<splits.length; i++)
		{
			searchAppTextfield.sendKeys(splits[i]);
			selectApps.click();
		}
		goBtn.click();
	}

	public void verifyMultipleApps() throws Exception {
		clickAppToCollapse.click();
		Thread.sleep(500);
		String nowSplit = appNameVerify.getText();
		String actual[] = nowSplit.split(" ()");
		System.out.println("Expected Result: "+ actual[0]);
		System.err.println("All Values: "+ actual);
		Assert.assertEquals(Arrays.toString(actual), expectedAppName);
		Assert.assertEquals(nowSplit, expectedAppName);
	}

	public void enterAppsHavingNoRecord(String NoRecord) throws Exception {
		searchAppTextfield.sendKeys(NoRecord);
		Thread.sleep(500);
	}

	public void selectAppsHavingNoRecord() throws Exception {
		selectApps.click();
		goBtn.click();
	}

	public void verifyNoRecordsFoundMsgInListings() throws Exception {
		log.info(searchAppTextfield);
		Assert.assertEquals(noRecordFoundListings.getText()," No Records Found.");
	}

	public void verifyRecordsPendingYourReview() throws Exception {
		Assert.assertEquals(recordsPendingYourReview.getText(),"Records Pending Your Review");
	}

	public void verifyRecordsPendingYourReviewColor() throws Exception {
		Assert.assertEquals(helper.FontColor(recordsPendingYourReview), "#337ab7");
	}

	public void verifyRecordsYouAccessedToday() throws Exception {
		Assert.assertEquals(recordsYouAccessedToday.getText(),"Records You Accessed Today");
	}

	public void verifyRecordsYouAccessedTodayColor() throws Exception {
		Assert.assertEquals(helper.FontColor(recordsYouAccessedToday), "#337ab7");
	}

	public void verifyRecordsAssignedToYou() throws Exception {
		Assert.assertEquals(recordsAssignedToYou.getText(),"Records Assigned To You");
	}

	public void verifyRecordsAssignedToYouColor() throws Exception {
		Assert.assertEquals(helper.FontColor(recordsAssignedToYou), "#337ab7");
	}

	public void verifyRecordsYouCreatedToday() throws Exception {
		Assert.assertEquals(recordsYouCreatedToday.getText(),"Records You Created Today");
	}

	public void verifyRecordsYouCreatedTodayColor() throws Exception {
		Assert.assertEquals(helper.FontColor(recordsYouCreatedToday), "#337ab7");
	}

	public void clickFilterByExpand() throws Exception {
		expandcollapseFilterBy.click();
	}

	public void clickFilterByCollapse() throws Exception {
		expandcollapseFilterBy.click();
	}

	public void verifyCollapseFilterByOption() throws Exception {
		boolean actual;
		if(driver.findElements(By.xpath("//label[@for='date']")).size() != 0)
		{
			actual = false;
		}
		else
		{
			actual = true;
		}
		Assert.assertEquals(actual, true);
	}

	public void searchForApp(String validApp) throws Exception {
		searchAppTextfield.sendKeys(validApp);
		Thread.sleep(500);
	}

	public void verifyGoBtnColor() throws Exception {
		Assert.assertEquals(helper.BtnColor(goBtn), "#5cb85c");
	}

	public void clickGoBtn() throws Exception {
		Thread.sleep(500);
		selectApps.click();
		Thread.sleep(500);
		goBtn.click();
	}

	public void verifyBatchReviewLink() throws Exception {
		log.info(verifyBatchLink);
		if(driver.findElements(By.xpath("//*[contains(text(), 'Batch')]")).size() != 0)
		{
			Assert.assertEquals(true, true);
		}
		else
		{
			Assert.assertEquals(true, false);
		}
	}

	public void clickClearAllBtn() throws Exception {
		log.info(clickClearBtn);
		clickClearBtn.click();
	}

	public void verifyClearAllFunctionality() throws Exception {
		log.info(clickClearBtn);
		WebElement element;
		int selectedModule = 1;
		int actual = 1;
		int count = driver.findElements(By.xpath("//*[@class='check--label-box']")).size();

		for (int i = 1; i < count; i++) {
			element = driver.findElement(By.xpath("//*[@id='module" + i + "']"));
			if (element.isSelected()) {
				selectedModule++;
			}
			else
			{
				actual++;
			}
		}
		System.out.println("Total selected module Actual: " + actual);
		System.out.println("Total selected module Old: " + selectedModule);
		Assert.assertEquals(actual, count);
	}

	public void clickClearAllBtnColor() throws Exception {
		log.info(clickClearBtn);
		Assert.assertEquals(helper.BtnColor(clickClearBtn), "#5a5a5a");
	}

	public void verifyAllFieldGetCleared() throws Exception {
		log.info(clickClearBtn);
		WebElement element;
		int selectedModule = 1;
		int actual = 1;
		int count = driver.findElements(By.xpath("//*[@class='check--label-box']")).size();

		for (int i = 1; i < count; i++) {
			element = driver.findElement(By.xpath("//*[@id='module" + i + "']"));
			if (element.isSelected()) {
				selectedModule++;
			}
			else
			{
				actual++;
			}
		}
		System.out.println("Total selected module Actual: " + actual);
		System.out.println("Total selected module Old: " + selectedModule);
		Assert.assertEquals(actual, count);
	}

	public void clickCollapseBtnGrid() throws Exception {
		log.info(clickAppToCollapse);
		clickAppToCollapse.click();
	}

	public void verifyCollapseBtnGrid() throws Exception {
		String nowSplit = appNameVerify.getText();
		if(nowSplit.contains(" ("))
		{
			nowSplit= nowSplit.substring(0, nowSplit.indexOf(" (")); 
			System.out.println(nowSplit);
		}
		Assert.assertEquals(nowSplit, expectedAppName);
	}

	public void verifyCSVIconIsVisible() throws Exception {
		boolean actual = csvIconDisplay.isDisplayed();
		Assert.assertEquals(actual, true);
	}

	public void verifyExcelIconIsVisible() throws Exception {
		boolean actual = excelIconDisplay.isDisplayed();
		Assert.assertEquals(actual, true);
	}

	public void verifyPDFIconIsVisible() throws Exception {
		boolean actual = pdfIconDisplay.isDisplayed();
		Assert.assertEquals(actual, true);
	}

	public void verifyAddToFavoriteBtn() throws Exception {
		Assert.assertEquals(helper.BtnColor(addToFavoriteBtn), "#337ab7");
	}

	public void clickAddToFavoriteBtn() throws Exception {
		addToFavoriteBtn.click();
		Thread.sleep(1000);
	}

	public void clickAddToFavoriteSaveBtn() throws Exception {
		addToFavoriteSaveBtn.click();
	}

	public void verifyAddToFavoriteRequiredMsg() throws Exception {
		Assert.assertEquals(addToFavoriteRequiredMsg.getText(), "Favorite Name is required.");
		cancelBtnAddToFavorite.click();
	}

	public void verifyAddToFavoriteRequiredMsgColor() throws Exception {
		Assert.assertEquals(helper.FontColor(addToFavoriteRequiredMsg), "#a94442");
		cancelBtnAddToFavorite.click();
	}

	public void clickAddToFavoriteTextField() throws Exception {
		addToFavoriteTextfield.click();
	}

	public void enterAddToFavoriteMinimumChar(String minChar) throws Exception {
		addToFavoriteTextfield.clear();
		Thread.sleep(1000);
		addToFavoriteTextfield.sendKeys(minChar);
	}

	public void verifyAddToFavoriteMinimumMsg() throws Exception {
		Assert.assertEquals(addToFavoriteMinimumMsg.getText(), "Favorite Name must be at least 2 characters.");
		cancelBtnAddToFavorite.click();
	}

	public void verifyAddToFavoriteMinimumMsgColor() throws Exception {
		Assert.assertEquals(helper.FontColor(addToFavoriteMinimumMsg), "#a94442");
		cancelBtnAddToFavorite.click();
	}

	public void enterAddToFavoriteMaximumChar(String maxChar) throws Exception {
		addToFavoriteTextfield.clear();
		addToFavoriteTextfield.sendKeys(maxChar);
	}

	public void verifyAddToFavoriteMaximumMsg() throws Exception {
		Assert.assertEquals(addToFavoriteMaximumMsg.getText(), "Favorite Name cannot be more than 50 characters.");
		Thread.sleep(500);
		cancelBtnAddToFavorite.click();
	}

	public void verifyAddToFavoriteMaximumMsgColor() throws Exception {
		Assert.assertEquals(helper.FontColor(addToFavoriteMaximumMsg), "#a94442");
		cancelBtnAddToFavorite.click();
	}

	public void enterAddToFavoriteValidChar(String validChar) throws Exception {
		addToFavoriteTextfield.clear();
		addToFavoriteTextfield.sendKeys(validChar);
	}

	public void verifyAddToFavoriteNoValidationMsg() throws Exception {
		boolean required = driver.findElements(By.xpath("//p[@translate='text.FAVORITENAMEISREQUIRED']")).size() == 0;
		boolean minimum = driver.findElements(By.xpath("//p[@translate='text.FAVORITENAMESHOULDBEAT2']")).size() == 0;
		boolean maximum = driver.findElements(By.xpath("//p[@translate='text.FAVORITENAMESHOULDNOTMORE50']")).size() == 0;
		boolean actual;

		if(required && minimum && maximum)
		{
			actual = true;
		}
		else
		{
			actual = false;
		}
		Assert.assertEquals(actual, true);
		Thread.sleep(500);
		cancelBtnAddToFavorite.click();
	}

	public void verifyAddToFavoriteLabel() throws Exception {
		Thread.sleep(1000);
		Assert.assertEquals(addToFavoriteLabel.getText(), "Add To Favorite");
		cancelBtnAddToFavorite.click();
		Thread.sleep(500);
	}

	public void clickCloseIconAddToFavorite() throws Exception {
		closeIconAddToFavorite.click();
		Thread.sleep(500);
	}

	public void verifyCloseIconFunctionalityOfAddToFavorite() throws Exception {
		boolean actual = addToFavoriteBtn.isEnabled();
		Assert.assertEquals(actual, true);
	}

	public void clickCancelBtnAddToFavorite() throws Exception {
		cancelBtnAddToFavorite.click();
		Thread.sleep(500);
	}

	public void verifyCancelBtnFunctionalityOfAddToFavorite() throws Exception {
		boolean actual = addToFavoriteBtn.isEnabled();
		Assert.assertEquals(actual, true);
	}

	public void verifyMyFavoriteRadioSelectedByDefault() throws Exception {
		boolean actual = addToFavoriteRadioBtn.isSelected();
		cancelBtnAddToFavorite.click();
		Assert.assertEquals(actual, true);
	}

	public void verifyMyFavoriteSaveBtnColor() throws Exception {
		Assert.assertEquals(helper.BtnColor(addToFavoriteSaveBtn), "#5cb85c");
		cancelBtnAddToFavorite.click();
		Thread.sleep(500);
	}

	public void verifyMyFavoriteCancelBtnColor() throws Exception {
		Assert.assertEquals(helper.BtnColor(cancelBtnAddToFavorite), "#5a5a5a");
		cancelBtnAddToFavorite.click();
		Thread.sleep(500);
	}

	public void clickMyFavoriteSaveBtn() throws Exception {
		addToFavoriteSaveBtn.click();
	}

	public void verifySavedFavoriteDisplayInListings(String getVal) throws Exception {
		String actual = favoriteSavedSuccess.getText();
		Assert.assertEquals(actual, getVal);
		Thread.sleep(1000);
	}

	public void enterAddToFavoriteDuplicate(String duplicateChar) throws Exception {
		addToFavoriteTextfield.clear();
		addToFavoriteTextfield.sendKeys(duplicateChar);
		Thread.sleep(1000);
	}

	public void verifyMyFavoriteUniqueMsg() throws Exception {
		Assert.assertEquals(addToFavoriteUniqueMsg.getText(), "Favorite Name must be unique.");
		cancelBtnAddToFavorite.click();
	}

	public void clickRemoveIconMyFavorites() throws Exception {
		Thread.sleep(500);
		clickMyFavoriteTable.click();
		Thread.sleep(500);
		removeFromMyFavorite.click();
	}

	public void verifyRemoveMsgMyFavorites(String getText) throws Exception {
		Assert.assertEquals(removeMsgMyFavorite.getText(), "Are you sure you want to remove "+ getText +"?");
		Thread.sleep(250);
		removeYesMsgMyFavorite.click();
	}

	public void verifyMyFavoriteRemoveYesBtnColor() throws Exception {
		Assert.assertEquals(helper.BtnColor(removeYesMsgMyFavorite), "#5cb85c");
		Thread.sleep(250);
		removeYesMsgMyFavorite.click();
	}

	public void verifyMyFavoriteRemoveNoBtnColor() throws Exception {
		Assert.assertEquals(helper.BtnColor(removeNoMsgMyFavorite), "#d9534f");
		Thread.sleep(500);
		removeYesMsgMyFavorite.click();
	}

	public void clickMyFavoriteRemoveNoBtn() throws Exception {
		removeNoMsgMyFavorite.click();
		Thread.sleep(500);
	}

	public void verifyMyFavoriteRemoveNoBtnClick(String duplicate) throws Exception {
		Assert.assertEquals(favoriteSavedSuccess.getText(), duplicate);
	}

	public void verifySavedDataInMyFavorite(String duplicate) throws Exception {
		Assert.assertEquals(favoriteSavedSuccess.getText(), duplicate);		
		Thread.sleep(500);
		removeFromMyFavorite.click();
		Thread.sleep(500);
		removeYesMsgMyFavorite.click();
	}

	public void verifyFilterByLabel() throws Exception {
		Assert.assertEquals(filterByLabel.getText(), "Filter By");
	}

	public void verifyFilterByIconsVisible() throws Exception {
		Assert.assertEquals(filterByIcons.getText(), "Date Range\n" + 
				"Attributes/Entity\n" + 
				"Sites\n" + 
				"Batch Entries\n" + 
				"Assignee\n" + 
				"Record Status");
	}

	public void verifyDateRangeSpelling() throws Exception {
		Assert.assertEquals(dateRangeSpelling.getText(), "Date Range");
	}

	public void verifyAttributesEntitiesSpelling() throws Exception {
		Assert.assertEquals(attributes_EntitiesSpelling.getText(), "Attributes/Entity");
	}

	public void verifySitesSpelling() throws Exception {
		Assert.assertEquals(sitesSpelling.getText(), "Sites");
	}

	public void verifyBatchEntriesSpelling() throws Exception {
		Assert.assertEquals(batchEntriesSpelling.getText(), "Batch Entries");
	}

	public void verifyAssigneeSpelling() throws Exception {
		Assert.assertEquals(assigneeSpelling.getText(), "Assignee");
	}

	public void verifyRecordStatusSpelling() throws Exception {
		Assert.assertEquals(recordStatusSpelling.getText(), "Record Status");
	}

	public void selectTodaysDateFilterBy() throws Exception {
		helper.SelectDrpDwnValueByText(dateDrpDwnFilterBy, "Today");
	}

	public void verifyTodaysDateFilterBy() throws Exception {
		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		Date date = new Date();
		String systemDate = dateFormat.format(date);
		System.out.println("System Date: "+ systemDate);
		String currentDate = helper.getClipboardContents(getDateFilterBy);
		System.out.println("Current Date: "+ currentDate);
		Assert.assertEquals(systemDate, currentDate);
	}

	public void selectThisWeekDateFilterBy() throws Exception {
		helper.SelectDrpDwnValueByText(dateDrpDwnFilterBy, "This Week");
	}

	public void verifyThisWeekFilterBy() throws Exception {
		String expectedSunday = helper.weekDateOfSunday(); 
		String expectedSaturday = helper.weekDateOfSaturday();
		String actualSunday = helper.getClipboardContents(getStartDateFilterBy);
		String actualSaturday = helper.getClipboardContents(getEndDateFilterBy);
		Assert.assertEquals(actualSunday + " " + actualSaturday, expectedSunday + " " + expectedSaturday);
	}

	public void verifyThisWeekFilterByByDefault() throws Exception {
		boolean actual = dateRangeByDefault.isSelected();
		Assert.assertEquals(actual, true);
	}

	public void selectThisMonthFilterBy() throws Exception {
		helper.SelectDrpDwnValueByText(dateDrpDwnFilterBy, "This Month");
	}

	public void verifyThisMonthFilterBy() throws Exception {
		String expectedSunday = helper.thisMonthStartDate(); 
		String expectedSaturday = helper.thisMonthEndDate();
		String actualSunday = helper.getClipboardContents(getStartDateFilterBy);
		String actualSaturday = helper.getClipboardContents(getEndDateFilterBy);
		Assert.assertEquals(actualSunday + " " + actualSaturday, expectedSunday + " " + expectedSaturday);
	}

	public void verifyThisMonthFilterByByDefault() throws Exception {
		boolean actual = dateRangeByDefault.isSelected();
		Assert.assertEquals(actual, true);
	}

	public void verifyClearLinkVisibleInDateRange() throws Exception {
		boolean actual = clearLinkFilterBy.isDisplayed();
		Assert.assertEquals(actual, true);
	}

	public void verifyClearLinkVisibleInDateRangeColor() throws Exception {
		Assert.assertEquals(helper.FontColor(clearLinkFilterBy), "#337ab7");
	}

	public void clickClearLinkFilterBy() throws Exception {
		clearLinkFilterBy.click();
	}

	public void verifyAllFieldIsCleared() throws Exception {
		boolean actual = dateRangeByDefault.isSelected();
		Assert.assertEquals(actual, false);
	}

	public void verifyRemoveIconIsVisible() throws Exception {
		boolean actual = removeIconFilterBy.isDisplayed();
		Assert.assertEquals(actual, true);
	}

	public void clickMonthYearDrpdwn() throws Exception {
		Thread.sleep(500);
		monthYearDrpDwn.click();
	}

	public void verifyMonthYearDrpdwnIsVisible() throws Exception {
		boolean monthDrpDwn = selectMonthDrpDwn.isDisplayed();
		boolean yearDrpDwn = selectYearDrpDwn.isDisplayed();
		boolean flag;
		if(monthDrpDwn == true && yearDrpDwn == true)
		{
			flag = true;
		}
		else {
			flag = false;
		}
		Assert.assertEquals(flag, true);
	}

	public void verifyRemoveIconColor() throws Exception {
		Assert.assertEquals(helper.BtnColor(removeIconFilterBy), "#000000");
	}

	public void searchForThisWeekApp(String thisWeek) throws Exception {
		searchAppTextfield.clear();
		searchAppTextfield.sendKeys(thisWeek);
	}

	public void VerifySearchForThisWeekApp() throws Exception {
		goBtn.click();
		
		String nextBtnColor, startDate, endDate;
		int i = 0, count = 0, add = 0;

		List<org.openqa.selenium.WebElement> inList = new ArrayList<WebElement>();
		List<String> temp = new ArrayList<String>();

		startDate = helper.getClipboardContents(getStartDateFilterBy);
		endDate = helper.getClipboardContents(getEndDateFilterBy);

		// String todayDate="10/24/2018";

		// checking if there are more than 10 records is display
		if (driver.findElements(By.xpath("//*[@ng-click='selectPage(page + 1, $event)']"))
				.size() != 0 == true) {
			System.out.println("found more than 10 records");

			for (i = 0; i < 100; i++) {

				// this line is storing a hex code of next button.
				nextBtnColor = helper.FontColorCodeHex(nextPagging);

				// this line is taking a "Created Date column" all value.
				inList = (ArrayList<org.openqa.selenium.WebElement>) driver.findElements(By.xpath("//*[@data-th='Created Date']"));
				try {
					for (add = 0; add < inList.size(); add++) {
						temp.add(count, inList.get(add).getText());
						count++;
					}

					System.out.println(i + "-grid list: " + temp.size());
					System.out.println("--------------------------------------");
					System.out.println(Arrays.deepToString(temp.toArray()));
					System.out.println("--------------------------------------");

				} catch (Exception e) {
					System.out.println(e);
				}
				nextPagging.click();
				if (nextBtnColor.equalsIgnoreCase("#777777")) {
					break;
				}
			}
		} else {
			inList = (ArrayList<org.openqa.selenium.WebElement>) driver.findElements(By.xpath("//*[@data-th='Created Date']"));

			for (add = 0; add < inList.size(); add++) {
				temp.add(count, inList.get(add).getText());
				count++;

				System.out.println(i + "-grid list: " + temp.size());
				System.out.println("--------------------------------------");
				System.out.println(Arrays.deepToString(temp.toArray()));
				System.out.println("--------------------------------------");
			}
		}

		String[] outArray = new String[temp.size()]; // instantiate Array

		int actualMatchedRecords = 0;
		int expectedMatchedRecords = temp.size();

		for (int j = 0; j < temp.size(); j++) {
			outArray[j] = temp.get(j);

			String Str = outArray[j];

			// slipting date and time
			String[] arrOfStr = Str.split(" ");
			// System.out.println("date: "+arrOfStr[0]);

			SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
			Date srtDate = sdf.parse(startDate);
			Date enDate = sdf.parse(endDate);
			Date foundDate = sdf.parse(arrOfStr[0]);

			if (foundDate.after(srtDate) && foundDate.before(enDate)) {
				actualMatchedRecords++;
				System.out.println("--------------------------------");
				System.out.println("Record Matched                |");
				System.out.println("actual: " + arrOfStr[0] + "\nexpected: " + "\nStart Date: " + startDate
						+ "\nEnd Date: " + endDate);
			} else if (foundDate.equals(srtDate) || foundDate.equals(enDate)) {
				{
					actualMatchedRecords++;
					System.out.println("--------------------------------");
					System.out.println("Record Matched                |");
					System.out.println("actual: " + arrOfStr[0] + "\nexpected: " + "\nStart Date: " + startDate
							+ "\nEnd Date: " + endDate);
				}
			} else {
				System.out.println("--------------------------------");
				System.out.println("Record Does't Matched          |");
				System.out.println("actual: " + arrOfStr[0] + "\nexpected: " + "\nStart Date: " + startDate
						+ "\nEnd Date: " + endDate);
			}
			System.out.println("--------------------------------");

		}
		System.out.println("Actual no of record is: " + actualMatchedRecords);
		System.out.println("Total no of record is: " + expectedMatchedRecords);
		Assert.assertEquals(actualMatchedRecords, expectedMatchedRecords);
	}

	public void searchForTodayApp(String today) throws Exception {
		searchAppTextfield.clear();
		searchAppTextfield.sendKeys(today);
	}

	public void verifySearchForTodayApp() throws Exception {

		goBtn.click();
		Thread.sleep(500);

		String nextBtnColor;
		int i = 0, count = 0, add = 0;

		List<org.openqa.selenium.WebElement> inList = new ArrayList<WebElement>();
		List<String> temp = new ArrayList<String>();

		String todayDate = helper.getClipboardContents(getDateFilterBy);
		// String todayDate="10/24/2018";

		// checking if there are more than 10 records is display
		if (driver.findElements(By.xpath("//*[@ng-click='selectPage(page + 1, $event)']"))
				.size() != 0 == true) {
			System.out.println("found more than 10 records");

			for (i = 0; i < 100; i++) {

				// this line is storing a hex code of next button.
				nextBtnColor = helper.FontColorCodeHex(nextPagging);

				// this line is taking a "Created Date column" all value.
				inList = (ArrayList<org.openqa.selenium.WebElement>) driver.findElements(By.xpath("//*[@data-th='Created Date']"));

				try {

					for (add = 0; add < inList.size(); add++) {
						temp.add(count, inList.get(add).getText());
						count++;
					}

					System.out.println(i + "-grid list: " + temp.size());
					System.out.println("--------------------------------------");
					System.out.println(Arrays.deepToString(temp.toArray()));
					System.out.println("--------------------------------------");

				} catch (Exception e) {
					System.out.println(e);
				}
				nextPagging.click();
				Thread.sleep(1000);

				if (nextBtnColor.equalsIgnoreCase("#777777")) {
					break;
				}
			}
		} else {
			inList = (ArrayList<org.openqa.selenium.WebElement>) driver.findElements(By.xpath("//*[@data-th='Created Date']"));

			for (add = 0; add < inList.size(); add++) {
				temp.add(count, inList.get(add).getText());
				count++;

				System.out.println(i + "-grid list: " + temp.size());
				System.out.println("--------------------------------------");
				System.out.println(Arrays.deepToString(temp.toArray()));
				System.out.println("--------------------------------------");
			}
		}

		String[] outArray = new String[temp.size()]; // instantiate Array

		int actualMatchedRecords = 0;
		int expectedMatchedRecords = temp.size();

		for (int j = 0; j < temp.size(); j++) {
			outArray[j] = temp.get(j);

			String Str = outArray[j];

			// slipting date and time
			String[] arrOfStr = Str.split(" ");

			if (arrOfStr[0].equalsIgnoreCase(todayDate)) {
				actualMatchedRecords++;
				System.out.println("--------------------------------");
				System.out.println("Record Matched                |");
				System.out.println("actual: " + arrOfStr[0] + "\nexpected: " + todayDate);

			} else {
				System.out.println("--------------------------------");
				System.out.println("Record Does't Matched          |");
				System.out.println("actual: " + arrOfStr[0] + "\nexpected: " + todayDate);
			}
			System.out.println("--------------------------------");

		}
		System.out.println("Actual no of record is: " + actualMatchedRecords);
		System.out.println("Total no of record is: " + expectedMatchedRecords);

		Assert.assertEquals(actualMatchedRecords, expectedMatchedRecords);
	}

	public void searchForThisMonthApp(String thisMonth) throws Exception {
		searchAppTextfield.clear();
		searchAppTextfield.sendKeys(thisMonth);
	}

	public void verifySearchForThisMonthApp() throws Exception {
		goBtn.click();
		
		String nextBtnColor, startDate, endDate;
		int i = 0, count = 0, add = 0;

		List<org.openqa.selenium.WebElement> inList = new ArrayList<WebElement>();
		List<String> temp = new ArrayList<String>();
		startDate = helper.getClipboardContents(getStartDateFilterBy);
		endDate = helper.getClipboardContents(getEndDateFilterBy);

		// checking if there are more than 10 records is display
		if (driver.findElements(By.xpath("//*[@ng-click='selectPage(page + 1, $event)']"))
				.size() != 0 == true) {
			System.out.println("found more than 10 records");

			for (i = 0; i < 100; i++) {

				// this line is storing a hex code of next button.
				nextBtnColor = helper.FontColorCodeHex(nextPagging);

				// this line is taking a "Created Date column" all value.
				inList = (ArrayList<org.openqa.selenium.WebElement>) driver.findElements(By.xpath("//*[@data-th='Created Date']"));

				try {

					for (add = 0; add < inList.size(); add++) {
						temp.add(count, inList.get(add).getText());
						count++;
					}

					System.out.println(i + "-grid list: " + temp.size());
					System.out.println("--------------------------------------");
					System.out.println(Arrays.deepToString(temp.toArray()));
					System.out.println("--------------------------------------");

				} catch (Exception e) {
					System.out.println(e);
				}
				nextPagging.click();
				Thread.sleep(1000);

				if (nextBtnColor.equalsIgnoreCase("#777777")) {
					break;
				}
			}
		} else {
			inList = (ArrayList<org.openqa.selenium.WebElement>) driver.findElements(By.xpath("//*[@data-th='Created Date']"));

			for (add = 0; add < inList.size(); add++) {
				temp.add(count, inList.get(add).getText());
				count++;

				System.out.println(i + "-grid list: " + temp.size());
				System.out.println("--------------------------------------");
				System.out.println(Arrays.deepToString(temp.toArray()));
				System.out.println("--------------------------------------");
			}
		}

		String[] outArray = new String[temp.size()]; // instantiate Array
		int actualMatchedRecords = 0;
		int expectedMatchedRecords = temp.size();

		for (int j = 0; j < temp.size(); j++) {
			outArray[j] = temp.get(j);
			String Str = outArray[j];
			// slipting date and time
			String[] arrOfStr = Str.split(" ");
			SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
			Date srtDate = sdf.parse(startDate);
			Date enDate = sdf.parse(endDate);
			Date foundDate = sdf.parse(arrOfStr[0]);

			if (foundDate.after(srtDate) && foundDate.before(enDate)) {
				actualMatchedRecords++;
				System.out.println("--------------------------------");
				System.out.println("Record Matched                |");
				System.out.println("actual: " + arrOfStr[0] + "\nexpected: " + "\nStart Date: " + startDate
						+ "\nEnd Date: " + endDate);

			} else if (foundDate.equals(srtDate) || foundDate.equals(enDate)) {
				{
					actualMatchedRecords++;
					System.out.println("--------------------------------");
					System.out.println("Record Matched                |");
					System.out.println("actual: " + arrOfStr[0] + "\nexpected: " + "\nStart Date: " + startDate
							+ "\nEnd Date: " + endDate);
				}
			} else {
				System.out.println("--------------------------------");
				System.out.println("Record Does't Matched          |");
				System.out.println("actual: " + arrOfStr[0] + "\nexpected: " + "\nStart Date: " + startDate
						+ "\nEnd Date: " + endDate);
			}
			System.out.println("--------------------------------");
		}
		System.out.println("Actual no of record is: " + actualMatchedRecords);
		System.out.println("Total no of record is: " + expectedMatchedRecords);

		Assert.assertEquals(actualMatchedRecords, expectedMatchedRecords);
	}

	public void selectSpecificDateFromDrpDwn() throws Exception {
		helper.SelectDrpDwnValueByText(dateDrpDwnFilterBy, "Specific Date");
	}

	public void selectSpecificDate() throws Exception {
		specificDateIcon.click();
		Thread.sleep(500);
		selectSpecificDate.click();
	}

	public void verifyBySelectSpecificDate() throws Exception {

		goBtn.click();
		Thread.sleep(500);

		String nextBtnColor;
		int i = 0, count = 0, add = 0;

		List<org.openqa.selenium.WebElement> inList = new ArrayList<WebElement>();
		List<String> temp = new ArrayList<String>();

		String todayDate = helper.getClipboardContents(getDateFilterBy);
		// String todayDate="10/24/2018";

		// checking if there are more than 10 records is display
		if (driver.findElements(By.xpath("//*[@ng-click='selectPage(page + 1, $event)']"))
				.size() != 0 == true) {
			System.out.println("found more than 10 records");

			for (i = 0; i < 100; i++) {

				// this line is storing a hex code of next button.
				nextBtnColor = helper.FontColorCodeHex(nextPagging);

				// this line is taking a "Created Date column" all value.
				inList = (ArrayList<org.openqa.selenium.WebElement>) driver.findElements(By.xpath("//*[@data-th='Created Date']"));

				try {

					for (add = 0; add < inList.size(); add++) {
						temp.add(count, inList.get(add).getText());
						count++;
					}

					System.out.println(i + "-grid list: " + temp.size());
					System.out.println("--------------------------------------");
					System.out.println(Arrays.deepToString(temp.toArray()));
					System.out.println("--------------------------------------");

				} catch (Exception e) {
					System.out.println(e);
				}
				nextPagging.click();
				Thread.sleep(1000);

				if (nextBtnColor.equalsIgnoreCase("#777777")) {
					break;
				}
			}
		} else {
			inList = (ArrayList<org.openqa.selenium.WebElement>) driver.findElements(By.xpath("//*[@data-th='Created Date']"));

			for (add = 0; add < inList.size(); add++) {
				temp.add(count, inList.get(add).getText());
				count++;

				System.out.println(i + "-grid list: " + temp.size());
				System.out.println("--------------------------------------");
				System.out.println(Arrays.deepToString(temp.toArray()));
				System.out.println("--------------------------------------");
			}
		}

		String[] outArray = new String[temp.size()]; // instantiate Array

		int actualMatchedRecords = 0;
		int expectedMatchedRecords = temp.size();

		for (int j = 0; j < temp.size(); j++) {
			outArray[j] = temp.get(j);

			String Str = outArray[j];

			// slipting date and time
			String[] arrOfStr = Str.split(" ");

			if (arrOfStr[0].equalsIgnoreCase(todayDate)) {
				actualMatchedRecords++;
				System.out.println("--------------------------------");
				System.out.println("Record Matched                |");
				System.out.println("actual: " + arrOfStr[0] + "\nexpected: " + todayDate);

			} else {
				System.out.println("--------------------------------");
				System.out.println("Record Does't Matched          |");
				System.out.println("actual: " + arrOfStr[0] + "\nexpected: " + todayDate);
			}
			System.out.println("--------------------------------");

		}
		System.out.println("Actual no of record is: " + actualMatchedRecords);
		System.out.println("Total no of record is: " + expectedMatchedRecords);

		Assert.assertEquals(actualMatchedRecords, expectedMatchedRecords);
	}

	public void selectMonthFromDrpDwn(String selectMonth) throws Exception {
		helper.SelectDrpDwnValueByText(selectMonthDrpDwn, selectMonth);
	}

	public void verifyMonthRequiredMsg() throws Exception {
		Assert.assertEquals(monthDrpDwnRequiredMsg.getText(), "Month is required.");
	}

	public void verifyMonthRequiredMsgColor() throws Exception {
		Assert.assertEquals(helper.FontColor(monthDrpDwnRequiredMsg), "#a94442");
	}
	
	public void verifyYearRequiredMsg() throws Exception {
		Assert.assertEquals(yearDrpDwnRequiredMsg.getText(), "Year is required.");
	}

	public void verifyYearRequiredMsgColor() throws Exception {
		Assert.assertEquals(helper.FontColor(yearDrpDwnRequiredMsg), "#a94442");
	}

	public void selectYearFromDrpDwn(String selectYear) throws Exception {
		helper.SelectDrpDwnValueByText(selectYearDrpDwn, selectYear);
	}

	public void verifyMonthYearProperData(String selectMonth, String selectYear) throws Exception {
		goBtn.click();

		selectMonthFromDrpDwn(selectMonth);
		selectYearFromDrpDwn(selectYear);

		String monthInput = helper.getDrpDwnFirstSelectedOptn(selectMonthDrpDwn);
		String yearInput = helper.getDrpDwnFirstSelectedOptn(selectYearDrpDwn);

		String nextBtnColor;
		int i = 0, count = 0, add = 0;

		List<org.openqa.selenium.WebElement> inList = new ArrayList<WebElement>();
		List<String> temp = new ArrayList<String>();

		// checking if there are more than 10 records is display
		if (driver.findElements(By.xpath("//*[@ng-click='selectPage(page + 1, $event)']"))
				.size() != 0 == true) {
			System.out.println("found more than 10 records");

			for (i = 0; i < 100; i++) {

				// this line is storing a hex code of next button.
				nextBtnColor = helper.FontColorCodeHex(nextPagging);

				// this line is taking a "Created Date column" all value.
				inList = (ArrayList<org.openqa.selenium.WebElement>) driver.findElements(By.xpath("//*[@data-th='Created Date']"));
				try {

					for (add = 0; add < inList.size(); add++) {
						temp.add(count, inList.get(add).getText());
						count++;
					}
					System.out.println(i + "-grid list: " + temp.size());
					System.out.println("--------------------------------------");
					System.out.println(Arrays.deepToString(temp.toArray()));
					System.out.println("--------------------------------------");

				} catch (Exception e) {
					System.out.println(e);
				}
				nextPagging.click();
				Thread.sleep(1000);

				if (nextBtnColor.equalsIgnoreCase("#777777")) {
					break;
				}
			}
		} else {
			inList = (ArrayList<org.openqa.selenium.WebElement>) driver.findElements(By.xpath("//*[@data-th='Created Date']"));

			for (add = 0; add < inList.size(); add++) {
				temp.add(count, inList.get(add).getText());
				count++;

				System.out.println(i + "-grid list: " + temp.size());
				System.out.println("--------------------------------------");
				System.out.println(Arrays.deepToString(temp.toArray()));
				System.out.println("--------------------------------------");
			}
		}

		String[] outArray = new String[temp.size()]; // instantiate Array

		int actualMatchedRecords = 1;
		int expectedMatchedRecords = temp.size();
		System.out.println("Temvferfdfffffffffff: "+ temp.size());
		for (int j = 0; j < temp.size(); j++) {
			outArray[j] = temp.get(j);

			String Str = outArray[j];

			// slipting date and time
			String[] arrOfStr = Str.split(" ");
			System.out.println("split1: " + Arrays.toString(arrOfStr));
			String[] dateToSplit = arrOfStr[j].split("/");

			System.out.println("split1: " + dateToSplit);
			System.out.println("month: " + dateToSplit[0]);
			System.out.println("year: " + dateToSplit[2]);

			String monthNo = getMonthNo(monthInput);

			if (dateToSplit[0].equalsIgnoreCase(monthNo) && dateToSplit[2].equalsIgnoreCase(yearInput)) {
				actualMatchedRecords++;
				System.out.println("--------------------------------");
				System.out.println("Record Matched                |");
				System.out.println("actual month and year: " + dateToSplit[0] + " / " + dateToSplit[2] + "\nexpected: "
						+ monthNo + " / " + yearInput);
			} else {
				System.out.println("--------------------------------");
				System.out.println("Record Does't Matched          |");
				System.out.println("actual month and year: " + dateToSplit[0] + " / " + dateToSplit[2] + "\nexpected: "
						+ monthNo + " / " + yearInput);
			}
			System.out.println("--------------------------------");

		}
		System.out.println("Actual no of record is: " + actualMatchedRecords);
		System.out.println("Total no of record is: " + expectedMatchedRecords);

		Assert.assertEquals(actualMatchedRecords, expectedMatchedRecords);
	}

	static public String getMonthNo(String monthName) {
		String monthNo = "0";

		switch (monthName) {
		case "January":
			monthNo = "01";
			break;
		case "February":
			monthNo = "02";
			break;
		case "March":
			monthNo = "03";
			break;
		case "April":
			monthNo = "04";
			break;
		case "May":
			monthNo = "05";
			break;
		case "June":
			monthNo = "06";
			break;
		case "July":
			monthNo = "07";
			break;
		case "August":
			monthNo = "08";
			break;
		case "September":
			monthNo = "09";
			break;
		case "October":
			monthNo = "10";
			break;
		case "November":
			monthNo = "11";
			break;
		case "December":
			monthNo = "12";
			break;
		default:
			System.out.println("Invalid input");
			break;
		}
		return monthNo;
	}

	public void attributesLabel() throws Exception {
		Assert.assertEquals(attributes_EntitiesSpelling.getText(), "Attributes/Entity");
	}

	public void clickAttributesIcon() throws Exception {
		attributes_EntitiesSpelling.click();
	}

	public void selectKeyAttributesDrpDwn(String selectAttributes) throws Exception {
		helper.SelectDrpDwnValueByText(attributeDrpDwn, selectAttributes);
	}

	public void verifyValueRequiredMsg() throws Exception {
		String actual = valueRequiredMsg.getText();
		String expected = "Red Blood - 2 - ID is required.";
		Assert.assertEquals(actual, expected);
	}

	public void verifyValueRequiredMsgColor() throws Exception {
		Assert.assertEquals(helper.FontColor(valueRequiredMsg), "#a94442");
	}

	public void verifyAddAttributeIcon() throws Exception {
		boolean actual = addAttribute.isDisplayed();
		Assert.assertEquals(actual, true);
	}

	public void verifyAddAttributeIconColor() throws Exception {
		Assert.assertEquals(helper.BtnColor(addAttribute), "#5cb85c");
	}

	public void selectAttributeValue(String value) throws Exception {
		helper.SelectDrpDwnValueByText(attributeValue, value);
	}

	public void clickAddAttributes() throws Exception {
		addAttribute.click();
	}

	public void verifyAttributeFilterData() throws Exception {
		List<WebElement> count = ObjectRepo.driver.findElements(By.xpath("//button[@ng-click='vm.removeAttribute($index)']"));
		Assert.assertEquals(count.size(), 2);
	}

	public void verifyRemoveIconVisible() throws Exception {
		boolean expected = attributesRemoveIcon.isDisplayed();
		Assert.assertEquals(true, expected);
	}

	public void removeIconColor() throws Exception {
		Assert.assertEquals(helper.BtnColor(attributesRemoveIcon), "#000000");
	}

	public void clickRemoveIcon() throws Exception {
		attributesRemoveIcon.click();
	}

	public void verifyRemoveIconFunctionality() throws Exception {
		boolean actual;
		if(driver.findElements(By.xpath("//select[@ng-model='vm.attribEtyFilter[$index].attrb']")).size() != 0)
		{
			actual = true;
		}
		else
		{
			actual = false;
		}
		Assert.assertEquals(actual, false);
	}

	public void verifyDataAccordingly(String attributeVal, String inputAttributeVal) throws Exception {

		String attributeValue = attributeVal;
		String inputValue = inputAttributeVal;

		String nextBtnColor;
		int i = 0, count = 0, add = 0;

		List<org.openqa.selenium.WebElement> inList = new ArrayList<WebElement>();
		List<String> temp = new ArrayList<String>();

		filterByLabel.click();

		// checking if there are more than 10 records is display
		if (driver.findElements(By.xpath("//*[@ng-click='selectPage(page + 1, $event)']"))
				.size() != 0 == true) {
			System.out.println("found more than 10 records");

			for (i = 0; i < 100; i++) {

				// this line is storing a hex code of next button.
				nextBtnColor = helper.FontColorCodeHex(nextPagging);

				// this line is taking a "Created Date column" all value.
				inList = (ArrayList<org.openqa.selenium.WebElement>) driver.findElements(By.xpath("//*[@data-th='" + attributeValue + "']"));

				try {

					for (add = 0; add < inList.size(); add++) {
						temp.add(count, inList.get(add).getText());
						count++;
					}

					System.out.println(i + "-grid list: " + temp.size());
					System.out.println("--------------------------------------");
					System.out.println(Arrays.deepToString(temp.toArray()));
					System.out.println("--------------------------------------");

				} catch (Exception e) {
					System.out.println(e);
				}
				nextPagging.click();
				Thread.sleep(1000);

				if (nextBtnColor.equalsIgnoreCase("#777777")) {
					break;
				}
			}
		} else {
			inList = (ArrayList<org.openqa.selenium.WebElement>) driver.findElements(By.xpath("//*[@data-th='" + attributeValue + "']"));

			for (add = 0; add < inList.size(); add++) {
				temp.add(count, inList.get(add).getText());
				count++;

				System.out.println(i + "-grid list: " + temp.size());
				System.out.println("--------------------------------------");
				System.out.println(Arrays.deepToString(temp.toArray()));
				System.out.println("--------------------------------------");
			}
		}

		String[] outArray = new String[temp.size()]; // instantiate Array

		int actualMatchedRecords = 0;
		int expectedMatchedRecords = temp.size();

		for (int j = 0; j < temp.size(); j++) {
			outArray[j] = temp.get(j);

			if (outArray[0].equalsIgnoreCase(inputValue)) {
				actualMatchedRecords++;
				System.out.println("--------------------------------");
				System.out.println("Record Matched                |");
				System.out.println("actual: " + outArray[0] + "\nexpected: " + inputValue);

			} else {
				System.out.println("--------------------------------");
				System.out.println("Record Does't Matched          |");
				System.out.println("actual: " + outArray[0] + "\nexpected: " + inputValue);

			}
			System.out.println("--------------------------------");

		}
		System.out.println("Actual no of record is: " + actualMatchedRecords);
		System.out.println("Total no of record is: " + expectedMatchedRecords);

		Assert.assertEquals(actualMatchedRecords, expectedMatchedRecords);

	}

	public void verifyClearLinkVisible() throws Exception {
		boolean link = attributesClearLink.isDisplayed();
		Assert.assertEquals(link, true);
	}

	public void verifyClearLinkVisibleColor() throws Exception {
		Assert.assertEquals(helper.BtnColor(attributesClearLink), "#000000");
	}

	public void clickAttributesClearLink() throws Exception {
		attributesClearLink.click();
	}

	public void verifyAttributesClearLinkFunctionality() throws Exception {
		String actual = attributeDrpDwn.getText();
		Assert.assertEquals(actual, "-- Please Select --\n" + 
				"Collection Date\n" + 
				"DIN\n" + 
				"Red Blood - 2 - ID");
	}

	public void verifyAttributeRemoveIconVisible() throws Exception {
		boolean actual = attributesRemoveIcon.isDisplayed();
		Assert.assertEquals(actual, true);
	}

	public void attributeRemoveIconColor() throws Exception {
		Assert.assertEquals(helper.BtnColor(attributesRemoveIcon), "#000000");
	}

	public void verifySitesLabel() throws Exception {
		Assert.assertEquals(sitesSpelling.getText(), "Sites");
	}

	public void clickOnSites() throws Exception {
		sitesSpelling.click();
	}

	public void verifySitesClearLink() throws Exception {
		boolean actual = sitesClearLink.isDisplayed();
		Assert.assertEquals(actual, true);
	}

	public void verifySitesClearLinkColor() throws Exception {
		Assert.assertEquals(helper.FontColor(sitesClearLink), "#337ab7");
	}

	public void selectSitesDrpDwn() throws Exception {
		helper.SelectDrpDwnValueByText(sitesRegionDrpDwn, "MD Anderson Cancer Center - Updated");
	}

	public void clickSitesClearLink() throws Exception {
		sitesClearLink.click();
	}

	public void verifySitesClearLinkFunctionality() throws Exception {
		String actual = sitesRegionDrpDwn.getText();
		Assert.assertEquals(actual, "-- Select Region --\n" + 
				"MD Anderson Cancer Center - Updated");
	}

	public void verifySitesRemoveIconVisible() throws Exception {
		boolean actual = sitesRemoveIcon.isDisplayed();
		Assert.assertEquals(actual, true);
	}

	public void verifySitesRemoveIconColor() throws Exception {
		Assert.assertEquals(helper.BtnColor(sitesRemoveIcon), "#000000");
	}

	public void clickSitesRemoveIcon() throws Exception {
		sitesRemoveIcon.click();
	}

	public void verifySitesRemoveIconFunctionality() throws Exception {
		boolean actual;
		if(driver.findElements(By.xpath("//select[@title='Please select Region']")).size() != 0)
		{
			actual = false;
		}else {
			actual = true;
		}
		Assert.assertEquals(actual, true);
	}

	public void verifyBatchEntryLabel() throws Exception {
		Assert.assertEquals(batchEntriesSpelling.getText(), "Batch Entries");
	}

	public void clickBatchEntries() throws Exception {
		batchEntriesSpelling.click();
	}

	public void enterTwentyCharBatchEntriesField(String maxChar) throws Exception {
		batchEntrytextfield.clear();
		batchEntrytextfield.sendKeys(maxChar);
	}

	public void verifyBatchEntiesMaximimMsg() throws Exception {
		Assert.assertEquals(batchEntryMaximumMsg.getText(), "Batch Entry ID must not be more than 20 characters.");
	}

	public void verifyBatchEntiesMaximimMsgColor() throws Exception {
		Assert.assertEquals(helper.FontColor(batchEntryMaximumMsg), "#a94442");
	}

	public void clickBatchEntiesField() throws Exception {
		batchEntrytextfield.click();
	}

	public void enterBatchEntiesValidChar(String validChar) throws Exception {
		batchEntrytextfield.clear();
		batchEntrytextfield.sendKeys(validChar);
	}

	public void verifyBatchEntitiesSearching(String validBatchEntry) throws Exception {
		goBtn.click();
		String inputValue = validBatchEntry;
		String batchInputValue = inputValue;
		String nextBtnColor;
		int i = 0, count = 0, add = 0;

		List<org.openqa.selenium.WebElement> inList = new ArrayList<WebElement>();
		List<String> temp = new ArrayList<String>();

		// checking if there are more than 10 records is display
		if (driver.findElements(By.xpath("//*[@ng-click='selectPage(page + 1, $event)']"))
				.size() != 0 == true) {
			System.out.println("found more than 10 records");

			for (i = 0; i < 100; i++) {

				// this line is storing a hex code of next button.
				nextBtnColor = helper.FontColorCodeHex(nextPagging);

				// this line is taking a "Created Date column" all value.
				inList = (ArrayList<org.openqa.selenium.WebElement>) driver.findElements(By.xpath("//*[@data-th='Batch Entry ID']"));

				try {

					for (add = 0; add < inList.size(); add++) {
						temp.add(count, inList.get(add).getText());
						count++;
					}

					System.out.println(i + "-grid list: " + temp.size());
					System.out.println("--------------------------------------");
					System.out.println(Arrays.deepToString(temp.toArray()));
					System.out.println("--------------------------------------");

				} catch (Exception e) {
					System.out.println(e);
				}
				nextPagging.click();
				Thread.sleep(1000);

				if (nextBtnColor.equalsIgnoreCase("#777777")) {
					break;
				}
			}
		} else {
			inList = (ArrayList<org.openqa.selenium.WebElement>) driver.findElements(By.xpath("//*[@data-th='Batch Entry ID']"));

			for (add = 0; add < inList.size(); add++) {
				temp.add(count, inList.get(add).getText());
				count++;

				System.out.println(i + "-grid list: " + temp.size());
				System.out.println("--------------------------------------");
				System.out.println(Arrays.deepToString(temp.toArray()));
				System.out.println("--------------------------------------");
			}
		}

		String[] outArray = new String[temp.size()]; // instantiate Array

		int actualMatchedRecords = 0;
		int expectedMatchedRecords = temp.size();

		for (int j = 0; j < temp.size(); j++) {
			outArray[j] = temp.get(j);

			if (outArray[0].equalsIgnoreCase(batchInputValue)) {
				actualMatchedRecords++;
				System.out.println("--------------------------------");
				System.out.println("Record Matched                |");
				System.out.println("actual: " + outArray[0] + "\nexpected: " + batchInputValue);

			} else {
				System.out.println("--------------------------------");
				System.out.println("Record Does't Matched          |");
				System.out.println("actual: " + outArray[0] + "\nexpected: " + batchInputValue);

			}
			System.out.println("--------------------------------");

		}
		System.out.println("Actual no of record is: " + actualMatchedRecords);
		System.out.println("Total no of record is: " + expectedMatchedRecords);

		Assert.assertEquals(actualMatchedRecords, expectedMatchedRecords);
	}

	public void verifyBatchEntryClearLinkVisible() throws Exception {
		boolean actual = batchEntryClearLink.isDisplayed();
		Assert.assertEquals(actual, true);
	}

	public void verifyBatchEntryClearLinkColor() throws Exception {
		Assert.assertEquals(helper.FontColor(batchEntryClearLink), "#337ab7");
	}

	public void clickBatchEntryClearLink() throws Exception {
		batchEntryClearLink.click();
	}

	public void verifyBatchEntryClearLinkFunctionality() throws Exception {
		String actual = batchEntrytextfield.getText();
		Assert.assertEquals(actual, "");
	}

	public void verifyBatchEntryRemoveIconVisible() throws Exception {
		boolean actual = batchEntryRemoveIcon.isDisplayed();
		Assert.assertEquals(actual, true);
	}

	public void verifyBatchEntryRemoveIconColor() throws Exception {
		Assert.assertEquals(helper.BtnColor(batchEntryRemoveIcon), "#000000");
	}

	public void clickBatchEntryRemoveIcon() throws Exception {
		batchEntryRemoveIcon.click();
	}

	public void verifyBatchEntryRemoveIconFunctionality() throws Exception {
		boolean actual;
		if(driver.findElements(By.xpath("//input[@name='batchEntryId']")).size() != 0)
		{
			actual = false;
		}
		else{
			actual = true;
		}
		Assert.assertEquals(actual, true);
	}

	public void clickAssigneeIcon() throws Exception {
		assigneeSpelling.click();
	}

	public void verifyAssigneeRadioBtnVisible() throws Exception {
		boolean actual = assigneeRadioBtn.isDisplayed();
		Assert.assertEquals(actual, true);
	}

	public void verifyAssigneeToMeRadioBtnVisible() throws Exception {
		boolean actual = assigneeToMeRadioBtn.isDisplayed();
		Assert.assertEquals(actual, true);
	}

	public void verifyAssigneeRadioBtnByDefaultSelected() throws Exception {
		/*boolean actual = assigneeRadioBtn.isSelected();
		Assert.assertEquals(actual, true);*/

		boolean actual = true;
		if(driver.findElements(By.xpath("//input[@id='test11' and @aria-checked='true']")).size() != 0)
		{
			actual = true;
		} else{
			actual = false;
		}
		Assert.assertEquals(actual, true);
	}

	public void clickRoleDrpDwnVal() throws Exception {
		assigneeRoleDrpdwn.click();
	}

	public void clickCheckAllRoleDrpDwnVal() throws Exception {
		checkAllRoleDrpdwn.click();
	}

	public void verifyCheckAllRoleDrpDwnValFunctionality() throws Exception {
		List<WebElement> totlChkBox = new ArrayList<WebElement>();
		totlChkBox = driver.findElements(By.xpath("//*[@class='checkboxInput']"));
		System.out.println("Total checkbox value is: " + totlChkBox.size());
		int count = 0;
		for (int i = 0; i < totlChkBox.size(); i++) {
			if (totlChkBox.get(i).isSelected() == true) {
				count++;
			}
		}
		Assert.assertEquals(count, totlChkBox.size());
	}

	public void clickUnCheckAllRoleDrpDwnVal() throws Exception {
		unCheckAllRoleDrpdwn.click();
	}

	public void verifyUnCheckAllRoleDrpDwnValFunctionality() throws Exception {
		List<WebElement> totlChkBox = new ArrayList<WebElement>();
		totlChkBox = driver.findElements(By.xpath("//input[@checked='checked']"));
		System.out.println("Total checkbox value is: " + totlChkBox.size());
		int count = 0;
		for (int i = 0; i < totlChkBox.size(); i++) {
			if (totlChkBox.get(i).isSelected() == true) {
				count++;
			}
		}
		Assert.assertEquals(count, totlChkBox.size());
	}

	public void verifyRecordStatusLabel() throws Exception {
		Assert.assertEquals(recordStatusSpelling.getText(), "Record Status");
	}

	public void clickRecordStatusLabel() throws Exception {
		recordStatusSpelling.click();
	}

	public void verifyRecordStatusCount(String statusCount) throws Exception {
		String[] count= statusCount.split(",");
		List<WebElement> xpath = driver.findElements(By.xpath("//*[@ng-change='vm.validateFields()']"));
		int xpathCount = xpath.size();
		Assert.assertEquals(xpathCount, count.length);
	}

	public void verifyPassedRecordStatus() throws Exception {
		boolean actual = passRecordStatus.isDisplayed();
		Assert.assertEquals(actual, true);
	}

	public void verifyFailedRecordStatus() throws Exception {
		boolean actual = failRecordStatus.isDisplayed();
		Assert.assertEquals(actual, true);
	}

	public void verifyReviewPendingRecordStatus() throws Exception {
		boolean actual = reviewPendingRecordStatus.isDisplayed();
		Assert.assertEquals(actual, true);
	}

	public void verifyVoidRecordStatus() throws Exception {
		boolean actual = voidRecordStatus.isDisplayed();
		Assert.assertEquals(actual, true);
	}

	public void verifyDraftRecordStatus() throws Exception {
		boolean actual = draftRecordStatus.isDisplayed();
		Assert.assertEquals(actual, true);
	}

	public void verifyRejectedRecordStatus() throws Exception {
		boolean actual = rejectedRecordStatus.isDisplayed();
		Assert.assertEquals(actual, true);
	}

	public void clickPassedRecordCheckbox() throws Exception {
		passRecordStatus.click();
	}

	public void verifyPassedRecordCheckboxFunctionality() throws Exception {
		boolean actual;
		if(driver.findElements(By.xpath("//input[@ng-change='vm.validateFields()' and @aria-checked='true']")).size() != 0)
		{
			actual = true;
		}else {
			actual = false;
		}
		Assert.assertEquals(actual, true);
	}

	public void clickFailedRecordCheckbox() throws Exception {
		failRecordStatus.click();
	}

	public void verifyFailedRecordCheckboxFunctionality() throws Exception {
		boolean actual;
		if(driver.findElements(By.xpath("//input[@ng-change='vm.validateFields()' and @aria-checked='true']")).size() != 0)
		{
			actual = true;
		}else {
			actual = false;
		}
		Assert.assertEquals(actual, true);
	}

	public void clickReviewPendingRecordCheckbox() throws Exception {
		reviewPendingRecordStatus.click();
	}

	public void verifyReviewPendingRecordCheckboxFunctionality() throws Exception {
		boolean actual;
		if(driver.findElements(By.xpath("//input[@ng-change='vm.validateFields()' and @aria-checked='true']")).size() != 0)
		{
			actual = true;
		}else {
			actual = false;
		}
		Assert.assertEquals(actual, true);
	}

	public void clickVoidRecordCheckbox() throws Exception {
		voidRecordStatus.click();
	}

	public void verifyVoidRecordCheckboxFunctionality() throws Exception {
		boolean actual;
		if(driver.findElements(By.xpath("//input[@ng-change='vm.validateFields()' and @aria-checked='true']")).size() != 0)
		{
			actual = true;
		}else {
			actual = false;
		}
		Assert.assertEquals(actual, true);
	}

	public void clickDraftRecordCheckbox() throws Exception {
		draftRecordStatus.click();
	}

	public void verifyDraftRecordCheckboxFunctionality() throws Exception {
		boolean actual;
		if(driver.findElements(By.xpath("//input[@ng-change='vm.validateFields()' and @aria-checked='true']")).size() != 0)
		{
			actual = true;
		}else {
			actual = false;
		}
		Assert.assertEquals(actual, true);
	}

	public void clickRejectedRecordCheckbox() throws Exception {
		rejectedRecordStatus.click();
	}

	public void verifyRejectedRecordCheckboxFunctionality() throws Exception {
		boolean actual;
		if(driver.findElements(By.xpath("//input[@ng-change='vm.validateFields()' and @aria-checked='true']")).size() != 0)
		{
			actual = true;
		}else {
			actual = false;
		}
		Assert.assertEquals(actual, true);
	}

	public void verifyRecordStatusRemoveIconVisible() throws Exception {
		boolean actual = recordStatusRemoveIcon.isDisplayed();
		Assert.assertEquals(actual, true);
	}

	public void verifyRecordStatusRemoveIconColor() throws Exception {
		Assert.assertEquals(helper.BtnColor(recordStatusRemoveIcon), "#000000");
	}

	public void clickRecordStatusRemoveIcon() throws Exception {
		recordStatusRemoveIcon.click();
	}

	public void verifyRecordStatusRemoveIconFunctionality() throws Exception {
		boolean actual;
		if(driver.findElements(By.xpath("//div[2]/div[2]/div[2]/div/ul/li[6]")).size() != 0)
		{
			actual = false;
		}else {
			actual = true;
		}
		Assert.assertEquals(actual, true);
	}

	public void clickViewLinkOfApp() throws Exception {
		viewLinkApp.click();
	}

	public void verifyViewLinkOfApp() throws Exception {
		String actual = viewLinkAppTitle.getText();
		if(actual.equals("Blood Sample Master") || actual.equals("Blood Sample Form")) {
			Assert.assertEquals(true, true);
		}else {
			Assert.assertEquals(false, true);
		}
	}

	public void verifyAdvancedSearchPagination() throws Exception {
		String nextBtnColor, Pagination, TotlRcd;
		int i = 0, count = 0, add = 0;

		List<org.openqa.selenium.WebElement> inList = new ArrayList<WebElement>();
		List<String> temp = new ArrayList<String>();

		// checking if there are more than 10 records is display
		if (driver.findElements(By.xpath("//*[@ng-click='selectPage(page + 1, $event)']"))
				.size() != 0 == true) {
			System.out.println("found more than 10 records");

			for (i = 0; i < 100; i++) {

				// this line is storing a hex code of next button.
				nextBtnColor = helper.FontColorCodeHex(nextPagging);

				// this line is taking a "Created Date column" all value.
				inList = (ArrayList<org.openqa.selenium.WebElement>) driver.findElements(By.xpath("//*[@ng-click='vm.getActivityinfo(etry._id,etry.App)']"));

				try {

					for (add = 0; add < inList.size(); add++) {
						temp.add(count, inList.get(add).getText());
						count++;
					}

					System.out.println(i + "-grid list: " + temp.size());
					System.out.println("--------------------------------------");
					System.out.println(Arrays.deepToString(temp.toArray()));
					System.out.println("--------------------------------------");

				} catch (Exception e) {
					System.out.println(e);
				}
				nextPagging.click();
				Thread.sleep(1000);

				if (nextBtnColor.equalsIgnoreCase("#777777")) {
					break;
				}
			}
		} else {
			inList = (ArrayList<org.openqa.selenium.WebElement>) driver.findElements(By.xpath("//*[@ng-click='vm.getActivityinfo(etry._id,etry.App)']"));

			for (add = 0; add < inList.size(); add++) {
				temp.add(count, inList.get(add).getText());
				count++;
			}
			System.out.println(i + "-grid list: " + temp.size());

		}

		TotlRcd = Integer.toString(temp.size());
		Pagination = driver.findElement(By.xpath("//*[@class='pull-right showing-text-adva-search ng-binding']")).getText();

		String[] arryOfPagination = Pagination.split(" ");

		if (arryOfPagination[3].equalsIgnoreCase(arryOfPagination[6])) {

			if (temp.size() > 10) {

				System.out.println("sss Showing " + i + "1 to " + TotlRcd + " of total " + TotlRcd + " entries");
				Assert.assertEquals("Showing " + i + "1 to " + TotlRcd + " of total " + TotlRcd + " entries",
						Pagination);
			} else {

				Assert.assertEquals("Showing 1 to " + TotlRcd + " of total " + TotlRcd + " entries", Pagination);
			}
		} else {
			Assert.assertEquals(true, false);
		}
	}	
}
