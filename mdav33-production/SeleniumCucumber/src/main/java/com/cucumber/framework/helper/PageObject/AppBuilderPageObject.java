﻿package com.cucumber.framework.helper.PageObject;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.util.Iterator;
import java.util.List;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.Assert;
import com.cucumber.framework.helper.Helper;
import com.cucumber.framework.helper.Button.ButtonHelper;
import com.cucumber.framework.helper.DropDown.DropDownHelper;
import com.cucumber.framework.helper.Logger.LoggerHelper;
import com.cucumber.framework.helper.Scroll.Scroll;
import com.cucumber.framework.settings.ObjectRepo;
import com.google.common.base.Strings;

public class AppBuilderPageObject extends PageBase {
	
	private WebDriver driver;
	public ButtonHelper btnHelper;
	private final Logger log = LoggerHelper.getLogger(AppBuilderPageObject.class);
	public Helper helper = new Helper();
	
	public AppBuilderPageObject(WebDriver driver) {
		super(driver);
		this.driver = driver;
		btnHelper = new ButtonHelper(driver);
	}

	/** WebElements */

	@FindBy(how = How.XPATH, using = "//*[@ui-sref='secure.admin.schemas.list']")
	public WebElement appBuilderTile;

	@FindBy(how = How.XPATH, using = "//*[@class='currentStep ng-binding']")
	public WebElement pageTxtLabel;

	@FindBy(how = How.XPATH, using = "//*[@ng-click='vm.createduplicate(field,row)']")
	public WebElement copyAction;

	@FindBy(how = How.XPATH, using = "//*[@ui-sref='secure.admin.schemas.edit.basic']")
	public WebElement addNewBtn;

	@FindBy(how = How.XPATH, using = "//*[@ng-click='vm.gotoState(1)']")
	public WebElement basicTabHeader;

	@FindBy(how = How.XPATH, using = "//*[@ng-click='vm.gotoState(2)']")
	public WebElement attributeTabHeader;

	@FindBy(how = How.XPATH, using = "//*[@ng-click='vm.gotoState(3)']")
	public WebElement workflowTabHeader;

	@FindBy(how = How.XPATH, using = "//*[@ng-click='vm.gotoState(4)']")
	public WebElement acceptanceTabHeader;

	@FindBy(how = How.XPATH, using = "//*[@ng-click='vm.gotoState(5)']")
	public WebElement previewTabHeader;

	@FindBy(how = How.XPATH, using = "//*[@ng-click='vm.gotoState(6)']")
	public WebElement permissionsTabHeader;
	
	@FindBy(how = How.XPATH, using = "//*[@class='pull-left ng-binding']")
	public WebElement appDetailsLabel;

	@FindBy(how = How.XPATH, using = "//*[@class='pull-right qc-form-tab-attributs']")
	public WebElement currntVersion;

	@FindBy(how = How.XPATH, using = "//*[@name='qcModuleSelect']")
	public WebElement appMdul;

	@FindBy(how = How.XPATH, using = "//*[@ng-if='(editSchemaForm.qcModuleSelect.$dirty || editSchemaForm.qcModuleSelect.$touched) && editSchemaForm.qcModuleSelect.$error.required']")
	public WebElement appMdulReqMsg;

	@FindBy(how = How.XPATH, using = "//*[@ng-click='vm.saveAsDraft(editSchemaForm)']")
	public WebElement saveDraftApp;

	@FindBy(how = How.XPATH, using = "//*[@ng-click='vm.continue(editSchemaForm)']")
	public WebElement continueBasicBtn;

	@FindBy(how = How.XPATH, using = "//*[@ng-click='vm.continue(editAttributeForm)']")
	public WebElement continueAttributeBtn;
	
	@FindBy(how = How.XPATH, using = "//*[@ng-click='vm.ShowConfirm()']")
	public WebElement cancelBtn;

	@FindBy(how = How.XPATH, using = "//*[@style='display: block;']")
	public WebElement cancelAlertPopupMsg;

	@FindBy(how = How.XPATH, using = "//*[@class='cancel']")
	public WebElement cancelAlertPopupNoBtn;

	@FindBy(how = How.XPATH, using = "//*[@class='confirm']")
	public WebElement cancelAlertPopupYesBtn;

	@FindBy(how = How.XPATH, using = "//*[@ng-model='vm.schema.title']")
	public WebElement appTitleTxtBox;

	@FindBy(how = How.XPATH, using = "//*[@translate='secure.admin.schema.edit.basic.APPTITLER']")
	public WebElement appTitleTxtBoxReqMsg;

	@FindBy(how = How.XPATH, using = "//*[@translate='secure.admin.schema.edit.basic.APPTITLE2']")
	public WebElement appTitleTxtBoxMinMsg;

	@FindBy(how = How.XPATH, using = "//*[@translate='secure.admin.schema.edit.basic.APPTITLE50']")
	public WebElement appTitleTxtBoxMaxMsg;

	@FindBy(how = How.XPATH, using = "//*[@ng-model='vm.schema.description']")
	public WebElement appDescptTxtBox;

	@FindBy(how = How.XPATH, using = "//*[@translate='secure.admin.schema.edit.basic.APPDESCRIPTION2']")
	public WebElement appDescptTxtBoxMinMsg;

	@FindBy(how = How.XPATH, using = "//*[@translate='secure.admin.schema.edit.basic.APPDESCRIPTION800']")
	public WebElement appDescptTxtBoxMaxMsg;

	@FindBy(how = How.XPATH, using = "//*[@name='instructionForUserRadioButtonURL']")
	public WebElement URLRadio;

	@FindBy(how = How.XPATH, using = "//*[@ng-model='vm.schema.instructionURL']")
	public WebElement URLTextBox;

	@FindBy(how = How.XPATH, using = "//*[@name='instructionForUserRadioButtonUploadHelpDocument']")
	public WebElement UploadDocumentRadio;

	@FindBy(how = How.XPATH, using = "//*[@translate='secure.admin.schema.edit.basic.URLNOTVALID']")
	public WebElement invalidURLValMsg;

	@FindBy(how = How.XPATH, using = "//*[@ng-if='!editSchemaForm.qcUploader.$error.validation']")
	public WebElement removeURLValMsg;

	@FindBy(how = How.XPATH, using = ".//div[3]/div/div[7]/div/input")
	public WebElement sendEmailChkbox;

	@FindBy(how = How.XPATH, using = "//*[@ng-model='vm.schema.emailIdForQCFormFail']")
	public WebElement sendEmailTxtbox;

	@FindBy(how = How.XPATH, using = "//*[@translate='secure.admin.schema.edit.basic.EMAILADDMUL']")
	public WebElement emailInfoMsg;

	@FindBy(how = How.XPATH, using = "//*[@translate='secure.threshold.edit.EMAILONE']")
	public WebElement sendEmailReqMsg;

	@FindBy(how = How.XPATH, using = "//*[@translate='secure.admin.schema.edit.basic.EMAILADDI']")
	public WebElement sendEmailInvalidMsg;

	@FindBy(how = How.XPATH, using = "//*[@ng-if='vm.yesSelected']")
	public WebElement siteSelectionYesInfoMsg;

	@FindBy(how = How.XPATH, using = "//*[@ng-if='vm.noSelected']")
	public WebElement siteSelectionNoInfoMsg;

	@FindBy(how = How.XPATH, using = "//*[@ng-model='vm.schema.siteYesNo']")
	public WebElement SiteSelectionDrp;

	@FindBy(how = How.XPATH, using = "//*[@ng-repeat='(key, value) in vm.siteValidationMsg']")
	public WebElement siteSelectionReqMsg;

	@FindBy(how = How.XPATH, using = "//*[@ng-model='vm.schema.headMsgPrint']")
	public WebElement defaultHeaderMsgTxtBox;

	@FindBy(how = How.XPATH, using = "//*[@ng-model='vm.schema.headMsgPrintForPass']")
	public WebElement passedHeaderMsgTxtBox;

	@FindBy(how = How.XPATH, using = "//*[@ng-model='vm.schema.headMsgPrintForFail']")
	public WebElement failedHeaderMsgTxtBox;

	@FindBy(how = How.XPATH, using = "//*[@translate='secure.admin.schema.edit.basic.HEADERMESSAGEFORPRINT2']")
	public WebElement defaultHeaderMinMsg;

	@FindBy(how = How.XPATH, using = "//*[@translate='secure.admin.schema.edit.basic.HEADERMESSAGEFORPRINT200']")
	public WebElement defaultHeaderMaxMsg;

	@FindBy(how = How.XPATH, using = "//*[@translate='text.HEADERMSGFORPRINTWHENSTATUSISPASSED']")
	public WebElement passedHeaderMinMsg;

	@FindBy(how = How.XPATH, using = "//*[@translate='text.HEADERMSGNOTMORETHAN200']")
	public WebElement passedHeaderMaxMsg;

	@FindBy(how = How.XPATH, using = "//*[@translate='text.HEADERMESSAGEFORPRINTFAILED2CHAR']")
	public WebElement failedHeaderMinMsg;

	@FindBy(how = How.XPATH, using = "//*[@translate='text.HEADERMSGFORPINTFAILEDNOT200']")
	public WebElement failedHeaderMaxMsg;

	@FindBy(how = How.XPATH, using = "//*[@ng-model='vm.schema.setreviewworkflow']")
	public WebElement reviewWorkflowChkBox;
	
	@FindBy(how = How.XPATH, using = "//*[@class='breadcrumb breadcrumb-1']//following::p")
	public WebElement attributeInfoMsg;
	
	@FindBy(how = How.XPATH, using = "//*[@ng-model='vm.attribute']")
	public WebElement attributeDrpDwn;
	
	@FindBy(how = How.XPATH, using = "//*[@ng-click='vm.add(vm.attribute)']")
	public WebElement addAttributeBtn;
	
	@FindBy(how = How.XPATH, using = "//*[@ng-click='vm.removeAttribute(attr._id,$index)']")
	public WebElement attributeRemoveBtn;
	
	@FindBy(how = How.XPATH, using = "//*[@translate='secure.admin.schema.edit.workflow.procedure.ENTITYWORKFLOW']")
	public WebElement entityWorkflowLable;
	
	@FindBy(how = How.XPATH, using = "//*[@translate='secure.admin.schema.edit.workflow.procedure.PROCEDUREWORKFLOW']")
	public WebElement procedureWorkflowLable;
	
	@FindBy(how = How.XPATH, using = "//*[@class='Set-Entity-Rule-font-size']")
	public WebElement applyEntityFilterMsg;
	
	@FindBy(how = How.XPATH, using = "//*[@ng-click='vm.addEntity(vm.selectedEntity)']")
	public WebElement addEntityBtn;
	
	@FindBy(how = How.XPATH, using = "//*[@placeholder='Search and select Entity']")
	public WebElement SelectEntityTextbox;
	
	@FindBy(how = How.XPATH, using = "//*[@ng-click='selectMatch($index, $event)']")
	public WebElement SelectEntityAutocomplete;
	
	@FindBy(how = How.XPATH, using = "//*[@class='btn btn-default ng-binding' and contains(text(),'Filter')]")
	public WebElement entityFilterBtn;
	
	@FindBy(how = How.XPATH, using = "//*[@class='btn btn-default ng-binding' and contains(text(),'Add Rule')]")
	public WebElement entityAddRuleBtn;
	
	@FindBy(how = How.XPATH, using = "//*[@class='Set-Entity-Rule-font-size']")
	public WebElement applyEntityFilterLabel;
	
	@FindBy(how = How.XPATH, using = "//*[@translate='secure.admin.schema.edit.workflow.setrule.DEFINERULE']")
	public WebElement defineRuleLabel;
	
	@FindBy(how = How.XPATH, using = "//*[@ng-click='showme=true; vm.setindex = -1']")
	public WebElement showLink;
	
	@FindBy(how = How.XPATH, using = "//*[@ng-click='showme=false; vm.setindex = -1']")
	public WebElement hideLink;
	
//	=================================== Chirag Kariya ===================================
	
	@FindBy(how = How.XPATH, using = "//*[@placeholder='Search and select Procedure']")
	public WebElement selectProcedureTextbox;
	
	@FindBy(how = How.XPATH, using = "//a[@ng-attr-title='{{match.label}}']")
	public WebElement selectProcedureAutocomplete;
	
	@FindBy(how = How.XPATH, using = "//button[@ng-click='vm.continue(schemaProcedureForm)']")
	public WebElement continueBtnWorkflowTab;
	
	@FindBy(how = How.XPATH, using = "//button[@ng-click='vm.continue()']")
	public WebElement continueBtnAcceptanceTab;
	
	@FindBy(how = How.XPATH, using = "//button[@ng-click='vm.continue()']")
	public WebElement publishContinueBtnPreviewTab;
	
	@FindBy(how = How.XPATH, using = "//button[@ng-click='vm.save()']")
	public WebElement saveBtnPermissionTab;
	
	@FindBy(how = How.XPATH, using = "//select[@name='qcProcedureSelect0']")
	public WebElement selectProcedureAcceptance;
	
	@FindBy(how = How.XPATH, using = "//button[@title='Add selected procedure']")
	public WebElement addProcedureAcceptanceBtn;
	
	@FindBy(how = How.XPATH, using = "//tr[1]/td[1]/span[1]/span/a")
	public WebElement verifyNewApp;
	
	@FindBy(how = How.XPATH, using = "//button[@ng-click='vm.addEntity(vm.selectedEntity)']")
	public WebElement addEntityBtnWorkflow;
	
	@FindBy(how = How.XPATH, using = "//button[@ng-click='vm.addProcedure(vm.selectedProcedure)']")
	public WebElement addProcedureBtnWorkflow;
	
	@FindBy(how = How.XPATH, using = "//input[@ng-model='item.editSchema']")
	public WebElement clickBuildAppCheckbox;
	
	@FindBy(how = How.XPATH, using = "//input[@ng-model='item.review']")
	public WebElement clickOtherCheckbox;
	
	@FindBy(how = How.XPATH, using = "//div[@class='col-md-12 overflow-ipad-grid']")
	public WebElement appListingsScreen;
	
	@FindBy(how = How.XPATH, using = "//div[@class='pull-left ng-binding']/b")
	public WebElement appNamePermissionTab;
	
	@FindBy(how = How.XPATH, using = "//p[@translate='secure.admin.schema.edit.permission.DEFINEPER']")
	public WebElement definePermissionApp;
	
	@FindBy(how = How.XPATH, using = "//div[@class='col-md-4 col-sm-3 preview-tab-align-rt']")
	public WebElement currentVersionPermissionTab;
	
	@FindBy(how = How.XPATH, using = "//div[@class='col-md-8 col-sm-9 preview-tab-align-left']")
	public WebElement appNamePreviewTab;
	
	@FindBy(how = How.XPATH, using = "//a[@ng-click='vm.gotoState(6)']")
	public WebElement permissionTabNavigation;

	@FindBy(how = How.XPATH, using = "//a[@ng-click='vm.gotoState(5)']")
	public WebElement previewTabNavigation;
	
	@FindBy(how = How.XPATH, using = "//*[@class='disabled']")
	public WebElement nextTabDisabled;
	
	@FindBy(how = How.XPATH, using = "//p[@translate='secure.admin.schema.edit.preview.PUBLISHEDAPP']")
	public WebElement previewTabInformationMsg;
	
	@FindBy(how = How.XPATH, using = "//i[@ng-click='vm.removeProcedure($parent.$index, $index, criteria)']")
	public WebElement removeProcedureAcceptanceTab;
	
	@FindBy(how = How.XPATH, using = "//p[@translate='secure.admin.schema.edit.acceptance.TALL']")
	public WebElement keyAttributeMsgAcceptanceTab;
	
	@FindBy(how = How.XPATH, using = "//p[@translate='option.DEFINEPASSCRITE']")
	public WebElement definePassCriteriaMsgAcceptanceTab;
	
	@FindBy(how = How.XPATH, using = "//p[@translate='secure.admin.schema.edit.acceptance.DEFINEAPPANDAPPPASS']")
	public WebElement addProcedureMsgAcceptanceTab;
	
	@FindBy(how = How.XPATH, using = "//a[@ng-click='vm.gotoState(4)']")
	public WebElement acceptanceTabNavigation;
	
	@FindBy(how = How.XPATH, using = "//div[@class='pull-left ng-binding']")
	public WebElement acceptanceTabLabel;
	
	@FindBy(how = How.XPATH, using = "//i[@class='ng-binding']")
	public WebElement acceptanceTabCurrentVersion;
	
	@FindBy(how = How.XPATH, using = "//p[@class='heightforpannel-schema-font ng-binding']")
	public WebElement trackUniqueRecordsMsg;
	
	@FindBy(how = How.XPATH, using = "(//*[@ng-click='vm.go(field,row,vm.resource);'])[3]")
	public WebElement viewAduitTrail;
	
	@FindBy(how = How.XPATH, using = "(//*[@ng-hide='showme'])[1]")
	public WebElement showBtnOfProcedureClick;
	
	@FindBy(how = How.XPATH, using = "(//*[@class='btn btn-primary acceptance-criteria-btn-schema ng-binding'])[1]")
	public WebElement firstAcceptacneCriteria;
	
	@FindBy(how = How.XPATH, using = "//select[@name='condition0']")
	public WebElement conditionDrpDwn;
	
	
	// --------------------public methods--------------------

	public WebDriver getDriver() {

		return this.driver;

	}

	public void appBuilderTileClick() throws Exception {
		appBuilderTile.click();
		Thread.sleep(2000);
	}
	public void continueBasicBtnClick() {
		continueBasicBtn.click();
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void continueAttributeBtnClick() {
		continueAttributeBtn.click();
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		new Scroll().scrollTillElem(pageTxtLabel);
	}
	public void verifyAppBuilderLabel() throws Exception {

		new Scroll().scrollUp(driver);
		Assert.assertEquals(pageTxtLabel.getText(), "App Builder");
	}

	public void copyActionClick() throws Exception {
		copyAction.click();
		Thread.sleep(2000);
	}

	public void VerifyCopyPopup(String PopupTitle) throws Exception {

		if (ObjectRepo.driver
				.findElements(By.xpath("//*[@class='sweet-alert cross-button show-input showSweetAlert visible']"))
				.size() != 0) {
			String Title = ObjectRepo.driver.findElement(By.xpath("//*[contains(text(),'" + PopupTitle + "')]"))
					.getText();
			System.out.println("Title: " + Title + " and Popup Title:  " + PopupTitle);
			Assert.assertEquals(Title, PopupTitle);
		}
	}

	public void verifyCopyPopupDefaultValue() throws Exception {
		Thread.sleep(2000);
		String Expected = ObjectRepo.driver.findElement(By.xpath(".//table/tbody/tr[1]/td[1]")).getText() + " - Copy";
		Thread.sleep(1000);
		ObjectRepo.driver.findElement(By.xpath("//*[@ng-click='vm.createduplicate(field,row)']")).click();
		Thread.sleep(1000);
		if (ObjectRepo.driver
				.findElements(By.xpath("//*[@class='sweet-alert cross-button show-input showSweetAlert visible']"))
				.size() != 0) {
			Thread.sleep(3000);

			WebElement txtFName = ObjectRepo.driver.findElement(By.xpath("//*[@tabindex='3' and @type='text']"));
			Actions a = new Actions(ObjectRepo.driver);
			a.sendKeys(txtFName, Keys.chord(Keys.CONTROL, "a")).perform();
			a.sendKeys(Keys.chord(Keys.CONTROL, "c")).perform();
			Clipboard c = Toolkit.getDefaultToolkit().getSystemClipboard();
			System.out.println(c.getData(DataFlavor.stringFlavor));
			String Actual = (String) c.getData(DataFlavor.stringFlavor);
			System.out.println("Expected: " + Expected + " and Actual:  " + Actual);

			Assert.assertEquals(true, true);
		}
	}

	public void verifyAddNewBtnColor() {
		String btnColor = new Helper().BtnColor(addNewBtn);

		Assert.assertEquals(btnColor, "#337ab7");
	}

	public void addNewBtnClick() throws Exception {
		addNewBtn.click();
		Thread.sleep(3000);
	}

	public void verifyAddNewBtnFucn() throws Exception {
		Assert.assertEquals(pageTxtLabel.getText(), "Build App");
	}

	public void verifyBasicHeaderTabLabel() throws Exception {
		Assert.assertEquals(basicTabHeader.getText(), "1  BASIC");
	}
	public void verifyAttributeHeaderTabLabel() throws Exception {
		Assert.assertEquals(attributeTabHeader.getText(), "2   ATTRIBUTES");
	}
	public void verifyWorkflowHeaderTabLabel() throws Exception {
		Assert.assertEquals(workflowTabHeader.getText(), "3   WORKFLOW");
	}
	public void verifyAcceptanceHeaderTabLabel() throws Exception {
		Assert.assertEquals(acceptanceTabHeader.getText(), "4  ACCEPTANCE");
	}
	public void verifyPreviewHeaderTabLabel() throws Exception {
		Assert.assertEquals(previewTabHeader.getText(), "5  PREVIEW");
	}
	public void verifyPermissionsHeaderTabLabel() throws Exception {
		Assert.assertEquals(permissionsTabHeader.getText(), "6  PERMISSIONS");
	}
	
	
	public void verifyBasicHeaderTabColor() {
		String Color = new Helper().BtnColor(basicTabHeader);

		Assert.assertEquals(Color, "#006600");
	}
	public void verifyAttributeHeaderTabColor() {
		String Color = new Helper().BtnColor(attributeTabHeader);

		Assert.assertEquals(Color, "#006600");
	}
	public void verifyWorkflowHeaderTabColor() {
		String Color = new Helper().BtnColor(workflowTabHeader);

		Assert.assertEquals(Color, "#006600");
	}
	public void verifyAppDetailsLabel() throws Exception {
		Assert.assertEquals(appDetailsLabel.getText(), "App Details");
	}

	public void verifyCurreVersionLabel() throws Exception {
		Assert.assertEquals(currntVersion.getText(), "Current Version 1.0");
	}

	public void verifyCurreVersionColor() {
		String Color = new Helper().FontColor(currntVersion);

		Assert.assertEquals(Color, "#0099cc");
	}

	public void verifyBasicTabLables() throws Exception {

		String Expected = "Site Selections\n" + "Approval Sequence\n" + "Share App Link Settings\n" + "Print Settings";
		String Actual = null;
		List<WebElement> links = driver.findElements(By.xpath("//*[contains(@class,'breadcrumb')]/h4"));

		Iterator<WebElement> iter = links.iterator();

		while (iter.hasNext()) {
			WebElement we = iter.next();
			if (Strings.isNullOrEmpty(Actual)) {
				Actual = we.getText();
			} else {
				Actual = Actual + "\n" + we.getText();
			}
		}
		System.out.println(Actual);
		Assert.assertEquals(Actual, Expected);
	}

	public void verifyDisabledTab_Basic() throws Exception {

		String Expected = "2   ATTRIBUTES\n" + "3   WORKFLOW\n" + "4  ACCEPTANCE\n" + "5  PREVIEW\n" + "6  PERMISSIONS";
		String Actual = null;
		List<WebElement> links = driver.findElements(By.xpath("//*[@class='disabled']"));

		Iterator<WebElement> iter = links.iterator();

		while (iter.hasNext()) {
			WebElement we = iter.next();
			if (Strings.isNullOrEmpty(Actual)) {
				Actual = we.getText();
			} else {
				Actual = Actual + "\n" + we.getText();
			}
		}

		System.out.println(Actual);
		Assert.assertEquals(Actual, Expected);
	}
	public void verifyDisabledTab_Attribute() throws Exception {

		String Expected = "3   WORKFLOW\n" + "4  ACCEPTANCE\n" + "5  PREVIEW\n" + "6  PERMISSIONS";
		String Actual = null;
		List<WebElement> links = driver.findElements(By.xpath("//*[@class='disabled']"));

		Iterator<WebElement> iter = links.iterator();

		while (iter.hasNext()) {
			WebElement we = iter.next();
			if (Strings.isNullOrEmpty(Actual)) {
				Actual = we.getText();
			} else {
				Actual = Actual + "\n" + we.getText();
			}
		}

		System.out.println(Actual);
		Assert.assertEquals(Actual, Expected);
	}
	public void verifyDisabledTab_Workflow() throws Exception {

		String Expected = "4  ACCEPTANCE\n" + "5  PREVIEW\n" + "6  PERMISSIONS";
		String Actual = null;
		List<WebElement> links = driver.findElements(By.xpath("//*[@class='disabled']"));

		Iterator<WebElement> iter = links.iterator();

		while (iter.hasNext()) {
			WebElement we = iter.next();
			if (Strings.isNullOrEmpty(Actual)) {
				Actual = we.getText();
			} else {
				Actual = Actual + "\n" + we.getText();
			}
		}

		System.out.println(Actual);
		Assert.assertEquals(Actual, Expected);
	}
	public void verifyAppMdlReqMsg() {

		Assert.assertEquals(appMdulReqMsg.getText(), "App Module is required.");
	}

	public void verifySaveDraftAppColor() {
		String Color = new Helper().BtnColor(saveDraftApp);

		Assert.assertEquals(Color, "#337ab7");
	}

	public void verifyContinueBtnColor() {
		String Color = new Helper().BtnColor(continueBasicBtn);

		Assert.assertEquals(Color, "#5cb85c");
	}

	public void verifyCancelBtnColor() {
		String Color = new Helper().BtnColor(cancelBtn);

		Assert.assertEquals(Color, "#5a5a5a");
	}

	public void CancelBtnClick() throws InterruptedException {
		cancelBtn.click();
		Thread.sleep(1000);
	}

	public void verifyAlrtBoxDisply() {

		if (driver.findElements(By.xpath("//*[@class='sweet-alert showSweetAlert visible']")).size() != 0) {
			Assert.assertEquals(true, true);
		} else {
			Assert.assertEquals(true, false);
		}
	}

	public void verifyCancelAlertPopUpMsg() {

		Assert.assertEquals(cancelAlertPopupMsg.getText(), "Whoa! Are you sure? All changes will be lost.");
	}

	public void verifyCancelAlertboxNoBtnColor() {

		String Color = new Helper().BtnColor(cancelAlertPopupNoBtn);
		Assert.assertEquals(Color, "#d9534f");
	}

	public void verifyCancelAlertboxYesBtnColor() {
		String Color = new Helper().BtnColor(cancelAlertPopupYesBtn);
		Assert.assertEquals(Color, "#5cb85c");
	}

	public void cancelAlertPopupNoBtnClick() throws InterruptedException {
		cancelAlertPopupNoBtn.click();
		Thread.sleep(2000);
	}

	public void cancelAlertPopupYesBtnClick() throws InterruptedException {
		cancelAlertPopupYesBtn.click();
		Thread.sleep(3000);
	}

	public void verifyCancelAlertPopupNoBtnClick() {

		if (driver.findElements(By.xpath("//*[@class='sweet-alert showSweetAlert visible']")).size() != 0) {
			Assert.assertEquals(false, true);
		} else {
			Assert.assertEquals(true, true);
		}
	}

	public void verifyCancelAlertPopupyesBtnClick() {

		Assert.assertEquals(pageTxtLabel.getText(), "App Builder");
	}

	public void appTitleTxtBoxSendkeys(String inputValue) {
		appTitleTxtBox.sendKeys(inputValue);
	}

	public void verifyAppTitleTxtBoxReqMsg() {
		Assert.assertEquals(appTitleTxtBoxReqMsg.getText(), "App Title is required.");
	}

	public void verifyAppTitleTxtBoxReqMsgColor() {

		Assert.assertEquals(new Helper().FontColor(appTitleTxtBoxReqMsg), "#a94442");
	}

	public void verifyAppTitleTxtBoxMinMsg() {
		Assert.assertEquals(appTitleTxtBoxMinMsg.getText(), "App Title must be at least 2 characters.");
	}

	public void verifyAppTitleTxtBoxMinMsgColor() {

		Assert.assertEquals(new Helper().FontColor(appTitleTxtBoxMinMsg), "#a94442");
	}

	public void verifyAppTitleTxtBoxMaxMsg() {
		Assert.assertEquals(appTitleTxtBoxMaxMsg.getText(), "App Title cannot be more than 50 characters long.");
	}

	public void verifyAppTitleTxtBoxMaxMsgColor() {

		Assert.assertEquals(new Helper().FontColor(appTitleTxtBoxMaxMsg), "#a94442");
	}

	public void verifyAppTitleTxtBoxNoMsg() {

		boolean reqMsg = ObjectRepo.driver
				.findElements(By.xpath("//*[@translate='secure.admin.schema.edit.basic.APPTITLER']")).size() == 0,

				MaxMsg = ObjectRepo.driver
						.findElements(By.xpath("//*[@translate='secure.admin.schema.edit.basic.APPTITLE2']"))
						.size() == 0,
				MinMsg = ObjectRepo.driver
						.findElements(By.xpath("//*[@translate='secure.admin.schema.edit.basic.APPTITLE50']"))
						.size() == 0,
				flag;

		if (reqMsg && MaxMsg && MinMsg) {
			flag = true;
		} else {
			flag = false;
		}

		Assert.assertEquals(flag, true);
	}

	// ----------------------------------------------------------------

	public void appDescptTxtBoxSendkeys(String inputValue) {
		appDescptTxtBox.sendKeys(inputValue);
	}

	public void verifyAppDescptTxtBoxMinMsg() {
		Assert.assertEquals(appDescptTxtBoxMinMsg.getText(), "App Description must be at least 2 characters.");
	}

	public void verifyAppDescptTxtBoxMinMsgColor() {

		Assert.assertEquals(new Helper().FontColor(appDescptTxtBoxMinMsg), "#a94442");
	}

	public void verifyAppDescptTxtBoxMaxMsg() {
		Assert.assertEquals(appDescptTxtBoxMaxMsg.getText(), "App Description cannot be more than 800 characters.");
	}

	public void verifyAppDescptTxtBoxMaxMsgColor() {

		Assert.assertEquals(new Helper().FontColor(appDescptTxtBoxMaxMsg), "#a94442");
	}

	public void verifyAppDescptTxtBoxNoMsg() {

		boolean MaxMsg = ObjectRepo.driver
				.findElements(By.xpath("//*[@translate='secure.admin.schema.edit.basic.APPDESCRIPTION2']")).size() == 0,
				MinMsg = ObjectRepo.driver
						.findElements(By.xpath("//*[@translate='secure.admin.schema.edit.basic.APPDESCRIPTION800']"))
						.size() == 0,
				flag;

		if (MaxMsg && MinMsg) {
			flag = true;
		} else {
			flag = false;
		}

		Assert.assertEquals(flag, true);
	}

	public void URLRadioClick() throws InterruptedException {
		URLRadio.click();
		Thread.sleep(1000);
	}

	public void UploadDocumentRadioClick() throws InterruptedException {
		UploadDocumentRadio.click();
		Thread.sleep(1000);
	}

	public void URLTextBoxSendkeys(String inputValue) {
		URLTextBox.sendKeys(inputValue);
	}

	public void verifyURLTextBoxInvalidMsg() {
		new Scroll().scrollTillElem(URLRadio);
		Assert.assertEquals(invalidURLValMsg.getText(), "Not a valid URL, Don't forget to use http:// or https://");
	}

	public void verifyURLTextBoxRemoveMsg() {
		new Scroll().scrollTillElem(URLRadio);
		Assert.assertEquals(removeURLValMsg.getText(), "Please remove the entered URL.");

	}

	public void verifyURLTextBoxInvalidMsgColor() {
		new Scroll().scrollTillElem(URLRadio);

		Assert.assertEquals(new Helper().FontColor(invalidURLValMsg), "#a94442");
	}

	public void verifyURLTextBoxNoMsg() {

		boolean invalidMsg = ObjectRepo.driver
				.findElements(By.xpath("//*[@translate='secure.admin.schema.edit.basic.URLNOTVALID']")).size() == 0,
				flag;

		if (invalidMsg) {
			flag = true;
		} else {
			flag = false;
		}
		new Scroll().scrollTillElem(URLRadio);
		Assert.assertEquals(flag, true);
	}

	public void sendEmailChkboxClick() throws Exception {

		for (int i = 0; i < 10; i++) {
			if (sendEmailChkbox.isSelected()) {
				System.err.println("isSelected...");
				break;
			} else {
				System.err.println("not selected ......");
				sendEmailChkbox.click();
			}
		}
		Thread.sleep(2000);
	}

	public void sendEmailChkboxClickUnchk() throws Exception {

		for (int i = 0; i < 10; i++) {
			if (sendEmailChkbox.isSelected()) {
				System.err.println("isSelected...");
				sendEmailChkbox.click();
			} else {
				System.err.println("not selected ......");
				break;
			}
		}
		Thread.sleep(2000);
	}

	public void verifySendEmailTxtBoxAppear() {

		boolean emailTxtBox = ObjectRepo.driver
				.findElements(By.xpath("//*[@ng-model='vm.schema.emailIdForQCFormFail']")).size() != 0, flag;

		if (emailTxtBox) {
			flag = true;
		} else {
			flag = false;
		}
		new Scroll().scrollTillElem(URLRadio);

	}

	public void verifySendEmailTxtBoxDisappear() {

		boolean emailTxtBox = ObjectRepo.driver
				.findElements(By.xpath("//*[@ng-model='vm.schema.emailIdForQCFormFail']")).size() != 0, flag;

		if (emailTxtBox) {
			flag = true;
		} else {
			flag = false;
		}
		new Scroll().scrollTillElem(URLRadio);
		Assert.assertEquals(flag, false);
	}

	public void verifySendEmailInfoMsg() {
		new Scroll().scrollTillElem(URLRadio);
		Assert.assertEquals(emailInfoMsg.getText(), "Separate multiple Email Addresses with a comma(,).");
	}

	public void verifySendEmailInfoMsgColor() {
		new Scroll().scrollTillElem(URLRadio);

		Assert.assertEquals(new Helper().FontColor(emailInfoMsg), "#0000ff");
	}

	public void sendEmailTxtboxSendkeys(String inputValue) {
		sendEmailTxtbox.sendKeys(inputValue);
	}

	public void verifySendEmailReqMsg() {
		new Scroll().scrollTillElem(URLRadio);
		Assert.assertEquals(sendEmailReqMsg.getText(), "Please enter at least one Email Address.");
	}

	public void verifySendEmailInvalidMsg() {
		new Scroll().scrollTillElem(URLRadio);
		Assert.assertEquals(sendEmailInvalidMsg.getText(), "Invalid Email Address.");
	}

	public void verifySendEmailNoMsg() {

		boolean ReqMsg = ObjectRepo.driver.findElements(By.xpath("//*[@translate='secure.threshold.edit.EMAILONE']"))
				.size() == 0,
				InvalidMsg = ObjectRepo.driver
						.findElements(By.xpath("//*[@translate='secure.admin.schema.edit.basic.EMAILADDI']"))
						.size() == 0,
				flag;

		if (ReqMsg && InvalidMsg) {
			flag = true;
		} else {
			flag = false;
		}

		Assert.assertEquals(flag, true);
	}

	public void verifySiteSelectionYesInfoMsg() {
		new Scroll().scrollTillElem(URLRadio);
		Assert.assertEquals(siteSelectionYesInfoMsg.getText(),
				"Please select at least one option from each site level.");
	}

	public void verifySiteSelectionNoInfoMsg() {
		new Scroll().scrollTillElem(URLRadio);
		Assert.assertEquals(siteSelectionNoInfoMsg.getText(),
				"Please select default option from each site level. They will be stored in the database for each app.");
	}

	public void SelectionNoOptiClick() {
		//new DropDownHelper(driver).SelectUsingVisibleValue(SiteSelectionDrp, "No");
	}

	public void verifySiteSelectionYesInfoMsgColor() {
		new Scroll().scrollTillElem(URLRadio);

		Assert.assertEquals(new Helper().FontColor(siteSelectionYesInfoMsg), "#0000ff");
	}

	public void verifySiteSelectionNoInfoMsgColor() {
		new Scroll().scrollTillElem(URLRadio);

		Assert.assertEquals(new Helper().FontColor(siteSelectionNoInfoMsg), "#0000ff");
	}

	public void verifySiteSelectionReqMsg() {
		new Scroll().scrollTillElem(URLRadio);
		Assert.assertEquals(siteSelectionReqMsg.getText(), "At least one site is required from each level.");
	}

	public void verifySiteSelectionReqMsgColor() {
		new Scroll().scrollTillElem(URLRadio);

		Assert.assertEquals(new Helper().FontColor(siteSelectionReqMsg), "#a94442");
	}

	public void defaultHeaderMsgTxtBoxSendkeys(String inputValue) {
		defaultHeaderMsgTxtBox.sendKeys(inputValue);
	}

	public void passedHeaderMsgTxtBoxSendkeys(String inputValue) {
		passedHeaderMsgTxtBox.sendKeys(inputValue);
	}

	public void failedHeaderMsgTxtBoxSendkeys(String inputValue) {
		failedHeaderMsgTxtBox.sendKeys(inputValue);
	}

	public void verifyDefaultHeaderMinMsg() {
		new Scroll().scrollTillElem(defaultHeaderMsgTxtBox);
		Assert.assertEquals(defaultHeaderMinMsg.getText(), "Header Message for Print must be at least 2 characters.");
	}

	public void verifydefaultHeaderMinMsgColor() {
		new Scroll().scrollTillElem(defaultHeaderMsgTxtBox);

		Assert.assertEquals(new Helper().FontColor(defaultHeaderMinMsg), "#a94442");
	}

	public void verifyDefaultHeaderMaxMsg() {
		new Scroll().scrollTillElem(defaultHeaderMsgTxtBox);
		Assert.assertEquals(defaultHeaderMaxMsg.getText(),
				"Header Message for Print cannot be more than 200 characters.");
	}

	public void verifyDefaultHeaderMaxMsgColor() {
		new Scroll().scrollTillElem(defaultHeaderMsgTxtBox);

		Assert.assertEquals(new Helper().FontColor(defaultHeaderMaxMsg), "#a94442");
	}

	public void verifyDefaultHeaderNoMsg() {

		boolean minMsg = ObjectRepo.driver
				.findElements(By.xpath("//*[@translate='secure.admin.schema.edit.basic.HEADERMESSAGEFORPRINT2']"))
				.size() == 0,
				maxMsg = ObjectRepo.driver
						.findElements(
								By.xpath("//*[@translate='secure.admin.schema.edit.basic.HEADERMESSAGEFORPRINT200']"))
						.size() == 0,
				flag;

		if (minMsg && maxMsg) {
			flag = true;
		} else {
			flag = false;
		}

		Assert.assertEquals(flag, true);
	}

	public void verifyPassedHeaderMinMsg() {
		new Scroll().scrollTillElem(defaultHeaderMsgTxtBox);
		Assert.assertEquals(passedHeaderMinMsg.getText(),
				"Header Message for Print When App Status is Passed should be at least 2 characters.");
	}

	public void verifyPassedHeaderMinMsgColor() {
		new Scroll().scrollTillElem(defaultHeaderMsgTxtBox);

		Assert.assertEquals(new Helper().FontColor(passedHeaderMinMsg), "#a94442");
	}

	public void verifyPassedHeaderMaxMsg() {
		new Scroll().scrollTillElem(defaultHeaderMsgTxtBox);
		Assert.assertEquals(passedHeaderMaxMsg.getText(),
				"Header Message for Print When App Status is Passed must not be more than 200 characters.");
	}

	public void verifyPassedHeaderMaxMsgColor() {
		new Scroll().scrollTillElem(defaultHeaderMsgTxtBox);

		Assert.assertEquals(new Helper().FontColor(passedHeaderMaxMsg), "#a94442");
	}

	public void verifyPassedHeaderNoMsg() {

		boolean minMsg = ObjectRepo.driver
				.findElements(By.xpath("//*[@translate='text.HEADERMSGFORPRINTWHENSTATUSISPASSED']"))
				.size() == 0,
				maxMsg = ObjectRepo.driver
						.findElements(
								By.xpath("//*[@translate='text.HEADERMSGNOTMORETHAN200']"))
						.size() == 0,
				flag;

		if (minMsg && maxMsg) {
			flag = true;
		} else {
			flag = false;
		}

		Assert.assertEquals(flag, true);
	}
	
	public void verifyFailedHeaderMinMsg() {
		new Scroll().scrollTillElem(defaultHeaderMsgTxtBox);
		Assert.assertEquals(failedHeaderMinMsg.getText(),
				"Header Message for Print When App Status is Failed should be at least 2 characters.");
	}

	public void verifyFailedHeaderMinMsgColor() {
		new Scroll().scrollTillElem(defaultHeaderMsgTxtBox);

		Assert.assertEquals(new Helper().FontColor(failedHeaderMinMsg), "#a94442");
	}

	public void verifyFailedHeaderMaxMsg() {
		new Scroll().scrollTillElem(defaultHeaderMsgTxtBox);
		Assert.assertEquals(failedHeaderMaxMsg.getText(),
				"Header Message for Print When App Status is Failed must not be more than 200 characters.");
	}

	public void verifyFailedHeaderMaxMsgColor() {
		new Scroll().scrollTillElem(defaultHeaderMsgTxtBox);

		Assert.assertEquals(new Helper().FontColor(failedHeaderMaxMsg), "#a94442");
	}
	
	public void verifyFailedHeaderNoMsg() {

		boolean minMsg = ObjectRepo.driver
				.findElements(By.xpath("//*[@translate='text.HEADERMESSAGEFORPRINTFAILED2CHAR']"))
				.size() == 0,
				maxMsg = ObjectRepo.driver
						.findElements(
								By.xpath("//*[@translate='text.HEADERMSGFORPINTFAILEDNOT200']"))
						.size() == 0,
				flag;

		if (minMsg && maxMsg) {
			flag = true;
		} else {
			flag = false;
		}

		Assert.assertEquals(flag, true);
	}
	
	public void reviewWorkflowChkBoxClick() {
		reviewWorkflowChkBox.click();
		new Scroll().scrollTillElem(reviewWorkflowChkBox);
	}
	
	public void verifyReviewWorkflowOPtiVisible() {
		boolean isDisplay = driver.findElements(By.xpath("//*[@selected-model='vm.rolesValue']"))
				.size() != 0;

		Assert.assertEquals(isDisplay, true);
	}
	
	public void verifyReviewWorkflowOPtiInvisible() {
		boolean isDisplay = driver.findElements(By.xpath("//*[@selected-model='vm.rolesValue']"))
				.size() != 0;

		Assert.assertEquals(isDisplay, false);
	}
	public static void SiteClick(WebDriver driver, String Site) {
		driver
		.findElement(
				By.xpath("//*[contains(text(),'" + Site + "')]//preceding-sibling::input[@type='checkbox']"))
		.click();
	}
	String getAppTitle;
	public void addBasicTabDetails(String mdlName, String appTitle, String SiteName) throws Exception {
		
		helper.SelectDrpDwnValueByText(appMdul, mdlName);

		getAppTitle = appTitle +" - "+ new Helper().randomNumberGeneration();
		appTitleTxtBoxSendkeys(getAppTitle);
		
		String[] SplitSites = SiteName.split(">");
		SiteClick(driver, SplitSites[0]);
		SiteClick(driver, SplitSites[1]);
		SiteClick(driver, SplitSites[2]);
		
	}
	
	public void viewFirstAuditTrailClick() throws Exception {
		Thread.sleep(1000);
		viewAduitTrail.click();
		Thread.sleep(1000);
	}
	
	public void verifyAttributeDetailsLabel(String appTitle) throws Exception {
		Thread.sleep(2000);
		Assert.assertEquals(appDetailsLabel.getText(), getAppTitle+" -Attribute Details");
	}

	public void verifyAttributeInfoMsg() {
		Assert.assertEquals(attributeInfoMsg.getText(),
				"Define Attributes for App. Minimum 2 Attributes and/or Entities are required.");
	}
	
	public void verifyAttributeInfoMsgColor() {

		Assert.assertEquals(new Helper().FontColor(attributeInfoMsg), "#0000ff");
	}
	
	public void verifyInitiallyAddAttribute() {

		boolean addBtnDisplay = ObjectRepo.driver
				.findElements(By.xpath("//*[@ng-click='vm.add(vm.attribute)' and @aria-disabled='false']"))
				.size() == 0,
				flag;

		if (addBtnDisplay) {
			flag = true;
		} else {
			flag = false;
		}

		Assert.assertEquals(flag, true);
	}

	 public void selectAttributeDrpDwn(String visibleValue) {
		// new DropDownHelper(driver).SelectUsingVisibleValue(attributeDrpDwn, visibleValue);
	}
	 
	public void verifyAttributeAddBtnVisible() {
		boolean addBtnDisplay = ObjectRepo.driver
				.findElements(By.xpath("//*[@ng-click='vm.add(vm.attribute)' and @aria-disabled='false']")).size() == 0,
				flag;

		if (addBtnDisplay) {
			flag = true;
		} else {
			flag = false;
		}

		Assert.assertEquals(flag, false);
	}	

	public void addAttributeBtnClick() throws Exception {
		addAttributeBtn.click();
		Thread.sleep(2000);
	}
	public void attributeRemoveBtnClick() throws Exception {
		attributeRemoveBtn.click();
		Thread.sleep(2000);
	}
	public void verifyAttributeRemoveBtnClickable() {
		boolean addBtnDisplay = ObjectRepo.driver
				.findElements(By.xpath("//*[@ng-if='vm.schema.attributes.length > 0']")).size() != 0,
				flag;

		if (addBtnDisplay) {
			flag = true;
		} else {
			flag = false;
		}

		Assert.assertEquals(flag, false);
	}	
	public void verifyAttributeAddBtnClickable() {
		boolean addBtnDisplay = ObjectRepo.driver
				.findElements(By.xpath("//*[@ng-if='vm.schema.attributes.length > 0']")).size() != 0,
				flag;

		if (addBtnDisplay) {
			flag = true;
		} else {
			flag = false;
		}

		Assert.assertEquals(flag, true);
	}
	public void verifyAttributeAddBtnColor() {
		String Color = new Helper().BtnColor(addAttributeBtn);

		Assert.assertEquals(Color, "#337ab7");
	}
	
	
	public void addAttributeTabDetails() throws Exception {
		
		helper.SelectDrpDwnValueByIndex(attributeDrpDwn, 1);
		
		addAttributeBtnClick();
		Thread.sleep(2000);
		
		helper.SelectDrpDwnValueByIndex(attributeDrpDwn, 2);
		
		addAttributeBtnClick();
		Thread.sleep(2000);
	}
	
	
	public void verifyWorkflowDetailsLabel(String appTitle) throws Exception {
		Assert.assertEquals(appDetailsLabel.getText(), getAppTitle+" - Workflow Details");
	}
	
	
	public void verifyEntityWorkflowLable() {
		
		Assert.assertEquals(entityWorkflowLable.getText(),"Entity Workflow");
	}
	
	public void verifyProcedureWorkflowLable() {
		
		Assert.assertEquals(procedureWorkflowLable.getText(),"Procedure Workflow");
	}
	
	public void verifyAddEntityInvisible() {
		boolean addBtnDisplay = ObjectRepo.driver
				.findElements(By.xpath("//*[@ng-click='vm.addEntity(vm.selectedEntity)' and @aria-hidden='false']")).size() != 0,
				flag;

		if (addBtnDisplay) {
			flag = true;
		} else {
			flag = false;
		}

		Assert.assertEquals(flag, false);
	}
	
	public void verifyAddEntityVisible() {
		boolean addBtnDisplay = ObjectRepo.driver
				.findElements(By.xpath("//*[@ng-click='vm.addEntity(vm.selectedEntity)' and @aria-hidden='false']")).size() != 0,
				flag;

		if (addBtnDisplay) {
			flag = true;
		} else {
			flag = false;
		}

		Assert.assertEquals(flag, true);
	}
	
	public void addEntityBtnClick() throws Exception {
		addEntityBtn.click();
		Thread.sleep(2000);
	}
	
	public void selectEntitySendkeys(String entName) throws Exception {
		SelectEntityTextbox.clear();
		SelectEntityTextbox.sendKeys(entName);
		Thread.sleep(500);
		SelectEntityAutocomplete.click();
		Thread.sleep(500);
	}
	
	public void verifyEntityAddBtnColor() {
		String Color = new Helper().BtnColor(addEntityBtn);

		Assert.assertEquals(Color, "#5bc0de");
	}
	
	public void verifyAddEntityBtnClickable() {
		boolean addBtnDisplay = ObjectRepo.driver
				.findElements(By.xpath("//*[@ng-click='vm.removeAddedEntity($index,ent)']")).size() != 0,
				flag;

		if (addBtnDisplay) {
			flag = true;
		} else {
			flag = false;
		}

		Assert.assertEquals(flag, true);
	}
	public void verifyAddProcedureBtnClickable() {
		boolean addBtnDisplay = ObjectRepo.driver
				.findElements(By.xpath("//*[@ng-click='vm.removeAddedProcedure($index, pro)']")).size() != 0,
				flag;

		if (addBtnDisplay) {
			flag = true;
		} else {
			flag = false;
		}

		Assert.assertEquals(flag, true);
	}
	public void entityFilterBtnClick() throws Exception {
		entityFilterBtn.click();
		Thread.sleep(2000);
	}
	
	public void entityAddRuleBtnClick() throws Exception {
		entityAddRuleBtn.click();
		Thread.sleep(2000);
	}
	
	public void verifyRedirectFilterPage() {
		
	}
	
	public void verifyApplyEntityFilterLabel() {
		
		Assert.assertEquals(applyEntityFilterLabel.getText(),"Apply Entity Filter");
	}

	public void verifyDefineRuleLabelLabel() {

		Assert.assertEquals(defineRuleLabel.getText(), "Define Rule");
	}
	
	public void showLinkClick() throws Exception {
		showLink.click();
		Thread.sleep(1000);
	}
	public void hideLinkClick() throws Exception {
		hideLink.click();
		Thread.sleep(1000);
	}
	
	public void verifyShowLinkClickable() {
		boolean addBtnDisplay = ObjectRepo.driver
				.findElements(By.xpath("//button[@class='glyphicon glyphicon-chevron-right hide-show-icon ng-hide']")).size() != 0,
				flag;

		if (addBtnDisplay) {
			flag = true;
		} else {
			flag = false;
		}

		Assert.assertEquals(flag, true);
	}
	
	public void verifyHideLinkClickable() {
		boolean addBtnDisplay = ObjectRepo.driver
				.findElements(By.xpath("//button[@class='glyphicon glyphicon-chevron-right hide-show-icon ng-hide']")).size() != 0,
				flag;

		if (addBtnDisplay) {
			flag = true;
		} else {
			flag = false;
		}

		Assert.assertEquals(flag, false);
	}
	
	public void verifyAddProcedureVisible() {
		boolean addBtnDisplay = ObjectRepo.driver
				.findElements(By.xpath("//*[@class='btn btn-info ng-binding']")).size() != 0,
				flag;

		if (addBtnDisplay) {
			flag = true;
		} else {
			flag = false;
		}

		Assert.assertEquals(flag, true);
	}
	
	public void verifyAddProcedureInvisible() {
		boolean addBtnDisplay = ObjectRepo.driver
				.findElements(By.xpath("//*[@class='btn btn-info ng-binding']")).size() != 0,
				flag;

		if (addBtnDisplay) {
			flag = true;
		} else {
			flag = false;
		}

		Assert.assertEquals(flag, false);
	}
//	=================================== Chirag Kariya ===================================
	
	public void enterAllRequiredDataWorkflow(String entity, String procedure) throws Throwable {
		SelectEntityTextbox.sendKeys(entity);
		SelectEntityAutocomplete.click();
		addEntityBtnWorkflow.click();
		selectProcedureTextbox.clear();
		Thread.sleep(500);
		selectProcedureTextbox.sendKeys(procedure);
		Thread.sleep(500);
		selectProcedureAutocomplete.click();
		Thread.sleep(500);
		addProcedureBtnWorkflow.click();
	}

	public void selectProcedureInAutoComSendkeys(String proceName) throws InterruptedException {
		selectProcedureTextbox.clear();
		Thread.sleep(500);
//		"DemoP_MJ"
		selectProcedureTextbox.sendKeys(proceName);
		Thread.sleep(500);
		selectProcedureAutocomplete.click();
		Thread.sleep(500);
	}
	
	public void clickContinueBtnWorkflowTab() throws Exception {
		continueBtnWorkflowTab.click();
		Thread.sleep(2000);
	}
	
	public void clickContinueBtnAcceptanceTab() throws Exception {
		continueBtnAcceptanceTab.click();
		Thread.sleep(2000);
	}
	
	public void clickPublishContinueBtnPreviewTab() throws Exception {
		publishContinueBtnPreviewTab.click();
		Thread.sleep(2000);
	}
	
	public void clickSaveBtnPermissionTab() throws Exception {
		clickBuildAppCheckbox.click();
		clickOtherCheckbox.click();
		saveBtnPermissionTab.click();
		Thread.sleep(5000);
	}
	
	public void selectProcedureAcceptanceTab() throws Exception {
//		helper.SelectDrpDwnValueByText(selectProcedureAcceptance, " DemoP_MJ P1");
		addProcedureAcceptanceBtn.click();
	}
	
	public void verifyAddNewAppFunctionality() throws Exception {
		Assert.assertEquals(verifyNewApp.getText(), getAppTitle);
	}
	
	public void verifySaveBtnNavigateAppListings() throws Exception {
		boolean actual = appListingsScreen.isDisplayed();
		Assert.assertEquals(actual, true);
	}
	
	public void verifyAppNameInPermissionTab() throws Exception {
		Assert.assertEquals(appNamePermissionTab.getText(), getAppTitle);
	}
	
	public void verifyDefinedPermissionForApp() throws Exception {
		Assert.assertEquals(definePermissionApp.getText(), "Define Permissions for App.");
	}
	
	public void verifyDefinedPermissionForAppColor() throws Exception {
		Assert.assertEquals(helper.FontColor(definePermissionApp), "#0000ff");
	}
	
	public void verifyCurrentVersionOfPreviewTab() throws Exception {
		boolean actual = currentVersionPermissionTab.isDisplayed();
		Assert.assertEquals(actual, true);
	}
	
	public void verifyAppNameInPreviewTab() throws Exception {
		Assert.assertEquals(appNamePreviewTab.getText(), getAppTitle);
	}
	
	public void verifyNavigateToPermissionTab() throws Exception {
		Assert.assertEquals(appNamePreviewTab.getText(), getAppTitle);
	}
	
	public void verifyPermissionTabNavigation() throws Exception {
		boolean actual;
		if(driver.findElements(By.xpath("//p[@translate='secure.admin.schema.edit.permission.DEFINEPER']")).size() != 0)
		{
			actual = true;
		} else {
			actual = false;
		}
		
		Assert.assertEquals(actual, true);
	}
	
	public void verifyPermissionTabLabel() throws Exception {
		Assert.assertEquals(permissionTabNavigation.getText(), "6  PERMISSIONS");
	}
	
	public void verifyPermissionTabLabelColor() throws Exception {
		Assert.assertEquals(helper.BtnColor(permissionTabNavigation), "#006600");
	}

	public void verifyPreviewTabLabel() throws Exception {
		Assert.assertEquals(previewTabNavigation.getText(), "5  PREVIEW");
	}
	
	public void verifyPreviewTabLabelColor() throws Exception {
		Assert.assertEquals(helper.BtnColor(previewTabNavigation), "#006600");
	}
	
	public void verifyPermissionTabDisabled() throws Exception {
		boolean actual = nextTabDisabled.isEnabled();
		Assert.assertEquals(actual, true);
	}
	
	public void verifyPreviewTabInformationMsg() throws Exception {
		Assert.assertEquals(previewTabInformationMsg.getText(), "Preview the App before publishing.");
	}
	
	public void verifyPreviewTabInformationMsgColor() throws Exception {
		Assert.assertEquals(helper.FontColor(previewTabInformationMsg), "#0000ff");
	}
	
	public void selectProcedureDrpDwnAcceptance() throws Exception {
//		helper.SelectDrpDwnValueByText(selectProcedureAcceptance, " DemoP_MJ P1");
	}
	
	public void clickRemoveProcedureAcceptanceTab() throws Exception {
		Thread.sleep(500);
		removeProcedureAcceptanceTab.click();
	}
		
	public void verifyRemoveProcedureAcceptanceTabFunctionality() throws Exception {
		Thread.sleep(1000);
		boolean actual;
		if(driver.findElements(By.xpath("//h4[@class='text-center ng-binding']")).size() != 0)
		{
			actual = false;
		} else {
			actual = true;
		}
		Assert.assertEquals(actual, true);
	}
	
	public void verifyNavigateToPreviewTabFunctionality() throws Exception {
		boolean actual;
		if(driver.findElements(By.xpath("//p[@translate='secure.admin.schema.edit.preview.PUBLISHEDAPP']")).size() != 0)
		{
			actual = true;
		} else {
			actual = false;
		}
		Assert.assertEquals(actual, true);
	}
	
	public void verifyAddProcedureBtnVisibleAcceptanceTab() throws Exception {
		Thread.sleep(500);
		boolean actual = addProcedureAcceptanceBtn.isDisplayed();
		Assert.assertEquals(actual, true);
	}
	
	public void verifyAddProcedureBtnFunctionalAcceptanceTab() throws Exception {
		Thread.sleep(500);
		boolean actual;
		if(driver.findElements(By.xpath("//h4[@class='text-center ng-binding']")).size() != 0)
		{
			actual = true;
		} else {
			actual = false;
		}
		Assert.assertEquals(actual, true);
	}
	
	public void verifyAddProcedureBtnVisibleAcceptanceTabColor() throws Exception {
		Assert.assertEquals(helper.BtnColor(addProcedureAcceptanceBtn), "#337ab7");
	}
	
	public void verifyKeyAttributesInformationMsg() throws Exception {
		Assert.assertEquals(keyAttributeMsgAcceptanceTab.getText(), "Combination of selected 'Key Attributes' will be checked against any matching entry.");
	}
	
	public void verifyKeyAttributesInformationMsgColor() throws Exception {
		Assert.assertEquals(helper.FontColor(keyAttributeMsgAcceptanceTab), "#0000ff");
	}
	
	public void verifyDefinePassCriteriaInformationMsgColor() throws Exception {
		Assert.assertEquals(definePassCriteriaMsgAcceptanceTab.getText(), "Define Pass Criteria(s) on App");
	}
	
	public void verifyAddProcedureInformationMsg() throws Exception {
		Assert.assertEquals(addProcedureMsgAcceptanceTab.getText(), "Add Procedure(s) that will allow the App to Pass.");
	}
	
	public void verifyAddProcedureInformationMsgColor() throws Exception {
		Assert.assertEquals(helper.FontColor(addProcedureMsgAcceptanceTab), "#0000ff");
	}
	
	public void verifyAccpetanceCriteriaRedirection() throws Exception {
		boolean actual;
		if(driver.findElements(By.xpath("//p[@translate='secure.admin.schema.edit.acceptance.TALL']")).size() != 0)
		{
			actual = true;
		} else {
			actual = false;
		}
		Assert.assertEquals(actual, true);
	}
	
	public void verifyAcceptanceTabLabel() throws Exception {
		Assert.assertEquals(acceptanceTabNavigation.getText(), "4  ACCEPTANCE");
	}
	
	public void verifyAcceptanceTabLabelColor() throws Exception {
		Assert.assertEquals(helper.BtnColor(acceptanceTabNavigation), "#006600");
	}
	
	public void verifyAcceptanceTabAppLabel() throws Exception {
		Assert.assertEquals(acceptanceTabLabel.getText(), getAppTitle+" -App Acceptance Criteria");
	}
	
	public void verifyAcceptanceTabCurrentVersion() throws Exception {
		Assert.assertEquals(acceptanceTabCurrentVersion.getText(), "Current Version 1.0");
	}
	
	public void verifyAcceptanceTabCurrentVersionColor() throws Exception {
		Assert.assertEquals(helper.BtnColor(acceptanceTabNavigation), "#006600");
	}
	
	public void verifyPreviewTabDisabled() throws Exception {
		boolean actual = nextTabDisabled.isEnabled();
		Assert.assertEquals(actual, true);
	}
	
	public void verifyTrackUniqueRecordsMsg() throws Exception {
		Assert.assertEquals(trackUniqueRecordsMsg.getText(), "How do you want to track Unique Records?");
	}
	
	public void clickShowBtnOfProcedure() throws Exception {
		showBtnOfProcedureClick.click();
		Thread.sleep(2000);
	}
	
	public void clickFirstAcceptanceCriteria() throws Exception {
		firstAcceptacneCriteria.click();
		Thread.sleep(2000);
	}
	
	public void selectConditionDrpDwnVal() throws Exception {
		helper.SelectDrpDwnValueByIndex(conditionDrpDwn, 5);
		
	}
	
	
}// end
