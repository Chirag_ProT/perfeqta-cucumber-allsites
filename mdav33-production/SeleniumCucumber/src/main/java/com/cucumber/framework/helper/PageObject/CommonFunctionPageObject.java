package com.cucumber.framework.helper.PageObject;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cucumber.framework.helper.Logger.LoggerHelper;
import com.cucumber.framework.settings.ObjectRepo;

public class CommonFunctionPageObject extends PageBase
{
	int  a= 0;
    private WebDriver driver;
	
    public CommonFunctionPageObject(WebDriver driver)
	{
		super(driver);
		this.driver = driver;
	}
	private final Logger log = LoggerHelper.getLogger(AttributesPageObject.class);
	
	@FindBy(how = How.XPATH, using = "//*[@ng-model='vm.user.username']")
	public WebElement username;
	
	@FindBy(how = How.XPATH, using = "//*[@ng-model='vm.user.password']")
	public WebElement password;
	
	@FindBy(how = How.XPATH, using = "//*[@ng-click='vm.login()']")
	public WebElement btn_login;
	
	@FindBy(how = How.XPATH, using = "//span[@id='wootric-x']")
    public WebElement dismissClick;
	
	@FindBy(how = How.XPATH, using = "//*[@ui-sref='secure.admin.home']")
	public WebElement administrationTile;
	
	WebDriverWait wait =new WebDriverWait (ObjectRepo.driver,60);
	
	public void clicktologin() throws Exception {
		wait.until(ExpectedConditions.elementToBeClickable(btn_login));
		btn_login.click();
	}
	public void clickDismissIcon() throws Exception {
		wait.until(ExpectedConditions.elementToBeClickable(dismissClick));
        Thread.sleep(2000);
		dismissClick.click();
		Thread.sleep(2000);
    }
	public void clickAdministrationTile() throws Exception {
        log.info(administrationTile);
        Thread.sleep(1000);
        administrationTile.click();
    }

}
