package com.cucumber.framework.helper.PageObject;

import static org.junit.Assert.assertArrayEquals;

import java.awt.event.KeyAdapter;
import java.util.List;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.Assert;

import com.cucumber.framework.helper.Helper;
import com.cucumber.framework.helper.Button.ButtonHelper;
import com.cucumber.framework.helper.Logger.LoggerHelper;
import com.cucumber.framework.helper.Scroll.Scroll;
import com.cucumber.framework.helper.Search.Search;
import com.cucumber.framework.helper.TextBox.TextBoxHelper;
import com.cucumber.framework.settings.ObjectRepo;

public class CreateWindowPageObject extends PageBase {
	private WebDriver driver;
	public ButtonHelper btnHelper;
	private final Logger log = LoggerHelper.getLogger(CreateWindowPageObject.class);
	Helper helper = new Helper();

	public CreateWindowPageObject(WebDriver driver) {
		super(driver);
		this.driver = driver;
		btnHelper = new ButtonHelper(driver);
	}

	/** WebElements */

	@FindBy(how = How.XPATH, using = "//*[@ui-sref='secure.qualificationPerformance.home']")
	public WebElement qualificationPerformanceTile;
	
	@FindBy(how = How.XPATH, using = "//*[@ui-sref='secure.qualificationPerformance.createWindow']")
	public WebElement createWindowTile;
	
	@FindBy(how = How.XPATH, using = "//input[@name='windowName']")
	public WebElement createWindowTextfield;
	
	@FindBy(how = How.XPATH, using = "//select[@ng-model='vm.windows.module._id']")
	public WebElement moduleDrpDwn;
	
	@FindBy(how = How.XPATH, using = "//select[@ng-model='vm.windows.app._id']")
	public WebElement appDrpDwn;
	
	@FindBy(how = How.XPATH, using = "//select[@ng-model='vm.windows.windowType']")
	public WebElement windowTypeDrpDwn;
	
	@FindBy(how = How.XPATH, using = "//select[@ng-model='vm.windows.windowPlan']")
	public WebElement windowPlanDrpDwn;
	
	@FindBy(how = How.XPATH, using = "//select[@ng-model='vm.thisSampleSize']")
	public WebElement sampleSizeDrpDwn;
	
	@FindBy(how = How.XPATH, using = "//input[@name='sampleSize']")
	public WebElement sampleSizeTextField;
	
	@FindBy(how = How.XPATH, using = "//input[@name='populationSize']")
	public WebElement populationSizeTextfield;
	
	@FindBy(how = How.XPATH, using = "//select[@ng-model='vm.windows.windowPeriod.month']")
	public WebElement monthDrpDwn;
	
	@FindBy(how = How.XPATH, using = "//select[@ng-model='vm.windows.windowPeriod.year']")
	public WebElement yearDrpDwn;
	
	@FindBy(how = How.XPATH, using = "//input[@name='sendEmailTextBox']")
	public WebElement emailAddressTextfield;
	
	@FindBy(how = How.XPATH, using = "//button[@class='btn btn-success ng-binding ng-scope']")
	public WebElement saveBtn;
	
	@FindBy(how = How.XPATH, using = "//button[@class='confirm']")
	public WebElement okConfirmBtn;
	
	@FindBy(how = How.XPATH, using = "//p[@style='display: block;']")
	public WebElement confirmMsg;
	
	@FindBy(how = How.XPATH, using = "//p[@translate='secure.qualificationPerformance.edit.createwindow.WINDOWNAMEISREQUIRED']")
	public WebElement windowNameRequiredMsg;
	
	@FindBy(how = How.XPATH, using = "//p[@translate='secure.qualificationPerformance.edit.createwindow.WINDOWNAMESHOULDBEATLEAST2CHARACTERSLONG']")
	public WebElement windowNameMinimumMsg;
	
	@FindBy(how = How.XPATH, using = "//p[@translate='secure.qualificationPerformance.edit.createwindow.WINDOWNAMESHOULDNOTBEMORETHAN50CHARACTERSLONG']")
	public WebElement windowNameMaximumMsg;
	
	@FindBy(how = How.XPATH, using = "//p[@translate='secure.qualificationPerformance.edit.createwindow.WINDOWNAMEMUSTBEUNIQUE']")
	public WebElement windowNameUniqueMsg;
	
	@FindBy(how = How.XPATH, using = "//textarea[@name='windowDescription']")
	public WebElement descriptionTextArea;
	
	@FindBy(how = How.XPATH, using = "//p[@translate='secure.admin.groupsettings.edit.DESCRIPTIONSHOULDBEATLEAST2CHARACTERSLONG']")
	public WebElement descriptionMinimumMsg;
	
	@FindBy(how = How.XPATH, using = "//p[@translate='secure.qualificationPerformance.edit.createwindow.DESCRIPTIONSHOULDNOTBEMORETHAN800CHARACTERSLONG']")
	public WebElement descriptionMaximumMsg;
	
	@FindBy(how = How.XPATH, using = "//div[3]/h3[@class='ng-binding']")
	public WebElement appDetailsHeading;
	
	@FindBy(how = How.XPATH, using = "//div[4]/h3[@class='ng-binding']")
	public WebElement testTypeSamplePlanHeading;
	
	@FindBy(how = How.XPATH, using = "//div[5]/h3[@class='ng-binding']")
	public WebElement frequencySettingHeading;
	
	@FindBy(how = How.XPATH, using = "//p[@translate='import.modr']")
	public WebElement moduleDrpDwnRequiredMsg;
	
	@FindBy(how = How.XPATH, using = "//p[@translate='text.ps2']")
	public WebElement appDrpDwnRequiredMsg;
	
	@FindBy(how = How.XPATH, using = "//select[@name='attributeSelect0']")
	public WebElement attributeEntityDrpDwn;
	
	@FindBy(how = How.XPATH, using = "//p[@class='err-message ng-binding ng-scope']")
	public WebElement attributeEntityRequiredMsg;
	
	@FindBy(how = How.XPATH, using = "//input[@name='attributeTextbox0']")
	public WebElement attributeEntityValueTextfield;
	
	@FindBy(how = How.XPATH, using = "//button[@ng-click='vm.addAttribute($index)']")
	public WebElement attributeEntityAddIcon;
	
	@FindBy(how = How.XPATH, using = "//input[@name='attributeTextbox1']")
	public WebElement attributeEntityAddMultiple;
	
	@FindBy(how = How.XPATH, using = "//button[@ng-click='vm.removeAttribute($index)']")
	public WebElement attributeEntityRemove;
	
	@FindBy(how = How.XPATH, using = "//input[@aria-checked='true' and @name='Procedure']")
	public WebElement procedureByDefaulSelected;
	
	@FindBy(how = How.XPATH, using = "//input[@name='Procedure']")
	public WebElement procedureSelected;
	
	@FindBy(how = How.XPATH, using = "//input[@name='Question']")
	public WebElement questionSelected;
	
	@FindBy(how = How.XPATH, using = "//select[@name='testTypeProcedure']")
	public WebElement procedureDrpDwn;
	
	@FindBy(how = How.XPATH, using = "//p[@translate='secure.logireports.analysisreports.drilldown.PROC']")
	public WebElement procedureDrpDwnRequiredMsg;
	
	@FindBy(how = How.XPATH, using = "//select[@name='testTypeQuestion']")
	public WebElement questionDrpDwn;
	
	@FindBy(how = How.XPATH, using = "//p[@translate='secure.qualificationPerformance.edit.createwindow.QUESTIONISREQUIRED']")
	public WebElement questionDrpDwnRequiredMsg;
	
	@FindBy(how = How.XPATH, using = "//p[@translate='secure.qualificationPerformance.edit.createwindow.WINDOWTYPEISREQUIRED']")
	public WebElement windowTypeRequiredMsg;
	
	@FindBy(how = How.XPATH, using = "//p[@translate='secure.qualificationPerformance.edit.createwindow.WINDOWPLANISREQUIRED']")
	public WebElement windowPlanRequiredMsg;
	
	@FindBy(how = How.XPATH, using = "//p[@translate='secure.qualificationPerformance.edit.createwindow.SAMPLESIZEISREQUIRED']")
	public WebElement sampleSizeRequiredMsg;
	
	@FindBy(how = How.XPATH, using = "//input[@name='maximumAllowedProcessFailure1']")
	public WebElement maximumAllowedProcessRequiredMsg;
	
	@FindBy(how = How.XPATH, using = "//input[@type='checkbox']")
	public WebElement checkAllowSecondStageCheckbox;
	
	@FindBy(how = How.XPATH, using = "//input[@name='allowSecondStage' and @aria-checked='true']")
	public WebElement checkAllowSecondStageCheckboxSelected;
	
	@FindBy(how = How.XPATH, using = "//p[@class='err-message ng-binding ng-scope']")
	public WebElement collectionDateRequiredMsg;
	
	@FindBy(how = How.XPATH, using = "//p[@translate='text.monthisrequired']")
	public WebElement monthRequiredMsg;
	
	@FindBy(how = How.XPATH, using = "//p[@translate='text.yearisrequired']")
	public WebElement yearRequiredMsg;
	
	@FindBy(how = How.XPATH, using = "//p[@ng-if='vm.windowStartDate']")
	public WebElement monthYearMsg;
	
	@FindBy(how = How.XPATH, using = "//input[@name='repeatWindow']")
	public WebElement repeatWindowCheckbox;
	
	@FindBy(how = How.XPATH, using = "//input[@name='repeatWindow' and @aria-checked='true']")
	public WebElement repeatWindowCheckboxStatus;
	
	@FindBy(how = How.XPATH, using = "//span[@class='glyphicon glyphicon-info-sign text-primary hoversymbol repeat-window-size']")
	public WebElement informationIcon;
	
	@FindBy(how = How.XPATH, using = "//div[@class='tooltip ng-isolate-scope top fade in']")
	public WebElement informationIconMsg;
	
	@FindBy(how = How.XPATH, using = "//input[@aria-checked='true' and @value='All']")
	public WebElement siteAllIsDefaultSelected;
	
	@FindBy(how = How.XPATH, using = "//input[@value='All']")
	public WebElement selectSiteAll;
	
	@FindBy(how = How.XPATH, using = "//input[@value='Selection']")
	public WebElement selectSiteSelection;
	
	@FindBy(how = How.XPATH, using = "//em[@title='At least one site is required from each level']")
	public WebElement selectionRequiredMsg;
	
	@FindBy(how = How.XPATH, using = "//input[@ng-model='vm.allSelected[level._id]']")
	public WebElement clickAllCheckbox;
	
	@FindBy(how = How.XPATH, using = "//span[@ng-bind-html='value']")
	public WebElement clickAllCheckboxMsg;
	
	@FindBy(how = How.XPATH, using = "//ul/li[2]/div[2]/input")
	public WebElement clickAllCheckboxDepartment;
	
	@FindBy(how = How.XPATH, using = "//ul/li[3]/div[2]/input")
	public WebElement clickAllCheckboxSites;
	
	@FindBy(how = How.XPATH, using = "//p[@translate='secure.qualificationPerformance.edit.createwindow.POPULATIONSIZEISREQUIRED']")
	public WebElement populationRequiredMsg;
	
	@FindBy(how = How.XPATH, using = "//p[@translate='secure.qualificationPerformance.edit.createwindow.THEPOPULATIONSIZESHOULDBEGREATERTHAN0']")
	public WebElement populationMinimumMsg;
	
	@FindBy(how = How.XPATH, using = "//p[@translate='secure.qualificationPerformance.edit.createwindow.POPULATIONSIZESHOULDNOTBEMORETHAN10000']")
	public WebElement populationMaximumMsg;
	
	@FindBy(how = How.XPATH, using = "//p[@translate='secure.qualificationPerformance.edit.createwindow.SAMPLESIZESHOULDBEATLEAST1']")
	public WebElement sampleSizeMinimumMsg;
	
	@FindBy(how = How.XPATH, using = "//p[@translate='secure.qualificationPerformance.edit.createwindow.SAMPLESIZESHOULDNOTBEMORETHAN999']")
	public WebElement sampleSizeMaximumMsg;
	
	@FindBy(how = How.XPATH, using = "//input[@name='maximumAllowedProcessFailure']")
	public WebElement maximumAllowedProcessFailureTextfield;
	
	@FindBy(how = How.XPATH, using = "//p[@translate='secure.qualificationPerformance.edit.createwindow.MAXIMUMALLOWEDPROCESSFAILUREINPERIODISREQUIRED']")
	public WebElement maximumAllowedProcessFailureRequiredMsg;
	
	@FindBy(how = How.XPATH, using = "//p[@translate='secure.qualificationPerformance.edit.createwindow.MAXIMUMALLOWEDPROCESSFAILUREINPERIODSHOULDBEATLEAST1']")
	public WebElement maximumAllowedProcessFailureMinimumMsg;
	
	@FindBy(how = How.XPATH, using = "//p[@translate='secure.qualificationPerformance.edit.createwindow.MAXIMUMALLOWEDPROCESSFAILUREINPERIODSHOULDNOTBEMORETHAN99']")
	public WebElement maximumAllowedProcessFailureMaximumMsg;
	
	@FindBy(how = How.XPATH, using = "//p[@translate='secure.qualificationPerformance.edit.createwindow.MAXIMUMALLOWEDPROCESSFAILUREINPERIODSHOULDNOTBEMORETHANSAMPLESIZE']")
	public WebElement sampleSizeLessThan;
	
	@FindBy(how = How.XPATH, using = "//input[@value='Hourly']")
	public WebElement hourlyRadioBtn;
	
	@FindBy(how = How.XPATH, using = "//input[@value='Weekly']")
	public WebElement weeklyRadioBtn;
	
	@FindBy(how = How.XPATH, using = "//input[@value='Monthly']")
	public WebElement monthlyRadioBtn;
	
	@FindBy(how = How.XPATH, using = "//a[@ng-click='vm.clearFrequencyAlert(createWindowForm)']")
	public WebElement windowFrequencyClearLink;
	
	@FindBy(how = How.XPATH, using = "//input[@aria-checked='true' and @value='Hourly']")
	public WebElement hourlyRadioClickable;
	
	@FindBy(how = How.XPATH, using = "//input[@aria-checked='true' and @value='Weekly']")
	public WebElement weeklyRadioClickable;
	
	@FindBy(how = How.XPATH, using = "//input[@aria-checked='true' and @value='Monthly']")
	public WebElement monthlyRadioClickable;
	
	@FindBy(how = How.XPATH, using = "//div/div[1]/div[2]/div/div[2]/div[1]")
	public WebElement hourlyColumnLabel;
	
	@FindBy(how = How.XPATH, using = "//p[@translate='secure.admin.schedule.edit.EVERYHOURISREQUIRED']")
	public WebElement hourlyRequiredMsg;
	
	@FindBy(how = How.XPATH, using = "//input[@name='hourlyTextBox']")
	public WebElement hourlyTextfield;
	
	@FindBy(how = How.XPATH, using = "//p[@translate='secure.qualificationPerformance.edit.createwindow.INVALIDNUMBERSHOULDBEATLEAST1']")
	public WebElement hourlyMinimumMsg;
	
	@FindBy(how = How.XPATH, using = "//p[@translate='secure.qualificationPerformance.edit.createwindow.INVALIDNUMBERCANNOTADDMORETHAN24HOURS']")
	public WebElement hourlyMaximumMsg;
	
	@FindBy(how = How.XPATH, using = "//p[@ng-if='!vm.windows.windowFrequencyStatusAlert.weekly.repeats.days.length']")
	public WebElement weeklyRequiredMsg;
	
	@FindBy(how = How.XPATH, using = "//input[@name='sendEmailTextBox']")
	public WebElement emailTextfield;
	
	@FindBy(how = How.XPATH, using = "//p[@translate='secure.admin.user.myprofile.myprofile.EMAILREQ']")
	public WebElement emailRequiredMsg;
	
	@FindBy(how = How.XPATH, using = "//p[@translate='secure.admin.user.myprofile.myprofile.INVALIDEMAIL']")
	public WebElement emailInvalidMsg;
	
	@FindBy(how = How.XPATH, using = "//p[@translate='secure.importdata.automated.Duplicate']")
	public WebElement emailDuplicateMsg;
	
	@FindBy(how = How.XPATH, using = "//p[@translate='secure.admin.schedule.edit.FORMULTIPLEEMAILADDRESSESUSECOMMA']")
	public WebElement emailInformationMsg;
	
	@FindBy(how = How.XPATH, using = "//button[@ui-sref='secure.qualificationPerformance.home']")
	public WebElement cancelBtn;
	
	
	
	
	
	
	
	

		// --------------------public methods--------------------

	public WebDriver getDriver() {
		return this.driver;

	}
	
	public void clickQualificationPerformanceTile() throws Exception {
		Thread.sleep(2000);
		qualificationPerformanceTile.click();
		Thread.sleep(1000);
	}

	public void clickCreateWindowTile() throws Exception {
		createWindowTile.click();
		Thread.sleep(1000);
	}
	
	public void enterValidData(String enterWindowName, String moduleSelect, String appSelect, String windowTypeSelect, String windowPlanSelect, String sampleSizeSelect, String monthSelect, String yearSelect, String emailAddress) throws Exception {
		createWindowTextfield.clear();
		createWindowTextfield.sendKeys(enterWindowName);
		Thread.sleep(500);
		helper.SelectDrpDwnValueByText(moduleDrpDwn, moduleSelect);
		Thread.sleep(500);
		helper.SelectDrpDwnValueByText(appDrpDwn, appSelect);
		Thread.sleep(500);
		helper.SelectDrpDwnValueByText(windowTypeDrpDwn, windowTypeSelect);
		Thread.sleep(500);
		helper.SelectDrpDwnValueByText(windowPlanDrpDwn, windowPlanSelect);
		Thread.sleep(500);
		helper.SelectDrpDwnValueByText(sampleSizeDrpDwn, sampleSizeSelect);
		Thread.sleep(500);
		helper.SelectDrpDwnValueByText(monthDrpDwn, monthSelect);
		Thread.sleep(500);
		helper.SelectDrpDwnValueByText(yearDrpDwn, yearSelect);
		Thread.sleep(500);
		emailAddressTextfield.clear();
		emailAddressTextfield.sendKeys(emailAddress);
	}
	
	public void clickSaveBtn() throws Exception {
		saveBtn.click();
		Thread.sleep(1000);
	}
	
	public void verifySaveFunctionality(String windowName) throws Exception {
//		int i=203;
		String actual = confirmMsg.getText();
		Assert.assertEquals(actual, windowName+" is created with Window ID i.");
		okConfirmBtn.click();
		Thread.sleep(1000);
	}
	
	public void emptyWindowNameField(String emptyValue) throws Exception {
		createWindowTextfield.sendKeys(emptyValue);
		createWindowTextfield.clear();
	}
	
	public void clickTabKey() throws Exception {
		createWindowTextfield.sendKeys(Keys.TAB);
	}
	
	public void verifyWindowNameRequiredMsg() throws Exception {
		Assert.assertEquals(windowNameRequiredMsg.getText(), "Window Name is required.");
	}
	
	public void verifyWindowNameRequiredMsgColor() throws Exception {
		Assert.assertEquals(helper.FontColor(windowNameRequiredMsg), "#a94442");
	}
	
	public void minWindowNameField(String minValue) throws Exception {
		createWindowTextfield.clear();
		TextBoxHelper tb = new TextBoxHelper(ObjectRepo.driver);
		tb.sendKeys(createWindowTextfield, minValue);
		
		//createWindowTextfield.sendKeys(minValue);
	}
	
	public void verifyWindowNameMinimumMsg() throws Exception {
		Assert.assertEquals(windowNameMinimumMsg.getText(), "Window Name must be at least 2 characters.");
	}
	
	public void verifyWindowNameMinimumMsgColor() throws Exception {
		Assert.assertEquals(helper.FontColor(windowNameMinimumMsg), "#a94442");
	}
	
	public void maxWindowNameField(String maxValue) throws Exception {
		createWindowTextfield.clear();
		createWindowTextfield.sendKeys(maxValue);
	}
	
	public void verifyWindowNameMaximumMsg() throws Exception {
		Assert.assertEquals(windowNameMaximumMsg.getText(), "Window Name cannot be more than 50 characters.");
	}
	
	public void verifyWindowNameMaximumMsgColor() throws Exception {
		Assert.assertEquals(helper.FontColor(windowNameMaximumMsg), "#a94442");
	}
	
	public void uniqueWindowNameField(String uniqueValue) throws Exception {
		createWindowTextfield.clear();
		createWindowTextfield.sendKeys(uniqueValue);
	}
	
	public void verifyWindowNameUniqueMsg() throws Exception {
		Assert.assertEquals(windowNameUniqueMsg.getText(), "Window Name must be unique.");
	}
	
	public void verifyWindowNameUniqueMsgColor() throws Exception {
		Assert.assertEquals(helper.FontColor(windowNameUniqueMsg), "#a94442");
	}
	
	public void clickDescriptionTextarea() throws Exception {
		descriptionTextArea.click();
	}
	
	public void minDescriptionTextarea(String minDesc) throws Exception {
		descriptionTextArea.clear();
		descriptionTextArea.sendKeys(minDesc);
	}
	
	public void verifyDescriptionMinimumMsg() throws Exception {
		Assert.assertEquals(descriptionMinimumMsg.getText(), "Description must be at least 2 characters.");
	}
	
	public void verifyDescriptionMinimumMsgColor() throws Exception {
		Assert.assertEquals(helper.FontColor(descriptionMinimumMsg), "#a94442");
	}
	
	public void maxDescriptionTextarea(String maxDesc) throws Exception {
		descriptionTextArea.clear();
		for(int i=0; i<15; i++)
		{
			descriptionTextArea.sendKeys(maxDesc);
		}
	}
	
	public void verifyDescriptionMaximumMsg() throws Exception {
		Assert.assertEquals(descriptionMaximumMsg.getText(), "Description cannot be more than 800 characters.");
	}
	
	public void verifyDescriptionMaximumMsgColor() throws Exception {
		Assert.assertEquals(helper.FontColor(descriptionMaximumMsg), "#a94442");
	}
	
	public void verifyAppDetailsHeading() throws Exception {
		boolean actual = appDetailsHeading.isDisplayed();
		Assert.assertEquals(actual, true);
	}
	
	public void verifyTestTypeSamplePlanHeading() throws Exception {
		boolean actual = testTypeSamplePlanHeading.isDisplayed();
		Assert.assertEquals(actual, true);
	}
	
	public void verifyFrequencySettingHeading() throws Exception {
		new Scroll().scrollTillElem(frequencySettingHeading);
		boolean actual = frequencySettingHeading.isDisplayed();
		Assert.assertEquals(actual, true);
	}
	
	public void verifyModuleDefaultValue() throws Exception {
		Assert.assertEquals(helper.getDrpDwnFirstSelectedOptn(moduleDrpDwn), "-- Please Select --");
	}
	
	public void verifyAppDefaultValue() throws Exception {
		Assert.assertEquals(helper.getDrpDwnFirstSelectedOptn(appDrpDwn), "-- Please Select --");
	}
	
	public void clickModuleDrpDwn() throws Exception {
		moduleDrpDwn.click();
	}
	
	public void verifyclickModuleDrpDwn() throws Exception {
		Assert.assertEquals(moduleDrpDwn.getText(), "-- Please Select --\n" + 
				"Blood\n" + 
				"Equipment\n" + 
				"Reagent QC");
	}
	
	public void selectModuleDrpDwn() throws Exception {
		helper.SelectDrpDwnValueByText(moduleDrpDwn, "Blood");
	}
	
	public void selectAppDrpDwn() throws Exception {
		helper.SelectDrpDwnValueByText(appDrpDwn, "Blood Sample Form");
	}
	
	public void verifyModuledrpDwnRequiredMsg() throws Exception {
		Assert.assertEquals(moduleDrpDwnRequiredMsg.getText(), "Module is required.");
	}
	
	public void verifyModuledrpDwnRequiredMsgColor() throws Exception {
		Assert.assertEquals(helper.FontColor(moduleDrpDwnRequiredMsg), "#a94442");
	}
	
	public void clickAppDrpDwn() throws Exception {
		appDrpDwn.click();
	}
	
	public void verifyAppDrpDwnRequiredMsg() throws Exception {
		Assert.assertEquals(appDrpDwnRequiredMsg.getText(), "App is required.");
	}
	
	public void verifyAppDrpDwnRequiredMsgColor() throws Exception {
		Assert.assertEquals(helper.FontColor(appDrpDwnRequiredMsg), "#a94442");
	}
	
	public void clickAttributeEntityDrpDwn() throws Exception {
		helper.SelectDrpDwnValueByText(attributeEntityDrpDwn, "DIN");
	}
	
	public void selectAttributeEntityDrpDwn() throws Exception {
		attributeEntityValueTextfield.sendKeys("1234");
	}
	
	public void verifyAttributeEntityDrpDwnRequiredMsg() throws Exception {
		Assert.assertEquals(attributeEntityRequiredMsg.getText(), "DIN is required.");
	}
	
	public void verifyAttributeEntityDrpDwnRequiredMsgColor() throws Exception {
		Assert.assertEquals(helper.FontColor(attributeEntityRequiredMsg), "#a94442");
	}
	
	public void verifyAttributeEntityValueTextfield() throws Exception {
		boolean actual = attributeEntityValueTextfield.isDisplayed();
		Assert.assertEquals(actual, true);
	}
	
	public void clickAddIconAttributeEntity() throws Exception {
		attributeEntityAddIcon.click();
	}
	
	public void verifyMultipleAddAttributeEntity() throws Exception {
		boolean actual = attributeEntityAddMultiple.isDisplayed();
		Assert.assertEquals(actual, true);
	}
	
	public void clickRemoveIconAttributeEntity() throws Exception {
		attributeEntityRemove.click();
	}
	
	public void verifyRemoveAttributeEntity() throws Exception {
		boolean actual;
		if(driver.findElements(By.xpath("//input[@name='attributeTextbox1']")).size() != 0)
		{
			actual = false;
		}
		else
		{
			actual = true;
		}
		Assert.assertEquals(actual, true);
	}
	
	public void verifyProcedureRadioBtnByDefault() throws Exception {
		boolean actual = procedureByDefaulSelected.isSelected();
		Assert.assertEquals(actual, true);
	}
	
	public void clickQuestionRadioBtn() throws Exception {
		questionSelected.click();
	}
	
	public void clickProcedureRadioBtn() throws Exception {
		procedureSelected.click();
	}
	
	public void verifyOneRadioBtnIsSelected() throws Exception {
		boolean actual;
		if(driver.findElements(By.xpath("//input[@aria-checked='true' and @name='Question']")).size() != 0)
		{
			actual = false;
		}
		else
		{
			actual = true;
		}
		Assert.assertEquals(actual, true);
	}
	
	public void clickProcedureTestTypeDrpDwn() throws Exception {
		helper.SelectDrpDwnValueByIndex(procedureDrpDwn, 1);
		Thread.sleep(1000);
		helper.SelectDrpDwnValueByText(procedureDrpDwn, "-- Please Select --");
	}
	
	public void verifyProcedureTestTypeDrpDwnRequiredMsg() throws Exception {
		Assert.assertEquals(procedureDrpDwnRequiredMsg.getText(), "Procedure is required.");
	}
	
	public void verifyProcedureTestTypeDrpDwnRequiredMsgColor() throws Exception {
		Assert.assertEquals(helper.FontColor(procedureDrpDwnRequiredMsg), "#a94442");
	}
	
	public void selectAppsDrpDwn() throws Exception {
		helper.SelectDrpDwnValueByText(appDrpDwn, "Patient1");
	}
	
	public void selectQuestionDrpDwn() throws Exception {
		clickQuestionRadioBtn();
		Thread.sleep(1000);
		helper.SelectDrpDwnValueByText(questionDrpDwn, "-- Please Select --");
	}
	
	public void verifyQuestionTestTypeDrpDwnRequiredMsg() throws Exception {
		Assert.assertEquals(questionDrpDwnRequiredMsg.getText(), "Question is required.");
	}
	
	public void verifyQuestionTestTypeDrpDwnRequiredMsgColor() throws Exception {
		Assert.assertEquals(helper.FontColor(questionDrpDwnRequiredMsg), "#a94442");
	}
	
	public void selectWindowTypeDrpDwn() throws Exception {
		helper.SelectDrpDwnValueByIndex(windowTypeDrpDwn, 1);
		Thread.sleep(1000);
		helper.SelectDrpDwnValueByText(windowTypeDrpDwn, "-- Please Select --");
	}
	
	public void verifyWindowTypeDrpDwnRequiredMsg() throws Exception {
		Assert.assertEquals(windowTypeRequiredMsg.getText(), "Window Type is required.");
	}
	
	public void verifyWindowTypeDrpDwnRequiredMsgColor() throws Exception {
		Assert.assertEquals(helper.FontColor(windowTypeRequiredMsg), "#a94442");
	}
	
	public void verifyWindowTypeDrpDwnOptions() throws Exception {
		Assert.assertEquals(windowTypeDrpDwn.getText(), "-- Please Select --\n" + 
				"Binomial Distribution\n" + 
				"Hypergeometric Distribution\n" + 
				"Customized Windows");
	}
	
	public void selectWindowTypeDrpDwnFirstOption() throws Exception {
		helper.SelectDrpDwnValueByIndex(windowTypeDrpDwn, 1);
	}
	
	public void clickWindowPlanDrpDwnFirst() throws Exception {
		windowPlanDrpDwn.click();
	}
	
	public void selectWindowPlanDrpDwnFirstOption() throws Exception {
		helper.SelectDrpDwnValueByIndex(windowPlanDrpDwn, 1);
	}
	
	public void selectWindowPlanDrpDwnDefaultOption() throws Exception {
		Thread.sleep(1000);
		helper.SelectDrpDwnValueByText(windowPlanDrpDwn, "-- Please Select --");
	}
	
	public void verifyWindowPlanDrpDwnRequiredMsg() throws Exception {
		Assert.assertEquals(windowPlanRequiredMsg.getText(), "Window Plan is required.");
	}
	
	public void verifyWindowPlanDrpDwnRequiredMsgColor() throws Exception {
		Assert.assertEquals(helper.FontColor(windowPlanRequiredMsg), "#a94442");
	}
	
	public void verifyWindowPlanDrpDwnOptions() throws Exception {
		Assert.assertEquals(windowPlanDrpDwn.getText(), "-- Please Select --\n" + 
				"95% / 95%\n" + 
				"95% / 75%\n" + 
				"95% / 90%");
	}
	
	public void selectBinomialWindowPlanDrpDwnOption() throws Exception {
		helper.SelectDrpDwnValueByText(windowTypeDrpDwn, "Binomial Distribution");
	}
	
	public void verifyAfterSelectBinomialOptionDisplayOtherDrpDwn() throws Exception {
		boolean plan = windowPlanDrpDwn.isDisplayed();
		boolean size = sampleSizeDrpDwn.isDisplayed();
		Assert.assertEquals(plan, size);
	}
	
	public void selectHypergeometricWindowPlanDrpDwnOption() throws Exception {
		helper.SelectDrpDwnValueByText(windowTypeDrpDwn, "Hypergeometric Distribution");
	}
	
	public void verifyAfterSelectHypergeometricOptionDisplayOtherDrpDwn() throws Exception {
		boolean population = populationSizeTextfield.isDisplayed();
		boolean size = sampleSizeDrpDwn.isDisplayed();
		Assert.assertEquals(population, size);
	}
	
	public void selectCustomizedWindowPlanDrpDwnOption() throws Exception {
		helper.SelectDrpDwnValueByText(windowTypeDrpDwn, "Customized Windows");
	}
	
	public void verifyAfterSelectCustomizedOptionDisplayOtherDrpDwn() throws Exception {
		boolean size = sampleSizeTextField.isDisplayed();
		Assert.assertEquals(true, size);
	}
	
	public void verifySampleSizeDrpDwnIsDisabled() throws Exception {
		boolean size = sampleSizeDrpDwn.isEnabled();
		Assert.assertEquals(false, size);
	}
	
	public void clickSampleSizeDrpDwn() throws Exception {
		helper.SelectDrpDwnValueByIndex(sampleSizeDrpDwn, 1);
		Thread.sleep(1000);
		helper.SelectDrpDwnValueByText(sampleSizeDrpDwn, "-- Please Select --");
	}
	
	public void verifySampleSizeDrpDwnRequiredMsg() throws Exception {
		Assert.assertEquals(sampleSizeRequiredMsg.getText(), "Sample Size is required.");
	}
	
	public void verifySampleSizeDrpDwnRequiredMsgColor() throws Exception {
		new Scroll().scrollTillElem(windowPlanDrpDwn);
		Assert.assertEquals(helper.FontColor(sampleSizeRequiredMsg), "#a94442");
	}
	
	public void verifyMaximumAllowedProcessFailurePeriodIsDisabled() throws Exception {
		boolean size = maximumAllowedProcessRequiredMsg.isEnabled();
		Assert.assertEquals(false, size);
	}
	
	public void selectWindowPlanDrpDwnValue() throws Exception {
		helper.SelectDrpDwnValueByIndex(windowPlanDrpDwn, 1);
	}
	
	public void selectSampleSizeDrpDwnValue() throws Exception {
		helper.SelectDrpDwnValueByIndex(sampleSizeDrpDwn, 1);
	}
	
	public void checkMaxAllowedProcessFailureIsDisabled() throws Exception {
		checkAllowSecondStageCheckbox.click();
	}
	
	public void verifyMaxAllowedProcessFailureIsDisabled() throws Exception {
		checkAllowSecondStageCheckbox.click();
	}
	
	public void verifyMaxAllowedProcessFailureClickable() throws Exception {
		boolean actual = checkAllowSecondStageCheckboxSelected.isSelected();
		Assert.assertEquals(actual, true);
	}
	
	public void selectCollectedDateAttributeEntityDrpDwn() throws Exception {
		helper.SelectDrpDwnValueByText(attributeEntityDrpDwn, "Collection Date");
	}
	
	public void verifyCollectionRequiredMsg() throws Exception {
		Assert.assertEquals(collectionDateRequiredMsg.getText(), "Collection Date is required.");
	}
	
	public void verifyCollectionRequiredMsgColor() throws Exception {
		Assert.assertEquals(helper.FontColor(collectionDateRequiredMsg), "#a94442");
	}
	
	public void clickMonthDrpDwn() throws Exception {
		helper.SelectDrpDwnValueByIndex(monthDrpDwn, 1);
		Thread.sleep(2000);
		helper.SelectDrpDwnValueByIndex(monthDrpDwn, 0);
	}
	
	public void clickonMonthDrpDwn() throws Exception {
		new Scroll().scrollTillElem(emailAddressTextfield);
		Thread.sleep(1000);
		monthDrpDwn.click();
		
	}
	
	public void verifyMonthDrpDwnValues() throws Exception {
		Assert.assertEquals(monthDrpDwn.getText(), "-- Select Month --\n" + 
				"January\n" + 
				"February\n" + 
				"March\n" + 
				"April\n" + 
				"May\n" + 
				"June\n" + 
				"July\n" + 
				"August\n" + 
				"September\n" + 
				"October\n" + 
				"November\n" + 
				"December");
	}
	
	public void verifyMonthDrpDwnRequiredMsg() throws Exception {
		Assert.assertEquals(monthRequiredMsg.getText(), "Month is required.");
	}
	
	public void verifyMonthDrpDwnRequiredMsgColor() throws Exception {
		Assert.assertEquals(helper.FontColor(monthRequiredMsg), "#a94442");
	}
	
	public void clickYearDrpDwn() throws Exception {
		helper.SelectDrpDwnValueByIndex(yearDrpDwn, 1);
		Thread.sleep(500);
		helper.SelectDrpDwnValueByIndex(yearDrpDwn, 0);
	}
	
	public void verifyYearDrpDwnDefaultValues() throws Exception {
		Assert.assertEquals(helper.getDrpDwnFirstSelectedOptn(yearDrpDwn), "-- Select Year --");
	}
	
	public void verifyYearDrpDwnRequiredMsg() throws Exception {
		Assert.assertEquals(yearRequiredMsg.getText(), "Year is required.");
	}
	
	public void verifyYearDrpDwnRequiredMsgColor() throws Exception {
		Assert.assertEquals(helper.FontColor(yearRequiredMsg), "#a94442");
	}
	
	public void selectCurrentMonthInDrpDwn(String month) throws Exception {
		helper.SelectDrpDwnValueByText(monthDrpDwn, month);
	}
	
	public void selectCurrentYearInDrpDwn(String year) throws Exception {
		helper.SelectDrpDwnValueByText(yearDrpDwn, year);
	}
	
	public void verifyMonthYearInformationMsg() throws Exception {
		Assert.assertEquals(monthYearMsg.getText(), "Start date will be Today for the current window.");
	}
	
	public void verifyMonthYearInformationMsgColor() throws Exception {
		Assert.assertEquals(helper.FontColor(monthYearMsg), "#0000ff");
	}
	
	public void selectNextMonthInDrpDwn(String nextMonth) throws Exception {
		helper.SelectDrpDwnValueByText(monthDrpDwn, nextMonth);
	}
	
	public void verifySecondMonthYearInformationMsg() throws Exception {
		Assert.assertEquals(monthYearMsg.getText(), "Start date will be 1st Day of the month for the current window.");
	}
	
	public void clickRepeatWindowCheckbox() throws Exception {
		repeatWindowCheckbox.click();
	}
	
	public void verifyClickableRepeatWindowCheckbox() throws Exception {
		boolean actual = repeatWindowCheckboxStatus.isSelected();
		Assert.assertEquals(actual, true);
	}
	
	public void mouseHoverInformation() throws Exception {
		informationIcon.click();
	}
	
	public void verifyMouseHoverInformationMsg() throws Exception {
		Assert.assertEquals(informationIconMsg.getText(), "Select this check box to automatically create the window for the next month at the end of the current month.");
	}
	
	public void verifySiteAllIsSelectedByDrfault() throws Exception {
		boolean actual = siteAllIsDefaultSelected.isSelected();
		new Scroll().scrollTillElem(siteAllIsDefaultSelected);
		Assert.assertEquals(actual, true);
	}
	
	public void clickSitesAllRadioBtn() throws Exception {
		selectSiteAll.click();
	}
	
	public void clickSitesSelectionRadioBtn() throws Exception {
		selectSiteSelection.click();
	}
	
	public void verifySiteSelectionRequiredMsg() throws Exception {
		new Scroll().scrollTillElem(selectionRequiredMsg);
		Assert.assertEquals(selectionRequiredMsg.getText(), "At least one site is required from each level.");
	}
	
	public void verifySiteSelectionRequiredMsgColor() throws Exception {
		new Scroll().scrollTillElem(selectionRequiredMsg);
		Assert.assertEquals(helper.FontColor(selectionRequiredMsg), "#a94442");
	}
	
	public void selectValueInModuleDrpDwn(String selectModule) throws Exception {
		moduleDrpDwn.sendKeys(selectModule);
	}
	
	public void selectValueInAppDrpDwn(String selectApp) throws Exception {
		Thread.sleep(1000);
		appDrpDwn.sendKeys(selectApp);
	}
	
	public void selectAllCheckboxOfSelection() throws Exception {
		appDrpDwn.sendKeys();
	}
	
	public void clickAllCheckboxOfSelection() throws Exception {
		clickAllCheckbox.click();
		Thread.sleep(500);
	}
	
	public void verifyAllSelectionOfRegion() throws Exception {
		new Scroll().scrollTillElem(monthDrpDwn);
		Assert.assertEquals(clickAllCheckboxMsg.getText(), "Department is required for Region MD Anderson Cancer Center - Updated");
	}
	
	public void verifyAllSelectionOfRegionColor() throws Exception {
		new Scroll().scrollTillElem(monthDrpDwn);
		Assert.assertEquals(helper.FontColor(clickAllCheckboxMsg), "#a94442");
	}
	
	public void clickAllCheckboxOfDepartment() throws Exception {
		clickAllCheckboxDepartment.click();
		Thread.sleep(500);
	}
	
	public void verifyAllSelectionOfDepatment() throws Exception {
		new Scroll().scrollTillElem(monthDrpDwn);
		Assert.assertEquals(clickAllCheckboxMsg.getText(), "Site is required for Department Donor Operations, Transfusion Service");
	}
	
	public void clickAllCheckboxOfSites() throws Exception {
		clickAllCheckboxSites.click();
		Thread.sleep(500);
	}
	
	public void verifyAllSelectionOfSitesNoMsg() throws Exception {
		new Scroll().scrollTillElem(monthDrpDwn);
		boolean sites = driver.findElements(By.xpath("//span[@ng-bind-html='value']")).size() == 0;
		boolean actual;
		if(sites == true)
		{
			actual = true;
		}
		else
		{
			actual = false;
		}
		Assert.assertEquals(actual, true);
	}
	
	public void selectHypergeometricOption() throws Exception {
		helper.SelectDrpDwnValueByIndex(windowTypeDrpDwn, 2);
		Thread.sleep(500);
	}
	
	public void clickPopulationTextbox() throws Exception {
		populationSizeTextfield.click();
		populationSizeTextfield.sendKeys(Keys.TAB);
	}
	
	public void verifyPopulationRequiredMsg() throws Exception {
		Assert.assertEquals(populationRequiredMsg.getText(), "Population Size is required.");
	}
	
	public void verifyPopulationRequiredMsgColor() throws Exception {
		Assert.assertEquals(helper.FontColor(populationRequiredMsg), "#a94442");
	}
	
	public void enterPopulationMinimumTextbox(String minValue) throws Exception {
		populationSizeTextfield.sendKeys(minValue);
	}
	
	public void verifyPopulationMinimumMsg() throws Exception {
		Assert.assertEquals(populationMinimumMsg.getText(), "The Population Size should be greater than 0.");
	}
	
	public void verifyPopulationMinimumMsgColor() throws Exception {
		Assert.assertEquals(helper.FontColor(populationMinimumMsg), "#a94442");
	}
	
	public void enterPopulationMaximumTextbox(String maxValue) throws Exception {
		populationSizeTextfield.sendKeys(maxValue);
	}
	
	public void verifyPopulationMaximumMsg() throws Exception {
		Assert.assertEquals(populationMaximumMsg.getText(), "Population Size must not be more than 10000.");
	}
	
	public void verifyPopulationMaximumMsgColor() throws Exception {
		Assert.assertEquals(helper.FontColor(populationMaximumMsg), "#a94442");
	}
	
	public void enterPopulationValidTextbox(String validValue) throws Exception {
		populationSizeTextfield.sendKeys(validValue);
	}
	
	public void verifyPopulationNoValidation() throws Exception {
		boolean min = driver.findElements(By.xpath("//p[@translate='secure.qualificationPerformance.edit.createwindow.THEPOPULATIONSIZESHOULDBEGREATERTHAN0']")).size() == 0;
		boolean max = driver.findElements(By.xpath("//p[@translate='secure.qualificationPerformance.edit.createwindow.POPULATIONSIZESHOULDNOTBEMORETHAN10000']")).size() == 0;
		boolean req = driver.findElements(By.xpath("//p[@translate='secure.qualificationPerformance.edit.createwindow.POPULATIONSIZEISREQUIRED']")).size() == 0;
		boolean actual;
		if(min == true && max == true && req == true)
		{
			actual = true;
		}
		else
		{
			actual = false;
		}
		Assert.assertEquals(actual, true);
	}
	
	public void selectCustomizedInWindowTypeDrpDwn() throws Exception {
		helper.SelectDrpDwnValueByIndex(windowTypeDrpDwn, 3);
	}
	
	public void clickSampleSizeTextbox() throws Exception {
		sampleSizeTextField.click();
	}
	
	public void enterSampleSizeMinimumValue(String minValue) throws Exception {
		sampleSizeTextField.sendKeys(minValue);
	}
	
	public void verifySampleSizeMinimumMsg() throws Exception {
		Assert.assertEquals(sampleSizeMinimumMsg.getText(), "Sample Size should be at least 1.");
	}
	
	public void verifySampleSizeMinimumMsgColor() throws Exception {
		Assert.assertEquals(helper.FontColor(sampleSizeMinimumMsg), "#a94442");
	}
	
	public void enterSampleSizeMaximumValue(String maxValue) throws Exception {
		sampleSizeTextField.sendKeys(maxValue);
	}
	
	public void verifySampleSizeMaximumMsg() throws Exception {
		Assert.assertEquals(sampleSizeMaximumMsg.getText(), "Sample Size must not be more than 999.");
	}
	
	public void verifySampleSizeMaximumMsgColor() throws Exception {
		Assert.assertEquals(helper.FontColor(sampleSizeMaximumMsg), "#a94442");
	}
	
	public void enterSampleSizeValidValue(String validValue) throws Exception {
		sampleSizeTextField.sendKeys(validValue);
	}
	
	public void verifySampleSizeNoValidation() throws Exception {
		boolean min = driver.findElements(By.xpath("//p[@translate='secure.qualificationPerformance.edit.createwindow.SAMPLESIZESHOULDBEATLEAST1']")).size() == 0;
		boolean max = driver.findElements(By.xpath("//p[@translate='secure.qualificationPerformance.edit.createwindow.SAMPLESIZESHOULDNOTBEMORETHAN999']")).size() == 0;
		boolean req = driver.findElements(By.xpath("//p[@translate='secure.qualificationPerformance.edit.createwindow.SAMPLESIZEISREQUIRED']")).size() == 0;
		boolean actual;
		if(min == true && max == true && req == true)
		{
			actual = true;
		}
		else
		{
			actual = false;
		}
		Assert.assertEquals(actual, true);
	}
	
	public void clickMaximumAllowedProcessFailureTextfield() throws Exception {
		maximumAllowedProcessFailureTextfield.click();
		maximumAllowedProcessFailureTextfield.sendKeys(Keys.TAB);
	}
	
	public void verifyMaximumAllowedProcessFailureRequiredMsg() throws Exception {
		Assert.assertEquals(maximumAllowedProcessFailureRequiredMsg.getText(), "Maximum Allowed Process Failure in Period is required.");
	}
	
	public void verifyMaximumAllowedProcessFailureRequiredMsgColor() throws Exception {
		Assert.assertEquals(helper.FontColor(maximumAllowedProcessFailureRequiredMsg), "#a94442");
	}
	
	public void enterMaximumAllowedProcessFailureMinimum(String minValue) throws Exception {
		maximumAllowedProcessFailureTextfield.sendKeys(minValue);
	}
	
	public void verifyMaximumAllowedProcessFailureMinimumMsg() throws Exception {
		Assert.assertEquals(maximumAllowedProcessFailureMinimumMsg.getText(), "Maximum Allowed Process Failure in Period should be at least 1.");
	}
	
	public void verifyMaximumAllowedProcessFailureMinimumMsgColor() throws Exception {
		Assert.assertEquals(helper.FontColor(maximumAllowedProcessFailureMinimumMsg), "#a94442");
	}
	
	public void enterMaximumAllowedProcessFailureMaximum(String maxValue) throws Exception {
		maximumAllowedProcessFailureTextfield.sendKeys(maxValue);
	}
	
	public void verifyMaximumAllowedProcessFailureMaximumMsg() throws Exception {
		Assert.assertEquals(maximumAllowedProcessFailureMaximumMsg.getText(), "Maximum Allowed Process Failure in Period must not be more than 99.");
	}
	
	public void verifyMaximumAllowedProcessFailureMaximumMsgColor() throws Exception {
		Assert.assertEquals(helper.FontColor(maximumAllowedProcessFailureMaximumMsg), "#a94442");
	}
	
	public void enterNumericValueSampleSizeTextfield(String lessThan) throws Exception {
		sampleSizeTextField.sendKeys(lessThan);
	}
	
	public void enterNumericValueMaximumAllowedProcessFailureTextfield(String greaterThan) throws Exception {
		maximumAllowedProcessFailureTextfield.sendKeys(greaterThan);
	}
	
	public void verifySampleSizeMinimumAllowedProcessFailureMaximumMsg() throws Exception {
		Assert.assertEquals(sampleSizeLessThan.getText(), "Maximum Allowed Process Failure in Period must not be more than Sample Size.");
	}
	
	public void verifySampleSizeMinimumAllowedProcessFailureMaximumMsgColor() throws Exception {
		Assert.assertEquals(helper.FontColor(sampleSizeLessThan), "#a94442");
	}
	
	public void enterValidNumericValueMaximumAllowedProcessFailureTextfield(String validValue) throws Exception {
		maximumAllowedProcessFailureTextfield.sendKeys(validValue);
	}
	
	public void verifyValidNumericValueMaximumAllowedProcessFailureTextfield() throws Exception {
		boolean actual = driver.findElements(By.xpath("//p[@translate='secure.qualificationPerformance.edit.createwindow.MAXIMUMALLOWEDPROCESSFAILUREINPERIODSHOULDNOTBEMORETHANSAMPLESIZE']")).size() == 0;
		Assert.assertEquals(actual, true);
	}
	
	public void clickHourlyRadioBtn() throws Exception {
		hourlyRadioBtn.click();
	}
	
	public void clickWeeklyRadioBtn() throws Exception {
		weeklyRadioBtn.click();
		//new Scroll().scrollDown(ObjectRepo.driver);
		new Scroll().scrollTillElem(weeklyRadioBtn);
	}
	
	public void clickMonthlyRadioBtn() throws Exception {
		monthlyRadioBtn.click();
	}
	
	public void verifyHourlyRadioBtnClickable() throws Exception {
		boolean actual = hourlyRadioClickable.isSelected();
		Assert.assertEquals(actual, true);
	}
	
	public void verifyWeeklyRadioBtnClickable() throws Exception {
		boolean actual = weeklyRadioClickable.isSelected();
		Assert.assertEquals(actual, true);
	}
	
	public void verifyMontlyRadioBtnClickable() throws Exception {
		boolean actual = monthlyRadioClickable.isSelected();
		Assert.assertEquals(actual, true);
	}
	
	public void verifyWindowFrequencyClearLinkColor() throws Exception {
		Assert.assertEquals(helper.FontColor(windowFrequencyClearLink), "#337ab7");
	}
	
	public void clickWindowFrequencyClearLink() throws Exception {
		windowFrequencyClearLink.click();
	}
	
	public void verifyHourlyRadioBtnAfterClear() throws Exception {
		boolean actual;
		if(driver.findElements(By.xpath("//input[@aria-checked='true' and @value='Hourly']")).size() != 0)
		{
			actual = false;
		}else {
			actual = true;
		}
		Assert.assertEquals(actual, true);
	}
	
	public void verifyWeeklyRadioBtnAfterClear() throws Exception {
		boolean actual;
		if(driver.findElements(By.xpath("//input[@aria-checked='true' and @value='Weekly']")).size() != 0)
		{
			actual = false;
		}else {
			actual = true;
		}
		Assert.assertEquals(actual, true);
	}
	
	public void verifyMontlyRadioBtnAfterClear() throws Exception {
		boolean actual;
		if(driver.findElements(By.xpath("//input[@aria-checked='true' and @value='Monthly']")).size() != 0)
		{
			actual = false;
		}else {
			actual = true;
		}
		Assert.assertEquals(actual, true);
	}
	
	public void verifyHourlyColumnLabel() throws Exception {
		String actual = hourlyColumnLabel.getText();
		Assert.assertEquals(actual, "Every\n" + 
				"hour(s)");
	}
	
	public void verifyHourlyRequiredMsg() throws Exception {
		Assert.assertEquals(hourlyRequiredMsg.getText(), "Every Hour(s) is required.");
	}
	
	public void verifyHourlyRequiredMsgColor() throws Exception {
		Assert.assertEquals(helper.FontColor(hourlyRequiredMsg), "#a94442");
	}
	
	public void clickHourlyTextfield() throws Exception {
		hourlyTextfield.click();
	}
	
	public void enterMinimumHourlyTextfield(String minValue) throws Exception {
		hourlyTextfield.sendKeys(minValue);
	}
	
	public void verifyHourlyMinimumMsg() throws Exception {
		Assert.assertEquals(hourlyMinimumMsg.getText(), "Invalid Number.It must be at least 1.");
	}
	
	public void verifyHourlyMinimumMsgColor() throws Exception {
		Assert.assertEquals(helper.FontColor(hourlyMinimumMsg), "#a94442");
	}
	
	public void enterMaximumHourlyTextfield(String maxValue) throws Exception {
		hourlyTextfield.sendKeys(maxValue);
	}
	
	public void verifyHourlyMaximumMsg() throws Exception {
		Assert.assertEquals(hourlyMaximumMsg.getText(), "Invalid Number. You cannot add more than 24 Hours.");
	}
	
	public void verifyHourlyMaximumMsgColor() throws Exception {
		Assert.assertEquals(helper.FontColor(hourlyMaximumMsg), "#a94442");
	}
	
	public void enterValidHourlyTextfield(String validValue) throws Exception {
		hourlyTextfield.sendKeys(validValue);
	}
	
	public void verifyHourlyValidValue() throws Exception {
		boolean req = driver.findElements(By.xpath("//p[@translate='secure.admin.schedule.edit.EVERYHOURISREQUIRED']")).size() == 0;
		boolean min = driver.findElements(By.xpath("//p[@translate='secure.qualificationPerformance.edit.createwindow.INVALIDNUMBERSHOULDBEATLEAST1']")).size() == 0;
		boolean max = driver.findElements(By.xpath("//p[@translate='secure.qualificationPerformance.edit.createwindow.INVALIDNUMBERCANNOTADDMORETHAN24HOURS']")).size() == 0;
		boolean actual;
		if(min == true && max == true && req == true)
		{
			actual = false;
		}else {
			actual = true;
		}
		Assert.assertEquals(actual, false);
	}
	
	public void verifyWeeklyCheckboxAllSelectedByDefault() throws Exception {
		int count=0;
		for(int i=1; i<=7; i++)
		{
			if(driver.findElements(By.xpath("//div/div/div/label["+i+"]/input")).size() != 0)
			{
				count++;
			}
			else
			{
				System.out.println("... Else ...");
			}
		}
		Assert.assertEquals(count, 7);
	}
	
	public void clickAllWeeklyCheckbox() throws Exception {
		for(int i=1; i<=7; i++)
		{
			driver.findElement(By.xpath("//div/div/div/label["+i+"]/input")).click();
		}
	}
	
	public void verifyWeeklyCheckboxAllUnselected() throws Exception {
		int count=0;
		for(int i=1; i<=7; i++)
		{
			if(driver.findElements(By.xpath("//div/div/div/label["+i+"]/input")).size() == 0)
			{
				count++;
			}
			else
			{
				System.out.println("... Else ...");
			}
		}
		
		Assert.assertEquals(count, 0);
	}
	
	public void uncheckAllWeeklyCheckbox() throws Exception {
		for(int i=1; i<=7; i++)
		{
			driver.findElement(By.xpath("//div/div/div/label["+i+"]/input")).click();
		}
	}
	
	public void verifyWeeklyRequiredMsg() throws Exception {
		Assert.assertEquals(weeklyRequiredMsg.getText(), "Please select at least one day.");
	}
	
	public void verifyWeeklyRequiredMsgColor() throws Exception {
		Assert.assertEquals(helper.FontColor(weeklyRequiredMsg), "#a94442");
	}
	
	public void verifyWeeklyNoValidationMsg() throws Exception {
		boolean actual;
		if(driver.findElements(By.xpath("//p[@ng-if='!vm.windows.windowFrequencyStatusAlert.weekly.repeats.days.length']")).size() != 0)
		{
			actual = false;
		}else {
			actual = true;
		}
		Assert.assertEquals(actual, true);
	}
	
	public void clickEmailAddressTextfield() throws Exception {
		emailTextfield.click();
		emailTextfield.sendKeys(Keys.TAB);
	}
	
	public void verifyEmailAddressRequiredMsg() throws Exception {
		Assert.assertEquals(emailRequiredMsg.getText(), "Email Address is required.");
	}
	
	public void verifyEmailAddressRequiredMsgColor() throws Exception {
		Assert.assertEquals(helper.FontColor(emailRequiredMsg), "#a94442");
	}
	
	public void enterInvalidEmailAddress(String invalidEmail) throws Exception {
		emailTextfield.clear();
		emailTextfield.sendKeys(invalidEmail);
	}
	
	public void verifyInvalidEmailAddressMsg() throws Exception {
		Assert.assertEquals(emailInvalidMsg.getText(), "Invalid Email Address.");
	}
	
	public void verifyInvalidEmailAddressMsgColor() throws Exception {
		Assert.assertEquals(helper.FontColor(emailInvalidMsg), "#a94442");
	}
	
	public void verifyDuplicateEmailAddressMsg() throws Exception {
		Assert.assertEquals(emailDuplicateMsg.getText(), "Duplicate Email Address(es) found.");
	}
	
	public void verifyDuplicateEmailAddressMsgColor() throws Exception {
		Assert.assertEquals(helper.FontColor(emailDuplicateMsg), "#a94442");
	}
	
	public void enterDuplicateEmailAddress(String duplicateEmail) throws Exception {
		emailTextfield.clear();
		emailTextfield.sendKeys(duplicateEmail);
	}
	
	public void enterValidEmailAddress(String validEmail) throws Exception {
		emailTextfield.clear();
		emailTextfield.sendKeys(validEmail);
	}
	
	public void verifyValidEmailAddress() throws Exception {
		boolean req = driver.findElements(By.xpath("//p[@translate='secure.admin.user.myprofile.myprofile.EMAILREQ']")).size() == 0;
		boolean invalid = driver.findElements(By.xpath("//p[@translate='secure.admin.user.myprofile.myprofile.INVALIDEMAIL']")).size() == 0;
		boolean duplicate = driver.findElements(By.xpath("//p[@translate='secure.importdata.automated.Duplicate']")).size() == 0;
		boolean actual;
		if(invalid == true && duplicate == true && req == true)
		{
			actual = false;
		}else {
			actual = true;
		}
		Assert.assertEquals(actual, false);
	}
	
	public void verifyInformationEmailAddressMsg() throws Exception {
		Assert.assertEquals(emailInformationMsg.getText(), "Separate multiple Email Addresses with a comma(,).");
	}
	
	public void verifyInformationEmailAddressMsgColor() throws Exception {
		Assert.assertEquals(helper.FontColor(emailInformationMsg), "#0000ff");
	}
	
	public void verifyTextOfSaveBtn() throws Exception {
		Assert.assertEquals(saveBtn.getText(), "Save");
	}
	
	public void verifyTextOfSaveBtnColor() throws Exception {
		Assert.assertEquals(helper.BtnColor(saveBtn), "#5cb85c");
	}
	
	public void verifyTextOfCancelBtn() throws Exception {
		Assert.assertEquals(cancelBtn.getText(), "Cancel");
	}
	
	public void verifyTextOfCancelBtnColor() throws Exception {
		Assert.assertEquals(helper.BtnColor(cancelBtn), "#5a5a5a");
	}
	
	
	
	
	
	
	
	
	
	
	

}
