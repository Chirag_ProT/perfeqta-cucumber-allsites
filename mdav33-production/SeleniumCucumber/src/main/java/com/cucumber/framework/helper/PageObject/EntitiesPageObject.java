package com.cucumber.framework.helper.PageObject;

import java.util.List;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.Assert;
import com.cucumber.framework.helper.Button.ButtonHelper;
import com.cucumber.framework.helper.Logger.LoggerHelper;
import com.cucumber.framework.helper.Scroll.Scroll;
import com.cucumber.framework.helper.Search.Search;
import com.cucumber.framework.settings.ObjectRepo;

public class EntitiesPageObject extends PageBase {
	private WebDriver driver;
	public ButtonHelper btnHelper;
	private final Logger log = LoggerHelper.getLogger(EntitiesPageObject.class);

	public EntitiesPageObject(WebDriver driver) {
		super(driver);
		this.driver = driver;
		btnHelper = new ButtonHelper(driver);
	}

	/** WebElements */

	@FindBy(how = How.XPATH, using = "//*[@ui-sref='secure.admin.entities.list']")
	public WebElement entitiesTile;

	@FindBy(how = How.XPATH, using = "//*[@class='btn btn-primary add-new pull-right ng-binding']")
	public WebElement addNewBtn;

	@FindBy(how = How.XPATH, using = "//*[@class='currentStep ng-binding']")
	public WebElement pageTxtLabel;

	@FindBy(how = How.XPATH, using = "//*[@class='pull-right qc-form-tab-attributs']")
	public WebElement currVrsonTxtLbl;

	@FindBy(how = How.XPATH, using = "//*[@class='dropdown-toggle ng-binding btn btn-default']")
	public WebElement moduleDrpdwn;

	@FindBy(how = How.XPATH, using = "//*[@data-ng-click='selectAll()']")
	public WebElement chkAllBtn;

	@FindBy(how = How.XPATH, using = "//*[@data-ng-click='deselectAll();']")
	public WebElement unchkAllBtn;

	@FindBy(how = How.XPATH, using = "//*[@ng-model='searchFilter']")
	public WebElement moduleSrchBox;

	@FindBy(how = How.XPATH, using = "//*[@name='entityName']")
	public WebElement entyNameTxtBox;

	@FindBy(how = How.XPATH, using = "//*[@translate='secure.admin.entities.edit.entity.ENTITYNAMEISREQUIRED']")
	public WebElement entyNameTxtBoxReqMsg;

	@FindBy(how = How.XPATH, using = "//*[@translate='secure.admin.entities.edit.entity.ENTITYNAMESHOULDBEATLEAST2CHARACTERSLONG']")
	public WebElement entyNameTxtBoxMinMsg;

	@FindBy(how = How.XPATH, using = "//*[@translate='secure.admin.entities.edit.entity.ENTITYNAMESHOULDNOTBEMORETHAN200CHARACTERSLONG']")
	public WebElement entyNameTxtBoxMaxMsg;

	@FindBy(how = How.XPATH, using = "//*[@translate='secure.admin.entities.edit.entity.ENTITYNAMEMUSTBEUNIQUE']")
	public WebElement entyNameTxtBoxUniqMsg;

	@FindBy(how = How.XPATH, using = "//*[@name='entitytag']")
	public WebElement entyTagTxtBox;

	@FindBy(how = How.XPATH, using = "//*[@ng-if='vm.tag']")
	public WebElement entyTagAddBtn;

	@FindBy(how = How.XPATH, using = "//*[@translate='secure.admin.entities.edit.entity.ENTITYTAGISREQUIRED']")
	public WebElement entyTagTxtBoxReqMsg;

	@FindBy(how = How.XPATH, using = "//*[@translate='secure.admin.entities.edit.entity.ENTITYTAGSHOULDBEATLEAST2CHARACTERSLONG']")
	public WebElement entyTagTxtBoxMinMsg;

	@FindBy(how = How.XPATH, using = "//*[@translate='secure.admin.entities.edit.entity.ENTITYTAGSHOULDNOTBEMORETHAN20CHARACTERSLONG']")
	public WebElement entyTagTxtBoxMaxMsg;

	@FindBy(how = How.XPATH, using = "//*[@ng-if='!entityForm.entitytag.$invalid']")
	public WebElement entyTagTxtBoxUniqMsg;

	@FindBy(how = How.XPATH, using = "//*[@name='entityDescription']")
	public WebElement entyDscipTxtBox;

	@FindBy(how = How.XPATH, using = "//*[@translate='secure.admin.entities.edit.entity.DESCRIPTIONSHOULDBEATLEAST2CHARACTERSLONG']")
	public WebElement entyDscipTxtBoxMinMsg;

	@FindBy(how = How.XPATH, using = "//*[@translate='secure.admin.groupsettings.edit.DESCRIPTIONSHOULDBEATLEAST800CHARACTERSLONG']")
	public WebElement entyDscipTxtBoxMaxMsg;

	@FindBy(how = How.XPATH, using = "//*[@ng-change='vm.isInstructionForUser()']")
	public WebElement instForUsrChkBox;

	@FindBy(how = How.XPATH, using = "//*[@ng-change='vm.onChangeInstructionForUserTextbox()']")
	public WebElement instForUsrTxtBox;

	@FindBy(how = How.XPATH, using = "//*[@translate='secure.admin.procedure.edit.procedure.INSTRUCTIONFORUSERISREQUIRED']")
	public WebElement instForUsrTxtBoxRqiMsg;

	@FindBy(how = How.XPATH, using = "//*[@translate='secure.admin.procedure.edit.procedure.INSTRUCTIONFORUSERSHOULDBEATLEAST2CHARACTERSLONG']")
	public WebElement instForUsrTxtBoxMinMsg;

	@FindBy(how = How.XPATH, using = "//*[@translate='secure.admin.procedure.edit.procedure.INSTRUCTIONFORUSERSHOULDNOTBEMORETHAN800CHARACTERSLONG']")
	public WebElement instForUsrTxtBoxMaxMsg;

	@FindBy(how = How.XPATH, using = "//*[@name='instructionForUserRadioButtonUploadHelpDocument']")
	public WebElement instForUsrUpldDocRdoBtn;

	@FindBy(how = How.XPATH, using = "//*[@name='questionSelection']")
	public WebElement attriNameTxtBox;

	@FindBy(how = How.XPATH, using = "//*[@translate='secure.admin.procedure.edit.procedure.ATTRIBUTENAMEISREQUIRED']")
	public WebElement attriNameTxtBoxRqiMsg;

	@FindBy(how = How.XPATH, using = "//*[@translate='secure.admin.procedure.edit.procedure.ATTRIBUTENAMESHOULDBEATLEAST2CHARACTERSLONG']")
	public WebElement attriNameTxtBoxMinMsg;

	@FindBy(how = How.XPATH, using = "//*[@translate='secure.admin.procedure.edit.procedure.ATTRIBUTENAMESHOULDNOTBEMORETHAN200CHARACTERSLONG']")
	public WebElement attriNameTxtBoxMaxMsg;

	@FindBy(how = How.XPATH, using = "//*[@ng-model='vm.question.isKeyValue']")
	public WebElement attkeyIdchkBox;

	@FindBy(how = How.XPATH, using = "//*[@id=\"breadcrum-ipad-768\"]/li[3]/a")
	public WebElement entBrdCrum;

	@FindBy(how = How.XPATH, using = "//tr[1]/td[9]/span[1]/span/a")
	public WebElement entityViewLinkInformation;

	@FindBy(how = How.XPATH, using = "//*[contains(text(),'Your current Entity is used in these Apps:')]")
	public WebElement entityViewPopuplabel;
	// --------------------public methods--------------------

	public WebDriver getDriver() {
		return this.driver;

	}

	public void clickEntitiesTile() throws Exception {
		entitiesTile.click();
		Thread.sleep(2000);
	}

	public void clickAddNewBtn() throws Exception {
		addNewBtn.click();
		Thread.sleep(1000);
	}

	public void verifyAddEditTxtLabel() {
		Assert.assertEquals(pageTxtLabel.getText(), "Add / Edit Entity");
	}

	public void verifyEntitiesTxtLabel() {
		Assert.assertEquals(pageTxtLabel.getText(), "Entities");
	}

	public void verifyCurrVrsnTxtLabel() {
		Assert.assertEquals(currVrsonTxtLbl.getText(), "Current Version 1.00");
	}

	public void moduleDrpdwnClick() {
		moduleDrpdwn.click();
	}

	public void chkAllBtnClick() {
		chkAllBtn.click();
	}

	public void attkeyIdchkBoxClick() {
		attkeyIdchkBox.click();
	}

	public void unchkAllBtnClick() {
		unchkAllBtn.click();
	}

	public void verifyChkAllBtnFun() {

		int count = 6, CheckedValue = 0, expectedChkedValue = 3;

		// number of available modules
		int numOfModule = 3;

		for (int i = 0; i < numOfModule; i++) {
			if (ObjectRepo.driver.findElement(By.xpath("//div/ul/li[" + count + "]/a/div/label/input")).isSelected())

				CheckedValue++;
			count++;
		}
		System.out.println("Checked Values: " + CheckedValue);

		Assert.assertEquals(CheckedValue, expectedChkedValue);
	}

	public void verifyUnchkAllBtnFun() {

		int count = 6, CheckedValue = 0, expectedChkedValue = 0;

		// number of available modules
		int numOfModule = 3;

		for (int i = 0; i < numOfModule; i++) {
			if (ObjectRepo.driver.findElement(By.xpath("//div/ul/li[" + count + "]/a/div/label/input")).isSelected())

				CheckedValue++;
			count++;
		}
		System.out.println("Checked Values: " + CheckedValue);

		Assert.assertEquals(CheckedValue, expectedChkedValue);
	}

	public void moduleSrchBoxSendkeys(String SrchValue) {
		moduleSrchBox.sendKeys(SrchValue);

	}

	public void verifyModuleSrchBoxFun(String SrchValue) {

		String module = null, allModule = "";

		List<WebElement> srchResult = ObjectRepo.driver
				.findElements(By.xpath("//*[@ng-repeat='option in options | filter: searchFilter']"));

		for (int i = 0; i < srchResult.size(); i++) {
			module = srchResult.get(i).getText();
			allModule = module + allModule;
			System.out.println(srchResult.get(i).getText());
		}
		System.out.println(allModule);
		Assert.assertEquals(allModule, SrchValue);
	}

	public void entyNameTxtBoxSendkeys(String inputValue) {
		entyNameTxtBox.sendKeys(inputValue);
		try {
			Thread.sleep(850);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public void verifyEntyNameTxtBoxRequiValid() {

		Assert.assertEquals(entyNameTxtBoxReqMsg.getText(), "Entity Name is required.");
	}

	public void verifyEntyNameTxtBoxMinValid() {

		Assert.assertEquals(entyNameTxtBoxMinMsg.getText(), "Entiity Name must be at least 2 characters.");
	}

	public void verifyEntyNameTxtBoxMaxValid() {

		Assert.assertEquals(entyNameTxtBoxMaxMsg.getText(), "Entity Name cannot be more than 200 characters.");
	}

	public void verifyEntyNameTxtBoxUniqValid() {

		Assert.assertEquals(entyNameTxtBoxUniqMsg.getText(), "Entity Name must be unique.");
	}

	public void verifyEntyNameTxtBoxNoValid() {

		boolean reqMsg = ObjectRepo.driver
				.findElements(By.xpath("//*[@translate='secure.admin.entities.edit.entity.ENTITYNAMEISREQUIRED']"))
				.size() == 0,
				minMsg = ObjectRepo.driver.findElements(By.xpath(
						"//*[@translate='secure.admin.entities.edit.entity.ENTITYNAMESHOULDBEATLEAST2CHARACTERSLONG']"))
						.size() == 0,
				maxMsg = ObjectRepo.driver.findElements(By.xpath(
						"//*[@translate='secure.admin.entities.edit.entity.ENTITYNAMESHOULDNOTBEMORETHAN200CHARACTERSLONG']"))
						.size() == 0,
				uniqMsg = ObjectRepo.driver
						.findElements(
								By.xpath("//*[@translate='secure.admin.entities.edit.entity.ENTITYNAMEMUSTBEUNIQUE']"))
						.size() == 0,
				flag;

		if (reqMsg && minMsg && maxMsg && uniqMsg) {
			flag = true;
		} else {
			flag = false;
		}

		Assert.assertEquals(flag, true);
	}

	public void entyTagTxtBoxSendkeys(String inputValue) {
		entyTagTxtBox.sendKeys(inputValue);
		try {
			Thread.sleep(850);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public void entyTagAddBtnClick() {

		entyTagAddBtn.click();
	}

	public void verifyEntyTagTxtBoxRequiValid() {

		Assert.assertEquals(entyTagTxtBoxReqMsg.getText(), "Entity Tag is required.");
	}

	public void verifyEntyTagTxtBoxMinValid() {

		Assert.assertEquals(entyTagTxtBoxMinMsg.getText(), "Entity Tag must be at least 2 characters.");
	}

	public void verifyEntyTagTxtBoxMaxValid() {

		Assert.assertEquals(entyTagTxtBoxMaxMsg.getText(), "Entity Tag cannot be more than 20 characters.");
	}

	public void verifyEntyTagTxtBoxUniqValid() {

		Assert.assertEquals(entyTagTxtBoxUniqMsg.getText(), "Entity Tag already exist");
	}

	public void verifyEntyTagTxtBoxNoValid() {

		boolean reqMsg = ObjectRepo.driver
				.findElements(By.xpath("//*[@translate='secure.admin.entities.edit.entity.ENTITYTAGISREQUIRED']"))
				.size() == 0,
				minMsg = ObjectRepo.driver.findElements(By.xpath(
						"//*[@translate='secure.admin.entities.edit.entity.ENTITYTAGSHOULDBEATLEAST2CHARACTERSLONG']"))
						.size() == 0,
				maxMsg = ObjectRepo.driver.findElements(By.xpath(
						"//*[@translate='secure.admin.entities.edit.entity.ENTITYTAGSHOULDNOTBEMORETHAN20CHARACTERSLONG']"))
						.size() == 0,
				uniqMsg = ObjectRepo.driver
						.findElements(
								By.xpath("//*[@translate='secure.admin.entities.edit.entity.ENTITYNAMEMUSTBEUNIQUE']"))
						.size() == 0,
				flag;

		if (reqMsg && minMsg && maxMsg && uniqMsg) {
			flag = true;
		} else {
			flag = false;
		}

		Assert.assertEquals(flag, true);
	}

	public void entyDscipTxtBoxSendkeys(String inputValue) {
		entyDscipTxtBox.sendKeys(inputValue);
		try {
			Thread.sleep(850);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public void verifyEntyDscipTxtBoxMinValid() {

		Assert.assertEquals(entyDscipTxtBoxMinMsg.getText(), "Description must be at least 2 characters.");
	}

	public void verifyEntyDscipTxtBoxMaxValid() {

		Assert.assertEquals(entyDscipTxtBoxMaxMsg.getText(), "Description cannot be more than 800 characters.");
	}

	public void verifyEntyDscipTxtBoxNoValid() {

		boolean minMsg = ObjectRepo.driver.findElements(By
				.xpath("//*[@translate='secure.admin.entities.edit.entity.DESCRIPTIONSHOULDBEATLEAST2CHARACTERSLONG']"))
				.size() == 0,
				maxMsg = ObjectRepo.driver.findElements(By.xpath(
						"//*[@translate='secure.admin.groupsettings.edit.DESCRIPTIONSHOULDBEATLEAST800CHARACTERSLONG']"))
						.size() == 0,
				flag;

		if (minMsg && maxMsg) {
			flag = true;
		} else {
			flag = false;
		}

		Assert.assertEquals(flag, true);
	}

	public void instForUsrTxtBoxSendkeys(String inputValue) {
		instForUsrTxtBox.sendKeys(inputValue);
		try {
			Thread.sleep(850);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public void instForUsrChkBoxClick() {

		instForUsrChkBox.click();
	}

	public void verifyInstForUsrTxtBoxRequiValid() {

		Assert.assertEquals(instForUsrTxtBoxRqiMsg.getText(), "User Instructions are required.");
	}

	public void verifyInstForUsrTxtBoxMinValid() {

		Assert.assertEquals(instForUsrTxtBoxMinMsg.getText(), "Instructions for user must be at least 2 characters.");
	}

	public void verifyInstForUsrTxtBoxMaxValid() {

		Assert.assertEquals(instForUsrTxtBoxMaxMsg.getText(),
				"Instructions for user cannot be more than 800 characters.");
	}

	public void verifyInstForUsrTxtBoxNoValid() {

		boolean reqMsg = ObjectRepo.driver
				.findElements(By
						.xpath("//*[@translate='secure.admin.procedure.edit.procedure.INSTRUCTIONFORUSERISREQUIRED']"))
				.size() == 0,
				minMsg = ObjectRepo.driver.findElements(By.xpath(
						"//*[@translate='secure.admin.procedure.edit.procedure.INSTRUCTIONFORUSERSHOULDBEATLEAST2CHARACTERSLONG']"))
						.size() == 0,
				maxMsg = ObjectRepo.driver.findElements(By.xpath(
						"//*[@translate='secure.admin.procedure.edit.procedure.INSTRUCTIONFORUSERSHOULDNOTBEMORETHAN800CHARACTERSLONG']"))
						.size() == 0,
				flag;

		if (reqMsg && minMsg && maxMsg) {
			flag = true;
		} else {
			flag = false;
		}

		Assert.assertEquals(flag, true);
	}

	public void verifyInstForUsrTxtBoxAppears() {

		boolean instForUsrTxtBox = ObjectRepo.driver
				.findElements(By.xpath("//*[@ng-change='vm.onChangeInstructionForUserTextbox()']")).size() != 0, flag;

		if (instForUsrTxtBox) {
			flag = true;
		} else {
			flag = false;
		}
		Assert.assertEquals(flag, true);
	}

	public void verifyinstForUsrUpldDocAppears() {

		boolean instForUsrTxtBox = ObjectRepo.driver
				.findElements(By.xpath("//*[@title='Upload Document from Local Drive']")).size() != 0, flag;

		if (instForUsrTxtBox) {
			flag = true;
		} else {
			flag = false;
		}
		Assert.assertEquals(flag, true);
	}

	public void instForUsrUpldDocRdoBtnClick() {

		instForUsrUpldDocRdoBtn.click();
	}

	public void attriNameTxtBoxSendkeys(String inputValue) {
		attriNameTxtBox.sendKeys(inputValue);
		try {
			Thread.sleep(850);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public void verifyAttriNameTxtBoxRequiValid() {

		Assert.assertEquals(attriNameTxtBoxRqiMsg.getText(), "Attribute Name is required.");
	}

	public void verifyAttriNameTxtBoxMinValid() {

		Assert.assertEquals(attriNameTxtBoxMinMsg.getText(), "Attribute Name must be at least 2 characters.");
	}

	public void verifyAttriNameTxtBoxMaxValid() {

		Assert.assertEquals(attriNameTxtBoxMaxMsg.getText(), "Attribute Name cannot be more than 200 characters.");
	}

	public void verifyAttriNameTxtBoxNoValid() {

		boolean reqMsg = ObjectRepo.driver
				.findElements(
						By.xpath("//*[@translate='secure.admin.procedure.edit.procedure.ATTRIBUTENAMEISREQUIRED']"))
				.size() == 0,
				minMsg = ObjectRepo.driver.findElements(By.xpath(
						"//*[@translate='secure.admin.procedure.edit.procedure.ATTRIBUTENAMESHOULDBEATLEAST2CHARACTERSLONG']"))
						.size() == 0,
				maxMsg = ObjectRepo.driver.findElements(By.xpath(
						"//*[@translate='secure.admin.procedure.edit.procedure.ATTRIBUTENAMESHOULDNOTBEMORETHAN200CHARACTERSLONG']"))
						.size() == 0,
				flag;

		if (reqMsg && minMsg && maxMsg) {
			flag = true;
		} else {
			flag = false;
		}

		Assert.assertEquals(flag, true);
	}

	public void verifyLnkToEntyRdoNotAppears() {

		new Scroll().scrollDown(ObjectRepo.driver);

		boolean LnkToEntyRdo = ObjectRepo.driver.findElements(By.xpath("//*[@name='linkToEntity']")).size() != 0, flag;

		if (LnkToEntyRdo) {
			flag = true;
		} else {
			flag = false;
		}
		Assert.assertEquals(flag, false);
	}

	public void verifysearch(String searchedItem) throws Exception {

		AttributesPageObject obj = new AttributesPageObject(ObjectRepo.driver);

		String PaginationVal = null, PaginationSelectedVal = null, srchResultNo = null;
		try {
			PaginationVal = obj.paginationText.getText();
			srchResultNo = obj.srcResultPageNo.getText();
			PaginationSelectedVal = obj.pageSizeDrp_GetSelectedValue();
			new Search().SearchVerification(PaginationVal, PaginationSelectedVal, obj.grid_data, searchedItem,obj.textSearch, srchResultNo);
		} catch (Exception e) {
			System.err.println(e);
		}

	}

	public void entBrdCrumClick() throws Exception {
		entBrdCrum.click();
		Thread.sleep(2000);
	}

	public void verifyRedrctToEntPage() {

		boolean LnkToEntyRdo = ObjectRepo.driver.findElements(By.xpath("//*[@id='breadcrum-ipad-768']/li[3]/a"))
				.size() != 0, flag;

		if (LnkToEntyRdo) {
			flag = true;
		} else {
			flag = false;
		}
		Assert.assertEquals(flag, false);
	}

	public void entityViewLinkClick() {
		entityViewLinkInformation.click();
	}

	public void verifyViewPopupLabel() {

		try {
			String ViewPopupLabel = entityViewPopuplabel.getText();
			Assert.assertEquals(ViewPopupLabel, "Your current Entity is used in these Apps:");
			Thread.sleep(1000);
			// ObjectRepo.driver.navigate().refresh();
		} catch (Exception e) {
			System.out.println(e.toString());
		}
	}

	// rough work
	public void uniq() {
		int[] a = { 1, 2, 3, 4, 5, 1, 2, 3, 4 };
		int count = 0;

		for (int i = 0; i < a.length; i++) {
			if (a[i] == count) {

				count = a[i];
			} else {

			}
			System.out.println("this is uniq: " + a.toString());
		}
	}

}// end
