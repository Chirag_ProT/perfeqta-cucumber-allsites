package com.cucumber.framework.helper.PageObject;


import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;

import com.cucumber.framework.helper.Helper;
import com.cucumber.framework.helper.Button.ButtonHelper;
import com.cucumber.framework.helper.DropDown.DropDownHelper;
import com.cucumber.framework.helper.Logger.LoggerHelper;
import com.cucumber.framework.helper.Scroll.Scroll;
import com.cucumber.framework.helper.Search.Search;
import com.cucumber.framework.settings.ObjectRepo;
import com.cucumber.framework.utility.Constant;
import com.cucumber.framework.utility.ExcelUtils;


import cucumber.api.java.lu.a;
import gherkin.lexer.Th;

import com.cucumber.framework.helper.Sorting.SortingOnColumn;

public class EntityRecordsPageObject extends PageBase
{
	private WebDriver driver;
	public ButtonHelper btnHelper;
	private final Logger log = LoggerHelper.getLogger(EntityRecordsPageObject.class);
	private SortingOnColumn sortColumn;
	private Search search;
	private Scroll scroll;
	public Helper helper = new Helper();
	private CommonFunctionPageObject commonFnPageObject;

	public EntityRecordsPageObject(WebDriver driver)
	{
		super(driver);
		this.driver = driver;
		btnHelper = new ButtonHelper(driver);
		sortColumn = new SortingOnColumn();
		search = new Search();
		scroll = new Scroll();
	}

	/** WebElements */

	@FindBy(how = How.XPATH, using = "//*[@ng-click=\"vm.go('secure.entityrecords.list',vm.allPer.entityRecords)\"]")
	public WebElement entityRecdsTile;

	@FindBy(how = How.XPATH, using = "//SPAN[@class='currentStep ng-binding'][text()='Entity Records']")
	public WebElement entityRecdsLabel;	

	@FindBy(how = How.XPATH, using = "//A[@href=''][text()='Home']")
	public WebElement entityBreadCrum;

	@FindBy(how = How.XPATH, using = "//SPAN[@ng-bind-html='step.ncyBreadcrumbLabel']")
	public WebElement breadCrum;

	@FindBy(how = How.XPATH, using = "(//A[@class='app-record-search-tab-normal ng-binding'])[2]")
	public WebElement entitySearchLabl;

	@FindBy(how = How.XPATH, using = "//*[@class='list']")
	public WebElement entityModule;

	public WebElement entityModuleChecked;

	@FindBy(how = How.XPATH, using = "//*[@class='ui-select-match']")
	public WebElement entitySearchBox;

	@FindBy(how = How.XPATH, using = "//*[@type='search']")
	public WebElement enterValue;

	@FindBy(how = How.XPATH, using = "//*[@class='ui-select-no-choice dropdown-menu']")
	public WebElement noEntityFound;

	@FindBy(how = How.XPATH, using = "//*[@title='Click to save your search']")
	public WebElement addToFavorite;

	@FindBy(how = How.XPATH, using = "//input[@name='searchNameTextbox']")
	public WebElement enterFavoriteName;

	@FindBy(how = How.XPATH, using = "//*[@ng-click='vm.saveData(favoriteSearchForm)']")
	public WebElement saveButton;

	@FindBy(how = How.XPATH, using = "(//*[@class='col-md-10 col-md-offset-1 alert alert-warning no-records ng-binding ng-scope'])[1]")
	public WebElement noRecordFoundAtFavorite;
	
	@FindBy(how = How.XPATH, using = "//*[@class='col-md-10 col-md-offset-1 alert alert-warning no-records ng-binding ng-scope']")
	public WebElement noRecordFoundAtEntityValue;
	//+ "(//*[@class='col-md-10 col-md-offset-1 alert alert-warning no-records ng-binding ng-scope'])[2]")

//	@FindBy(how = How.XPATH, using = "//*[@translate='text.FAVORITENAMEISREQUIRED']")
//	public WebElement favoriteNameMessage;
	
	@FindBy(how = How.XPATH, using = "//*[@class='err-message ng-scope']")
	public WebElement favoriteNameMessageValidation;
	
	@FindBy(how = How.XPATH, using = "//*[@class='btn btn-success ng-binding']")
	public WebElement goButton;
	
	@FindBy(how = How.XPATH, using = "//*[@class='col-sm-11 ng-binding']")
	public WebElement addToFavoriteLabelPopup;
	
	@FindBy(how = How.XPATH, using = "//*[@class='col-sm-1']")
	public WebElement favoritePopupCrossButton;
	
	@FindBy(how = How.XPATH, using = "(//*[@class='btn btn-danger11 ng-binding'])[1]")
	public WebElement favoritePopupCancelButton;
	
	@FindBy(how = How.XPATH, using = "//*[@class='form-group']/span[1]/input")
	public WebElement addToFavoriteMyFavRadioBtn;
	
	@FindBy(how = How.XPATH, using = "(//*[@ng-click='vm.favoriteClicked(data)'])[1]")
	public WebElement addToFavoriteFirstData;
	
	@FindBy(how = How.XPATH, using = "(//*[@class='glyphicon glyphicon-remove critearia-01 ng-click-active'])[1]")
	public WebElement clickOnRemoveButtonOfFirstRecordInMyFavorite;
	
	@FindBy(how = How.XPATH, using = "//*[@style='display: block;']")
	public WebElement removePopupMyFavoriteLabel;
	
	@FindBy(how = How.XPATH, using = "//*[@class='confirm']")
	public WebElement removePopupMyFavoriteYesButton;
	
	@FindBy(how = How.XPATH, using = "//*[@class='cancel']")
	public WebElement removePopupMyFavoriteNoButton;
	
	@FindBy(how = How.XPATH, using = "//*[@class='btn btn-danger11 ng-binding']")
	public WebElement clearAllButton;
	
	@FindBy(how = How.XPATH, using = "//*[@ng-if=\"vm.entityPermission.create\"]")
	public WebElement adBtnGrid;
	
	@FindBy(how = How.XPATH, using = "//*[@ng-click='vm.onDeleteAll()']")
	public WebElement delBtnGrid;
	
	@FindBy(how = How.XPATH, using = "//*[@name='attributeSelect0']")
	public WebElement filterByEnAtt;
	
	@FindBy(how = How.XPATH, using = "//*[@name='attributeTextbox0']")
	public WebElement filterByEnAttField;
	
	@FindBy(how = How.XPATH, using = "//*[@ng-click='selectPage(page + 1, $event)']")
	public WebElement gridPaggingNextBtn;


	// -----------------------------------public methods ----------------------------------------------------------
	public void entityRecdsTileClick() throws Exception {
		Thread.sleep(1000);
		entityRecdsTile.click();
	}

	public void verifyEntityRecdsLabel() throws Exception {
		Assert.assertEquals(entityRecdsLabel.getText(), "Entity Records");	
	}


	public void entityBrdCrumClick() throws InterruptedException {
		entityBreadCrum.click();
	}

	public void verifyBrdCrum() throws InterruptedException {
		Assert.assertEquals(breadCrum.getText(), "Home");
	}

	public void verifyRecdSerchLblEntitiy() throws InterruptedException {
		Assert.assertEquals(entitySearchLabl.getText(), "Entity Record Search");
	}

	public void verifyEntityRecordSearchColor() throws InterruptedException {
		Assert.assertEquals(new Helper().FontColor(entitySearchLabl), "#1896bb");
	}

	public void verifyEntityAllModuleNameforEntityRecordsModule() throws InterruptedException {
		Assert.assertEquals(entityModule.getText(), "Blood\n" + "Equipment\n" + "Reagent QC");
	}

	public void verifyEntityAllModuleCheckBoxforEntityRecordsModule() throws InterruptedException {
		int selectedModule = 0;

		List<WebElement> count = driver.findElements(By.xpath("//*[@ng-repeat='module in vm.filteredModules']"));


		for (int i = 0; i < count.size(); i++) {
			entityModuleChecked = ObjectRepo.driver.findElement(By.xpath("//*[@id='module" + i + "']"));

			if (entityModuleChecked.isSelected()) {
				selectedModule++;
			}
		}
		Assert.assertEquals(selectedModule, count.size());
	}

	public void uncheckCheckedModule() throws InterruptedException {

		List<WebElement> count = driver.findElements(By.xpath("//*[@ng-repeat='module in vm.filteredModules']"));


		for (int i = 0; i < count.size() ; i++ ) {
			entityModuleChecked = ObjectRepo.driver.findElement(By.xpath("//*[@id='module" + i + "']"));

			JavascriptExecutor js = (JavascriptExecutor)driver;		
			js.executeScript("arguments[0].click();", entityModuleChecked);

			Thread.sleep(1000);

		}
	}
	public void verifyEntityAllModuleUnCheckBoxforEntityRecordsModule() throws InterruptedException {
		int selectedModule = 0;

		List<WebElement> count = driver.findElements(By.xpath("//*[@ng-repeat='module in vm.filteredModules']"));


		for (int i = 0; i < count.size(); i++) {
			entityModuleChecked = ObjectRepo.driver.findElement(By.xpath("//*[@id='module" + i + "']"));

			if (entityModuleChecked.isSelected()) {

			}
			else {
				selectedModule++;
			}
		}
		Assert.assertEquals(selectedModule, count.size());
	}
	public void checkUnCheckedModule() throws InterruptedException {

		List<WebElement> count = driver.findElements(By.xpath("//*[@ng-repeat='module in vm.filteredModules']"));


		for (int i = 0; i < count.size() ; i++ ) {
			entityModuleChecked = ObjectRepo.driver.findElement(By.xpath("//*[@id='module" + i + "']"));

			JavascriptExecutor js = (JavascriptExecutor)driver;		
			js.executeScript("arguments[0].click();", entityModuleChecked);

			Thread.sleep(1000);

		}
	}

	public void clickSearchBoxEntity() {
		entitySearchBox.click();

	}

	public void enterWrongEntityName(String entityRecordValue) throws Exception {
		enterValue.sendKeys(entityRecordValue);
		enterValue.sendKeys(Keys.TAB);
	}

	public void verifyEntityNoModuleFoundEntityRecordSearch() throws InterruptedException {
		Assert.assertEquals(noEntityFound.getText(), "  No entity found.");
	}

	public void enterEntityNameWhichHaveNoRecords(String entityRecordValue) throws Exception {
		enterValue.sendKeys(entityRecordValue);
		enterValue.sendKeys(Keys.TAB);
	}

	public void clickAddToFavorite() {
		addToFavorite.click();

	}

	public void enterFavoriteName(String favoriteName) throws Exception {
		enterFavoriteName.sendKeys(favoriteName);
	}

	public void clickOnSave() throws Exception {
		saveButton.click();
		Thread.sleep(1000);
	}

	public void verifyNoRecords() throws InterruptedException {
		
		Assert.assertEquals(noRecordFoundAtEntityValue.getText(), " No Records Found.");
	}

	public void verifyFavoriteNameReuired() throws InterruptedException {
		Assert.assertEquals(favoriteNameMessageValidation.getText(), "Favorite Name is required.");
	}
	
	public void verifyFavoriteNameMinimumSize() throws InterruptedException {
		Assert.assertEquals(favoriteNameMessageValidation.getText(), "Favorite Name must be at least 2 characters.");
	}
	
	public void verifyFavoriteErrorColor() throws InterruptedException {
		Assert.assertEquals(new Helper().FontColor(favoriteNameMessageValidation), "#a94442");
	}
	
	public void verifyFavoriteNameMaximumSize() throws InterruptedException {
		Assert.assertEquals(favoriteNameMessageValidation.getText(), "Favorite Name cannot be more than 50 characters.");
	}
	
	public void verifyNoValidationMessage() throws InterruptedException {
		boolean messageValidaton = driver.findElements(By.xpath("//*[@class='err-message ng-scope']")).size()!=0;
		String actual;
		if(messageValidaton == false) {
			 actual = "passed";
		}
		else {
			actual = "Failed";
		}
		Assert.assertEquals(actual, "passed");
	}
	
	public void clickOnGo() throws Exception {
		Thread.sleep(1000);
		goButton.click();
		Thread.sleep(2000);
	}
	
	public void verifyNoRecordsInMyFavorite() throws InterruptedException {
		Assert.assertEquals(noRecordFoundAtFavorite.getText(), " No Records Found.");
	}
	
	public void verifyLabelAddToFavoriteOnPopup() throws InterruptedException {
		Assert.assertEquals(addToFavoriteLabelPopup.getText(), "Add To Favorite");
	}
	
	public void clickOnCrossButtonOnAddToFavoritePopup() throws Exception {
		favoritePopupCrossButton.click();
	}
	
	public void verifyPopupToEntityRecordScreen() throws InterruptedException {
		boolean IspopupDisplay = driver.findElements(By.xpath("//*[@class='err-message ng-scope']")).size()!=0;
		String actual;
		if(IspopupDisplay == false) {
			 actual = "passed";
		}
		else {
			actual = "Failed";
		}
		Assert.assertEquals(actual, "passed");
	}
	
	public void clickOnCancleButtonOnAddToFavoritePopup() throws Exception {
		favoritePopupCancelButton.click();
	}
	
	public void verifyAddToFavoriteDefalutMyFavRadioSelect() throws Exception {
		String actual;
		Thread.sleep(1000);
		boolean radioBtnIsSelected = addToFavoriteMyFavRadioBtn.isSelected();
		if(radioBtnIsSelected == true) {
			 actual = "passed";
		}
		else {
			actual = "Failed";
		}
		Assert.assertEquals(actual, "passed");	
	}
	
	public void verifyAddFavoriteSaveButtonColor() throws InterruptedException {
		Thread.sleep(1000);
		Assert.assertEquals(new Helper().BtnColor(saveButton), "#5cb85c");
	}
	
	public void verifyAddFavoriteCancelButtonColor() throws InterruptedException {
		Assert.assertEquals(new Helper().BtnColor(favoritePopupCancelButton), "#5a5a5a");
	}
	
	public void verifyGoButtonColor() throws InterruptedException {
		Assert.assertEquals(new Helper().BtnColor(goButton), "#5cb85c");
	}
	
	public void verifyAddToFavoriteSaveButton() throws IOException {
		String Input = ExcelUtils.readXLSFile("Entity Records", 2, 2);
		Assert.assertEquals(addToFavoriteFirstData.getText(), Input);
	}
	
	public void verifyFavoriteNameMustBeUnique() throws InterruptedException {
		Assert.assertEquals(favoriteNameMessageValidation.getText(), "Favorite Name must be unique.");
	}

	public void clickOnRemoveButtonOfFirstRecordInMyFavorite() throws Exception {
		clickOnRemoveButtonOfFirstRecordInMyFavorite.click();
	}
	
	public void verifyRemovePopupMyFavoriteLabel() throws InterruptedException {
		Assert.assertEquals(removePopupMyFavoriteLabel.getText(), "Are you sure you want to remove demo?");
	}
	
	public void verifyConfirmationOfDeleteMyFavoriteYesButtonColor() throws InterruptedException {
		Assert.assertEquals(new Helper().BtnColor(removePopupMyFavoriteYesButton), "#5cb85c");
	}
	
	public void verifyConfirmationOfDeleteMyFavoriteNoButtonColor() throws InterruptedException {
		Assert.assertEquals(new Helper().BtnColor(removePopupMyFavoriteNoButton), "#d9534f");
	}
	
	public void verifyConfirmationOfDeleteMyFavoriteNoButtonClick() throws InterruptedException {
		removePopupMyFavoriteNoButton.click();
	}
	
	public void verifyConfirmationOfDeleteMyFavoriteNoButtonFunctionality() throws Exception {
		boolean isPopupDisplay = driver.findElements(By.xpath("//*[@class='sweet-alert showSweetAlert visible']")).size() !=0;
		Assert.assertEquals(isPopupDisplay , false );	
	}
	
	public void verifyConfirmationOfDeleteMyFavoriteYesButtonClick() throws InterruptedException {
		Thread.sleep(2000);
		removePopupMyFavoriteYesButton.click();
	}
	
	public void verifyClearAllButtonColor() throws InterruptedException {
		Assert.assertEquals(new Helper().BtnColor(clearAllButton), "#5a5a5a");
	}
	
	public void verifyAddToFavoriteColor() throws InterruptedException {
		Assert.assertEquals(new Helper().BtnColor(addToFavorite), "#337ab7");
	}
	
	public void clkClrAllBut() throws InterruptedException {
		clearAllButton.click();
	}
	
	public void clrAllBtnFun() throws InterruptedException {

		int selectedModule = 0;

		List<WebElement> count = driver.findElements(By.xpath("//*[@ng-repeat='module in vm.filteredModules']"));
		String checkbox,actual;

		for (int i = 0; i < count.size(); i++) {
			entityModuleChecked = ObjectRepo.driver.findElement(By.xpath("//*[@id='module" + i + "']"));

			if (entityModuleChecked.isSelected()) {

			}
			else {
				selectedModule++;
			}
		}
		if(selectedModule==count.size()) {
			checkbox = "passed";
		}
		else {
			checkbox = "failed";
		}
		
		boolean grid1abel = driver.findElements(By.xpath("//*[@class='entity-values-lable ng-binding']")).size()!=0;
		//entitySearchBox.click();
		String entityName = enterValue.getText();
		if(checkbox.equals("passed") && grid1abel==false && entityName.equals("")) {
			actual = "cleared";
		}
		else {
			actual = "Unclered";
		}
		Assert.assertEquals(actual, "cleared");
	}
	
	public void enterEntityName() throws Exception {
		//Thread.sleep(2000);
		enterValue.sendKeys("No Records");
		enterValue.sendKeys(Keys.TAB);
	}
	
	public void verifyGrdAddBtnColor() throws InterruptedException {
		Assert.assertEquals(new Helper().BtnColor(adBtnGrid), "#337ab7");
	}
	
	public void verifyGrdDelBtnColor() throws InterruptedException {
		Assert.assertEquals(new Helper().BtnColor(delBtnGrid), "#d9534f");
	}
	
	public void clkFilByDrdwn() throws InterruptedException {
		filterByEnAtt.click();
	}
	
	public void selectkFilByDrdwn() throws Exception {
		
		helper.SelectDrpDwnValueByText(filterByEnAtt, "Equipment type");
	}
	
	public void enterEntityNaAttText() throws Exception {
		enterValue.sendKeys("Equipment Master");
		enterValue.sendKeys(Keys.TAB);
	}
	
	public void selectkFilByDrdwnData() throws InterruptedException {
		filterByEnAtt.click();
	}
	
	public void enterEntityNaAttData() throws Exception {
		//Thread.sleep(2000);
		filterByEnAttField.sendKeys("Equipment Name");
		filterByEnAttField.sendKeys(Keys.TAB);
	}
	
	public void verifyEntityAttSrchFunclity() throws Exception {
		
		int i = 0, count = 0, add = 0 ;
		String nextBtnColor, selectAttributeValue = "Aviation";
		 
		List<org.openqa.selenium.WebElement> inList = new ArrayList<WebElement>();
		List<String> temp = new ArrayList<String>();
		
		
		if (driver.findElements(By.xpath("//*[@ng-click='selectPage(page + 1, $event)']"))
				.size() != 0 == true) {
			System.out.println("found more than 10 records");

			for (i = 0; i < 100; i++) {

				// this line is storing a hex code of next button.
				nextBtnColor = new Helper().FontColor(gridPaggingNextBtn);

				// this line is taking a "Created Date column" all value.
				inList = (ArrayList<org.openqa.selenium.WebElement>) driver
						.findElements(By.xpath("//*[@data-th='" + selectAttributeValue + "']"));

				try {

					for (add = 0; add < inList.size(); add++) {
						temp.add(count, inList.get(add).getText());
						count++;
					}

					System.out.println(i + "-grid list: " + temp.size());
					System.out.println("--------------------------------------");
					System.out.println(Arrays.deepToString(temp.toArray()));
					System.out.println("--------------------------------------");

				} catch (Exception e) {
					System.out.println(e);
				}
				gridPaggingNextBtn.click();
				Thread.sleep(3000);

				if (nextBtnColor.equalsIgnoreCase("#777777")) {
					break;
				}
			}
		} else {
			inList = (ArrayList<org.openqa.selenium.WebElement>) driver
					.findElements(By.xpath("//*[@data-th='" + selectAttributeValue + " :']"));

			for (add = 0; add < inList.size(); add++) {
				temp.add(count, inList.get(add).getText());
				count++;

				System.out.println(i + "-grid list: " + temp.size());
				System.out.println("--------------------------------------");
				System.out.println(Arrays.deepToString(temp.toArray()));
				System.out.println("--------------------------------------");
			}
		}

		String[] outArray = new String[temp.size()]; // instantiate Array

		int actualMatchedRecords = 0;
		int expectedMatchedRecords = temp.size();

		for (int j = 0; j < temp.size(); j++) {
			outArray[j] = temp.get(j);

			if (outArray[0].equalsIgnoreCase(selectAttributeValue)) {
				actualMatchedRecords++;
				System.out.println("--------------------------------");
				System.out.println("Record Matched                |");
				System.out.println("actual: " + outArray[0] + "\nexpected: " + selectAttributeValue);

			} else {
				System.out.println("--------------------------------");
				System.out.println("Record Does't Matched          |");
				System.out.println("actual: " + outArray[0] + "\nexpected: " + selectAttributeValue);

			}
			System.out.println("--------------------------------");

		}
		System.out.println("Actual no of record is: " + actualMatchedRecords);
		System.out.println("Total no of record is: " + expectedMatchedRecords);

		
	}
	
	public void verifysearch(String searchedItem) throws Exception {

		AttributesPageObject obj = new AttributesPageObject(ObjectRepo.driver);

		String PaginationVal = null, PaginationSelectedVal = null, srchResultNo = null;
		try {
			PaginationVal = obj.paginationText.getText();
			srchResultNo = obj.srcResultPageNo.getText();
			PaginationSelectedVal = obj.pageSizeDrp_GetSelectedValue();
			new Search().SearchVerification(PaginationVal, PaginationSelectedVal, obj.grid_data, searchedItem,obj.textSearch, srchResultNo);
		} catch (Exception e) {
			System.err.println(e);
		}

	}
}
