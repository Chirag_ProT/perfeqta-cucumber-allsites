package com.cucumber.framework.helper.PageObject;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.server.handler.SendKeys;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.Assert;

import com.cucumber.framework.helper.Button.ButtonHelper;
import com.cucumber.framework.helper.Logger.LoggerHelper;
import com.cucumber.framework.helper.Scroll.Scroll;




public class GeneralSettingsPageObject extends PageBase {
	private WebDriver driver;
	public ButtonHelper btnHelper;
	private final Logger log = LoggerHelper.getLogger(GeneralSettingsPageObject.class);
	private Scroll scroll;

	public GeneralSettingsPageObject(WebDriver driver) {
		super(driver);
		this.driver = driver;
		btnHelper = new ButtonHelper(driver);
		scroll = new Scroll();
	}

	/** WebElements */

	@FindBy(how = How.XPATH, using = "//*[@ui-sref='secure.admin.entities.list']")
	public WebElement entitiesTile;

	@FindBy(how = How.XPATH, using = "//a[@ui-sref='secure.admin.generalSettings.edit']")
	public WebElement generalSettingModuleName;

	@FindBy(how = How.XPATH, using = "//span[@class='currentStep ng-binding']")
	public WebElement generalSettingName;
	
	@FindBy(how = How.XPATH, using = "//*[@id=\"breadcrum-ipad-768\"]/li[2]/a")
	public WebElement ClickBread;
	
	
	@FindBy(how = How.XPATH, using = "//*[@class='crumb breadcrumb gray ng-isolate-scope']")
	public WebElement breadcrumb;
	
	@FindBy(how = How.XPATH, using = "//input[@ng-model=\"vm.generalSettings.siteTitle\"]")
	public WebElement clickRecordTitle;
	
	@FindBy(how = How.XPATH, using = "/html/body/div/div/ui-view/ui-view/fieldset/div/form/div/div[2]/div[1]/p")
	public WebElement validationRecordTitle;
	
	@FindBy(how = How.XPATH, using = "//fieldset/div/form/div/div[2]/div[1]/input")
	public WebElement minLengthvalidationMsg;
	
	@FindBy(how = How.XPATH, using = "//fieldset/div/form/div/div[2]/div[1]/p")
	public WebElement verifyMinLengthvalidationMsg;
	
	@FindBy(how = How.XPATH, using = "//ui-view/fieldset/div/form/div/div[2]/div[1]/p")
	public WebElement verifyMaxLengthvalidationMsg;
	
	@FindBy(how = How.XPATH, using = "//form/div/div[2]/div[2]/input")
	public WebElement stdReportsubTitlebox;
	
	@FindBy(how = How.XPATH, using = "//div/form/div/div[2]/div[2]/p")
	public WebElement stdReportsubTitleRequiredValidation;
	
	
	@FindBy(how = How.XPATH, using = "//div/form/div/div[2]/div[2]/p")
	public WebElement stdReportsubTitleMinValidation;
	
	@FindBy(how = How.XPATH, using = "//div/form/div/div[2]/div[2]/p")
	public WebElement stdReportsubTitleMaxValidation;
	
	@FindBy(how = How.XPATH, using = "//div/form/div/div[2]/div[4]/textarea")
	public WebElement clickContactdetails;
	
	@FindBy(how = How.XPATH, using = "//fieldset/div/form/div/div[2]/div[4]/p")
	public WebElement textareaMinValidation;
	
	@FindBy(how = How.XPATH, using = " //div/form/div/div[2]/div[4]/p")
	public WebElement textareaMaxValidation;
	
	@FindBy(how = How.XPATH, using = " //div/form/div/div[3]/div[1]/input")
	public WebElement passwordAgingBox;
	
	@FindBy(how = How.XPATH, using = "//div/form/div/div[3]/div[1]/p")
	public WebElement verify0passwordAgingBox;
	
	@FindBy(how = How.XPATH, using = "//div/form/div/div[3]/div[1]/p")
	public WebElement verify999passwordAgingBox;
	
	@FindBy(how = How.XPATH, using = "//div/form/div/div[3]/div[2]/input")
	public WebElement PasswordAgingMsgbox;
	
	@FindBy(how = How.XPATH, using = "//div/form/div/div[3]/div[2]/p")
	public WebElement PasswordAgingMsgMinValidation;
	
	@FindBy(how = How.XPATH, using = "//div/form/div/div[3]/div[2]/p")
	public WebElement PasswordAgingMsgMaxValidation;
	
	@FindBy(how = How.XPATH, using = "//div/form/div/div[4]/div[1]/input")
	public WebElement PreviousUsedPasswordsbox;
	
	@FindBy(how = How.XPATH, using = "//fieldset/div/form/div/div[4]/div[1]/p")
	public WebElement enterZeroValidationPreviousUsedPasswordsBox;
	
	@FindBy(how = How.XPATH, using = "//div/form/div/div[4]/div[1]/p")
	public WebElement lessthan4ValidationPreviousUsedPasswordsBox;
	
	@FindBy(how = How.XPATH, using = "//div/form/div/div[4]/div[1]/p")
	public WebElement greaterthan99ValidationPreviousUsedPasswordsBox;
	
	@FindBy(how = How.XPATH, using = "//div/form/div/div[4]/div[2]/input")
	public WebElement PreviousUsedPasswordsMsgBox;
	
	@FindBy(how = How.XPATH, using = "//form/div/div[4]/div[2]/p")
	public WebElement MinValidationPreviousUsedPasswordsMsgBox;
	
	@FindBy(how = How.XPATH, using = "//fieldset/div/form/div/div[4]/div[2]/p")
	public WebElement MaxValidationPreviousUsedPasswordsMsgBox;
	
	@FindBy(how = How.XPATH, using = "//div/form/div/div[5]/div[1]/input")
	public WebElement FailedLoginAttemptsBox;
	
	
	@FindBy(how = How.XPATH, using = "//div/form/div/div[5]/div[1]/p")
	public WebElement MinValidationFailedLoginAttemptsBox;
	
	@FindBy(how = How.XPATH, using = "//div/form/div/div[5]/div[2]/input")
	public WebElement FailedLoginAttemptsAlertBox;
	
	@FindBy(how = How.XPATH, using = "//div/form/div/div[5]/div[2]/p")
	public WebElement ValidationFailedLoginAttemptsAlertBox;
	
	@FindBy(how = How.XPATH, using = "//div/form/div/div[6]/div[1]/div[2]/input")
	public WebElement monthlyWindowsBox;
	
	@FindBy(how = How.XPATH, using = "//fieldset/div/form/div/div[6]/div[2]/p")
	public WebElement ValidatioMonthlyWindow;
	
	@FindBy(how = How.XPATH, using = "//*[@id=\"hideProcTitle\"]")
	public WebElement ShowSequenceCheckbox;
	
	@FindBy(how = How.XPATH, using = "//*[@id=\"showHideCriteriaDetails\"]")
	public WebElement AcceptanceCriteriaCheckbox;
	

	@FindBy(how = How.XPATH, using = "//*[@id=\"displayNullValues\"]")
	public WebElement DoNotCheckbox;
	
	@FindBy(how = How.XPATH, using = "//*[@id=\"allowPrintSites\"]")
	public WebElement AllowAllSiteCheckbox;
	
	@FindBy(how = How.XPATH, using = "//div[12]/div/button")
	public WebElement SaveButton;
	
	
	
	 

	// --------------------public methods--------------------

	public WebDriver getDriver() {
		return this.driver;

	}

	public void clickGeneralSetting() throws Exception{
		Thread.sleep(1000);
		generalSettingModuleName.click();
	}
	public void verifyGeneralSetting() {
		Assert.assertEquals(generalSettingName.getText(), "Edit General Settings");
	}
public void clickBreadcrumb() {
	ClickBread.click();
		
	}
	public void verifyRedirectPreviousPage() {
		
		Assert.assertEquals(breadcrumb.getText(), "HomeAdministration");
	}
	public void clickStandardReport() {
		
		clickRecordTitle.click();
	}
	public void removePreviousData() {
		clickRecordTitle.clear();
		
	}
	public void verifyMandatoryValidationMessage() {
		
		Assert.assertEquals(validationRecordTitle.getText(), "Standard Report Title is required.");
		
	}
	
	public void lessThanCharactersMsg(String val) {
		
		minLengthvalidationMsg.sendKeys(val);
	}
	public void verifyMinimumValidationMsg() {
		
		Assert.assertEquals(verifyMinLengthvalidationMsg.getText(), "Standard Report Title should be at least 2 characters long.");
	}
	public void moreThanCharacters(String value) {
		
		clickRecordTitle.sendKeys(value);
	}
public void verifyMaximumValidationMsg() {
		
	Assert.assertEquals(verifyMaxLengthvalidationMsg.getText(), "Standard Report Title cannot be more than 100 characters.");

	}
public void enterValidData(String value) {
	clickRecordTitle.sendKeys(value);
}
public void verifyValidDataAllowed() {
	Assert.assertEquals(true, true);
}

public void clickStandardReportSubtitle() {
	stdReportsubTitlebox.click();
	
}

public void removePreviousSubtitletext() {
	stdReportsubTitlebox.clear();
}
public void verifySubTitleRequiredMsg() {
	
	Assert.assertEquals(stdReportsubTitleRequiredValidation.getText(), "Standard Report Sub Title is required.");

	
}
public void enterlessCharactersforSubtitlebox(String value) {
	
	stdReportsubTitlebox.sendKeys(value);
}

public void verifyMinlengthValidationMsg() {
	
	Assert.assertEquals(stdReportsubTitleMinValidation.getText(), "Standard Report Sub Title should be at least 2 characters long.");

	
}

public void enterMorethanCharactersSubtitle(String value) {
	stdReportsubTitlebox.sendKeys(value);
	
}

public void verifyMaxlengthValidationMsgSubtitle() {
	
	Assert.assertEquals(stdReportsubTitleMaxValidation.getText(), "Standard Report Sub Title must not be more than 100 characters.");

}
public void validDataallowedSubtitle(String Value) {
	
	stdReportsubTitlebox.sendKeys(Value);
	
}
public void verifyvalidDataSubtitle() {
	
	Assert.assertEquals(true, true);

}
public void clickContactdetail() {
	clickContactdetails.click();
	
}
public void removePreviousFromTextarea() {
	clickContactdetails.clear();
	
}
public void enterLessforTextarea(String value) {
	
	clickContactdetails.sendKeys(value);
}
public void verifyMinValidation() {
	
	Assert.assertEquals(textareaMinValidation.getText(), "Support Contact Details should be at least 2 characters long.");

}
public void enterMorethanCharactersinTextarea(String value) {
	clickContactdetails.sendKeys(value);
	
}

public void verifyMaxValidationTextArea() {
	
	Assert.assertEquals(textareaMaxValidation.getText(), "Support Contact Details must not be more than 300 characters.");	
	
}
public void enterValidforTextArea(String value) {
	clickContactdetails.sendKeys(value);
	
}
public void verifyValidDataForTextarea() {
	Assert.assertEquals(true,true);		
}
 public void clickPasswordAgingLimit() {
	 
	 passwordAgingBox.click();
	 
}
 
 public void removePreviousDatapasswordAging() {
	 passwordAgingBox.clear();
}
 
 public void enterZeroinTextbox(String value) {
	 
	
	 passwordAgingBox.sendKeys(value);
	 
}
 
 public void verifyMinValidationforPasswordAging() {
	 
	 Assert.assertEquals(verify0passwordAgingBox.getText(),"Password Aging Limit must be at least 1.");	
}
 
 
 public void entermorethan999Numeric(String value) {
	 passwordAgingBox.sendKeys(value);
}
 
 public void verifyMaxValidationPasswordAging() {
	 scroll.scrollDown(driver);
	 Assert.assertEquals(verify999passwordAgingBox.getText(),"Password Aging Limit shomustuld not be more than 999.");	
}
 
 public void enterValidinPasswordAging(String value) {
	 
	 passwordAgingBox.sendKeys(value);

}
 public void verifypasswordAgingBoxallowValidData() {
	 
	 Assert.assertEquals(true, true);
	
}
 
 public void enterlessPasswordAging(String value) {
	
}
 
 public void clickPasswordAgingLimitAlertMsg() {
	 
	 PasswordAgingMsgbox.click();
}
 public void removepreviousPasswordAgingMsg() {
	 
	 PasswordAgingMsgbox.clear();
	
}
 
 public void enterlessCharactersPasswordAgingMsg(String value) {
	 
	 PasswordAgingMsgbox.sendKeys(value);
}
 
 public void verifyMinValidationPasswordAgingMsg() {
	 Assert.assertEquals(PasswordAgingMsgMinValidation.getText(), "Alert Message must be at least 2 characters.");
}
 
 public void enterMoreCharPasswordAgingMsg(String value) {
	 PasswordAgingMsgbox.sendKeys(value);
}
 
 public void verifyMaxValidationPasswordAgingMsgalert() {
	 
	 Assert.assertEquals(PasswordAgingMsgMaxValidation.getText(), "Alert Message cannot be more than 200 characters.");
}
 public void enterValidDataPasswordAgingMsgBox(String value) {
	 
	 PasswordAgingMsgbox.sendKeys(value);
}
 public void verifyValidDataAlertMsg() {
	 
	 Assert.assertEquals(true, true);

}
 
 public void clickOnNumberofPreviousUsedPassword() {
	 PreviousUsedPasswordsbox.click();
}
 public void removePreviousUseddatainTextbox() {
	 PreviousUsedPasswordsbox.clear();
}
 public void enterZeroinPreviousPasswordTextbox(String value) {
	 PreviousUsedPasswordsbox.sendKeys(value);
}
 public void verifyMinValidationMsgPreviousAllowedBox() {
	 
	 Assert.assertEquals(enterZeroValidationPreviousUsedPasswordsBox.getText(), "Prevent Number of Last reused Passwords should be at least 1.");

}
 
 public void enterLess4inPreviousUsedTextBox(String value) {
	 PreviousUsedPasswordsbox.sendKeys(value);
	
}
 public void verifyValidationMsglessThan4PreviousPasswordUsed() {
	 Assert.assertEquals(lessthan4ValidationPreviousUsedPasswordsBox.getText(), "Regulation CFR 21 Part 11 requires that this number be set to a minimum of 4. Are you sure you want to proceed with a value less than 4?");

}
 
 public void enterMorethan99inNumberofPreviousUsedPasswords(String value) {
	 PreviousUsedPasswordsbox.sendKeys(value);
	
}
 
 public void verifyMorethan99inNumberofPreviousUsed() {
	 Assert.assertEquals(greaterthan99ValidationPreviousUsedPasswordsBox.getText(), "Prevent Number of Last reused Passwords cannot be more than 99.");

}
 
 public void entervaliddatainPreviousUsed(String value) {
	 PreviousUsedPasswordsbox.sendKeys(value);
	 
	
}
 public void verifyValidDatainPreviousUsedTextBox() {
	Assert.assertEquals(true, true);
}

public void clickOnNumberofPreviousUsedPasswordMsgBox() {
	PreviousUsedPasswordsMsgBox.click();
}

public void removePreviousDataofPreviousUsedPasswordAlertMsgBox() {
	
	PreviousUsedPasswordsMsgBox.clear();
}

public void enterLessthan2CharinAmlertMsgBox(String value) {
	PreviousUsedPasswordsMsgBox.sendKeys(value);
}
public void verifyMinimumValidationMsgforAlertBox() {
	Assert.assertEquals(MinValidationPreviousUsedPasswordsMsgBox.getText(), "Alert Message must be at least 2 characters.");
}

public void enterMorethan200ValidationMsg(String value) {
	
	PreviousUsedPasswordsMsgBox.sendKeys(value);
	
}

public void verifyMaxValidationMsgforAlertMsgBox() {
	Assert.assertEquals(MaxValidationPreviousUsedPasswordsMsgBox.getText(), "Alert Message cannot be more than 200 characters.");

}
public void enterValidDatainPreviouslyUsedAlertMsgBox(String value) {
	
	PreviousUsedPasswordsMsgBox.sendKeys(value);
}

public void verifyValidDatainPreviousUsedAlertMsgBox() {
	Assert.assertEquals(true, true);
}

public void clickFailedLoginAttemptsTextbox() {
	FailedLoginAttemptsBox.click();
}

public void removePreviousDataofFailedLoginAttemps() {
	FailedLoginAttemptsBox.clear();
}
public void enterZeroinFailedLoginAttemptsTextbox(String value) {
	FailedLoginAttemptsBox.sendKeys(value);
}
public void verifyMinforFailedLoginAttemptsTextbox() {
	Assert.assertEquals(MinValidationFailedLoginAttemptsBox.getText(), "Number of Failed Login Attempts should be at least 1.");
	
}
public void enterMorethan999FailedLoginAttemptsTextbox(String value) {
	FailedLoginAttemptsBox.sendKeys(value);
	
}
public void verifyMaxforFailedLoginAttemptsTextbox() {
	Assert.assertEquals(MinValidationFailedLoginAttemptsBox.getText(), "Number of Failed Login Attempts must not be more than 999.");
	
}
public void enterValidDataforFailedLoginAttempts(String value) {
	FailedLoginAttemptsBox.sendKeys(value);
}
public void verifyValidDataforFailedLoginAttemptsTextbox() {
	Assert.assertEquals(true, true);
	
}

public void clickFailedLoginAttemptsMsgTextbox() {
	FailedLoginAttemptsAlertBox.click();
}
public void removeDatafromAlertMsgBox() {
	FailedLoginAttemptsAlertBox.clear();
}

public void enterLessthan2CharinFailedAlertMsgBox(String value) {
	FailedLoginAttemptsAlertBox.sendKeys(value);
	
}

public void verifyMinCharValidationMsgFailedLoginAlertBox() {
	Assert.assertEquals(ValidationFailedLoginAttemptsAlertBox.getText(), "Alert Message must be at least 2 characters.");
}

public void enterMorethan200CharinMsgAlertBox(String value) {
	FailedLoginAttemptsAlertBox.sendKeys(value);
	
}
public void verifyMaxCharFailedAlertMsg() {
	Assert.assertEquals(ValidationFailedLoginAttemptsAlertBox.getText(), "Alert Message cannot be more than 200 characters.");
}
public void enterValidDatainFailedAlertMsgbox(String value) {
	FailedLoginAttemptsAlertBox.sendKeys(value);
}

public void verifyValidCharFailedAlertMsg() {
	Assert.assertEquals(true, true);
}
public void clickmonthlywindowsbox() {
	monthlyWindowsBox.click();
	
}
public void removedataFrommontlyWindows() {
	monthlyWindowsBox.clear();
}
public void enterZeroInMonthlyWindows(String value) {
	monthlyWindowsBox.sendKeys(value);
}
public void verifyZeroinMonthlyWindows() {
	Assert.assertEquals(ValidatioMonthlyWindow.getText(), "Day should be at least 1.");
}
public void enterMaxinMonthlyWindows(String value) {
	monthlyWindowsBox.sendKeys(value);
	
}
public void verifyMaxMontlyWindows() {
	Assert.assertEquals(ValidatioMonthlyWindow.getText(), "Day cannot be more than 31.");
	
}
public void enterValidDatainMonthlyWindow(String value) {
	
	monthlyWindowsBox.sendKeys(value);
}
public void verifyValidDataMonthlyWindows() {
	Assert.assertEquals(true, true);
}

public void clickCheckboxofShowSequence() {
	ShowSequenceCheckbox.click();
	
}

public void verifyChecboxofShowSequence() {
	
	if (ShowSequenceCheckbox.isSelected()) {
		Assert.assertEquals(true, true);
	} else {
		Assert.assertEquals(false, true);
	        }
	
}
public void clickShowAcceptanceCheckbox() {
	AcceptanceCriteriaCheckbox.click();
}
public void verifyChecboxofAcceptanceCriteria() {
	
	if (AcceptanceCriteriaCheckbox.isSelected()) {
		Assert.assertEquals(false, true);
	} else {
		Assert.assertEquals(true, true);
	        }
	
}

public void clickonDoNoTallowCheckbox() {
	
	DoNotCheckbox.click();
}
public void verifyCheckboxofDoNotAllow() {
	if (DoNotCheckbox.isSelected()) {
		Assert.assertEquals(true, true);
	} else {
		Assert.assertEquals(false, true);
	        }
	
}

public void clickAllowAllSiteCheckbox() {
	AllowAllSiteCheckbox.click();
	
}

public void verifyCheckboxofAllowAllSite() {
	if (AllowAllSiteCheckbox.isSelected()) {
		Assert.assertEquals(false, true);
	} else {
		Assert.assertEquals(true, true);
	        }
	
}
public void ClickonSaveButton() {
	SaveButton.click();
}

public void verifySavebutton() throws Exception {
	//SaveButton
	Thread.sleep(3000);
	scroll.scrollUp(driver);
	Assert.assertEquals(breadcrumb.getText(), "HomeAdministration");
}



}// end
