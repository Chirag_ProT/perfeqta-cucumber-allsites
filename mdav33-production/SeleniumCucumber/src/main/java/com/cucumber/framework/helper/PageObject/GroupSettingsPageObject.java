package com.cucumber.framework.helper.PageObject;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.By.ByXPath;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.server.browserlaunchers.Sleeper;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import com.cucumber.framework.helper.Helper;
import com.cucumber.framework.helper.Button.ButtonHelper;
import com.cucumber.framework.helper.Logger.LoggerHelper;
import com.cucumber.framework.helper.Pagination.Pagination;
import com.cucumber.framework.helper.Search.Search;
import com.fasterxml.jackson.annotation.JsonTypeInfo.As;

public class GroupSettingsPageObject extends PageBase {
	private WebDriver driver;
	private Helper helper = new Helper();
	public ButtonHelper btnHelper;
	private final Logger log = LoggerHelper.getLogger(SitesPageObject.class);
	private Search sObj;

	public GroupSettingsPageObject(WebDriver driver) {
		super(driver);
		this.driver = driver;
		btnHelper = new ButtonHelper(driver);
		sObj = new Search();
	}

	@FindBy(how = How.XPATH, using = "//*[@ui-sref='secure.admin.home']")
	public WebElement administrationTile;
	@FindBy(how = How.XPATH, using = "//div[18]/div/a")
	public WebElement GrpSetingsTile;
	@FindBy(how = How.XPATH, using = "//*[@id='breadcrum-ipad-768']/li[3]/span")
	public WebElement verifyGrpSetingsTile;
	@FindBy(how = How.XPATH, using = "//tbody/tr[1]/td[6]")
	public WebElement FirstAudit;
	
	@FindBy(how = How.XPATH, using = "//th[1]")
	public WebElement Groupname;
	@FindBy(how = How.XPATH, using = "//*[@id='breadcrum-ipad-768']/li[4]/span")
	public WebElement AddEditpage;
	@FindBy(how = How.XPATH, using = "//a[@href='#/administration/groupsettings/new/edit']")
	public WebElement newBtn;
	@FindBy(how = How.XPATH, using = "//input[@ng-change='vm.checkgroupsettingvalue(vm.groupsetting.title)']")
	public WebElement Groupnametxt;
	@FindBy(how = How.XPATH, using = "//p[@translate='secure.admin.groupsettings.edit.GROUPNAMEISREQUIRED']")
	public WebElement ValidationGroupnametxt;
	
	@FindBy(how = How.XPATH, using = "//p[@translate='secure.admin.groupsettings.edit.GROUPNAMESHOULDBEATLEAST2CHARACTERSLONG']")
	public WebElement minValidationGroupnametxt;
	
	
	@FindBy(how = How.XPATH, using = "//p[@translate='secure.admin.groupsettings.edit.GROUPNAMESHOULDNOTBEMORETHAN50CHARACTERSLONG']")
	public WebElement MaxValidationGroupnametxt;
	
	@FindBy(how = How.XPATH, using = "//p[@translate='secure.admin.groupsettings.edit.GROUPNAMEMUSTBEUNIQUE']")
	public WebElement UniqueValidationGroupnametxt;
	 
	@FindBy(how = How.XPATH, using = "//li[7]/a/div/label")
	public WebElement selectmodule;
	@FindBy(how = How.XPATH, using = "//option[1]")
	public WebElement selectapp;
	@FindBy(how = How.XPATH, using = "//div[3]/div/span")
	public WebElement movebutton;
	@FindBy(how = How.XPATH, using = "//div/li/span")
	public WebElement selectedapp;
	
	@FindBy(how = How.XPATH, using = "//span[@ng-click='vm.moveDown(vm.groupsettingindex)']")
	public WebElement moveDown;
	@FindBy(how = How.XPATH, using = "//li[3]/span")
	public WebElement moveDownafter;
	@FindBy(how = How.XPATH, using = "//li[1]/span")
	public WebElement moveUpafter;
	@FindBy(how = How.XPATH, using = "//li[2]/span")
	public WebElement heighlightselectedapp;
	@FindBy(how = How.XPATH, using = "//span[@ng-click='vm.moveUp(vm.groupsettingindex)']")
	public WebElement moveup;
	@FindBy(how = How.XPATH, using = "//button[@ng-click='vm.save(vm.groupsetting,editGroupsettingsForm)']")
	public WebElement saveBtnColor;
	
	@FindBy(how = How.XPATH, using = "//a[@ui-sref='secure.admin.groupsettings.list']")
	public WebElement calcelBtn;
	@FindBy(how = How.XPATH, using = "//input[@name = 'appFilter']")
	public WebElement SearchApp;
	@FindBy(how = How.XPATH, using = "//div[2]/div/li/span")
	public WebElement selectedAppforGrp;
	
	
	
	
	
	/* -------------------------- |----------------| -------------------------- */
	/* -------------------------- | Public Methods | -------------------------- */
	/* -------------------------- |----------------| -------------------------- */

	public void clicktoAdministrationTile() throws Exception {
		log.info(administrationTile);
		administrationTile.click();
	}
	public void clickonGrpSettingTile() {
		GrpSetingsTile.click();
	}
	 public void verifyGrpSetting() {
		Assert.assertEquals(verifyGrpSetingsTile.getText(), "Group Settings");

	 }
	 public void clickonFirstRecord() {
		 FirstAudit.click();
	}
	 public void clickonGroupName() {
		 Groupname.click();
	}
	 public void clickonNewBtn() {
		newBtn.click();
	}
	 public void verifynewBtnRedirection() {
		Assert.assertEquals(AddEditpage.getText(), "Add / Edit Group Settings");
	}
	 public void clickonGrpnamebox() {
		 Groupnametxt.click();
	}
	 public void pressTab() {
		 Groupnametxt.sendKeys(Keys.TAB);
	}
	 public void verifyValidationMsg() {
		Assert.assertEquals(ValidationGroupnametxt.getText(), "Group Name is required.");
	}
	 public void entervalueGrpnameTxt(String value) {
		 Groupnametxt.sendKeys(value);
	}
	 public void verifyMinValidation() {
		Assert.assertEquals(minValidationGroupnametxt.getText(), "Group Name must be at least 2 characters.");
	}
	 public void enterMaxValue(String value) {
		 Groupnametxt.sendKeys(value);
	}
	 public void verifyenterMaxValue() {
			Assert.assertEquals(MaxValidationGroupnametxt.getText(), "Group Name cannot be more than 50 characters long.");

	}
	 public void enterDuplicateValue(String value) {
		 Groupnametxt.sendKeys(value);
    }
	 public void verifyenterDuplicateValue() throws Exception {
		 	Thread.sleep(1000);
			Assert.assertEquals(UniqueValidationGroupnametxt.getText(), "Group Name must be unique.");

	}
	 public void enterValidValue(String value) {
		 Groupnametxt.sendKeys(value);
	}
	 public void verifyenterValidValue() {
			Assert.assertEquals(true,true);

	}
	 public void selectModule() {
		 selectmodule.click();
	}
	 public void selectApp() throws Exception{ 
		 Thread.sleep(4000);
		 SearchApp.click();
		selectapp.click();
		String p = selectapp.getText();
		 movebutton.click();
		verifyselectedApp(p);
	}
	 public void ClickonMove() {
		
	}
	 public void verifyselectedApp(String value) {
		Assert.assertEquals(value, selectedAppforGrp.getText());
	}
	 public void verifyAscendingorderIcon() {
		 selectapp.click(); movebutton.click();selectapp.click(); movebutton.click();
		 System.out.println(
					"We can't perform this test case due to Java sorting function giving different output as compared to perfeqta's output ");
		
	}
	 
	 public void verifydsendingIcon() {
		 selectapp.click(); movebutton.click();selectapp.click(); movebutton.click();
		 System.out.println(
					"We can't perform this test case due to Java sorting function giving different output as compared to perfeqta's output ");
		
	}
	 public void selectMultipleApps() {
		 selectapp.click(); movebutton.click();selectapp.click(); movebutton.click();selectapp.click(); movebutton.click();
	}
	 public void clickMoveDown() {
		 heighlightselectedapp.click();
		 String heighlightedapp = heighlightselectedapp.getText();
		 moveDown.click();
		 verifymovedown(heighlightedapp);
	}
	 
	 public void  verifymovedown(String value) {
		Assert.assertEquals(value,moveDownafter.getText());
	}
	 public void clickMoveUP() {
		 heighlightselectedapp.click();
		 String heighlightedapp = heighlightselectedapp.getText();
		 moveup.click();
		 verifyMoveUp(heighlightedapp);
	}
	 public void verifyMoveUp(String value) {
		 Assert.assertEquals(value ,moveUpafter.getText());
	}
	 public void verifysaveBtnGreen() {
		 Assert.assertEquals(helper.BtnColor(saveBtnColor), "#5cb85c");
	}
	 public void verifycancelBtnBlack() {
		 Assert.assertEquals(helper.BtnColor(calcelBtn), "#5a5a5a");
	}
	 public void clickCancelBtn() throws Exception {
		 Thread.sleep(4000);
		 calcelBtn.click();
	}
	 public void verifyRedirectiontoGroupSetting() {
		Assert.assertEquals(verifyGrpSetingsTile.getText(), "Group Settings");
	}
}