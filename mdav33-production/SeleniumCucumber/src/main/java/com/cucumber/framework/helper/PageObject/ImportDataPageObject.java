package com.cucumber.framework.helper.PageObject;


import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;

import com.cucumber.framework.helper.Helper;
import com.cucumber.framework.helper.Button.ButtonHelper;
import com.cucumber.framework.helper.Logger.LoggerHelper;
import com.cucumber.framework.helper.Scroll.Scroll;
import com.cucumber.framework.helper.Search.Search;
import com.cucumber.framework.settings.ObjectRepo;
import com.cucumber.framework.utility.Constant;
import com.cucumber.framework.utility.ExcelUtils;
import cucumber.api.java.lu.a;
import gherkin.lexer.Th;

import com.cucumber.framework.helper.Sorting.SortingOnColumn;

public class ImportDataPageObject extends PageBase
{
	private WebDriver driver;
	public ButtonHelper btnHelper;
	private final Logger log = LoggerHelper.getLogger(ImportDataPageObject.class);
	private SortingOnColumn sortColumn;
	private Search search;
	private Scroll scroll;
	private CommonFunctionPageObject commonFnPageObject;

	public ImportDataPageObject(WebDriver driver)
	{
		super(driver);
		this.driver = driver;
		btnHelper = new ButtonHelper(driver);
		sortColumn = new SortingOnColumn();
		search = new Search();
		scroll = new Scroll();
	}

	/** WebElements */

	@FindBy(how = How.LINK_TEXT, using = "Import Data")
	public WebElement importDataRecdsTile;

	@FindBy(how = How.XPATH, using = "//*[@class='currentStep ng-binding']")
	public WebElement importDataRecdsLbl;

	@FindBy(how = How.XPATH, using = "//*[@class='crumb breadcrumb gray ng-isolate-scope']")
	public WebElement breadcrumImportData;

	@FindBy(how = How.LINK_TEXT, using = "Manual Import")
	public WebElement manualImport;

	@FindBy(how = How.XPATH, using = "//*[@class='currentStep ng-binding']")
	public WebElement manualImportLabel;

	@FindBy(how = How.XPATH, using = "//*[@class='crumb breadcrumb gray ng-isolate-scope']")
	public WebElement breadcrumManImp;

	@FindBy(how = How.XPATH, using = "//*[@class='col-md-4 col-sm-12 col-xs-12']")
	public WebElement uploadCsvFileLabel;

	@FindBy(how = How.XPATH, using = "//*[@name='qcModuleSelect']")
	public WebElement moduleDrpDwn;

	@FindBy(how = How.XPATH, using = "//*[@translate='import.modr']")
	public WebElement moduleDrpDwnValMsg;

	@FindBy(how = How.XPATH, using = "//*[@name='qcTypeSelect']")
	public WebElement typeDrpDwn;

	@FindBy(how = How.XPATH, using = "//*[@class='help-block ng-binding ng-scope']")
	public WebElement typeDrpDwnValMsg;

	@FindBy(how = How.XPATH, using = "//*[@name='mappingNameTextBox']")
	public WebElement mappingName;

	@FindBy(how = How.XPATH, using = "//*[@translate='manual.mapr']")
	public WebElement mappingNameValMsg;
	
	@FindBy(how = How.XPATH, using = "//*[@translate='manual.mapu']")
	public WebElement mappingNameValMsgForDup;
	
	@FindBy(how = How.XPATH, using = "//*[@translate='manual.map2']")
	public WebElement mappingNameValMsgForMinSize;
	
	@FindBy(how = How.XPATH, using = "//*[@translate='manual.map50']")
	public WebElement mappingNameValMsgForMaxSize;
	
	@FindBy(how = How.XPATH, using = "//*[@class='btn btn-success ng-binding']")
	public WebElement uploadCsvBtn;
	
	@FindBy(how = How.XPATH, using = "//*[@ui-sref='secure.importdata.home']")
	public WebElement cancelManUpBtn;
	
	@FindBy(how = How.LINK_TEXT, using = "View Saved Mapping")
	public WebElement viewSavedMapping;
	
	@FindBy(how = How.XPATH, using = "//*[@class='currentStep ng-binding']")
	public WebElement viewSavedMappingLabel;
	
	@FindBy(how = How.XPATH, using = "//*[contains(normalize-space(text()),'Showing')]")
	public WebElement paginationText;
	
	@FindBy(how = How.XPATH, using = "//*[@ng-if='vm.options.data && vm.options.data.length']")
	public WebElement grid;
	
	@FindBy(how = How.XPATH, using = "//*[@ng-change='vm.onPageNumberChange()']")
	public WebElement pageSizeDrp;
	
	@FindBy(how = How.XPATH, using = "//*[@ng-model='vm.textSearch']")
	public WebElement viewMapSrchBox;
	
	@FindBy(how = How.XPATH, using = "(//*[@ng-click='vm.go(field,row,vm.resource);'])[2]")
	public WebElement viewAduitTrail;

	// -----------------------------------public methods ----------------------------------------------------------
	public void importDataRecdsTileClick() throws Exception {
		Thread.sleep(1000);
		importDataRecdsTile.click();
	}

	public void verifyImportDaLabel() throws Exception {
		Assert.assertEquals(importDataRecdsLbl.getText(), "Import Data");
	}

	public void verifyImportDaBreadCrum() throws Exception {
		Assert.assertEquals(breadcrumImportData.getText(), "HomeImport Data");
	}

	public void manualImpoDataRecdsTileClick() throws Exception {
		Thread.sleep(1000);
		manualImport.click();
	}

	public void verifyManualImLbl() throws Exception {
		Assert.assertEquals(manualImportLabel.getText(), "Manual Import");
	}

	public void verifyManImpBreadCrum() throws Exception {
		Assert.assertEquals(breadcrumManImp.getText(), "HomeImport DataManual Import");
	}

	public void verifyuploadCsFilebl() throws Exception {
		Assert.assertEquals(uploadCsvFileLabel.getText(), "Upload CSV File");
	}

	public void selectDrpDwnModule() throws Exception {
		Select mdulDrpdwn = new Select(moduleDrpDwn);
		mdulDrpdwn.selectByIndex(0);
	}

	public void verifyModuleDrpDwnValMsgColor() throws Exception {
		Assert.assertEquals(new Helper().FontColor(moduleDrpDwnValMsg), "#a94442");
	}

	public void selectDrpDwnType() throws Exception {
		Select typeDrpdwn = new Select(typeDrpDwn);
		typeDrpdwn.selectByIndex(0);
	}

	public void verifyTypeDrpDwnValMsg() throws Exception {
		Assert.assertEquals(typeDrpDwnValMsg.getText(), "Type is required.");
	}

	public void verifyTypeDrpDwnValMsgColor() throws Exception {
		Assert.assertEquals(new Helper().FontColor(typeDrpDwnValMsg), "#a94442");
	}

	public void mappingNameClick() throws Exception {
		Thread.sleep(1000);
		mappingName.click();
	}

	public void verifyMappingNameValMsg() throws Exception {
		Assert.assertEquals(mappingNameValMsg.getText(), "Mapping Name is required.");
	}

	public void verifyMappingNameValMsgColor() throws Exception {
		Assert.assertEquals(new Helper().FontColor(mappingNameValMsg), "#a94442");
	}

	public void enterDupMapName(String mappingNameValue) throws Exception {
		mappingName.sendKeys(mappingNameValue);
		Thread.sleep(1000);
	}
	
	public void verifyMappingNameValMsgForDup() throws Exception {
		Assert.assertEquals(mappingNameValMsgForDup.getText(), "Mapping Name must be unique.");
	}
	
	public void verifyMappingNameValMsgColorForDup() throws Exception {
		Assert.assertEquals(new Helper().FontColor(mappingNameValMsgForDup), "#a94442");
	}
	
	public void enterMinSizeMapName(String mappingNameValue) throws Exception {
		mappingName.sendKeys(mappingNameValue);
		Thread.sleep(1000);
	}
	
	public void verifyMappingNameValMsgForMinSize() throws Exception {
		Assert.assertEquals(mappingNameValMsgForMinSize.getText(), "Mapping Name should be at least 2 characters long.");
	}
	
	public void verifyMappingNameValMsgColorForMinSize() throws Exception {
		Assert.assertEquals(new Helper().FontColor(mappingNameValMsgForMinSize), "#a94442");
	}
	
	public void enterMaxSizeMapName(String mappingNameValue) throws Exception {
		mappingName.sendKeys(mappingNameValue);
		Thread.sleep(1000);
	}
	
	public void verifyMappingNameValMsgForMaxSize() throws Exception {
		Assert.assertEquals(mappingNameValMsgForMaxSize.getText(), "Mapping Name cannot be more than 50 characters long.");
	}
	
	public void verifyMappingNameValMsgColorForMaxSize() throws Exception {
		Assert.assertEquals(new Helper().FontColor(mappingNameValMsgForMaxSize), "#a94442");
	}
	
	public void verifyUploadCsvBtnColor() throws Exception {
		Assert.assertEquals(new Helper().BtnColor(uploadCsvBtn), "#5cb85c");
	}
	
	public void verifyCancelManImBtnColor() throws Exception {
		Assert.assertEquals(new Helper().BtnColor(cancelManUpBtn), "#5a5a5a");
	}
	
	public void clickManImCancelBtn() throws Exception {
		cancelManUpBtn.click();
	}
	
	public void viewSavedMapRecdsTileClick() throws Exception {
		Thread.sleep(1000);
		viewSavedMapping.click();
	}
	
	public void verifyViewSavMaLbl() throws Exception {
		Assert.assertEquals(viewSavedMappingLabel.getText(), "View Saved Mapping");
	}
	
	public void viewMapSrchBoxSendkeys(String SrchValue) {
		viewMapSrchBox.click();
		viewMapSrchBox.sendKeys(SrchValue);
	}
	
	public void verifysearch(String searchedItem) throws Exception {

		AttributesPageObject obj = new AttributesPageObject(ObjectRepo.driver);

		String PaginationVal = null, PaginationSelectedVal = null, srchResultNo = null;
		try {
			PaginationVal = obj.paginationText.getText();
			srchResultNo = obj.srcResultPageNo.getText();
			PaginationSelectedVal = obj.pageSizeDrp_GetSelectedValue();
			new Search().SearchVerification(PaginationVal, PaginationSelectedVal, obj.grid_data, searchedItem,obj.textSearch, srchResultNo);
		} catch (Exception e) {
			System.err.println(e);
		}

	}
	
	public void viewFirstAuditTrailClick() throws Exception {
		Thread.sleep(1000);
		viewAduitTrail.click();
	}
	
}
