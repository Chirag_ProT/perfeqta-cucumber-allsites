package com.cucumber.framework.helper.PageObject;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.Assert;

import com.cucumber.framework.helper.Helper;
import com.cucumber.framework.helper.Button.ButtonHelper;
import com.cucumber.framework.helper.Logger.LoggerHelper;
import com.cucumber.framework.settings.ObjectRepo;

public class LoginPageObject extends PageBase {
	private WebDriver driver;
	public ButtonHelper btnHelper;
	private CommonFunctionPageObject commonFnPageObject;
	private final Logger log = LoggerHelper.getLogger(LoginPageObject.class);
int a =0;
	public LoginPageObject(WebDriver driver) {
		super(driver);
		this.driver = driver;
		btnHelper = new ButtonHelper(driver);
		commonFnPageObject = new CommonFunctionPageObject(ObjectRepo.driver);
	}	
	/** WebElements */

	@FindBy(how = How.XPATH, using = "//*[@ng-if='(loginForm.username.$dirty || loginForm.username.$touched) && loginForm.username.$error.required']")
	public WebElement usernameMandatoryValidation;

	@FindBy(how = How.XPATH, using = "//*[@ng-if='(loginForm.password.$dirty || loginForm.password.$touched) && loginForm.password.$error.required']")
	public WebElement passwordMandatoryValidation;

	@FindBy(how = How.XPATH, using = "//p[@ng-if='vm.loginFailed']")
	public WebElement invalidLoginMessage;

	@FindBy(how = How.XPATH, using = "//*[@ng-bind-html='step.ncyBreadcrumbLabel']")
	public WebElement homepageBreadcrumb;

	@FindBy(how = How.XPATH, using = "//h3[contains(text(),'User Login')]")
	public WebElement loginlabel;

	@FindBy(how = How.XPATH, using = "//button[@ng-click='vm.No()']") // button[@ng-click='vm.No()']
	public WebElement btnNo;

	@FindBy(how = How.XPATH, using = "//button[@ng-click='vm.Yes({id: vm.adminId})']")
	public WebElement btnYes;

	@FindBy(how = How.XPATH, using = "//*[@ng-mousedown='vm.onCLickForgotpassword()']")
	public WebElement forgotPassLink;

	@FindBy(how = How.XPATH, using = "//*[@id=\"forgot-password\"]/h3")
	public WebElement forgotPassLabel;

	@FindBy(how = How.XPATH, using = "//*[@ng-model='vm.forgotPassword.username']")
	public WebElement forgotPassUserName;

	@FindBy(how = How.XPATH, using = "//*[@translate='div.usernamereq']")
	public WebElement forgotPassUsrNameReqValiMsg;

	@FindBy(how = How.XPATH, using = "//*[@translate='div.div-user2char']")
	public WebElement forgotPassUsrNameMinValiMsg;

	@FindBy(how = How.XPATH, using = "//*[@translate='div.div-user20char']")
	public WebElement forgotPassUsrNameMaxValiMsg;

	@FindBy(how = How.XPATH, using = "//*[@ng-click='vm.submit(vm.forgotPassword)']")
	public WebElement forgotPassSubmitBtn;

	@FindBy(how = How.XPATH, using = "//*[@class='btn btn-danger11 btn-default ng-binding']")
	public WebElement forgotPassCancelBtn;

	@FindBy(how = How.XPATH, using = "//*[@id=\"bs-example-navbar-collapse-6\"]/ul/li[1]/a[contains(text(),'Home')]")
	public WebElement homeLabel;

	// --------------------public methods--------------------


	public WebDriver getDriver() {
		return this.driver;
	}

	public void verifyLoginBtnLable() {
		Assert.assertEquals(loginlabel.getText(), "User Login");
	}

	public String sendusername(String unm) throws Exception {
		commonFnPageObject.username.sendKeys(unm);
		return unm;
	}

	public void sendpassword(String pwd) throws Exception {
		commonFnPageObject.password.sendKeys(pwd);
	}

	public String gettitle(String title) throws Exception {
		log.info(title);
		title = driver.getTitle();
		return title;
	}

	public String validateUsernamemsg(String validateUsername) {
		log.info(validateUsername);
		try
		{
			Thread.sleep(1000);
			validateUsername = usernameMandatoryValidation.getText().toString();
		}
		catch (InterruptedException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Assert.assertEquals(validateUsername, "Username is required.");
		return validateUsername;
	}

	public String validatePasswordmsg(String validatePasswrod) {
		log.info(validatePasswrod);
		validatePasswrod = passwordMandatoryValidation.getText().toString();
		Assert.assertEquals(validatePasswrod, "Password is required");
		return validatePasswrod;
	}

	public void verifyInvalidLoginMsg() {
		log.info(" verification of inValidLogin message");
		String inValidLogin = invalidLoginMessage.getText().toString();
		Assert.assertEquals(inValidLogin,
				"The combination of the username and password doesn't match our records. Please check your entries. If you have forgotten your password, please click the Forgot Password? link to recover it.");
	}

	public String homepageverification() {
		log.info(homepageBreadcrumb);
		String homepgbreadcrumb = homepageBreadcrumb.getText().toString();
		Assert.assertEquals(homepgbreadcrumb, "Home");
		return homepgbreadcrumb;
	}

	public void loginlabelverification() {
		log.info(loginlabel);
		Assert.assertEquals(loginlabel.getText(), "User Login");
	}

	public void verifyUserNameReqMsgColor() {
		log.info(usernameMandatoryValidation);
		Assert.assertEquals(btnHelper.FontColorCodeHex(usernameMandatoryValidation), "#a94442");
	}

	public void verifyPasswordReqMsgColor() {
		log.info(passwordMandatoryValidation);
		Assert.assertEquals(btnHelper.FontColorCodeHex(passwordMandatoryValidation), "#a94442");
	}

	public void verifyInvalidLoginMsgColor() {
		log.info(invalidLoginMessage);
		Assert.assertEquals(btnHelper.FontColorCodeHex(invalidLoginMessage), "#a94442");
	}

	public void verifyBtnLoginColor() {
		log.info(commonFnPageObject.btn_login);
		Assert.assertEquals(btnHelper.BtnColor(commonFnPageObject.btn_login), "#2894be");
	}

	public void btnNoColor() {
		log.info("Already loged in user No button color verification");
		Assert.assertEquals(btnHelper.BtnColor(btnNo), "#2894be");
	}

	public void btnYesColor() {
		log.info("Already logged in user Yes buton color verification");
		Assert.assertEquals(btnHelper.BtnColor(btnYes), "#2894be");
	}

	public void verifyforgotPassLinkColor() {
		log.info(forgotPassLink);
		Assert.assertEquals(btnHelper.FontColorCodeHex(forgotPassLink), "#888888");
	}

	public void verifyforgotPassLinkText() {
		log.info(forgotPassLink);
		Assert.assertEquals(forgotPassLink.getText(), "Forgot Password?");
	}

	public void verifyforgotPassLableText() {
		log.info(forgotPassLabel);
		Assert.assertEquals(forgotPassLabel.getText(), "Forgot Password");
	}

	public void forgotPassLinkClick() {
		log.info(forgotPassLink);
		try 
		{
			forgotPassLink.click();

			Thread.sleep(5000);
		} 
		catch 
		(InterruptedException e)
		{
			e.printStackTrace();
		}
	}

	public void verifyforgotPassUsrNameReqValiMsg() {
		log.info(forgotPassUsrNameReqValiMsg);
		Assert.assertEquals(forgotPassUsrNameReqValiMsg.getText(), "Username is required.");
	}

	public void verifyforgotPassUsrNameReqValiMsgColor() {
		log.info(forgotPassUsrNameReqValiMsg);
		Assert.assertEquals(btnHelper.FontColorCodeHex(forgotPassUsrNameReqValiMsg), "#a94442");
	}

	public void verifyforgotPassUsrNameMinValiMsg() {
		log.info(forgotPassUsrNameMinValiMsg);
		Assert.assertEquals(forgotPassUsrNameMinValiMsg.getText(), "Username must be at least 2 characters.");
	}

	public void verifyforgotPassUsrNameMinValiMsgColor() {
		log.info(forgotPassUsrNameMinValiMsg);
		Assert.assertEquals(btnHelper.FontColorCodeHex(forgotPassUsrNameMinValiMsg), "#a94442");
	}

	public void verifyforgotPassUsrNameMaxValiMsg() {
		log.info(forgotPassUsrNameMaxValiMsg);
		Assert.assertEquals(forgotPassUsrNameMaxValiMsg.getText(), "Username cannot be more than 20 characters.");
	}

	public void verifyforgotPassUsrNameMaxValiMsgColor() {
		log.info(forgotPassUsrNameMaxValiMsg);
		Assert.assertEquals(btnHelper.FontColorCodeHex(forgotPassUsrNameMaxValiMsg), "#a94442");
	}

	public void verifyForgotPassSubmitBtnLabel() {
		log.info(forgotPassSubmitBtn);
		Assert.assertEquals(forgotPassSubmitBtn.getText(), "Submit");
	}

	public void verifyForgotPassCancelBtnLabel() {
		log.info(forgotPassCancelBtn);
		Assert.assertEquals(forgotPassCancelBtn.getText(), "Cancel");
	}

	public void verifyForgotPassSubmitBtnColor() {
		log.info(forgotPassSubmitBtn);
		Assert.assertEquals(btnHelper.BtnColor(forgotPassSubmitBtn), "#5cb85c");
	}

	public void verifyForgotPassCancelBtnColor() {
		log.info(forgotPassCancelBtn);
		Assert.assertEquals(btnHelper.BtnColor(forgotPassCancelBtn), "#5a5a5a");
	}

	public void verifyForgotPassSubmitBtnClick() {
		log.info(forgotPassSubmitBtn);
		forgotPassSubmitBtn.click();
	}

	public void verifyForgotPassCancelBtnClick() throws Exception {
		log.info(forgotPassCancelBtn);

		try {
			Thread.sleep(3000);
			new Helper().javascriptExecutorClick(forgotPassCancelBtn);
//			forgotPassCancelBtn.click();
			Thread.sleep(2000);
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("failed to click on cancel btn");
		}
	
	}

	public void verifyHomeLabel() {
		log.info(homeLabel);
		Assert.assertEquals(homeLabel.getText(), "Home");
	}
}
