package com.cucumber.framework.helper.PageObject;

import static org.testng.Assert.assertTrue;

import org.apache.log4j.Logger;
import org.aspectj.apache.bcel.generic.TABLESWITCH;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import com.cucumber.framework.helper.Helper;
import com.cucumber.framework.helper.Button.ButtonHelper;
import com.cucumber.framework.helper.Copy.Copy;
import com.cucumber.framework.helper.Logger.LoggerHelper;
import com.cucumber.framework.helper.Scroll.Scroll;
import com.cucumber.framework.helper.Search.Search;
import com.cucumber.framework.helper.Sorting.SortingOnColumn;
import com.cucumber.framework.settings.ObjectRepo;
import com.cucumber.framework.utility.Constant;
import com.cucumber.framework.utility.ExcelUtils;

public class MasterAppSettingPageObject extends PageBase
{
	private WebDriver driver;
	public ButtonHelper btnHelper;
	private final Logger log = LoggerHelper.getLogger(MasterAppSettingPageObject.class);
	private SortingOnColumn sortColumn;
	private Search search;
	private Helper helper = new Helper();
	private Scroll scroll;
	private Copy copy;
	public ExcelUtils excel;

	public MasterAppSettingPageObject(WebDriver driver)

	{
		super(driver);
		this.driver = driver;
		btnHelper = new ButtonHelper(driver);
		sortColumn = new SortingOnColumn();
		search = new Search();
		scroll = new Scroll();
	}

	public String excelData(int rowVal, int colVal) throws Exception {
		excel = new ExcelUtils();
		excel.setExcelFile(Constant.Path_TestData + Constant.File_TestData, "Questions");
		System.err.println();
		return excel.readXLSFile("Questions", rowVal, colVal);
	}

	@FindBy(how = How.XPATH, using = "//*[@ui-sref='secure.admin.masterqcsettings.list']")
	public WebElement masterAppSettingsTile;

	@FindBy(how = How.XPATH, using = "//*[@class='currentStep ng-binding']")
	public WebElement masterAppSettingsModuleName;

	@FindBy(how = How.XPATH, using = "//ul[@class='nav navbar-nav menu-nav']")
	public WebElement userNameButton;

	@FindBy(how = How.XPATH, using = "//*[@class='currentStep ng-binding']")
	public WebElement editMasterAppBreadCrumbs;

	@FindBy(how = How.XPATH, using = "//*[@ng-click='vm.save(editmasterqcForm)']")
	public WebElement editMasterAppSaveBtn;
	
	@FindBy(how = How.XPATH, using = "//*[@ng-if='!vm.readOnlyPermission']")
	public WebElement editMasterAppCancelBtn;

	@FindBy(how = How.XPATH, using = "//*[contains(normalize-space(text()),'Showing')]")
	public WebElement paginationText;
	
	@FindBy(how = How.ID, using = "//*[@ng-change='vm.onPageNumberChange()']")
	public WebElement pageSizeDrp;

	@FindBy(how = How.XPATH, using = "//*[@ng-if='vm.options.data && vm.options.data.length']")
	public WebElement grid_data;

	@FindBy(how = How.XPATH, using = "//*[@class='paddingLeft-15px ng-binding']")
	public WebElement srcResultPageNo;
	
	@FindBy(how = How.XPATH, using = "//*[@ui-sref='secure.admin.masterqcsettings.edit']")
	public WebElement masterAppAddNewBtn;
	
	@FindBy(how = How.XPATH, using = "//*[@ng-model='vm.masterqc.title']")
	public WebElement masterAppTitle;
	
	@FindBy(how = How.XPATH, using = "//*[@ng-if='(editmasterqcForm.masterqcTextBox.$dirty || editmasterqcForm.masterqcTextBox.$touched) && editmasterqcForm.masterqcTextBox.$error.required']")
	public WebElement masterAppReqVal;
	
	@FindBy(how = How.XPATH, using = "//*[@ng-if='editmasterqcForm.masterqcTextBox.$dirty && editmasterqcForm.masterqcTextBox.$error.unique']")
	public WebElement masterAppUniqueVal;
	
	@FindBy(how = How.XPATH, using = "//*[@ng-if='editmasterqcForm.masterqcTextBox.$dirty && editmasterqcForm.masterqcTextBox.$error.minlength']")
	public WebElement masterAppMiniVal;
	
	@FindBy(how = How.XPATH, using = "//*[@ng-if='editmasterqcForm.masterqcTextBox.$dirty && editmasterqcForm.masterqcTextBox.$error.maxlength']")
	public WebElement masterAppMaxVal;
	
	@FindBy(how = How.XPATH, using = "//*[@ng-model='vm.masterqc.module._id']")
	public WebElement masterAppSelectModule;
	
	@FindBy(how = How.XPATH, using = "//div/div[1]/div[2]/div/button")
	public WebElement masterAppSelectApps;
	
	@FindBy(how = How.XPATH, using = "//*[@ng-disabled='(!(vm.masterqc.qcform.length > 0))']")
	public WebElement masterAppSelectAppsAddBtn;
	
	@FindBy(how = How.XPATH, using = "//*[@ng-click='vm.save(editmasterqcForm)']")
	public WebElement masterAppSaveBtn;
	
	public void clickOnMasterAppSettingsTile() throws Exception
	{
		masterAppSettingsTile.click();

	}
	public void verifyMasterAppSettingsModuleName() throws Exception
	{
		JavascriptExecutor js = (JavascriptExecutor) driver;
		Thread.sleep(1000);
		String val = masterAppSettingsModuleName.getText();
		js.executeScript("arguments[0].scrollIntoView();", userNameButton);
		Assert.assertEquals(val, "Master App Settings");
	}
	public void clickToFirstMasterApp() {
		driver.findElement(By.xpath("//table/tbody/tr[1]/td[1]/span[1]/span/a")).click();
	}
	public void verifyEditMasterAppBreadCrumbs() throws Exception {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		String val = editMasterAppBreadCrumbs.getText();
		Thread.sleep(1000);
		js.executeScript("arguments[0].scrollIntoView();", userNameButton);
		Assert.assertEquals(val, "Add / Edit Master App Settings");

	}
	public void clickOnSaveBtnMasterApp() throws Exception {
		editMasterAppSaveBtn.click();
	}
	public void verifyMasterAppSaveBtn() throws Exception
	{
		Thread.sleep(500);
		scroll.scrollUp(ObjectRepo.driver);
		Assert.assertEquals(masterAppSettingsModuleName.getText(), "Master App Settings");
	}
	public void clickOnCancelBtnMasterApp() throws Exception {
		editMasterAppCancelBtn.click();
	}
	public void verifyMasterAppCancelBtn() throws Exception
	{
		Thread.sleep(500);
		scroll.scrollUp(ObjectRepo.driver);
		Assert.assertEquals(masterAppSettingsModuleName.getText(), "Master App Settings");
	}
	public void verifySaveBtnColor() throws Exception {
		new Scroll().scrollDown(ObjectRepo.driver);
		Assert.assertEquals(btnHelper.BtnColor(editMasterAppSaveBtn), "#5cb85c");
	}
	public void verifyCancelBtnColor() throws Exception {
		new Scroll().scrollDown(ObjectRepo.driver);
		Assert.assertEquals(btnHelper.BtnColor(editMasterAppCancelBtn), "#5a5a5a");
	}
	public void verifysearch(String searchedItem) throws Exception {

		QuestionsPageObject obj = new QuestionsPageObject(ObjectRepo.driver);

		String PaginationVal=null, PaginationSelectedVal=null,srchResultNo=null;

		PaginationVal = obj.paginationText.getText();
		srchResultNo = obj.srcResultPageNo.getText();
		PaginationSelectedVal = obj.pageSizeDrp_GetSelectedValue();
		search.SearchVerification(PaginationVal, PaginationSelectedVal, obj.grid_data, searchedItem, obj.textSearch,srchResultNo);
	}
	public String pageSizeDrp_GetSelectedValue() {
		String defaultItem;
		try 
		{
			if (driver.findElements(By.xpath("//*[@ng-change='vm.onPageNumberChange()']")).size() != 0) 
			{
				Select select = new Select(pageSizeDrp);
				WebElement option = select.getFirstSelectedOption();
				defaultItem = option.getText();
				return defaultItem;
			}
			else 
			{
				defaultItem = "1";
				return defaultItem;
			}
		}
		catch(Exception e)
		{
			defaultItem = "1";
		}
		return defaultItem;
	}
	public void verifyAscendingOrder() {
		System.out.println("click on ascending");
	}
	public void verifyAscendingOrderResult(String sortingColName) {
		try 
		{
			String PaginationVal = paginationText.getText();
			String PaginationSelectedVal = pageSizeDrp_GetSelectedValue();
			sortColumn.verifySorting(sortingColName, 1, PaginationVal, PaginationSelectedVal, grid_data);
		}
		catch (Exception e)
		{
			System.out.println(e);
		}
	}
	public void clickOnMasterAppAddNewBtn() throws Exception
	{
		masterAppAddNewBtn.click();
	}
	public void masterAppNamesendkeys(String masterappnameminival) throws Exception
	{
		Thread.sleep(1000);
		masterAppTitle.clear();
		masterAppTitle.sendKeys(masterappnameminival);
	}
	public void verifyMasterAppNameReqVal()
	{
		try
		{
			Thread.sleep(1000);
			String masterappval = masterAppReqVal.getText();
			Assert.assertEquals("Master App is required.", masterappval);
		}
		catch(Exception e)
		{
			e.toString();
		}
	}
	public void verifyMasterAppNameUniqueVal()
	{
		try
		{
			Thread.sleep(1000);
			Assert.assertEquals("Master App must be unique.", masterAppUniqueVal.getText());
		}
		catch(Exception e)
		{
			e.toString();
		}
	}
	public void verifyMasterAppNameMiniVal()
	{
		try
		{
			Thread.sleep(1000);
			String masterappnameminival = masterAppMiniVal.getText();
			Assert.assertEquals("Master App must be at least 2 characters.", masterappnameminival);
		}
		catch(Exception e)
		{
			e.toString();
		}
	}
	public void verifyMasterAppNameMaxVal()
	{
		try
		{
			Thread.sleep(1000);
			String masterappnamemaxval = masterAppMaxVal.getText();
			Assert.assertEquals("Master App cannot be more than 50 characters.", masterappnamemaxval);
		}
		catch(Exception e)
		{
			e.toString();
		}
	}
	public void verifyMasterAppNameNoVal()
	{
		boolean reqMsg = ObjectRepo.driver.findElements(By.xpath("//*[@ng-if='(editmasterqcForm.masterqcTextBox.$dirty || editmasterqcForm.masterqcTextBox.$touched) && editmasterqcForm.masterqcTextBox.$error.required']")).size() == 0,

				minMsg = ObjectRepo.driver.findElements(By.xpath("//*[@ng-if='editmasterqcForm.masterqcTextBox.$dirty && editmasterqcForm.masterqcTextBox.$error.minlength']")).size() == 0,

				maxMsg = ObjectRepo.driver.findElements(By.xpath("//*[@ng-if='editmasterqcForm.masterqcTextBox.$dirty && editmasterqcForm.masterqcTextBox.$error.maxlength']")).size() == 0,                  

				uniqMsg = ObjectRepo.driver.findElements(By.xpath("//*[@ng-if='editmasterqcForm.masterqcTextBox.$dirty && editmasterqcForm.masterqcTextBox.$error.unique']")).size() == 0,                         

				flag;

		if (reqMsg && minMsg && maxMsg && uniqMsg) {
			flag = true;
		} else {
			flag = false;
		}
		Assert.assertEquals(flag, true);
	}
	public void selectMasterModule() throws Exception
	{
		helper.SelectDrpDwnValueByText(masterAppSelectModule, "Blood");
	}
	public void selectMasterApps() throws Exception
	{
		masterAppSelectApps.click();
		Thread.sleep(500);
		for(int i=6; i<8; i++)
		{
			driver.findElement(By.xpath("//div[2]/div/ul/li["+i+"]/a/div/label/input")).click();
		}
	}
	public void clickOnselectMasterAppAddBtn() throws Exception
	{
		masterAppSelectAppsAddBtn.click();
	}
	public void clickOnSaveBtn() throws Exception
	{
		masterAppSaveBtn.click();
		Thread.sleep(3000);
	}
}