package com.cucumber.framework.helper.PageObject;

import static org.testng.Assert.assertEquals;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.Assert;

import com.cucumber.framework.configreader.PropertyFileReader;
import com.cucumber.framework.helper.Helper;
import com.cucumber.framework.helper.InitializeWebDrive;
import com.cucumber.framework.helper.Button.ButtonHelper;
import com.cucumber.framework.helper.Logger.LoggerHelper;
import com.cucumber.framework.helper.Scroll.Scroll;
import com.cucumber.framework.settings.ObjectRepo;

public class MyProfilePageObject extends PageBase {
	private WebDriver driver;
	public ButtonHelper btnHelper;
	private final Logger log = LoggerHelper.getLogger(MyProfilePageObject.class);
	private Helper helper = new Helper();
	
	public MyProfilePageObject(WebDriver driver) {
		super(driver);
		this.driver = driver;
		btnHelper = new ButtonHelper(driver);
	}

	/** WebElements */

	@FindBy(how = How.XPATH, using = "//i[@class='header-username-bx ng-binding']")
	public WebElement usernameTab;
	
	@FindBy(how = How.XPATH, using = "//a[@ui-sref='secure.myprofile']")
	public WebElement clickMyProfile;
	
	@FindBy(how = How.XPATH, using = "//span[@class='currentStep ng-binding']")
	public WebElement moduleNameMyProfile;
	
	@FindBy(how = How.XPATH, using = "//li[@ng-repeat='step in steps | limitTo:(steps.length-1)']")
	public WebElement myProfileBreadcrumbs;
	
	@FindBy(how = How.XPATH, using = "//label[@for='userNameTextBox']")
	public WebElement usernameLabel;
	
	@FindBy(how = How.XPATH, using = "//input[@name='userNameTextBox']")
	public WebElement usernameFieldDisable;
	
	@FindBy(how = How.XPATH, using = "//label[@for='firstNameTextBox']")
	public WebElement firstnameLabel;
	
	@FindBy(how = How.XPATH, using = "//input[@name='firstNameTextBox']")
	public WebElement firstnameSameAsLogin;
	
	@FindBy(how = How.XPATH, using = "//p[@translate='secure.admin.user.myprofile.myprofile.FNAMEREQ']")
	public WebElement firstnameRequiredMsg;
	
	@FindBy(how = How.XPATH, using = "//p[@translate='secure.admin.user.myprofile.myprofile.FNAMESHOULDAT2']")
	public WebElement firstnameMinimumMsg;
	
	@FindBy(how = How.XPATH, using = "//p[@translate='secure.admin.user.myprofile.myprofile.FNAMENOTMOR50']")
	public WebElement firstnameMaximumMsg;
	
	@FindBy(how = How.XPATH, using = "//label[@for='lastNameTextBox']")
	public WebElement lastnameLabel;
	
	@FindBy(how = How.XPATH, using = "//input[@name='lastNameTextBox']")
	public WebElement lastnameVal;
	
	@FindBy(how = How.XPATH, using = "//p[@translate='secure.admin.user.myprofile.myprofile.LASTNAMEREQ']")
	public WebElement lastnameRequiredMsg;
	
	@FindBy(how = How.XPATH, using = "//p[@translate='secure.admin.user.myprofile.myprofile.LNAMESHOULDAT2']")
	public WebElement lastnameMinimumMsg;
	
	@FindBy(how = How.XPATH, using = "//p[@translate='secure.admin.user.myprofile.myprofile.LNAMENOTMOR50']")
	public WebElement lastnameMaximumMsg;
	
	@FindBy(how = How.XPATH, using = "//label[@for='emailTextBox']")
	public WebElement emailLabel;
	
	@FindBy(how = How.XPATH, using = "//input[@name='emailTextBox']")
	public WebElement emailTextfield;
	
	@FindBy(how = How.XPATH, using = "//p[@translate='secure.admin.user.myprofile.myprofile.EMAILREQ']")
	public WebElement emailAddressRequired;
	
	@FindBy(how = How.XPATH, using = "//button[@class='btn btn-success ng-binding']")
	public WebElement saveBtn;
	
	@FindBy(how = How.XPATH, using = "//p[@translate='secure.admin.user.myprofile.myprofile.INVALIDEMAIL']")
	public WebElement emailAddressInvalid;
	
	@FindBy(how = How.XPATH, using = "//p[@translate='secure.importdata.automated.Email50']")
	public WebElement emailAddressMaximum;
	
	@FindBy(how = How.XPATH, using = "//p[@translate='secure.admin.user.myprofile.myprofile.EMAILMUSTBUNI']")
	public WebElement emailAddressUnique;
	
	@FindBy(how = How.XPATH, using = "//*[@ng-if='vm.user.newEmail']")
	public WebElement verifiedEmailInfo;
	
	@FindBy(how = How.XPATH, using = "//a[@ng-click='vm.cancelEmailVerificatoin(editMyProfileForm)']")
	public WebElement cancelVerificationBtn;
	
	// ----------------------------------------- Security Question -----------------------------------------
	
	@FindBy(how = How.XPATH, using = "//button[@ui-sref='secure.myprofile.changesecurity']")
	public WebElement clickSecQues;
	
	@FindBy(how = How.XPATH, using = "//div[@class='breadcrumb ng-binding']")
	public WebElement secQuesTitle;
	
	@FindBy(how = How.XPATH, using = "//label[@for='questionSelect']")
	public WebElement secQuesFieldName;
	
	@FindBy(how = How.XPATH, using = "//label[@for='answerTextBox']")
	public WebElement answerFieldName;
	
	@FindBy(how = How.XPATH, using = "//input[@name='answerTextBox']")
	public WebElement clickAnswerField;
	
	@FindBy(how = How.XPATH, using = "//select[@name='questionSelect']")
	public WebElement defaultDrpDwnVal;
	
	@FindBy(how = How.XPATH, using = "//p[@translate='secure.admin.user.myprofile.changesecurity.SECQUEREQ']")
	public WebElement secQuesRequiredMsg;
	
	@FindBy(how = How.XPATH, using = "//p[@translate='secure.admin.user.myprofile.changesecurity.ANSISREQ']")
	public WebElement answerRequiredMsg;
	
	@FindBy(how = How.XPATH, using = "//p[@translate='secure.admin.user.myprofile.changesecurity.ANSSHOULDAT2CHAR']")
	public WebElement answerMinimumMsg;
	
	@FindBy(how = How.XPATH, using = "//p[@translate='secure.admin.user.myprofile.changesecurity.ANSNOTMOR50']")
	public WebElement answerMaximumMsg;

	@FindBy(how = How.XPATH, using = "//button[@ng-click='vm.save(vm.user,editSecuritySettingsForm)']")
	public WebElement saveAnswerBtn;
	
	@FindBy(how = How.XPATH, using = "//p[@ng-bind-html='vm.result1']")
	public WebElement saveSuccessMsg;
	// ----------------------------------------- End of Security Question -----------------------------------------
	// -==========================================================================================================-
	// ---------------------------------------------- Change Password ---------------------------------------------
	
	@FindBy(how = How.XPATH, using = "//button[@ui-sref='secure.myprofile.changepassword']")
	public WebElement changePassBtn;
	
	@FindBy(how = How.XPATH, using = "//div[@class='breadcrumb ng-binding']")
	public WebElement changePassTitle;
	
	@FindBy(how = How.XPATH, using = "//label[@for='currentPasswordTextBox']")
	public WebElement changePassLabel;
	
	@FindBy(how = How.XPATH, using = "//label[@for='newPasswordTextBox']")
	public WebElement newPassLabel;
	
	@FindBy(how = How.XPATH, using = "//label[@for='confirmPasswordTextBox']")
	public WebElement confirmPassLabel;
	
	@FindBy(how = How.XPATH, using = "//span[@uib-tooltip='You cannot reuse your recent 4 passwords.']")
	public WebElement informationIcon;
	
	@FindBy(how = How.XPATH, using = "//input[@name='currentPasswordTextBox']")
	public WebElement currentPassField;
	
	@FindBy(how = How.XPATH, using = "//p[@translate='secure.admin.user.myprofile.changepassword.CURRENTPASSREQ']")
	public WebElement currentPassRequiredMsg;
	
	@FindBy(how = How.XPATH, using = "//input[@name='newPasswordTextBox']")
	public WebElement newPassField;
	
	@FindBy(how = How.XPATH, using = "//input[@name='confirmPasswordTextBox']")
	public WebElement confirmPassField;
	
	@FindBy(how = How.XPATH, using = "//button[@ng-click='vm.save(vm.changepassword,editChangePasswordForm)']")
	public WebElement saveBtnChangePass;
	
	@FindBy(how = How.XPATH, using = "//div[1]/p")
	public WebElement currentPassInvalid;
	
	@FindBy(how = How.XPATH, using = "//p[@translate='secure.admin.user.myprofile.changepassword.NEWPASSREQ']")
	public WebElement newPassRequiredMsg;
	
	@FindBy(how = How.XPATH, using = "//p[@ng-if='(editChangePasswordForm.confirmPasswordTextBox.$dirty && vm.changepassword.confirmPassword != (vm.changepassword.newPassword || editChangePasswordForm.newPasswordTextBox.$viewValue)) && vm.changepassword.confirmPassword']")
	public WebElement confirmPassMismatch;
	
	@FindBy(how = How.XPATH, using = "//span[@ng-if='editChangePasswordForm.newPasswordTextBox.$dirty && editChangePasswordForm.newPasswordTextBox.$error.pattern']")
	public WebElement invalidMsg;
	
	@FindBy(how = How.XPATH, using = "//p[@translate='secure.admin.user.myprofile.changepassword.CONFIRMPASSREQ']")
	public WebElement confirmPassRequired;
	
	@FindBy(how = How.XPATH, using = "//p[@translate='secure.admin.user.myprofile.changepassword.CURNEWNOTSAME']")
	public WebElement newCurrentSamePass;
	
	@FindBy(how = How.XPATH, using = "//button[@ng-click='vm.cancel()']")
	public WebElement cancelBtn;
	
	@FindBy(how = How.XPATH, using = "//*[@ui-sref='secure.myprofile.changeverificationcode']")
	public WebElement changeESignPINBtn;
	
	@FindBy(how = How.XPATH, using = "//*[@for='currentVerificationCodeTextBox']")
	public WebElement changeESignPINFirstLabl;
	
	@FindBy(how = How.XPATH, using = "//*[@for='newVerificationCodeTextBox']")
	public WebElement changeESignPINSecondLabl;
	
	@FindBy(how = How.XPATH, using = "//*[@for='confirmNewVerificationCodeTextBox']")
	public WebElement changeESignPINThirdLabl;
	
	@FindBy(how = How.XPATH, using = "//*[@name='currentVerificationCodeTextBox']")
	public WebElement currentSignTextBox;
	
	@FindBy(how = How.XPATH, using = "//*[@translate='secure.admin.user.myprofile.changeverificationcode.CUREPINREQ']")
	public WebElement currentSignTextBoxValMsg;
	
	@FindBy(how = How.XPATH, using = "//*[@name='newVerificationCodeTextBox']")
	public WebElement newSignTextBox;
	
	@FindBy(how = How.XPATH, using = "//*[@name='confirmNewVerificationCodeTextBox']")
	public WebElement confirmSignTextBox;
	
	@FindBy(how = How.XPATH, using = "//*[@ng-click='vm.save(editVerificationCodeForm)']")
	public WebElement saveBtnOfESign;
	
	@FindBy(how = How.XPATH, using = "(//*[@ng-bind-html='vm.result'])[2]")
	public WebElement currentSignInValSignMsg;
	
	@FindBy(how = How.XPATH, using = "//*[@translate='secure.admin.user.myprofile.changeverificationcode.NEWPINREQ']")
	public WebElement newSignValMsg;
	
	@FindBy(how = How.XPATH, using = "//*[@translate='secure.admin.user.myprofile.changeverificationcode.CONNEWESIREQ']")
	public WebElement confirmValMsg;
	
	@FindBy(how = How.XPATH, using = "//*[@translate='secure.admin.user.myprofile.changeverificationcode.CURNEWNOTSAME']")
	public WebElement newSignValMsgForDup;
	
	@FindBy(how = How.XPATH, using = "//*[@translate='secure.admin.user.myprofile.changeverificationcode.ESIGNMISSMATCH']")
	public WebElement confirmSignValMsgForMisMatch;
	
	@FindBy(how = How.XPATH, using = "//*[@translate='secure.admin.user.myprofile.changeverificationcode.NEWEPINAT2']")
	public WebElement newSignValMsgForMin;
	
	@FindBy(how = How.XPATH, using = "//*[@translate='secure.admin.user.myprofile.changeverificationcode.NEWEPINNOT50']")
	public WebElement newSignValMsgForMax;
	
	@FindBy(how = How.XPATH, using = "//*[@translate='secure.admin.user.myprofile.changeverificationcode.CONESIGNAT2']")
	public WebElement confirmSignValMsgForMin;
	
	@FindBy(how = How.XPATH, using = "//*[@translate='secure.admin.user.myprofile.changeverificationcode.CONESIGNNOT50']")
	public WebElement confirmSignValMsgForMax;
	
	@FindBy(how = How.XPATH, using = "//*[@class='alert alert-danger col-sm-12']")
	public WebElement generalErr;
	
	@FindBy(how = How.XPATH, using = "//*[@ng-click='vm.cancel()']")
	public WebElement cancelSignBtn;
	
	


	//	------ Public Methods ------
	
	public void clickUsernameHomePage() throws Exception{
		log.info(usernameTab);
		usernameTab.click();
	}
	
	public void clickMyProfileLink() throws Exception{
		log.info(clickMyProfile);
		clickMyProfile.click();
	}
	
	public void verifyMyProfileModuleName() throws Exception{
		log.info(clickMyProfile);
		Assert.assertEquals(moduleNameMyProfile.getText(), "My Profile");
	}
	
	public void clickMyProfileBreadcrumbs() throws Exception{
		log.info(myProfileBreadcrumbs);
		myProfileBreadcrumbs.click();
		Thread.sleep(1000);
	}
	
	public void verifyPreviousPageOfMyProfile() throws Exception{
		log.info(moduleNameMyProfile);
		Assert.assertEquals(moduleNameMyProfile.getText(), "Home");
	}
	
	public void verifyUsernameLabel() throws Exception{
		log.info(usernameLabel);
		Assert.assertEquals(usernameLabel.getText(), "Username *");
	}
	
	public void verifyUsernameLableDisable() throws Exception{
		log.info(usernameFieldDisable);
		boolean actual = usernameFieldDisable.isEnabled();
		Assert.assertEquals(false, actual);
	}
	
	public void verifyFirstNameLabel() throws Exception{
		log.info(firstnameLabel);
		String actual = firstnameLabel.getText();
		Assert.assertEquals(actual, "First Name *");
	}
	
	public void verifyFirstNameAndLoginSame() throws Exception{
		log.info(firstnameSameAsLogin);
		String actual = helper.getClipboardContents(firstnameSameAsLogin);
		String expected = usernameTab.getText();
		Assert.assertEquals(actual, expected);
	}
	
	public void clearFirstNameField() throws Exception{
		log.info(firstnameSameAsLogin);
		firstnameSameAsLogin.clear();
		firstnameSameAsLogin.sendKeys(Keys.TAB);
	}
	
	public void verifyFirstNameRequiredMsg() throws Exception{
		log.info(firstnameRequiredMsg);
		Assert.assertEquals(firstnameRequiredMsg.getText(), "First Name is required.");
	}
	
	public void verifyFirstNameRequiredMsgColor() throws Exception{
		log.info(firstnameRequiredMsg);
		Assert.assertEquals(helper.FontColor(firstnameRequiredMsg), "#a94442");
	}
	
	public void minimumFirstnameMsg(String minVal) throws Exception{
		log.info(firstnameSameAsLogin);
		firstnameSameAsLogin.clear();
		firstnameSameAsLogin.sendKeys(minVal);
	}
	
	public void verifyFirstNameMinimumMsg() throws Exception{
		log.info(firstnameMinimumMsg);
		Assert.assertEquals(firstnameMinimumMsg.getText(), "Firstname must be at least 2 characters.");
	}
	
	public void verifyFirstNameMinimumMsgColor() throws Exception{
		log.info(firstnameMinimumMsg);
		Assert.assertEquals(helper.FontColor(firstnameMinimumMsg), "#a94442");
	}
	
	public void maximumFirstnameMsg(String maxVal) throws Exception{
		log.info(firstnameSameAsLogin);
		firstnameSameAsLogin.clear();
		firstnameSameAsLogin.sendKeys(maxVal);
	}
	
	public void verifyFirstNameMaximumMsg() throws Exception{
		log.info(firstnameMaximumMsg);
		Assert.assertEquals(firstnameMaximumMsg.getText(), "First Name cannot be more than 50 characters.");
	}
	
	public void verifyFirstNameMaximumMsgColor() throws Exception{
		log.info(firstnameMaximumMsg);
		Assert.assertEquals(helper.FontColor(firstnameMaximumMsg), "#a94442");
	}
	
	public void verifyLastnameLabel() throws Exception{
		log.info(lastnameLabel);
		Assert.assertEquals(lastnameLabel.getText(), "Last Name *");
	}
	
	public void lastnameValidValue(String lastVal) throws Exception{
		log.info(lastnameVal);
		lastnameVal.clear();
		lastnameVal.sendKeys(lastVal);
	}
	
	public void lastnameEmptyValue(String lastVal) throws Exception{
		log.info(lastnameVal);
		lastnameVal.sendKeys(lastVal);
		lastnameVal.clear();
	}
	
	public void verifyLastnameRequiredMsg() throws Exception{
		log.info(lastnameRequiredMsg);
		Assert.assertEquals(lastnameRequiredMsg.getText(), "Last Name is required.");
	}
	
	public void verifyLastnameRequiredMsgColor() throws Exception{
		log.info(lastnameRequiredMsg);
		Assert.assertEquals(helper.FontColor(lastnameRequiredMsg), "#a94442");
	}
	
	public void enterOneCharLastname(String minVal) throws Exception{
		log.info(lastnameVal);
		lastnameVal.clear();
		lastnameVal.sendKeys(minVal);
	}

	public void verifyLastnameMinimumMsg() throws Exception{
		log.info(lastnameMinimumMsg);
		String firstresult = lastnameMinimumMsg.getText();
		String secondresult = "Last Name must be at least 2 characters.";
		Assert.assertEquals(firstresult, secondresult);
	}
	
	public void verifyLastnameMinimumMsgColor() throws Exception{
		log.info(lastnameMinimumMsg);
		Assert.assertEquals(helper.FontColor(lastnameMinimumMsg), "#a94442");
	}
	
	public void enterFiftyOneCharLastname(String maxVal) throws Exception{
		log.info(lastnameVal);
		lastnameVal.clear();
		lastnameVal.sendKeys(maxVal);
	}

	public void verifyLastnameMaximumMsg() throws Exception{
		log.info(lastnameMaximumMsg);
		String actual = lastnameMaximumMsg.getText();
		String expected = "Last Name cannot be more than 50 characters.";
		Assert.assertEquals(actual, expected);
	}
	
	public void verifyLastnameMaximumMsgColor() throws Exception{
		log.info(lastnameMaximumMsg);
		Assert.assertEquals(helper.FontColor(lastnameMaximumMsg), "#a94442");
	}
	
	public void verifyEmailLabel() throws Exception{
		log.info(emailLabel);
		Assert.assertEquals(emailLabel.getText(), "Email Address *");
	}
	
	public void emailEmptyValue(String emailEmptyVal) throws Exception{
		log.info(emailTextfield);
		emailTextfield.sendKeys(emailEmptyVal);
		emailTextfield.clear();
		saveBtn.click();
	}
	
	public void verifyEmailAddressRequiredMsg() throws Exception{
		log.info(emailAddressRequired);
		Assert.assertEquals(emailAddressRequired.getText(), "Email Address is required.");
	}
	
	public void verifyEmailAddressRequiredMsgColor() throws Exception{
		log.info(emailAddressRequired);
		Assert.assertEquals(helper.FontColor(emailAddressRequired), "#a94442");
	}
	
	public void invalidEmailAddress(String emailEmptyVal) throws Exception{
		log.info(emailTextfield);
		emailTextfield.clear();
		emailTextfield.sendKeys(emailEmptyVal);
	}
	
	public void verifyEmailAddressInvalidMsg() throws Exception{
		log.info(emailAddressInvalid);
		Assert.assertEquals(emailAddressInvalid.getText(), "Invalid Email Address.");
	}
	
	public void verifyEmailAddressInvalidMsgColor() throws Exception{
		log.info(emailAddressInvalid);
		Assert.assertEquals(helper.FontColor(emailAddressInvalid), "#a94442");
	}
	
	public void maximumEmailAddress(String maxVal) throws Exception{
		log.info(emailTextfield);
		emailTextfield.clear();
		emailTextfield.sendKeys(maxVal);
	}
	
	public void verifyEmailAddressMaximumMsg() throws Exception{
		log.info(emailAddressMaximum);
		Assert.assertEquals(emailAddressMaximum.getText(), "Email Address cannot be more than 50 characters long.");
	}
	
	public void verifyEmailAddressMaximumMsgColor() throws Exception{
		log.info(emailAddressMaximum);
		Assert.assertEquals(helper.FontColor(emailAddressMaximum), "#a94442");
	}
	
	
	public void enterValidFirstName(String ValidFirstName) throws Exception{
		log.info(firstnameSameAsLogin);
		firstnameSameAsLogin.clear();
		firstnameSameAsLogin.sendKeys(ValidFirstName);
	}
	
	public void verifyFirstNameWithValidValue() throws Exception{
		log.info(firstnameMinimumMsg);
		boolean actual;
		boolean min = driver.findElements(By.xpath("//p[@translate='secure.admin.user.myprofile.myprofile.FNAMESHOULDAT2']")).size() == 0;
		boolean max = driver.findElements(By.xpath("//p[@translate='secure.admin.user.myprofile.myprofile.FNAMENOTMOR50']")).size() == 0;
		boolean required = driver.findElements(By.xpath("//p[@translate='secure.admin.user.myprofile.myprofile.FNAMEREQ']")).size() == 0;
		if(min == true && max == true && required == true )
		{
			actual = true;
		} else {
			actual= false;
		}
		Assert.assertEquals(actual,true);
	}
	
	public void enterValidLastName(String ValidLastName) throws Exception{
		log.info(lastnameVal);
		lastnameVal.clear();
		lastnameVal.sendKeys(ValidLastName);
	}
	
	public void verifyLastNameWithValidValue() throws Exception{
		log.info(lastnameVal);
		boolean actual;
		boolean min = driver.findElements(By.xpath("//p[@translate='secure.admin.user.myprofile.myprofile.LNAMESHOULDAT2']")).size() == 0;
		boolean max = driver.findElements(By.xpath("//p[@translate='secure.admin.user.myprofile.myprofile.LNAMENOTMOR50']")).size() == 0;
		boolean required = driver.findElements(By.xpath("//p[@translate='secure.admin.user.myprofile.myprofile.LASTNAMEREQ']")).size() == 0;
		if(min == true && max == true && required == true )
		{
			actual = true;
		} else {
			actual= false;
		}
		Assert.assertEquals(actual,true);
	}
	
	public void uniqueEmailAddress(String uniqueEmail) throws Exception{
		log.info(emailTextfield);
		emailTextfield.clear();
		emailTextfield.sendKeys(uniqueEmail);
	}
	
	public void uniqueEmailAddressMsg() throws Exception{
		log.info(emailTextfield);
		Assert.assertEquals(emailAddressUnique.getText(), "Email Address must be unique.");
	}
	
	public void emailAddressVerification(String verifyEmail) throws Exception{
		log.info(emailTextfield);
		emailTextfield.clear();
		emailTextfield.sendKeys(verifyEmail);
	}
	
	public void clickSaveBtn() throws Exception{
		log.info(saveBtn);
		saveBtn.click();
	}
	
	public void verifyLastVerifiedEmailMsg() throws Exception{
		log.info(verifiedEmailInfo);
		String actual = verifiedEmailInfo.getText();
		String expected = "Last verified email is automation@yopmail.in\n" + 
                "( Cancel verification | Resend verification email )";
		Assert.assertEquals(actual, expected);
		cancelVerificationBtn.click();
	}
	
	// ----------------------------------------- Security Question ----------------------------------------- 
	
	public void clickSecurityQuestion() throws Exception{
		log.info(clickSecQues);
		clickSecQues.click();
	}
	
	public void verifySecurityQuestionTitle() throws Exception{
		log.info(secQuesTitle);
		Thread.sleep(1500);
		String actual = secQuesTitle.getText();
		Assert.assertEquals(actual, "Security Question");
	}
	
	public void verifySecurityQuestionFieldName() throws Exception{
		log.info(secQuesFieldName);
		Assert.assertEquals(secQuesFieldName.getText(), "Security Question *");
	}
	
	public void verifySecurityQuestionAnswer() throws Exception{
		log.info(answerFieldName);
		new Scroll().scrollDown(ObjectRepo.driver);
		Assert.assertEquals(answerFieldName.getText(), "Answer *");
	}
	
	public void selectDefaultValInDrpDwn() throws Exception{
		log.info(defaultDrpDwnVal);
		Thread.sleep(1500);
		helper.SelectDrpDwnValueByText(defaultDrpDwnVal, "--Select Security Question--");
	}
	
	public void verifyRequiredMsgSecQuesDrpDwn() throws Exception{
		log.info(secQuesRequiredMsg);
		Assert.assertEquals(secQuesRequiredMsg.getText(), "Security Question is required.");
	}
	
	public void verifyRequiredMsgSecQuesDrpDwnColor() throws Exception{
		log.info(secQuesRequiredMsg);
		Assert.assertEquals(helper.FontColor(secQuesRequiredMsg), "#a94442");
	}
	
	public void clickAnswerTextbox() throws Exception{
		log.info(clickAnswerField);
		Thread.sleep(1000);
		clickAnswerField.click();
	}
	
	public void clickAnswerTextboxTab() throws Exception{
		log.info(clickAnswerField);
		Thread.sleep(500);
		clickAnswerField.sendKeys(Keys.TAB);
	}
	
	public void verifyAnswerRequiredMsg() throws Exception{
		log.info(answerRequiredMsg);
		Thread.sleep(1500);
		new Scroll().scrollDown(ObjectRepo.driver);
		Assert.assertEquals(answerRequiredMsg.getText(), "Answer is required.");
	}
	
	public void verifyAnswerRequiredMsgColor() throws Exception{
		log.info(answerRequiredMsg);
		new Scroll().scrollDown(ObjectRepo.driver);
		Assert.assertEquals(helper.FontColor(answerRequiredMsg), "#a94442");
	}
	
	public void enterOneCharAnswer(String minAnswer) throws Exception{
		log.info(clickAnswerField);
		clickAnswerField.clear();
		clickAnswerField.sendKeys(minAnswer);
	}
	
	public void verifyAnswerMinimumMsg() throws Exception{
		log.info(answerMinimumMsg);
		Thread.sleep(1500);
		new Scroll().scrollDown(ObjectRepo.driver);
		Assert.assertEquals(answerMinimumMsg.getText(), "Answer must be at least 2 characters.");
	}
	
	public void verifyAnswerMinimumMsgColor() throws Exception{
		log.info(answerMinimumMsg);
		Assert.assertEquals(helper.FontColor(answerMinimumMsg), "#a94442");
	}
	
	public void enterFiftyOneCharAnswer(String maxAnswer) throws Exception{
		log.info(clickAnswerField);
		clickAnswerField.clear();
		clickAnswerField.sendKeys(maxAnswer);
	}
	
	public void verifyAnswerMaximumMsg() throws Exception{
		log.info(answerMaximumMsg);
		Thread.sleep(1500);
		Assert.assertEquals(answerMaximumMsg.getText(), "Answer cannot be more than 50 characters.");
	}
	
	public void verifyAnswerMaximumMsgColor() throws Exception{
		log.info(answerMaximumMsg);
		Thread.sleep(1000);
		Assert.assertEquals(helper.FontColor(answerMaximumMsg), "#a94442");
	}
	
	public void enterValidAnswer(String validAnswer) throws Exception{
		log.info(clickAnswerField);
		clickAnswerField.sendKeys(validAnswer);
		Thread.sleep(1000);
	}
	
	public void verifyAnswerWithValidValue() throws Exception{
		log.info(clickAnswerField);
		boolean actual;
		boolean min = driver.findElements(By.xpath("//p[@translate='secure.admin.user.myprofile.changesecurity.ANSSHOULDAT2CHAR']")).size() == 0;
		boolean max = driver.findElements(By.xpath("//p[@translate='secure.admin.user.myprofile.changesecurity.ANSNOTMOR50']")).size() == 0;
		boolean required = driver.findElements(By.xpath("//p[@translate='secure.admin.user.myprofile.changesecurity.ANSISREQ']")).size() == 0;
		if(min == true && max == true && required == true )
		{
			actual = true;
		} else {
			actual= false;
		}
		Assert.assertEquals(actual,true);
	}
	
	public void clickAnswersaveBtn() throws Exception{
		log.info(saveAnswerBtn);
		saveAnswerBtn.click();
		Thread.sleep(1000);
	}
	
	public void verifySaveSuccessMsgColor() throws Exception{
		log.info(saveSuccessMsg);
		Thread.sleep(500);
		Assert.assertEquals(helper.FontColor(saveSuccessMsg), "#008000");
	}
	
	public void verifySaveSuccessMsg() throws Exception{
		log.info(saveSuccessMsg);
		Assert.assertEquals(saveSuccessMsg.getText(), "Security Question has changed successfully.");
	}
	
	// ----------------------------------------- Change Password -----------------------------------------
	
	public void clickChangePassBtn() throws Exception{
		Thread.sleep(500);
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", changePassBtn);
		Thread.sleep(500);
	}
	
	public void verifyChangePassTitle() throws Exception{
		new Scroll().scrollDown(ObjectRepo.driver);
		Assert.assertEquals(changePassTitle.getText(), "Change Password");
	}
	
	public void verifyChangePassLabel() throws Exception{
		log.info(changePassLabel);
		new Scroll().scrollDown(ObjectRepo.driver);
		Assert.assertEquals(changePassLabel.getText(), "Current Password *");
	}
	
	public void verifyNewPassLabel() throws Exception{
		log.info(newPassLabel);
		new Scroll().scrollDown(ObjectRepo.driver);
		Assert.assertEquals(newPassLabel.getText(), "New Password *");
	}
	
	public void verifyConfirmPassLabel() throws Exception{
		log.info(confirmPassLabel);
		new Scroll().scrollDown(ObjectRepo.driver);
		Assert.assertEquals(confirmPassLabel.getText(), "Confirm Password *");
	}
	
	public void clickInformationIcon() throws Exception{
		log.info(informationIcon);
		informationIcon.getAttribute("uib-tooltip");
	}
	
	public void verifyInformationIconMsg() throws Exception{
		log.info(informationIcon);
		new Scroll().scrollDown(ObjectRepo.driver);
		Assert.assertEquals(informationIcon.getAttribute("uib-tooltip"), "You cannot reuse your recent 4 passwords.");
	}
	
	public void clickCurrentPass(String emptyVal) throws Exception{
		log.info(currentPassField);
		currentPassField.sendKeys(emptyVal);
		currentPassField.clear();
	}
	
	public void verifyCurrentPassRequiredMsg() throws Exception{
		log.info(currentPassRequiredMsg);
		Assert.assertEquals(currentPassRequiredMsg.getText(), "Current Password is required.");
	}
	
	public void verifyCurrentPassRequiredMsgColor() throws Exception{
		log.info(currentPassRequiredMsg);
		Assert.assertEquals(helper.FontColor(currentPassRequiredMsg), "#a94442");
	}
	
	public void enterInvalidCurrentPass(String invalidPass) throws Exception{
		log.info(currentPassField);
		currentPassField.sendKeys(invalidPass);
	}
	
	public void clickNewPassField() throws Exception{
		log.info(newPassField);
		newPassField.click();
		newPassField.sendKeys(Keys.TAB);
	}
	
	public void enterPassInNewPassField(String invalidPass) throws Exception{
		log.info(newPassField);
		newPassField.sendKeys(invalidPass);
	}
	
	public void enterPassInConfirmPassField(String invalidPass) throws Exception{
		log.info(confirmPassField);
		confirmPassField.sendKeys(invalidPass);
	}
	
	public void clickConfirmPassField() throws Exception{
		log.info(confirmPassField);
		confirmPassField.click();
	}
	
	public void clickChangePassSaveBtn() throws Exception{
		log.info(saveBtnChangePass);
		saveBtnChangePass.click();
		Thread.sleep(500);
	}
	
	public void verifyInvalidCurrentPassMsg() throws Exception{
		log.info(currentPassInvalid);
		Assert.assertEquals(currentPassInvalid.getText(), "Current Password is incorrect.");
	}
	
	public void verifyInvalidCurrentPassMsgColor() throws Exception{
		log.info(currentPassInvalid);
		Assert.assertEquals(helper.FontColor(currentPassInvalid), "#a94445");
	}
	
	public void verifyNewPassRequiredMsg() throws Exception{
		log.info(newPassRequiredMsg);
		Assert.assertEquals(newPassRequiredMsg.getText(), "New Password is required.");
	}
	
	public void verifyNewPassRequiredMsgColor() throws Exception{
		log.info(newPassRequiredMsg);
		Assert.assertEquals(helper.FontColor(newPassRequiredMsg), "#a94442");
	}
	
	public void enterValidCurrentPass(String validCurrentPass) throws Exception{
		log.info(currentPassField);
		currentPassField.sendKeys(validCurrentPass);
	}
	
	public void enterInvalidConfirmPass(String invalidConfirmPass) throws Exception{
		log.info(confirmPassField);
		confirmPassField.sendKeys(invalidConfirmPass);
		confirmPassField.sendKeys(Keys.TAB);
	}
	
	public void verifyConfirmPassSameMsg() throws Exception{
		log.info(confirmPassMismatch);
		new Scroll().scrollDown(ObjectRepo.driver);
		Assert.assertEquals(confirmPassMismatch.getText(), "Confirm PasswordandNew Passwordmust be exactly same.");
	}
	
	public void verifyConfirmPassSameMsgColor() throws Exception{
		log.info(confirmPassMismatch);
		Assert.assertEquals(helper.FontColor(confirmPassMismatch), "#a94442");
	}
	
	public void enterAlphabetsCharacters(String alphabetsVal) throws Exception{
		log.info(newPassField);
		newPassField.sendKeys(alphabetsVal);
//		newPassField.sendKeys(Keys.TAB);
	}
	
	public void verifyInvalidNewPassMsg() throws Exception{
		log.info(invalidMsg);
		Assert.assertEquals(invalidMsg.getText(), "New Password must have the following properties.\n" + 
				"Must have at least One Special symbol (!, @, #, $, *)\n" + 
				"Must have at least One Capital case letter\n" + 
				"Must have at least One Number\n" + 
				"Must be at least 8 characters long");
	}
	
	public void verifyInvalidNewPassMsgColor() throws Exception{
		log.info(invalidMsg);
		Assert.assertEquals(helper.FontColor(invalidMsg), "#a94442");
	}
	
	public void verifyConfirmPassRequiredMsg() throws Exception{
		log.info(confirmPassRequired);
		confirmPassField.sendKeys(Keys.TAB);
		Assert.assertEquals(confirmPassRequired.getText(), "Confirm Password is required.");
	}
	
	public void verifyConfirmPassRequiredMsgColor() throws Exception{
		log.info(confirmPassRequired);
		confirmPassField.sendKeys(Keys.TAB);
		new Scroll().scrollDown(ObjectRepo.driver);
		Assert.assertEquals(helper.FontColor(confirmPassRequired), "#a94442");
	}
	
	public void enterCurrentPass(String sameAsNew) throws Exception{
		log.info(currentPassField);
		currentPassField.sendKeys(sameAsNew);
	}
	
	public void enterNewPass(String sameAsCurrent) throws Exception{
		log.info(newPassField);
		newPassField.sendKeys(sameAsCurrent);
	}
	
	public void verifyNewCurrentSamePassMsg() throws Exception{
		log.info(newCurrentSamePass);
		Assert.assertEquals(newCurrentSamePass.getText(), "New Password cannot be the same as your Old Password.");
	}
	
	public void verifyNewCurrentSamePassMsgColor() throws Exception{
		log.info(newCurrentSamePass);
		Assert.assertEquals(helper.FontColor(newCurrentSamePass), "#a94442");
	}
	
	public void clickCancelBtnChangePass() throws Exception{
		log.info(cancelBtn);
		cancelBtn.click();
	}
	
	public void verifyCancelBtnFunctionality() throws Exception{
		log.info(cancelBtn);
		Thread.sleep(1000);
		if(driver.findElements(By.xpath("//div[@class='breadcrumb ng-binding']")).size() == 0)
		{
			Assert.assertEquals(true, true);
		}
		else
		{
			Assert.assertEquals(false, true);
		}
	}
	
	// ----------------------------------------- E-Signature PIN -----------------------------------------
	
	public void clickChangeESignPIN() throws Exception{
		Thread.sleep(2500);
		changeESignPINBtn.click();
	}
	
	public void verifyChangeESignPINFun() throws Exception{
		log.info(changeESignPINBtn);
		new Scroll().scrollDown(ObjectRepo.driver);
		if(driver.findElements(By.xpath("//*[@name='editVerificationCodeForm']")).size() != 0)
		{
			Assert.assertEquals(true, true);
		}
		else
		{
			Assert.assertEquals(false, true);
		}
	}
	
	public void verifyFirstLablOfESignPIN() throws Exception{
		new Scroll().scrollDown(ObjectRepo.driver);
		Assert.assertEquals(changeESignPINFirstLabl.getText(), "Current eSignature PIN *(Forgot eSignature PIN?)");
	}
	
	public void verifySecondLablOfESignPIN() throws Exception{
		new Scroll().scrollDown(ObjectRepo.driver);
		Assert.assertEquals(changeESignPINSecondLabl.getText(), "New eSignature PIN *");
	}
	
	public void verifyThirdLablOfESignPIN() throws Exception{
		new Scroll().scrollDown(ObjectRepo.driver);
		Assert.assertEquals(changeESignPINThirdLabl.getText(), "Confirm New eSignature PIN *");
	}
	
	public void clickFirstTextBoxOfESignPIN() throws Exception {
		currentSignTextBox.click();
		Thread.sleep(500);
	}
	
	public void verifyFirstESignPINValMsg() throws Exception{
		currentSignTextBox.sendKeys(Keys.TAB);
		new Scroll().scrollDown(ObjectRepo.driver);
		Assert.assertEquals(currentSignTextBoxValMsg.getText(), "Current eSignature PIN is required.");
	}
	
	public void verifyFirstESignPINValMsgColor() throws Exception{
		currentSignTextBox.sendKeys(Keys.TAB);
		new Scroll().scrollDown(ObjectRepo.driver);
		Assert.assertEquals(helper.FontColor(currentSignTextBoxValMsg), "#a94442");
	}
	
	public void enterCurrentESignPIN(String signature) throws Exception{
		currentSignTextBox.sendKeys(signature);
	}
	
	public void clickNewSignTextBox() throws Exception {
		newSignTextBox.click();
		Thread.sleep(500);
	}
	
	public void enterNewSignPIN(String signature) throws Exception{
		newSignTextBox.sendKeys(signature);
	}
	
	public void clickConfirmSignTextBox() throws Exception {
		confirmSignTextBox.click();
		Thread.sleep(500);
	}
	
	public void enterConfirmSignPIN(String signature) throws Exception{
		confirmSignTextBox.sendKeys(signature);
	}
	
	public void clickSaveBtnOfESign() throws Exception {
		saveBtnOfESign.click();
		Thread.sleep(500);
	}
	
	public void verifyCurrentESignInValData() throws Exception{
		new Scroll().scrollDown(ObjectRepo.driver);
		Assert.assertEquals(currentSignInValSignMsg.getText(), "Current eSignature PIN is incorrect.");
	}
	
	public void verifyCurrentESignInValDataColor() throws Exception{
		new Scroll().scrollDown(ObjectRepo.driver);
		Assert.assertEquals(helper.FontColor(currentSignInValSignMsg), "#a94442");
	}
	
	public void verifyNewESignPINValMsg() throws Exception{
		newSignTextBox.sendKeys(Keys.TAB);
		new Scroll().scrollDown(ObjectRepo.driver);
		Assert.assertEquals(newSignValMsg.getText(), "New eSignature PIN is required.");
	}
	
	public void verifyNewESignPINValMsgColor() throws Exception{
		newSignTextBox.sendKeys(Keys.TAB);
		new Scroll().scrollDown(ObjectRepo.driver);
		Assert.assertEquals(helper.FontColor(newSignValMsg), "#a94442");
	}
	
	public void verifyConfirmESignPINValMsg() throws Exception{
		confirmSignTextBox.sendKeys(Keys.TAB);
		new Scroll().scrollDown(ObjectRepo.driver);
		Assert.assertEquals(confirmValMsg.getText(), "Confirm New eSignature PIN is required.");
	}
	
	public void verifyConfirmESignPINValMsgColor() throws Exception{
		confirmSignTextBox.sendKeys(Keys.TAB);
		new Scroll().scrollDown(ObjectRepo.driver);
		Assert.assertEquals(helper.FontColor(confirmValMsg), "#a94442");
	}
	
	public void verifyNewSignValMsgForDup() throws Exception{
		new Scroll().scrollDown(ObjectRepo.driver);
		Assert.assertEquals(newSignValMsgForDup.getText(), "Current eSignature PIN and New eSignature PIN cannot be same.");
	}
	
	public void verifyNewSignValMsgForDupColor() throws Exception{
		new Scroll().scrollDown(ObjectRepo.driver);
		Assert.assertEquals(helper.FontColor(newSignValMsgForDup), "#a94442");
	}
	
	public void verifyConfirmSignValMsgForMisMatch() throws Exception{
		new Scroll().scrollDown(ObjectRepo.driver);
		Assert.assertEquals(confirmSignValMsgForMisMatch.getText(), "eSignature PIN mismatch.");
	}
	
	public void verifyConfirmSignValMsgForMisMatchColor() throws Exception{
		new Scroll().scrollDown(ObjectRepo.driver);
		Assert.assertEquals(helper.FontColor(confirmSignValMsgForMisMatch), "#a94442");
	}
	
	public void verifyNewSignValMsgForMin() throws Exception{
		new Scroll().scrollDown(ObjectRepo.driver);
		Assert.assertEquals(newSignValMsgForMin.getText(), "New eSignature PIN must be at least 2 characters.");
	}
	
	public void verifyNewSignValMsgForMinColor() throws Exception{
		new Scroll().scrollDown(ObjectRepo.driver);
		Assert.assertEquals(helper.FontColor(newSignValMsgForMin), "#a94442");
	}
	
	public void verifyNewSignValMsgForMax() throws Exception{
		new Scroll().scrollDown(ObjectRepo.driver);
		Assert.assertEquals(newSignValMsgForMax.getText(), "New eSignature PIN cannot be more than 50 characters.");
	}
	
	public void verifyNewSignValMsgForMaxColor() throws Exception{
		new Scroll().scrollDown(ObjectRepo.driver);
		Assert.assertEquals(helper.FontColor(newSignValMsgForMax), "#a94442");
	}
	
	public void verifyConfirmSignValMsgForMin() throws Exception{
		new Scroll().scrollDown(ObjectRepo.driver);
		Assert.assertEquals(confirmSignValMsgForMin.getText(), "Confirm New eSignature PIN must be at least 2 characters.");
	}
	
	public void verifyConfirmSignValMsgForMinColor() throws Exception{
		new Scroll().scrollDown(ObjectRepo.driver);
		Assert.assertEquals(helper.FontColor(confirmSignValMsgForMin), "#a94442");
	}
	
	public void verifyConfirmSignValMsgForMax() throws Exception{
		new Scroll().scrollDown(ObjectRepo.driver);
		Assert.assertEquals(confirmSignValMsgForMax.getText(), "Confirm New eSignature PIN cannot be more than 50 characters.");
	}
	
	public void verifyConfirmSignValMsgForMaxColor() throws Exception{
		new Scroll().scrollDown(ObjectRepo.driver);
		Assert.assertEquals(helper.FontColor(confirmSignValMsgForMax), "#a94442");
	}
	
	public void verifyGeneralError() throws Exception{
		new Scroll().scrollDown(ObjectRepo.driver);
		Assert.assertEquals(generalErr.getText(), "You must complete all required fields marked with (*).");
	}
	
	public void verifyGeneralErrorColor() throws Exception{
		new Scroll().scrollDown(ObjectRepo.driver);
		Assert.assertEquals(helper.FontColor(generalErr), "#a94442");
	}
	
	public void clickCancelBtnChangeSign() throws Exception{
		log.info(cancelSignBtn);
		cancelSignBtn.click();
	}
	
}
