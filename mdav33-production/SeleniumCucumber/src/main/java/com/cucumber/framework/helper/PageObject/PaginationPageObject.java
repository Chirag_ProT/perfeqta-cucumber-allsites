package com.cucumber.framework.helper.PageObject;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import com.cucumber.framework.helper.Logger.LoggerHelper;
import com.cucumber.framework.helper.Pagination.Pagination;

public class PaginationPageObject extends PageBase
{
	private final Logger log = LoggerHelper.getLogger(SetsPageObject.class);
	private Pagination paginationObj = new Pagination();
	private WebDriver driver;

	public PaginationPageObject(WebDriver driver)
	{
		super(driver);
		this.driver = driver;
	}

	@FindBy(how = How.LINK_TEXT, using = "View Audit Trail")
	public WebElement ViewAuditTrail_Link;

	@FindBy(how = How.XPATH, using = "//a[@ng-click='selectPage(totalPages, $event)']")
	public WebElement clickLastBtn;

	@FindBy(how = How.XPATH, using = "//a[@ng-click='selectPage(1, $event)']")
	public WebElement clickFirstBtn;

	@FindBy(how = How.XPATH, using = ".//grid/div[3]/div[2]/ul/li[3]/a")
	public WebElement verifyFirstPage;

	@FindBy(how = How.XPATH, using = "//a[@ng-click='selectPage(page + 1, $event)']")
	public WebElement clickNextPage;

	@FindBy(how = How.XPATH, using = "//*[contains(normalize-space(text()),'Showing')]")
	public WebElement verifyNextData;

	@FindBy(how = How.XPATH, using = "//a[@ng-click='selectPage(page - 1, $event)']")
	public WebElement clickPreviousPage;

	@FindBy(how = How.XPATH, using = "//*[contains(normalize-space(text()),'Showing')]")
	public WebElement verifyPreviousData;

	@FindBy(how = How.XPATH, using = "//li[@class='pagination-last ng-scope disabled']")
	public WebElement verifyLastDisabled;

	@FindBy(how = How.XPATH, using = "//*[contains(normalize-space(text()),'Showing')]")
	public WebElement showingText;

	@FindBy(how = How.XPATH, using = "//select[@id='ddlPageSize']")
	public WebElement pageSizeDrpDwn;

	public void clickToViewAuditTrailLink() 
	{
		try
		{
			ViewAuditTrail_Link.click();
			Thread.sleep(2000);
		}
		catch(Exception e)
		{
			System.out.println(e.toString());
		}
	}
	public void clickLastBtnPagination() throws Exception 
	{
		log.info(clickLastBtn);
		
		if (driver.findElements(By.xpath("//a[@ng-click='selectPage(1, $event)']")).size() != 0) {
			clickLastBtn.click();

				
				} else {
					System.out.println("Less than 10 records found");
					Assert.assertEquals(true, true);
				}
	}
	public void clickFirstBtnPagination() throws Exception
	{
		log.info(clickFirstBtn);
if (driver.findElements(By.xpath("//a[@ng-click='selectPage(1, $event)']")).size() != 0) {
	clickFirstBtn.click();

		
		} else {
			System.out.println("Less than 10 records found");
			Assert.assertEquals(true, true);
		}
	}

	public void verifyFirstPagePagination() {
		log.info(verifyFirstPage);
		
		if (driver.findElements(By.xpath("//a[@ng-click='selectPage(1, $event)']")).size() != 0) {
			
			System.out.println(verifyFirstPage.getText());
		
			Assert.assertEquals(verifyFirstPage.getText(), "1");
		
		} else {
			System.out.println("Less than 10 records found");
			Assert.assertEquals(true, true);
		}

	}

	public void clickNextBtnPagination() throws Exception {
		log.info(clickNextPage);



			if (driver.findElements(By.xpath("//a[@ng-click='selectPage(1, $event)']")).size() != 0) {
				
				clickNextPage.click();

			
			} else {
				System.out.println("Less than 10 records found");
				Assert.assertEquals(true, true);
			}
	}
	public void verifyNextBtnPagination() throws Exception
	{
		log.info(verifyNextData);
	
		
		if (driver.findElements(By.xpath("//a[@ng-click='selectPage(1, $event)']")).size() != 0) {

			Thread.sleep(1000);
			String output = verifyNextData.getText();
			boolean actual = output.startsWith("11", 8);
			Assert.assertEquals(actual, true);
			
		} else {
			System.out.println("Less than 10 records found");
			Assert.assertEquals(true, true);
		}
	}
	public void clickPreviousBtnPagination() throws Exception
	{
		log.info(clickPreviousPage);
		
		
		if (driver.findElements(By.xpath("//a[@ng-click='selectPage(1, $event)']")).size() != 0) {

			clickNextPage.click();
			Thread.sleep(1000);
			clickPreviousPage.click();
			
		} else {
			System.out.println("Less than 10 records found");
			Assert.assertEquals(true, true);
		}
	}
	public void verifyPreviousBtnPagination() throws Exception 
	{
		log.info(clickPreviousPage);
		
		
		if (driver.findElements(By.xpath("//a[@ng-click='selectPage(1, $event)']")).size() != 0) {

			Thread.sleep(1000);
			String output = verifyPreviousData.getText();
			boolean actual = output.startsWith("1", 8);
			Assert.assertEquals(actual, true);
			
		} else {
			System.out.println("Less than 10 records found");
			Assert.assertEquals(true, true);
		}
	}
	public void verifyLastBtnPagination() 
	{
		log.info(verifyLastDisabled);
		if (driver.findElements(By.xpath("//li[@class='pagination-last ng-scope disabled']")).size() != 0) 
		{
			Assert.assertEquals(true, true);
		} 
		else
		{
			Assert.assertEquals(true, true);
		}
	}
	public void verifyCountOfListingScreen() throws Exception 
	{
		log.info("verify count of listing screen");
		String paginationText = showingText.getText();
		Select select = new Select(pageSizeDrpDwn);
		WebElement option = select.getFirstSelectedOption();
		String pageSizeDrpDwnVal = option.getText();
		WebElement Grid = driver.findElement(By.xpath("//*[@ng-if='vm.options.data && vm.options.data.length']"));
		paginationObj.PaginationVerification(paginationText, pageSizeDrpDwnVal, Grid);
	}
}
