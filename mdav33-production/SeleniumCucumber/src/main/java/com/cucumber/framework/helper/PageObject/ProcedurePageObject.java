package com.cucumber.framework.helper.PageObject;

import java.awt.RenderingHints.Key;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.By.ByXPath;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.server.browserlaunchers.Sleeper;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import com.cucumber.framework.helper.Helper;
import com.cucumber.framework.helper.Button.ButtonHelper;
import com.cucumber.framework.helper.Logger.LoggerHelper;
import com.cucumber.framework.helper.Pagination.Pagination;
import com.cucumber.framework.helper.Scroll.Scroll;
import com.cucumber.framework.helper.Search.Search;
import com.cucumber.framework.settings.ObjectRepo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.As;

public class ProcedurePageObject extends PageBase {
	private WebDriver driver;
	private Helper helper = new Helper();
	public ButtonHelper btnHelper;
	private final Logger log = LoggerHelper.getLogger(SitesPageObject.class);
	private Search sObj;

	public ProcedurePageObject(WebDriver driver) {
		super(driver);
		this.driver = driver;
		btnHelper = new ButtonHelper(driver);
		sObj = new Search();
	}

	@FindBy(how = How.XPATH, using = "//*[@ui-sref='secure.admin.home']")
	public WebElement administrationTile;
	@FindBy(how = How.XPATH, using = "//IMG[@src='./assets/images/procedures.png']")
	public WebElement ProcedureTile;
	@FindBy(how = How.XPATH, using = "//*[@id='breadcrum-ipad-768']/li[2]/a")
	public WebElement previousTileclick;
	@FindBy(how = How.XPATH, using = "//*[@id='breadcrum-ipad-768']/li[2]/span")
	public WebElement previousTile;
	@FindBy(how = How.XPATH, using = "//tr[1]/td[1]/span[1]/span/a")
	public WebElement firstRecord;
	@FindBy(how = How.XPATH, using = "//*[@id='breadcrum-ipad-768']/li[4]/span")
	public WebElement addEditProcedure;
	@FindBy(how = How.XPATH, using = "//*[@id='breadcrum-ipad-768']")
	public WebElement addEditProcedurebreadcrumb;
	@FindBy(how = How.XPATH, using = "//button[@ng-click='vm.save(vm.procedure,procedureForm)']")
	public WebElement saveBtn;
	@FindBy(how = How.XPATH, using = "//*[@id='breadcrum-ipad-768']/li[3]/span")
	public WebElement procedureBreadcrumb;
	@FindBy(how = How.XPATH, using = "//a[@ui-sref='secure.admin.procedures.list']")
	public WebElement cancelBtn;
	@FindBy(how = How.XPATH, using = "//a[@ui-sref='secure.admin.procedures.edit.procedure']")
	public WebElement addnewBtn;
	@FindBy(how = How.XPATH, using = "//input[@ng-model='vm.procedure.title']")
	public WebElement procedurename;
	@FindBy(how = How.XPATH, using = "//p[@translate='secure.admin.procedure.edit.procedure.PROCEDURENAMEISREQUIRED']")
	public WebElement procedurenameRequired;
	@FindBy(how = How.XPATH, using = "//p[@translate='secure.admin.procedure.edit.procedure.PROCEDURETAGSHOULDBEATLEAST2CHARACTERSLONG']")
	public WebElement procedurenameMinvalidation;
	@FindBy(how = How.XPATH, using = "//p[@translate='secure.admin.procedure.edit.procedure.PROCEDURENAMESHOULDBEATLEAST200CHARACTERSLONG']")
	public WebElement procedurenameMaxvalidation;
	@FindBy(how = How.XPATH, using = "//p[@translate='secure.admin.procedure.edit.procedure.PROCEDURENAMEMUSTBEUNIQUE']")
	public WebElement procedurenameUniquevalidation;
	@FindBy(how = How.XPATH, using = "//tr[1]/td[1]/span[1]/span/a")
	public WebElement firstProcedureRecord;
	@FindBy(how = How.XPATH, using = "//button[@ng-click='vm.save(vm.procedure,procedureForm)']")
	public WebElement editrecordSaveBtn;
	@FindBy(how = How.XPATH, using = "//tr[1]/td[6]/span[1]/span/a/img")
	public WebElement CopyIcon;
	@FindBy(how = How.XPATH, using = "//button[@class='confirm']")
	public WebElement OkBtnofCopyIcon;
	@FindBy(how = How.XPATH, using = "//fieldset/input")
	public WebElement procedurenameincopypopup;
	@FindBy(how = How.XPATH, using = "//input[@name='proceduretag']")
	public WebElement proceduretagInputbox;
	@FindBy(how = How.XPATH, using = "//p[@translate='secure.admin.procedure.edit.procedure.PROCEDURETAGISREQUIRED']")
	public WebElement proceduretagRequired;
	@FindBy(how = How.XPATH, using = "//p[@translate='secure.admin.procedure.edit.procedure.PROCEDURETAGSHOULDBEATLEAST2CHARACTERSLONG']")
	public WebElement proceduretagMinValidation;
	@FindBy(how = How.XPATH, using = "//p[@translate='secure.admin.procedure.edit.procedure.PROCEDURETAGSHOULDNOTBEMORETHAN20CHARACTERSLONG']")
	public WebElement proceduretagMaxValidation;
	@FindBy(how = How.XPATH, using = "//input[@name='isInstructionForUser']")
	public WebElement instructionForUsercheckbox;
	@FindBy(how = How.XPATH, using = "//textarea[@ng-change='vm.onChangeInstructionForUserTextbox()']")
	public WebElement instructionForUserInput;
	@FindBy(how = How.XPATH, using = "//p[@translate='secure.admin.procedure.edit.procedure.INSTRUCTIONFORUSERISREQUIRED']")
	public WebElement instructionForUserRequired;
	@FindBy(how = How.XPATH, using = "//p[@translate='secure.admin.procedure.edit.procedure.INSTRUCTIONFORUSERSHOULDBEATLEAST2CHARACTERSLONG']")
	public WebElement instructionForUserMinValidation;
	@FindBy(how = How.XPATH, using = "//p[@translate='secure.admin.procedure.edit.procedure.INSTRUCTIONFORUSERSHOULDNOTBEMORETHAN800CHARACTERSLONG']")
	public WebElement instructionForUserMaxValidation;
	@FindBy(how = How.XPATH, using = "//input[@name='instructionForUserRadioButtonURL']")
	public WebElement UrlCheckbox;
	@FindBy(how = How.XPATH, using = "//input[@name='urlTextBox']")
	public WebElement UrlInput;
	@FindBy(how = How.XPATH, using = "//p[@translate='secure.admin.procedure.edit.procedure.URLISREQUIRED']")
	public WebElement UrlInputrequired;
	@FindBy(how = How.XPATH, using = "//p[@translate='secure.admin.procedure.edit.procedure.NOTVALIDURL2']")
	public WebElement invalidUrlInput;
	@FindBy(how = How.XPATH, using = "//input[@id='txtSearch']")
	public WebElement searchBox;
	
	@FindBy(how = How.XPATH, using = "(//*[@name='questionSelection'])[2]")
	public WebElement selectQuestion;
	
	@FindBy(how = How.XPATH, using = "//*[@ng-click='vm.addProceduerTags(vm.tag)']")
	public WebElement clickOnAddTag;
	
	@FindBy(how = How.XPATH, using = "//*[@ng-click='vm.addQuestion(vm.selectedQuestion)']")
	public WebElement clickOnAddQue;
	
	@FindBy(how = How.XPATH, using = "(//*[@ng-bind-html='match.label | uibTypeaheadHighlight:query'])[1]")
	public WebElement clickQue;
	
	@FindBy(how = How.XPATH, using = "(//*[@ng-click='vm.go(field,row,vm.resource);'])[1]")
	public WebElement firstProc;
	
	
	
	public void clickOnProcedureTile() {
		ProcedureTile.click();
	}
	public void clickonBreadcrumb() {
		previousTileclick.click();
	}
	public void verifyPageRedirection() {
		Assert.assertEquals(previousTile.getText(), "Administration");
	}
	public void clickonFirstProcedure() {
		firstRecord.click();
	}
	public void verifyRecordEditscreen() {
		Assert.assertEquals(addEditProcedure.getText(), "Add / Edit Procedure");
	}
	public void verifyWholebreadcrumbEditRecordScreen() throws Exception {
		Thread.sleep(2000);
		Assert.assertEquals(addEditProcedurebreadcrumb.getText(), "HomeAdministrationProceduresAdd / Edit Procedure");
	}
	public void clickOnSaveBtn() throws Exception {
		saveBtn.click();
		Thread.sleep(2000);
	}
	public void verifyProcedurelistingScreen() {
		Assert.assertEquals(procedureBreadcrumb.getText(), "Procedures");
	}
	public void clickOnCancelBtn() {
		cancelBtn.click();
	}
	public void verifycancelBtnFunctionality() {
		Assert.assertEquals(procedureBreadcrumb.getText(), "Procedures");
	}
	public void verifySaveBtnColor() {
		new Scroll().scrollDown(ObjectRepo.driver);
		Assert.assertEquals(helper.BtnColor(saveBtn), "#5cb85c");
	}
	public void verifyCancelBtnColor() {
		new Scroll().scrollDown(ObjectRepo.driver);
		Assert.assertEquals(helper.BtnColor(cancelBtn), "#5a5a5a");
	}
	public void verifybreadcrumbiAudittrail() {
		Assert.assertEquals(addEditProcedurebreadcrumb.getText(), "HomeAdministrationProceduresAudit Trail");
	}
	public void verifybackBtnofviewAuditredirection() {
		Assert.assertEquals(procedureBreadcrumb.getText(), "Procedures");
	}
	public void clickonAddnewBtn() {
		addnewBtn.click();
	}
	public void clickonProcedurenameInput() {
		procedurename.sendKeys(Keys.TAB);
	}
	public void verifyreuiredValidation() {
		Assert.assertEquals(procedurenameRequired.getText(), "Procedure Name is required.");
	}
	public void enterLessThan2inProcedurename(String value) {
		procedurename.sendKeys(value);

	}
	public void verifyMinValidationProcedureName() {
		Assert.assertEquals(procedurenameMinvalidation.getText(), "Procedure Tag must be at least 2 characters.");
	}
	public void enterMorethan200CharProcedureName(String value) {
		procedurename.sendKeys(value);
	}
	public void verifyMaxValidatioProcedureName() {
		
		Assert.assertEquals(procedurenameMaxvalidation.getText(), "Procedure Name cannot be more than 200 characters.");	
	}
	public void entervalidDataProcedurename(String value) {
		procedurename.sendKeys(value);
	}
	public void verifyValiddataProcedurename() {
		String actual = procedurename.getText();
		Assert.assertEquals(actual, "");
	}
	public void enterDuplicateDataProcedurename(String value) {
		procedurename.sendKeys(value);
	}
	public void verifyUniqueValidationProcedurename() {
		Assert.assertEquals(procedurenameUniquevalidation.getText(), "Procedure Name must be unique.");
	}
	public void clickonFirstprocedureRecord() {
		firstProcedureRecord.click();
	}
	public void clickonProcedurenameEdit() {
		procedurename.click();
	}
	public void editProcedurename(String value) {
		procedurename.clear();
		procedurename.sendKeys(value);

	}
	public void clickOnSaveBtnProcedurerecord() {
		editrecordSaveBtn.click();
	}
	public void verifyEditedModule(String value) {
		Assert.assertEquals(firstProcedureRecord.getText(),value);
	}
	public void clickonCopyIcon() {
		CopyIcon.click();
	}
	public void verifypopupwithProcedurename() throws Exception {
		if(driver.findElements(By.xpath("//fieldset/input")).size() != 0) {
			Thread.sleep(1000);
		Assert.assertEquals(true, true);
		OkBtnofCopyIcon.click();
		}
		else {
			Thread.sleep(1000);
			Assert.assertEquals(false, true);
		}
	}
	public void verifyPostfixofprocedurecopiedIcon() {
		
		String s = procedurenameincopypopup.getText();
		procedurenameincopypopup.clear();
		String[] cicon = s.split("Copy");
		String p = cicon[1];
	}
	public void clickonOkofCopyIcon() {
		OkBtnofCopyIcon.click();
	}
	 
	public void clickonProcedureTag() {
		proceduretagInputbox.click();
		proceduretagInputbox.sendKeys(Keys.TAB);
	}
	public void verifyProcedureTagRequiredValidation() {
		Assert.assertEquals(proceduretagRequired.getText(),  "Procedure Tag is required.");
	}
	public void enterlessthan2ProcedureTag(String value) {
		proceduretagInputbox.sendKeys(value);
	}
	public void verifyMinValidationProceduretag() {
		Assert.assertEquals(proceduretagMinValidation.getText(), "Procedure Tag must be at least 2 characters.");
	}
	public void enterMorethan200CharProcedureTag(String value) {
		proceduretagInputbox.sendKeys(value);
	}
	public void verifyMaxValidatioProcedureTag() {
		Assert.assertEquals(proceduretagMaxValidation.getText(), "Procedure Tag cannot be more than 20 characters.");
	}
	public void entervalidDataProcedureTag(String value) {
		proceduretagInputbox.sendKeys(value);
	}
	public void verifyvaliddataProcedureTag() {
		String actual = proceduretagInputbox.getText();
		Assert.assertEquals(actual, "");
	}
	public void verifyinstructionCheckbox() {
		if(instructionForUsercheckbox.isSelected()) {
			Assert.assertEquals(true, true);
		}else {
			Assert.assertEquals(false, true);
		}
	}
	public void clickonInstructionInput() {
		instructionForUserInput.click();
	}
	public void pressTabInstruction() {
		instructionForUserInput.sendKeys(Keys.TAB);
	}
	public void verifyInstructionRequired() {
		Assert.assertEquals(instructionForUserRequired.getText(), "User Instructions are required.");
	}
	public void enterLessThan2inInstruction(String value) {
		instructionForUserInput.sendKeys(value);
	}
	public void minValidationInstruction() {
		
		Assert.assertEquals(instructionForUserMinValidation.getText(), "Instructions for user must be at least 2 characters.");
	}
	public void enterMorethan200CharInstruction(String value) {
		instructionForUserInput.sendKeys(value);
	}
	public void verifyMaxValidatioInstruction() {
		
		Assert.assertEquals(instructionForUserMaxValidation.getText(), "Instructions for user cannot be more than 800 characters.");
	}
	public void entervalidDataInstruction(String value) {
		instructionForUserInput.sendKeys(value);
	}
	public void verifyValiddataInstruction() {
		String actual = instructionForUserInput.getText();
		Assert.assertEquals(actual, "");
	}
	public void clickonUrlChecbox() {
		UrlCheckbox.click();
	}
	public void verifyUlrcheckboxClickable() {
		if(UrlCheckbox.isSelected()) {
			Assert.assertEquals(true, true);
		}else {
			Assert.assertEquals(false, true);
		}
	}
	public void pressTabUrl() {
		UrlInput.sendKeys(Keys.TAB);
	}
	public void verifyRequiredMsgUrl() {
		Assert.assertEquals(UrlInputrequired.getText(), "URL is required.");
	}
	public void enterinvalidUrl(String value) {
		UrlInput.sendKeys(value);
	}
	public void verifyInvalidUrl() {
		Assert.assertEquals(invalidUrlInput.getText(), "Not a valid URL, don't forget to use http:// or https://");
	}
	public void enterValidURLdata(String value) {
		UrlInput.sendKeys(value);
		
	}
	public void verifyValidUrl() {
		String actual = UrlInput.getText();
		Assert.assertEquals(actual, "");
	}
	public void enterprocedurenameSearch() {
		searchBox.click();
	}
	
	public void enterSearchAndSelectQuestion(String value) throws Exception {
		selectQuestion.sendKeys(value);
		Thread.sleep(1000);
		clickQue.click();
	}
	
	public void clickOnAddTag() throws Exception {
		clickOnAddTag.click();
		Thread.sleep(1000);
	}
	
	public void clickOnAddQue() throws Exception {
		clickOnAddQue.click();
	}
	
	public void verifyAddedProcedure(String value) {
		String actual = firstProc.getText();
		Assert.assertEquals(actual, value);
	}
	
}