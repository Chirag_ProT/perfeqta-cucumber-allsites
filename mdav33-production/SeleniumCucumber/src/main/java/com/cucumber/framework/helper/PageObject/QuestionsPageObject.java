package com.cucumber.framework.helper.PageObject;

import static org.testng.Assert.assertTrue;

import org.apache.log4j.Logger;
import org.aspectj.apache.bcel.generic.TABLESWITCH;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import com.cucumber.framework.helper.Helper;
import com.cucumber.framework.helper.Button.ButtonHelper;
import com.cucumber.framework.helper.Copy.Copy;
import com.cucumber.framework.helper.Logger.LoggerHelper;
import com.cucumber.framework.helper.Scroll.Scroll;
import com.cucumber.framework.helper.Search.Search;
import com.cucumber.framework.helper.Sorting.SortingOnColumn;
import com.cucumber.framework.settings.ObjectRepo;
import com.cucumber.framework.utility.Constant;
import com.cucumber.framework.utility.ExcelUtils;

public class QuestionsPageObject extends PageBase
{
	private WebDriver driver;
	public ButtonHelper btnHelper;
	private final Logger log = LoggerHelper.getLogger(QuestionsPageObject.class);
	private SortingOnColumn sortColumn;
	private Search search;
	private Helper helper = new Helper();
	private Scroll scroll;
	private Copy copy;
	public ExcelUtils excel;

	public QuestionsPageObject(WebDriver driver)

	{
		super(driver);
		this.driver = driver;
		btnHelper = new ButtonHelper(driver);
		sortColumn = new SortingOnColumn();
		search = new Search();
		scroll = new Scroll();
	}

	public String excelData(int rowVal, int colVal) throws Exception {
		excel = new ExcelUtils();
		excel.setExcelFile(Constant.Path_TestData + Constant.File_TestData, "Questions");
		System.err.println();
		return excel.readXLSFile("Questions", rowVal, colVal);
	}


	@FindBy(how = How.XPATH, using = "//*[@ui-sref='secure.admin.questions.list']")
	public WebElement questionsTile;

	@FindBy(how = How.XPATH, using = "//*[@class='currentStep ng-binding']")
	public WebElement questionsModuleName;

	@FindBy(how = How.NAME, using = "searchBy")
	public WebElement questionsDropFilterBy;

	@FindBy(how = How.XPATH, using = "//*[contains(normalize-space(text()),'Showing')]")
	public WebElement paginationText;

	@FindBy(how = How.ID, using = "//*[@ng-change='vm.onPageNumberChange()']")
	public WebElement pageSizeDrp;

	@FindBy(how = How.XPATH, using = "//*[@ng-if='vm.options.data && vm.options.data.length']")
	public WebElement grid_data;

	@FindBy(how = How.XPATH, using = "//*[@class='paddingLeft-15px ng-binding']")
	public WebElement srcResultPageNo;

	@FindBy(how = How.ID, using = "txtSearch")
	public WebElement textSearch;

	@FindBy(how = How.XPATH, using = "//*[@class='crumb breadcrumb gray ng-isolate-scope']")
	public WebElement editQuestionBreadCrumbs;

	@FindBy(how = How.XPATH, using = "//*[@ng-click='vm.loadsave?return:vm.save(vm.question, editQuestionForm)']")
	public WebElement editQuestionSaveButton;

	@FindBy(how = How.XPATH, using = "//*[@class='btn btn-danger11 ng-binding ng-scope']")
	public WebElement editQuestionCancelButton;

	@FindBy(how = How.XPATH, using = "//*[@ui-sref='secure.admin.questions.edit']")
	public WebElement questionsAddNewBtn;

	@FindBy(how = How.NAME, using = "questiontitle")
	public WebElement questionsNameTitle;

	@FindBy(how = How.XPATH, using = "//*[@translate='secure.admin.question.edit.QUESTIONNAMEISREQUIRED']")
	public WebElement questionsRequiredVal;

	@FindBy(how = How.XPATH, using = "//*[@translate='secure.admin.question.edit.QUESTIONNAMESHOULDBEATLEAST2CHARACTERSLONG']")
	public WebElement questionsMinimumVal;

	@FindBy(how = How.XPATH, using = "//*[@translate='secure.admin.question.edit.QUESTIONNAMESHOULDNOTBEMORETHAN200CHARACTERSLONG']")
	public WebElement questionsMaximumVal;

	@FindBy(how = How.XPATH, using = "//*[@translate='secure.admin.question.edit.QUESTIONNAMEMUSTBEUNIQUE']")
	public WebElement questionsUniqueVal;

	@FindBy(how = How.NAME, using = "questiontag")
	public WebElement questionsTag;

	@FindBy(how = How.XPATH, using = "//*[@translate='secure.admin.question.edit.QUESTIONRAGISREQUIRED']")
	public WebElement questionsTagRequiredVal;

	@FindBy(how = How.XPATH, using = "//*[text()='Question Details']")
	public WebElement questionsDetails;

	@FindBy(how = How.XPATH, using = "//*[@translate='secure.admin.question.edit.QUESTIONTAGSHOULDBEATLEAST2CHARACTERSLONG']")
	public WebElement questionsTagMinimumVal;

	@FindBy(how = How.XPATH, using = "//*[@translate='secure.admin.question.edit.QUESTIONTAGSHOULDNOTBEMORETHAN20CHARACTERSLONG']")
	public WebElement questionsTagMaximumVal;

	@FindBy(how = How.XPATH, using = "//*[@ng-if='vm.questiontag']")
	public WebElement questionsAddTag;

	@FindBy(how = How.XPATH, using = "//*[@ng-bind-html='vm.message1']")
	public WebElement questionsTagUniqueVal;

	@FindBy(how = How.XPATH, using = "//*[@name='isInstructionForUser']")
	public WebElement questionsInstForUserCheck;

	@FindBy(how = How.XPATH, using = "//*[@name='instructionForUser']")
	public WebElement questionsInstForUserTextbox;

	@FindBy(how = How.XPATH, using = "//*[@translate='secure.admin.procedure.edit.procedure.INSTRUCTIONFORUSERISREQUIRED']")
	public WebElement questionsInstForUserRequiredVal;

	@FindBy(how = How.XPATH, using = "//*[@translate='secure.admin.procedure.edit.procedure.INSTRUCTIONFORUSERSHOULDBEATLEAST2CHARACTERSLONG']")
	public WebElement questionsInstForUserMinimumVal;

	@FindBy(how = How.XPATH, using = "//*[@translate='secure.admin.procedure.edit.procedure.INSTRUCTIONFORUSERSHOULDNOTBEMORETHAN800CHARACTERSLONG']")
	public WebElement questionsInstForUserMaximumVal;

	@FindBy(how = How.XPATH, using = "//*[@name='typeSelect']")
	public WebElement questionsType;

	@FindBy(how = How.XPATH, using = "//*[@ng-click='vm.loadsave?return:vm.save(vm.question, editQuestionForm)']")
	public WebElement questionsSaveButton;

	@FindBy(how = How.XPATH, using = "//span[@class='currentStep ng-binding']")
	public WebElement verifyAuditTrail;

	@FindBy(how = How.XPATH, using = "//*[@class='btn btn-danger11 sets-logspage-left-mar ng-binding']")
	public WebElement auditTrailBackButton;

	@FindBy(how = How.XPATH, using = "(//*[@ng-click='vm.go(field,row,vm.resource);'])[1]")
	public WebElement firstQuestion;

	@FindBy(how = How.XPATH, using = "//*[@ng-change='vm.changeExistingSets()']")
	public WebElement radioBtnOfRegAndCondition;

	@FindBy(how = How.XPATH, using = "//*[@name='conditionCheckbox']")
	public WebElement conditionRadioBtn;

	@FindBy(how = How.XPATH, using = "//*[@name='condition']")
	public WebElement conditionType;
	
	@FindBy(how = How.XPATH, using = "//*[@ng-model='vm.question.validation.condition.value']")
	public WebElement conditionLength;
	
	@FindBy(how = How.XPATH, using = "//*[@ng-model='vm.question.validation.condition.validationMessage']")
	public WebElement conditionValMsg;



	public void clickOnQuestionsTile()
	{
		try
		{
			Thread.sleep(3000);
			questionsTile.click();
		}
		catch(Exception e)
		{
			e.toString();
		}

	}

	public void verifyQuestionModuleName() throws Exception
	{
		String val = questionsModuleName.getText();
		Assert.assertEquals(val, "Questions");
	}
	public void verifyQuestionsDrpFilter() throws Exception
	{
		boolean bool = questionsDropFilterBy.isEnabled();
		assertTrue(bool);
	}

	public void verifyQuestionsDrpClickable() throws Exception
	{
		boolean bool = questionsDropFilterBy.isEnabled();
		Assert.assertEquals(bool, true);
	}

	public void selectQuestionTypeOption() throws Exception
	{
		helper.SelectDrpDwnValueByText(questionsDropFilterBy, "Question Type");
	}

	public void verifyQuestionsTypeDrpFilter() throws Exception
	{
		boolean bool = questionsDropFilterBy.isEnabled();
		assertTrue(bool);
	}

	public void verifyQuestionsTypeClickable() throws Exception
	{
		boolean bool = questionsDropFilterBy.isEnabled();
		Assert.assertEquals(bool, true);
	}

	public String pageSizeDrp_GetSelectedValue() {
		String defaultItem;
		try 
		{
			if (driver.findElements(By.xpath("//*[@ng-change='vm.onPageNumberChange()']")).size() != 0) 
			{
				Select select = new Select(pageSizeDrp);
				WebElement option = select.getFirstSelectedOption();
				defaultItem = option.getText();
				return defaultItem;
			}
			else 
			{
				defaultItem = "1";
				return defaultItem;
			}
		}
		catch(Exception e)
		{
			defaultItem = "1";
		}
		return defaultItem;
	}

	public void verifyAscendingOrder() {
		System.out.println("click on ascending");
	}

	public void verifyAscendingOrderResult(String sortingColName) {
		try 
		{
			String PaginationVal = paginationText.getText();
			String PaginationSelectedVal = pageSizeDrp_GetSelectedValue();
			sortColumn.verifySorting(sortingColName, 1, PaginationVal, PaginationSelectedVal, grid_data);
		}
		catch (Exception e)
		{
			System.out.println(e);
		}
	}

	public void verifysearch(String searchedItem) throws Exception {

		QuestionsPageObject obj = new QuestionsPageObject(ObjectRepo.driver);

		String PaginationVal=null, PaginationSelectedVal=null,srchResultNo=null;

		PaginationVal = obj.paginationText.getText();
		srchResultNo = obj.srcResultPageNo.getText();
		PaginationSelectedVal = obj.pageSizeDrp_GetSelectedValue();
		search.SearchVerification(PaginationVal, PaginationSelectedVal, obj.grid_data, searchedItem, obj.textSearch,srchResultNo);
	}

	public void clickToFirstQuestion() {
		firstQuestion.click();
	}

	public void verifyEditQuestionBreadCrumbs() {
		try
		{
			String val = editQuestionBreadCrumbs.getText();
			Thread.sleep(1000);
			Assert.assertEquals(val, "Home" + "Administration" + "Questions" + "Add / Edit Question");
		}
		catch(Exception e)
		{
			e.toString();
		}
	}

	public void clickToSaveButton() throws Exception {
		editQuestionSaveButton.click();
	}

	public void verifyQuestionsSaveButton() throws Exception
	{
		boolean bool = editQuestionSaveButton.isEnabled();
		new Scroll().scrollDown(ObjectRepo.driver);
		Thread.sleep(2000);
		assertTrue(bool);
	}

	public void clickToCancelButton() {
		editQuestionCancelButton.click();
	}

	public void verifyQuestionPageRedirection() throws Exception {
		Thread.sleep(1000);
		Assert.assertEquals(questionsModuleName.getText(), "Questions");
	}

	public void verifySaveButtonColor() throws Exception {
		new Scroll().scrollDown(ObjectRepo.driver);
		Thread.sleep(1000);
		Assert.assertEquals(btnHelper.BtnColor(editQuestionSaveButton), "#5cb85c");
	}

	public void verifyCancelButtonColor() throws Exception {
		new Scroll().scrollDown(ObjectRepo.driver);
		Thread.sleep(1000);
		Assert.assertEquals(btnHelper.BtnColor(editQuestionCancelButton), "#5a5a5a");
	}

	public void CopyClick(String firstcopyname) throws Exception {
		Copy.VerifyCopyPopup(firstcopyname);
	}

	public void CopyDefaultValue() throws Exception {
		Copy.CopyPopupDefaultValue();
	}

	public void CreateCopy() throws Exception {
		Copy.CreateCopy();
	}
	public void clickonAddNewBtn() throws InterruptedException
	{
		Thread.sleep(3000);
		questionsAddNewBtn.click();
	}
	public void questionsNamesendkeys(String questionsnameminival) throws Exception
	{
		Thread.sleep(1000);
		questionsNameTitle.clear();
		questionsNameTitle.sendKeys(questionsnameminival);
	}
	public void verifyQuestionRequiredVal()
	{
		try
		{
			Thread.sleep(1000);
			String questionval = questionsRequiredVal.getText();
			Assert.assertEquals("Question Name is required.", questionval);
		}
		catch(Exception e)
		{
			e.toString();
		}
	}
	public void verifyQuestionMinimumVal()
	{
		try
		{
			Thread.sleep(1000);
			String questionminival = questionsMinimumVal.getText();
			Assert.assertEquals("Question Name must be at least 2 characters.", questionminival);
		}
		catch(Exception e)
		{
			e.toString();
		}
	}
	public void verifyQuestionMaximumVal()
	{
		try
		{
			Thread.sleep(1000);
			String questionmaxval = questionsMaximumVal.getText();
			Assert.assertEquals("Question Name cannot be more than 200 characters.", questionmaxval);
		}
		catch(Exception e)
		{
			e.toString();
		}
	}
	public void verifyQuestionNoVal()
	{
		boolean reqMsg = ObjectRepo.driver.findElements(By.xpath("//*[@translate='secure.admin.question.edit.QUESTIONNAMEISREQUIRED']")).size() == 0,

				minMsg = ObjectRepo.driver.findElements(By.xpath("//*[@translate='secure.admin.question.edit.QUESTIONNAMESHOULDBEATLEAST2CHARACTERSLONG']")).size() == 0,

				maxMsg = ObjectRepo.driver.findElements(By.xpath("//*[@translate='secure.admin.question.edit.QUESTIONNAMESHOULDNOTBEMORETHAN200CHARACTERSLONG']")).size() == 0,                  

				uniqMsg = ObjectRepo.driver.findElements(By.xpath("//*[@translate='secure.admin.question.edit.QUESTIONNAMEMUSTBEUNIQUE']")).size() == 0,                         

				flag;

		if (reqMsg && minMsg && maxMsg && uniqMsg) {
			flag = true;
		} else {
			flag = false;
		}
		Assert.assertEquals(flag, true);
	}
	public void verifyQuestionUniqueVal()
	{
		try
		{
			Thread.sleep(1000);
			Assert.assertEquals("Question Name must be unique.", questionsUniqueVal.getText());
		}
		catch(Exception e)
		{
			e.toString();
		}
	}
	public void questionsTagsendkeys(String questionstagminival) throws Exception
	{
		questionsTag.clear();
		questionsTag.sendKeys(questionstagminival);
	}
	public void verifyQuestionTagRequiredVal()
	{
		try
		{
			Thread.sleep(1000);
			String questiontagval = questionsTagRequiredVal.getText();
			Assert.assertEquals("Question Tag is required.", questiontagval);
		}
		catch(Exception e)
		{
			e.toString();
		}
	}
	public void verifyQuestionTagMinimumVal()
	{
		try
		{
			Thread.sleep(1000);
			String questiontagminival = questionsTagMinimumVal.getText();
			questionsDetails.click();
			Assert.assertEquals("Question Tag must be at least 2 characters.", questiontagminival);
		}
		catch(Exception e)
		{
			e.toString();
		}
	}
	public void verifyQuestionTagMaximumVal()
	{
		try
		{
			Thread.sleep(1000);
			String questiontagmaxval = questionsTagMaximumVal.getText();
			Assert.assertEquals("Question Tag cannot be more than 20 characters.", questiontagmaxval);
		}
		catch(Exception e)
		{
			e.toString();
		}
	}
	public void clickonAddTagBtn() throws InterruptedException
	{
		Thread.sleep(3000);
		questionsAddTag.click();
	}
	public void verifyQuestionTagUniqueVal()
	{
		try
		{
			Thread.sleep(1000);
			Assert.assertEquals("Question Tag already exists.", questionsTagUniqueVal.getText());
		}
		catch(Exception e)
		{
			e.toString();
		}
	}
	public void verifyQuestionTagNoVal()
	{
		boolean reqTagMsg = ObjectRepo.driver.findElements(By.xpath("//*[@translate='secure.admin.question.edit.QUESTIONRAGISREQUIRED']")).size() == 0,

				minTagMsg = ObjectRepo.driver.findElements(By.xpath("//*[@translate='secure.admin.question.edit.QUESTIONTAGSHOULDBEATLEAST2CHARACTERSLONG']")).size() == 0,

				maxTagMsg = ObjectRepo.driver.findElements(By.xpath("//*[@translate='secure.admin.question.edit.QUESTIONTAGSHOULDNOTBEMORETHAN20CHARACTERSLONG']")).size() == 0,                  

				uniqTagMsg = ObjectRepo.driver.findElements(By.xpath("//*[contains(text(), 'Question Tag already exists.')]")).size() == 0,                         

				flag;

		if (reqTagMsg && minTagMsg && maxTagMsg && uniqTagMsg) {
			flag = true;
		} else {
			flag = false;
		}
		Assert.assertEquals(flag, true);
	}
	public void selectInstructionForUser() throws InterruptedException
	{
		questionsInstForUserCheck.click();
		Thread.sleep(3000);
	}
	public void verifyInstructionForUser() throws InterruptedException
	{
		boolean instTxtBox = ObjectRepo.driver.findElements(By.xpath("//*[@name='instructionForUser']")).size() != 0,
				flag;

		if (instTxtBox) 
		{
			flag = true;
		}
		else
		{
			flag = false;
		}
		Assert.assertEquals(flag, true);
	}
	public void questionsInstructionForUsersendkeys(String questionsinstforuserminival) throws Exception
	{
		questionsInstForUserTextbox.clear();
		questionsInstForUserTextbox.sendKeys(questionsinstforuserminival);
	}
	public void verifyQuestionInstructionForUserRequiredVal()
	{
		try
		{
			String questioninstructionforuserReqval = questionsInstForUserRequiredVal.getText();
			questionsDetails.click();	
			Assert.assertEquals("User Instructions are required.", questioninstructionforuserReqval);
		}
		catch(Exception e)
		{
			e.toString();
		}
	}
	public void verifyQuestionInstForUserMinimumVal()
	{
		try
		{
			Thread.sleep(1000);
			String questioninstforuserminival = questionsInstForUserMinimumVal.getText();
			questionsDetails.click();
			Assert.assertEquals("Instructions for user must be at least 2 characters.", questioninstforuserminival);
		}
		catch(Exception e)
		{
			e.toString();
		}
	}
	public void verifyQuestionInstForUserMaximumVal()
	{
		try
		{
			Thread.sleep(1000);
			String questioninstforusermaxval = questionsInstForUserMaximumVal.getText();
			questionsDetails.click();
			Assert.assertEquals("Instructions for user cannot be more than 800 characters.", questioninstforusermaxval);
		}
		catch(Exception e)
		{
			e.toString();
		}
	}
	public void verifyQuestionInstForUserNoVal()
	{
		boolean reqInstForUserMsg = ObjectRepo.driver.findElements(By.xpath("//*[@translate='secure.admin.procedure.edit.procedure.INSTRUCTIONFORUSERISREQUIRED']")).size() == 0,

				minInstForUserMsg = ObjectRepo.driver.findElements(By.xpath("//*[@translate='secure.admin.procedure.edit.procedure.INSTRUCTIONFORUSERSHOULDBEATLEAST2CHARACTERSLONG']")).size() == 0,

				maxInstForUserMsg = ObjectRepo.driver.findElements(By.xpath("//*[@translate='secure.admin.procedure.edit.procedure.INSTRUCTIONFORUSERSHOULDNOTBEMORETHAN800CHARACTERSLONG']")).size() == 0,                                           

				flag;

		if (reqInstForUserMsg && minInstForUserMsg && maxInstForUserMsg) {
			flag = true;
		} else {
			flag = false;
		}
		Assert.assertEquals(flag, true);
	}
	public void selectTypeOption() throws Exception
	{
		helper.SelectDrpDwnValueByText(questionsType, "Textbox");
	}
	public void questionsSaveButton() throws InterruptedException {
		Thread.sleep(3000);
		questionsSaveButton.click();
		Thread.sleep(3000);
	}
	public void verifyAuditTrailPage() throws Exception {
		log.info(verifyAuditTrail);
		Assert.assertEquals(verifyAuditTrail.getText(), "Audit Trail");
	}
	public void verifyAuditTrailBreadCrumbs() {
		try
		{
			String val = editQuestionBreadCrumbs.getText();
			Thread.sleep(1000);
			Assert.assertEquals(val, "Home" + "Administration" + "Questions" + "Audit Trail");
		}
		catch(Exception e)
		{
			e.toString();
		}
	}
	public void verifyBackButtonColor() throws Exception {
		Assert.assertEquals(btnHelper.BtnColor(auditTrailBackButton), "#5a5a5a");
	}
	public void clickBackButton() throws InterruptedException {
		Thread.sleep(3000);
		auditTrailBackButton.click();
	}
	public void verifyAuditTrailBackPage() throws Exception {
		log.info(questionsModuleName);
		Assert.assertEquals(questionsModuleName.getText(), "Questions");
	}
	public void verifyQuestionsBreadCrumbs() {
		try
		{
			String val = editQuestionBreadCrumbs.getText();
			Thread.sleep(1000);
			Assert.assertEquals(val, "Home" + "Administration" + "Questions" );
		}
		catch(Exception e)
		{
			e.toString();
		}
	}

	public void verifyNewAddedQuestion(String newQuestion) throws Exception {

		Assert.assertEquals(firstQuestion.getText(), newQuestion);
	}

	public void clickonRadioBtnOfRegAndCondition() throws InterruptedException
	{
		Thread.sleep(2000);
		radioBtnOfRegAndCondition.click();
		Thread.sleep(1000);
	}

	public void clickonRadioBtnOfCondition() throws InterruptedException
	{
		Thread.sleep(2000);
		conditionRadioBtn.click();
		Thread.sleep(1000);
	}

	public void selectTypeOptionForConditon() throws Exception
	{
		helper.SelectDrpDwnValueByText(conditionType, "Greater than");
	}

	public void enterLengthOfCondition (String length) throws Exception {
		conditionLength.sendKeys(length);
	}
	
	public void enterValMsgOfCondition (String valMsg) throws Exception {
		conditionValMsg.sendKeys(valMsg);
	}
}