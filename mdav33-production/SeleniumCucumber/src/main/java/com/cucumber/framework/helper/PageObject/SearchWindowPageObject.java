package com.cucumber.framework.helper.PageObject;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.swing.text.StyledEditorKit.BoldAction;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;

import com.cucumber.framework.helper.Helper;
import com.cucumber.framework.helper.Button.ButtonHelper;
import com.cucumber.framework.helper.Logger.LoggerHelper;
import com.cucumber.framework.helper.Scroll.Scroll;
import com.cucumber.framework.helper.Search.Search;
import com.cucumber.framework.settings.ObjectRepo;
import com.cucumber.framework.utility.Constant;
import com.cucumber.framework.utility.ExcelUtils;
import com.fasterxml.jackson.annotation.JsonTypeInfo.As;

import cucumber.api.java.eo.Se;
import cucumber.api.java.lu.a;
import gherkin.lexer.Th;

import com.cucumber.framework.helper.Sorting.SortingOnColumn;

public class SearchWindowPageObject extends PageBase {
	private WebDriver driver;
	public ButtonHelper btnHelper;
	private final Logger log = LoggerHelper.getLogger(SearchWindowPageObject.class);
	private SortingOnColumn sortColumn;
	private Search search;
	private Scroll scroll;
	private CommonFunctionPageObject commonFnPageObject;

	public SearchWindowPageObject(WebDriver driver) {
		super(driver);
		this.driver = driver;
		btnHelper = new ButtonHelper(driver);
		sortColumn = new SortingOnColumn();
		search = new Search();
		scroll = new Scroll();
	}

	/** WebElements */

	@FindBy(how = How.LINK_TEXT, using = "Qualification Performance")
	public WebElement qualificationPerTile;

	@FindBy(how = How.LINK_TEXT, using = "Search Window")
	public WebElement searchWindTile;

	@FindBy(how = How.XPATH, using = "//*[@name='searchModuleSelect']")
	public WebElement moduleDrpDwn;

	@FindBy(how = How.XPATH, using = "//*[@title='Click to search']")
	public WebElement searchBtn;

	@FindBy(how = How.XPATH, using = "(//*[@ng-class='settings.buttonClasses'])[1]")
	public WebElement appDrpDwn;

	@FindBy(how = How.XPATH, using = "//*[@ng-model='searchFilter']")
	public WebElement searchFilter;

	@FindBy(how = How.XPATH, using = "(//*[@class='checkboxInput'])[1]")
	public WebElement searchFirstAppRadBtn;

	@FindBy(how = How.XPATH, using = "//*[@class='currentStep ng-binding']")
	public WebElement searchWindowLabl;

	@FindBy(how = How.XPATH, using = "//*[@class='crumb breadcrumb gray ng-isolate-scope']")
	public WebElement searchWindowBreCr;

	public WebElement appName;

	@FindBy(how = How.XPATH, using = "//*[@ng-click='vm.onChangeFilter()']")
	public WebElement filterByBtn;

	@FindBy(how = How.XPATH, using = "(//*[@ng-class='settings.buttonClasses'])[2]")
	public WebElement windowType;

	@FindBy(how = How.XPATH, using = "(//*[@data-ng-click='selectAll()'])[2]")
	public WebElement checkAllBtn2;

	@FindBy(how = How.XPATH, using = "(//*[@data-ng-click='deselectAll();'])[2]")
	public WebElement uncheckAllBtn2;

	public WebElement windowTypeCheckBox;

	@FindBy(how = How.XPATH, using = "//*[@ng-model='vm.window.searchByDrop']")
	public WebElement dateOpDrpDwn;

	@FindBy(how = How.XPATH, using = "//*[@name='startDateTextBox']")
	public WebElement dateOption;

	@FindBy(how = How.XPATH, using = "//*[@class='btn btn-sm btn-info uib-datepicker-current ng-binding']")
	public WebElement todayDateBtn;

	@FindBy(how = How.XPATH, using = "//*[@class='btn btn-default margin-right-0-advanced-search']")
	public WebElement datePicker;

	@FindBy(how = How.XPATH, using = "(//a[@href=''][text()='Clear'])[1]")
	public WebElement clearLink;

	@FindBy(how = How.XPATH, using = "//*[@value='monthYear']")
	public WebElement monthYearRaBtn;

	@FindBy(how = How.XPATH, using = "//select[@name='yearSelect']")
	public WebElement yearDrpDwn;

	@FindBy(how = How.XPATH, using = "//*[@class='err-message ng-scope']")
	public WebElement monthyerValMsg;

	@FindBy(how = How.XPATH, using = "(//*[@class='nav-link ng-binding'])[2]")
	public WebElement systemFav;

	@FindBy(how = How.XPATH, using = "(//*[@class='ng-binding ng-scope'])[1]")
	public WebElement lastUpdatedBy;

	@FindBy(how = How.XPATH, using = "//*[@ng-if='!vm.isFavoriteUpdate']")
	public WebElement addToFavBtn;

	@FindBy(how = How.XPATH, using = "//*[@name='searchNameTextbox']")
	public WebElement favNameTextBox;

	@FindBy(how = How.XPATH, using = "//*[@class='btn btn-success ng-binding']")
	public WebElement addToFavSaveBtn;

	@FindBy(how = How.XPATH, using = "//*[@class='wrap-file-title-entity']")
	public WebElement favSearchName;

	@FindBy(how = How.XPATH, using = "//*[@translate='text.FAVORITENAMEMUSTBEUNIQUE']")
	public WebElement favNameValMsgForDup;

	@FindBy(how = How.XPATH, using = "//*[@translate='text.FAVORITENAMEISREQUIRED']")
	public WebElement favNameValMsgRequired;

	@FindBy(how = How.XPATH, using = "//*[@translate='text.FAVORITENAMESHOULDBEAT2']")
	public WebElement favNameValMsgForMin;

	@FindBy(how = How.XPATH, using = "//*[@translate='text.FAVORITENAMESHOULDNOTMORE50']")
	public WebElement favNameValMsgForMax;

	@FindBy(how = How.XPATH, using = "(//*[@ng-click='vm.closePopup()'])[2]")
	public WebElement addToFavCancelBtn;

	@FindBy(how = How.XPATH, using = "//*[@title='Click to clear filters and record results']")
	public WebElement clearAllBtn;

	@FindBy(how = How.XPATH, using = "//*[@class='glyphicon glyphicon-remove critearia-01 ng-click-active']")
	public WebElement deleteFirstEntry;

	@FindBy(how = How.XPATH, using = "//*[@style='display: block;']")
	public WebElement deleteFirstEntryConfiPopup;

	@FindBy(how = How.XPATH, using = "//*[@class='cancel']")
	public WebElement deleteFirstEntryConfiPopupNoBtn;

	@FindBy(how = How.XPATH, using = "//*[@class='confirm']")
	public WebElement deleteFirstEntryConfiPopupYesBtn;





	// -----------------------------------public methods
	// ----------------------------------------------------------
	public void clickQualificationPerTile() throws Exception {
		Thread.sleep(1000);
		qualificationPerTile.click();
	}

	public void clicksearchWindTile() throws Exception {
		Thread.sleep(1000);
		searchWindTile.click();
	}

	public void verifyModDrpDwnDefVal() throws Exception {

		Select selectedValue = new Select(moduleDrpDwn);
		WebElement option = selectedValue.getFirstSelectedOption();
		Assert.assertEquals(option.getText(), "-- Please Select --");
	}

	public void selectValOfModDrpDwn() throws Exception {

		Select selectedValue = new Select(moduleDrpDwn);
		selectedValue.selectByIndex(1);
	}

	public void verifySearchBtnDisabled() throws Exception {

		Assert.assertEquals(searchBtn.isEnabled(), false);
	}

	public void selectValOfAppDrpDwn(String search) throws Exception {

		appDrpDwn.click();
		searchFilter.click();
		searchFilter.sendKeys(search);
		searchFirstAppRadBtn.click();
		appDrpDwn.click();
	}

	public void clickSearchBtn() throws Exception {
		searchBtn.click();
		Thread.sleep(2000);
	}

	public void verifySearchFun(String appVal) throws Exception {
		int findValue = 0;

		List<WebElement> count = driver.findElements(By.xpath("//*[@data-th='App:']"));

		for (int i = 1; i <= count.size(); i++) {
			appName = ObjectRepo.driver.findElement(By.xpath("(//*[@data-th='App:'])[" + i + "]"));
			String AppName = appName.getText();
			if (AppName.equalsIgnoreCase(appVal)) {
				findValue++;
			} else {

			}
		}
		Assert.assertEquals(findValue, count.size());

	}

	public void verifySearchBtnEnabled() throws Exception {
		Assert.assertEquals(searchBtn.isEnabled(), true);
	}

	public void verifySearchWindowLabl() throws Exception {
		Assert.assertEquals(searchWindowLabl.getText(), "Search Window");
	}

	public void verifySearchWindowBreCrum() throws Exception {
		Assert.assertEquals(searchWindowBreCr.getText(), "HomeQualification PerformanceSearch Window");
	}

	public void clickFilterByBtn() throws Exception {
		filterByBtn.click();
		Thread.sleep(2000);
	}

	public void clickWindowTypeBtn() throws Exception {
		windowType.click();
	}

	public void clickCheckAllBtn2() throws Exception {
		checkAllBtn2.click();
		Thread.sleep(2000);
	}

	public void verifyCheckAllFunOfWinTyp() throws Exception {
		int findValue = 0;

		List<WebElement> count = driver.findElements(By.xpath("//*[@ng-click='checkboxClick($event, getPropertyForObject(option,settings.idProp))']"));

		for (int i = 1; i <= count.size(); i++) {
			windowTypeCheckBox = ObjectRepo.driver.findElement(By.xpath("(//*[@ng-click='checkboxClick($event, getPropertyForObject(option,settings.idProp))'])[" + i + "]"));

			if (windowTypeCheckBox.isSelected()) {
				findValue++;
			} else {

			}
		}
		Assert.assertEquals(findValue, count.size());

	}

	public void clickUncheckAllBtn2() throws Exception {
		uncheckAllBtn2.click();
		Thread.sleep(2000);
	}

	public void verifyuncheckAllFunOfWinTyp() throws Exception {
		int findValue = 0;

		List<WebElement> count = driver.findElements(By.xpath("//*[@ng-click='checkboxClick($event, getPropertyForObject(option,settings.idProp))']"));

		for (int i = 1; i <= count.size(); i++) {
			windowTypeCheckBox = ObjectRepo.driver.findElement(By.xpath("(//*[@ng-click='checkboxClick($event, getPropertyForObject(option,settings.idProp))'])[" + i + "]"));

			if (windowTypeCheckBox.isSelected()) {

			} else {
				findValue++;
			}
		}
		Assert.assertEquals(findValue, count.size());

	}

	public void selectDateOpDrpDwn() throws Exception {

		Select selectedValue = new Select(dateOpDrpDwn);
		selectedValue.selectByIndex(1);
	}

	public void verifyCurrentDate() throws Exception {
		Helper hs = new Helper();
		String dateVal =  hs.getClipboardContents(dateOption);

		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		Date date = new Date();
		String dateFormatted = dateFormat.format(date);
		System.out.println("System Date with Date Picker: " + dateFormatted);

		Assert.assertEquals(dateFormatted, dateVal);
	}

	public void clickDatePicker() throws Exception {
		datePicker.click();
	}

	public void clickTodayDateBtn() throws Exception {
		todayDateBtn.click();
	}

	public void clickClearLink() throws Exception {
		clearLink.click();
	}

	public void verifyClearFun() throws Exception {
		Select selectedValue = new Select(dateOpDrpDwn);
		WebElement option = selectedValue.getFirstSelectedOption();
		Assert.assertEquals(option.getText(), "-- Quick Dates --");
	}

	public void clickMonthYearRadBtn() throws Exception {
		monthYearRaBtn.click();
	}

	public void selectYearDrpDwn() throws Exception {

		Select selectedValue = new Select(yearDrpDwn);
		selectedValue.selectByIndex(1);
	}

	public void verifyMonthValMsg() throws Exception {
		Assert.assertEquals(monthyerValMsg.getText(), "Month is required.");
	}

	public void selectYearDrpDwnAndPressTab() throws Exception {
		Select year = new Select(yearDrpDwn);
		year.selectByIndex(0);
		yearDrpDwn.sendKeys(Keys.TAB);
		Thread.sleep(1000);
	}

	public void verifyYearValMsg() throws Exception {
		Assert.assertEquals(monthyerValMsg.getText(), "Year is required.");
	}

	public void clickSystemFav() throws Exception {
		systemFav.click();
	}

	public void verifySystemfav() throws Exception {
		Assert.assertEquals(lastUpdatedBy.getText(), "Last Updated By");
	}

	public void verifyAddToFavLabl() throws Exception {
		Assert.assertEquals(addToFavBtn.getText(), "Add To Favorite");
	}

	public void clickAddToFav() throws Exception {
		addToFavBtn.click();
	}

	public void clickFavNameTextBox() throws Exception {
		favNameTextBox.click();
	}

	public void enterDataInFavName(String favName) throws Exception {
		favNameTextBox.sendKeys(favName);
	}

	public void clickSaveBtn() throws Exception {
		addToFavSaveBtn.click();
		Thread.sleep(2000);
	}

	public void verifyAddToFavFun(String favName) throws Exception {
		Assert.assertEquals(favSearchName.getText(), favName);
	}

	public void verifyUniqueFavNamValMsg() throws Exception {
		Assert.assertEquals(favNameValMsgForDup.getText(),  "Favorite Name must be unique.");
	}

	public void verifyRequiredFavNamValMsg() throws Exception {
		Assert.assertEquals(favNameValMsgRequired.getText(),  "Favorite Name is required.");
	}

	public void verifyFavNamValMsgForMin() throws Exception {
		Assert.assertEquals(favNameValMsgForMin.getText(),  "Favorite Name must be at least 2 characters.");
	}

	public void verifyFavNamValMsgForMax() throws Exception {
		Assert.assertEquals(favNameValMsgForMax.getText(),  "Favorite Name cannot be more than 50 characters.");
	}

	public void verifyAddToFavSaveBtnColor() throws Exception {
		Assert.assertEquals(new Helper().BtnColor(addToFavSaveBtn),  "#5cb85c");
	}

	public void verifyAddToFavCancelBtnColor() throws Exception {
		Assert.assertEquals(new Helper().BtnColor(addToFavCancelBtn),  "#5a5a5a");
	}

	public void verifySearchBtnColor() throws Exception {
		Assert.assertEquals(new Helper().BtnColor(searchBtn),  "#5cb85c");
	}

	public void verifyClearAllBtnColor() throws Exception {
		Assert.assertEquals(new Helper().BtnColor(clearAllBtn),  "#5a5a5a");
	}

	public void verifyAddToFavBtnColor() throws Exception {
		Assert.assertEquals(new Helper().BtnColor(addToFavBtn),  "#337ab7");
	}

	public void clickDeleteBtnOfMyFav() throws Exception {
		deleteFirstEntry.click();
	}

	public void verifyConfirmationPopUpLabl(String windowname) throws Exception {
		Assert.assertEquals(deleteFirstEntryConfiPopup.getText(),  "Are you sure you want to remove "+ windowname + "?");
	}

	public void verifyConfirmationNoBtnColor() throws Exception {
		Assert.assertEquals(new Helper().BtnColor(deleteFirstEntryConfiPopupNoBtn),  "#d9534f");
	}

	public void clickConfirmationNoBtn() throws Exception {
		deleteFirstEntryConfiPopupNoBtn.click();
	}

	public void verifyConfirmationNoBtnFun() throws Exception {
		boolean noBtn = driver.findElements(By.xpath("//*[@class='wrap-file-title-entity']")).size()!=0;
		Assert.assertEquals(noBtn,  true);
	}

	public void verifyConfirmatinYesBtnColor() throws Exception {
		Assert.assertEquals(new Helper().BtnColor(deleteFirstEntryConfiPopupYesBtn),  "#5cb85c");
	}

	public void clickConfirmationYesBtn() throws Exception {
		Thread.sleep(2000);
		deleteFirstEntryConfiPopupYesBtn.click();
		Thread.sleep(2000);
	}

	public void verifyConfirmationYesBtnFun() throws Exception {
		boolean noBtn = driver.findElements(By.xpath("//*[@class='wrap-file-title-entity']")).size()!=0;
		Assert.assertEquals(noBtn, false);
	}
	
	public void clickClearAllBtn() throws Exception {
		Thread.sleep(2000);
		clearAllBtn.click();
		Thread.sleep(2000);
	}
	 public void verifyClearAllFun() throws Exception {
		 
		 Select moduleDropDown = new Select(moduleDrpDwn);
			moduleDropDown.getFirstSelectedOption();
		 String MudlSelct1 = moduleDropDown.getFirstSelectedOption().getText();
			System.out.println("After:" + MudlSelct1);
			String Appslct = appDrpDwn.getText();
			System.out.println("After:" + Appslct);
			String result = null;
			boolean table = driver.findElements(By.xpath("//*[@ng-if='vm.getWindows && vm.getWindows.length']")).size()!=0;
			if (Appslct.contains("-- Please Select --") && MudlSelct1.contains("-- Please Select --") && table==false) {
				result = "true";
			} else {
				result = "false";
			}
			
			Assert.assertEquals(result, "true");

	}
	
}
