package com.cucumber.framework.helper.PageObject;

import java.util.ArrayList;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import com.cucumber.framework.helper.Button.ButtonHelper;
import com.cucumber.framework.helper.Logger.LoggerHelper;
import com.cucumber.framework.helper.Pagination.Pagination;
import com.cucumber.framework.helper.Search.Search;

public class SetsPageObject extends PageBase {
	private WebDriver driver;
	public ButtonHelper btnHelper;
	private final Logger log = LoggerHelper.getLogger(SetsPageObject.class);
	private Search sObj;
	public SetsPageObject(WebDriver driver) 
	{
		super(driver);
		this.driver = driver;
		btnHelper = new ButtonHelper(driver);
		sObj = new Search();
	}

	@FindBy(how = How.XPATH, using = "//*[@ui-sref='secure.admin.home']")
	public WebElement administrationTile;

	@FindBy(how = How.XPATH, using = "//*[@ui-sref='secure.admin.sets.list']")
	public WebElement setTile;

	@FindBy(how = How.XPATH, using = "//div/span[@class='currentStep ng-binding']")
	public WebElement setsModuleName;

	@FindBy(how = How.XPATH, using = "/html/body/div/div/ui-view/ui-view/div/div[2]/grid/div[3]/div[2]/ul/li[3]/a")
	public WebElement verifyFirstPage;

	@FindBy(how = How.XPATH, using = "//*[contains(normalize-space(text()),'Showing')]")
	public WebElement verifyNextData;

	@FindBy(how = How.XPATH, using = "//*[contains(normalize-space(text()),'Showing')]")
	public WebElement verifyPreviousData;

	@FindBy(how = How.XPATH, using = "//select[@id='ddlPageSize']")
	public WebElement pageSizeDrpDwn;

	@FindBy(how = How.XPATH, using = "//*[contains(normalize-space(text()),'Showing')]")
	public WebElement showingText;

	@FindBy(how = How.XPATH, using = "//span/a[@ng-click='openModal(row._id,row.title,row.route);']")
	public WebElement linkInformation;

	@FindBy(how = How.XPATH, using = "//h4[@class='col-sm-11 ng-binding']")
	public WebElement linkInformationPopUp;

	@FindBy(how = How.XPATH, using = "//i[@class='glyphicon glyphicon-remove pull-right critearia-01 ng-click-active']")
	public WebElement linkInformationPopUpClose;

	@FindBy(how = How.XPATH, using = "//tr[1]//*[contains(text(),'View Audit Trail')]")
	public WebElement clickAuditTrail;

	@FindBy(how = How.XPATH, using = "//span[@class='currentStep ng-binding']")
	public WebElement verifyAuditTrail;

	@FindBy(how = How.XPATH, using = "//input[@id='txtSearch']")
	public WebElement searchTextBox;

	@FindBy(how = How.XPATH, using = "//a[@ng-show='vm.isValidUser']")
	public WebElement clickAddBtn;

	@FindBy(how = How.XPATH, using = "//div[@class='form-group']")
	public WebElement currentVersion;

	@FindBy(how = How.XPATH, using = "//input[@name='setNameTextBox']")
	public WebElement setTextBox;

	@FindBy(how = How.XPATH, using = "//p[@translate='secure.admin.groupsettings.edit.SETNAMEISREQUIRED']")
	public WebElement requiredMsg;

	@FindBy(how = How.XPATH, using = "//p[@translate='secure.admin.groupsettings.edit.SETNAMESHOULDBEATLEAST2CHARACTERSLONG']")
	public WebElement minimumMsg;

	@FindBy(how = How.XPATH, using = "//p[@translate='secure.admin.groupsettings.edit.SETVALUESHOULDNOTBEMORETHAN50CHARACTERSLONG']")
	public WebElement maximumMsg;

	@FindBy(how = How.XPATH, using = "//p[@ng-bind-html='vm.allowcharacter']")
	public WebElement specialCharMsg;

	@FindBy(how = How.XPATH, using = "//p[@translate='secure.admin.groupsettings.edit.SETNAMEMUSTBEUNIQUE']")
	public WebElement duplicateMsg;

	@FindBy(how = How.XPATH, using = "//input[@name='addSetValueTextBox']")
	public WebElement enterSetValue;

	@FindBy(how = How.XPATH, using = "//button[@ng-click='vm.addsetvalue(vm.setvalue.value)']")
	public WebElement addBtnSetValue;

	@FindBy(how = How.XPATH, using = "/html/body/div/div/ui-view/ui-view/fieldset/div/form/div[3]/div[1]/div[3]/div[2]/div/li[2]/div[2]")
	public WebElement setValueTable;

	@FindBy(how = How.XPATH, using = "//input[@aria-checked='true']")
	public WebElement toggleBtn;

	@FindBy(how = How.XPATH, using = "//label[@for='isActiveCheckbox']")
	public WebElement toggleBtnInactive;

	@FindBy(how = How.XPATH, using = "//a[@class='btn btn-danger11 ng-binding ng-scope']")
	public WebElement cancelBtnSet;

	@FindBy(how = How.XPATH, using = "//input[@aria-checked='false']")
	public WebElement Verify_checkbox_is_clickable_of_Add_Set_Screen;

	@FindBy(how = How.XPATH, using = "//div[3]/div[2]/div/li[2]/div[1]/input")
	public WebElement Verify_checkbox_checked_unchecked;

	@FindBy(how = How.XPATH, using = "//a[@ng-click='vm.editsetvalue(set,$index)']")
	public WebElement click_on_edit_button;

	@FindBy(how = How.XPATH, using = "//input[@name='addSetValueTextBox']")
	public WebElement edit_value;

	@FindBy(how = How.XPATH, using = "//input[@ng-model='vm.setvalue.value']")
	public WebElement new_edited_value;

	@FindBy(how = How.XPATH, using = "//a[@ng-click='vm.deletesetvalue(set,$index)']")
	public WebElement delete_s_value;

	@FindBy(how = How.XPATH, using = "//li[@ng-model='vm.set.setvalues']")
	public WebElement delete_verify;

	@FindBy(how = How.XPATH, using = "//span[@ng-click='vm.orderalphabet()']")
	public WebElement ascendingIcon;

	@FindBy(how = How.XPATH, using = "//span[@ng-click='vm.orderalphabetalt()']")
	public WebElement descendingIcon;

	@FindBy(how = How.XPATH, using = "//span[@ng-click='vm.moveDown(vm.setindex)']")
	public WebElement moveDown;
	@FindBy(how = How.XPATH, using = "//span[@ng-click='vm.moveUp(vm.setindex)']")
	public WebElement moveUp;

	@FindBy(how = How.XPATH, using = "//form/div[3]/div[1]/div[3]/div[2]/div/li[4]/div[2]")
	public WebElement verifyDown;

	@FindBy(how = How.XPATH, using = "//*[@value='importFromExistingSet']")
	public WebElement radioExistingSet;

	@FindBy(how = How.XPATH, using = "//select[@ng-model='vm.existingset.existingset']")
	public WebElement selectExistingSet;

	@FindBy(how = How.XPATH, using = "//*[@ng-model='vm.selectAllValue']")
	public WebElement selectAllSet;

	@FindBy(how = How.XPATH, using = "/html/body/div/div/ui-view/ui-view/fieldset/div/form/div[3]/div[3]/div/span")
	public WebElement moveArrow;

	@FindBy(how = How.XPATH, using = "/html/body/div[1]/div/ui-view/ui-view/fieldset/div/form/div[3]/div[1]/div[3]/div[2]/div/li[7]/div[2]")
	public WebElement beforeclickDownbtn;

	/* -------------------------- |----------------| -------------------------- */
	/* -------------------------- | Public Methods | -------------------------- */
	/* -------------------------- |----------------| -------------------------- */

	public void clicktoAdministrationTile() throws Exception {
		log.info(administrationTile);
		administrationTile.click();
	}

	public void clicktoSetTile() throws Exception {
		log.info(setTile);
		setTile.click();
	}

	public void verifySetsModuleName() throws Exception {
		log.info(setsModuleName);
		Assert.assertEquals(setsModuleName.getText(), "Sets");
	}

	public void verifyFirstPagePagination() throws Exception {
		log.info(verifyFirstPage);
		Assert.assertEquals(verifyFirstPage.getText(), "1");
	}

	public void verifyNextBtnPagination() throws Exception {
		log.info(verifyNextData);
		Thread.sleep(1000);
		String output = verifyNextData.getText();
		boolean actual = output.startsWith("11", 8);
		Assert.assertEquals(actual, true);
	}

	public void clickLinkInformationLink() throws Exception {
		log.info(linkInformation);
		Thread.sleep(1000);
		linkInformation.click();
	}

	public void verifyLinkInformationPopUp() throws Exception {
		log.info(linkInformationPopUp);
		Thread.sleep(1000);
		Assert.assertEquals(linkInformationPopUp.getText(),
				"Your current set used in these following Attributes, Entities and/or Questions:");
		linkInformationPopUpClose.click();
	}

	public String searchTextBoxSendkeys(String searchValue) throws Exception {
		log.info(clickAuditTrail);
		Thread.sleep(1500);
		searchTextBox.sendKeys(searchValue);
		return searchValue;
	}

	public void verifySearchTextBoxInput(String searchValue) throws Exception {
		log.info(clickAuditTrail);
		Thread.sleep(1500);
		String paginationText = showingText.getText();
		Select select = new Select(pageSizeDrpDwn);
		WebElement option = select.getFirstSelectedOption();
		String pageSizeDrpDwnVal = option.getText();
		WebElement Grid = driver.findElement(By.xpath("//*[@ng-if='vm.options.data && vm.options.data.length']"));
		sObj.SearchVerification(paginationText, pageSizeDrpDwnVal, Grid, searchValue, searchTextBox, paginationText);
	}

	public void clickViewAuditTrailLink() throws Exception {
		log.info(clickAuditTrail);
		Thread.sleep(1500);
		clickAuditTrail.click();
	}

	public void verifyAuditTrailPage() throws Exception {
		log.info(verifyAuditTrail);
		Assert.assertEquals(verifyAuditTrail.getText(), "Audit Trail");
	}

	public void clickAddBtnSets() throws Exception {
		log.info(clickAddBtn);
		Thread.sleep(1500);
		clickAddBtn.click();
	}

	public void verifyCurrentVersion() throws Exception {
		log.info(currentVersion);
		Thread.sleep(1500);
		ArrayList<Object> list = new ArrayList<Object>();
		list.add(currentVersion.getText());
		Assert.assertEquals(list.toString(), "[Current Version\n" + "1.00]");
	}

	public void enterSetTextbox(String reqVal) throws Exception {
		log.info(setTextBox);
		Thread.sleep(1500);
		setTextBox.sendKeys(reqVal);
		setTextBox.clear();
		setTextBox.sendKeys(Keys.TAB);
	}

	public void verifySetRequiredValidationMsg() throws Exception {
		log.info(requiredMsg);
		String actual = requiredMsg.getText();
		String expected = "Set Name is required.";
		Assert.assertEquals(actual, expected);
	}

	public void enterOneChar(String reqVal) throws Exception {
		log.info(setTextBox);
		Thread.sleep(1500);
		setTextBox.sendKeys(reqVal);
	}

	public void verifySetMinimumValidationMsg() throws Exception {
		log.info(minimumMsg);
		String actual = minimumMsg.getText();
		String expected = "Set Name must be at least 2 characters.";
		Assert.assertEquals(actual, expected);
	}

	public void enterFiftyOneChar(String reqVal) throws Exception {
		log.info(setTextBox);
		Thread.sleep(1500);
		setTextBox.sendKeys(reqVal);
	}

	public void verifySetMaximumValidationMsg() throws Exception {
		log.info(minimumMsg);
		String actual = maximumMsg.getText();
		String expected = "Set Value cannot be more than 50 characters long.";
		Assert.assertEquals(actual, expected);
	}

	public void enterSpecialChar(String reqVal) throws Exception {
		log.info(setTextBox);
		Thread.sleep(1500);
		setTextBox.sendKeys(reqVal);
		setTextBox.sendKeys(Keys.TAB);
	}

	public void verifySetSpecialCharValidationMsg() throws Exception {
		log.info(specialCharMsg);
		String actual = specialCharMsg.getText();
		String expected = "Set Name should start with alphabets only.";
		Assert.assertEquals(actual, expected);
	}

	public void enterValidSets(String reqVal) throws Exception {
		log.info(setTextBox);
		Thread.sleep(1500);
		setTextBox.sendKeys(reqVal);
		setTextBox.sendKeys(Keys.TAB);
	}

	public void verifyValidSetsNoMsg() throws Exception {
		log.info(specialCharMsg);
		String actual = setTextBox.getText();
		Assert.assertEquals(actual, "");
	}

	public void enterDuplicateSetsName(String duplicateVal) throws Exception {
		log.info(specialCharMsg);
		setTextBox.sendKeys(duplicateVal);
		setTextBox.sendKeys(Keys.TAB);
	}

	public void verifyDuplicateValidationMsg() throws Exception {
		log.info(duplicateMsg);
		String actual = duplicateMsg.getText();
		String expected = "Set Name must be unique.";
		Assert.assertEquals(actual, expected);
	}

	public void enterSetValueTextBox(String enterSet) throws Exception {
		log.info(enterSetValue);
		enterSetValue.sendKeys(enterSet);
	}

	public void clickAddBtnSetValue() throws Exception {
		log.info(addBtnSetValue);
		addBtnSetValue.click();
	}

	public void verifySetValueTextBoxValue(String enterSet) throws Exception {
		log.info(setValueTable);
		Thread.sleep(1500);
		String actual = setValueTable.getText();
		Assert.assertEquals(actual, enterSet);
	}

	public void verifyToggleBtnDefaultVal() throws Exception {
		log.info(toggleBtn);
		Thread.sleep(1500);
		boolean actual = toggleBtn.isSelected();
		Assert.assertEquals(actual, true);
	}

	public void clickToggleBtnInactive() throws Exception {
		log.info(toggleBtn);
		toggleBtnInactive.click();
	}

	public void verifyToggleBtnInactiveVal() throws Exception {
		log.info(toggleBtn);
		Thread.sleep(1500);
		if (driver.findElements(By.xpath("//input[@aria-checked='false']")).size() != 0) 
		{
			System.out.println("Status: True");
		}
		else
		{
			System.out.println("Status: False");
		}
		boolean actual = toggleBtn.isSelected();
		Assert.assertEquals(actual, false);
	}

	public void clickCancelBtnAddSet() throws Exception {
		log.info(toggleBtn);
		cancelBtnSet.click();
	}

	public void verifyCancelBtnFunctionality() throws Exception {
		log.info(toggleBtn);
		Thread.sleep(1500);
		Assert.assertEquals(setsModuleName.getText(), "Sets");
	}

	public void checkedDefaultValue() throws Exception {

		Verify_checkbox_checked_unchecked.click();
	}

	public void verifyCheckboxClickable() throws Exception {
		if (Verify_checkbox_checked_unchecked.isSelected()) 
		{
			Assert.assertEquals(true, true);
		}
		else
		{
			Assert.assertEquals(false, true);
		}
	}

	public void clickonEdit() throws Exception {
		click_on_edit_button.click();
	}

	public void editSetValue(String value) throws Exception {
		edit_value.click();
	}

	public void verifyEditFunctionality(String val) {
		String v = new_edited_value.getText();
		edit_value.clear();
		if (v != val) 
		{
			Assert.assertEquals(true, true);
			edit_value.sendKeys(val);
		}
		else
		{
			Assert.assertEquals(false, true);
		}
	}

	public void deleteSetValue() {
		delete_s_value.click();
	}

	public void deleteSetVerify() {
		boolean actual;
		if (driver.findElements(By.xpath("//li[@ng-model='vm.set.setvalues']")).size() != 0) 
		{
			actual = false;
		}
		else
		{
			actual = true;
		}
		Assert.assertEquals(actual, true);
	}

	public void addMultipleSet(String setvalue) {
		String[] arraySetValue = setvalue.split(",");
		for (int i = 0; i < arraySetValue.length; i++) 
		{
			enterSetValue.sendKeys(arraySetValue[i]);
			addBtnSetValue.click();
		}
	}

	public void clickAscendingIcon() {
		ascendingIcon.click();
	}

	public void verifyAscendingOrder(String setValues) {
		System.out.println(
				"We can't perform this test case due to Java sorting function giving different output as compared to perfeqta's output ");
	}

	public void clickDescendingIcon() {
		descendingIcon.click();
	}

	public void verifyDescendingOrder() {
		System.out.println(
				"We can't perform this test case due to Java sorting function giving different output as compared to perfeqta's output ");
	}

	public void clickMoveDown() {

		// functionality implemented in verifyMoveDown();
	}

	public void verifyMoveDown(String setvalue) {

		String[] arraySetValue = setvalue.split(",");
		String beforeClick = arraySetValue[2];
		moveDown.click();
		String afterClick = verifyDown.getText();
		if (beforeClick.equals(afterClick))
		{
			Assert.assertEquals(false, true);
		} 
		else
		{
			Assert.assertEquals(true, true);
		}
	}

	public void clickMoveUp() {
		// functionality implemented in verifyMoveUp();
	}

	public void verifyMoveUp(String setvalue) {
		String[] arraySetValue = setvalue.split(",");
		String beforeClick = arraySetValue[2];
		moveUp.click();
		String afterClick = verifyDown.getText();
		if (beforeClick.equals(afterClick))
		{
			Assert.assertEquals(false, true);

		}
		else
		{
			Assert.assertEquals(true, true);
		}
	}

	public void radiobtnAddfromExistingSet() {
		radioExistingSet.click();
		selectExistingSet.click();
	}

	public void selectValExistingSetfromDropdown() {
		Select dropdown = new Select(driver.findElement(By.xpath("//select[@ng-model='vm.existingset.existingset']")));
		dropdown.selectByIndex(16);
		selectAllSet.click();
	}

	public void moveArrow() {
		moveArrow.click();
	}

	public void verifyRecordofExistingSetvalueMoveDown() {
		String beforeClickDown = beforeclickDownbtn.getText();
		moveDown.click();
		String afterClickDown = beforeclickDownbtn.getText();
		if (beforeClickDown.equals(afterClickDown)) 
		{
			Assert.assertEquals(true, true);
		} 
		else
		{
			Assert.assertEquals(false, true);
		}
	}

	public void verifyRecordofExistingSetvalueMoveUp() {
		String beforeClickDown = beforeclickDownbtn.getText();
		moveUp.click();
		String afterClickDown = beforeclickDownbtn.getText();
		if (beforeClickDown.equals(afterClickDown))
		{
			Assert.assertEquals(false, true);
		} 
		else 
		{
			Assert.assertEquals(true, true);
		}
	}
}