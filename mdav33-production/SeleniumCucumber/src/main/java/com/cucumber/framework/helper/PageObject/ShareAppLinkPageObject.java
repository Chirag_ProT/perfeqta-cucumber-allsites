package com.cucumber.framework.helper.PageObject;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.server.handler.SendKeys;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import com.cucumber.framework.helper.Helper;
import com.cucumber.framework.helper.Button.ButtonHelper;
import com.cucumber.framework.helper.Logger.LoggerHelper;
import com.cucumber.framework.helper.Scroll.Scroll;
import com.cucumber.framework.settings.ObjectRepo;

public class ShareAppLinkPageObject extends PageBase {
	private WebDriver driver;
	public ButtonHelper btnHelper;
	private Helper helper = new Helper();
	private final Logger log = LoggerHelper.getLogger(ShareAppLinkPageObject.class);

	public ShareAppLinkPageObject(WebDriver driver) {
		super(driver);
		this.driver = driver;
		btnHelper = new ButtonHelper(driver);
	}

	/** WebElements */

	@FindBy(how = How.XPATH, using = "//a[@ui-sref='secure.admin.shareapplink.list']")
	public WebElement ShareLinkTile;
	@FindBy(how = How.XPATH, using = "//*[@id='breadcrum-ipad-768']/li[3]/span")
	public WebElement verifyShareLinkTile;
	@FindBy(how = How.XPATH, using = "//*[@id='breadcrum-ipad-768']")
	public WebElement verifybreadcrumb;

	@FindBy(how = How.XPATH, using = "//th[1]")
	public WebElement colNameForSorting;
	@FindBy(how = How.XPATH, using = "(//A[@ng-switch-when='true'][text()='View Records'])[1]")
	public WebElement FirstRecods;
	@FindBy(how = How.XPATH, using = "//*[@id='breadcrum-ipad-768']")
	public WebElement VerifyBreadcrumb;
	@FindBy(how = How.XPATH, using = "//A[@ng-show='vm.isValidUser'][text()='Share App']")
	public WebElement ShareAppBtn;

	@FindBy(how = How.XPATH, using = "//SPAN[@ng-bind-html='step.ncyBreadcrumbLabel'][text()='Add / Edit Share App Link']")
	public WebElement ShareAppBtnDirection;

	@FindBy(how = How.XPATH, using = "//DIV[@class='pull-left ng-binding'][text()='Share App Link Details']")
	public WebElement verifyLableDisplay;
	@FindBy(how = How.XPATH, using = "//DIV[@class='pull-left ng-binding'][text()='App Details']")
	public WebElement verifyLableAppDeails;
	@FindBy(how = How.XPATH, using = "//DIV[@class='pull-left ng-binding'][text()='Outside User Details']")
	public WebElement verifyLableOtherUserDeails;
	@FindBy(how = How.XPATH, using = "//SELECT[@title='Please Select']")
	public WebElement moduleDropdown;
	@FindBy(how = How.XPATH, using = "//DIV[@class='error-message-color ng-scope'][text()='Module is required.']")
	public WebElement moduleDropdownReqValidation;
	@FindBy(how = How.XPATH, using = "//*[@ng-if='(editAppShareLinkForm.app.$dirty || editAppShareLinkForm.app.$touched) && editAppShareLinkForm.app.$error.required']")
	public WebElement appDropdownReqValidation;
	@FindBy(how = How.XPATH, using = "//select[@ng-model='vm.appShareLink.app']")
	public WebElement appDropdownclick;

	@FindBy(how = How.XPATH, using = "//select[@title='Equipment']")
	public WebElement selectModule;
	@FindBy(how = How.XPATH, using = "//select[@title=\"Donation Test\"]")
	public WebElement selectApp;

	@FindBy(how = How.XPATH, using = "//A[@href=''][text()='Copy Link']")
	public WebElement copyLink;

	@FindBy(how = How.XPATH, using = "//input[@ng-change='vm.onChangeEmailTextbox()']")
	public WebElement emailInputBox;
	@FindBy(how = How.XPATH, using = "//P[@translate='option.INVALIDEMAILADDRESSES'][text()='Invalid Email Address(es).']")
	public WebElement invalidEmailMsg;
	@FindBy(how = How.XPATH, using = "//BUTTON[@ng-if='vm.emailAddress && !vm.isDuplicateEmail']")
	public WebElement EmailAddBtn;
	@FindBy(how = How.XPATH, using = "(//INPUT[@ng-disabled='vm.isVersion || !vm.save || !vm.isValidUserforEdit'])[2]")
	public WebElement VerifyMultipleEmail;

	@FindBy(how = How.XPATH, using = "//button[@ng-click='obj.isDeleted=true;editAppShareLinkForm.emaillist.$validate();']")
	public WebElement deleteEmail;
	@FindBy(how = How.XPATH, using = "//div[2]/div/div[1]/p")
	public WebElement EmailInfoMsgColor;
	@FindBy(how = How.XPATH, using = "(//INPUT[@type='checkbox'])[1]")
	public WebElement AllowMultipleApp;
	@FindBy(how = How.XPATH, using = "//div[3]/div/div/label[1]/em")
	public WebElement twooptions1InAllowMultipleApp;
	@FindBy(how = How.XPATH, using = "//div[3]/div/div/label[2]/em")
	public WebElement twooptions2InAllowMultipleApp;
	@FindBy(how = How.XPATH, using = "//input[@ng-checked=\"(vm.appShareLink.isForPerEmail == false)\"]")
	public WebElement totalNumberofAppRadioBtn;
	@FindBy(how = How.XPATH, using = "//INPUT[@type='submit']")
	public WebElement shareAppBtn;
	@FindBy(how = How.XPATH, using = "//p[@ng-if='editAppShareLinkForm.noOfTimeAllowed.$dirty && editAppShareLinkForm.noOfTimeAllowed.$error.required']")
	public WebElement totalNumberofAppRadioBtnValidationMsg;
	@FindBy(how = How.XPATH, using = "//INPUT[@name='noOfTimeAllowed']")
	public WebElement totalNumberofAppSubmissionBox;

	@FindBy(how = How.XPATH, using = "//p[@ng-if='editAppShareLinkForm.noOfTimeAllowed.$dirty && editAppShareLinkForm.noOfTimeAllowed.$error.min']")
	public WebElement totalNumberofAppSubmissioZeroValidation;
	@FindBy(how = How.XPATH, using = "//p[@ng-if='editAppShareLinkForm.noOfTimeAllowed.$dirty && editAppShareLinkForm.noOfTimeAllowed.$error.max']")
	public WebElement totalNumberofAppSubmissioMaxValidation;
	@FindBy(how = How.XPATH, using = "//input[@ng-click='vm.onAppSubmissionOptionSelect(editAppShareLinkForm, true)']")
	public WebElement clickonNumberofSunmissionperIndiv;
	@FindBy(how = How.XPATH, using = "//p[@ng-if='editAppShareLinkForm.noOfTimeAllowed.$dirty && editAppShareLinkForm.noOfTimeAllowed.$error.required']")
	public WebElement blankValidationNumberofSunmissionperIndiv;
	@FindBy(how = How.XPATH, using = "//input[@ng-model='vm.appShareLink.noOfTimeAllowed']")
	public WebElement numberofSunmissionperIndivBox;
	@FindBy(how = How.XPATH, using = "//p[@ng-if='editAppShareLinkForm.noOfTimeAllowed.$dirty && editAppShareLinkForm.noOfTimeAllowed.$error.min']")
	public WebElement minValidationNumberofSunmissionperIndivBox;
	@FindBy(how = How.XPATH, using = "//div[3]/p")
	public WebElement maxValidationNumberofSunmissionperIndivBox;
	@FindBy(how = How.XPATH, using = "//input[@ng-model='vm.setExpiryDate']")
	public WebElement setLinkexpiryDatecheckbox;
	@FindBy(how = How.XPATH, using = "//p[@translate='option.EXPIRYDATEISREQ']")
	public WebElement requiredsetLinkexpiryDate;
	@FindBy(how = How.XPATH, using = "//p[@translate='option.EXPIRYDATEFORMATISINV']")
	public WebElement invalidsetLinkexpiryDate;
	@FindBy(how = How.XPATH, using = "//input[@name='expiryDateTextBox']")
	public WebElement setLinkexpiryDateinput;
	@FindBy(how = How.XPATH, using = "//textarea[@ng-model='vm.appShareLink.message']")
	public WebElement specialMsgInputbox;
	@FindBy(how = How.XPATH, using = "//div[@translate='option.SPLMSGSHOULD2TO800']")
	public WebElement minvalidationSpecialMsg;

	@FindBy(how = How.XPATH, using = "//*[@ng-model='vm.textSearch']")
	public WebElement searchBox;

	//////////////////////////////////////////

	public void clickShareAppTile() {
		ShareLinkTile.click();
	}
	public void verifyShareAppTile() throws Exception {
		Thread.sleep(1000);
		Assert.assertEquals(verifyShareLinkTile.getText(), "Share App Link");
	}
	public void verifyBreadcrumb() {
		Assert.assertEquals(verifybreadcrumb.getText(), "HomeAdministrationShare App Link");
	}
	public void clickFirstRecord() {
		FirstRecods.click();
	}
	public void verifyBreadcrumbRecods() throws Exception {
		Thread.sleep(1000);
		Assert.assertEquals(VerifyBreadcrumb.getText(), "HomeAdministrationShare App LinkView Submitted Records");
	}
	public void clickonShareAppBtn() {
		ShareAppBtn.click();
	}
	public void verifyShareAppBtnRedirection() {
		Assert.assertEquals(ShareAppBtnDirection.getText(), "Add / Edit Share App Link");
	}
	public void VerifyLabelDisplay() {
		Assert.assertEquals(verifyLableDisplay.getText(), "Share App Link Details");
	}
	public void verifyAppdetailsLable() {
		Assert.assertEquals(verifyLableAppDeails.getText(), "App Details");
	}
	public void verifyLableOutsideUserDetail() {
		Assert.assertEquals(verifyLableOtherUserDeails.getText(), "Outside User Details");
	}
	public void pressTab() {
		moduleDropdown.sendKeys(Keys.TAB);
		moduleDropdown.sendKeys(Keys.TAB);
	}
	public void clickmoduleDropDown() {
		moduleDropdown.click();
	}
	public void verifyModuleDropdownReqValidation() {
		Assert.assertEquals(moduleDropdownReqValidation.getText(), "Module is required.");
	}
	public void clickAppDropdown() {
		appDropdownclick.click();
	}
	public void verifyAppDropdownReqValidation() {
		Assert.assertEquals(appDropdownReqValidation.getText(), "App is required.");
	}
	public void selectModulefromModuleDropdown() {

		Select dropdown = new Select(driver.findElement(By.xpath("//SELECT[@title='Please Select']")));
		dropdown.selectByIndex(1);
		// selectModule.click();
	}
	public void selectAppFromAppDropdown() {
		Select dropdown = new Select(driver.findElement(By.xpath("//select[@ng-change='vm.getToken()']")));
		dropdown.selectByIndex(1);
		//selectApp.click();
	}
	public void verifyDisplayedLink() {
		if(copyLink.isDisplayed())
		{
			Assert.assertEquals(true, true);
		} else {
			Assert.assertEquals(false, true);
		}
	}	
	public void enterEmailAddr(String value) {
		emailInputBox.sendKeys(value);
	}
	public void verifyInvalidEmail() {

		Assert.assertEquals(invalidEmailMsg.getText(), "Invalid Email Address(es).");
	}
	public void entervalidEmail(String value) {
		emailInputBox.sendKeys(value);
	}
	public void verifyValidEmail() {

		Assert.assertEquals(true, true);

	}
	public void enter1stValidEmail(String value) {

		emailInputBox.sendKeys(value);
		EmailAddBtn.click();
	}
	public void enter2ndValidEmail(String value) {
		emailInputBox.sendKeys(value); EmailAddBtn.click();
	}
	public void verifyAddedEmail() {
		if(VerifyMultipleEmail.isSelected()) {
			Assert.assertEquals(true, true);}
	}
	public void clickonRemoveEmail() {
		deleteEmail.click();
	}
	public void verifyRemovedEmail() {
		Assert.assertEquals(true, true);

	}
	public void verifyColorInfoMsgEmail() {
		System.out.println("This color belongs to predefined library so we can't verify");
		Assert.assertEquals(true, true);
	}
	public void allowMultipleApp() {
		AllowMultipleApp.click();
	}
	public void systemDisplay2options() {
		if(AllowMultipleApp.isSelected()) {

			Assert.assertEquals(twooptions1InAllowMultipleApp.getText(), "Total Number of App Submissions");
			Assert.assertEquals(twooptions2InAllowMultipleApp.getText(), "Number of Submissions per Individual Email Address");
		}
	}
	public void selectBtnofTotalNumberofApp() {
		totalNumberofAppRadioBtn.click();
		totalNumberofAppSubmissionBox.sendKeys(Keys.TAB);
	}
	//
	public void clickOnShareAppBtn() throws Exception{
		shareAppBtn.click();
		Thread.sleep(5000);
	}
	public void verifyValidationMsg()  {

		Assert.assertEquals(totalNumberofAppRadioBtnValidationMsg.getText(), "Total Number of App Submissions is required.");

	}
	public void enter0inTotalnuberofAppSubmission(String value) {
		totalNumberofAppSubmissionBox.sendKeys(value);
	}
	public void verifyZeroValidationNuberOfAppSubmission() {
		Assert.assertEquals(totalNumberofAppSubmissioZeroValidation.getText(), "Total Number of App Submissions should be at least 1.");
	}
	public void entermorethan10000inNumberofApp(String value) {
		totalNumberofAppSubmissionBox.sendKeys(value);
	}
	public void verifyMorethan10000valueinNumberofAppSub() {
		Assert.assertEquals(totalNumberofAppSubmissioMaxValidation.getText(), "Total Number of App Submissions must not be more than 9999.");

	}
	public void entervalidvalueInNumberofAppSub(String value) {
		totalNumberofAppSubmissionBox.sendKeys(value);
	}
	public void verifyValidvalueinNumberofAppSubmission() {
		Assert.assertEquals(true,true);

	}
	public void clickradioBtnofNumberofSubmissionIndividuals() {
		clickonNumberofSunmissionperIndiv.click();
		numberofSunmissionperIndivBox.sendKeys(Keys.TAB);
	}
	public void verifyBlankValidation() {
		Assert.assertEquals(blankValidationNumberofSunmissionperIndiv.getText(), "Number of Submissions per Individual Email Address is required.");

	}
	public void enterValueZeroinNumberofSubmissionper(String value) {
		numberofSunmissionperIndivBox.sendKeys(value);
	}
	public void verifyZeroValidationNumberofSubmission() {
		Assert.assertEquals(minValidationNumberofSunmissionperIndivBox.getText(), "Number of Submissions per Individual Email Address should be at least 1.");
	}
	public void enterMaxvalueNuberofSubmissionIndividuals(String value) {
		numberofSunmissionperIndivBox.sendKeys(value);
	}
	public void verifyMaxValidationInNumberofSubmission() {
		Assert.assertEquals(maxValidationNumberofSunmissionperIndivBox.getText(), "Number of Submissions per Individual Email Address must not be more than 9999.");

	}
	public void entervaliddataNumberofSubmission(String value) {
		numberofSunmissionperIndivBox.sendKeys(value);
	}
	public void verifyValidvalueinNumberofSubmission() {
		Assert.assertEquals(true,true);

	}
	public void clickonCheckboxofSetLinkExpiryDate() {
		setLinkexpiryDatecheckbox.click();
	}
	public void pressTabsetLinkExpiry() {
		setLinkexpiryDatecheckbox.sendKeys(Keys.TAB);
		setLinkexpiryDatecheckbox.sendKeys(Keys.TAB);
	}
	public void verifyExpiryDateValidation() {
		Assert.assertEquals(requiredsetLinkexpiryDate.getText(), "Expiry Date is required.");

	}
	public void enterInvalidDate(String value) {
		setLinkexpiryDateinput.sendKeys(value);
	}
	public void verifyValidationMsgSetlinkExpiry() {
		Assert.assertEquals(invalidsetLinkexpiryDate.getText(), "Expiry Date must be MM/DD/YYYY.");
	}
	public void enterlessthan2InSpecialMsg(String value) {
		specialMsgInputbox.sendKeys(value);
	}
	public void verifyMinValidationSpecialMsg() {
		new Scroll().scrollDown(ObjectRepo.driver);
		Assert.assertEquals(minvalidationSpecialMsg.getText(), "Special Message should be between 2 to 800 characters.");
	}
	public void enterValidSpecialMsg(String value) {
		specialMsgInputbox.sendKeys(value);
	}
	public void verifyValidDataAcceptSpecialMsg() {
		new Scroll().scrollDown(ObjectRepo.driver);
		Assert.assertEquals(true,true);
	}
	public void verifyredirectiontoListingScreen() {
		Assert.assertEquals(verifyShareLinkTile.getText(), "Share App Link");
	}
	public void pressTabAppDropdown() {
		appDropdownclick.click();
		appDropdownclick.sendKeys(Keys.TAB);
		appDropdownclick.sendKeys(Keys.TAB);

	}

	public void enterSearchBox(String value) {
		searchBox.sendKeys(value); 
	}

}// end
