package com.cucumber.framework.helper.PageObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.server.browserlaunchers.Sleeper;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import com.cucumber.framework.helper.Helper;
import com.cucumber.framework.helper.Button.ButtonHelper;
import com.cucumber.framework.helper.Logger.LoggerHelper;
import com.cucumber.framework.helper.Pagination.Pagination;
import com.cucumber.framework.helper.Search.Search;
import com.fasterxml.jackson.annotation.JsonTypeInfo.As;

public class SitesPageObject extends PageBase {
	private WebDriver driver;
	private Helper helper = new Helper();
	public ButtonHelper btnHelper;
	private final Logger log = LoggerHelper.getLogger(SitesPageObject.class);
	private Search sObj;

	public SitesPageObject(WebDriver driver) {
		super(driver);
		this.driver = driver;
		btnHelper = new ButtonHelper(driver);
		sObj = new Search();
	}

	@FindBy(how = How.XPATH, using = "//*[@ui-sref='secure.admin.home']")
	public WebElement administrationTile;

	@FindBy(how = How.XPATH, using = "//div[2]/div/a")
	public WebElement sitesTile;
	@FindBy(how = How.XPATH, using = "//breadcrumb/div/div/span[1]")
	public WebElement verifySitesTile;
	@FindBy(how = How.XPATH, using = "//div/div[1]/ul/li[2]/a")
	public WebElement siteLevelNameTab;
	@FindBy(how = How.XPATH, using = "//div/div[3]/ui-view/div/p")
	public WebElement verifySiteLevelNameTab;
	@FindBy(how = How.XPATH, using = "//*[@id='txtSearch']")
	public WebElement searchbox;

	@FindBy(how = How.XPATH, using = "//*[@id='shareapp-remove-search-text']")
	public WebElement removeSearchboxBtn;

	@FindBy(how = How.XPATH, using = "//a[@ng-click='vm.setTab(1)']")
	public WebElement siteListingBtn;
	@FindBy(how = How.XPATH, using = "//table/tbody/tr[1]/td[1]/span[1]/span/a")
	public WebElement firstSiteofSiteListing;
	@FindBy(how = How.XPATH, using = "//breadcrumb/div/div/span[1]")
	public WebElement addEditSitesofSiteListing;
	@FindBy(how = How.XPATH, using = "//a[@ng-click='vm.addSitelevel()']")
	public WebElement addnewofSitenameLevel;

	@FindBy(how = How.XPATH, using = "//button[@ng-click='vm.save(vm.sitelevel,editSiteLevelForm)']")
	public WebElement savebtnofnamelevelSite;
	@FindBy(how = How.XPATH, using = "//div[2]/a")
	public WebElement cancelbtnofnamelevelSite;

	@FindBy(how = How.XPATH, using = "//div[3]/grid/div[2]/div/table/tbody/tr[1]/td[1]/span[1]/span/a")
	public WebElement firstsiteofNameLevelSite;
	@FindBy(how = How.XPATH, using = "//div[2]/a")
	public WebElement cancelBtnfirstsiteofNameLevelSite;

	@FindBy(how = How.XPATH, using = "//*[@id='breadcrum-ipad-768']/li[3]/span")
	public WebElement redirectedNameLevelSite;
	@FindBy(how = How.XPATH, using = "//tr[1]//*[contains(text(),'View Audit Trail')]")
	public WebElement ViewAuditoflistingscreen;
	@FindBy(how = How.XPATH, using = "/html/body/div/div/div/div[1]/a")
	public WebElement verifyCancelBtnColorViewAuditoflistingscreen;
	@FindBy(how = How.XPATH, using = "//*[@id='breadcrum-ipad-768']/li[3]/span")
	public WebElement redirectedSiteListinScreen;
	@FindBy(how = How.XPATH, using = "//tr[1]/td[3]/span[1]/span/a")
	public WebElement viewAuditofSiteleveName;
	@FindBy(how = How.XPATH, using = "//*[@id='breadcrum-ipad-768']/li[4]/span")
	public WebElement viewAuditscreenofSiteleveName;

	@FindBy(how = How.XPATH, using = "//div/div/div[1]/a")
	public WebElement verifyCancelBtnColorViewAuditSiteLevelNameScreen;
 
	
	@FindBy(how = How.XPATH, using = "//grid/div[3]/div")
	public WebElement verifysearchboxSiteListingScreen;

	@FindBy(how = How.XPATH, using = "//li[7]/a")
	public WebElement lastPageOfPaginationSiteListing;
	@FindBy(how = How.XPATH, using = "//div[3]/grid/div[2]/div/table/tbody/tr[1]")
	public WebElement countnumberEnriesinNamelevelSite;

	@FindBy(how = How.XPATH, using = "//div[3]/grid/div[3]/div")
	public WebElement verifyNumberEnriesinNamelevelSite;

	@FindBy(how = How.XPATH, using = "//div[3]/grid/div[2]/div/table/thead/tr/th[1]")
	public WebElement sortingIconNamelevelSite;

	@FindBy(how = How.XPATH, using = "//tr[1]/td[7]/span[1]/span/a")
	public WebElement AuditTrailSiteListing;

	@FindBy(how = How.XPATH, using = "//grid/div[3]/div[1]")
	public WebElement totalentriesSiteListing;

	@FindBy(how = How.XPATH, using = "//div[3]/div/label")
	public WebElement Selectedpagesize;
	
	 

	/* -------------------------- |----------------| -------------------------- */
	/* -------------------------- | Public Methods | -------------------------- */
	/* -------------------------- |----------------| -------------------------- */

	public void clicktoAdministrationTile() throws Exception {
		log.info(administrationTile);
		administrationTile.click();
	}

	public void ClickonSitesTile() {

		sitesTile.click();
	}

	public void verifySitesTile() throws InterruptedException {

		String siteTitle = verifySitesTile.getText();
		Thread.sleep(1000);
		Assert.assertEquals(siteTitle, "Site Listing");

	}

	public void clickonSiteLevelName() {

		siteLevelNameTab.click();
	}

	public void verifySiteLevelname() throws InterruptedException {
		String siteLevelName = verifySiteLevelNameTab.getText(); 
		Thread.sleep(1000);
		Assert.assertEquals(siteLevelName,"Only Sites defined through bottom level will display in App.");
	}

	public void enterSiteNameInSearchbox(String value) {
		searchbox.sendKeys(value);

	}

	public void clickonRemoveBtn() {
		removeSearchboxBtn.click();

	}

	public void verifysearchboxRemovebtn() throws InterruptedException {
		String searchval = 	searchbox.getText();	
		Thread.sleep(1000);
		Assert.assertEquals(searchval, "");
	}

	public void clickonSitelisting() {
		siteListingBtn.click();

	}

	public void clickonFirstSiteofSitelisting() {
		firstSiteofSiteListing.click();

	}

	public void verifyaddEditofSitelisting() {
		Assert.assertEquals(addEditSitesofSiteListing.getText(), "Add / Edit Site");

	}

	public void clickonAddnewofsiteLevelName() throws InterruptedException {
		Thread.sleep(2000);
		addnewofSitenameLevel.click();
	}

	public void verifyColorofSaveBtn() {
		Assert.assertEquals(helper.BtnColor(savebtnofnamelevelSite), "#5cb85c");
	}

	public void verifyColorofcancelBtn() {
		Assert.assertEquals(helper.BtnColor(cancelbtnofnamelevelSite), "#5a5a5a");
	}

	public void clickonFirstSiteofsitelevelName() {
		firstsiteofNameLevelSite.click();
	}

	public void clickcancelBtnofSiteLevelName() {
		cancelBtnfirstsiteofNameLevelSite.click();
	}

	public void verifycancelBtnofSiteLevelName() {
		Assert.assertEquals(redirectedNameLevelSite.getText(), "Site Level Name");
	}

	public void clickonViewAuditSitelistingScreen() {
		ViewAuditoflistingscreen.click();
	}

	public void verifyColorofcancelBtnViewAuditScreen() {
		Assert.assertEquals(helper.BtnColor(verifyCancelBtnColorViewAuditoflistingscreen), "#5a5a5a");
	}

	public void clickonCancelBtnViewAuditScreen() {
		verifyCancelBtnColorViewAuditoflistingscreen.click();
	}

	public void verifycancelBtnofviewAuditredirection() {
		Assert.assertEquals(redirectedSiteListinScreen.getText(), "Site Listing");
	}

	public void verifySiteLevelNamescreen() {
		Assert.assertEquals(redirectedNameLevelSite.getText(), "Site Level Name");
	}

	public void clickonViewAuditSiteLevename() {
		viewAuditofSiteleveName.click();
	}

	public void verifyredirectionofAudittrailScreen() {

		Assert.assertEquals(viewAuditscreenofSiteleveName.getText(), "Audit Trail");
	}

	public void verifyColorofCancelButtonAudittrailinSiteLevelName() {
		Assert.assertEquals(helper.BtnColor(verifyCancelBtnColorViewAuditSiteLevelNameScreen), "#5a5a5a");
	}

	public void clickonCancelButtonViewAuditofSitelevel() {
		verifyCancelBtnColorViewAuditSiteLevelNameScreen.click();
		
	}

	public void verifycancelBtnofviewAuditredirectionSiteLevel() {
		Assert.assertEquals(redirectedNameLevelSite.getText(), "Site Level Name");
	}

	public void verifysearchoflisingscreen() throws Exception {
		String showmsg = verifysearchboxSiteListingScreen.getText();
		Thread.sleep(2000);
		Assert.assertEquals(showmsg, "Showing 1 to 1 of total 1 entries");
	}

	public void clickLastbtnofSiteListing() {
		lastPageOfPaginationSiteListing.click();
	}

	public void verifyNumberofEntries() {
		String entryarray = verifyNumberEnriesinNamelevelSite.getText();
		String totalnumberofentries = entryarray.substring(24, 25);
		int result = Integer.parseInt(totalnumberofentries);
		List<WebElement> TotalRowCount = driver.findElements(By.xpath("//div[3]/grid/div[2]/div/table/tbody/tr"));
		int p = TotalRowCount.size();
		Assert.assertEquals(TotalRowCount.size(), result);
	}

	public void clickonSortingIconOfSitelevel() {
		sortingIconNamelevelSite.click();
	}

	public void verifytotalEntriesSiteListing() throws Exception {
		double count = 0; double intPageSize;
		 
		String entryarray = totalentriesSiteListing.getText();
		String[] ab = entryarray.split("l ");
		String str = String.join(",", ab);
		 
		String[] cd = str.split(" e");
		String[] pp = cd[0].split(",");
		String totalnumberofentries = pp[1];
		 
		double totalEntries = Integer.parseInt(totalnumberofentries);
         
         
		String pageSizestring = Selectedpagesize.getText();
		System.out.println("disply::::" + Selectedpagesize.isDisplayed());

		if (Selectedpagesize.isDisplayed()) {
			String pagesize = pageSizestring.substring(11, 13);
			intPageSize = Integer.parseInt(pagesize);
		} else {
			intPageSize = 10;
		}
		
		double n = Math.ceil(totalEntries / intPageSize);

		for (double j = 0; j < n; j++) {

			for (int i = 1; i <= intPageSize; i++) {

				if (driver.findElements(By.xpath("//div/table/tbody/tr[" + i + "]")).size() == 1) {
					count++;
				} else {
					System.err.println("element not found");
					break;
				}
			}
			if(driver.findElements(By.xpath("//li/a[@ng-click='selectPage(page + 1, $event)']")).size() == 1)
			new PaginationPageObject(driver).clickNextBtnPagination();
		
		}
		Assert.assertEquals(count, totalEntries);
	}

}