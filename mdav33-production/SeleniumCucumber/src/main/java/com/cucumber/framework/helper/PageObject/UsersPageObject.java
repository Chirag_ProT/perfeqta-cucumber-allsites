package com.cucumber.framework.helper.PageObject;

import java.util.List;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;
import com.cucumber.framework.helper.Button.ButtonHelper;
import com.cucumber.framework.helper.Logger.LoggerHelper;
import com.cucumber.framework.helper.Scroll.Scroll;
import com.cucumber.framework.helper.Search.Search;
import com.cucumber.framework.helper.Sorting.SortingOnColumn;
import com.cucumber.framework.settings.ObjectRepo;
import com.cucumber.framework.utility.Constant;
import com.cucumber.framework.utility.ExcelUtils;
public class UsersPageObject extends PageBase
{
	private WebDriver driver;
	private ButtonHelper btnHelper;
	private final Logger log = LoggerHelper.getLogger(SetsPageObject.class);
	private Scroll scroll;
	private Search search;
	private SortingOnColumn sortColumn;
	static SoftAssert softAssertion = new SoftAssert();
	//private static CommonFunctionPageObject commonFnPageObject;
	private ExcelUtils excel;
	public UsersPageObject(WebDriver driver) 
	{
		super(driver);
		this.driver = driver;
		btnHelper = new ButtonHelper(driver);
		scroll = new Scroll();
		search =new Search();
		sortColumn= new SortingOnColumn();
	}
	public String excelData(int rowVal, int colVal) throws Exception {
		System.out.println("excelCalling.........");
		excel = new ExcelUtils();
		excel.setExcelFile(Constant.Path_TestData + Constant.File_TestData, "Users");
		System.err.println();
		return excel.readXLSFile("Users", rowVal, colVal);
	}
	@FindBy(how = How.XPATH, using = "//*[@ui-sref='secure.admin.users.list']")
	public WebElement usersTile;

	@FindBy(how=How.XPATH, using = "//*[@class='currentStep ng-binding']")
	public WebElement usersModuleName;

	@FindBy (how=How.XPATH, using="//*[@ng-click='vm.save(vm.user,editUsersForm)']")
	public WebElement usersSavebtn; 

	@FindBy(how=How.XPATH, using="//*[@class='currentStep ng-binding']")
	public WebElement usersBreadcrumbtxt;

	@FindBy (how=How.XPATH, using="//*[@ng-if='!vm.readOnlyPermission']")
	public WebElement usersCancelBtn;

	@FindBy (how=How.XPATH, using="//*[contains(normalize-space(text()),'Showing')]")
	public WebElement paginationText;

	@FindBy (how=How.XPATH, using="//*[@ng-change='vm.onPageNumberChange()']")
	public WebElement pageSizeDrp;

	@FindBy (how=How.XPATH, using="//*[@ng-if='vm.options.data && vm.options.data.length']")
	public WebElement grid_data;

	@FindBy (how=How.ID, using="txtSearch")
	public WebElement srcData;

	@FindBy (how=How.XPATH, using="//*[@class='paddingLeft-15px ng-binding']")
	public WebElement srcResultPageNo;

	@FindBy (how=How.XPATH, using="//*[@class='btn btn-danger11 sets-logspage-left-mar ng-binding']")
	public WebElement usersAuditTrailBackbtn;

	@FindBy (how=How.XPATH, using="//*[@class='crumb breadcrumb gray ng-isolate-scope']")
	public WebElement usersListingBreadCrumbs;

	@FindBy (how=How.XPATH, using ="//*[@ui-sref='secure.admin.users.edit']")
	public WebElement usersAddNewBtn;

	@FindBy (how=How.XPATH, using="//*[@ng-model='vm.user.username']")
	public WebElement usersTxtUserName;

	@FindBy (how=How.XPATH, using="//*[@ng-if='(editUsersForm.usernameTextBox.$dirty || editUsersForm.usernameTextBox.$touched) && editUsersForm.usernameTextBox.$error.required']")
	public WebElement userNameMandatoryValMsg;

	@FindBy (how=How.XPATH, using="//*[@ng-if='editUsersForm.usernameTextBox.$dirty && editUsersForm.usernameTextBox.$error.minlength']")
	public WebElement userNameMinimumvalMsg;

	@FindBy (how=How.XPATH, using="//*[@ng-if='editUsersForm.usernameTextBox.$dirty && editUsersForm.usernameTextBox.$error.maxlength']")
	public WebElement userNameMaxvalMsg;

	@FindBy (how=How.XPATH, using="//*[@ng-if='editUsersForm.usernameTextBox.$dirty && editUsersForm.usernameTextBox.$error.unique']")
	public WebElement userNameDuplicatevalMsg;

	@FindBy (how=How.XPATH, using="//*[@ng-if='(editUsersForm.firstNameTextBox.$dirty || editUsersForm.firstNameTextBox.$touched) && editUsersForm.firstNameTextBox.$error.required']")
	public WebElement firstNameMandatoryValMsg;

	@FindBy (how=How.XPATH, using="//*[@ng-if='editUsersForm.firstNameTextBox.$dirty && editUsersForm.firstNameTextBox.$error.minlength']")
	public WebElement firstNameMiniValMsg;

	@FindBy (how=How.XPATH, using="//*[@ng-model='vm.user.firstname']")
	public WebElement txtFirstName;

	@FindBy (how=How.XPATH, using="//*[@ng-if='editUsersForm.firstNameTextBox.$dirty && editUsersForm.firstNameTextBox.$error.maxlength']")
	public WebElement firstNameMaxValMsg;

	@FindBy (how=How.XPATH, using="//*[@ng-model='vm.user.lastname']")
	public WebElement txtLastName;

	@FindBy (how=How.XPATH, using="//*[@ng-if='(editUsersForm.lastNameTextBox.$dirty || editUsersForm.lastNameTextBox.$touched) && editUsersForm.lastNameTextBox.$error.required']")
	public WebElement lastNameMandatoryValMsg;

	@FindBy (how=How.XPATH, using="//*[@ng-if='editUsersForm.lastNameTextBox.$dirty && editUsersForm.lastNameTextBox.$error.minlength']")
	public WebElement lastNameMinValMsg;

	@FindBy (how=How.XPATH, using="//*[@ng-if='editUsersForm.lastNameTextBox.$dirty && editUsersForm.lastNameTextBox.$error.maxlength']")
	public WebElement lastNameMaxValMsg;

	@FindBy (how=How.XPATH, using="//*[@ng-model='vm.user.currentEmail']")
	public WebElement txtEmailAddress;

	@FindBy (how=How.XPATH, using="//*[@ng-if='(editUsersForm.usernameTextBox.$dirty || editUsersForm.usernameTextBox.$touched) && editUsersForm.usernameTextBox.$error.required']")
	public WebElement emailAddMandatoryValMsg;

	@FindBy (how=How.XPATH, using="//*[@ng-if='editUsersForm.emailTextBox.$dirty && (editUsersForm.emailTextBox.$error.pattern || editUsersForm.emailTextBox.$error.email)']")
	public WebElement invalidEmailAdd;

	@FindBy (how=How.XPATH, using="//*[@ng-if='editUsersForm.emailTextBox.$dirty && editUsersForm.emailTextBox.$error.unique']")
	public WebElement uniqueEmailAdd;

	@FindBy (how=How.XPATH, using="//input[@name='departmentTextBox']")
	public WebElement userDepartment;

	@FindBy (how=How.XPATH, using="//*[@ng-model='vm.user.passwordGenerationMethod']")
	public WebElement userGeneratePassword;

	@FindBy (how=How.XPATH, using="//*[@ng-model='vm.changepassword.newPassword']")
	public WebElement usersNewPassword;

	@FindBy (how=How.XPATH, using="//*[@ng-model='vm.user.password']")
	public WebElement usersConfirmPassword;

	@FindBy (how=How.XPATH, using="//*[@ng-if='editResetPasswordForm.newPasswordTextBox.$viewValue.length < 8' or @translate='secure.admin.user.myprofile.changepassword.MUSTBE8CHARS']")
	public static WebElement atleastMinCharMsg;

	@FindBy (how=How.XPATH, using="//*[@ng-if='editResetPasswordForm.newPasswordTextBox.$viewValue.length > 20' or @translate='secure.admin.user.myprofile.changepassword.NOTLONGERTHAN20CHAR']")
	public static WebElement atleastMaxCharMsg;

	@FindBy (how=How.XPATH, using="//*[@ng-if=\\\"!vm.matchPattern('(?=.*[a-z])',editResetPasswordForm.newPasswordTextBox.$viewValue)\\\" or @translate='secure.admin.user.myprofile.changepassword.HAVEATLEASTSMALL']")
	public static WebElement atleastSmallCaseMsg;

	@FindBy (how=How.XPATH, using="//*[@ng-if=\\\"!vm.matchPattern('(?=.*[A-Z])',editResetPasswordForm.newPasswordTextBox.$viewValue)\\\" or @translate='secure.admin.user.myprofile.changepassword.HAVEATLEASTCAPITAL']")
	public static WebElement atleastCapitalCaseMsg;

	@FindBy (how=How.XPATH, using="//*[@ng-if=\\\"!vm.matchPattern('(?=.*[!@#$*])',editResetPasswordForm.newPasswordTextBox.$viewValue)\\\" or @translate='secure.admin.user.myprofile.changepassword.HAVEATLEASTSPECSYM']")
	public static WebElement atleastSpecialMsg;

	@FindBy (how=How.XPATH, using="//*[@ng-if=\\\"!vm.matchPattern('(?=.*[0-9])',editResetPasswordForm.newPasswordTextBox.$viewValue)\\\" or @translate='secure.admin.user.myprofile.changepassword.HAVEATLEASTNUMBER']")
	public static WebElement atleastDigitMsg;

	@FindBy (how=How.XPATH, using="//*[@ng-model='vm.signup.newPassword']")
	public static WebElement SignUpNewPassword;

	@FindBy (how=How.XPATH, using="//*[@ng-model='vm.signup.confirmPassword']")
	public static WebElement SignUpConfirmPassword;

	@FindBy (how=How.XPATH, using="//*[@ng-model='vm.signup.verificationCode']")
	public static WebElement SignUpEsign;

	@FindBy (how=How.XPATH, using="//*[@ng-model='vm.signup.securityQuestion']")
	public static WebElement SignUpSecurityQue;

	@FindBy (how=How.XPATH, using="//*[@ng-model='vm.signup.securityAnswer']")
	public static WebElement SignUpSecurityAns;

	@FindBy (how=How.XPATH, using="//*[@ng-click='vm.save(vm.signup, editSignUpForm)']")
	public static WebElement SignSave;

	@FindBy (how=How.XPATH, using="//*[@class='currentStep ng-binding']")
	public static WebElement breadcrumbTxtUsers;

	@FindBy(how = How.XPATH, using = "//*[@ng-model='vm.user.username']")
	public WebElement username;

	@FindBy(how = How.XPATH, using = "//*[@ng-model='vm.user.password']")
	public WebElement password;

	@FindBy(how = How.XPATH, using = "//*[@ng-click='vm.login()']")
	public WebElement btn_login;

	@FindBy(how = How.XPATH, using = "//*[@title='Reset']")
	public WebElement resetLInk;

	@FindBy(how = How.XPATH, using = "//*[contains(text(),'Reset User Access')]//preceding-sibling::input[@type='radio' and @ng-model='vm.resetAction']")
	public WebElement rdbtnResetUserAccess;

	@FindBy(how = How.XPATH, using = "//*[@translate='text.CLICKONSUBMITBTNTORESETUSER']")
	public WebElement activeUserResetAccessMsg;

	@FindBy(how = How.XPATH, using = "//*[@ng-click='vm.submit(editResetPasswordForm)']")
	public WebElement saveButton;

	@FindBy(how = How.XPATH, using = "//*[@ng-if='!vm.isInactiveUser && vm.isValidUser && vm.isVerifiedUser']")
	public WebElement inactiveUserResetAccessMsg;

	@FindBy(how = How.XPATH, using = "//*[@ui-sref='secure.admin.users.list']")
	public WebElement resetCancelBtn;

	@FindBy(how = How.XPATH, using = "//*[contains(text(),'Reset User Password')]//preceding-sibling::input[@type='radio' and @ng-model='vm.resetAction']")
	public WebElement resetPasswordRadioBtn;

	@FindBy(how = How.XPATH, using = "//*[@ng-model='vm.resetpassword.passwordGenerationMethod']")
	public WebElement generatePassword;

	@FindBy(how = How.XPATH, using = "//*[@ng-model='vm.resetpassword.newPassword']")
	public WebElement resetPageNewPwd;

	@FindBy(how = How.XPATH, using = "//*[@ng-if='(editResetPasswordForm.newPasswordTextBox.$dirty || editResetPasswordForm.newPasswordTextBox.$touched) && editResetPasswordForm.newPasswordTextBox.$error.required']")
	public WebElement newPasswordManValMsg;

	@FindBy(how = How.XPATH, using = "//*[@ng-model='vm.resetpassword.password']")
	public WebElement resetPageConfirmPwd;

	@FindBy(how = How.XPATH, using = "//*[@ng-if=\"(editResetPasswordForm.confirmPasswordTextBox.$dirty || editResetPasswordForm.confirmPasswordTextBox.$touched) && editResetPasswordForm.confirmPasswordTextBox.$error.required\"]")
	public WebElement resetPageConfirmPwdMandValmsg;

	@FindBy(how = How.XPATH, using = "//*[@ng-if='(editResetPasswordForm.confirmPasswordTextBox.$dirty && (vm.resetpassword.password != (vm.resetpassword.newPassword || editResetPasswordForm.newPasswordTextBox.$viewValue)))']")
	public WebElement confirmPasswordMsg;

	@FindBy(how = How.XPATH, using = "//*[@ng-click='vm.submit(editResetPasswordForm)']")
	public WebElement passwordActionsubmitBtn;

	@FindBy(how = How.XPATH, using = "//*[@ng-model='vm.resetpassword.passwordGenerationMethod']")
	public WebElement resetPageGeneratePwd;
	
	@FindBy(how = How.XPATH, using = "(//*[@ng-click='vm.go(field,row,vm.resource);'])[1]")
	public WebElement clickOnFirstUser;

	/*Public Methods */
	public void clickToUsersicon()
	{
		usersTile.click();
	}

	public void verifyUsersModuleName()
	{
		String usersModuleNm = usersModuleName.getText().toString();
		Assert.assertEquals("Users", usersModuleNm);
	}
	public void clickToFirstRecordUnm() throws InterruptedException
	{		
		clickOnFirstUser.click();
		Thread.sleep(2000);


	}
	public void clickToSavebtn()
	{
		usersSavebtn.click();
	}
	public void  verifyUsersEditBreadCrumbs()
	{
		String vcl = usersBreadcrumbtxt.getText();
		Assert.assertEquals("Add / Edit User",usersBreadcrumbtxt.getText() );
	}
	public void clickonSaveBtn()
	{
		usersSavebtn.click();
	}
	public void clickonCancelBtn() throws InterruptedException
	{
		scroll.scrollDown(ObjectRepo.driver);
		Thread.sleep(1000);
		usersCancelBtn.click();
	}
	public void verifySaveButtonColor() throws InterruptedException
	{
		Assert.assertEquals(btnHelper.BtnColor(usersSavebtn), "#5cb85c");
	}
	public void verifyCanceButtonColor()
	{
		Assert.assertEquals(btnHelper.BtnColor(usersCancelBtn), "#5a5a5a");
	}
	public void userSendSearchData()
	{
		System.out.println("test");
	}
	public void verifyusersSearchData(String searchData) throws Exception
	{
		String FirstRowData[] = getFirstRowData(driver);
		String PaginationVal = paginationText.getText();
		String PaginationSelectedVal =pageSizeDrp_GetSelectedValue();
		WebElement Grid = grid_data;
		WebElement srcbox = srcData;
		String srchResultNo = srcResultPageNo.getText();
		search.SearchVerification(PaginationVal, PaginationSelectedVal, Grid, FirstRowData[0], srcbox, srchResultNo);
	}
	public String[] getFirstRowData(WebDriver driver) throws Exception 
	{
		// Example:gopi Admin Gopi Gor Reset 07/16/2018 02:15:49 Keval Prajapati (keval)
		// Active View Audit Trail
		String Username = getFirstRowValue(driver,"1").getText();
		String UserRoles = getFirstRowValue(driver, "2").getText();
		String Full_Name = getFirstRowValue(driver, "3").getText();
		String Action = getFirstRowValue(driver, "4").getText();
		String Last_Updated = getFirstRowValue(driver, "5").getText();
		String Modified_By = getFirstRowValue(driver, "6").getText();
		String Status = getFirstRowValue(driver, "7").getText();
		String Audit_Trail = getFirstRowValue(driver, "8").getText();
		String[] splited = { Username, UserRoles, Full_Name, Action, Last_Updated, Modified_By, Status, Audit_Trail };
		return splited;
	}
	public WebElement getFirstRowValue(WebDriver driver,String ColumnNo)
	{
		WebElement ele =driver.findElement(By.xpath(".//table/tbody/tr[1]/td[" + ColumnNo + "]"));
		return ele;
	}
	public String pageSizeDrp_GetSelectedValue() {
		String defaultItem;
		try 
		{
			if (driver.findElements(By.xpath("//*[@ng-change='vm.onPageNumberChange()']")).size() != 0) 
			{
				Select select = new Select(pageSizeDrp);
				WebElement option = select.getFirstSelectedOption();
				defaultItem = option.getText();
				return defaultItem;
			}
			else 
			{
				defaultItem = "1";
				return defaultItem;
			}
		}
		catch(Exception e)
		{
			defaultItem = "1";
		}
		return defaultItem;
	}
	public void verifyAscendingorder() throws Exception
	{
		String PaginationVal = paginationText.getText();
		String PaginationSelectedVal = pageSizeDrp_GetSelectedValue();
		WebElement Grid = grid_data;
		sortColumn.verifySorting("Username", 1, PaginationVal, PaginationSelectedVal, Grid);
		Thread.sleep(1000);
	}
	public void verifyBackbuttonColor()
	{
		Assert.assertEquals(btnHelper.BtnColor(usersAuditTrailBackbtn), "#5a5a5a");
	}
	public void clickonBackButton()
	{
		usersAuditTrailBackbtn.click();
	}
	public void verifyUserlistingBreadcrumbs() throws Exception
	{
		Thread.sleep(1000);
		System.out.println("test2");
		scroll.scrollUp(ObjectRepo.driver);
		String breadcrumb = usersListingBreadCrumbs.getText();
		Assert.assertEquals("Home" + "Administration" + "Users",breadcrumb);
	}
	public void clickonAddNewbtn()
	{
		usersAddNewBtn.click();
	}
	public void verifyUserNameMandatoryValMsg()	
	{
		String userNameMandatory = userNameMandatoryValMsg.getText();
		Assert.assertEquals("Username is required.", userNameMandatory);
	}
	public void userNameSendkeys(String usersName)
	{
		usersTxtUserName.sendKeys(usersName);
	}
	public void userNamemMinimumValMsg()
	{
		String userNameMini= userNameMinimumvalMsg.getText();
		Assert.assertEquals("Username must be at least 2 characters.", userNameMini);
	}
	public void userNamemMaximumValMsg()
	{
		String userNameMaximumvalMsg= userNameMaxvalMsg.getText();
		Assert.assertEquals("Username cannot be more than 20 characters.", userNameMaximumvalMsg);
	}
	public void verifyDuplicateUserName()
	{
		try
		{
			Thread.sleep(1000);
			String duplicatemsg=userNameDuplicatevalMsg.getText();
			Assert.assertEquals("User Name must be unique.", duplicatemsg);
		}
		catch(Exception e)
		{
			e.toString();
		}
	}
	public void firstNameSendkeys(String firstName)
	{
		txtFirstName.sendKeys(firstName);
	}
	public void verifyFirstNameValMsg()
	{
		String firstNameValMsg = firstNameMandatoryValMsg.getText();
		Assert.assertEquals("First Name is required.", firstNameValMsg);
	}
	public void verifyMinimumValMsg()
	{
		String firstNmMinValMsg=firstNameMiniValMsg.getText();
		Assert.assertEquals(firstNmMinValMsg, "Firstname must be at least 2 characters." );
	}
	public void verifyFirstMaxValMsg()
	{
		String firstNmMaxValMsg =firstNameMaxValMsg.getText();
		Assert.assertEquals("First Name cannot be more than 50 characters.", firstNmMaxValMsg);
	}
	public void lastNameSendkeys(String lastName)
	{
		txtLastName.sendKeys(lastName);
	}
	public void verifyLastNmMandatoryValMsg()
	{
		String lastNmMandatoryValMsg= lastNameMandatoryValMsg.getText();
		Assert.assertEquals("Last Name is required.", lastNmMandatoryValMsg);
	}
	public void verifyLastNmMinVaMsg()
	{
		String lastNmMinValMsg = lastNameMinValMsg.getText();
		Assert.assertEquals("Last Name must be at least 2 characters.", lastNmMinValMsg);
	}
	public void verifyLastNmMaxVakMsg()
	{
		String lastNmMaxValMsg = lastNameMaxValMsg.getText();
		Assert.assertEquals("Last Name cannot be more than 50 characters.", lastNmMaxValMsg);
	}
	public void verifyInvalidEmailAddManValMsg()
	{
		String inValidEmailAdd = emailAddMandatoryValMsg.getText();
		Assert.assertEquals("Username is required.", inValidEmailAdd);
	}
	public void txtEmailSendkeys(String email)
	{
		txtEmailAddress.sendKeys(email);
	}
	public void verifyInValidEmailAdd()
	{ 
		String inValidEmailAdd = invalidEmailAdd.getText();
		Assert.assertEquals("Invalid Email Address.", inValidEmailAdd);
	}
	public void verifyDuplicateValMsg()
	{
		String emailDuplicateValMsg = uniqueEmailAdd.getText();
		Assert.assertEquals("Email must be unique.", emailDuplicateValMsg);
	}
	public void departmentSendKeys(String department)
	{
		userDepartment.sendKeys(department);
	}
	public void selectGeneratePassword(String generatedBy)
	{
		Select select = new Select(userGeneratePassword);
		select.selectByVisibleText(generatedBy);
	}
	public void sendNewPassword(String newPassword)
	{
		usersNewPassword.sendKeys(newPassword);
	}
	public void sendConfirmPassword(String confirmPassword)
	{
		usersConfirmPassword.sendKeys(confirmPassword);
	}
	public void selectRole(String roleName)
	{
		WebElement role = driver.findElement(By.xpath("//*[contains(text(),'"+ roleName + "')]//preceding-sibling::input[@type='checkbox']"));
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", role);
	}
	public void selectSite()
	{
		List<WebElement> SiteCount = driver.findElements(By.xpath("/html/body/div[1]/div/ui-view/ui-view/fieldset/div/form/div[4]/div/ul/li"));
		int liTagcount = SiteCount.size();
		for (int s = 1; s <= liTagcount; s++) {
			WebElement SitesAll = driver.findElement(
					By.xpath("/html/body/div[1]/div/ui-view/ui-view/fieldset/div/form/div[4]/div/ul/li["
							+ s + "]/div[2]/input"));
			((JavascriptExecutor)driver).executeScript("arguments[0].click();",
					SitesAll);
		}
	}
	public void verifyUesrCreation(String userName, String Password) 
	{
		try
		{
			if (ObjectRepo.driver.findElements(By.xpath("//*[@class='header-username-bx ng-binding']")).size() == 1) 
			{
				System.out.println("loging out from site");
				ObjectRepo.driver.findElement(By.xpath("//*[@class='header-username-bx ng-binding']")).click();
				Thread.sleep(1000);
				ObjectRepo.driver.findElement(By.xpath("//*[@ui-sref='logout']")).click();
				Thread.sleep(2000);
			}
			else 
			{
				System.out.println("test");
			}
			username.sendKeys(userName);
			password.sendKeys(Password);
			Thread.sleep(1000);
			btn_login.click();
			Thread.sleep(5000);
			String newPassword = excelData(12, 18);
			String newConfirmPassword = excelData(12, 19);
			String securityQuestion = excelData(12,15);
			String securityAns = excelData(12, 16);
			String esign = excelData(12, 17);
			SignUpForm(newPassword, newConfirmPassword, userName, securityQuestion, securityAns, esign);

			username.sendKeys(userName);
			password.sendKeys(newPassword);

			btn_login.click();
			Thread.sleep(1000);
			softAssertion.assertEquals(breadcrumbTxtUsers.getText(), "Home");
		}
		catch(Exception e)
		{
			e.toString();
		}
	}
	public static void SignUpForm(String NewPwdByUser, String ConfirmPwdByUser, String Username, String SecurityQue,
			String SecurityAns, String eSign) throws Exception {

		Thread.sleep(1000);
		if (PasswordValidator(NewPwdByUser) && NewPwdByUser.equals(ConfirmPwdByUser)) {
			Thread.sleep(1000);

			Thread.sleep(1000);
			SignUpNewPassword.clear();
			SignUpNewPassword.sendKeys(NewPwdByUser);
			Thread.sleep(1000);
			SignUpConfirmPassword.clear();
			SignUpConfirmPassword.sendKeys(ConfirmPwdByUser);
			Thread.sleep(1000);
			Thread.sleep(1000);
			SignUpEsign.clear();
			SignUpEsign.sendKeys(eSign);
			Thread.sleep(1000);
			SelectSecurityQue(SecurityQue);
			Thread.sleep(1000);
			SignUpSecurityAns.sendKeys(SecurityAns);
			Thread.sleep(1000);
			SignSave.click();
			Thread.sleep(1000);

		} else {
			softAssertion.assertEquals(true, false);
		}
	}
	public static boolean PasswordValidator(String passwd) throws Exception {
		String pattern = "(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$*])(?=\\S+$).{8,}";

		boolean LowerCase = false, UpperCase = false, Digit = false, SpecialCase = false,
				UnicodeCase = false,
				ValidPassword = false;

		if (passwd.matches(pattern)) {

			ValidPassword = true;
		} else {
			ValidPassword = true;

			if (passwd.length() < 8) {
				try {

					Thread.sleep(1000); 
					String MinCaseVal = atleastMinCharMsg.getText();
					softAssertion.assertEquals(MinCaseVal, "Must be at least 8 characters long");
				} catch (NoSuchElementException e) {

				}
			} else if (passwd.length() > 20) {
				try {

					Thread.sleep(1000);
					String MaxCaseVal = atleastMaxCharMsg.getText();
					softAssertion.assertEquals(MaxCaseVal, "Password cannot be longer than 20 characters.");
				} catch (NoSuchElementException e) {
					e.toString();
				}
			}

			for (int i = 0; i < passwd.length(); i++) {
				char c = passwd.charAt(i);

				// See if the character is a letter or not.
				if (Character.isLetter(c)) {
					// ExtentBase.reportLog("This " + c + " = LETTER");

					if ((c < 65 || c > 90)) {
						// ExtentBase.reportLog("This " + c + " = LOWER LETTER");
						LowerCase = true;

					}
					if ((c < 97 || c > 122)) {
						// ExtentBase.reportLog("This " + c + " = CAPITAL LETTER");
						UpperCase = true;
					}
				}

				if (Character.isDigit(c)) {
					// ExtentBase.reportLog("This " + c + " DIGIT");
					Digit = true;

				}

				if (("" + c).matches("\\p{M}")) {
					// ExtentBase.reportLog("This " + c + " = UNICODE LETTER");
					UnicodeCase = true;

				}

				if (isSpecialCharacter(c)) {
					// ExtentBase.reportLog("This " + c + " = Special LETTER");
					SpecialCase = true;
				}
			}

			if (!LowerCase) {
				try {
					Thread.sleep(1000);
					String SmallCaseVal = atleastSmallCaseMsg.getText();
					softAssertion.assertEquals(SmallCaseVal, "Must have at least One Small case letter");

				} catch (NoSuchElementException e) {
					System.out.println(e.toString());
				}
			}
			if (!UpperCase) {
				try {
					Thread.sleep(1000);
					String UpperCaseVal = atleastCapitalCaseMsg.getText();
					softAssertion.assertEquals(UpperCaseVal, "Must have at least One Capital case letter");

				} catch (NoSuchElementException e) {
					System.out.println(e.toString());
				}
			}
			if (!SpecialCase) {
				try {
					Thread.sleep(1000);
					String SpecialCaseVal = atleastSpecialMsg.getText();
					softAssertion.assertEquals(SpecialCaseVal, "Must have at least One Special symbol (!, @, #, $, *)");

				} catch (NoSuchElementException e) {
					System.out.println(e.toString());
				}
			}
			if (!Digit) {
				try {
					Thread.sleep(1000);
					String DigitCase = atleastDigitMsg.getText();
					softAssertion.assertEquals(DigitCase, "Must have at least One Number");

				} catch (NoSuchElementException e) {
					e.toString();
				}
			}
		}
		softAssertion.assertAll();
		return ValidPassword;
	}
	public static  boolean isSpecialCharacter(Character c) {
		return c.toString().matches("[!@#$*]");
	}
	public static void SelectSecurityQue(String Que) throws Exception {

		Select select = new Select(SignUpSecurityQue);
		select.selectByVisibleText(Que);
	}
	public void clickonResetLink()
	{
		resetLInk.click();
	}
	public void verifyActionpgbreadcrumb()
	{
		String actionBreadcrumbs = usersBreadcrumbtxt.getText();
		Assert.assertEquals("Action", actionBreadcrumbs);
	}
	public void selectResetAccessBtn()
	{
		rdbtnResetUserAccess.click();
	}
	public void verifyResetFunctionality()
	{
		boolean actualValue;
		try
		{
			String FirstRowData[] = getFirstRowData(driver);
			if (FirstRowData[6].equals("Active"))
			{
				actualValue = userActiveORInactive();
				softAssertion.assertEquals(actualValue, true);
				String ActiveInfoMsg = activeUserResetAccessMsg.getText();
				softAssertion.assertEquals(ActiveInfoMsg, "Click Submit to reset user access.");
				saveButton.click();
				Thread.sleep(6000);
				String PageName = usersBreadcrumbtxt.getText();
				softAssertion.assertEquals(PageName, "Users");
			}
			else if (FirstRowData[6].equals("Inactive")) 
			{
				actualValue = userActiveORInactive();
				softAssertion.assertEquals(actualValue, false);
				String InactiveInfoMsg = inactiveUserResetAccessMsg.getText();
				softAssertion.assertEquals(InactiveInfoMsg,
						"Sorry, you cannot reset user access as " + FirstRowData[0] + " is an inactive user.");
				resetCancelBtn.click();
				Thread.sleep(6000);
				String PageName = usersBreadcrumbtxt.getText();
				softAssertion.assertEquals(PageName, "Users");
			}
			softAssertion.assertAll();
		} 
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	public boolean userActiveORInactive()
	{
		if (driver.findElements(By.xpath("//*[@ng-click='vm.submit(editResetPasswordForm)']")).size() != 0)
		{	
			return true;
		}
		else
		{	
			return false;
		}
	}
	public void selectGeneratePassword()
	{
		try
		{
			resetPasswordRadioBtn.click();
			String generatePass  = excelData(12,20);
			Select select = new Select(generatePassword);
			select.selectByVisibleText(generatePass);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		} 

	}
	public void enterNewPassword()
	{
		try
		{
			String newPassword  = excelData(16,18);
			resetPageNewPwd.clear();
			resetPageNewPwd.sendKeys(newPassword);
		}
		catch(Exception e)
		{
			e.toString();
		}
	}
	public void verifyInvalidNewPassword()
	{
		try
		{
			String newPassword  = excelData(14,18);
			PasswordValidator(newPassword);
		}
		catch(Exception e)
		{
			e.toString();
		}
	}
	public void verifyNewPassMandatoryValMsg()
	{
		String newPassValMsg= newPasswordManValMsg.getText();
		Assert.assertEquals("New Password is required.", newPassValMsg);
	}
	public void verifyNewConfirmPassMandatoryValMsg()
	{
		String newConfirmPassValMsg= resetPageConfirmPwdMandValmsg.getText();
		Assert.assertEquals("Confirm Password is required.", newConfirmPassValMsg);
	}
	public void enterConfirmPassword()
	{
		resetPageConfirmPwd.sendKeys("Abc@1234");
	}
	public void verifyConfirmPasswordMsg()
	{ 
		try
		{
			Thread.sleep(1000);
			String misConfirmPassword = confirmPasswordMsg.getText();
			Thread.sleep(1000);
			Assert.assertEquals("Confirm Password and New Password must be exactly same.", misConfirmPassword);
		}
		catch(Exception e)
		{
			e.toString();
		}
	}
	public void clickonSubmitBtn()
	{
		try
		{
			Thread.sleep(2000);
			passwordActionsubmitBtn.click();	
			
		}
		catch(Exception e)
		{
			e.toString();
		}
	}
	public void verifyPasswordReset()
	{
		try
		{
			Thread.sleep(8000);
			String PageName = usersBreadcrumbtxt.getText();
			Assert.assertEquals(PageName, "Users");
			/*if (ObjectRepo.driver.findElements(By.xpath("//*[@class='header-username-bx ng-binding']")).size() == 1) 
			{
				System.out.println("loging out from site");
				ObjectRepo.driver.findElement(By.xpath("//*[@class='header-username-bx ng-binding']")).click();
				Thread.sleep(1000);
				ObjectRepo.driver.findElement(By.xpath("//*[@ui-sref='logout']")).click();
				Thread.sleep(2000);
			}
			else 
			{
				System.out.println("test");
			}
			String userName = excelData(1,1);
			String passwordval = excelData(15,18);

//			ExcelUtils loginExcel= new ExcelUtils();
//			loginExcel.setExcelFile(Constant.Path_TestData + Constant.File_TestData, "Login");
			username.sendKeys(userName);
			password.sendKeys(passwordval);

			btn_login.click();
			Thread.sleep(1000);
			softAssertion.assertEquals(breadcrumbTxtUsers.getText(), "Home"); */
		}
		catch(Exception e)
		{
			e.toString();
		}
	}
	public void autoGeneratePassword() throws Exception {
		Select select = new Select(resetPageGeneratePwd);
		WebElement option = select.getFirstSelectedOption();
		String defaultItem = option.getText();
		saveButton.click();
		Thread.sleep(1000);
		String PageName = usersBreadcrumbtxt.getText();
		softAssertion.assertEquals(PageName, "Users");
	}

}
