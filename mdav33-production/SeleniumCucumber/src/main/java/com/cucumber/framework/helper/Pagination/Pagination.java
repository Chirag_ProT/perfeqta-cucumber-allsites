/**
 * @author rahul.rathore
 *	
 *	06-Aug-2016
 */
package com.cucumber.framework.helper.Pagination;

import static org.testng.Assert.assertEquals;

import java.util.Arrays;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import com.cucumber.framework.settings.ObjectRepo;

public class Pagination {

	public void PaginationVerification(String PaginationText, String PaginationSelectedVal, WebElement Grid)
			throws Exception {
		Thread.sleep(2000);
		if (ObjectRepo.driver.findElements(By.xpath("//*[contains(text(),'No Records Found.')]")).size() != 0) {

			System.out.println("No record Found Condition met.");
			System.out.println("No records found.");

		} else {

			System.out.println("Inside Pagination");
			String Pagination = PaginationText;
			System.out.println("Pagination Text:" + Pagination);
			String PaginationDrp = PaginationSelectedVal;
			String[] splited = Pagination.split("\\s+");

			// If PaginationDrp is one then there is no PaginationDrp available
			if (PaginationDrp.equalsIgnoreCase("")) {
				PaginationDrp = "1";
				System.out.println("Pagination Dropdown not available");
			}
			int ActualRowCount = 0, FromEntry = Integer.parseInt(splited[1]), ToEntry = Integer.parseInt(splited[3]),
					TotalEntry = Integer.parseInt(splited[6]), PageSizeDrp = Integer.parseInt(PaginationDrp);

			System.out.println("FromEntry:" + FromEntry + "ToEntry:" + ToEntry + "TotalEntry" + TotalEntry
					+ "PaginationDrp" + PageSizeDrp);

			if (TotalEntry <= 10) {
				PageSizeDrp = 1;
			}
			for (FromEntry = 1; ActualRowCount < TotalEntry; FromEntry = FromEntry + PageSizeDrp) {

				WebElement GridElement = Grid;

				WebElement Webtable = GridElement; // Replace TableID with Actual Table ID or Xpath
				Thread.sleep(500);
				List<WebElement> TotalRowCount = Webtable.findElements(By.xpath(
						"//*[contains(@class,'table table-hover table-striped table-bordered table-condensed')]/tbody/tr"));

				System.out.println("No. of Rows in the WebTable: " + TotalRowCount.size());
				// Now we will Iterate the Table and print the Values

				// Total Row count
				ActualRowCount = ActualRowCount + TotalRowCount.size();
				ToEntry = ActualRowCount;
				System.out.println("In loop ActualRowCount" + ActualRowCount);
				System.out.println("FromEntry:" + FromEntry + "ToEntry:" + ToEntry + "TotalEntry" + TotalEntry
						+ "PaginationDrp" + PageSizeDrp);

				if (TotalEntry > 10) {
					WebElement linkElement = ObjectRepo.driver
							.findElement(By.xpath("//*[@ng-click='selectPage(page + 1, $event)']"));
					System.out.println(linkElement.getText());
					((JavascriptExecutor) ObjectRepo.driver).executeScript("arguments[0].click();", linkElement);

				}
				Thread.sleep(500);

				System.out.println("Successfully paging");
			}
			assertEquals(ActualRowCount, TotalEntry);
			System.out.println("---------------------------------------------");
			System.out.println("Successfully: Verify Pagination");
			System.out.println("---------------------------------------------");
		}
	}

	public void EntityPaginationVerification(String PaginationText, String PaginationSelectedVal,
			WebElement Grid) throws Exception {
		Thread.sleep(2000);
		System.out.println("Inside Pagination");
		String Pagination = PaginationText;
		System.out.println("Pagination Text:" + Pagination);
		String PaginationDrp = PaginationSelectedVal;
		String[] splited = Pagination.split("\\s+");

		// If PaginationDrp is one then there is no PaginationDrp available
		if (PaginationDrp.equalsIgnoreCase("")) {
			PaginationDrp = "1";
			System.out.println("Pagination Dropdown not available");
		}
		int ActualRowCount = 0, FromEntry = Integer.parseInt(splited[1]), ToEntry = Integer.parseInt(splited[3]),
				TotalEntry = Integer.parseInt(splited[6]), PageSizeDrp = Integer.parseInt(PaginationDrp);

		System.out.println("FromEntry:" + FromEntry + "ToEntry:" + ToEntry + "TotalEntry" + TotalEntry + "PaginationDrp"
				+ PageSizeDrp);

		if (TotalEntry <= 10) {
			PageSizeDrp = 1;
		}
		for (FromEntry = 1; ActualRowCount < TotalEntry; FromEntry = FromEntry + PageSizeDrp) {

			WebElement GridElement = Grid;

			WebElement Webtable = GridElement; // Replace TableID with Actual Table ID or Xpath
			Thread.sleep(1000);
			List<WebElement> TotalRowCount = Webtable.findElements(By.xpath(
					"//*[contains(@class,'table table-hover table-striped table-bordered table-condensed') and @ng-if='vm.entityValues.length']/tbody/tr"));

			System.out.println("No. of Rows in the WebTable: " + TotalRowCount.size());
			// Now we will Iterate the Table and print the Values

			// Total Row count
			ActualRowCount = ActualRowCount + TotalRowCount.size();
			ToEntry = ActualRowCount;
			System.out.println("In loop ActualRowCount" + ActualRowCount);
			System.out.println("FromEntry:" + FromEntry + "ToEntry:" + ToEntry + "TotalEntry" + TotalEntry
					+ "PaginationDrp" + PageSizeDrp);

			if (TotalEntry > 10) {
				WebElement linkElement = ObjectRepo.driver
						.findElement(By.xpath("//*[@ng-click='selectPage(page + 1, $event)']"));
				System.out.println(linkElement.getText());
				((JavascriptExecutor) ObjectRepo.driver).executeScript("arguments[0].click();", linkElement);
			}
			Thread.sleep(1000);

			System.out.println("Successfully paging");
		}
		assertEquals(ActualRowCount, TotalEntry);
		System.out.println("---------------------------------------------");
		System.out.println("Successfully: Verify Pagination");
		System.out.println("---------------------------------------------");
	}

	public int[] PaginationCount(String Pagination) {

		String[] splited = Pagination.split("\\s+");
		int ActualRowCount = 0, FromEntry = Integer.parseInt(splited[1]), ToEntry = Integer.parseInt(splited[3]),
				TotalEntry = Integer.parseInt(splited[6]);
		int[] ArrayVal = { ActualRowCount, FromEntry, ToEntry, TotalEntry };
		System.out.println("Print Int Array");
		Arrays.toString(ArrayVal);
		return ArrayVal;
	}

	public void LinkInfoPagination(String PaginationText, WebElement Grid) throws Exception {
		Thread.sleep(2000);
		System.out.println("Inside Pagination of Link information");

		String Pagination = PaginationText;

		System.out.println("Pagination Text:" + Pagination);

		String[] splited = Pagination.split("\\s+");

		int ActualRowCount = 0, FromEntry = Integer.parseInt(splited[1]), ToEntry = Integer.parseInt(splited[3]),
				TotalEntry = Integer.parseInt(splited[6]), PageSizeDrp = 5;

		System.out.println("FromEntry:" + FromEntry + "ToEntry:" + ToEntry + "TotalEntry" + TotalEntry + "PaginationDrp"
				+ PageSizeDrp);

		if (TotalEntry <= 5) {
			PageSizeDrp = 1;
		}
		Thread.sleep(1000);
		for (FromEntry = 1; ActualRowCount < TotalEntry; FromEntry = FromEntry + PageSizeDrp) {
			WebElement GridElement = Grid;
			WebElement Webtable = GridElement; // Replace TableID with Actual Table ID or Xpath
			Thread.sleep(1000);
			List<WebElement> TotalRowCount = Webtable.findElements(By.xpath(
					"//*[@class='table table-hover table-striped table-bordered table-condensed qcform-select-menu-width-100 ng-scope']/tbody/tr"));

			System.out.println("No. of Rows in the WebTable: " + TotalRowCount.size());
			// Now we will Iterate the Table and print the Values

			// Total Row count
			ActualRowCount = ActualRowCount + TotalRowCount.size();
			ToEntry = ActualRowCount;
			System.out.println("In loop ActualRowCount" + ActualRowCount);
			System.out.println("FromEntry:" + FromEntry + "ToEntry:" + ToEntry + "TotalEntry" + TotalEntry
					+ "PaginationDrp" + PageSizeDrp);

			if (TotalEntry > 5) {
				WebElement linkElement = ObjectRepo.driver
						.findElement(By.xpath("//*[@ng-click='selectPage(page + 1, $event)']"));
				System.out.println(linkElement.getText());
				((JavascriptExecutor) ObjectRepo.driver).executeScript("arguments[0].click();", linkElement);
			}
			Thread.sleep(1000);

			System.out.println("Successfully paging");
		}
		assertEquals(ActualRowCount, TotalEntry);
		System.out.println("---------------------------------------------");
		System.out.println("Successfully: Verify Pagination");
		System.out.println("---------------------------------------------");
	}

	public void ClickFirst() throws Exception {
		Thread.sleep(1000);
		
		ObjectRepo.driver.findElement(By.xpath("//*[contains(normalize-space(text()),'Showing')]"));

		int[] Count = PaginationCount(ObjectRepo.driver
				.findElement(By.xpath("//*[contains(normalize-space(text()),'Showing')]")).getText());
		System.out.println(Count[3]);
		if (Count[3] > 10) {
			WebElement FirstButton = ObjectRepo.driver
					.findElement(By.xpath("//*[@ng-click='selectPage(1, $event)']"));

			WebElement LastButton = ObjectRepo.driver
					.findElement(By.xpath("//*[@ng-click='selectPage(totalPages, $event)']"));
			((JavascriptExecutor) ObjectRepo.driver).executeScript("arguments[0].click();", LastButton);

			List<WebElement> liElements = ObjectRepo.driver
					.findElements(By.xpath("//*[@ng-change='vm.pageChanged()']/li"));
			String[] ElementName = new String[liElements.size() + 1];
			String[] ElementClass = new String[liElements.size() + 1];

			for (int i = 1; i <= liElements.size(); i++) {

				WebElement PaginationElements = ObjectRepo.driver
						.findElement(By.xpath("//*[@ng-change='vm.pageChanged()']/li[" + i + "]"));
				System.out.println(PaginationElements.getText() + " & Its Class Value : "
						+ PaginationElements.getAttribute("class"));
				ElementName[i] = PaginationElements.getText();
				ElementClass[i] = PaginationElements.getAttribute("class");

			}

			for (int index = 1; index < ElementName.length; index++) {
				System.out.println(ElementName.length);
				// First Button Verification
				if (ElementName[index].equalsIgnoreCase("First") && ElementClass[index].contains("disabled")) {

					assertEquals(ElementName[index + 1], "Previous");
					assertEquals(ElementClass[index + 1].contains("disabled"), true);
					assertEquals(ElementName[index + 2], "1");
					assertEquals(ElementClass[index + 2].contains("active"), true);

					System.out.println("First Button is disabled & First page is loaded");
				} else {
					((JavascriptExecutor) ObjectRepo.driver).executeScript("arguments[0].click();", FirstButton);

					System.out.println("Click on First Button");
				}
			}
		} else {
			System.out.println("Records are less than 10.");
		}
	}

	public void ClickLast() throws Exception {
		Thread.sleep(5000);

		WebElement LastButton = ObjectRepo.driver
				.findElement(By.xpath("//*[@ng-click='selectPage(totalPages, $event)']"));

		List<WebElement> liElements = ObjectRepo.driver
				.findElements(By.xpath("//*[@ng-change='vm.pageChanged()']/li"));
		String[] ElementName = new String[liElements.size() + 1];
		String[] ElementClass = new String[liElements.size() + 1];

		for (int i = 1; i <= liElements.size(); i++) {

			WebElement PaginationElements = ObjectRepo.driver
					.findElement(By.xpath("//*[@ng-change='vm.pageChanged()']/li[" + i + "]"));
			System.out.println(
					PaginationElements.getText() + " & Its Class Value : " + PaginationElements.getAttribute("class"));
			ElementName[i] = PaginationElements.getText();
			ElementClass[i] = PaginationElements.getAttribute("class");
		}

		for (int index = 1; index < ElementName.length; index++) {
			System.out.println(ElementName.length);
			// First Button Verification
			if (ElementName[index].equalsIgnoreCase("Last") && ElementClass[index].contains("disabled")) {

				assertEquals(ElementName[index - 1], "Next");
				assertEquals(ElementClass[index - 1].contains("disabled"), true);
				// assertEquals(ElementName[index - 2], "1");
				assertEquals(ElementClass[index - 2].contains("active"), true);

				System.out.println("First Button is disabled & First page is loaded");
			} else {
				((JavascriptExecutor) ObjectRepo.driver).executeScript("arguments[0].click();", LastButton);

				System.out.println("Click on Last Button");
			}
		}
	}
}
