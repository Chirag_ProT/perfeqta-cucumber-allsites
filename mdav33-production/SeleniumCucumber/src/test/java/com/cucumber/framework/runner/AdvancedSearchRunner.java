/**
 * @author Chirag Karia
 *	
 *	04-06-2019
 */
package com.cucumber.framework.runner;

import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;

@CucumberOptions(features = {"classpath:featurefile/AdvancedSearch.feature" }, glue = {
		"classpath:com.cucumber.framework.stepdefinition",
		"classpath:com.cucumber.framework.helper" }, plugin = { "pretty",
		"html:target/cucumber-reports","json:target/LoginRunner.json"
		}, tags = {"@production"},
		 monochrome = true
		)
public class AdvancedSearchRunner extends AbstractTestNGCucumberTests 
{
	
}
/* "junit:target/cucumber.xml","return:target/turn.txt" */