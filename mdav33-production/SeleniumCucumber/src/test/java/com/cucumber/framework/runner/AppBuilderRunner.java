/**
 * @author rahul.rathore
 *	
 *	14-Aug-2016
 */
package com.cucumber.framework.runner;

import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;

@CucumberOptions(features = { "classpath:featurefile/AppBuilder.feature"}, glue = {
		"classpath:com.cucumber.framework.stepdefinition",
		"classpath:com.cucumber.framework.helper" }, plugin = { "pretty",
		"html:target/cucumber-reports","json:target/AppBuilderRunner.json"
		}, tags={"@production"},
		 monochrome = true
		)
public class AppBuilderRunner extends AbstractTestNGCucumberTests 
{
	
}

/* "junit:target/cucumber.xml","return:target/turn.txt" */