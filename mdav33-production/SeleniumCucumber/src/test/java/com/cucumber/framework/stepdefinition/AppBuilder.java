﻿package com.cucumber.framework.stepdefinition;

import javax.swing.text.TabableView;

import org.openqa.selenium.Keys;
import com.cucumber.framework.helper.PageObject.AppBuilderPageObject;
import com.cucumber.framework.settings.ObjectRepo;
import com.cucumber.framework.utility.Constant;
import com.cucumber.framework.utility.ExcelUtils;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.lu.a;

public class AppBuilder {

	public ExcelUtils excel;
	public AppBuilderPageObject appBuilderObj;

	public AppBuilder() {
		appBuilderObj = new AppBuilderPageObject(ObjectRepo.driver);
	}

	public String excelData(int rowVal, int colVal) throws Exception {
		System.out.println("excelCalling.........");
		excel = new ExcelUtils();
		excel.setExcelFile(Constant.Path_TestData + Constant.File_TestData, "App Builder");
		System.err.println();
		return excel.readXLSFile("App Builder", rowVal, colVal);
	}

	@Given("^: Click on App Builder Tile$")
	public void click_on_App_Builder_Tile() throws Throwable {
		appBuilderObj.appBuilderTileClick();
	}

	@Then("^: Verify module name as App Builder$")
	public void verify_module_name_as_App_Builder() throws Throwable {
		appBuilderObj.verifyAppBuilderLabel();
	}

	@Given("^: Click on Create Copy icon of first record from the Action column$")
	public void click_on_Create_Copy_icon_of_first_record_from_the_Action_column() throws Throwable {
		appBuilderObj.copyActionClick();
	}

	@Then("^: Verify that system should display Copy pop-up when a user click on the Create Copy icon$")
	public void verify_that_system_should_display_Copy_pop_up_when_a_user_click_on_the_Create_Copy_icon()
			throws Throwable {
		appBuilderObj.VerifyCopyPopup("App Title *");
	}

	@Then("^: Verify that system should display Default App Title into Copy pop-up when a user create copy of a App$")
	public void verify_that_system_should_display_Default_App_Title_into_Copy_pop_up_when_a_user_create_copy_of_a_App()
			throws Throwable {
		appBuilderObj.verifyCopyPopupDefaultValue();
	}

	@Then("^: Verify that color of Add New button should be blue$")
	public void verify_that_color_of_Add_New_button_should_be_blue() throws Throwable {
		appBuilderObj.verifyAddNewBtnColor();
	}

	@Given("^: Click on Add New button of App Builder$")
	public void click_on_Add_New_button_of_App_Builder() throws Throwable {
		appBuilderObj.addNewBtnClick();
	}

	@Then("^: Verify that Build App page should be display when a user Click on Add New button of App Builder$")
	public void verify_that_Build_App_page_should_be_display_when_a_user_Click_on_Add_New_button_of_App_Builder()
			throws Throwable {
		appBuilderObj.verifyAddNewBtnFucn();
	}

	@Then("^: Verify that (\\d+)  BASIC text should be displayed in Basic tab$")
	public void verify_that_BASIC_text_should_be_displayed_in_Basic_tab(int arg1) throws Throwable {
		appBuilderObj.verifyBasicHeaderTabLabel();
	}

	@Then("^: Verify that Color of the Basic tab should be green$")
	public void verify_that_Color_of_the_Basic_tab_should_be_green() throws Throwable {
		appBuilderObj.verifyBasicHeaderTabColor();
	}

	@Then("^: Verify that App Details label should be displayed in top left side of Basic Tab$")
	public void verify_that_App_Details_label_should_be_displayed_in_top_left_side_of_Basic_Tab() throws Throwable {
		appBuilderObj.verifyAppDetailsLabel();
	}

	@Then("^: Verify that a system should display the Current Version of the App in top right side of Basic Tab$")
	public void verify_that_a_system_should_display_the_Current_Version_of_the_App_in_top_right_side_of_Basic_Tab()
			throws Throwable {
		appBuilderObj.verifyCurreVersionLabel();
	}

	@Then("^: Verify that Color of Current Version message should blue$")
	public void verify_that_Color_of_Current_Version_message_should_blue() throws Throwable {
		appBuilderObj.verifyCurreVersionColor();
	}

	@Then("^: Verify that Site Selections label should be displayed after App Details section in Basic Tab$")
	public void verify_that_Site_Selections_label_should_be_displayed_after_App_Details_section_in_Basic_Tab()
			throws Throwable {
		appBuilderObj.verifyBasicTabLables();
	}

	@Then("^: Verify that all other tabs should be displayed in disable mode when a user access Basic Tab$")
	public void verify_that_all_other_tabs_should_be_displayed_in_disable_mode_when_a_user_access_Basic_Tab()
			throws Throwable {
		appBuilderObj.verifyDisabledTab_Basic();
	}

	@Given("^: Press \"([^\"]*)\" Key on Select App Module$")
	public void press_Key_on_Select_App_Module(String arg1) throws Throwable {
		appBuilderObj.appMdul.sendKeys(Keys.TAB);
	}

	@Then("^: Verify that system should display Required validation message as App Module is required\\.$")
	public void verify_that_system_should_display_Required_validation_message_as_App_Module_is_required()
			throws Throwable {
		appBuilderObj.verifyAppMdlReqMsg();
	}

	@Then("^: Verify that Color of Save Draft button should be blue$")
	public void verify_that_Color_of_Save_Draft_button_should_be_blue() throws Throwable {
		appBuilderObj.verifySaveDraftAppColor();
	}

	@Then("^: Verify that Color of Continue button should be green$")
	public void verify_that_Color_of_Continue_button_should_be_green() throws Throwable {
		appBuilderObj.verifyContinueBtnColor();
	}

	@Then("^: Verify that Color of App Builder Cancel button should be black$")
	public void verify_that_Color_of_App_Builder_Cancel_button_should_be_black() throws Throwable {
		appBuilderObj.verifyCancelBtnColor();
	}

	@Given("^: Click on Cancel button of App Builder$")
	public void click_on_Cancel_button_of_App_Builder() throws Throwable {
		appBuilderObj.CancelBtnClick();
	}

	@Then("^: Verify that system should display Alertbox when a user click on the Cancel button$")
	public void verify_that_system_should_display_Alertbox_when_a_user_click_on_the_Cancel_button() throws Throwable {
		appBuilderObj.verifyAlrtBoxDisply();
	}

	@Then("^: Verify that system should display Alertbox message as Whoa! Are you sure\\? All changes will be lost\\.$")
	public void verify_that_system_should_display_Alertbox_message_as_Whoa_Are_you_sure_All_changes_will_be_lost()
			throws Throwable {
		appBuilderObj.verifyCancelAlertPopUpMsg();
	}

	@Then("^: Verify that color of No button in Alertbox should be red$")
	public void verify_that_color_of_No_button_in_Alertbox_should_be_red() throws Throwable {
		appBuilderObj.verifyCancelAlertboxNoBtnColor();
	}

	@Then("^: Verify that color of Yes button in Alertbox should be green$")
	public void verify_that_color_of_Yes_button_in_Alertbox_should_be_green() throws Throwable {
		appBuilderObj.verifyCancelAlertboxYesBtnColor();
	}

	@Given("^: Click on No button of Alert Popup$")
	public void click_on_No_button_of_Alert_Popup() throws Throwable {
		appBuilderObj.cancelAlertPopupNoBtnClick();
	}

	@Then("^: Verify that Alertbox should be disappear when a user click on the No button$")
	public void verify_that_Alertbox_should_be_disappear_when_a_user_click_on_the_No_button() throws Throwable {
		appBuilderObj.verifyCancelAlertPopupNoBtnClick();
	}

	@Given("^: Click on Yes button of Alert Popup$")
	public void click_on_Yes_button_of_Alert_Popup() throws Throwable {
		appBuilderObj.cancelAlertPopupYesBtnClick();
	}

	@Then("^: Verify that system should redirect to the App Listing page when a user click on the Yes button of Alertbox$")
	public void verify_that_system_should_redirect_to_the_App_Listing_page_when_a_user_click_on_the_Yes_button_of_Alertbox()
			throws Throwable {
		appBuilderObj.verifyCancelAlertPopupyesBtnClick();
	}

	@Given("^: Click on the App Title textbox and Press \"([^\"]*)\" Key$")
	public void click_on_the_App_Title_textbox_and_Press_Key(String arg1) throws Throwable {
		appBuilderObj.appTitleTxtBox.sendKeys(Keys.TAB);
	}

	@Then("^: Verify the Required validation message for App Title field as \"([^\"]*)\"$")
	public void verify_the_Required_validation_message_for_App_Title_field_as(String arg1) throws Throwable {
		appBuilderObj.verifyAppTitleTxtBoxReqMsg();
	}

	@Then("^: Verify that color of Required validation for App Title field message should be red$")
	public void verify_that_color_of_Required_validation_for_App_Title_field_message_should_be_red() throws Throwable {
		appBuilderObj.verifyAppTitleTxtBoxReqMsgColor();
	}

	@Given("^: Click on App title textbox and Enter App title as value less than (\\d+) characters$")
	public void click_on_App_title_textbox_and_Enter_App_title_as_value_less_than_characters(int arg1)
			throws Throwable {
		appBuilderObj.appTitleTxtBoxSendkeys(excelData(1, 1));
	}

	@Then("^: Verify Minimum validation message of App title textbox asApp title must be at least (\\d+) characters\\.$")
	public void verify_Minimum_validation_message_of_App_title_textbox_asApp_title_must_be_at_least_characters(int arg1)
			throws Throwable {
		appBuilderObj.verifyAppTitleTxtBoxMinMsg();
	}

	@Then("^: Verify that color of Minimum validation message for App Title field should be red$")
	public void verify_that_color_of_Minimum_validation_message_for_App_Title_field_should_be_red() throws Throwable {
		appBuilderObj.verifyAppTitleTxtBoxMinMsgColor();
	}

	@Given("^: Click on App title textbox and Enter App title value more than (\\d+) characters$")
	public void click_on_App_title_textbox_and_Enter_App_title_value_more_than_characters(int arg1) throws Throwable {
		appBuilderObj.appTitleTxtBoxSendkeys(excelData(2, 1));
	}

	@Then("^: Verify Maximum validation message of App title textbox as App title cannot be more than (\\d+) characters\\.$")
	public void verify_Maximum_validation_message_of_App_title_textbox_as_App_title_cannot_be_more_than_characters(
			int arg1) throws Throwable {
		appBuilderObj.verifyAppTitleTxtBoxMaxMsg();
	}

	@Then("^: Verify that color of Maximum validation message for App Title field should be red$")
	public void verify_that_color_of_Maximum_validation_message_for_App_Title_field_should_be_red() throws Throwable {
		appBuilderObj.verifyAppTitleTxtBoxMaxMsgColor();
	}

	@Given("^: Click on App title textbox and Enter Valid Value in App title textbox$")
	public void click_on_App_title_textbox_and_Enter_Valid_Value_in_App_title_textbox() throws Throwable {
		appBuilderObj.appTitleTxtBoxSendkeys(excelData(3, 1));
	}

	@Then("^: Verify that system should not display validation message when a user enter valid App Title$")
	public void verify_that_system_should_not_display_validation_message_when_a_user_enter_valid_App_Title()
			throws Throwable {
		appBuilderObj.verifyAppTitleTxtBoxNoMsg();
	}

	@Given("^: Click on App Description textbox and Enter App Description as value less than (\\d+) characters$")
	public void click_on_App_Description_textbox_and_Enter_App_Description_as_value_less_than_characters(int arg1)
			throws Throwable {
		appBuilderObj.appDescptTxtBoxSendkeys(excelData(4, 2));
	}

	@Then("^: Verify Minimum validation message of App Description textbox as App Description must be at least (\\d+) characters\\.$")
	public void verify_Minimum_validation_message_of_App_Description_textbox_as_App_Description_must_be_at_least_characters(
			int arg1) throws Throwable {
		appBuilderObj.verifyAppDescptTxtBoxMinMsg();
	}

	@Then("^: Verify that color of App Description textbox Minimum validation message should be red$")
	public void verify_that_color_of_App_Description_textbox_Minimum_validation_message_should_be_red()
			throws Throwable {
		appBuilderObj.verifyAppDescptTxtBoxMinMsgColor();
	}

	@Given("^: Click on App Description textbox and Enter App Description as value more than (\\d+) characters$")
	public void click_on_App_Description_textbox_and_Enter_App_Description_as_value_more_than_characters(int arg1)
			throws Throwable {
		appBuilderObj.appDescptTxtBoxSendkeys(excelData(5, 2));
	}

	@Then("^: Verify Maximum validation message of App Description textbox as App Description cannot be more than (\\d+) characters\\.$")
	public void verify_Maximum_validation_message_of_App_Description_textbox_as_App_Description_cannot_be_more_than_characters(
			int arg1) throws Throwable {
		appBuilderObj.verifyAppDescptTxtBoxMaxMsg();
	}

	@Then("^: Verify that color of App Description textbox Maximum validation message should be red$")
	public void verify_that_color_of_App_Description_textbox_Maximum_validation_message_should_be_red()
			throws Throwable {
		appBuilderObj.verifyAppDescptTxtBoxMaxMsgColor();
	}

	@Given("^: Enter Valid Value in App Description textbox$")
	public void enter_Valid_Value_in_App_Description_textbox() throws Throwable {
		appBuilderObj.appDescptTxtBoxSendkeys(excelData(6, 2));
	}

	@Then("^: Verify that system should not display validation message when a user enter Valid App Description$")
	public void verify_that_system_should_not_display_validation_message_when_a_user_enter_Valid_App_Description()
			throws Throwable {
		appBuilderObj.verifyAppDescptTxtBoxNoMsg();
	}

	@Given("^: Select URL Radio button$")
	public void select_URL_Radio_button() throws Throwable {
		appBuilderObj.URLRadioClick();
	}

	@Given("^: Click on the URL textbox and Enter URL as abc$")
	public void click_on_the_URL_textbox_and_Enter_URL_as_abc() throws Throwable {
		appBuilderObj.URLTextBoxSendkeys(excelData(7, 3));
	}

	@Then("^: Verify that system display invalid validation for URL as Not a valid URL, Don't forget to use http:// or https://$")
	public void verify_that_system_display_invalid_validation_for_URL_as_Not_a_valid_URL_Don_t_forget_to_use_http_or_https()
			throws Throwable {
		appBuilderObj.verifyURLTextBoxInvalidMsg();
	}

	@Then("^: Verify that Color of invalid Validation message color should be red$")
	public void verify_that_Color_of_invalid_Validation_message_color_should_be_red() throws Throwable {
		appBuilderObj.verifyURLTextBoxInvalidMsgColor();
	}

	@Given("^: Click on the URL textbox and Enter Valid URL$")
	public void click_on_the_URL_textbox_and_Enter_Valid_URL() throws Throwable {
		appBuilderObj.URLTextBoxSendkeys(excelData(8, 3));
	}

	@Then("^: Verify that system should not display Validation message when a user enter valid URL$")
	public void verify_that_system_should_not_display_Validation_message_when_a_user_enter_valid_URL()
			throws Throwable {
		appBuilderObj.verifyURLTextBoxNoMsg();
	}

	@Given("^: Select Upload Document Radio button$")
	public void select_Upload_Document_Radio_button() throws Throwable {
		appBuilderObj.UploadDocumentRadioClick();
	}

	@Then("^: Verify that system should display Remove Validation message as Please remove the entered URL\\.$")
	public void verify_that_system_should_display_Remove_Validation_message_as_Please_remove_the_entered_URL()
			throws Throwable {
		appBuilderObj.verifyURLTextBoxRemoveMsg();

	}

	@Given("^: Select Send Email Alert when Record result is a failure checkbox$")
	public void select_Send_Email_Alert_when_Record_result_is_a_failure_checkbox() throws Throwable {
		appBuilderObj.sendEmailChkboxClick();
	}

	@Then("^: Verify that system should display Email Address textbox when a user select Send Email Alert when Record result is a failure checkbox$")
	public void verify_that_system_should_display_Email_Address_textbox_when_a_user_select_Send_Email_Alert_when_Record_result_is_a_failure_checkbox()
			throws Throwable {
		appBuilderObj.verifySendEmailTxtBoxAppear();
	}

	@Given("^: Deselect Send Email Alert when Record result is a failure checkbox$")
	public void deselect_Send_Email_Alert_when_Record_result_is_a_failure_checkbox() throws Throwable {
		appBuilderObj.sendEmailChkboxClickUnchk();
	}

	@Then("^: Verify that system should not display Email textbox when a user deselect Send Email Alert when Record result is a failure checkbox$")
	public void verify_that_system_should_not_display_Email_textbox_when_a_user_deselect_Send_Email_Alert_when_Record_result_is_a_failure_checkbox()
			throws Throwable {
		appBuilderObj.verifySendEmailTxtBoxDisappear();
	}

	@Then("^: Verify that system should display information message as \"([^\"]*)\" when a user select Send Email Alert when Record result is a failure checkbox$")
	public void verify_that_system_should_display_information_message_as_when_a_user_select_Send_Email_Alert_when_Record_result_is_a_failure_checkbox(
			String arg1) throws Throwable {
		appBuilderObj.verifySendEmailInfoMsg();
	}

	@Then("^: Verify that color of Email Information message should be blue$")
	public void verify_that_color_of_Email_Information_message_should_be_blue() throws Throwable {
		appBuilderObj.verifySendEmailInfoMsgColor();
	}

	@Given("^: Click on the Email Address textbox and Press \"([^\"]*)\" key$")
	public void click_on_the_Email_Address_textbox_and_Press_key(String arg1) throws Throwable {
		appBuilderObj.sendEmailTxtbox.sendKeys(Keys.TAB);
	}

	@Then("^: Verify that system should display required message for Email Address textbox as Please enter at least one Email Address\\.$")
	public void verify_that_system_should_display_required_message_for_Email_Address_textbox_as_Please_enter_at_least_one_Email_Address()
			throws Throwable {
		appBuilderObj.verifySendEmailReqMsg();
	}

	@Given("^: Click on the Email Address textbox and Enter Email Address as abc$")
	public void click_on_the_Email_Address_textbox_and_Enter_Email_Address_as_abc() throws Throwable {
		appBuilderObj.sendEmailTxtboxSendkeys(excelData(9, 4));
	}

	@Then("^: Verify that Invalid email Address validation message should display as Invalid Email Address\\.$")
	public void verify_that_Invalid_email_Address_validation_message_should_display_as_Invalid_Email_Address()
			throws Throwable {
		appBuilderObj.verifySendEmailInvalidMsg();
	}

	@Given("^: Click on the Email Address textbox and Enter Valid Email Address$")
	public void click_on_the_Email_Address_textbox_and_Enter_Valid_Email_Address() throws Throwable {
		appBuilderObj.sendEmailTxtboxSendkeys(excelData(10, 4));
	}

	@Then("^: Verify that system should not display validation message when a user enter valid email address$")
	public void verify_that_system_should_not_display_validation_message_when_a_user_enter_valid_email_address()
			throws Throwable {
		appBuilderObj.verifySendEmailNoMsg();
	}

	@Then("^: Verify that Information message for Site selection should be display as Please select at least one option from each site level\\.\" when Yes option is selected$")
	public void verify_that_Information_message_for_Site_selection_should_be_display_as_Please_select_at_least_one_option_from_each_site_level_when_Yes_option_is_selected()
			throws Throwable {
		appBuilderObj.verifySiteSelectionYesInfoMsg();
	}

	@Then("^: Verify that Color of Information message should be blue for Yes option of Site Selection$")
	public void verify_that_Color_of_Information_message_should_be_blue_for_Yes_option_of_Site_Selection()
			throws Throwable {
		appBuilderObj.verifySiteSelectionYesInfoMsgColor();
	}

	@Given("^: Select No option form Does this app require site selections\\? from dropdown$")
	public void select_No_option_form_Does_this_app_require_site_selections_from_dropdown() throws Throwable {
		appBuilderObj.SelectionNoOptiClick();
	}

	@Then("^: Verify that system should display Information Message for NO option as Please select default option from each site level\\. They will be stored in the database for each app\\.$")
	public void verify_that_system_should_display_Information_Message_for_NO_option_as_Please_select_default_option_from_each_site_level_They_will_be_stored_in_the_database_for_each_app()
			throws Throwable {
		appBuilderObj.verifySiteSelectionNoInfoMsg();
	}

	@Then("^: Verify that Color of Information message should be blue for No option of Site Selection$")
	public void verify_that_Color_of_Information_message_should_be_blue_for_No_option_of_Site_Selection()
			throws Throwable {
		appBuilderObj.verifySiteSelectionNoInfoMsgColor();
	}

	@Then("^: Verify that Required validation Message should be display as At least one site is required from each level\\. when a user select No option$")
	public void verify_that_Required_validation_Message_should_be_display_as_At_least_one_site_is_required_from_each_level_when_a_user_select_No_option()
			throws Throwable {
		appBuilderObj.verifySiteSelectionReqMsg();
	}

	@Then("^: Verify that Color of Required validation message should be red for No option of Site Selection$")
	public void verify_that_Color_of_Required_validation_message_should_be_red_for_No_option_of_Site_Selection()
			throws Throwable {
		appBuilderObj.verifySiteSelectionReqMsgColor();
	}

	@Given("^: Click on Header Message for Print textbox and Enter less than (\\d+) characters$")
	public void click_on_Header_Message_for_Print_textbox_and_Enter_less_than_characters(int arg1) throws Throwable {
		appBuilderObj.defaultHeaderMsgTxtBoxSendkeys(excelData(11, 5));
	}

	@Then("^: Verify that system should display Minimum validation of Header Message for Print as \"([^\"]*)\"$")
	public void verify_that_system_should_display_Minimum_validation_of_Header_Message_for_Print_as(String arg1)
			throws Throwable {
		appBuilderObj.verifyDefaultHeaderMinMsg();
	}

	@Then("^: Verify that Color of Minimum validation message should be red for Header Message for Print textbox$")
	public void verify_that_Color_of_Minimum_validation_message_should_be_red_for_Header_Message_for_Print_textbox()
			throws Throwable {
		appBuilderObj.verifydefaultHeaderMinMsgColor();
	}

	@Given("^: Click on Header Message for Print textbox and Enter more than (\\d+) characters$")
	public void click_on_Header_Message_for_Print_textbox_and_Enter_more_than_characters(int arg1) throws Throwable {
		appBuilderObj.defaultHeaderMsgTxtBoxSendkeys(excelData(12, 5));
	}

	@Then("^: Verify that system should display Maximum validation message as Header Message for Print cannot be more than (\\d+) characters\\.$")
	public void verify_that_system_should_display_Maximum_validation_message_as_Header_Message_for_Print_cannot_be_more_than_characters(
			int arg1) throws Throwable {
		appBuilderObj.verifyDefaultHeaderMaxMsg();
	}

	@Then("^: Verify that Color of Maximum validation message should be red for Header Message for Print textbox$")
	public void verify_that_Color_of_Maximum_validation_message_should_be_red_for_Header_Message_for_Print_textbox()
			throws Throwable {
		appBuilderObj.verifyDefaultHeaderMaxMsgColor();
	}

	@Given("^: Click on Header Message for Print textbox and Enter valid data$")
	public void click_on_Header_Message_for_Print_textbox_and_Enter_valid_data() throws Throwable {
		appBuilderObj.defaultHeaderMsgTxtBoxSendkeys(excelData(13, 5));
	}

	@Then("^: Verify that system should not display any validation message when a user enter Valid Value into Header Message for Print textbox$")
	public void verify_that_system_should_not_display_any_validation_message_when_a_user_enter_Valid_Value_into_Header_Message_for_Print_textbox()
			throws Throwable {
		appBuilderObj.verifyDefaultHeaderNoMsg();
	}

	@Given("^: Click on Header Message for Print When App Status is Passed textbox and Enter less than (\\d+) characters$")
	public void click_on_Header_Message_for_Print_When_App_Status_is_Passed_textbox_and_Enter_less_than_characters(
			int arg1) throws Throwable {
		appBuilderObj.passedHeaderMsgTxtBoxSendkeys(excelData(14, 6));
	}

	@Then("^: Verify that system should display Minimum validation of Header Message for Print When App Status is Passed as \"([^\"]*)\"$")
	public void verify_that_system_should_display_Minimum_validation_of_Header_Message_for_Print_When_App_Status_is_Passed_as(
			String arg1) throws Throwable {
		appBuilderObj.verifyPassedHeaderMinMsg();
	}

	@Then("^: Verify that Color of Minimum validation message should be red for Header Message for Print When App Status is Passed textbox$")
	public void verify_that_Color_of_Minimum_validation_message_should_be_red_for_Header_Message_for_Print_When_App_Status_is_Passed_textbox()
			throws Throwable {
		appBuilderObj.verifyPassedHeaderMinMsgColor();
	}

	@Given("^: Click on Header Message for Print When App Status is Passed textbox and Enter more than (\\d+) characters$")
	public void click_on_Header_Message_for_Print_When_App_Status_is_Passed_textbox_and_Enter_more_than_characters(
			int arg1) throws Throwable {
		appBuilderObj.passedHeaderMsgTxtBoxSendkeys(excelData(15, 6));
	}

	@Then("^: Verify that system should display Maximum validation message as Header Message for Print When App Status is Passed cannot be more than (\\d+) characters\\.$")
	public void verify_that_system_should_display_Maximum_validation_message_as_Header_Message_for_Print_When_App_Status_is_Passed_cannot_be_more_than_characters(
			int arg1) throws Throwable {
		appBuilderObj.verifyPassedHeaderMaxMsg();
	}

	@Then("^: Verify that Color of Maximum validation message should be red for Header Message for Print When App Status is Passed textbox$")
	public void verify_that_Color_of_Maximum_validation_message_should_be_red_for_Header_Message_for_Print_When_App_Status_is_Passed_textbox()
			throws Throwable {
		appBuilderObj.verifyPassedHeaderMaxMsgColor();
	}

	@Given("^: Click on Header Message for Print When App Status is Passed textbox and Enter valid data$")
	public void click_on_Header_Message_for_Print_When_App_Status_is_Passed_textbox_and_Enter_valid_data()
			throws Throwable {
		appBuilderObj.passedHeaderMsgTxtBoxSendkeys(excelData(16, 6));
	}

	@Then("^: Verify that system should not display any validation message when a user enter Valid Value into Header Message for Print When App Status is Passed textbox$")
	public void verify_that_system_should_not_display_any_validation_message_when_a_user_enter_Valid_Value_into_Header_Message_for_Print_When_App_Status_is_Passed_textbox()
			throws Throwable {
		appBuilderObj.verifyPassedHeaderNoMsg();
	}

	@Given("^: Click on Header Message for Print When App Status is Failed textbox and Enter less than (\\d+) characters$")
	public void click_on_Header_Message_for_Print_When_App_Status_is_Failed_textbox_and_Enter_less_than_characters(
			int arg1) throws Throwable {
		appBuilderObj.failedHeaderMsgTxtBoxSendkeys(excelData(17, 7));

	}

	@Then("^: Verify that system should display Minimum validation of Header Message for Print When App Status is Failed as Header Message for Print when App Status is Failed must be at least (\\d+) characters\\.$")
	public void verify_that_system_should_display_Minimum_validation_of_Header_Message_for_Print_When_App_Status_is_Failed_as_Header_Message_for_Print_when_App_Status_is_Failed_must_be_at_least_characters(
			int arg1) throws Throwable {
		appBuilderObj.verifyFailedHeaderMinMsg();
	}

	@Then("^: Verify that Color of Minimum validation message should be red for Header Message for Print When App Status is Failed textbox$")
	public void verify_that_Color_of_Minimum_validation_message_should_be_red_for_Header_Message_for_Print_When_App_Status_is_Failed_textbox()
			throws Throwable {
		appBuilderObj.verifyFailedHeaderMinMsgColor();
	}

	@Given("^: Click on Header Message for Print When App Status is Failed textbox and Enter more than (\\d+) characters$")
	public void click_on_Header_Message_for_Print_When_App_Status_is_Failed_textbox_and_Enter_more_than_characters(
			int arg1) throws Throwable {
		appBuilderObj.failedHeaderMsgTxtBoxSendkeys(excelData(18, 7));

	}

	@Then("^: Verify that system should display Maximum validation message as Header Message for Print When App Status is Failed cannot be more than (\\d+) characters\\.$")
	public void verify_that_system_should_display_Maximum_validation_message_as_Header_Message_for_Print_When_App_Status_is_Failed_cannot_be_more_than_characters(
			int arg1) throws Throwable {
		appBuilderObj.verifyFailedHeaderMaxMsg();
	}

	@Then("^: Verify that Color of Maximum validation message should be red for Header Message for Print When App Status is Failed textbox$")
	public void verify_that_Color_of_Maximum_validation_message_should_be_red_for_Header_Message_for_Print_When_App_Status_is_Failed_textbox()
			throws Throwable {
		appBuilderObj.verifyFailedHeaderMaxMsgColor();
	}

	@Given("^: Click on Header Message for Print When App Status is Failed textbox and Enter valid data$")
	public void click_on_Header_Message_for_Print_When_App_Status_is_Failed_textbox_and_Enter_valid_data()
			throws Throwable {
		appBuilderObj.failedHeaderMsgTxtBoxSendkeys(excelData(19, 7));

	}

	@Then("^: Verify that system should not display any validation message when a user enter Valid Value into Header Message for Print When App Status is Failed textbox$")
	public void verify_that_system_should_not_display_any_validation_message_when_a_user_enter_Valid_Value_into_Header_Message_for_Print_When_App_Status_is_Failed_textbox()
			throws Throwable {
		appBuilderObj.verifyFailedHeaderNoMsg();
	}

	@Given("^: Select Peer or Second Review checkbox$")
	public void select_Peer_or_Second_Review_checkbox() throws Throwable {
		appBuilderObj.reviewWorkflowChkBoxClick();
	}

	@Then("^: Verify that system should display Select Role dropdown when a user select Peer or Second Review checkbox$")
	public void verify_that_system_should_display_Select_Role_dropdown_when_a_user_select_Peer_or_Second_Review_checkbox()
			throws Throwable {
		appBuilderObj.verifyReviewWorkflowOPtiVisible();
	}

	@Given("^: Deselect Peer or Second Review checkbox$")
	public void deselect_Peer_or_Second_Review_checkbox() throws Throwable {
		appBuilderObj.reviewWorkflowChkBoxClick();
	}

	@Then("^: Verify that system should not display Select Role dropdown when a user unselect the Peer or Second Review checkbox$")
	public void verify_that_system_should_not_display_Select_Role_dropdown_when_a_user_unselect_the_Peer_or_Second_Review_checkbox()
			throws Throwable {
		appBuilderObj.verifyReviewWorkflowOPtiInvisible();
	}

	@Given("^: Add All Required App Details in Basic Tab$")
	public void add_All_Required_App_Details_in_Basic_Tab() throws Throwable {
		String mdlName = excelData(20, 8), appTitle = excelData(20, 1), siteName = excelData(20, 9);

		appBuilderObj.addBasicTabDetails(mdlName, appTitle, siteName);
	}
	
	@Given("^: Click on View Audit Trail link of first record from the Audit Trail column on app builder listing page$")
	public void click_on_View_Audit_Trail_link_of_first_record_from_the_Audit_Trail_column_on_app_builder_listing_page() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    appBuilderObj.viewFirstAuditTrailClick();
	}

	@Then("^: Verify that system should redirect to the Attribute tab when a user Adds All required details into Basic Tab$")
	public void verify_that_system_should_redirect_to_the_Attribute_tab_when_a_user_Adds_All_required_details_into_Basic_Tab()
			throws Throwable {
		appBuilderObj.verifyAttributeDetailsLabel(excelData(20, 1));
	}

	@Given("^: Click on Continue button of basic tab$")
	public void click_on_Continue_button() throws Throwable {
		appBuilderObj.continueBasicBtnClick();
	}

	@Then("^: Verify that (\\d+) ATTRIBUTES text should be display in Attribute Tab$")
	public void verify_that_ATTRIBUTES_text_should_be_display_in_Attribute_Tab(int arg1) throws Throwable {
		appBuilderObj.verifyAttributeHeaderTabLabel();
	}

	@Then("^: Verify that Color of Attribute tab should be green$")
	public void verify_that_Color_of_Attribute_tab_should_be_green() throws Throwable {
		appBuilderObj.verifyAttributeHeaderTabColor();
	}

	@Then("^: Verify that Attribute Details label should be displayed in top left side of Attribute Tab$")
	public void verify_that_Attribute_Details_label_should_be_displayed_in_top_left_side_of_Attribute_Tab()
			throws Throwable {
		appBuilderObj.verifyAttributeDetailsLabel(excelData(20, 1));
	}

	@Then("^: Verify that a system should display the Current Version of the App in top right side of Attribute Tab$")
	public void verify_that_a_system_should_display_the_Current_Version_of_the_App_in_top_right_side_of_Attribute_Tab()
			throws Throwable {
		appBuilderObj.verifyCurreVersionLabel();
	}
	@Then("^: Verify that Color of Current Version message should be blue in Attribute tab$")
	public void verify_that_Color_of_Current_Version_message_should_be_blue_in_Attribute_tab() throws Throwable {
		appBuilderObj.verifyCurreVersionColor();
	}

	@Then("^: Verify that all next tabs should displayed in Disable mode when a user access Attribute tab$")
	public void verify_that_all_next_tabs_should_displayed_in_Disable_mode_when_a_user_access_Attribute_tab() throws Throwable {
	    appBuilderObj.verifyDisabledTab_Attribute();
	}

	@Then("^: Verify that system should display information message in Attribute tab as Define Attributes for App\\. Minimum (\\d+) Attributes and/or Entities are required\\.$")
	public void verify_that_system_should_display_information_message_in_Attribute_tab_as_Define_Attributes_for_App_Minimum_Attributes_and_or_Entities_are_required(
			int arg1) throws Throwable {
		appBuilderObj.verifyAttributeInfoMsg();
	}

	@Then("^: Verify that Color of Information message in Attribute tab should be blue$")
	public void verify_that_Color_of_Information_message_in_Attribute_tab_should_be_blue() throws Throwable {
		appBuilderObj.verifyAttributeInfoMsgColor();
	}

	@Then("^: Verify that Add Attribute button should not displayed initially to the user when Attribute tab is loaded$")
	public void verify_that_Add_Attribute_button_should_not_displayed_initially_to_the_user_when_Attribute_tab_is_loaded()
			throws Throwable {
		appBuilderObj.verifyInitiallyAddAttribute();
	}

	@Given("^: Select Attribute from Select Attribute dropdown$")
	public void select_Attribute_from_Select_Attribute_dropdown() throws Throwable {
		appBuilderObj.selectAttributeDrpDwn(excelData(21, 10));
	}

	@Then("^: Verify that system should display Add Attribute button only when a user select any Attribute from dropdown$")
	public void verify_that_system_should_display_Add_Attribute_button_only_when_a_user_select_any_Attribute_from_dropdown()
			throws Throwable {
		appBuilderObj.verifyAttributeAddBtnVisible();
	}

	@Given("^: Click on Add Attribute button$")
	public void click_on_Add_Attribute_button() throws Throwable {
		appBuilderObj.addAttributeBtnClick();
	}

	@Then("^: Verify that Add Attribute button is clickable or not$")
	public void verify_that_Add_Attribute_button_is_clickable_or_not() throws Throwable {
		appBuilderObj.verifyAttributeAddBtnClickable();
	}

	@Then("^: Verify that Color of Add Attribute button should be blue$")
	public void verify_that_Color_of_Add_Attribute_button_should_be_blue() throws Throwable {
		appBuilderObj.verifyAttributeAddBtnColor();
	}

	@Given("^: Click on Remove button$")
	public void click_on_Remove_button() throws Throwable {
		appBuilderObj.attributeRemoveBtnClick();
	}

	@Then("^: Verify that Selected Attribute should be removed when a user click on the Remove button$")
	public void verify_that_Selected_Attribute_should_be_removed_when_a_user_click_on_the_Remove_button()
			throws Throwable {
		appBuilderObj.verifyAttributeRemoveBtnClickable();
	}

	@Given("^: Add All Required Attribute Details in Attribute tab$")
	public void add_All_Required_Attribute_Details_in_Attribute_tab() throws Throwable {
		appBuilderObj.addAttributeTabDetails();
	}
	
	@Given("^: Click on Continue button of attribute tab$")
	public void click_on_Continue_button_of_attribute_tab() throws Throwable {
		appBuilderObj.continueAttributeBtnClick();

	}
	
	@Then("^: Verify that system should redirect to the Workflow tab when a user Adds All required details into Attribute Tab$")
	public void verify_that_system_should_redirect_to_the_Workflow_tab_when_a_user_Adds_All_required_details_into_Attribute_Tab()
			throws Throwable {
		appBuilderObj.verifyWorkflowDetailsLabel(excelData(20, 1));
	}

	@Then("^: Verify that (\\d+)  WORKFLOW text should be display in Workflow tab$")
	public void verify_that_WORKFLOW_text_should_be_display_in_Workflow_tab(int arg1) throws Throwable {
		appBuilderObj.verifyWorkflowHeaderTabLabel();
	}

	@Then("^: Verify that Color of Workflow tab should be green$")
	public void verify_that_Color_of_Workflow_tab_should_be_green() throws Throwable {
		appBuilderObj.verifyWorkflowHeaderTabColor();
	}

	@Then("^: Verify that Workflow Details label should be displayed in top left side of Workflow Tab$")
	public void verify_that_Workflow_Details_label_should_be_displayed_in_top_left_side_of_Workflow_Tab()
			throws Throwable {
		appBuilderObj.verifyWorkflowDetailsLabel(excelData(20, 1));
	}

	@Then("^: Verify that a system should display the Current Version of the App in top right side of Workflow Tab$")
	public void verify_that_a_system_should_display_the_Current_Version_of_the_App_in_top_right_side_of_Workflow_Tab()
			throws Throwable {
		appBuilderObj.verifyCurreVersionLabel();
	}

	@Then("^: Verify that Color of Current Version message should be blue in Workflow tab$")
	public void verify_that_Color_of_Current_Version_message_should_be_blue_in_Workflow_tab() throws Throwable {
		appBuilderObj.verifyCurreVersionColor();
	}

	@Then("^: Verify that all next tabs should displayed in Disable mode when a user access Workflow tab$")
	public void verify_that_all_next_tabs_should_displayed_in_Disable_mode_when_a_user_access_Workflow_tab()
			throws Throwable {
		appBuilderObj.verifyDisabledTab_Workflow();
	}

	@Then("^: Verify that system should display Entity Workflow label below Workflow Details label$")
	public void verify_that_system_should_display_Entity_Workflow_label_below_Workflow_Details_label()
			throws Throwable {
		appBuilderObj.verifyEntityWorkflowLable();
	}

	@Then("^: Verify that system should display Procedure Workflow label below Entity Workflow section$")
	public void verify_that_system_should_display_Procedure_Workflow_label_below_Entity_Workflow_section()
			throws Throwable {
		appBuilderObj.verifyProcedureWorkflowLable();
	}

	@Then("^: Verify that system should not display Add Entity button initially when the page is loaded$")
	public void verify_that_system_should_not_display_Add_Entity_button_initially_when_the_page_is_loaded()
			throws Throwable {
		appBuilderObj.verifyAddEntityInvisible();
	}

	@Given("^: Type Entity name in Search and Select Entity dropdown$")
	public void type_Entity_name_in_Search_and_Select_Entity_dropdown() throws Throwable {
		appBuilderObj.selectEntitySendkeys(excelData(22, 11));
	}

	@Then("^: Verify that system should display Add Entity button when user Type Valid Entity name in Search and Select Entity dropdown$")
	public void verify_that_system_should_display_Add_Entity_button_when_user_Type_Valid_Entity_name_in_Search_and_Select_Entity_dropdown()
			throws Throwable {
		appBuilderObj.verifyAddEntityVisible();
	}

	@Given("^: Click on Add Entity button$")
	public void click_on_Add_Entity_button() throws Throwable {
		appBuilderObj.addEntityBtnClick();
	}

	@Then("^: Verify that Add Entity button is clickable or not$")
	public void verify_that_Add_Entity_button_is_clickable_or_not() throws Throwable {
		appBuilderObj.verifyAddEntityBtnClickable();
	}

	@Then("^: Verify that Color of Add Entity button should be soft cyan$")
	public void verify_that_Color_of_Add_Entity_button_should_be_soft_cyan() throws Throwable {
		appBuilderObj.verifyEntityAddBtnColor();
	}

	@Given("^: Click on Filter button$")
	public void click_on_Filter_button() throws Throwable {
		appBuilderObj.entityFilterBtnClick();
	}

	@Then("^: Verify that system should redirect to the Apply Entity Filter section when a user click on the Filter button$")
	public void verify_that_system_should_redirect_to_the_Apply_Entity_Filter_section_when_a_user_click_on_the_Filter_button()
			throws Throwable {
		appBuilderObj.verifyApplyEntityFilterLabel();
	}

	@Given("^: Click on Add Rule button$")
	public void click_on_Add_Rule_button() throws Throwable {
		appBuilderObj.entityAddRuleBtnClick();
	}

	@Then("^: Verify that system should redirect to the Define Rule section when a user click on the Add Rule button$")
	public void verify_that_system_should_redirect_to_the_Define_Rule_section_when_a_user_click_on_the_Add_Rule_button()
			throws Throwable {
		appBuilderObj.verifyDefineRuleLabelLabel();
	}

	@Given("^: Click on Show link$")
	public void click_on_Show_link() throws Throwable {
		appBuilderObj.showLinkClick();
	}

	@Given("^: Click on Hide link$")
	public void click_on_Hide_link() throws Throwable {
		appBuilderObj.hideLinkClick();
	}

	@Then("^: Verify that system should expand the Entity Details when a user click on the Show link$")
	public void verify_that_system_should_expand_the_Entity_Details_when_a_user_click_on_the_Show_link()
			throws Throwable {
		appBuilderObj.verifyShowLinkClickable();
	}

	@Then("^: Verify that system should collapse the Entity Details when a user click on the Hide link$")
	public void verify_that_system_should_collapse_the_Entity_Details_when_a_user_click_on_the_Hide_link()
			throws Throwable {
		appBuilderObj.verifyHideLinkClickable();
	}

	@Then("^: Verify that Add Procedure button should not displayed initially when the page is loaded$")
	public void verify_that_Add_Procedure_button_should_not_displayed_initially_when_the_page_is_loaded()
			throws Throwable {
		appBuilderObj.verifyAddProcedureInvisible();
	}

//==============chirag ==========================

	@Given("^: Add all Required Workflow Details in Workflow tab$")
	public void add_all_Required_Workflow_Details_in_Workflow_tab() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    appBuilderObj.enterAllRequiredDataWorkflow(excelData(23, 11), excelData(23, 12));
	}

	@Given("^: Add all required App Acceptance Criteria in Acceptance tab$")
	public void add_all_required_App_Acceptance_Criteria_in_Acceptance_tab() throws Throwable {
	    
	    appBuilderObj.selectProcedureAcceptanceTab();
	}

	@Given("^: Click on Publish & Continue button$")
	public void click_on_Publish_Continue_button() throws Throwable {
	    
	    appBuilderObj.clickPublishContinueBtnPreviewTab();
	}

	@Then("^: Verify that user is allowed to Add New app$")
	public void verify_that_user_is_allowed_to_Add_New_app() throws Throwable {
	    
	    appBuilderObj.verifyAddNewAppFunctionality();
	}

	@Given("^: Click on Continue button of workflow tab$")
	public void click_on_Continue_button_of_workflow_tab() throws Throwable {
	    
	    appBuilderObj.clickContinueBtnWorkflowTab();
	}

	@Given("^: Click on Continue button of acceptance tab$")
	public void click_on_Continue_button_of_acceptance_tab() throws Throwable {
	    
	    appBuilderObj.clickContinueBtnAcceptanceTab();
	}

	@Given("^: Click on Save button of permission tab$")
	public void click_on_Save_button_of_prmission_tab() throws Throwable {
	    
	    appBuilderObj.clickSaveBtnPermissionTab();
	}
	
	@Then("^: Verify that system should redirect to the Apps Listing Screen when a user click on the Save button$")
	public void verify_that_system_should_redirect_to_the_Apps_Listing_Screen_when_a_user_click_on_the_Save_button() throws Throwable {
	    
	    appBuilderObj.verifySaveBtnNavigateAppListings();
	}
	
	@Then("^: Verify that system should display Correct App Name label in Permissions tab$")
	public void verify_that_system_should_display_Correct_App_Name_label_in_Permissions_tab() throws Throwable {
	    
	    appBuilderObj.verifyAppNameInPermissionTab();
	}

	@Then("^: Verify that system should display information as \"([^\"]*)\" in Permissions tab$")
	public void verify_that_system_should_display_information_as_in_Permissions_tab(String arg1) throws Throwable {
	    
	    appBuilderObj.verifyDefinedPermissionForApp();
	}

	@Then("^: Verify that Color of information message in Permissions tab should be blue$")
	public void verify_that_Color_of_information_message_in_Permissions_tab_should_be_blue() throws Throwable {
	    
	    appBuilderObj.verifyDefinedPermissionForAppColor();
	}
	
	@Then("^: Verify that system should display the Current Version of the App in top right side of Preview Tab$")
	public void verify_that_system_should_display_the_Current_Version_of_the_App_in_top_right_side_of_Preview_Tab() throws Throwable {
	    
	    appBuilderObj.verifyCurrentVersionOfPreviewTab();
	}

	@Then("^: Verify that system should display Correct App name label in Preview tab$")
	public void verify_that_system_should_display_Correct_App_name_label_in_Preview_tab() throws Throwable {
	    
	    appBuilderObj.verifyAppNameInPreviewTab();
	}

	@Then("^: Verify that system should redirect to the Permissions tab when a user click on Publish & Continue button$")
	public void verify_that_system_should_redirect_to_the_Permissions_tab_when_a_user_click_on_Publish_Continue_button() throws Throwable {
	    
	    appBuilderObj.verifyPermissionTabNavigation();
	}

	@Then("^: Verify that system should display '(\\d+)  Permissions' text in Permissions tab$")
	public void verify_that_system_should_display_Permissions_text_in_Permissions_tab(int arg1) throws Throwable {
	    
	    appBuilderObj.verifyPermissionTabLabel();
	}

	@Then("^: Verify that Color of Permissions tab should be green$")
	public void verify_that_Color_of_Permissions_tab_should_be_green() throws Throwable {
	    
	    appBuilderObj.verifyPermissionTabLabelColor();
	}

	@Then("^: Verify that system should display '(\\d+) PREVIEW' in Preview tab$")
	public void verify_that_system_should_display_PREVIEW_in_Preview_tab(int arg1) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    appBuilderObj.verifyPreviewTabLabel();
	}

	@Then("^: Verify that Color of Preview tab should be green$")
	public void verify_that_Color_of_Preview_tab_should_be_green() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    appBuilderObj.verifyPreviewTabLabelColor();
	}

	@Then("^: Verify that all next tabs should displayed in Disable mode when a user access Preview tab$")
	public void verify_that_all_next_tabs_should_displayed_in_Disable_mode_when_a_user_access_Preview_tab() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    appBuilderObj.verifyPermissionTabDisabled();
	}

	@Then("^: Verify that system should display Preview tab information message as \"([^\"]*)\"$")
	public void verify_that_system_should_display_Preview_tab_information_message_as(String arg1) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    appBuilderObj.verifyPreviewTabInformationMsg();
	}

	@Then("^: Verify that Color of information message for Preview tab should be blue$")
	public void verify_that_Color_of_information_message_for_Preview_tab_should_be_blue() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    appBuilderObj.verifyPreviewTabInformationMsgColor();
	}
	
	@Given("^: Select Procedure from Select Procedure dropdown$")
	public void select_Procedure_from_Select_Procedure_dropdown() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    appBuilderObj.selectProcedureDrpDwnAcceptance();
	}

	@Given("^: Click on Add Procedure button$")
	public void click_on_Add_Procedure_button() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    appBuilderObj.selectProcedureAcceptanceTab();
	}
	
	@Given("^: Click on Remove button of Acceptance tab$")
	public void click_on_Remove_button_of_Accpetance_tab() throws Throwable {
		appBuilderObj.clickRemoveProcedureAcceptanceTab();
	}

	@Then("^: Verify that system should remove the Procedure when a user click on the Remove button$")
	public void verify_that_system_should_remove_the_Procedure_when_a_user_click_on_the_Remove_button() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    appBuilderObj.verifyRemoveProcedureAcceptanceTabFunctionality();
	}

	@Then("^: Verify that system should redirect to the Preview tab when a user Adds All required details into Acceptance Tab$")
	public void verify_that_system_should_redirect_to_the_Preview_tab_when_a_user_Adds_All_required_details_into_Acceptance_Tab() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    appBuilderObj.verifyNavigateToPreviewTabFunctionality();
	}
	
	@Then("^: Verify that system should display Add Procedure button when a user select Procedure from Select Procedure dropdown$")
	public void verify_that_system_should_display_Add_Procedure_button_when_a_user_select_Procedure_from_Select_Procedure_dropdown() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    appBuilderObj.verifyAddProcedureBtnVisibleAcceptanceTab();
	}

	@Then("^: Verify that Add Procedure button is clickable or not$")
	public void verify_that_Add_Procedure_button_is_clickable_or_not() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    appBuilderObj.verifyAddProcedureBtnFunctionalAcceptanceTab();
	}

	@Then("^: Verify that Color of Add Procedure button should be blue$")
	public void verify_that_Color_of_Add_Procedure_button_should_be_blue() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    appBuilderObj.verifyAddProcedureBtnVisibleAcceptanceTabColor();
	}
	
	@Then("^: Verify that system should display information message as \"([^\"]*)\"$")
	public void verify_that_system_should_display_information_message_as(String arg1) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    appBuilderObj.verifyKeyAttributesInformationMsg();
	}

	@Then("^: Verify that Color of Acceptance Attribute information message should be blue$")
	public void verify_that_Color_of_Acceptance_Attribute_information_message_should_be_blue() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    appBuilderObj.verifyKeyAttributesInformationMsgColor();
	}

	@Then("^: Verify that system should display Define Pass Criteria\\(s\\) on App label below How do you want to track Unique Records\\? section$")
	public void verify_that_system_should_display_Define_Pass_Criteria_s_on_App_label_below_How_do_you_want_to_track_Unique_Records_section() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    appBuilderObj.verifyDefinePassCriteriaInformationMsgColor();
	}

	@Then("^: Verify that system should display Procedure information message as \"([^\"]*)\"$")
	public void verify_that_system_should_display_Procedure_information_message_as(String arg1) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    appBuilderObj.verifyAddProcedureInformationMsg();
	}

	@Then("^: Verify that Color of Procedure information message should be blue$")
	public void verify_that_Color_of_Procedure_information_message_should_be_blue() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    appBuilderObj.verifyAddProcedureInformationMsgColor();
	}
	
	@Then("^: Verify that system should redirect to the Acceptance tab when a user Adds All required details into Workflow Tab$")
	public void verify_that_system_should_redirect_to_the_Acceptance_tab_when_a_user_Adds_All_required_details_into_Workflow_Tab() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    appBuilderObj.verifyAccpetanceCriteriaRedirection();
	}

	@Then("^: Verify that '(\\d+)   ACCEPTANCE' text should be display in Acceptance tab$")
	public void verify_that_ACCEPTANCE_text_should_be_display_in_Acceptance_tab(int arg1) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    appBuilderObj.verifyAcceptanceTabLabel();
	}

	@Then("^: Verify that Color of Acceptance tab should be green$")
	public void verify_that_Color_of_Acceptance_tab_should_be_green() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    appBuilderObj.verifyAcceptanceTabLabelColor();
	}

	@Then("^: Verify that App Acceptance Criteria label should be displayed in top left side of Acceptance Tab$")
	public void verify_that_App_Acceptance_Criteria_label_should_be_displayed_in_top_left_side_of_Acceptance_Tab() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    appBuilderObj.verifyAcceptanceTabAppLabel();
	}

	@Then("^: Verify that a system should display the Current Version of the App in top right side of Acceptance Tab$")
	public void verify_that_a_system_should_display_the_Current_Version_of_the_App_in_top_right_side_of_Acceptance_Tab() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    appBuilderObj.verifyAcceptanceTabCurrentVersion();
	}

	@Then("^: Verify that Color of Current Version message should be blue in Acceptance tab$")
	public void verify_that_Color_of_Current_Version_message_should_be_blue_in_Acceptance_tab() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    appBuilderObj.verifyAcceptanceTabCurrentVersionColor();
	}

	@Then("^: Verify that all next tabs should displayed in Disable mode when a user access Acceptance tab$")
	public void verify_that_all_next_tabs_should_displayed_in_Disable_mode_when_a_user_access_Acceptance_tab() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    appBuilderObj.verifyPreviewTabDisabled();
	}

	@Then("^: Verify that \"([^\"]*)\" label should be display below App Acceptance Criteria label$")
	public void verify_that_label_should_be_display_below_App_Acceptance_Criteria_label(String arg1) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    appBuilderObj.verifyTrackUniqueRecordsMsg();
	}
	
	@Given("^: Add all Required Workflow Details in Workflow tab for acceptance criteria$")
	public void add_all_Required_Workflow_Details_in_Workflow_tab_for_acceptance_criteria() throws Throwable {
		appBuilderObj.enterAllRequiredDataWorkflow(excelData(24, 11), excelData(24, 12));
	}

	@Given("^: Click on show button of procedure workflow$")
	public void click_on_show_button_of_procedure_workflow() throws Throwable {
	    appBuilderObj.clickShowBtnOfProcedure();
	}

	@Given("^: Click on first procedure acceptance criteria button$")
	public void click_on_first_procedure_acceptance_criteria_button() throws Throwable {
	    appBuilderObj.clickFirstAcceptanceCriteria();
	}

	@Given("^: Select condition dropdown from standrad Acceptance Criteria$")
	public void select_condition_dropdown_from_standrad_Acceptance_Criteria() throws Throwable {
	    appBuilderObj.selectConditionDrpDwnVal();
	}

	@Given("^: Enter value in value textbox$")
	public void enter_value_in_value_textbox() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    
	}

	@Given("^: Click on save button of Acceptance criteria$")
	public void click_on_save_button_of_Acceptance_criteria() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    
	}



	// ==================== santosh ====================
	
	// need do excel code for procedure name
	/*
	 * @Given("^: Type Procedure name in Search and Select Procedure dropdown$")
	 * public void type_Procedure_name_in_Search_and_Select_Procedure_dropdown()
	 * throws Throwable {
	 * appBuilderObj.selectProcedureInAutoComSendkeys(excelData(rowVal, colVal)); }
	 */

	@Then("^: Verify that system should display Add Procedure button when user Type Valid Procedure name in Search and Select Procedure dropdown$")
	public void verify_that_system_should_display_Add_Procedure_button_when_user_Type_Valid_Procedure_name_in_Search_and_Select_Procedure_dropdown()
			throws Throwable {
		appBuilderObj.verifyAddProcedureVisible();
	}

	/*
	 * // @Given("^: Click on Add Procedure button$") // public void
	 * click_on_Add_Procedure_button() throws Throwable { //
	 * appBuilderObj.addProcedureBtnWorkflowClick(); // }
	 */
	/*
	 * @Then("^: Verify that Add Procedure button is clickable or not$") public void
	 * verify_that_Add_Procedure_button_is_clickable_or_not() throws Throwable {
	 * appBuilderObj.verifyAddProcedureBtnClickable(); }
	 */
	
	

}// end
