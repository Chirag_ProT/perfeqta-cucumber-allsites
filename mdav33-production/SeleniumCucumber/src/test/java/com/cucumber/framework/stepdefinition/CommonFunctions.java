package com.cucumber.framework.stepdefinition;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cucumber.framework.helper.Javascript.JavaScriptHelper;
import com.cucumber.framework.helper.PageObject.AttributesPageObject;
import com.cucumber.framework.helper.PageObject.CommonFunctionPageObject;
import com.cucumber.framework.helper.PageObject.LoginPageObject;
import com.cucumber.framework.helper.Wait.WaitHelper;
import com.cucumber.framework.interfaces.IconfigReader;
import com.cucumber.framework.settings.ObjectRepo;
import com.cucumber.framework.utility.Constant;
import com.cucumber.framework.utility.ExcelUtils;
import cucumber.api.java.en.Given;

public class CommonFunctions
{
	public ExcelUtils excel;
	private CommonFunctionPageObject commonFnPageObject;
	private WaitHelper waitHelper;
	private IconfigReader reader;
	public CommonFunctions()
	{
		commonFnPageObject = new CommonFunctionPageObject(ObjectRepo.driver);
		waitHelper = new WaitHelper(ObjectRepo.driver, reader);
	}
	
	@Given("^: Navigate to the URL$")
	public void navigate_to_the_URL() throws Throwable {
		try {
			ObjectRepo.driver.get(ObjectRepo.reader.getWebsite());
			
			Thread.sleep(2000);
			if (commonFnPageObject.btn_login.getText().equalsIgnoreCase("Login Anyway")) {
				System.err.println("Found Non-Chrome Browser: "+commonFnPageObject.btn_login.getText());
				commonFnPageObject.clicktologin();
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("failed to navigate");
		}
	}
	
	/*  And : Enter valid credentials and Click on Login Button */
	@Given("^: Enter valid credentials and Click on Login Button$")
	public void enter_valid_credentials_and_Click_on_Login_Button() throws Throwable {
		ExcelUtils loginExcel= new ExcelUtils();
		Thread.sleep(2000);
		//waitHelper.elementExistAndVisible(commonFnPageObject.username, 60, 5);
		
		WebDriverWait wait = new WebDriverWait(ObjectRepo.driver, 60);
		
		wait.until(ExpectedConditions.visibilityOf(commonFnPageObject.username));
				
		loginExcel.setExcelFile(Constant.Path_TestData + Constant.File_TestData, "Login");
		commonFnPageObject.username.sendKeys(ExcelUtils.readXLSFile("Login", 2, 1));
		commonFnPageObject.password.sendKeys(ExcelUtils.readXLSFile("Login", 1, 2));
		commonFnPageObject.clicktologin();
		Thread.sleep(2000);
		if (ObjectRepo.driver.findElements(By.xpath("//*[@ng-click='vm.Yes({id: vm.adminId})']")).size() != 0) 
		{
			Thread.sleep(1000);
			ObjectRepo.driver.findElement(By.xpath("//*[@ng-click='vm.Yes({id: vm.adminId})']")).click();
			Thread.sleep(4000);
		} 
		if (ObjectRepo.driver.findElements(By.xpath("//span[@id='wootric-x']")).size() != 0) 
		{
			commonFnPageObject.clickDismissIcon();
		}
	}
	
	/* And : Click on Administration Tile */
	@Given("^: Click on Administration Tile$")
	public void click_on_Administration_Tile() throws Throwable {		
		commonFnPageObject.clickAdministrationTile();
	}
	
	
	
	
}
