package com.cucumber.framework.stepdefinition;

import org.openqa.selenium.Keys;
import com.cucumber.framework.helper.PageObject.AttributesPageObject;
import com.cucumber.framework.helper.PageObject.EntitiesPageObject;
import com.cucumber.framework.settings.ObjectRepo;
import com.cucumber.framework.utility.Constant;
import com.cucumber.framework.utility.ExcelUtils;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

public class Entities {

	public ExcelUtils excel;
	public EntitiesPageObject entitiesObj;

	public Entities() {
		entitiesObj = new EntitiesPageObject(ObjectRepo.driver);
	}

	public String excelData(int rowVal, int colVal) throws Exception {
		System.out.println("excelCalling.........");
		excel = new ExcelUtils();
		excel.setExcelFile(Constant.Path_TestData + Constant.File_TestData, "Entities");
		System.err.println();
		return excel.readXLSFile("Entities", rowVal, colVal);
	}

	@Given("^: Click on Entities Tile$")
	public void click_on_Entities_Tile() throws Throwable {
		entitiesObj.clickEntitiesTile();
	}

	@Given("^: Click on \"([^\"]*)\" button of Entities listing screen$")
	public void click_on_button_of_Entities_listing_screen(String arg1) throws Throwable {
		entitiesObj.clickAddNewBtn();
	}

	@Then("^: Verify the page name as Add / Edit Entity$")
	public void verify_the_page_name_as_Add_Edit_Entity() throws Throwable {
		entitiesObj.verifyAddEditTxtLabel();
	}

	@Then("^: Verify the module name as Entities$")
	public void verify_the_module_name_as_Entities() throws Throwable {
		entitiesObj.verifyEntitiesTxtLabel();
	}

	@Then("^: Verify the Current version in Add/Entity screen as \"([^\"]*)\"$")
	public void verify_the_Current_version_in_Add_Entity_screen_as(String arg1) throws Throwable {
		entitiesObj.verifyCurrVrsnTxtLabel();
	}

	@Given("^: Click on Module dropdown$")
	public void click_on_Module_dropdown() throws Throwable {
		entitiesObj.moduleDrpdwnClick();
	}

	@Given("^: Click to Check All option$")
	public void click_to_Check_All_option() throws Throwable {
		entitiesObj.chkAllBtnClick();
	}

	@Then("^: Verify that all the module name should be selected$")
	public void verify_that_all_the_module_name_should_be_selected() throws Throwable {
		entitiesObj.verifyChkAllBtnFun();
	}

	@Given("^: Click on UnCheck All option$")
	public void click_on_UnCheck_All_option() throws Throwable {
		entitiesObj.chkAllBtnClick();
		entitiesObj.unchkAllBtnClick();
	}

	@Then("^: Verify that all the module name should not be selected$")
	public void verify_that_all_the_module_name_should_not_be_selected() throws Throwable {
		entitiesObj.verifyUnchkAllBtnFun();
	}

	@Given("^: Enter the module name for in search box$")
	public void enter_the_module_name_for_in_search_box() throws Throwable {
		entitiesObj.moduleSrchBoxSendkeys(excelData(1, 1));
	}

	@Then("^: Verify that searched module should appear in the Module drop-down$")
	public void verify_that_searched_module_should_appear_in_the_Module_drop_down() throws Throwable {
		entitiesObj.verifyModuleSrchBoxFun(excelData(1, 1));
	}

	@Given("^: Click on Entity Name textbox and press the \"([^\"]*)\" Key$")
	public void click_on_Entity_Name_textbox_and_press_the_Key(String arg1) throws Throwable {
		entitiesObj.entyNameTxtBox.sendKeys(Keys.TAB);
	}

	@Then("^: Verify the validation message as Entity Name is required\\.$")
	public void verify_the_validation_message_as_Entity_Name_is_required() throws Throwable {
		entitiesObj.verifyEntyNameTxtBoxRequiValid();
	}

	@Given("^: Enter Entity Name value Less than (\\d+) chracters$")
	public void enter_Entity_Name_value_Less_than_chracters(int arg1) throws Throwable {
		entitiesObj.entyNameTxtBoxSendkeys(excelData(2, 2));
	}

	@Then("^: Verify the validation message as Entity Name must be at least (\\d+) characters\\.$")
	public void verify_the_validation_message_as_Entity_Name_must_be_at_least_characters(int arg1) throws Throwable {
		entitiesObj.verifyEntyNameTxtBoxMinValid();
	}

	@Given("^: Enter Entity Name value more than (\\d+) characters$")
	public void enter_Entity_Name_value_more_than_characters(int arg1) throws Throwable {
		entitiesObj.entyNameTxtBoxSendkeys(excelData(3, 2));
	}

	@Then("^: Verify validation message Entity Name cannot be more than (\\d+) characters\\.$")
	public void verify_validation_message_Entity_Name_cannot_be_more_than_characters(int arg1) throws Throwable {
		entitiesObj.verifyEntyNameTxtBoxMaxValid();
	}

	@Given("^: Enter a valid Entity Name$")
	public void enter_a_valid_Entity_Name() throws Throwable {
		entitiesObj.entyNameTxtBoxSendkeys(excelData(5, 2));
	}

	@Then("^: Veridy system should accept the entity name value without any validation message$")
	public void veridy_system_should_accept_the_entity_name_value_without_any_validation_message() throws Throwable {
		entitiesObj.verifyEntyNameTxtBoxNoValid();
	}

	@Given("^: Enter Duplicate Entity Name$")
	public void enter_Duplicate_Entity_Name() throws Throwable {
		entitiesObj.entyNameTxtBoxSendkeys(excelData(4, 2));
	}

	@Then("^: Verify the Unique validation message for the Entity Name Textbox$")
	public void verify_the_Unique_validation_message_for_the_Entity_Name_Textbox() throws Throwable {
		entitiesObj.verifyEntyNameTxtBoxUniqValid();

	}
	@Given("^: Click on the Entity Tag text box and press the \"([^\"]*)\" Key$")
	public void click_on_the_Entity_Tag_text_box_and_press_the_Key(String arg1) throws Throwable {
	    entitiesObj.entyTagTxtBox.sendKeys(Keys.TAB);
	}

	@Then("^: Verify validation message as Entity Name is required\\.$")
	public void verify_validation_message_as_Entity_Name_is_required() throws Throwable {
	    entitiesObj.verifyEntyTagTxtBoxRequiValid();
	}

	@Given("^: Enter Entity Tag value less than (\\d+)$")
	public void enter_Entity_Tag_value_less_than(int arg1) throws Throwable {
	    entitiesObj.entyTagTxtBoxSendkeys(excelData(2, 3));
	}
	@Then("^: Verify validation message as Entity Tag must be at least (\\d+) characters\\.$")
	public void verify_validation_message_as_Entity_Tag_must_be_at_least_characters(int arg1) throws Throwable {
		entitiesObj.verifyEntyTagTxtBoxMinValid();
	}

	@Given("^: Enter Entity Tag value more than (\\d+) characters$")
	public void enter_Entity_Tag_value_more_than_characters(int arg1) throws Throwable {
	    entitiesObj.entyTagTxtBoxSendkeys(excelData(3, 3));
	}

	@Then("^: Verify validation message as Entity Tag cannot be more than (\\d+) characters\\.$")
	public void verify_validation_message_as_Entity_Tag_cannot_be_more_than_characters(int arg1) throws Throwable {
		entitiesObj.verifyEntyTagTxtBoxMaxValid();
	}

	@Given("^: Enter the duplicate value into the Entity Tag text box$")
	public void enter_the_duplicate_value_into_the_Entity_Tag_text_box() throws Throwable {
		entitiesObj.entyTagTxtBoxSendkeys(excelData(4, 3));
	}

	@Then("^: Verify message as Entity Tag already exists$")
	public void verify_message_as_Entity_Tag_already_exists() throws Throwable {
		 
		entitiesObj.entyTagAddBtnClick();
		entitiesObj.entyTagTxtBoxSendkeys(excelData(4, 3)); 
		entitiesObj.entyTagAddBtnClick();
		
		entitiesObj.verifyEntyTagTxtBoxUniqValid();	    
	}

	@Given("^: Enter valid data into the Entity Tag text box$")
	public void enter_valid_data_into_the_Entity_Tag_text_box() throws Throwable {
	    entitiesObj.entyTagTxtBoxSendkeys(excelData(5, 3));
	}

	@Then("^: Verify the system should accept the entity tag value without any validation message$")
	public void verify_the_system_should_accept_the_entity_tag_value_without_any_validation_message() throws Throwable {
	    entitiesObj.verifyEntyTagTxtBoxNoValid();
	}
	
	@Given("^: Enter Entity Description value less than (\\d+) characters$")
	public void enter_Entity_Description_value_less_than_characters(int arg1) throws Throwable {
	    entitiesObj.entyDscipTxtBoxSendkeys(excelData(2, 4));
	}

	@Then("^: Verify the validation message as Description must be at least (\\d+) characters\\.$")
	public void verify_the_validation_message_as_Description_must_be_at_least_characters(int arg1) throws Throwable {
		entitiesObj.verifyEntyDscipTxtBoxMinValid();	    
	}

	@Given("^: Enter Entity Description value more than (\\d+) characters$")
	public void enter_Entity_Description_value_more_than_characters(int arg1) throws Throwable {
	    entitiesObj.entyDscipTxtBoxSendkeys(excelData(3, 4));
	}

	@Then("^: Verify the validation message as Description cannot be more than (\\d+) characters\\.$")
	public void verify_the_validation_message_as_Description_cannot_be_more_than_characters(int arg1) throws Throwable {
		entitiesObj.verifyEntyDscipTxtBoxMaxValid();	    
	}

	@Given("^: Enter valid data into Entity Description text box$")
	public void enter_valid_data_into_Entity_Description_text_box() throws Throwable {
		entitiesObj.entyDscipTxtBoxSendkeys(excelData(5, 4));
	}
	
	@Then("^: Verify the system should accept the entity description value without any validation message$")
	public void verify_the_system_should_accept_the_entity_description_value_without_any_validation_message() throws Throwable {
	   entitiesObj.verifyEntyDscipTxtBoxNoValid();
	}

	@Given("^: Click on Instruction for user checkbox$")
	public void click_on_Instruction_for_user_checkbox() throws Throwable {
		entitiesObj.instForUsrChkBoxClick();	    
	}
	
	@Given("^: Click on Upload Document radio button$")
	public void click_on_Upload_Document_radio_button() throws Throwable {
	    entitiesObj.instForUsrUpldDocRdoBtnClick();
	}
	
	@Then("^: Verify that Instruction for user checkbox appears on the screen$")
	public void verify_that_Instruction_for_user_checkbox_appears_on_the_screen() throws Throwable {
		entitiesObj.verifyInstForUsrTxtBoxAppears();
	}

	@Given("^: Click on Instruction for user text box and press the \"([^\"]*)\" Key$")
	public void click_on_Instruction_for_user_text_box_and_press_the_Key(String arg1) throws Throwable {
	    entitiesObj.instForUsrTxtBox.sendKeys(Keys.TAB);
	    
	}

	@Then("^: Verify the mandatory validation message as User Instructions are required\\.$")
	public void verify_the_mandatory_validation_message_as_User_Instructions_are_required() throws Throwable {
		entitiesObj.verifyInstForUsrTxtBoxRequiValid();
	}

	@Given("^: Enter Instruction for user textbox value less than (\\d+) characters$")
	public void enter_Instruction_for_user_textbox_value_less_than_characters(int arg1) throws Throwable {
	    entitiesObj.instForUsrTxtBoxSendkeys(excelData(2, 5));
	}

	@Then("^: Verify the validation message as Instructions for user must be at least (\\d+) characters\\.$")
	public void verify_the_validation_message_as_Instructions_for_user_must_be_at_least_characters(int arg1) throws Throwable {
		entitiesObj.verifyInstForUsrTxtBoxMinValid();	    
	}

	@Given("^: Enter Instruction for user textbox value more than (\\d+) characters$")
	public void enter_Instruction_for_user_textbox_value_more_than_characters(int arg1) throws Throwable {
	    entitiesObj.instForUsrTxtBoxSendkeys(excelData(3, 5));
	}

	@Then("^: Verify the validation message as Instructions for user cannot be more than (\\d+) characters\\.$")
	public void verify_the_validation_message_as_Instructions_for_user_cannot_be_more_than_characters(int arg1) throws Throwable {
		entitiesObj.verifyInstForUsrTxtBoxMaxValid();
	}

	@Given("^: Enter valid data into Instruction for user textbox$")
	public void enter_valid_data_into_Instruction_for_user_textbox() throws Throwable {
	    entitiesObj.instForUsrTxtBoxSendkeys(excelData(5, 5));	    
	}

	@Then("^: Veirfy the system should accept the Instruction for user textbox value wihtout any validation message$")
	public void veirfy_the_system_should_accept_the_Instruction_for_user_textbox_value_wihtout_any_validation_message() throws Throwable {
		entitiesObj.verifyInstForUsrTxtBoxNoValid();
	}
	
	@Then("^: Verify Upload document option appears on the screen after clicking Instruction for user checkbox'$")
	public void verify_Upload_document_option_appears_on_the_screen_after_clicking_Instruction_for_user_checkbox() throws Throwable {
	    entitiesObj.verifyinstForUsrUpldDocAppears();
	}

	@Given("^: Click on Attribute name textbox and then Press the \"([^\"]*)\" key in Entities screen$")
	public void click_on_Attribute_name_textbox_and_then_Press_the_key_in_Entities_screen(String arg1) throws Throwable {
		entitiesObj.attriNameTxtBox.sendKeys(Keys.TAB);	    
	}

	@Then("^: Verify the validation message as Attribute Name is required\\.$")
	public void verify_the_validation_message_as_Attribute_Name_is_required() throws Throwable {
		entitiesObj.verifyAttriNameTxtBoxRequiValid();
	}

	@Given("^: Enter Attribute name textbox value less than (\\d+) characters in Entities screen$")
	public void enter_Attribute_name_textbox_value_less_than_characters_in_Entities_screen(int arg1) throws Throwable {
		entitiesObj.attriNameTxtBoxSendkeys(excelData(2, 6));
	}

	@Then("^: Verify the validation message as Attribute Name must be at least (\\d+) characters\\.$")
	public void verify_the_validation_message_as_Attribute_Name_must_be_at_least_characters(int arg1) throws Throwable {
		entitiesObj.verifyAttriNameTxtBoxMinValid();
	}

	@Given("^: Enter Attribute Name textbox value more than (\\d+) characters in Entities screen$")
	public void enter_Attribute_Name_textbox_value_more_than_characters_in_Entities_screen(int arg1) throws Throwable {
		entitiesObj.attriNameTxtBoxSendkeys(excelData(3, 6));
	}

	@Then("^: Verify the validation message as  Attribute Name cannot be more than (\\d+) characters\\.$")
	public void verify_the_validation_message_as_Attribute_Name_cannot_be_more_than_characters(int arg1) throws Throwable {
		entitiesObj.verifyAttriNameTxtBoxMaxValid();
	}

	@Given("^: Enter valid Attribute Name Textbox value in Entities screen$")
	public void enter_valid_Attribute_Name_Textbox_value_in_Entities_screen() throws Throwable {
		entitiesObj.attriNameTxtBoxSendkeys(excelData(5, 6));
	}

	@Then("^: System should accept the Attribute Name Textbox value$")
	public void system_should_accept_the_Attribute_Name_Textbox_value() throws Throwable {
		entitiesObj.verifyAttriNameTxtBoxNoValid();
	}
	
	@Given("^: Select Attribute is Key Identifier option$")
	public void select_Attribute_is_Key_Identifier_option() throws Throwable {
	    entitiesObj.attkeyIdchkBoxClick();
	    Thread.sleep(1000);
	}

	@Then("^: Verify that the Link to Entity option does not appear on the screen$")
	public void verify_that_the_Link_to_Entity_option_does_appear_on_the_screen() throws Throwable {
	   entitiesObj.verifyLnkToEntyRdoNotAppears();
	}
	
	@Then("^: Verify that all the records of Entities column display in ascending order when click on sorting icon$")
	public void verify_that_all_the_records_of_Entities_column_display_in_ascending_order_when_click_on_sorting_icon() throws Throwable {
		new AttributesPageObject(ObjectRepo.driver).verifyAscendingOrder("Entities");

	}
	@Then("^: Verify the search results of Entities listing page$")
	public void verify_the_search_results_of_Entities_listing_page() throws Throwable {
		String searchItem = excelData(1, 2);

		entitiesObj.verifysearch(searchItem);
	}
	@Given("^: Click on bread crumb link$")
	public void click_on_bread_crumb_link() throws Throwable {
	    entitiesObj.entBrdCrumClick();
	}

	@Then("^: Verify that the page should be redirected$")
	public void verify_that_the_page_should_be_redirected() throws Throwable {
	    entitiesObj.verifyRedrctToEntPage();
	}
	
	@Given("^: Click on View link of the first record of the Entity listing screen$")
	public void click_on_View_link_of_the_first_record_of_the_Entity_listing_screen() throws Throwable {
	    entitiesObj.entityViewLinkClick();
	}

	@Then("^: Verify that popup should appear on the entity screen with the label \"([^\"]*)\"$")
	public void verify_that_popup_should_appear_on_the_entity_screen_with_the_label(String arg1) throws Throwable {
	    entitiesObj.verifyViewPopupLabel();
	}
	
	@Given("^: Add valid data for all the fields and click on the save button$")
	public void add_valid_data_for_all_the_fields_and_click_on_the_save_button() throws Throwable {
		
		entitiesObj.moduleDrpdwnClick();
		entitiesObj.chkAllBtnClick();
		
		entitiesObj.entyNameTxtBoxSendkeys("");
		entitiesObj.entyTagTxtBoxSendkeys("");
		entitiesObj.entyDscipTxtBoxSendkeys("");
		
		entitiesObj.attriNameTxtBoxSendkeys("");
		entitiesObj.attkeyIdchkBoxClick();
		
		//code select type
		//click add ent attr btn 
		//entitiesObj. save btn click
		
	}

	@Then("^: Verify that the newly added entity should appear on the Entity listing screen and the page should be redirected to entity listing screen$")
	public void verify_that_the_newly_added_entity_should_appear_on_the_Entity_listing_screen_and_the_page_should_be_redirected_to_entity_listing_screen() throws Throwable {
	    
	}

	@Given("^: Click on the first record of entity listing grid$")
	public void click_on_the_first_record_of_entity_listing_grid() throws Throwable {
	    
	}

	@Given("^: Change the valid data to any of the fields and Click to save button$")
	public void change_the_valid_data_to_any_of_the_fields_and_Click_to_save_button() throws Throwable {
	    
	}

	@Then("^: Verify that the newly added record updated and the page should be redirected to the Entities listing screen$")
	public void verify_that_the_newly_added_record_updated_and_the_page_should_be_redirected_to_the_Entities_listing_screen() throws Throwable {
	    
	}
}// end
