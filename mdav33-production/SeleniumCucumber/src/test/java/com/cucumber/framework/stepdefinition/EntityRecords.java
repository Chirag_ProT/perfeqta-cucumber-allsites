package com.cucumber.framework.stepdefinition;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import com.cucumber.framework.helper.PageObject.CommonFunctionPageObject;
import com.cucumber.framework.helper.PageObject.EntityRecordsPageObject;
import com.cucumber.framework.helper.PageObject.LoginPageObject;
import com.cucumber.framework.helper.PageObject.SmartAlertsPageObject;
import com.cucumber.framework.settings.ObjectRepo;
import com.cucumber.framework.utility.Constant;
import com.cucumber.framework.utility.ExcelUtils;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class EntityRecords {
	public ExcelUtils excel;

	public EntityRecordsPageObject entityRcdObj;

	public EntityRecords() {
		entityRcdObj = new EntityRecordsPageObject(ObjectRepo.driver);
	}

	/*public String excelData(int rowVal, int colVal) throws Exception {
		System.out.println("excelCalling.........");
		excel = new ExcelUtils();
		excel.setExcelFile(Constant.Path_TestData + Constant.File_TestData, "Entity Records");
		System.err.println();
		return excel.readXLSFile("Entity Records", rowVal, colVal);
	}*/
	
	public String excelData(int rowVal, int colVal) throws Exception {
		excel = new ExcelUtils();
		excel.setExcelFile(Constant.Path_TestData + Constant.File_TestData, "Entity Records");
		System.err.println();
		return excel.readXLSFile("Entity Records", rowVal, colVal);
	}

	@Given("^: Click on Entity Records Tile$")
	public void click_on_Entity_Records_Tile() throws Throwable {
		Thread.sleep(1000);
		entityRcdObj.entityRecdsTileClick();		
	}

	@Then("^: Verify the module name as Entity Records$")
	public void verify_the_module_name_as_Entity_Records() throws Throwable {
		entityRcdObj.verifyEntityRecdsLabel();
	}


	@Given("^: Click on bread crumb of Entity Records screen$")
	public void click_on_bread_crumb_of_Entity_Records_screen() throws Throwable {
		entityRcdObj.entityBrdCrumClick();
	}

	@Then("^: Verify that the page should be redirected to previous page when click on bread crumb$")
	public void verify_that_the_page_should_be_redirected_to_previous_page_when_click_on_bread_crumb() throws Throwable {
		entityRcdObj.verifyBrdCrum();
	}

	@Then("^: Verify that Record Search type label of Entity Record module should be displayed as \"([^\"]*)\"$")
	public void verify_that_Record_Search_type_label_of_Entity_Record_module_should_be_displayed_as(String arg1) throws Throwable {
		entityRcdObj.verifyRecdSerchLblEntitiy();
	}

	@Then("^: Verify that color of label Entity Record Search for Entity records module should be \"([^\"]*)\"$")
	public void verify_that_color_of_label_Entity_Record_Search_for_Entity_records_module_should_be(String arg1) throws Throwable {
		entityRcdObj.verifyEntityRecordSearchColor();
	}

	@Then("^: Verify that All Module Name for Entity records module should be displayed as \"([^\"]*)\" \\+ \"([^\"]*)\" \\+ \"([^\"]*)\"$")
	public void verify_that_All_Module_Name_for_Entity_records_module_should_be_displayed_as(String arg1, String arg2, String arg3) throws Throwable {
		entityRcdObj.verifyEntityAllModuleNameforEntityRecordsModule();
	}

	@Then("^: Verify that all module in Entity Records sould be selected bydefault$")
	public void verify_that_all_module_in_Entity_Records_sould_be_selected_bydefault() throws Throwable {
		entityRcdObj.verifyEntityAllModuleCheckBoxforEntityRecordsModule();
	}

	@Given("^: Uncheck the check box of modules$")
	public void uncheck_the_check_box_of_modules() throws Throwable {
		entityRcdObj.uncheckCheckedModule();
	}

	@Then("^: Verify that system should allow to Uncheck all modules in Entity Records screen$")
	public void verify_that_system_should_allow_to_Uncheck_all_modules_in_Entity_Records_screen() throws Throwable {
		entityRcdObj.verifyEntityAllModuleUnCheckBoxforEntityRecordsModule();	    
	}

	@Given("^: Check the check box of modules$")
	public void check_the_check_box_of_modules() throws Throwable {
		entityRcdObj.checkUnCheckedModule();
	}

	@Then("^: Verify that system should allow to Check all modules in Entity Records screen$")
	public void verify_that_system_should_allow_to_Check_all_modules_in_Entity_Records_screen() throws Throwable {
		entityRcdObj.verifyEntityAllModuleCheckBoxforEntityRecordsModule();
	}

		@Given("^: Click on serchbox of Enity$")
	public void click_on_serchbox_of_Enity() throws Throwable {
		entityRcdObj.clickSearchBoxEntity();
	}

	@Given("^: Enter Enity name which is not added$")
	public void enter_Enity_name_which_is_not_added() throws Throwable {
		entityRcdObj.enterWrongEntityName(excelData(1, 1));  
	}

	@Then("^: Verify that \"([^\"]*)\" message should be displayed when user enter entity name which is not added$")
	public void verify_that_message_should_be_displayed_when_user_enter_entity_name_which_is_not_added(String arg1) throws Throwable {
		entityRcdObj.verifyEntityNoModuleFoundEntityRecordSearch();
	}

	@Given("^: Enter Entity name which does not have any Record$")
	public void enter_Entity_name_which_does_not_have_any_Record() throws Throwable {
		entityRcdObj.enterEntityNameWhichHaveNoRecords(excelData(2, 1));
	}

	@Given("^: Click on Add To Favorite button of Entity Records$")
	public void click_on_Add_To_Favorite_button() throws Throwable {
		entityRcdObj.clickAddToFavorite();
	}

	@Given("^: Enter name in textbox of Enter Favorite Name$")
	public void enter_name_in_textbox_of_Enter_Favorite_Name() throws Throwable {
		entityRcdObj.enterFavoriteName(excelData(2, 2));
	}

	@Given("^: Click on Save Button$")
	public void click_on_Save_Button() throws Throwable {
		entityRcdObj.clickOnSave();
	}

	@Then("^: Verify that system should displayed message as \"([^\"]*)\"$")
	public void verify_that_system_should_displayed_message_as(String arg1) throws Throwable {
		entityRcdObj.verifyNoRecords();
	}

	@Given("^: Enter Entity name$")
	public void enter_Entity_name() throws Throwable {
		entityRcdObj.enterEntityNameWhichHaveNoRecords(excelData(2, 1));
	}


	@Then("^: Verify that Required validation message of Enter favorite name textbox for Add to favorite popup should be displayed as \"([^\"]*)\"$")
	public void verify_that_Required_validation_message_of_Enter_favorite_name_textbox_for_Add_to_favorite_popup_should_be_displayed_as(String arg1) throws Throwable {
		entityRcdObj.verifyFavoriteNameReuired();
	}

	@Given("^: Enter less than (\\d+) characters in Enter favorite name textbox$")
	public void enter_less_than_characters_in_Enter_favorite_name_textbox(int arg1) throws Throwable {
		entityRcdObj.enterFavoriteName(excelData(3, 2));
	}

	@Then("^: Verify that Minimum validation message of Enter favorite name textbox for Add to favorite popup should be displayed as \"([^\"]*)\"$")
	public void verify_that_Minimum_validation_message_of_Enter_favorite_name_textbox_for_Add_to_favorite_popup_should_be_displayed_as(String arg1) throws Throwable {
		entityRcdObj.verifyFavoriteNameMinimumSize();
	}

	@Then("^: Verify that color of Minimum validation message of Enter favorite name textbox should be \"([^\"]*)\"$")
	public void verify_that_color_of_Minimum_validation_message_of_Enter_favorite_name_textbox_should_be(String arg1) throws Throwable {
		entityRcdObj.verifyFavoriteErrorColor();
	}

	@Given("^: Enter more than (\\d+) characters in Enter favorite name textbox$")
	public void enter_more_than_characters_in_Enter_favorite_name_textbox(int arg1) throws Throwable {
		entityRcdObj.enterFavoriteName(excelData(4, 2));
	}

	@Then("^: Verify that Maximum validation message of Enter favorite name textbox for Add to favorite popup should be displayed as \"([^\"]*)\"$")
	public void verify_that_Maximum_validation_message_of_Enter_favorite_name_textbox_for_Add_to_favorite_popup_should_be_displayed_as(String arg1) throws Throwable {
		entityRcdObj.verifyFavoriteNameMaximumSize();
	}

	@Then("^: Verify that color of Maximum validation message of Enter favorite name textbox should be \"([^\"]*)\"$")
	public void verify_that_color_of_Maximum_validation_message_of_Enter_favorite_name_textbox_should_be(String arg1) throws Throwable {
		entityRcdObj.verifyFavoriteErrorColor();
	}

	@Then("^: Verify that system should allow valid data and no validation should be displayed for Enter favorite name textbox$")
	public void verify_that_system_should_allow_valid_data_and_no_validation_should_be_displayed_forEnter_favorite_name_textbox() throws Throwable {
		entityRcdObj.verifyNoValidationMessage();
	}

	@Given("^: Enter Entity name in search box which does not have any Record$")
	public void enter_Entity_name_in_search_box_which_does_not_have_any_Record() throws Throwable {
		entityRcdObj.enterEntityNameWhichHaveNoRecords(excelData(2, 1));
	}

	@Given("^: Click on go button$")
	public void click_on_go_button() throws Throwable {
		entityRcdObj.clickOnGo();
	}

	@Then("^: Verify that system should displayed message in my favorite as \"([^\"]*)\"$")
	public void verify_that_system_should_displayed_message_in_my_favorite_as(String arg1) throws Throwable {
		entityRcdObj.verifyNoRecordsInMyFavorite();
	}

	@Then("^: Verify that the label of Add to favorite  in Entity Records screen should be displayed as \"([^\"]*)\"$")
	public void verify_that_the_label_of_Add_to_favorite_in_Entity_Records_screen_should_be_displayed_as(String arg1) throws Throwable {
		entityRcdObj.verifyLabelAddToFavoriteOnPopup();
	}

	@Given("^: Click on Cross button$")
	public void click_on_Cross_button() throws Throwable {
		entityRcdObj.clickOnCrossButtonOnAddToFavoritePopup();
	}

	@Then("^: Verify that System should close the popup of Add to Favorite and redirected to the Entity screen when  click on Cross Button$")
	public void verify_that_System_should_close_the_popup_of_Add_to_Favorite_and_redirected_to_the_Entity_screen_when_click_on_Cross_Button() throws Throwable {
		entityRcdObj.verifyPopupToEntityRecordScreen();
	}

	@Given("^: Click on Cancel button of Add to Favorite$")
	public void click_on_Cancel_button() throws Throwable {
		entityRcdObj.clickOnCancleButtonOnAddToFavoritePopup();
	}

	@Then("^: Verify that System should close the popup of Add to Favorite and redirected to the Entity screen when  click on Cancel Button$")
	public void verify_that_System_should_close_the_popup_of_Add_to_Favorite_and_redirected_to_the_Entity_screen_when_click_on_Cancel_Button() throws Throwable {
		entityRcdObj.verifyPopupToEntityRecordScreen();
	}

	@Then("^: Verify that My favorite Radio Button should be selected bydefault in Add to Favorite popup$")
	public void verify_that_My_favorite_Radio_Button_should_be_selected_bydefault_in_Add_to_Favorite_popup() throws Throwable {
		entityRcdObj.verifyAddToFavoriteDefalutMyFavRadioSelect();
	}

	@Then("^: Verify that color of Save button of add to favoritepop should be \"([^\"]*)\"$")
	public void verify_that_color_of_Save_button_of_add_to_favoritepop_should_be(String arg1) throws Throwable {
		entityRcdObj.verifyAddFavoriteSaveButtonColor();
	}

	@Then("^: Verify that color of Cancel button of add favorite popup should be \"([^\"]*)\"$")
	public void verify_that_color_of_Cancel_button_of_add_favorite_popup_should_be(String arg1) throws Throwable {
		entityRcdObj.verifyAddFavoriteCancelButtonColor();
	}

	@Then("^: Verify that color of Go button of Enity Records screen should be \"([^\"]*)\"$")
	public void verify_that_color_of_Go_button_of_Enity_Records_screen_should_be(String arg1) throws Throwable {
		entityRcdObj.verifyGoButtonColor();
	}

	@Given("^: Enter valid data in Enter favorite name textbox$")
	public void enter_valid_data_in_Enter_favorite_name_textbox() throws Throwable {
		Thread.sleep(1000);
		entityRcdObj.enterFavoriteName(excelData(2, 2));
	}

	@Then("^: Verify that system should save the data and redirected to the Enity Records screen when Click on Save Button$")
	public void verify_that_system_should_save_the_data_and_redirected_to_the_Enity_Records_screen_when_Click_on_Save_Button() throws Throwable {
		entityRcdObj.verifyAddToFavoriteSaveButton();
	}

	@Given("^: Enter name in Enter favorite name textbox which is already added$")
	public void enter_name_in_Enter_favorite_name_textbox_which_is_already_added() throws Throwable {
		entityRcdObj.enterFavoriteName(excelData(2, 2));
	}

	@Then("^: Verify that Unique validation message for  Enter favorite name textbox of Add to favorite popupshouls be displayed as \"([^\"]*)\"$")
	public void verify_that_Unique_validation_message_for_Enter_favorite_name_textbox_of_Add_to_favorite_popupshouls_be_displayed_as(String arg1) throws Throwable {
		entityRcdObj.verifyFavoriteNameMustBeUnique();
	}

	@Given("^: Click on Remove button of first record under My favorite listing$")
	public void click_on_Remove_button_of_first_record_under_My_favorite_listing() throws Throwable {
		entityRcdObj.clickOnRemoveButtonOfFirstRecordInMyFavorite();
	}

	@Then("^: Verify that label of Remove popup for my favorite should be displayed as \"([^\"]*)\" \\+ SavedName \\+ \"([^\"]*)\"$")
	public void verify_that_label_of_Remove_popup_for_my_favorite_should_be_displayed_as_SavedName(String arg1, String arg2) throws Throwable {
		entityRcdObj.verifyRemovePopupMyFavoriteLabel();
	}

	@Then("^: Verify that color of Yes button under Remove popup should be \"([^\"]*)\"$")
	public void verify_that_color_of_Yes_button_under_Remove_popup_should_be(String arg1) throws Throwable {
		entityRcdObj.verifyConfirmationOfDeleteMyFavoriteYesButtonColor();
	}

	@Then("^: Verify that color of No button under Remove popup should be \"([^\"]*)\"$")
	public void verify_that_color_of_No_button_under_Remove_popup_should_be(String arg1) throws Throwable {
		entityRcdObj.verifyConfirmationOfDeleteMyFavoriteNoButtonColor();
	}

	@Given("^: Click on No Button in Remove popup$")
	public void click_on_No_Button_in_Remove_popup() throws Throwable {
		entityRcdObj.verifyConfirmationOfDeleteMyFavoriteNoButtonClick();
	}

	@Then("^: Verify that system close the Remove popup and redirected to the Entity Records screen when click on No Button$")
		public void verify_that_system_close_the_Remove_popup_and_redirected_to_the_Entity_Records_screen_when_click_on_No_Button() throws Throwable {
		entityRcdObj.verifyConfirmationOfDeleteMyFavoriteNoButtonFunctionality();
	}

	@Given("^: Click on Yes Button in Remove popup$")
	public void click_on_Yes_Button_in_Remove_popup() throws Throwable {
		entityRcdObj.verifyConfirmationOfDeleteMyFavoriteYesButtonClick();
	}

	@Then("^: Verify that system delete selected my favorite record and redirected to the Entity Records screen when click on Yes Button$")
	public void verify_that_system_delete_selected_my_favorite_record_and_redirected_to_the_Entity_Records_screen_when_click_on_Yes_Button() throws Throwable {
		entityRcdObj.verifyNoRecordsInMyFavorite();
	}

	@Then("^: Verify that color of Clear All Button in Entity Records screen should be \"([^\"]*)\"$")
	public void verify_that_color_of_Clear_All_Button_in_Entity_Records_screen_should_be(String arg1) throws Throwable {
		entityRcdObj.verifyClearAllButtonColor();
	}

	@Then("^: Verify that color of Add To Favorite Button in Entity Records screen should be \"([^\"]*)\"$")
	public void verify_that_color_of_Add_To_Favorite_Button_in_Entity_Records_screen_should_be(String arg1) throws Throwable {
		entityRcdObj.verifyAddToFavoriteColor();
	}

	@Given("^: Click on Clear All Button$")
	public void click_on_Clear_All_Button() throws Throwable {
		entityRcdObj.clkClrAllBut();
	}

	@Then("^: Verify system should clear all the data when click on Clear All button$")
	public void verify_system_should_clear_all_the_data_when_click_on_Clear_All_button() throws Throwable {
		entityRcdObj.clrAllBtnFun();
	}
	
	// excel error

	@Given("^: Enter Entity name which have records$")
	public void enter_Entity_name_which_have_records() throws Throwable {
		entityRcdObj.enterEntityName();
	}
	
	@Then("^: Verify that color of grid add Button should be \"([^\"]*)\"$")
	public void verify_that_color_of_grid_add_Button_should_be(String arg1) throws Throwable {
		entityRcdObj.verifyGrdAddBtnColor();
	}
	
	@Then("^: Verify that color of grid delete Button should be \"([^\"]*)\"$")
	public void verify_that_color_of_grid_delete_Button_should_be(String arg1) throws Throwable {
		entityRcdObj.verifyGrdDelBtnColor();
	}
	
	@Given("^: Enter Entity name which have text attributes$")
	public void enter_Entity_name_which_have_text_attributes() throws Throwable {
	   entityRcdObj.enterEntityNaAttText();
	}

	
	@Given("^: Click on Filter by Dropdown$")
	public void click_on_Filter_by_Dropdown() throws Throwable {
	    entityRcdObj.clkFilByDrdwn();
	}

	@Given("^: Select Filter By Entity Attribute$")
	public void select_Filter_By_Entity_Attribute() throws Throwable {
	    entityRcdObj.selectkFilByDrdwn();
	}

	@Given("^: Click on textbox which is beside the Dropdown$")
	public void click_on_textbox_which_is_beside_the_Dropdown() throws Throwable {
	    entityRcdObj.selectkFilByDrdwnData();
	}

	@Given("^: Enter the value in textbox$")
	public void enter_the_value_in_textbox() throws Throwable {
	    entityRcdObj.enterEntityNaAttData();
	}
	
	@Then("^: Verify that System should displayed data of Entity according to the Searched attribute Value$")
	public void verify_that_System_should_displayed_data_of_Entity_according_to_the_Searched_attribute_Value() throws Throwable {
	    entityRcdObj.verifyEntityAttSrchFunclity();
	}
}
