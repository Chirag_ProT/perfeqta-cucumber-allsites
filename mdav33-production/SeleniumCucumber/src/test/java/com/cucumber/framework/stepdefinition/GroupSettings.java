package com.cucumber.framework.stepdefinition;

import org.openqa.selenium.WebDriver;

import com.cucumber.framework.helper.PageObject.AttributesPageObject;
import com.cucumber.framework.helper.PageObject.EntitiesPageObject;
import com.cucumber.framework.helper.PageObject.GeneralSettingsPageObject;
import com.cucumber.framework.helper.PageObject.LoginPageObject;
import com.cucumber.framework.helper.PageObject.SitesPageObject;
import com.cucumber.framework.helper.PageObject.UserActivityPageObject;
import com.cucumber.framework.helper.PageObject.GroupSettingsPageObject;
import com.cucumber.framework.settings.ObjectRepo;
import com.cucumber.framework.utility.Constant;
import com.cucumber.framework.utility.ExcelUtils;
import cucumber.api.PendingException;
import cucumber.api.java.After;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import mx4j.log.Log;

public class GroupSettings {

	public ExcelUtils excel;
	private Login login;
	private GroupSettingsPageObject grpSettingsPage;


	public GroupSettings() {
//		lpage = new LoginPageObject(ObjectRepo.driver);
		 new LoginPageObject(ObjectRepo.driver);
		 grpSettingsPage = new GroupSettingsPageObject(ObjectRepo.driver);
		
	}
	
	public String excelData(int rowVal, int colVal) throws Exception {
		System.out.println("excelCalling.........");
		excel = new ExcelUtils();
		excel.setExcelFile(Constant.Path_TestData + Constant.File_TestData, "GroupSettings");
		System.err.println();
		return excel.readXLSFile("GroupSettings", rowVal, colVal);
			}

	@Given("^: Click on Group Settings Tile$")
	public void click_on_Group_Settings_Tile() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	     grpSettingsPage.clickonGrpSettingTile();
	}

	@Then("^: Verify the module name as Group Settings$")
	public void verify_the_module_name_as_Group_Settings() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    grpSettingsPage.verifyGrpSetting();
	}

	@Given("^: Click on \"([^\"]*)\" link of the first record of the Group Settings screen$")
	public void click_on_link_of_the_first_record_of_the_Group_Settings_screen(String arg1) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	     grpSettingsPage.clickonFirstRecord();
	}
	@Given("^: Click on Group Name column$")
	public void click_on_Group_Name_column() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	     grpSettingsPage.clickonGroupName();
	}
	@Then("^: Verify that all the records of Site Name column display in ascending order Grup setting$")
	public void verify_that_all_the_records_of_Site_Name_column_display_in_ascending_order_Grup_setting() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		new AttributesPageObject(ObjectRepo.driver).verifyAscendingOrder("Group Names");
	}

	@Then("^: Verify the search results of Group Settings screen$")
	public void verify_the_search_results_of_Group_Settings_screen() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		 
		new EntitiesPageObject(ObjectRepo.driver).verifysearch(excelData(1, 1));
	}

	@Given("^: Click on Add New button of Group Settings$")
	public void click_on_Add_New_button() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		 grpSettingsPage.clickonNewBtn();
		 

	}
	
	@Then("^: Verify that system should redirect to the Add / Edit Group Settings screen when user click on Add New button$")
	public void verify_that_system_should_redirect_to_the_Add_Edit_Group_Settings_screen_when_user_click_on_Add_New_button() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    grpSettingsPage.verifynewBtnRedirection();
	    
	}
	@Then("^: Verify the validation message as Group Name is required\\.$")
	public void verify_the_validation_message_as_Group_Name_is_required() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    grpSettingsPage.verifyValidationMsg();
	}
	@Given("^: Click on Group Name textbox$")
	public void click_on_Group_Name_textbox() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	     grpSettingsPage.clickonGrpnamebox();
	}

	@Given("^: Press the \"([^\"]*)\" Key$")
	public void press_the_Key(String arg1) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    grpSettingsPage.pressTab();
	}
	@Given("^: Enter Group Name value Less than (\\d+) chracters$")
	public void enter_Group_Name_value_Less_than_chracters(int arg1) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	     grpSettingsPage.entervalueGrpnameTxt(excelData(2, 2));
	     
	}

	@Then("^: Verify the validation message as Group Name must be at least (\\d+) characters\\.$")
	public void verify_the_validation_message_as_Group_Name_must_be_at_least_characters(int arg1) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	     grpSettingsPage.verifyMinValidation();
	}
	@Given("^: Enter Group Name value more than (\\d+) characters$")
	public void enter_Group_Name_value_more_than_characters(int arg1) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	   grpSettingsPage.enterMaxValue(excelData(3, 2));
	}

	@Then("^: Verify validation message Group Name cannot be more than (\\d+) characters long\\.$")
	public void verify_validation_message_Group_Name_cannot_be_more_than_characters_long(int arg1) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		 grpSettingsPage.verifyenterMaxValue();
	}
	@Given("^: Enter Duplicate Group Name$")
	public void enter_Duplicate_Group_Name() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		grpSettingsPage.enterDuplicateValue(excelData(4, 2));
	}

	@Then("^: Verify Unique validation message Group Name must be unique\\.$")
	public void verify_Unique_validation_message_Group_Name_must_be_unique() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		grpSettingsPage.verifyenterDuplicateValue();
	}

	@Given("^: Enter a valid Group Name$")
	public void enter_a_valid_Group_Name() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		grpSettingsPage.enterValidValue(excelData(5, 2));
	}

	@Then("^: Verify system should accept the Group name value without any validation message$")
	public void verify_system_should_accept_the_Group_name_value_without_any_validation_message() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		grpSettingsPage.verifyenterValidValue();
	}

	@Given("^: Select App\\(s\\) from list$")
	public void select_App_s_from_list() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	     
	}
	
	@Given("^: Click on Move button$")
	public void click_on_Move_button() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	     grpSettingsPage.ClickonMove();
	     
	}
	
	@Then("^: Verify that by click on Move button selected App\\(s\\) should be added to the Group$")
	public void verify_that_by_click_on_Move_button_selected_App_s_should_be_added_to_the_Group() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	     
	}
	@Given("^: Click and Select module from Module dropdown$")
	public void click_and_Select_module_from_Module_dropdown() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	     
	}
	@Given("^: Select App from list$")
	public void select_App_from_list() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	     grpSettingsPage.selectApp();
	}
     
	@Given("^: select Module from dropdown$")
	public void select_Module_from_dropdown() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		grpSettingsPage.selectModule();
	}
	@Then("^: Verify that by click on Move button selected App should be added to the Group$")
	public void verify_that_by_click_on_Move_button_selected_App_should_be_added_to_the_Group() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	     
	}
	 
	@Then("^: Verify that All added Apps should be displayed in ascending order$")
	public void verify_that_All_added_Apps_should_be_displayed_in_ascending_order() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		 grpSettingsPage.verifyAscendingorderIcon();
	}
	@Then("^: Verify that All added Apps should be displayed in descending order$")
	public void verify_that_All_added_Apps_should_be_displayed_in_descending_order() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		grpSettingsPage.verifydsendingIcon();
	}
	@Given("^: Select multiple Apps from list$")
	public void select_multiple_Apps_from_list() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	     grpSettingsPage.selectMultipleApps();
	}
	@Given("^: Click on Move Down button of added grp$")
	public void click_on_Move_Down_button_of_added_grp() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	     grpSettingsPage.clickMoveDown();
	}

	@Then("^: Verify that Selected App should be move to the Next row when user click on Move Down Button$")
	public void verify_that_Selected_App_should_be_move_to_the_Next_row_when_user_click_on_Move_Down_Button() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	     
	}

	@Given("^: Click on Move Up button of added grp$")
	public void click_on_Move_Up_button_of_added_grp() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		grpSettingsPage.clickMoveUP();
	}
	
	@Then("^: Verify that Selected App should be move to the Previous row when user click on Move Up Button$")
	public void verify_that_Selected_App_should_be_move_to_the_Previous_row_when_user_click_on_Move_Up_Button() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	     
	}

	@Then("^: Verify that color of Save button should be green group setting screen$")
	public void verify_that_color_of_Save_button_should_be_green_group_setting_screen() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	     grpSettingsPage.verifysaveBtnGreen();
	}
	@Then("^: Verify that color of Cancel button should be black group setting screen$")
	public void verify_that_color_of_Cancel_button_should_be_black_group_setting_screen() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		 grpSettingsPage.verifycancelBtnBlack();
	}

	
	@Given("^: Click on Cancel button of Group Settings$")
	public void click_on_Cancel_button() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	     grpSettingsPage.clickCancelBtn();
	}

	@Then("^: Verify that system should redirect to the Group Name listing screen when user click on the Cancel button$")
	public void verify_that_system_should_redirect_to_the_Group_Name_listing_screen_when_user_click_on_the_Cancel_button() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    grpSettingsPage.verifyRedirectiontoGroupSetting();
	}
	
} //end
