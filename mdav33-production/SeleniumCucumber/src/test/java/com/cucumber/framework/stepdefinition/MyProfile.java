package com.cucumber.framework.stepdefinition;

import com.cucumber.framework.helper.PageObject.AttributesPageObject;
import com.cucumber.framework.helper.PageObject.LoginPageObject;
import com.cucumber.framework.helper.PageObject.MyProfilePageObject;
import com.cucumber.framework.settings.ObjectRepo;
import com.cucumber.framework.utility.Constant;
import com.cucumber.framework.utility.ExcelUtils;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

public class MyProfile {

	private LoginPageObject lpage;
	private MyProfilePageObject myProfile;
	public ExcelUtils excel;
	private AttributesPageObject attributePageObj;
	private Login login;

	public MyProfile() {
		attributePageObj = new AttributesPageObject(ObjectRepo.driver);
		lpage = new LoginPageObject(ObjectRepo.driver);
		myProfile = new MyProfilePageObject(ObjectRepo.driver);

	}

	public String excelData(int rowVal, int colVal) throws Exception {
		System.out.println("excelCalling.........");
		excel = new ExcelUtils();
		excel.setExcelFile(Constant.Path_TestData + Constant.File_TestData, "My Profile");
		System.err.println();
		return excel.readXLSFile("My Profile", rowVal, colVal);
	}

	@Given("^: Click on Username from Home Page top right corner$")
	public void click_on_Username_from_Home_Page_top_right_corner() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		myProfile.clickUsernameHomePage();
	}

	@Given("^: Click on My Profile link$")
	public void click_on_My_Profile_link() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		myProfile.clickMyProfileLink();
	}

	@Then("^: Verify the module name as My Profile$")
	public void verify_the_module_name_as_My_Profile() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		myProfile.verifyMyProfileModuleName();
	}

	@Given("^: Click on breadcrumbs of My Profile screen$")
	public void click_on_breadcrumbs_of_My_Profile_screen() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		myProfile.clickMyProfileBreadcrumbs();
	}

	@Then("^: Verify that the page should be redirected to the previous page$")
	public void verify_that_the_page_should_be_redirected_to_the_previous_page() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		myProfile.verifyPreviousPageOfMyProfile();
	}

	@Then("^: Verify the Username label as \"([^\"]*)\"$")
	public void verify_the_Username_label_as(String arg1) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		myProfile.verifyUsernameLabel();
	}

	@Then("^: Verify that the Username field of the My Profile screen should be disable$")
	public void verify_that_the_Username_field_of_the_My_Profile_screen_should_be_disable() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		myProfile.verifyUsernameLableDisable();
	}

	@Then("^: Verify the First Name label as \"([^\"]*)\"$")
	public void verify_the_First_Name_label_as(String arg1) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		myProfile.verifyFirstNameLabel();
	}

	@Then("^: Verify that the FirstName value should be same as Logged in username$")
	public void verify_that_the_FirstName_value_should_be_same_as_Logged_in_username() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		myProfile.verifyFirstNameAndLoginSame();
	}

	@Given("^: Remove the First name value and Press the \"([^\"]*)\" key$")
	public void remove_the_First_name_value_and_Press_the_key(String arg1) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		myProfile.clearFirstNameField();
	}

	@Then("^: Verify the Mandatory validation message as First Name is required\\.$")
	public void verify_the_Mandatory_validation_message_as_First_Name_is_required() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		myProfile.verifyFirstNameRequiredMsg();
	}

	@Then("^: Verify the Mandatory validation message color as \"([^\"]*)\"$")
	public void verify_the_Mandatory_validation_message_color_as(String arg1) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		myProfile.verifyFirstNameRequiredMsgColor();
	}

	@Given("^: Enter First Name as \"([^\"]*)\"$")
	public void enter_First_Name_as(String arg1) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		myProfile.minimumFirstnameMsg(excelData(2, 1));
	}

	@Then("^: Verify validation message as Firstname must be at least (\\d+) characters\\.$")
	public void verify_validation_message_as_Firstname_must_be_at_least_characters(int arg1) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		myProfile.verifyFirstNameMinimumMsg();
	}

	@Then("^: Verify minimum charcter validation message color as \"([^\"]*)\"$")
	public void verify_minimum_charcter_validation_message_color_as(String arg1) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		myProfile.verifyFirstNameMinimumMsgColor();
	}

	@Given("^: Enter First Name value as more than (\\d+) characters$")
	public void enter_First_Name_value_as_more_than_characters(int arg1) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		myProfile.maximumFirstnameMsg(excelData(3, 1));
	}

	@Then("^: Verify the Maximum character validation message as First Name cannot be more than (\\d+) characters\\.$")
	public void verify_the_Maximum_character_validation_message_as_First_Name_cannot_be_more_than_characters(int arg1) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		myProfile.verifyFirstNameMaximumMsg();
	}

	@Then("^: Verify maximum charcter validation message color as \"([^\"]*)\"$")
	public void verify_maximum_charcter_validation_message_color_as(String arg1) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		myProfile.verifyFirstNameMaximumMsgColor();
	}

	@Then("^: Verify the Last Name label as \"([^\"]*)\"$")
	public void verify_the_Last_Name_label_as(String arg1) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		myProfile.verifyLastnameLabel();
	}

	@Then("^: Verify that the Last Name value should be proper$")
	public void verify_that_the_Last_Name_value_should_be_proper() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		myProfile.lastnameValidValue(excelData(5, 2));
	}

	@Given("^: Remove the Last name value and Press the \"([^\"]*)\" key$")
	public void remove_the_Last_name_value_and_Press_the_key(String arg1) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		myProfile.lastnameEmptyValue(excelData(1, 2));
	}

	@Then("^: Verify the Mandatory validation message as Last Name is required\\.$")
	public void verify_the_Mandatory_validation_message_as_Last_Name_is_required() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		myProfile.verifyLastnameRequiredMsg();
	}

	@Then("^: Verify the Lastname Mandatory validation message color as \"([^\"]*)\"$")
	public void verify_the_Lastname_Mandatory_validation_message_color_as(String arg1) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		myProfile.verifyLastnameRequiredMsgColor();
	}

	@Given("^: Enter Last Name as \"([^\"]*)\"$")
	public void enter_Last_Name_as(String arg1) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		myProfile.enterOneCharLastname(excelData(2, 2));
	}

	@Then("^: Verify validation message as Last Name must be at least (\\d+) characters\\.$")
	public void verify_validation_message_as_Last_Name_must_be_at_least_characters(int arg1) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		myProfile.verifyLastnameMinimumMsg();
	}

	@Then("^: Verify Lastname minimum character validation message color as \"([^\"]*)\"$")
	public void verify_Lastname_minimum_character_validation_message_color_as(String arg1) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		myProfile.verifyLastnameMinimumMsgColor();
	}

	@Given("^: Enter Last Name value as more than (\\d+) charcters$")
	public void enter_Last_Name_value_as_more_than_charcters(int arg1) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		myProfile.enterFiftyOneCharLastname(excelData(3, 2));
	}

	@Then("^: Verify the Maximum character validation message as Last Name cannot be more than (\\d+) characters\\.$")
	public void verify_the_Maximum_character_validation_message_as_Last_Name_cannot_be_more_than_characters(int arg1) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		myProfile.verifyLastnameMaximumMsg();
	}

	@Then("^: Verify maximum characters validation message color as \"([^\"]*)\"$")
	public void verify_maximum_characters_validation_message_color_as(String arg1) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		myProfile.verifyLastnameMaximumMsgColor();
	}

	@Then("^: Verify the Email Address label as \"([^\"]*)\"$")
	public void verify_the_Email_Address_label_as(String arg1) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		myProfile.verifyEmailLabel();
	}

	@Given("^: Remove the Email value and Press the \"([^\"]*)\" key$")
	public void remove_the_Email_value_and_Press_the_key(String arg1) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		myProfile.emailEmptyValue(excelData(1, 3));
	}

	@Then("^: Verify the Mandatory validation message as Email Address is required\\.$")
	public void verify_the_Mandatory_validation_message_as_Email_Address_is_required() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		myProfile.verifyEmailAddressRequiredMsg();
	}

	@Then("^: Verify the Email Mandatory validation message color as \"([^\"]*)\"$")
	public void verify_the_Email_Mandatory_validation_message_color_as(String arg1) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		myProfile.verifyEmailAddressRequiredMsgColor();
	}

	@Given("^: Enter InCorrect Email Address$")
	public void enter_InCorrect_Email_Address() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		myProfile.invalidEmailAddress(excelData(7, 3));
	}

	@Then("^: Verify the Incorrect Email validation message as Invalid Email Address\\.$")
	public void verify_the_Incorrect_Email_validation_message_as_Invalid_Email_Address() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		myProfile.verifyEmailAddressInvalidMsg();
	}

	@Then("^: Verify the Incorrect Email Address validation message color as \"([^\"]*)\"$")
	public void verify_the_Incorrect_Email_Address_validation_message_color_as(String arg1) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		myProfile.verifyEmailAddressInvalidMsgColor();
	}

	@Given("^: Enter Emai Address value as more than (\\d+) charcters$")
	public void enter_Emai_Address_value_as_more_than_charcters(int arg1) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		myProfile.maximumEmailAddress(excelData(3, 3));
	}

	@Then("^: Verify the Maximum character validation message as Email Address cannot be more than (\\d+) characters long\\.$")
	public void verify_the_Maximum_character_validation_message_as_Email_Address_cannot_be_more_than_characters_long(int arg1) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		myProfile.verifyEmailAddressMaximumMsg();
	}

	@Then("^: Verify the Maximum character Email Address validation message color as \"([^\"]*)\"$")
	public void verify_the_Maximum_character_Email_Address_validation_message_color_as(String arg1) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		myProfile.verifyEmailAddressMaximumMsgColor();
	}

	@Given("^: Enter valid name in First Name as \"([^\"]*)\"$")
	public void enter_valid_name_in_First_Name_as(String arg1) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		myProfile.enterValidFirstName(excelData(5, 1));
	}

	@Then("^: Verify that system should allow First Name with valid data$")
	public void verify_that_system_should_allow_First_Name_with_valid_data() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		myProfile.verifyFirstNameWithValidValue();
	}

	@Given("^: Enter valid name in Last Name as \"([^\"]*)\"$")
	public void enter_valid_name_in_Last_Name_as(String arg1) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		myProfile.enterValidLastName(excelData(5, 2));
	}

	@Then("^: Verify that system should allow Last Name with valid data$")
	public void verify_that_system_should_allow_Last_Name_with_valid_data() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		myProfile.verifyLastNameWithValidValue();
	}

	@Given("^: Enter Email Address which is already added$")
	public void enter_Email_Address_which_is_already_added() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		myProfile.uniqueEmailAddress(excelData(6, 3));
	}

	@Then("^: Verify system should be displayed validation message as \"([^\"]*)\"$")
	public void verify_system_should_be_displayed_validation_message_as(String arg1) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		myProfile.uniqueEmailAddressMsg();
	}

	@Given("^: Edit Email address$")
	public void edit_Email_address() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		myProfile.emailAddressVerification(excelData(4, 3));
	}

	@Given("^: Click on Save Buttton$")
	public void click_on_Save_Buttton() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		myProfile.clickSaveBtn();
	}

	@Then("^: Verify that system should displayed validation message as \"([^\"]*)\" \\+ \"([^\"]*)\"$")
	public void verify_that_system_should_displayed_validation_message_as(String arg1, String arg2) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		myProfile.verifyLastVerifiedEmailMsg();
	}

	// ----------------------------- Security Questions ----------------------------- 

	@Given("^: Click on Security Question Button$")
	public void click_on_Security_Question_Button() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		myProfile.clickSecurityQuestion();
	}

	@Then("^: Verify that Security Question button should be clickable when user click on button$")
	public void verify_that_Security_Question_button_should_be_clickable_when_user_click_on_button() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		myProfile.verifySecurityQuestionTitle();
	}

	@Then("^: Verify that Label of Security Question should be displayed as \"([^\"]*)\"$")
	public void verify_that_Label_of_Security_Question_should_be_displayed_as(String arg1) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		myProfile.verifySecurityQuestionFieldName();
	}

	@Then("^: Verify that Label of Answer should be displayed as \"([^\"]*)\"$")
	public void verify_that_Label_of_Answer_should_be_displayed_as(String arg1) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		myProfile.verifySecurityQuestionAnswer();
	}

	@Given("^: select \"([^\"]*)\"$")
	public void select(String arg1) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		myProfile.selectDefaultValInDrpDwn();
	}

	@Then("^: Verify that system should displayed Required vaildation message as \"([^\"]*)\"$")
	public void verify_that_system_should_displayed_Required_vaildation_message_as(String arg1) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		myProfile.verifyRequiredMsgSecQuesDrpDwn();
	}

	@Then("^: Verify that color of Required vaildation message should be \"([^\"]*)\"$")
	public void verify_that_color_of_Required_vaildation_message_should_be(String arg1) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		myProfile.verifyRequiredMsgSecQuesDrpDwnColor();
	}

	@Given("^: Click on Answer textbox$")
	public void click_on_Answer_textbox() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		myProfile.clickAnswerTextbox();
	}

	@Given("^: Press \"([^\"]*)\" key$")
	public void press_key(String arg1) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		myProfile.clickAnswerTextboxTab();
	}

	@Then("^: Verify that system should displayed Required vaildation message for Answer as \"([^\"]*)\"$")
	public void verify_that_system_should_displayed_Required_vaildation_message_for_Answer_as(String arg1) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		myProfile.verifyAnswerRequiredMsg();
	}

	@Then("^: Verify that color of Required vaildation message of Answer should be \"([^\"]*)\"$")
	public void verify_that_color_of_Required_vaildation_message_of_Answer_should_be(String arg1) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		myProfile.verifyAnswerRequiredMsgColor();
	}

	@Given("^: Enter less than (\\d+) characters$")
	public void enter_less_than_characters(int arg1) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		myProfile.enterOneCharAnswer(excelData(2, 4));
	}

	@Then("^: Verify that Minimum vaildation message for Answer should be displayed as \"([^\"]*)\"$")
	public void verify_that_Minimum_vaildation_message_for_Answer_should_be_displayed_as(String arg1) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		myProfile.verifyAnswerMinimumMsg();
	}

	@Then("^: Verify that color of Minimum vaildation message for Answer should be \"([^\"]*)\"$")
	public void verify_that_color_of_Minimum_vaildation_message_for_Answer_should_be(String arg1) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		myProfile.verifyAnswerMinimumMsgColor();
	}

	@Given("^: Enter more than (\\d+) characters$")
	public void enter_more_than_characters(int arg1) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		myProfile.enterFiftyOneCharAnswer(excelData(3, 4));
	}

	@Then("^: Verify that Maximum vaildation message for Answer should be displayed as \"([^\"]*)\"$")
	public void verify_that_Maximum_vaildation_message_for_Answer_should_be_displayed_as(String arg1) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		myProfile.verifyAnswerMaximumMsg();
	}

	@Then("^: Verify that color of Maximum validation message for Answer should be \"([^\"]*)\"$")
	public void verify_that_color_of_Maximum_validation_message_for_Answer_should_be(String arg1) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		myProfile.verifyAnswerMaximumMsgColor();
	}

	@Given("^: Enter valid data as \"([^\"]*)\"$")
	public void enter_valid_data_as(String arg1) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		myProfile.enterValidAnswer(excelData(5, 4));
	}

	@Then("^: Verify that system should allow to enter Valid data into Answer textbox$")
	public void verify_that_system_should_allow_to_enter_Valid_data_into_Answer_textbox() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		myProfile.verifyAnswerWithValidValue();
	}

	@Given("^: Click on \"([^\"]*)\" Button$")
	public void click_on_Button(String arg1) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		myProfile.clickAnswersaveBtn();
	}

	@Then("^: Verify that validation message should be displayed as \"([^\"]*)\"$")
	public void verify_that_validation_message_should_be_displayed_as(String arg1) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		myProfile.verifySaveSuccessMsg();
	}

	@Then("^: Verify that color of validation message should be displayed \"([^\"]*)\"$")
	public void verify_that_color_of_validation_message_should_be_displayed(String arg1) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		myProfile.verifySaveSuccessMsgColor();
	}

	// ----------------------------- Change Password -----------------------------

	@Given("^: Click on Change Password Button$")
	public void click_on_Change_Password_Button() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		myProfile.clickChangePassBtn();
	}

	@Then("^: Verify that Change Password should be clickable when user click on Button$")
	public void verify_that_Change_Password_should_be_clickable_when_user_click_on_Button() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		myProfile.verifyChangePassTitle();
	}

	@Then("^: Verify that First Label of Change Password should be displayed as \"([^\"]*)\"$")
	public void verify_that_First_Label_of_Change_Password_should_be_displayed_as(String arg1) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		myProfile.verifyChangePassLabel();
	}

	@Then("^: Verify that Second Label of Change Password should be displayed as \"([^\"]*)\"$")
	public void verify_that_Second_Label_of_Change_Password_should_be_displayed_as(String arg1) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		myProfile.verifyNewPassLabel();
	}

	@Then("^: Verify that Third Label of Change Password should be displayed as \"([^\"]*)\"$")
	public void verify_that_Third_Label_of_Change_Password_should_be_displayed_as(String arg1) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		myProfile.verifyConfirmPassLabel();
	}

	@Given("^: Move mouse on the information icon$")
	public void move_mouse_on_the_information_icon() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		myProfile.clickInformationIcon();
	}

	@Then("^: Verify that system should displayed Tooltip message as \"([^\"]*)\"$")
	public void verify_that_system_should_displayed_Tooltip_message_as(String arg1) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		myProfile.verifyInformationIconMsg();
	}

	@Given("^: Click on Current Password textbox$")
	public void click_on_Current_Password_textbox() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		myProfile.clickCurrentPass(excelData(1, 5));
	}

	@Then("^: Verify that system should displayed Required vaildation message of Current Password as \"([^\"]*)\"$")
	public void verify_that_system_should_displayed_Required_vaildation_message_of_Current_Password_as(String arg1) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		myProfile.verifyCurrentPassRequiredMsg();
	}

	@Then("^: Verify that color of Required validation message should be \"([^\"]*)\"$")
	public void verify_that_color_of_Required_validation_message_should_be(String arg1) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		myProfile.verifyCurrentPassRequiredMsgColor();
	}

	@Given("^: Enter Invalid Password$")
	public void enter_Invalid_Password() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		myProfile.enterInvalidCurrentPass(excelData(7, 5));
	}

	@Given("^: Click on New Password textbox$")
	public void click_on_New_Password_textbox() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		myProfile.clickNewPassField();
	}

	@Given("^: Click on Confirm Password textbox$")
	public void click_on_Confirm_Password_textbox() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		myProfile.clickConfirmPassField();
	}

	@Given("^: Click on Change Password \"([^\"]*)\" Button$")
	public void click_on_Change_Password_Button(String arg1) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		myProfile.clickChangePassSaveBtn();
	}

	@Then("^: Verify that Invalid vaildation message should be displayed as \"([^\"]*)\"$")
	public void verify_that_Invalid_vaildation_message_should_be_displayed_as(String arg1) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		myProfile.verifyInvalidCurrentPassMsg();
	}

	@Given("^: Enter New Password as \"([^\"]*)\"$")
	public void enter_New_Password_as(String arg1) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		myProfile.enterPassInNewPassField(excelData(5, 6));
	}

	@Given("^: Enter Confirm Password as \"([^\"]*)\"$")
	public void enter_Confirm_Password_as(String arg1) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		myProfile.enterPassInConfirmPassField(excelData(5, 6));
	}

	@Then("^: Verify that color of Invalid vaildation message should be \"([^\"]*)\"$")
	public void verify_that_color_of_Invalid_vaildation_message_should_be(String arg1) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		myProfile.verifyInvalidCurrentPassMsgColor();
	}

	@Then("^: Verify that system should displayed Required vaildation message of New Password as \"([^\"]*)\"$")
	public void verify_that_system_should_displayed_Required_vaildation_message_of_New_Password_as(String arg1) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		myProfile.verifyNewPassRequiredMsg();
	}

	@Then("^: Verify that color of Required vaildation message of New Password should be \"([^\"]*)\"$")
	public void verify_that_color_of_Required_vaildation_message_of_New_Password_should_be(String arg1) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		myProfile.verifyNewPassRequiredMsgColor();
	}

	@Given("^: Enter Current Password$")
	public void enter_Current_Password() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		myProfile.enterValidCurrentPass(excelData(5, 5));
	}

	@Then("^: Verify that validation message for Confirm Password should be displayed as \"([^\"]*)\"$")
	public void verify_that_validation_message_for_Confirm_Password_should_be_displayed_as(String arg1) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		myProfile.verifyConfirmPassSameMsg();
	}

	@Given("^: Enter Incorrect Confirm Password as \"([^\"]*)\"$")
	public void enter_Incorrect_Confirm_Password_as(String arg1) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		myProfile.enterInvalidConfirmPass(excelData(7, 7));
	}

	@Then("^: Verify that color of validation message for Confirm Password should be \"([^\"]*)\"$")
	public void verify_that_color_of_validation_message_for_Confirm_Password_should_be(String arg1) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		myProfile.verifyConfirmPassSameMsgColor();
	}

	@Given("^: Enter New Password as alphabets like \"([^\"]*)\"$")
	public void enter_New_Password_as_alphabets_like(String arg1) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		myProfile.enterAlphabetsCharacters(excelData(4, 6));
	}

	@Then("^: Verify that Invalid vaildation message should display properly$")
	public void verify_that_Invalid_vaildation_message_should_display_properly() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		myProfile.verifyInvalidNewPassMsg();
	}

	@Then("^: Verify that color of Invalid vaildation message for New Password should be \"([^\"]*)\"$")
	public void verify_that_color_of_Invalid_vaildation_message_for_New_Password_should_be(String arg1) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		myProfile.verifyInvalidNewPassMsgColor();
	}

	@Then("^: Verify that system should displayed Required vaildation message of Confirm Password as \"([^\"]*)\"$")
	public void verify_that_system_should_displayed_Required_vaildation_message_of_Confirm_Password_as(String arg1) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		myProfile.verifyConfirmPassRequiredMsg();
	}

	@Then("^: Verify that color of Required vaildation message for Confirm Password should be \"([^\"]*)\"$")
	public void verify_that_color_of_Required_vaildation_message_for_Confirm_Password_should_be(String arg1) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		myProfile.verifyConfirmPassRequiredMsgColor();
	}

	@Given("^: Enter Current Password as Test@(\\d+)$")
	public void enter_Current_Password_as_Test(int arg1) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		myProfile.enterCurrentPass(excelData(6, 5));
	}

	@Given("^: Enter New Password as Test@(\\d+)$")
	public void enter_New_Password_as_Test(int arg1) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		myProfile.enterNewPass(excelData(6, 6));
	}

	@Then("^: Verify that validation message for New Password is same as Current Password should be displayed as \"([^\"]*)\"$")
	public void verify_that_validation_message_for_New_Password_is_same_as_Current_Password_should_be_displayed_as(String arg1) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		myProfile.verifyNewCurrentSamePassMsg();
	}

	@Then("^: Verify that color of vaildation message should be \"([^\"]*)\" when Current Password and New Password is same$")
	public void verify_that_color_of_vaildation_message_should_be_when_Current_Password_and_New_Password_is_same(String arg1) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		myProfile.verifyNewCurrentSamePassMsgColor();
	}

	@Given("^: Click on Cancel Button$")
	public void click_on_Cancel_Button() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		myProfile.clickCancelBtnChangePass();
	}

	@Then("^: Verify that Cancel button should be clickable when user click on Cancel Button$")
	public void verify_that_Cancel_button_should_be_clickable_when_user_click_on_Cancel_Button() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		myProfile.verifyCancelBtnFunctionality();
	}

	//	-------------------------------------------- E-Signature PIN --------------------------------------------

	@Given("^: Click on Change E-Signature PIN Button$")
	public void click_on_Change_E_Signature_PIN_Button() throws Throwable {
		myProfile.clickChangeESignPIN();

	}

	@Then("^: Verify that Change E-Signature PIN button should be clickable when user click on Change E-Signature PIN Button$")
	public void verify_that_Change_E_Signature_PIN_button_should_be_clickable_when_user_click_on_Change_E_Signature_PIN_Button() throws Throwable {
		myProfile.verifyChangeESignPINFun();    
	}

	@Then("^: Verify that First Label of Change E-Signature PIN should be displayed as \"([^\"]*)\"$")
	public void verify_that_First_Label_of_Change_E_Signature_PIN_should_be_displayed_as(String arg1) throws Throwable {
		myProfile.verifyFirstLablOfESignPIN();
	}
	
	@Then("^: Verify that Second Label of Change E-Signature PIN should be displayed as \"([^\"]*)\"$")
	public void verify_that_Second_Label_of_Change_E_Signature_PIN_should_be_displayed_as(String arg1) throws Throwable {
	    myProfile.verifySecondLablOfESignPIN();
	}

	@Then("^: Verify that Last Label of Change E-Signature PIN should be displayed as \"([^\"]*)\"$")
	public void verify_that_Last_Label_of_Change_E_Signature_PIN_should_be_displayed_as(String arg1) throws Throwable {
	    myProfile.verifyThirdLablOfESignPIN();
	}
	
	@Given("^: Click on Current E-Signature PIN textbox$")
	public void click_on_Current_E_Signature_PIN_textbox() throws Throwable {
	    myProfile.clickFirstTextBoxOfESignPIN();
	}

	@Then("^: Verify that Required vaildation message for Current E-Signature PIN should be displayed as \"([^\"]*)\"$")
	public void verify_that_Required_vaildation_message_for_Current_E_Signature_PIN_should_be_displayed_as(String arg1) throws Throwable {
	    myProfile.verifyFirstESignPINValMsg();
	}

	@Then("^: Verify that color of Required vaildation message for Current E-Signature PIN should be \"([^\"]*)\"$")
	public void verify_that_color_of_Required_vaildation_message_for_Current_E_Signature_PIN_should_be(String arg1) throws Throwable {
	    myProfile.verifyFirstESignPINValMsgColor();
	}
	
	@Given("^: Enter Invalid E-Signature PIN$")
	public void enter_Invalid_E_Signature_PIN() throws Throwable {
	    myProfile.enterCurrentESignPIN(excelData(7, 8));
	}

	@Given("^: Click on New E-Signature PIN$")
	public void click_on_New_E_Signature_PIN() throws Throwable {
	    myProfile.clickNewSignTextBox();
	}

	@Given("^: Enter E-Signature PIN as \"([^\"]*)\" in New E-Signature PIN$")
	public void enter_E_Signature_PIN_as_in_New_E_Signature_PIN(String arg1) throws Throwable {
	    myProfile.enterNewSignPIN(excelData(5, 9));
	}

	@Given("^: Click on Confirm New eSignature PIN textbox$")
	public void click_on_Confirm_New_eSignature_PIN_textbox() throws Throwable {
	    myProfile.clickConfirmSignTextBox();
	}
	
	@Given("^: Enter E-Signature PIN as \"([^\"]*)\" in Confirm E-Signature PIN$")
	public void enter_E_Signature_PIN_as_in_Confirm_E_Signature_PIN(String arg1) throws Throwable {
	    myProfile.enterConfirmSignPIN(excelData(5, 10));
	}

	@Given("^: Click on \"([^\"]*)\" Button of Change E-Signature PIN$")
	public void click_on_Button_of_Change_E_Signature_PIN(String arg1) throws Throwable {
	    myProfile.clickSaveBtnOfESign();
	}

	@Then("^: Verify that system should displayed vaildation message as Current eSignature PIN is incorrect$")
	public void verify_that_system_should_displayed_vaildation_message_as_Current_eSignature_PIN_is_incorrect() throws Throwable {
	    myProfile.verifyCurrentESignInValData();
	}
	
	@Then("^: Verify that color of vaildation message should be Red$")
	public void verify_that_color_of_vaildation_message_should_be_Red() throws Throwable {
	    myProfile.verifyCurrentESignInValDataColor();
	}
	
	@Then("^: Verify that Required vaildation message shoudl be displayed as \"([^\"]*)\"$")
	public void verify_that_Required_vaildation_message_shoudl_be_displayed_as(String arg1) throws Throwable {
	    myProfile.verifyNewESignPINValMsg();
	}

	@Then("^: Verify that color of Required vaildation message should be \"([^\"]*)\" in New eSignature PIN$")
	public void verify_that_color_of_Required_vaildation_message_should_be_in_New_eSignature_PIN(String arg1) throws Throwable {
	    myProfile.verifyNewESignPINValMsgColor();
	}

	@Then("^: Verify that Required vaildation message for Confirm E-Signature PIN should be displayed as \"([^\"]*)\"$")
	public void verify_that_Required_vaildation_message_for_Confirm_E_Signature_PIN_should_be_displayed_as(String arg1) throws Throwable {
	    myProfile.verifyConfirmESignPINValMsg();
	}

	@Then("^: Verify that color of Required vaildation message for Confirm E-Signature PIN should be \"([^\"]*)\"$")
	public void verify_that_color_of_Required_vaildation_message_for_Confirm_E_Signature_PIN_should_be(String arg1) throws Throwable {
	    myProfile.verifyConfirmESignPINValMsgColor();
	}
	
	@Given("^: Enter E-Signature PIN as \"([^\"]*)\" in current E-Signature PIN$")
	public void enter_E_Signature_PIN_as_in_current_E_Signature_PIN(String arg1) throws Throwable {
	    myProfile.enterCurrentESignPIN(excelData(6, 8));
	}

	@Then("^: Verify that validation message should be displayed as Current eSignature PIN and New eSignature PIN cannot be same\\.$")
	public void verify_that_validation_message_should_be_displayed_as_Current_eSignature_PIN_and_New_eSignature_PIN_cannot_be_same() throws Throwable {
	    myProfile.verifyNewSignValMsgForDup();
	}
	
	@Then("^: Verify that color of validation message should be \"([^\"]*)\" in same Current and New eSignature$")
	public void verify_that_color_of_validation_message_should_be_in_same_Current_and_New_eSignature(String arg1) throws Throwable {
	    myProfile.verifyNewSignValMsgForDupColor();
	}
	
	@Given("^: Enter E-Signature PIN as (\\d+) in Confirm E-Signature PIN$")
	public void enter_E_Signature_PIN_as_in_Confirm_E_Signature_PIN(int arg1) throws Throwable {
	    myProfile.enterConfirmSignPIN(excelData(6, 10));
	}

	@Then("^: Verify that Confirm PIN Mismatch vaildation message should be displayed as \"([^\"]*)\"$")
	public void verify_that_Confirm_PIN_Mismatch_vaildation_message_should_be_displayed_as(String arg1) throws Throwable {
	    myProfile.verifyConfirmSignValMsgForMisMatch();
	}
	
	@Then("^: Verify that color of Confirm PIN Mismatch vaildation message should be \"([^\"]*)\"$")
	public void verify_that_color_of_Confirm_PIN_Mismatch_vaildation_message_should_be(String arg1) throws Throwable {
	    myProfile.verifyConfirmSignValMsgForMisMatchColor();
	}
	
	@Given("^: Enter less than (\\d+) characters in New eSignature$")
	public void enter_less_than_characters_in_New_eSignature(int arg1) throws Throwable {
	    myProfile.enterNewSignPIN(excelData(2, 9));
	}

	@Then("^: Verify that Minimum vaildation message should be displayed as \"([^\"]*)\"$")
	public void verify_that_Minimum_vaildation_message_should_be_displayed_as(String arg1) throws Throwable {
	    myProfile.verifyNewSignValMsgForMin();
	}

	@Then("^: Verify that color of Minimum vaildation message for New eSignature should be \"([^\"]*)\"$")
	public void verify_that_color_of_Minimum_vaildation_message_for_New_eSignature_should_be(String arg1) throws Throwable {
	    myProfile.verifyNewSignValMsgForMinColor();
	}

	@Given("^: Enter more than (\\d+) characters in New eSignature$")
	public void enter_more_than_characters_in_New_eSignature(int arg1) throws Throwable {
	    myProfile.enterNewSignPIN(excelData(3, 9));
	}

	@Then("^: Verify that Maximum vaildation message should be displayed for New eSignature as \"([^\"]*)\"$")
	public void verify_that_Maximum_vaildation_message_should_be_displayed_for_New_eSignature_as(String arg1) throws Throwable {
	    myProfile.verifyNewSignValMsgForMax();
	}

	@Then("^: Verify that color of Maximum vaildation message for New eSignature should be \"([^\"]*)\"$")
	public void verify_that_color_of_Maximum_vaildation_message_for_New_eSignature_should_be(String arg1) throws Throwable {
	    myProfile.verifyNewSignValMsgForMaxColor();
	}

	@Given("^: Enter less than (\\d+) characters in Confirm eSignature$")
	public void enter_less_than_characters_in_Confirm_eSignature(int arg1) throws Throwable {
	    myProfile.enterConfirmSignPIN(excelData(2, 10));
	}
	
	@Then("^: Verify that Minimum vaildation message for Confirm eSignature should be displayed as \"([^\"]*)\"$")
	public void verify_that_Maximum_vaildation_message_should_be_displayed_as(String arg1) throws Throwable {
	    myProfile.verifyConfirmSignValMsgForMin();
	}


	@Then("^: Verify that color of Minimum vaildation message should for Confirm eSignature be \"([^\"]*)\"$")
	public void verify_that_color_of_Minimum_vaildation_message_should_for_Confirm_eSignature_be(String arg1) throws Throwable {
	    myProfile.verifyConfirmSignValMsgForMinColor();
	}

	@Given("^: Enter more than (\\d+) characters in Confirm eSignature$")
	public void enter_more_than_characters_in_Confirm_eSignature(int arg1) throws Throwable {
	    myProfile.enterConfirmSignPIN(excelData(3, 10));
	}

	@Then("^: Verify that Maximum vaildation message for Confirm New E-Signature PIN should be displayed as \"([^\"]*)\"$")
	public void verify_that_Maximum_vaildation_message_for_Confirm_New_E_Signature_PIN_should_be_displayed_as(String arg1) throws Throwable {
	    myProfile.verifyConfirmSignValMsgForMax();
	}

	@Then("^: Verify that color of Maximum vaildation message should for Confirm eSignature be \"([^\"]*)\"$")
	public void verify_that_color_of_Maximum_vaildation_message_should_for_Confirm_eSignature_be(String arg1) throws Throwable {
	   myProfile.verifyConfirmSignValMsgForMaxColor();
	}
	
	@Then("^: Verify that General error vaildation message should be displayed as \"([^\"]*)\"$")
	public void verify_that_General_error_vaildation_message_should_be_displayed_as(String arg1) throws Throwable {
	    myProfile.verifyGeneralError();
	}

	@Then("^: Verify that color of General error vaildation message should be \"([^\"]*)\"$")
	public void verify_that_color_of_General_error_vaildation_message_should_be(String arg1) throws Throwable {
	    myProfile.verifyGeneralErrorColor();
	}
	
	@Given("^: Click on Cancel Button of Change E-Signature PIN$")
	public void click_on_Cancel_Button_of_Change_E_Signature_PIN() throws Throwable {
	    myProfile.clickCancelBtnChangeSign();
	}

	@Then("^: Verify that Cancel Button should be clickable when user click on Cancel button$")
	public void verify_that_Cancel_Button_should_be_clickable_when_user_click_on_Cancel_button() throws Throwable {
	    myProfile.verifyCancelBtnFunctionality();
	}
}
