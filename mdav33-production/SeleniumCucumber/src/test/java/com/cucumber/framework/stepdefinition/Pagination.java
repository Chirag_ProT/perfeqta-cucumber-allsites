package com.cucumber.framework.stepdefinition;

import com.cucumber.framework.helper.PageObject.PaginationPageObject;
import com.cucumber.framework.settings.ObjectRepo;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

public class Pagination {
	private PaginationPageObject paginationObject;

	public Pagination()
	{
		paginationObject = new PaginationPageObject(ObjectRepo.driver);
	}

	@Given("^: Click on \"([^\"]*)\" link of the first record of the listing page$")
	public void click_on_link_of_the_first_record_of_the_listing_page(String arg1) throws Throwable {
		paginationObject.clickToViewAuditTrailLink();    
	}

	@Given("^: Click on the Last button of Pagination$")
	public void click_on_the_Last_button_of_Pagination() throws Throwable {
		paginationObject.clickLastBtnPagination();
	}

	@Given("^: Click on the First button of Pagination$")
	public void click_on_the_First_button_of_Pagination() throws Throwable {
		paginationObject.clickFirstBtnPagination();
	}

	@Then("^: Verify that the system should display the first page of the Audit trail screen$")
	public void verify_that_the_system_should_display_the_first_page_of_the_Audit_trail_screen() throws Throwable {
		paginationObject.verifyFirstPagePagination();
	}

	@Given("^: Click on Next button of Pagination$")
	public void click_on_Next_button_of_Pagination() throws Throwable {
		paginationObject.clickNextBtnPagination();
	}

	@Then("^: Verify that the system should display the second page of the Audit trail screen$")
	public void verify_that_the_system_should_display_the_second_page_of_the_Audit_trail_screen() throws Throwable {
		paginationObject.verifyNextBtnPagination();
	}
	@Given("^: Click on the Previous button of Pagination$")
	public void click_on_the_Previous_button_of_Pagination() throws Throwable {
		paginationObject.clickPreviousBtnPagination();
	}

	@Then("^: Verify that the system should display the previous page of the Audit trail screen$")
	public void verify_that_the_system_should_display_the_previous_page_of_the_Audit_trail_screen() throws Throwable {
		paginationObject.verifyPreviousBtnPagination();
	}

	@Then("^: Verify that the system should display the last page of the Audit trail screen$")
	public void verify_that_the_system_should_display_the_last_page_of_the_Audit_trail_screen() throws Throwable {
		paginationObject.verifyLastBtnPagination();
	}

	@Then("^: Verify the record count$")
	public void verify_the_record_count() throws Throwable {
		paginationObject.verifyCountOfListingScreen();
	}

	@Then("^: Verify that the system should display the first page of the listing screen$")
	public void verify_that_the_system_should_display_the_first_page_of_the_listing_screen() throws Throwable {
		paginationObject.verifyFirstPagePagination();
	}

	@Then("^: Verify that the system should display the second page of the listing screen$")
	public void verify_that_the_system_should_display_the_second_page_of_the_listing_screen() throws Throwable {
		paginationObject.verifyNextBtnPagination();
	}

	@Then("^: Verify that the system should display the last page of the listing screen$")
	public void verify_that_the_system_should_display_the_last_page_of_the_listing_screen() throws Throwable {
		paginationObject.verifyLastBtnPagination();
	}
}


