package com.cucumber.framework.stepdefinition;

import org.openqa.selenium.WebDriver;

import com.cucumber.framework.helper.PageObject.AttributesPageObject;
import com.cucumber.framework.helper.PageObject.EntitiesPageObject;
import com.cucumber.framework.helper.PageObject.GeneralSettingsPageObject;
import com.cucumber.framework.helper.PageObject.LoginPageObject;
import com.cucumber.framework.helper.PageObject.PaginationPageObject;
import com.cucumber.framework.helper.PageObject.ProcedurePageObject;
import com.cucumber.framework.helper.PageObject.QuestionsPageObject;
import com.cucumber.framework.helper.PageObject.SetsPageObject;
import com.cucumber.framework.helper.PageObject.SitesPageObject;
import com.cucumber.framework.helper.PageObject.UserActivityPageObject;
import com.cucumber.framework.helper.PageObject.GroupSettingsPageObject;
import com.cucumber.framework.settings.ObjectRepo;
import com.cucumber.framework.utility.Constant;
import com.cucumber.framework.utility.ExcelUtils;
import cucumber.api.PendingException;
import cucumber.api.java.After;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import mx4j.log.Log;

public class Procedure {

	public ExcelUtils excel;
	private Login login;
	private ProcedurePageObject procedurePage;


	public Procedure() {
		//		lpage = new LoginPageObject(ObjectRepo.driver);
		new LoginPageObject(ObjectRepo.driver);
		procedurePage = new ProcedurePageObject(ObjectRepo.driver);

	}

	public String excelData(int rowVal, int colVal) throws Exception {
		System.out.println("excelCalling.........");
		excel = new ExcelUtils();
		excel.setExcelFile(Constant.Path_TestData + Constant.File_TestData, "Procedure");
		System.err.println();
		return excel.readXLSFile("Procedure", rowVal, colVal);
	}

	@Given("^: Click on Procedure Tile$")
	public void click_on_Procedure_Tile() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		procedurePage.clickOnProcedureTile();
	}

	@Given("^: Click on bread crumb of Procedure screen$")
	public void click_on_bread_crumb_of_Procedure_screen() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		procedurePage.clickonBreadcrumb();
	}
	@Then("^: Verify that the page should be redirected to the Administration Page$")
	public void verify_that_the_page_should_be_redirected_to_the_Administration_Page() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		procedurePage.verifyPageRedirection();
	}
	@Then("^: Verify that system should display the First page of the Procedure listing screen$")
	public void verify_that_system_should_display_the_First_page_of_the_Procedure_listing_screen() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		new PaginationPageObject(ObjectRepo.driver).verifyFirstPagePagination();
	}
	@Then("^: Verify that system should display the Last page of the Procedure listing screen$")
	public void verify_that_system_should_display_the_Last_page_of_the_Procedure_listing_screen() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		new PaginationPageObject(ObjectRepo.driver).verifyLastBtnPagination();
	}
	@Then("^: Verify that system should display the Next page of the Procedure listing screen$")
	public void verify_that_system_should_display_the_Next_page_of_the_Procedure_listing_screen() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		new PaginationPageObject(ObjectRepo.driver).verifyNextBtnPagination();
	}
	@Then("^: Verify that system should display the Previous page of the Procedure listing screen$")
	public void verify_that_system_should_display_the_Previous_page_of_the_Procedure_listing_screen() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		new PaginationPageObject(ObjectRepo.driver).verifyFirstPagePagination();
	}
	@Given("^: Click on First record from Procedure listing screen$")
	public void click_on_First_record_from_Procedure_listing_screen() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		procedurePage.clickonFirstProcedure();
	}

	@Then("^: Verify that system should be redirected to the Edit Procedure page$")
	public void verify_that_system_should_be_redirected_to_the_Edit_Procedure_page() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		procedurePage.verifyRecordEditscreen();
	}
	@Then("^: Verify that Breadcrumb for Edit Procedure screen should be displayed as Home > Administration > Procedures > Add / Edit Procedure$")
	public void verify_that_Breadcrumb_for_Edit_Procedure_screen_should_be_displayed_as_Home_Administration_Procedures_Add_Edit_Procedure() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		procedurePage.verifyWholebreadcrumbEditRecordScreen();
	}
	@Given("^: Click on Save Button of Edit Procedure screen$")
	public void click_on_Save_Button_of_Edit_Procedure_screen() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		procedurePage.clickOnSaveBtn();
	}

	@Then("^: Verify that system should save the record and redirected to the Procedure listing screen when user click on Save Button$")
	public void verify_that_system_should_save_the_record_and_redirected_to_the_Procedure_listing_screen_when_user_click_on_Save_Button() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		procedurePage.verifyProcedurelistingScreen();
	}
	@Given("^: Click on Cancel Button Edit Procedure$")
	public void click_on_Cancel_Button_Edit_Procedure() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		procedurePage.clickOnCancelBtn();
	}

	@Then("^: Verify that system should be redirected to the Procedure listing screen when user click on Cancel Button$")
	public void verify_that_system_should_be_redirected_to_the_Procedure_listing_screen_when_user_click_on_Cancel_Button() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		procedurePage.verifycancelBtnFunctionality();
	}
	@Then("^: Verify the color of Save Button of Edit Procedure screen should be Green$")
	public void verify_the_color_of_Save_Button_of_Edit_Procedure_screen_should_be_Green() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		procedurePage.verifySaveBtnColor();
	}
	@Then("^: Verify the color of Cancel Button of Edit Procedure screen should be Black$")
	public void verify_the_color_of_Cancel_Button_of_Edit_Procedure_screen_should_be_Black() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		procedurePage.verifyCancelBtnColor();
	}

	@Then("^: Verify that all the records of Procedure column display in ascending order$")
	public void verify_that_all_the_records_of_Procedure_column_display_in_ascending_order() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		new AttributesPageObject(ObjectRepo.driver).verifyAscendingOrder("Procedures");
	}
	@Given("^: Click on View Audit Trail link of first record in Procedure listing screen$")
	public void click_on_View_Audit_Trail_link_of_first_record_in_Procedure_listing_screen() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		new SitesPageObject(ObjectRepo.driver).clickonViewAuditSitelistingScreen();
	}

	@Then("^: Verify that system should be redirected to the Audit Trail page of the first record when user click on View Audit Trail link$")
	public void verify_that_system_should_be_redirected_to_the_Audit_Trail_page_of_the_first_record_when_user_click_on_View_Audit_Trail_link() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		new SitesPageObject(ObjectRepo.driver).verifyredirectionofAudittrailScreen();
	}

	@Then("^: Verify that Breadcrumb for Edit Procedure screen should be displayed as Home > Administration > Procedures > Audit Trail$")
	public void verify_that_Breadcrumb_for_Edit_Procedure_screen_should_be_displayed_as_Home_Administration_Procedures_Audit_Trail() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		procedurePage.verifybreadcrumbiAudittrail();
	}
	@Then("^: Verify that total number of entries should be matched with pagination of Audit Trail screen$")
	public void verify_that_total_number_of_entries_should_be_matched_with_pagination_of_Audit_Trail_screen() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		new SitesPageObject(ObjectRepo.driver).verifytotalEntriesSiteListing();
	}
	@Given("^: Verify that the color of Back Button of Audit Trail screen should be Black$")
	public void verify_that_the_color_of_Back_Button_of_Audit_Trail_screen_should_be_Black() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		new SitesPageObject(ObjectRepo.driver).verifyColorofcancelBtnViewAuditScreen();
	}
	@Given("^: Click on Back Button of Audit Trail screen$")
	public void click_on_Back_Button_of_Audit_Trail_screen() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		new SitesPageObject(ObjectRepo.driver).clickonCancelBtnViewAuditScreen();
	}

	@Then("^: Verify that system should be redirected to the Procedure listing screen when user click on Back Button$")
	public void verify_that_system_should_be_redirected_to_the_Procedure_listing_screen_when_user_click_on_Back_Button() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		procedurePage.verifybackBtnofviewAuditredirection();
	}
	@Given("^: Click on Add New Button procedure$")
	public void click_on_Add_New_Button_procedure() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		procedurePage.clickonAddnewBtn();
	}

	@Given("^: Press \"([^\"]*)\" key in add procedure name input box$")
	public void press_key_in_add_procedure_name_input_box(String arg1) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		procedurePage.clickonProcedurenameInput();
	}

	@Then("^: Verify that Required validation message for Procedure Name should be displayed as Procedure Name is required\\.$")
	public void verify_that_Required_validation_message_for_Procedure_Name_should_be_displayed_as_Procedure_Name_is_required() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		procedurePage.verifyreuiredValidation();
	}
	@Given("^: Enter less than (\\d+) characters in the textbox of Procedure Name$")
	public void enter_less_than_characters_in_the_textbox_of_Procedure_Name(int arg1) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		procedurePage.enterLessThan2inProcedurename(excelData(1, 1));
	}

	@Then("^: Verify that Minimum validation message for Procedure Name should be displayed as Procedure name must be at least (\\d+) characters\\.$")
	public void verify_that_Minimum_validation_message_for_Procedure_Name_should_be_displayed_as_Procedure_name_must_be_at_least_characters(int arg1) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		procedurePage.verifyMinValidationProcedureName();
	}
	@Given("^: Enter more than (\\d+) characters in the textbox of Procedure Name$")
	public void enter_more_than_characters_in_the_textbox_of_Procedure_Name(int arg1) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		procedurePage.enterMorethan200CharProcedureName(excelData(2, 1));
	}

	@Then("^: Verify that Maximum validation message for Procedure Name should be displayed as Procedure name cannot be more than (\\d+) characters\\.$")
	public void verify_that_Maximum_validation_message_for_Procedure_Name_should_be_displayed_as_Procedure_name_cannot_be_more_than_characters(int arg1) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		procedurePage.verifyMaxValidatioProcedureName();
	}
	@Given("^: Enter valid data in the textbox of Procedure Name$")
	public void enter_valid_data_in_the_textbox_of_Procedure_Name() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		procedurePage.entervalidDataProcedurename(excelData(3, 1));
	}

	@Then("^: Verify that system should not display any validation message when user enter valid data in the textbox of Procedure Name$")
	public void verify_that_system_should_not_display_any_validation_message_when_user_enter_valid_data_in_the_textbox_of_Procedure_Name() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		procedurePage.verifyValiddataProcedurename();
	}

	@Given("^: Enter Procedure Name of textbox which is already added$")
	public void enter_Procedure_Name_of_textbox_which_is_already_added() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		procedurePage.enterDuplicateDataProcedurename(excelData(4, 1));
	}

	@Then("^: Verify that Unique validation message for Procedure Name should be displayed as Procedure Name must be unique\\.$")
	public void verify_that_Unique_validation_message_for_Procedure_Name_should_be_displayed_as_Procedure_Name_must_be_unique() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		procedurePage.verifyUniqueValidationProcedurename();
	}
	@Given("^: Click on the first record of the Procedures column$")
	public void click_on_the_first_record_of_the_Procedures_column() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		procedurePage.clickonFirstprocedureRecord();
	}

	@Given("^: Click on Procedure Name textbox$")
	public void click_on_Procedure_Name_textbox() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		procedurePage.clickonProcedurenameEdit();
	}

	@Given("^: Edit the Procedure Name$")
	public void edit_the_Procedure_Name() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		procedurePage.editProcedurename(excelData(5, 1));
	}

	@Given("^: Click on Save Button procedure record$")
	public void click_on_Save_Button_procedure_record() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		procedurePage.clickOnSaveBtnProcedurerecord();
	}

	@Then("^: Verify that system should save the Editable data into the Existing record of Procedure module$")
	public void verify_that_system_should_save_the_Editable_data_into_the_Existing_record_of_Procedure_module() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		procedurePage.verifyEditedModule(excelData(5, 1));
	}
	@Given("^: Click on Copy icon of first record of Action column$")
	public void click_on_Copy_icon_of_first_record_of_Action_column() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		procedurePage.clickonCopyIcon();
	}
	@Given("^: Click on Ok button of pop up window$")
	public void click_on_Ok_button_of_pop_up_window() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		procedurePage.clickonOkofCopyIcon();
	}

	@Then("^: Verify that Label of Copy Procedure pop-up should be displayed as Procedure Name \\*$")
	public void verify_that_Label_of_Copy_Procedure_pop_up_should_be_displayed_as_Procedure_Name() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		procedurePage.verifypopupwithProcedurename();
	}
	@Then("^: Verify that system should display '- Copy' value by default with Procedure Name in Copy Procedure pop-up$")
	public void verify_that_system_should_display_Copy_value_by_default_with_Procedure_Name_in_Copy_Procedure_pop_up() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		new QuestionsPageObject(ObjectRepo.driver).CopyDefaultValue();
	}
	@Then("^: Verify that system should Create Copy of Procedure when user click on OK Button$")
	public void verify_that_system_should_Create_Copy_of_Procedure_when_user_click_on_OK_Button() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		new QuestionsPageObject(ObjectRepo.driver).CreateCopy();
	}
	@Given("^: Click on Sorting icon of the Last Updated column$")
	public void click_on_Sorting_icon_of_the_Last_Updated_column() throws Throwable {
		// Write code here that turns the phrase above into concrete actions

	}

	@Then("^: Verify that all the records should be displayed in ascending order$")
	public void verify_that_all_the_records_should_be_displayed_in_ascending_order() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		new AttributesPageObject(ObjectRepo.driver).verifyAscendingOrder("Last Updated");
	}

	@Given("^: Press \"([^\"]*)\" key in add Procedure Tag input box$")
	public void press_key_in_add_Procedure_Tag_input_box(String arg1) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		procedurePage.clickonProcedureTag();
	}

	@Then("^: Verify that Required validation message for Procedure Tag should be displayed as Procedure Name is required\\.$")
	public void verify_that_Required_validation_message_for_Procedure_Tag_should_be_displayed_as_Procedure_Name_is_required() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		procedurePage.verifyProcedureTagRequiredValidation();
	}

	@Given("^: Enter less than (\\d+) characters in the textbox of Procedure Tag$")
	public void enter_less_than_characters_in_the_textbox_of_Procedure_Tag(int arg1) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		procedurePage.enterlessthan2ProcedureTag(excelData(6, 2));
	}

	@Then("^: Verify that Minimum validation message for Procedure Tag should be displayed as Procedure name must be at least (\\d+) characters\\.$")
	public void verify_that_Minimum_validation_message_for_Procedure_Tag_should_be_displayed_as_Procedure_name_must_be_at_least_characters(int arg1) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		procedurePage.verifyMinValidationProceduretag();
	}

	@Given("^: Enter more than (\\d+) characters in the textbox of Procedure Tag$")
	public void enter_more_than_characters_in_the_textbox_of_Procedure_Tag(int arg1) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		procedurePage.enterMorethan200CharProcedureTag(excelData(7, 2));
	}

	@Then("^: Verify that Maximum validation message for Procedure Tag should be displayed as Procedure name cannot be more than (\\d+) characters\\.$")
	public void verify_that_Maximum_validation_message_for_Procedure_Tag_should_be_displayed_as_Procedure_name_cannot_be_more_than_characters(int arg1) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		procedurePage.verifyMaxValidatioProcedureTag();
	}

	@Given("^: Enter valid data in the textbox of Procedure Tag$")
	public void enter_valid_data_in_the_textbox_of_Procedure_Tag() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		procedurePage.entervalidDataProcedureTag(excelData(8, 2));
	}

	@Then("^: Verify that system should not display any validation message when user enter valid data in the textbox of Procedure Tag$")
	public void verify_that_system_should_not_display_any_validation_message_when_user_enter_valid_data_in_the_textbox_of_Procedure_Tag() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		procedurePage.verifyvaliddataProcedureTag();
	}

	@Then("^: Verify that system should allow toclick on checkbox$")
	public void verify_that_system_should_allow_toclick_on_checkbox() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		procedurePage.verifyinstructionCheckbox();
	}
	@Given("^: Click on Instructions input box$")
	public void click_on_Instructions_input_box() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		procedurePage.clickonInstructionInput();
	}

	@Given("^: Press Tab key Instruction Input box$")
	public void press_Tab_key_Instruction_Input_box() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		procedurePage.pressTabInstruction();
	}

	@Then("^: Verify that Required validation message for instructions should be displayed as Instructions is required\\.$")
	public void verify_that_Required_validation_message_for_instructions_should_be_displayed_as_Instructions_is_required() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		procedurePage.verifyInstructionRequired();
	}

	@Given("^: Enter less than (\\d+) characters in the textbox of Instruction Input box$")
	public void enter_less_than_characters_in_the_textbox_of_Instruction_Input_box(int arg1) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		procedurePage.enterLessThan2inInstruction(excelData(9, 3));
	}

	@Then("^: Verify that Minimum validation message for instructions should be displayed as Instructions for user must be at least (\\d+) characters\\.$")
	public void verify_that_Minimum_validation_message_for_instructions_should_be_displayed_as_Instructions_for_user_must_be_at_least_characters(int arg1) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		procedurePage.minValidationInstruction();
	}
	@Given("^: Enter more than (\\d+) characters in the textbox of Instruction Input box$")
	public void enter_more_than_characters_in_the_textbox_of_Instruction_Input_box(int arg1) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		procedurePage.enterMorethan200CharInstruction(excelData(10, 3));
	}

	@Then("^: Verify that Maximum validation message for instructions should be displayed as Instructions for user must be at least (\\d+) characters\\.$")
	public void verify_that_Maximum_validation_message_for_instructions_should_be_displayed_as_Instructions_for_user_must_be_at_least_characters(int arg1) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		procedurePage.verifyMaxValidatioInstruction();
	}
	@Given("^: Enter Valid data Instruction Input box$")
	public void enter_Valid_data_Instruction_Input_box() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		procedurePage.entervalidDataInstruction(excelData(11, 3));
	}

	@Then("^: Verify that no validation message for instructions$")
	public void verify_that_no_validation_message_for_instructions() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		procedurePage.verifyValiddataInstruction();
	}

	@Given("^: Click on Url checkbox$")
	public void click_on_Url_checkbox() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		procedurePage.clickonUrlChecbox();
	}

	@Then("^: Verify Url checkbox Should be clickable\\.$")
	public void verify_Url_checkbox_Should_be_clickable() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		procedurePage.verifyUlrcheckboxClickable();
	}
	@Given("^: press Tab in URl box$")
	public void press_Tab_in_URl_box() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		procedurePage.pressTabUrl();
	}

	@Then("^: Verify Required msg Should be displayed\\.$")
	public void verify_Required_msg_Should_be_displayed() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		procedurePage.verifyRequiredMsgUrl();
	}
	@Given("^: Enter invalid data url$")
	public void enter_invalid_data_url() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		procedurePage.enterinvalidUrl(excelData(12, 4));
	}

	@Then("^: Verify invalid  msg Should be displayed as Not a valid URL, don't forget to use http:// or https://$")
	public void verify_invalid_msg_Should_be_displayed_as_Not_a_valid_URL_don_t_forget_to_use_http_or_https() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		procedurePage.verifyInvalidUrl();
	}

	@Given("^: Enter valid data url$")
	public void enter_valid_data_url() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		procedurePage.enterValidURLdata(excelData(13, 4));
	}

	@Then("^: Verify valid url without any validation msg\\.$")
	public void verify_valid_url_without_any_validation_msg() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		procedurePage.verifyValidUrl();
	}
	@Then("^: Verify that Toggle button should be Active by default new Procedure screen$")
	public void verify_that_Toggle_button_should_be_Active_by_default_new_Procedure_screen() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		new SetsPageObject(ObjectRepo.driver).verifyToggleBtnDefaultVal();
	}
	@Given("^: Click on Toggle button and make it as Inactive in add prosedure$")
	public void click_on_Toggle_button_and_make_it_as_Inactive_in_add_prosedure() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		new SetsPageObject(ObjectRepo.driver).clickToggleBtnInactive();
	}

	@Then("^: Verify that Toggle button should be Inactive in Add procedure screen$")
	public void verify_that_Toggle_button_should_be_Inactive_in_Add_procedure_screen() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		new SetsPageObject(ObjectRepo.driver).verifyToggleBtnInactiveVal();
	}
	@Given("^: Enter the Procedure name into the search box of Procedure listing screen$")
	public void enter_the_Procedure_name_into_the_search_box_of_Procedure_listing_screen() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		procedurePage.enterprocedurenameSearch();
	}

	@Then("^: Verify the search result of Procedure Listin Screen$")
	public void verify_the_search_result_of_Procedure_Listin_Screen() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		String searchItem = excelData(14, 5);
		new EntitiesPageObject(ObjectRepo.driver).verifysearch(searchItem);

	}

	@Given("^: Enter valid data of all mandatory field in Add Procedure screen$")
	public void enter_valid_data_of_all_mandatory_field_in_Add_Procedure_screen() throws Throwable {
		procedurePage.clickOnAddTag();
		procedurePage.enterSearchAndSelectQuestion(excelData(15, 6));
		procedurePage.clickOnAddQue();
		
	}

	@Given("^: Click on Save Button in Add Procedure screen$")
	public void click_on_Save_Button_in_Add_Procedure_screen() throws Throwable {
		procedurePage.clickOnSaveBtn();
	}

	@Then("^: Verify that New Procedure should be added into the system when user click on Save Button in Add Procedure screen$")
	public void verify_that_New_Procedure_should_be_added_into_the_system_when_user_click_on_Save_Button_in_Add_Procedure_screen() throws Throwable {
		procedurePage.verifyAddedProcedure(excelData(3, 1));
	}
	
	@Given("^: Click on Copy icon of first record of Action column for checking -copy$")
	public void click_on_Copy_icon_of_first_record_of_Action_column_for_checking_copy() throws Throwable {
	    System.out.println("Blank method");
	}


} //end
