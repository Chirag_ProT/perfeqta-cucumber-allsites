package com.cucumber.framework.stepdefinition;


import java.awt.RenderingHints.Key;

import org.openqa.selenium.Keys;

import com.cucumber.framework.helper.PageObject.QuestionsPageObject;

import com.cucumber.framework.settings.ObjectRepo;
import com.cucumber.framework.utility.Constant;
import com.cucumber.framework.utility.ExcelUtils;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

public class Questions
{
	private QuestionsPageObject questionPageObj;
	public ExcelUtils excel;

	public Questions()
	{
		questionPageObj = new QuestionsPageObject(ObjectRepo.driver);
	}
	public String excelData(int rowVal, int colVal) throws Exception {
		excel = new ExcelUtils();
		excel.setExcelFile(Constant.Path_TestData + Constant.File_TestData, "Questions");
		System.err.println();
		return excel.readXLSFile("Questions", rowVal, colVal);
	}
	@Given("^: Click on Questions Tile$")
	public void click_on_Questions_Tile() throws Throwable {
		questionPageObj.clickOnQuestionsTile();
	}
	@Then("^: Verify the module name as Questions$")
	public void verify_the_module_name_as_Questions() throws Throwable {

		questionPageObj.verifyQuestionModuleName();
	}
	@Given("^: Click on Filter By dropdown$")
	public void click_on_Filter_By_dropdown() throws Throwable {
		questionPageObj.verifyQuestionsDrpFilter();

	}
	@Then("^: Verify the Filter By dropdown should be clickable$")
	public void verify_the_Filter_By_dropdown_should_be_clickable() throws Throwable {
		questionPageObj.verifyQuestionsDrpClickable();
	}
	@Given("^: Click on Filter By dropdown and select Question Type option$")
	public void click_on_Filter_By_dropdown_and_select_Question_Type_option() throws Throwable {
		questionPageObj.selectQuestionTypeOption();
	}
	@Given("^: Click on Question Type dropdown$")
	public void click_on_Question_Type_dropdown() throws Throwable {
		questionPageObj.verifyQuestionsTypeDrpFilter();
	}
	@Then("^: Verify the Question Type dropdown should be clickable$")
	public void verify_the_Question_Type_dropdown_should_be_clickable() throws Throwable {
		questionPageObj.verifyQuestionsTypeClickable();
	}
	@Given("^: Click on Sorting icon of the Questions column$")
	public void click_on_Sorting_icon_of_the_Questions_column() throws Throwable {
		questionPageObj.verifyAscendingOrder();
	}
	@Then("^: Verify that all the records of Questions column display in ascending order when click on sorting icon$")
	public void verify_that_all_the_records_of_Questions_column_display_in_ascending_order_when_click_on_sorting_icon() throws Throwable {		
		String sortingColName = excelData(1, 1);
		questionPageObj.verifyAscendingOrderResult(sortingColName);
	}
	@Then("^: Verify the search results of Questions listing page$")
	public void verify_the_search_results_of_Questions_listing_page() throws Throwable {
		questionPageObj.verifysearch(excelData(2, 2));
	}
	@Given("^: Click on the first record of Question listing grid$")
	public void click_on_the_first_record_of_Question_listing_grid() throws Throwable {
		questionPageObj.clickToFirstQuestion();
	}
	@Then("^: Verify the Breadcrumb of Edit Question as \"([^\"]*)\"$")
	public void verify_the_Breadcrumb_of_Edit_Question_as(String arg1) throws Throwable {
		questionPageObj.verifyEditQuestionBreadCrumbs();
	}
	@Given("^: Click on Save button of Questions screen$")
	public void click_on_Save_button_of_Questions_screen() throws Throwable {
		questionPageObj.clickToSaveButton();
	}
	@Then("^: Verify the Save button should be clickable$")
	public void verify_the_Save_button_should_be_clickable() throws Throwable {
		questionPageObj.verifyQuestionsSaveButton();
	}
	@Given("^: Click on Cancel button of Questions screen$")
	public void click_on_Cancel_button_of_Questions_screen() throws Throwable {
		questionPageObj.clickToCancelButton();
	}
	@Then("^: Verify the Cancel button should be clickable and redirect to Questions listing grid$")
	public void verify_the_Cancel_button_should_be_clickable() throws Throwable {
		questionPageObj.verifyQuestionPageRedirection();
	}
	@Then("^: Verify the Color of Save button$")
	public void verify_the_Color_of_Save_button() throws Throwable {
		questionPageObj.verifySaveButtonColor();
	}
	@Then("^: Verify the Color of Cancel button$")
	public void verify_the_Color_of_Cancel_button() throws Throwable {
		questionPageObj.verifyCancelButtonColor();
	}
	@Given("^: Click on the Create Copy Icon of first record of Question listing grid$")
	public void click_on_the_Create_Copy_Icon_of_first_record_of_Question_listing_grid() throws Throwable {
	}
	@Then("^: Verify the Label of Copy pop-up should be as \"([^\"]*)\"$")
	public void verify_the_Label_of_Copy_pop_up_should_be_as(String arg1) throws Throwable {
		questionPageObj.CopyClick("Questions Name *");
	}
	@Then("^: Verify the Default Value of Copy pop-up should be appended with \"([^\"]*)\"$")
	public void verify_the_Default_Value_of_Copy_pop_up_should_be_appended_with(String arg1) throws Throwable {
		questionPageObj.CopyDefaultValue();
	}
	@Given("^: Click on OK button of the Copy popup$")
	public void click_on_OK_button_of_the_Copy_popup() throws Throwable {
	}
	@Then("^: Verify that the system should Create Copy of Question$")
	public void verify_that_the_system_should_Create_Copy_of_Question() throws Throwable {
		questionPageObj.CreateCopy();
	}
	@Given("^: Click on \"([^\"]*)\" button of Questions listing screen$")
	public void click_on_button_of_Questions_listing_screen(String arg1) throws Throwable {
		questionPageObj.clickonAddNewBtn();
	}
	@Given("^: Click on Question Name textbox and press the \"([^\"]*)\" Key$")
	public void click_on_Question_Name_textbox_and_press_the_Key(String arg1) throws Throwable {
		Thread.sleep(1000);
		questionPageObj.questionsNameTitle.sendKeys(Keys.TAB);
	}
	@Then("^: Verify the validation message as Question Name is required\\.$")
	public void verify_the_validation_message_as_Question_Name_is_required() throws Throwable {
		questionPageObj.verifyQuestionRequiredVal();
	}
	@Given("^: Enter Question Name value less than (\\d+) chracters$")
	public void enter_Question_Name_value_less_than_chracters(int arg1) throws Throwable {
		String questionminival = excelData(5, 2);
		questionPageObj.questionsNamesendkeys(questionminival);
	}
	@Then("^: Verify the validation message as Question Name must be at least (\\d+) characters\\.$")
	public void verify_the_validation_message_as_Question_Name_must_be_at_least_characters(int arg1) throws Throwable {
		questionPageObj.verifyQuestionMinimumVal();
	}
	@Given("^: Enter Question Name value more than (\\d+) characters$")
	public void enter_Question_Name_value_more_than_characters(int arg1) throws Throwable {
		String questionmaxval = excelData(6, 2);
		questionPageObj.questionsNamesendkeys(questionmaxval);
	}
	@Then("^: Verify validation message Question Name cannot be more than (\\d+) characters\\.$")
	public void verify_validation_message_Question_Name_cannot_be_more_than_characters(int arg1) throws Throwable {
		questionPageObj.verifyQuestionMaximumVal();
	}
	@Given("^: Enter a valid Question Name$")
	public void enter_a_valid_Question_Name() throws Throwable {
		String questionnoval = excelData(7, 2);
		questionPageObj.questionsNamesendkeys(questionnoval);
	}
	@Then("^: Verify system should accept the Question name value without any validation message$")
	public void verify_system_should_accept_the_Question_name_value_without_any_validation_message() throws Throwable {
		questionPageObj.verifyQuestionNoVal();   
	}
	@Given("^: Enter duplicate Question Name$")
	public void enter_duplicate_Question_Name() throws Throwable {
		String questionuniqueval = excelData(8, 2);
		questionPageObj.questionsNamesendkeys(questionuniqueval);
	}
	@Then("^: Verify the Unique validation message for the Question Name Textbox as Question Name must be unique\\.$")
	public void verify_the_Unique_validation_message_for_the_Question_Name_Textbox_as_Question_Name_must_be_unique() throws Throwable {
		questionPageObj.verifyQuestionUniqueVal();
	}
	@Given("^: Click on the Question Tag text box and press the \"([^\"]*)\" Key$")
	public void click_on_the_Question_Tag_text_box_and_press_the_Key(String arg1) throws Throwable {
		questionPageObj.questionsTag.sendKeys(Keys.TAB);
	}
	@Then("^: Verify validation message as Question Tag is required\\.$")
	public void verify_validation_message_as_Question_Tag_is_required() throws Throwable {
		questionPageObj.verifyQuestionTagRequiredVal();
	}
	@Given("^: Enter Question Tag value less than (\\d+) characters$")
	public void enter_Question_Tag_value_less_than_characters(int arg1) throws Throwable {
		String questiontagminival = excelData(9, 2);
		questionPageObj.questionsTagsendkeys(questiontagminival);
	}
	@Then("^: Verify validation message as Question Tag must be at least (\\d+) characters\\.$")
	public void verify_validation_message_as_Question_Tag_must_be_at_least_characters(int arg1) throws Throwable {
		questionPageObj.verifyQuestionTagMinimumVal();
	}
	@Given("^: Enter Question Tag value more than (\\d+) characters$")
	public void enter_Question_Tag_value_more_than_characters(int arg1) throws Throwable {
		String questiontagmaxval = excelData(10, 2);
		questionPageObj.questionsTagsendkeys(questiontagmaxval);
	}
	@Then("^: Verify validation message as Question Tag cannot be more than (\\d+) characters\\.$")
	public void verify_validation_message_as_Question_Tag_cannot_be_more_than_characters(int arg1) throws Throwable {
	    questionPageObj.verifyQuestionTagMaximumVal();
	}
	@Given("^: Enter the duplicate value into the Question Tag text box$")
	public void enter_the_duplicate_value_into_the_Question_Tag_text_box() throws Throwable {
		String questionuniqueval = excelData(11, 2);
		questionPageObj.questionsTagsendkeys(questionuniqueval);
		questionPageObj.clickonAddTagBtn();
		questionPageObj.questionsTagsendkeys(questionuniqueval);
		questionPageObj.clickonAddTagBtn();
	}
	@Then("^: Verify message as Question Tag already exists$")
	public void verify_message_as_Question_Tag_already_exists() throws Throwable {
	    questionPageObj.verifyQuestionTagUniqueVal();
	}
	@Given("^: Enter valid data into the Question Tag text box$")
	public void enter_valid_data_into_the_Question_Tag_text_box() throws Throwable {
		String questiontagnoval = excelData(12, 2);
		questionPageObj.questionsTagsendkeys(questiontagnoval);
		questionPageObj.clickonAddTagBtn();
	}
	@Then("^: Verify the system should accept the Question tag value without any validation message$")
	public void verify_the_system_should_accept_the_Question_tag_value_without_any_validation_message() throws Throwable {
	    questionPageObj.verifyQuestionTagNoVal();
	}
	@Given("^: Click on Instructions for user checkbox$")
	public void click_on_Instructions_for_user_checkbox() throws Throwable {
	    questionPageObj.selectInstructionForUser();
	}
	@Then("^: Verify that Instructions for user checkbox appears on the screen$")
	public void verify_that_Instructions_for_user_checkbox_appears_on_the_screen() throws Throwable {
	    questionPageObj.verifyInstructionForUser();
	}
	@Given("^: Click on Instructions for user text box and press the \"([^\"]*)\" Key$")
	public void click_on_Instructions_for_user_text_box_and_press_the_Key(String arg1) throws Throwable {
		questionPageObj.questionsInstForUserTextbox.sendKeys(Keys.TAB);
	}
	@Then("^: Verify mandatory validation message as User Instructions are required\\.$")
	public void verify_mandatory_validation_message_as_User_Instructions_are_required() throws Throwable {
		questionPageObj.verifyQuestionInstructionForUserRequiredVal();
	}
	@Given("^: Enter Instructions For User textbox value less than (\\d+) characters$")
	public void enter_Instructions_For_User_textbox_value_less_than_characters(int arg1) throws Throwable {
		String questioninstforuserminival = excelData(13, 2);
		questionPageObj.questionsInstructionForUsersendkeys(questioninstforuserminival);
	}
	@Then("^: Verify that validation message as Instructions for user must be at least (\\d+) characters\\.$")
	public void verify_that_validation_message_as_Instructions_for_user_must_be_at_least_characters(int arg1) throws Throwable {
		questionPageObj.verifyQuestionInstForUserMinimumVal();
	}
	@Given("^: Enter Instructions for user textbox value more than (\\d+) characters$")
	public void enter_Instructions_for_user_textbox_value_more_than_characters(int arg1) throws Throwable {
		String questioninstforusermaxval = excelData(14, 2);
		questionPageObj.questionsInstructionForUsersendkeys(questioninstforusermaxval);
	}

	@Then("^: Verify that validation message as Instructions for user cannot be more than (\\d+) characters\\.$")
	public void verify_that_validation_message_as_Instructions_for_user_cannot_be_more_than_characters(int arg1) throws Throwable {
		questionPageObj.verifyQuestionInstForUserMaximumVal();
	}
	@Given("^: Enter valid data into Instructions for user textbox$")
	public void enter_valid_data_into_Instructions_for_user_textbox() throws Throwable {
		String questioninstforusernoval = excelData(15, 2);
		questionPageObj.questionsInstructionForUsersendkeys(questioninstforusernoval);
	}
	@Then("^: Veirfy the system should accept the Instructions for user textbox value wihtout any validation message$")
	public void veirfy_the_system_should_accept_the_Instructions_for_user_textbox_value_wihtout_any_validation_message() throws Throwable {
		questionPageObj.verifyQuestionInstForUserNoVal();
	}
	@Given("^: Add valid data for all fields and click on the save button$")
	public void add_valid_data_for_all_fields_and_click_on_the_save_button() throws Throwable {
		String questionnoval = excelData(16, 2);
		questionPageObj.questionsNamesendkeys(questionnoval);
		String questiontagnoval = excelData(12, 2);
		questionPageObj.questionsTagsendkeys(questiontagnoval);
		questionPageObj.clickonAddTagBtn();
		questionPageObj.selectTypeOption();
		questionPageObj.clickonRadioBtnOfRegAndCondition();
		questionPageObj.clickonRadioBtnOfCondition();
		questionPageObj.selectTypeOptionForConditon();
		questionPageObj.enterLengthOfCondition(excelData(16, 3));
		questionPageObj.enterValMsgOfCondition(excelData(16, 4));
		questionPageObj.questionsSaveButton();
	}
	@Then("^: Verify that the newly added Question should appear on the Questions listing screen and the page should be redirected to Questions listing screen$")
	public void verify_that_the_newly_added_Question_should_appear_on_the_Questions_listing_screen_and_the_page_should_be_redirected_to_Questions_listing_screen() throws Throwable {
		questionPageObj.verifyNewAddedQuestion(excelData(16, 2));
	}
	@Then("^: Verify that system should display Audit Trail page and Breadcrumb as \"([^\"]*)\" when user click on \"([^\"]*)\" link of the Questions listing screen$")
	public void verify_that_system_should_display_Audit_Trail_page_and_Breadcrumb_as_when_user_click_on_link_of_the_Questions_listing_screen(String arg1, String arg2) throws Throwable {
		questionPageObj.verifyAuditTrailPage();
		questionPageObj.verifyAuditTrailBreadCrumbs();
	}
	@Then("^: Verify the Color of Back button$")
	public void verify_the_Color_of_Back_button() throws Throwable {
		questionPageObj.verifyBackButtonColor();
	}
	@Given("^: Click on Back button of Question$")
	public void click_on_Back_button() throws Throwable {
	    questionPageObj.clickBackButton();
	}

	@Then("^: Verify that the system should redirect to Questions listing screen and Breadcrumb display as \"([^\"]*)\"$")
	public void verify_that_the_system_should_redirect_to_Questions_listing_screen_and_Breadcrumb_display_as(String arg1) throws Throwable {
	    questionPageObj.verifyQuestionsBreadCrumbs();
	}
}
