package com.cucumber.framework.stepdefinition;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import com.cucumber.framework.helper.DatePicker.Datepicker;
import com.cucumber.framework.helper.PageObject.AttributesPageObject;
import com.cucumber.framework.helper.PageObject.CommonFunctionPageObject;
import com.cucumber.framework.helper.PageObject.LoginPageObject;
import com.cucumber.framework.helper.PageObject.SearchWindowPageObject;
import com.cucumber.framework.helper.PageObject.SmartAlertsPageObject;
import com.cucumber.framework.settings.ObjectRepo;
import com.cucumber.framework.utility.Constant;
import com.cucumber.framework.utility.ExcelUtils;

import cucumber.api.java.ca.I;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class SearchWindow {
	public ExcelUtils excel;

	public SearchWindowPageObject searchWinObj;

	public SearchWindow() {
		searchWinObj = new SearchWindowPageObject(ObjectRepo.driver);
	}

	public String excelData(int rowVal, int colVal) throws Exception {
		excel = new ExcelUtils();
		excel.setExcelFile(Constant.Path_TestData + Constant.File_TestData, "Search Window");
		System.err.println();
		return excel.readXLSFile("Search Window", rowVal, colVal);
	}
	
	@Given("^: Click on Qualification Performance Tile$")
	public void click_on_Qualification_Performance_Tile() throws Throwable {
	    searchWinObj.clickQualificationPerTile();
	}

	@Given("^: Click on Search Window Tile$")
	public void click_on_Search_Window_Tile() throws Throwable {
	    searchWinObj.clicksearchWindTile();
	}

	@Then("^: Verify that \"([^\"]*)\" option should be selected bydefault in Module dropdown$")
	public void verify_that_option_should_be_selected_bydefault_in_Module_dropdown(String arg1) throws Throwable {
	    searchWinObj.verifyModDrpDwnDefVal();
	}

	@Given("^: Click on Module dropdown and Select value of Module dropdown of Search Window$")
	public void select_value_of_Module_dropdown_of_Search_Window() throws Throwable {
	    searchWinObj.selectValOfModDrpDwn();
	}
	
	@Then("^: Verify that Search button should be disabled before selecting any option$")
	public void verify_that_Search_button_should_be_disabled_before_selecting_any_option_in_Module_Drop_Down() throws Throwable {
		searchWinObj.verifySearchBtnDisabled();
	}
	
	@Given("^: Click on App dropdown and Select value of App dropdown of Search Window$")
	public void click_on_App_dropdown_and_Select_value_of_App_dropdown_of_Search_Window() throws Throwable {
		searchWinObj.selectValOfAppDrpDwn(excelData(1, 1));
	}

	@Given("^: Click on Search button$")
	public void click_on_Search_button() throws Throwable {
	    searchWinObj.clickSearchBtn();	
	}

	@Then("^: Verify that system should displayed searched value when user click on Search button after selcting module and app$")
	public void verify_that_system_should_displayed_searched_value_when_user_click_on_Search_button_after_selcting_module_and_app() throws Throwable {
	    searchWinObj.verifySearchFun(excelData(1, 1));
	}
	
	@Then("^: Verify that Search button should be enabled after selecting all option$")
	public void verify_that_Search_button_should_be_enabled_after_selecting_all_option() throws Throwable {
	    searchWinObj.verifySearchBtnEnabled();
	}
	
	@Then("^: Verify that Label of Seach Window screen should be displayed as Search Window$")
	public void verify_module_name_as_Search_Window() throws Throwable {
	    searchWinObj.verifySearchWindowLabl();
	}
	
	@Then("^: Verify that system should display Breadcrumb for Search Window as \"([^\"]*)\"$")
	public void verify_that_system_should_display_Breadcrumb_for_Search_Window_as(String arg1) throws Throwable {
	    searchWinObj.verifySearchWindowBreCrum();
	}
	
	@Given("^: Click on Filter By button$")
	public void click_on_Filter_By_button() throws Throwable {
	    searchWinObj.clickFilterByBtn();
	}

	@Given("^: Click on Window Type dropdown$")
	public void click_on_Window_Type_dropdown() throws Throwable {
	   searchWinObj.clickWindowTypeBtn();
	}

	@Given("^: Click on Check All button$")
	public void click_on_Check_All_button() throws Throwable {
	    searchWinObj.clickCheckAllBtn2();
	}

	@Then("^: Verify that All value of Window Type dropdown should be Checked when user Click on Check All button$")
	public void verify_that_All_value_of_Window_Type_dropdown_should_be_Checked_when_user_Click_on_Check_All_button() throws Throwable {
	    searchWinObj.verifyCheckAllFunOfWinTyp();
	}
	
	@Given("^: Click on Uncheck All button$")
	public void click_on_Uncheck_All_button() throws Throwable {
	    searchWinObj.clickUncheckAllBtn2();
	}

	@Then("^: Verify that All value of Window Type dropdown should be Unchecked when user Click on Uncheck All button$")
	public void verify_that_All_value_of_Window_Type_dropdown_should_be_Unchecked_when_user_Click_on_Uncheck_All_button() throws Throwable {
	    searchWinObj.verifyuncheckAllFunOfWinTyp();
	}

	@Given("^: Click on Date Option and select Today option from Date Option dropdown$")
	public void select_Today_option_from_Date_Option_dropdown() throws Throwable {
	    searchWinObj.selectDateOpDrpDwn();
	}

	@Then("^: Verify that Todays Date should be displayed when user select Today option from Date Option dropdown$")
	public void verify_that_Todays_Date_should_be_displayed_when_user_select_Today_option_from_Date_Option_dropdown() throws Throwable {
	    searchWinObj.verifyCurrentDate();
	}
	
	@Given("^: Click on Datepicker$")
	public void click_on_Datepicker() throws Throwable {
	    searchWinObj.clickDatePicker();
	}

	@Given("^: Click on Today button from Datepicker$")
	public void click_on_Today_button_from_Datepicker() throws Throwable {
	    searchWinObj.clickTodayDateBtn();
	}

	@Then("^: Verify that Syatem shoul be displayed Todays Date when user click Today button from Datepicker$")
	public void verify_that_Syatem_shoul_be_displayed_Todays_Date_when_user_click_Today_button_from_Datepicker() throws Throwable {
	    searchWinObj.verifyCurrentDate();
	}
	
	@Given("^:  Click on dropdown of Date Options and Select First value of Date option dropdown$")
	public void click_on_dropdown_of_Date_Options_and_Select_First_value_of_Date_option_dropdown() throws Throwable {
		searchWinObj.selectDateOpDrpDwn();
	}

	@Given("^: Click on Clear link of filter by option in search window$")
	public void click_on_Clear_link_of_filter_by_option_in_search_window() throws Throwable {
	    searchWinObj.clickClearLink();
	}

	@Then("^: Verify that system should clear selected value of Date option dropdown when user click on Clear link$")
	public void verify_that_system_should_clear_selected_value_of_Date_option_dropdown_when_user_click_on_Clear_link() throws Throwable {
	    searchWinObj.verifyClearFun();
	}
	
	@Given("^: Click of Month-Year radio button$")
	public void click_of_Month_Year_radio_button() throws Throwable {
	    searchWinObj.clickMonthYearRadBtn();
	}

	@Given("^: Click on Year dropdown and Select Year$")
	public void click_on_Year_dropdown_and_Select_Year() throws Throwable {
	    searchWinObj.selectYearDrpDwn();
	}

	@Then("^: Verify that Required validation message for Month dropdown should be displayed as Month is required\\.$")
	public void verify_that_Required_validation_message_for_Month_dropdown_should_be_displayed_as_Month_is_required() throws Throwable {
	    searchWinObj.verifyMonthValMsg();
	}
	
	@Given("^: Click on Year dropdown and Press \"([^\"]*)\" Key$")
	public void click_on_Year_dropdown_and_Press_Key(String arg1) throws Throwable {
	    searchWinObj.selectYearDrpDwnAndPressTab();
	}

	@Then("^: Verify that Required validation message for Year dropdown should be displayed as Year is required\\.$")
	public void verify_that_Required_validation_message_for_Year_dropdown_should_be_displayed_as_Year_is_required() throws Throwable {
	    searchWinObj.verifyYearValMsg();
	}
	
	@Given("^: Click on System Favorites$")
	public void click_on_System_Favorites() throws Throwable {
	    searchWinObj.clickSystemFav();
	}

	@Then("^: Verify that System Favorite tab should be displayed using Last Updated By$")
	public void verify_that_System_Favorite_tab_should_be_displayed_using_Last_Updated_By() throws Throwable {
	    searchWinObj.verifySystemfav();
	}
	
	@Then("^: Verify that label of Add To Favorite button should be displayed as Add To Favorite$")
	public void verify_that_label_of_Add_To_Favorite_button_should_be_displayed_as_Add_To_Favorite() throws Throwable {
	    searchWinObj.verifyAddToFavLabl();
	}
	
	@Given("^: Click on Add To Favorite button in search window screen$")
	public void click_on_Add_To_Favorite_button_in_search_window_screen() throws Throwable {
	    searchWinObj.clickAddToFav();
	}

	@Given("^: Click on textbox of Enter Favorite Name in search widnow pop up$")
	public void click_on_textbox_of_Enter_Favorite_Name_in_search_widnow_pop_up() throws Throwable {
	    searchWinObj.clickFavNameTextBox();
	}

	@Given("^: Enter valid data in Favorite name textbox of search window pop up$")
	public void enter_valid_data_in_Favorite_name_textbox_of_search_window_pop_up() throws Throwable {
	    searchWinObj.enterDataInFavName(excelData(2, 2));
	}

	@Given("^: Click on Save button of add to favorite pop up of search window pop up$")
	public void click_on_Save_button_of_add_to_favorite_pop_up_of_search_window_pop_up() throws Throwable {
		searchWinObj.clickSaveBtn();
	}

	@Then("^: Verify that system should save Add To Favorite when user click on Save button with valid data$")
	public void verify_that_system_should_save_Add_To_Favorite_when_user_click_on_Save_button_with_valid_data() throws Throwable {
	    searchWinObj.verifyAddToFavFun(excelData(2, 2));
	}
	
	@Given("^: Enter Name which is already added into Add To Favorite$")
	public void enter_Name_which_is_already_added_into_Add_To_Favorite() throws Throwable {
	    searchWinObj.enterDataInFavName(excelData(3, 2));
	}

	@Then("^: Verify that validation message for Duplicate Name Should be displayed as Favorite Name must be unique\\.$")
	public void verify_that_validation_message_for_Duplicate_Name_Should_be_displayed_as_Favorite_Name_must_be_unique() throws Throwable {
	    searchWinObj.verifyUniqueFavNamValMsg();
	}
	
	@Given("^: Press \"([^\"]*)\" key of Add to Favorite Name Pop Up text Box of Search Window$")
	public void press_key_of_Add_to_Favorite_Name_Pop_Up_text_Box_of_Search_Window(String arg1) throws Throwable {
	    searchWinObj.favNameTextBox.sendKeys(Keys.TAB);
	}

	@Then("^: Verify that Required validation message should be displayed as Favorite Name is required\\.$")
	public void verify_that_Required_validation_message_should_be_displayed_as_Favorite_Name_is_required() throws Throwable {
	    searchWinObj.verifyRequiredFavNamValMsg();
	}
	
	@Given("^: Enter less than (\\d+) characters in Favorite Name in search window pop up$")
	public void enter_less_than_characters_in_Favorite_Name_in_search_window_pop_up(int arg1) throws Throwable {
	    searchWinObj.enterDataInFavName(excelData(4, 2));
	}

	@Then("^: Verify that Minimum validation message should be displayed as Favorite Name must be at least (\\d+) characters\\.$")
	public void verify_that_Minimum_validation_message_should_be_displayed_as_Favorite_Name_must_be_at_least_characters(int arg1) throws Throwable {
	    searchWinObj.verifyFavNamValMsgForMin();
	}
	
	@Given("^: Enter more than (\\d+) characters in Favorite Name in search window pop up$")
	public void enter_more_than_characters_in_Favorite_Name_in_search_window_pop_up(int arg1) throws Throwable {
	    searchWinObj.enterDataInFavName(excelData(5, 2));
	}

	@Then("^: Verify that Maximum validation message should be displayed as Favorite Name cannot be more than (\\d+) characters\\.$")
	public void verify_that_Maximum_validation_message_should_be_displayed_as_Favorite_Name_cannot_be_more_than_characters(int arg1) throws Throwable {
	    searchWinObj.verifyFavNamValMsgForMax();
	}
	
	@Then("^: Verify that the color of Save button should be Green$")
	public void verify_that_the_color_of_Save_button_should_be_Green() throws Throwable {
	    searchWinObj.verifyAddToFavSaveBtnColor();
	}
	
	@Then("^: Verify that the color of Cancel button should be Black$")
	public void verify_that_the_color_of_Cancel_button_should_be_Black() throws Throwable {
	    searchWinObj.verifyAddToFavCancelBtnColor();
	}
	
	@Then("^: Verify that the color of Seach button should be Green$")
	public void verify_that_the_color_of_Seach_button_should_be_Green() throws Throwable {
	    searchWinObj.verifySearchBtnColor();
	}
	
	@Then("^: Verify that the color of Clear All button should be Black$")
	public void verify_that_the_color_of_Clear_All_button_should_be_Black() throws Throwable {
	    searchWinObj.verifyClearAllBtnColor();
	}
	
	@Then("^: Verify that the color of Add To Favorite button should be Blue$")
	public void verify_that_the_color_of_Add_To_Favorite_button_should_be_Blue() throws Throwable {
	    searchWinObj.verifyAddToFavBtnColor();
	}
	
	@Given("^: Click on Delete icon of first recod from My Favorites list$")
	public void click_on_Delete_icon_of_first_recod_from_My_Favorites_list() throws Throwable {
	   searchWinObj.clickDeleteBtnOfMyFav();
	}

	@Then("^: Verify that Confirmation message should be displayed as Are you sure you want to remove Example\\?$")
	public void verify_that_Confirmation_message_should_be_displayed_as_Are_you_sure_you_want_to_remove_Example() throws Throwable {
	    searchWinObj.verifyConfirmationPopUpLabl(excelData(2, 2));
	}
	
	@Then("^: Verify that the color of No button of Confirmation message popup should be Red$")
	public void verify_that_the_color_of_No_button_of_Confirmation_message_popup_should_be_Red() throws Throwable {
	    searchWinObj.verifyConfirmationNoBtnColor();
	}
	
	@Given("^: Click on No button of Confirmation message popup$")
	public void click_on_No_button_of_Confirmation_message_popup() throws Throwable {
	    searchWinObj.clickConfirmationNoBtn();
	}

	@Then("^: Verify that system should not delete the column when user click on No button of Confirmation message popup$")
	public void verify_that_system_should_not_delete_the_column_when_user_click_on_No_button_of_Confirmation_message_popup() throws Throwable {
	    searchWinObj.verifyConfirmationNoBtnFun();
	}
	
	@Then("^: Verify that the color of Yes button of Confirmation message popup should be Green$")
	public void verify_that_the_color_of_Yes_button_of_Confirmation_message_popup_should_be_Green() throws Throwable {
	    searchWinObj.verifyConfirmatinYesBtnColor();
	}
	
	@Given("^: Click on Yes button of Confirmation message popup$")
	public void click_on_Yes_button_of_Confirmation_message_popup() throws Throwable {
	    searchWinObj.clickConfirmationYesBtn();
	}

	@Then("^: Verify that system should be delete the column when user click on Yes button of Confirmation message popup$")
	public void verify_that_system_should_be_delete_the_column_when_user_click_on_Yes_button_of_Confirmation_message_popup() throws Throwable {
	    searchWinObj.verifyConfirmationYesBtnFun();
	}
	
	@Given("^: Click on Clear All button of Search Window Screen$")
	public void click_on_Clear_All_button_of_Search_Window_Screen() throws Throwable {
	    searchWinObj.clickClearAllBtn();
	}

	@Then("^: Verify that system should Clear all the selected data which is displayed on the screen when user click on Clear All button$")
	public void verify_that_system_should_Clear_all_the_selected_data_which_is_displayed_on_the_screen_when_user_click_on_Clear_All_button() throws Throwable {
	    searchWinObj.verifyClearAllFun();
	}
}
