package com.cucumber.framework.stepdefinition;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import com.cucumber.framework.helper.DatePicker.Datepicker;
import com.cucumber.framework.helper.PageObject.AttributesPageObject;
import com.cucumber.framework.helper.PageObject.SetFrequencyPageObject;
import com.cucumber.framework.settings.ObjectRepo;
import com.cucumber.framework.utility.Constant;
import com.cucumber.framework.utility.ExcelUtils;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;


public class SetFrequency {
	public ExcelUtils excel;

	public SetFrequencyPageObject setFrequencyObj;

	public SetFrequency() {
		setFrequencyObj = new SetFrequencyPageObject(ObjectRepo.driver);
	}

	public String excelData(int rowVal, int colVal) throws Exception {
		excel = new ExcelUtils();
		excel.setExcelFile(Constant.Path_TestData + Constant.File_TestData, "Set Frequency");
		System.err.println();
		return excel.readXLSFile("Set Frequency", rowVal, colVal);
	}

	@Given("^: Click on Set Frequency Tile$")
	public void click_on_Set_Frequency_Tile() throws Throwable {
		setFrequencyObj.setFreqRecdsTileClick();
	}

	@Then("^: Verify the module name as Set Frequency$")
	public void verify_the_module_name_as_Set_Frequency() throws Throwable {
		setFrequencyObj.verifySetFreLabl();
	}

	@Then("^: Verify that all the records of Screen should be display in ascending order as per Frequency Name$")
	public void verify_that_all_the_records_of_Screen_should_be_display_in_ascending_order_as_per_Frequency_Name() throws Throwable {
		new AttributesPageObject(ObjectRepo.driver).verifyAscendingOrder("Frequency Name");
	}

	@Given("^: Enter the data into search box of Set Frequency Screen$")
	public void enter_the_data_into_search_box_of_Set_Frequency_Screen() throws Throwable {
		setFrequencyObj.frequencySrchBoxSendkeys(excelData(1, 1));
	}

	@Then("^: Verify the search results of Set Frequency screen$")
	public void verify_the_search_results_of_Set_Frequency_screen() throws Throwable {
		setFrequencyObj.verifysearch(excelData(1, 1));
	}

	@Given("^: Click on \"([^\"]*)\" button of Set Frequency screen$")
	public void click_on_button_of_Set_Frequency_screen(String arg1) throws Throwable {
		setFrequencyObj.clickAddNewBtn();
	}

	@Then("^: Verify that system should redirect to the Add/Edit Frequency screen when user click on Add New button$")
	public void verify_that_system_should_redirect_to_the_Add_Edit_Frequency_screen_when_user_click_on_Add_New_button() throws Throwable {
		setFrequencyObj.verifyAddEditFreLabl();
	}

	@Given("^: Click on Frequency Name textbox$")
	public void click_on_Frequency_Name_textbox() throws Throwable {
		setFrequencyObj.clickfrencyNameTextBox();
	}

	@Given("^: Press the \"([^\"]*)\" Key in Frequency Name textbox$")
	public void press_the_Key_in_Frequency_Name_textbox(String arg1) throws Throwable {
		setFrequencyObj.freNameTextBox.sendKeys(Keys.TAB);
	}

	@Then("^: Verify the validation message as Frequency Name is required\\.$")
	public void verify_the_validation_message_as_Frequency_Name_is_required() throws Throwable {
		setFrequencyObj.verifyFrencyNameTexBoxValMsgl();
	}

	@Given("^: Enter Frequency Name value Less than (\\d+) chracters$")
	public void enter_Frequency_Name_value_Less_than_chracters(int arg1) throws Throwable {
		setFrequencyObj.frequencyNameSendkeys(excelData(2, 1));
	}

	@Then("^: Verify the validation message as Frequency Name must be at least (\\d+) characters\\.$")
	public void verify_the_validation_message_as_Frequency_Name_must_be_at_least_characters(int arg1) throws Throwable {
		setFrequencyObj.verifyFrencyNameValMsglForMinSize();
	}

	@Given("^: Enter Frequency Name value more than (\\d+) characters$")
	public void enter_Frequency_Name_value_more_than_characters(int arg1) throws Throwable {
		setFrequencyObj.frequencyNameSendkeys(excelData(3, 1));
	}

	@Then("^: Verify validation message Frequency Name cannot be more than (\\d+) characters long\\.$")
	public void verify_validation_message_Frequency_Name_cannot_be_more_than_characters_long(int arg1) throws Throwable {
		setFrequencyObj.verifyFrencyNameValMsglForMaxSize();
	}

	@Given("^: Enter Duplicate Frequency Name$")
	public void enter_Duplicate_Frequency_Name() throws Throwable {
		setFrequencyObj.frequencyNameSendkeys(excelData(4, 1));
	}

	@Then("^: Verify Unique validation message Frequency Name must be unique\\.$")
	public void verify_Unique_validation_message_Frequency_Name_must_be_unique() throws Throwable {
		setFrequencyObj.verifyFrencyNameValMsglForDup();
	}

	@Given("^: Enter a valid Frequency Name$")
	public void enter_a_valid_Frequency_Name() throws Throwable {
		setFrequencyObj.frequencyNameSendkeys(excelData(5, 1));
	}

	@Then("^: Verify system should accept the Frequency name value without any validation message$")
	public void verify_system_should_accept_the_Frequency_name_value_without_any_validation_message() throws Throwable {
		setFrequencyObj.verifyFrencyNameValMsglForVal();
	}

	@Given("^: Select dropdown of Apps and Press \"([^\"]*)\" Key$")
	public void select_dropdown_of_Apps_and_Press_Key(String arg1) throws Throwable {
		setFrequencyObj.selectDrpDwnApp();
		setFrequencyObj.appDrpDwn.sendKeys(Keys.TAB);
	}

	@Then("^: Verify the validation message as App is required\\.$")
	public void verify_the_validation_message_as_App_is_required() throws Throwable {
		setFrequencyObj.verifyAppDrpDwnValMsg();
	}

	@Given("^: Select Master App radio button$")
	public void select_Master_App_radio_button() throws Throwable {
		setFrequencyObj.clickMasAppRadBtn();
	}

	@Given("^: Select dropdown of Master App and Press \"([^\"]*)\" Key$")
	public void select_dropdown_of_Master_App_and_Press_Key(String arg1) throws Throwable {
		setFrequencyObj.selectDrpDwnMasterApp();
		setFrequencyObj.masterAppDrpDwn.sendKeys(Keys.TAB);
	}

	@Then("^: Verify the validation message as Master App is required\\.$")
	public void verify_the_validation_message_as_Master_App_is_required() throws Throwable {
		setFrequencyObj.verifyMasAppDrpDwnValMsg();
	}

	@Then("^: Verify that label of hourly frequency type should be displayed as \"([^\"]*)\"$")
	public void verify_that_label_of_hourly_frequency_type_should_be_displayed_as(String arg1) throws Throwable {
		setFrequencyObj.verifyHourlyLabl();
	}

	@Then("^: Verify that label of daily frequency type should be displayed as \"([^\"]*)\"$")
	public void verify_that_label_of_daily_frequency_type_should_be_displayed_as(String arg1) throws Throwable {
		setFrequencyObj.verifyDailyLabl();
	}

	@Then("^: Verify that label of weekly frequency type should be displayed as \"([^\"]*)\"$")
	public void verify_that_label_of_weekly_frequency_type_should_be_displayed_as(String arg1) throws Throwable {
		setFrequencyObj.verifyWeeklyLabl();
	}

	@Then("^: Verify that label of Monthly frequency type should be displayed as \"([^\"]*)\"$")
	public void verify_that_label_of_Monthly_frequency_type_should_be_displayed_as(String arg1) throws Throwable {
		setFrequencyObj.verifyMonthlyLabl();
	}

	@Then("^: Verify that user should able to select Date for Start Date$")
	public void verify_that_user_should_able_to_select_Date_for_Start_Date() throws Throwable {
		String toSplit = excelData(6, 2);
		String[] splitted = toSplit.split("[-/]");
		Thread.sleep(1000);
		WebElement CalPath = ObjectRepo.driver.findElement(By.xpath("//*[@ng-click='vm.open1()']"));

		Datepicker.Dateselection(splitted[0], Integer.parseInt(splitted[1]), Integer.parseInt(splitted[2]), CalPath);
	}

	@Given("^: Click on Every textbox for Hourly type$")
	public void click_on_Every_textbox_for_Hourly_type() throws Throwable {
		setFrequencyObj.clickhourlyTextbox();
	}

	@Given("^: Press \"([^\"]*)\" Key on Every text box for Hourly type$")
	public void press_Key_on_Every_text_box_for_Hourly_type(String arg1) throws Throwable {
		setFrequencyObj.hourlyTextbox.sendKeys(Keys.TAB);
	}

	@Then("^: Verify that system should display required message as \"([^\"]*)\"$")
	public void verify_that_system_should_display_required_message_as(String arg1) throws Throwable {
		setFrequencyObj.verifyhourlyTextBoxValMsg();
	}

	@Given("^: Enter Every textbox Value as \"([^\"]*)\"$")
	public void enter_Every_textbox_Value_as(String arg1) throws Throwable {
		setFrequencyObj.enterhourlyTextbox(excelData(7, 3));
	}

	@Then("^: Verify that system should display maximum Hour validation message as \"([^\"]*)\"$")
	public void verify_that_system_should_display_maximum_Hour_validation_message_as(String arg1) throws Throwable {
		setFrequencyObj.verifyhourlyTextBoxValMsgForMax();
	}

	@Given("^: Enter Every textbox Value as string$")
	public void enter_Every_textbox_Value_as_string() throws Throwable {
		setFrequencyObj.enterhourlyTextbox(excelData(8, 3));
	}

	@Then("^: Verify that system should display invalid validation message as \"([^\"]*)\"$")
	public void verify_that_system_should_display_invalid_validation_message_as(String arg1) throws Throwable {
		setFrequencyObj.verifyEveryTextBoxValMsgForString();
	}

	@Given("^: Enter Valid Value in Every textbox$")
	public void enter_Valid_Value_in_Every_textbox() throws Throwable {
		setFrequencyObj.enterhourlyTextbox(excelData(9, 3));
	}

	@Then("^: Verify that system should not display any validation message when a user enter Valid value into textbox$")
	public void verify_that_system_should_not_display_any_validation_message_when_a_user_enter_Valid_value_into_textbox() throws Throwable {
		setFrequencyObj.verifyHourlytextboxValMsglForValData();
	}

	@Given("^: Select Weekly radio button for Weekly Frequency type$")
	public void select_Weekly_radio_button_for_Weekly_Frequency_type() throws Throwable {
		setFrequencyObj.selectWeeklyRadBtn();
	}

	@Given("^: Click on Every textbox of weekly type$")
	public void click_on_Every_textbox() throws Throwable {
		setFrequencyObj.clickWeeklyTextbox();
	}

	@Given("^: Press \"([^\"]*)\" Key on Weekly Frequency type$")
	public void press_Key_for_Weekly_Frequency_type(String arg1) throws Throwable {
		setFrequencyObj.weeklyTextbox.sendKeys(Keys.TAB);
	}

	@Then("^: Verify that system should display required message for No input data as \"([^\"]*)\"$")
	public void verify_that_system_should_display_required_message_for_No_input_data_as(String arg1) throws Throwable {
		setFrequencyObj.verifyweeklyTextBoxValMsg();
	}

	@Given("^: Enter Every textbox Value for more than (\\d+) week as \"([^\"]*)\"$")
	public void enter_Every_textbox_Value_for_more_than_week_as(int arg1, String arg2) throws Throwable {
		setFrequencyObj.enterweeklyTextbox(excelData(10, 4));
	}

	@Then("^: Verify that system should display maximum validation message for weekly type as \"([^\"]*)\"$")
	public void verify_that_system_should_display_maximum_validation_message_for_weekly_type_as(String arg1) throws Throwable {
		setFrequencyObj.verifyweeklyTextBoxValMsgForMax();
	}

	@Given("^: Enter Every textbox Value for string as \"([^\"]*)\"$")
	public void enter_Every_textbox_Value_for_string_as(String arg1) throws Throwable {
		setFrequencyObj.enterweeklyTextbox(excelData(11, 4));
	}

	@Then("^: Verify that system should display invalid validation message for weekly type as \"([^\"]*)\"$")
	public void verify_that_system_should_display_invalid_validation_message_for_weekly_type_as(String arg1) throws Throwable {
		setFrequencyObj.verifyEveryTextBoxValMsgForString();
	}

	@Given("^: Enter Valid Value in Every textbox for weekly type$")
	public void enter_Valid_Value_in_Every_textbox_for_weekly_type() throws Throwable {
		setFrequencyObj.enterweeklyTextbox(excelData(12, 4));
	}

	@Then("^: Verify that system should not display any validation message when a user enter Valid value into textbox of Weekly type$")
	public void verify_that_system_should_not_display_any_validation_message_when_a_user_enter_Valid_value_into_textbox_of_Weekly_type() throws Throwable {
		setFrequencyObj.verifyWeeklytextboxValMsglForValData();
	}

	@Given("^: Select Daily radio button for Daily Frequency type$")
	public void select_Daily_radio_button_for_Daily_Frequency_type() throws Throwable {
		setFrequencyObj.selectDailyRadBtn();
	}

	@Given("^: Click on Every textbox of Daily type$")
	public void click_on_Every_textbox_of_Daily_type() throws Throwable {
		setFrequencyObj.clickDailyTextbox();
	}

	@Given("^: Press \"([^\"]*)\" Key on Daily Type$")
	public void press_Key_on_Daily_Type(String arg1) throws Throwable {
		setFrequencyObj.dailyTextbox.sendKeys(Keys.TAB);
	}

	@Then("^: Verify that system should display required message for Daily Type as \"([^\"]*)\"$")
	public void verify_that_system_should_display_required_message_for_Daily_Type_as(String arg1) throws Throwable {
		setFrequencyObj.verifyDailyTextBoxValMsg();
	}

	@Given("^: Enter Every textbox Value for days as \"([^\"]*)\"$")
	public void enter_Every_textbox_Value_for_days_as(String arg1) throws Throwable {
		setFrequencyObj.enterDailyTextbox(excelData(13, 5));
	}

	@Then("^: Verify that system should display maximum validation message for days as \"([^\"]*)\"$")
	public void verify_that_system_should_display_maximum_validation_message_for_days_as(String arg1) throws Throwable {
		setFrequencyObj.verifyDailyTextBoxValMsgForMax();
	}

	@Given("^: Enter Every textbox Value for days in string as \"([^\"]*)\"$")
	public void enter_Every_textbox_Value_for_days_in_string_as(String arg1) throws Throwable {
		setFrequencyObj.enterDailyTextbox(excelData(14, 5));
	}

	@Then("^: Verify that system should display invalid validation message for days as \"([^\"]*)\"$")
	public void verify_that_system_should_display_invalid_validation_message_for_days_as(String arg1) throws Throwable {
		setFrequencyObj.verifyEveryTextBoxValMsgForString();
	}

	@Given("^: Enter Valid Value in Every textbox for days$")
	public void enter_Valid_Value_in_Every_textbox_for_days() throws Throwable {
		setFrequencyObj.enterDailyTextbox(excelData(15, 5));
	}

	@Then("^: Verify that system should not display any validation message when a user enter Valid value into textbox for Daily type$")
	public void verify_that_system_should_not_display_any_validation_message_when_a_user_enter_Valid_value_into_textbox_for_Daily_type() throws Throwable {
		setFrequencyObj.verifyDailytextboxValMsglForValData();
	}

	@Given("^: Select Date from End the Date Picker$")
	public void select_Date_from_the_End_Date_Picker() throws Throwable {
		System.out.println("Blank Method");
	}

	@Then("^: Verify that user should able to select Date for End Date in Set Frequency Add new button$")
	public void verify_that_user_should_able_to_select_Date_for_End_Date_in_Set_Frequency_Add_new_button() throws Throwable {
		String toSplit = excelData(16, 6);
		String[] splitted = toSplit.split("[-/]");
		Thread.sleep(1000);
		WebElement CalPath = ObjectRepo.driver.findElement(By.xpath("//*[@ng-click='vm.open2()']"));

		Datepicker.Dateselection(splitted[0], Integer.parseInt(splitted[1]), Integer.parseInt(splitted[2]), CalPath);
	}

	@Given("^: Select End After radio button$")
	public void select_End_After_radio_button() throws Throwable {
		setFrequencyObj.selectEndAfterRadBtn();
	}

	@Given("^: Click on End After\\(Occurrences\\) textbox$")
	public void click_on_End_After_Occurrences_textbox() throws Throwable {
		setFrequencyObj.clickEndAfterTextBtn();
	}

	@Given("^: Press \"([^\"]*)\" Key on End After\\(Occurrences\\) textbox$")
	public void press_Key_on_End_After_Occurrences_textbox(String arg1) throws Throwable {
		setFrequencyObj.endAfterTextbox.sendKeys(Keys.TAB);
	}

	@Then("^: Verify that system should display required message for no data as \"([^\"]*)\"$")
	public void verify_that_system_should_display_required_message_for_no_data_as(String arg1) throws Throwable {
		setFrequencyObj.verifyEndAfterTextBoxValMsg();
	}

	@Given("^: Enter End textbox Value as \"([^\"]*)\"$")
	public void enter_End_textbox_Value_as(String arg1) throws Throwable {
		setFrequencyObj.enterEndAfterTextbox(excelData(17, 7));
	}

	@Then("^: Verify that system should display maximum validation message for End After as \"([^\"]*)\"$")
	public void verify_that_system_should_display_maximum_validation_message_for_End_After_as(String arg1) throws Throwable {
		setFrequencyObj.verifyEndAfterTextBoxValMsgForMax();
	}

	@Given("^: Enter End After textbox Value as \"([^\"]*)\"$")
	public void enter_End_After_textbox_Value_as(String arg1) throws Throwable {
		setFrequencyObj.enterEndAfterTextbox(excelData(18, 7));
	}

	@Then("^: Verify that system should display invalid validation message for End After as \"([^\"]*)\"$")
	public void verify_that_system_should_display_invalid_validation_message_for_End_After_as(String arg1) throws Throwable {
		setFrequencyObj.verifyEndAfterTextBoxValMsgFoInNum();
	}
	
	@Given("^: Enter Valid Value in End After\\(Occurrences\\) textbox$")
	public void enter_Valid_Value_in_End_After_Occurrences_textbox() throws Throwable {
	    setFrequencyObj.enterEndAfterTextbox(excelData(19, 7));
	}

	@Then("^: Verify that system should not display any validation message when a user enter Valid value into textbox in End After Text Box$")
	public void verify_that_system_should_not_display_any_validation_message_when_a_user_enter_Valid_value_into_textbox_in_End_After_Text_Box() throws Throwable {
	    setFrequencyObj.verifyEndAftertextboxValMsglForValData();
	}
	
	@Then("^: Verify that system should display current time in frequency time field$")
	public void verify_that_system_should_display_current_time_in_frequency_time_field() throws Throwable {
	    setFrequencyObj.verifyCurrentTime();
	}
	
	@Given("^: Checked Send Email Reminder checkbox$")
	public void checked_Send_Email_Reminder_checkbox() throws Throwable {
	    setFrequencyObj.selectSendEmailCheckBox();
	}

	@Given("^: Click on Email Address textbox$")
	public void click_on_Email_Address_textbox() throws Throwable {
	    setFrequencyObj.selectSendEmailTextBox();
	}

	@Given("^: Press \"([^\"]*)\" Key on Email textbox$")
	public void press_Key_on_Email_textbox(String arg1) throws Throwable {
	    setFrequencyObj.emailRemTextBox.sendKeys(Keys.TAB);
	}

	@Then("^: Verify that system should display required validation message for Email Address texbox as \"([^\"]*)\"$")
	public void verify_that_system_should_display_required_validation_message_for_as(String arg1) throws Throwable {
	    setFrequencyObj.verifySendEmailValMsg();
	}
	
	@Given("^: Enter Email Address as \"([^\"]*)\"$")
	public void enter_Email_Address_as(String arg1) throws Throwable {
	    setFrequencyObj.enterEmailRemTextbox(excelData(20, 8));
	    
	}

	@Then("^: Verify that system should display invalid validation message for Invaild email Address as \"([^\"]*)\"$")
	public void verify_that_system_should_display_invalid_validation_message_for_Invaild_email_Address_as(String arg1) throws Throwable {
	    setFrequencyObj.verifySendEmailValMsgForInValEmail();
	}
	
	@Given("^: Enter Valid Email Address$")
	public void enter_Valid_Email_Address() throws Throwable {
	    setFrequencyObj.enterEmailRemTextbox(excelData(21, 8));
	}

	@Then("^: Verify that system should not display any validation message when user enter Valid Email Address$")
	public void verify_that_system_should_not_display_any_validation_message_when_user_enter_Valid_Email_Address() throws Throwable {
	    setFrequencyObj.verifyEmailRemiForValData();
	}
	
	@Given("^: Add Frequency Name for Frequency Name field$")
	public void add_Frequency_Name_for_Frequency_Name_field() throws Throwable {
	    setFrequencyObj.frequencyNameSendkeys(excelData(22, 1));
	}

	@Given("^: Select App Name from App dropdown$")
	public void select_App_Name_from_App_dropdown() throws Throwable {
	    setFrequencyObj.selectDrpDwnAppValData();
	}

	@Given("^: Select Frequency Type and Add required data$")
	public void select_Frequency_Type_and_Add_required_data() throws Throwable {
	    setFrequencyObj.enterhourlyTextbox(excelData(22, 3));
	}

	@Given("^: Select Date from Start Date Picker$")
	public void select_Date_from_Start_Date_Picker() throws Throwable {
		String toSplit = excelData(22, 2);
		String[] splitted = toSplit.split("[-/]");
		Thread.sleep(1000);
		WebElement CalPath = ObjectRepo.driver.findElement(By.xpath("//*[@ng-click='vm.open1()']"));

		Datepicker.Dateselection(splitted[0], Integer.parseInt(splitted[1]), Integer.parseInt(splitted[2]), CalPath);
	}

	@Given("^: Select Date from End Date Picker$")
	public void select_Date_from_End_Date_Picker() throws Throwable {
		String toSplit = excelData(22, 6);
		String[] splitted = toSplit.split("[-/]");
		Thread.sleep(1000);
		WebElement CalPath = ObjectRepo.driver.findElement(By.xpath("//*[@ng-click='vm.open2()']"));

		Datepicker.Dateselection(splitted[0], Integer.parseInt(splitted[1]), Integer.parseInt(splitted[2]), CalPath);
	}

	@Given("^: Select Role\\(s\\) and Click on Add button$")
	public void select_Role_s_and_Click_on_Add_button() throws Throwable {
	    setFrequencyObj.selectAppRole();
	}

	@Given("^: Click on Save button$")
	public void click_on_Save_button() throws Throwable {
	    setFrequencyObj.saveBtn();
	}

	@Then("^: Verify that system should create new set frequency by Click on Add New button$")
	public void verify_that_system_should_create_new_set_frequency_by_Click_on_Add_New_button() throws Throwable {
	    setFrequencyObj.verifyFirFreName(excelData(22, 1));
	}
}
