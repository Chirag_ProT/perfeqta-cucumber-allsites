Feature: Module Name: Create Window
Executed script on "https://test1.beperfeqta.com/mdav33"

@chrome @ignore
Scenario: Verify the functionality of Create Window
    Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Qualification Performance Tile of Create window
    And : Click on Create Window Tile
		And : Enter valid data in Create Window screen
		And : Click on Save Button of Create Window
		Then : Verify that Window should be created
		
@chrome @production
Scenario: Verify Required validation message for Window Name in Create Window screen
	Given : Navigate to the URL 
  And : Enter valid credentials and Click on Login Button 
  And : Click on Qualification Performance Tile of Create window
  And : Click on Create Window Tile
	And : Click on textbox of Window Name field 
	And : Press "TAB" key of Create Window 
	Then : Verify that Required validation message should be displayed as Window Name is required.

@chrome 
Scenario: Verify the color of Required validation message for Window Name
	Given : Navigate to the URL 
  And : Enter valid credentials and Click on Login Button 
  And : Click on Qualification Performance Tile of Create window
  And : Click on Create Window Tile
	And : Click on textbox of Window Name field 
	And : Press "TAB" key of Create Window 
	Then : Verify that color of Required validation message should be Red
	
@chrome 
Scenario: Verify Minimum character validation message for Window Name
	Given : Navigate to the URL 
  And : Enter valid credentials and Click on Login Button 
  And : Click on Qualification Performance Tile of Create window
  And : Click on Create Window Tile
	And : Click on textbox of Window Name field 
	And : Enter less than 2 character in the textbox of Window Name
	Then : Verify that Minimum character validation message for Window Name should be displayed as Window Name must be at least 2 characters.
	
@chrome @demo
Scenario: Verify color of Minimum validation message for Window Name
	Given : Navigate to the URL 
  And : Enter valid credentials and Click on Login Button 
  And : Click on Qualification Performance Tile of Create window
  And : Click on Create Window Tile
	And : Click on textbox of Window Name field 
	And : Enter less than 2 character in the textbox of Window Name
	Then : Verify that color of Minimum validation message should be Red

@chrome 
Scenario: Verify Maximum validation message for Window Name
	Given : Navigate to the URL 
  And : Enter valid credentials and Click on Login Button 
  And : Click on Qualification Performance Tile of Create window
  And : Click on Create Window Tile
	And : Click on textbox of Window Name field 
	And : Enter more than 50 character in the textbox of Window Name
	Then : Verify that Maximum character validation message for Window Name should be displayed as Window Name cannot be more than 50 characters.
	
@chrome 
Scenario: Verify color of Maximum validation message for Window Name
	Given : Navigate to the URL 
  And : Enter valid credentials and Click on Login Button 
  And : Click on Qualification Performance Tile of Create window
  And : Click on Create Window Tile
	And : Click on textbox of Window Name field 
	And : Enter more than 50 character in the textbox of Window Name
	Then : Verify that color of Maximum validation message should be Red

@chrome 
Scenario: Verify validation message for Duplicate Window Name
	Given : Navigate to the URL 
  And : Enter valid credentials and Click on Login Button 
  And : Click on Qualification Performance Tile of Create window
  And : Click on Create Window Tile
	And : Click on textbox of Window Name field 
	And : Enter Window Name which is already added
	Then : Verify that validation message for Duplicate Window Name should be displayed as Window Name must be unique.

@chrome 
Scenario: Verify color of validation message for Duplicate Window Name
	Given : Navigate to the URL 
  And : Enter valid credentials and Click on Login Button 
  And : Click on Qualification Performance Tile of Create window
  And : Click on Create Window Tile
	And : Click on textbox of Window Name field 
	And : Enter Window Name which is already added
	Then : Verify that color of validation message for Duplicate Window Name should be Red


#-----Description-----

@chrome @productions
Scenario: Verify Minimum validation message for Description textbox
	Given : Navigate to the URL 
  And : Enter valid credentials and Click on Login Button 
  And : Click on Qualification Performance Tile of Create window
  And : Click on Create Window Tile
	And : Click on textbox of Description field 
	And : Enter less than 2 character in the Description textbox 
	Then : Verify that Minimum character validation message for Window Name should be displayed as Description must be at least 2 characters.

@chrome 
Scenario: Verify color of Minimum validation message for Description textbox
	Given : Navigate to the URL 
  And : Enter valid credentials and Click on Login Button 
  And : Click on Qualification Performance Tile of Create window
  And : Click on Create Window Tile
	And : Click on textbox of Description field 
	And : Enter less than 2 character in the Description textbox
	Then : Verify that color of Minimum validation message for Description should be Red

@chrome 
Scenario: Verify Maximum validation message for Description textbox
	Given : Navigate to the URL 
  And : Enter valid credentials and Click on Login Button 
  And : Click on Qualification Performance Tile of Create window
  And : Click on Create Window Tile
	And : Click on textbox of Description field 
	And : Enter more than 800 character in the Description textbox
	Then : Verify that Maximum character validation message for Window Name should be displayed as Description cannot be more than 800 characters.
	
@chrome 
Scenario: Verify color of Maximum validation message for Description textbox
	Given : Navigate to the URL 
  And : Enter valid credentials and Click on Login Button 
  And : Click on Qualification Performance Tile of Create window
  And : Click on Create Window Tile
	And : Click on textbox of Description field 
	And : Enter more than 800 character in the Description textbox
	Then : Verify that color of Maximum validation for Description message should be Red

@chrome 
Scenario: Verify the App Details label in Create Window screen
	Given : Navigate to the URL 
  And : Enter valid credentials and Click on Login Button 
  And : Click on Qualification Performance Tile of Create window
  And : Click on Create Window Tile
	Then : Verify that label should be displayed as App Details

@chrome 
Scenario: Verify Test Type and Sampling Plan label in Create Window screen
	Given : Navigate to the URL 
  And : Enter valid credentials and Click on Login Button 
  And : Click on Qualification Performance Tile of Create window
  And : Click on Create Window Tile
	Then : Verify that label should be displayed as Test Type and Sampling Plan

@chrome 
Scenario: Verify Frequency Settings label in Create Window sreen
	Given : Navigate to the URL 
  And : Enter valid credentials and Click on Login Button 
  And : Click on Qualification Performance Tile of Create window
  And : Click on Create Window Tile
	Then : Verify that label should be displayed as Frequency Settings
	
@chrome 
Scenario: Verify Default Selected Value of Module Dropdown
	Given : Navigate to the URL 
  And : Enter valid credentials and Click on Login Button 
  And : Click on Qualification Performance Tile of Create window
  And : Click on Create Window Tile
	Then : Verify that Default Selected Value of Module Dropdown should be displayed as -- Please Select --

@chrome 
Scenario: Verify Default Selected Value of App Dropdown
	Given : Navigate to the URL 
  And : Enter valid credentials and Click on Login Button 
  And : Click on Qualification Performance Tile of Create window
  And : Click on Create Window Tile
	Then : Verify that Default Selected Value of App Dropdown should be displayed as -- Please Select --

@chrome 
Scenario: Verify all values in dropdown list of Module 
	Given : Navigate to the URL 
  And : Enter valid credentials and Click on Login Button 
  And : Click on Qualification Performance Tile of Create window
  And : Click on Create Window Tile
	And : Click on Dropdown of Module
	Then : Verify that all the values of Module should be displayed as "-- Please Select --\n" + "Blood\n" + "Equipment\n" + "Reagent QC"
	
@chrome 
Scenario:  Verify Required validation message for Module dropdown in Create Window screen
	Given : Navigate to the URL 
  And : Enter valid credentials and Click on Login Button 
  And : Click on Qualification Performance Tile of Create window
  And : Click on Create Window Tile
	And : Click on Dropdown of Module
	And : Press "TAB" key of Create Window 
	Then : Verify that Required validation message should be displayed as Module is required.
	
@chrome 
Scenario: Verify the color of Required validation message for Module dropdown
	Given : Navigate to the URL 
  And : Enter valid credentials and Click on Login Button 
  And : Click on Qualification Performance Tile of Create window
  And : Click on Create Window Tile
	And : Click on dropdown of Module 
	And : Press "TAB" key of Create Window 
	Then : Verify that color of Required validation message for Module should be Red

@chrome 
Scenario:  Verify Required validation message for App dropdown in Create Window screen
	Given : Navigate to the URL 
  And : Enter valid credentials and Click on Login Button 
  And : Click on Qualification Performance Tile of Create window
  And : Click on Create Window Tile
	And : Click on Dropdown of App
	And : Press "TAB" key of Create Window 
	Then : Verify that Required validation message should be displayed as App is required.
	
@chrome 
Scenario: Verify the color of Required validation message for App dropdown
	Given : Navigate to the URL 
  And : Enter valid credentials and Click on Login Button 
  And : Click on Qualification Performance Tile of Create window
  And : Click on Create Window Tile
	And : Click on dropdown of App 
	And : Press "TAB" key of Create Window 
	Then : Verify that color of Required validation message for App should be Red
	
@chrome 
Scenario:  Verify Required validation message for Attributes/Entity Key Attributes Dropdown
	Given : Navigate to the URL 
  And : Enter valid credentials and Click on Login Button 
  And : Click on Qualification Performance Tile of Create window
  And : Click on Create Window Tile
  And : Select value into the dropdown of Module
	And : Select value into the dropdown of App	
	And : Click on Dropdown of Attributes/Entity Key Attributes
	Then : Verify that Required validation message should be displayed as Attributes/Entity Key Attributes is required.
	
@chrome  
Scenario: Verify the color of Required validation message for Attributes/Entity Key Attributes Dropdown
	Given : Navigate to the URL 
  And : Enter valid credentials and Click on Login Button 
  And : Click on Qualification Performance Tile of Create window
  And : Click on Create Window Tile
  And : Select value into the dropdown of Module
	And : Select value into the dropdown of App
	And : Click on Dropdown of Attributes/Entity Key Attributes
	Then : Verify that color of Required validation message for Attributes/Entity should be Red
	
@chrome 
Scenario: Verify functionality of input Dropdown when Attributes/Entity Key Attributes is not selected
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
  And : Click on Qualification Performance Tile of Create window
  And : Click on Create Window Tile
  And : Select value into the dropdown of Module
	And : Select value into the dropdown of App
	Then : Verify that input dropdown should be disable when Attributes/Entity Key Attributes is not selected 
	
@chrome 
Scenario: Verify functionality of Attributes/Entity input Add button in Create Window screen
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
  And : Click on Qualification Performance Tile of Create window
  And : Click on Create Window Tile
  And : Select value into the dropdown of Module
	And : Select value into the dropdown of App
	And : Click on Dropdown of Attributes/Entity Key Attributes 
	And : Select value from Attributes/Entity dropdown
	And : Click on Add button in Create Window screen
	Then : Verify that sytem should allow to add multiple Attributes/Entity Key Attributes when user click on Add button
	
@chrome 
Scenario: Verify functionality of Attributes/Entity input Remove button in Create Window screen
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
  And : Click on Qualification Performance Tile of Create window
  And : Click on Create Window Tile
  And : Select value into the dropdown of Module
	And : Select value into the dropdown of App
	And : Click on Dropdown of Attributes/Entity Key Attributes 
	And : Select value from Attributes/Entity dropdown
	And : Click on Add button in Create Window screen
	And : Click on Remove Button in Create Window screen
	Then : Verify that sytem should allow to remove value of Attributes/Entity Key Attributes when user click on Remove button

@chrome 
Scenario: Verify default selection of Test Type radio button for Test Type and Sampling Plan section in Create Window screen
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
  And : Click on Qualification Performance Tile of Create window
  And : Click on Create Window Tile
	Then : Verify that radio button of Procedure should be selected by default in Create Window screen
	
@chrome 
Scenario: Verify the functionality of Radio button of Test Type in Create Window screen
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
  And : Click on Qualification Performance Tile of Create window
  And : Click on Create Window Tile
	And : Click on radio button of Question in Test Type
	And : Click on radio button of Procedure in Test Type
	Then : Verify that at time only one radio button should be selectable when user select radio button

@chrome 
Scenario: Verify Required validation message for dropdown of Procedure Test Type
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
  And : Click on Qualification Performance Tile of Create window
  And : Click on Create Window Tile
  And : Select value into the dropdown of Module
	And : Select value into the dropdown of Apps
	And : Click on dropdown of Procedure Test Type
	Then : Verify that Required validation message for dropdown of Procedure Test Type should be displayed as Procedure is required.
	
@chrome 
Scenario: Verify the color of Required validation message for dropdown of Procedure Test Type
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
  And : Click on Qualification Performance Tile of Create window
  And : Click on Create Window Tile
  And : Select value into the dropdown of Module
	And : Select value into the dropdown of Apps
	And : Click on dropdown of Procedure Test Type
	Then : Verify that the color of Required validation message for dropdown of Procedure Test Type should be Red
	
@chrome 
Scenario: Verify Required validation message for dropdown of Question Test Type
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
  And : Click on Qualification Performance Tile of Create window
  And : Click on Create Window Tile
  And : Select value into the dropdown of Module
	And : Select value into the dropdown of Apps
	And : Click on dropdown of Question Test Type
	Then : Verify that Required validation message for dropdown of Question Test Type should be displayed as Question is required.
	
@chrome 
Scenario: Verify the color of Required validation message for dropdown of Question Test Type
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
  And : Click on Qualification Performance Tile of Create window
  And : Click on Create Window Tile
  And : Select value into the dropdown of Module
	And : Select value into the dropdown of Apps
	And : Click on dropdown of Question Test Type
	Then : Verify that the color of Required validation message for dropdown of Question Test Type should be Red
	
@chrome 
Scenario: Verify Required validation message for Window Type dropdown
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
  And : Click on Qualification Performance Tile of Create window
  And : Click on Create Window Tile
  And : Select value into the dropdown of Module
	And : Select value into the dropdown of Apps
	And : Click on dropdown of Window Type field
	Then : Verify that Required validation message for Window Type dropdown should be displayed as Window Type is required.
	
@chrome 
Scenario: Verify the color of Required validation message for Window Type dropdown
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
  And : Click on Qualification Performance Tile of Create window
  And : Click on Create Window Tile
  And : Select value into the dropdown of Module
	And : Select value into the dropdown of Apps
	And : Click on dropdown of Window Type field
	Then : Verify that color of Required validation message for Window Type should be Red
	
@chrome 
Scenario: Verify all values in dropdown of Window Type 
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
  And : Click on Qualification Performance Tile of Create window
  And : Click on Create Window Tile
	And : Click on dropdown of Window Type field
	Then : Verify that system should displayed all the values in dropdown of Window Type when user click on dropdown of Window Type field
	
@chrome @production
Scenario: Verify Required validation message for Window Plan Dropdown in Create Window screen
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
  And : Click on Qualification Performance Tile of Create window
  And : Click on Create Window Tile
	And : Click on dropdown of Window Type field
	And : Select First record in the dropdown of Window Type field
	And : Click on dropdown of Window Plan
	And : Select first record in the dropdown of Window Plan
	And : Select "--Please Select--" in the dropdown of Window Plan
	Then :  Verify that Required validation message for Window Plan Dropdown should be displayed as Window Plan is required.
	
@chrome  
Scenario: Verify the color of Required validation message for Window Plan Dropdown in Create Window screen
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
  And : Click on Qualification Performance Tile of Create window
  And : Click on Create Window Tile
	And : Click on dropdown of Window Type field
	And : Select First record in the dropdown of Window Type field
	And : Click on dropdown of Window Plan
	And : Select first record in the dropdown of Window Plan
	And : Select "--Please Select--" in the dropdown of Window Plan
	Then : Verify that color of Required validation message for Window Plan should be Red
	
@chrome 
Scenario: Verify all types of Window Plan dropdown 
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
  And : Click on Qualification Performance Tile of Create window
  And : Click on Create Window Tile
	And : Click on dropdown of Window Type field
	And : Select First record in the dropdown of Window Type field
	And : Click on dropdown of Window Plan
	Then : Verify that All types of Window Plan dropdown should be displayed as "-- Please Select --\n" + "95% / 95%\n" + "95% / 75%\n" + "95% / 90%"
	
@chrome 
Scenario: Verify functionality when selecting Binomial Distribution window type in Create Window screen
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
  And : Click on Qualification Performance Tile of Create window
  And : Click on Create Window Tile
	And : Click on dropdown of Window Type field
	And : Select Binomial Distribution type in the dropdown of Window Type field
	Then : Verify that system should displayed all related fields when user select Binomial Distribution window type
	
@chrome 
Scenario: Verify functionality when selecting Hypergeometric Distribution window type in Create Window screen
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
  And : Click on Qualification Performance Tile of Create window
  And : Click on Create Window Tile
	And : Click on dropdown of Window Type field
	And : Select Hypergeometric Distribution type in the dropdown of Window Type field
	Then : Verify that system should displayed all related fields when user select Hypergeometric Distribution window type
	
@chrome 
Scenario: Verify functionality when selecting Customized Windows window type in Create Window screen
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
  And : Click on Qualification Performance Tile of Create window
  And : Click on Create Window Tile
	And : Click on dropdown of Window Type field
	And : Select Customized Windows type in the dropdown of Window Type field
	Then : Verify that system should displayed all related fields when user select Customized Windows window type 
	
@chrome @ignore
Scenario: Verify functionality when selecting Scan Statistics window type in Create Window screen
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
  And : Click on Qualification Performance Tile of Create window
  And : Click on Create Window Tile
	And : Click on dropdown of Window Type field
	And : Select Scan Statistics type in the dropdown of Window Type field
	Then : Verify that system should displayed all related fields when user select Scan Statistics window type
	
@chrome 
Scenario: Verify Sample Size dropdown is disable by default in Create Window screen
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
  And : Click on Qualification Performance Tile of Create window
  And : Click on Create Window Tile
	And : Click on dropdown of Window Type field
	And : Select Binomial Distribution type in the dropdown of Window Type field
	Then : Verify that Sample Size dropdown should be disable by default in Create Window screen
	
@chrome 
Scenario: Verify Required validation message for Sample Size dropdown in Create Window screen
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
  And : Click on Qualification Performance Tile of Create window
  And : Click on Create Window Tile
	And : Click on dropdown of Window Type field
	And : Select Binomial Distribution type in the dropdown of Window Type field
	And : Click on dropdown of Window Plan
	And : Select first record in the dropdown of Window Plan
	And : Click on dropdown of Sample Size
	Then : Verify Required validation message for Sample Size dropdown should be displayed as Sample Size is required.
	
@chrome 
Scenario: Verify the color of Required validation message for Sample Size dropdown in Create Window screen
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
  And : Click on Qualification Performance Tile of Create window
  And : Click on Create Window Tile
	And : Click on dropdown of Window Type field
	And : Select Binomial Distribution type in the dropdown of Window Type field
	And : Click on dropdown of Window Plan
	And : Select first record in the dropdown of Window Plan
	And : Click on dropdown of Sample Size
	Then : Verify that color of Required validation message for Sample Size should be Red 

@chrome 
Scenario: Verify Maximum Allowed Process Failure in Period dropdown is disable by default in Create Window screen
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
  And : Click on Qualification Performance Tile of Create window
  And : Click on Create Window Tile
	And : Click on dropdown of Window Type field
	And : Select Binomial Distribution type in the dropdown of Window Type field
	Then : Verify that Maximum Allowed Process Failure in Period dropdown is disable by default in Create Window screen

@chrome 
Scenario: Verify functionality of Allow Second Stage checkbox  field in Create Window screen 
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
  And : Click on Qualification Performance Tile of Create window
  And : Click on Create Window Tile
	And : Click on dropdown of Window Type field
	And : Select Binomial Distribution type in the dropdown of Window Type field
	And : Click on dropdown of Window Plan
	And : Select record in the dropdown of Window Plan
	And : Click on dropdown of Sample Size
	And : Select record in the dropdown of Sample Size field
	And : Click on checkbox of  Allow Second Stage field 
	Then : Verify that checkbox of Allow Second Stage field should be clickable when user checked or unchecked the checkbox

@chrome 
Scenario: Verify Required validation message for Attribute dropdown in Create Window screen
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
  And : Click on Qualification Performance Tile of Create window
  And : Click on Create Window Tile
	And : Click on dropdown of Module
	And : Select value into the dropdown of Module
	And : Click on dropdown of App
	And : Select value into the dropdown of App	
	And : Click on Attributes/Entity key Attributes dropdown
	And : select Collection Date into the dropdown
	Then : Verify that Required validation message for Attribute dropdown should be displayed as Collection Date is required.

@chrome 
Scenario: Verify the color of Required validation message for Attribute dropdown in Create Window screen
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
  And : Click on Qualification Performance Tile of Create window
  And : Click on Create Window Tile
	And : Click on dropdown of Module
	And : Select value into the dropdown of Module
	And : Click on dropdown of App
	And : Select value into the dropdown of App	
	And : Click on Attributes/Entity key Attributes dropdown
	And : select Collection Date into the dropdown
	Then : Verify that the color of Required validation message for Attribute dropdown should be Red	
	
@chrome 
Scenario: Verify values of dropdown of Month field in Create Window screen
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
  And : Click on Qualification Performance Tile of Create window
  And : Click on Create Window Tile
	And : Click on dropdown of Month field for checking values
	Then : Verify that values of dropdown of Month field should be displayed as "-- Please Select --\n" + "January\n" + "February\n" + "March\n" + "April\n" + "May\n" + "June\n" + "July\n" + "August\n" + "September\n" + "October\n" + "November\n" + "December"

@chrome 
Scenario: Verify Required validation message for Month dropdown in Create Window screen
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
  And : Click on Qualification Performance Tile of Create window
  And : Click on Create Window Tile
	And : Click on dropdown of Month field 
	Then : Verify Required validation message for Month dropdown should be displayed as Month is required.
	
@chrome 
Scenario: Verify the color of Required validation message for Month dropdown in Create Window screen
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
  And : Click on Qualification Performance Tile of Create window
  And : Click on Create Window Tile
	And : Click on dropdown of Month field
	Then : Verify that color of Required validation message for Month should be Red 

@chrome 
Scenario: Verify values of dropdown of Year field in Create Window screen
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
  And : Click on Qualification Performance Tile of Create window
  And : Click on Create Window Tile
	And : Click on dropdown of Year field 
	Then : Verify that values of dropdown of Year field should be displayed as "-- Please Select --\n"
				
@chrome @production
Scenario: Verify Required validation message for Year dropdown in Create Window screen
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
  And : Click on Qualification Performance Tile of Create window
  And : Click on Create Window Tile
	And : Click on dropdown of Year field
	Then : Verify Required validation message for Year dropdown should be displayed as Year is required.
	
@chrome 
Scenario: Verify the color of Required validation message for Year dropdown in Create Window screen
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
  And : Click on Qualification Performance Tile of Create window
  And : Click on Create Window Tile
	And : Click on dropdown of Year field
	Then : Verify that color of Required validation message for Year should be Red 
	
@chrome 
Scenario: Verify Month-Year information message of �start from today� in Create Window
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
  And : Click on Qualification Performance Tile of Create window
  And : Click on Create Window Tile
	And : Click on dropdown of Month field in Month-Year Section
	And : Select Current Month
	And : Click on dropdown of Year field in Month-Year Section
	And : Select Current Year
	Then : Verify that system should displayed information message as Start date will be Today for the current window.	
	
@chrome @ignore
Scenario: Verify the color of Month-Year information message of �start from today� in Create Window
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
  And : Click on Qualification Performance Tile of Create window
  And : Click on Create Window Tile
	And : Click on dropdown of Month field in Month-Year Section
	And : Select Current Month
	And : Click on dropdown of Year field in Month-Year Section
	And : Select Current Year
	Then : Verify that color of Month-Year information message of start from today should be Red 
	
@chrome 
Scenario: Verify Month-Year information message of  �start from 1st day� in Create Window
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
  And : Click on Qualification Performance Tile of Create window
  And : Click on Create Window Tile
	And : Click on dropdown of Month field in Month-Year Section
	And : Select Month
	And : Click on dropdown of Year field in Month-Year Section
	And : Select Current Year
	Then : Verify that system should displayed information message as Start date will be 1st Day of the month for the current window.
	
@chrome 
Scenario: Verify the color of Month-Year information message of Start from 1st day in Create Window
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
  And : Click on Qualification Performance Tile of Create window
  And : Click on Create Window Tile
	And : Click on dropdown of Month field in Month-Year Section
	And : Select Month
	And : Click on dropdown of Year field in Month-Year Section
	And : Select Current Year
	Then : Verify that color of Month-Year information message of start from 1st day  should be blue 

@chrome 
Scenario: Verify functionality of Repeat Window checkbox field in Create Window screen 
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
  And : Click on Qualification Performance Tile of Create window
  And : Click on Create Window Tile
	And : Click on checkbox of  Repeat Window field 
	Then : Verify that Repeat Window checkbox field should be clickable when user checked or unchecked the checkbox
	
@chrome 
Scenario: Verify Tooltip message of Repeat Window checkbox in Create Window screen
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
  And : Click on Qualification Performance Tile of Create window
  And : Click on Create Window Tile
	And : Move the mouse over the information icon
	Then : Verify that Tooltip message of Repeat Window checkbox should be displayed as Select this check box to automatically create the window for the next month at the end of the current month.
	
@chrome 
Scenario: Verify the default selection of radio buttons for Site selection in Create Window screen
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
  And : Click on Qualification Performance Tile of Create window
  And : Click on Create Window Tile
	Then : Verify that radio button of All option should be selected by default

@chrome 
Scenario: Verify the functionality of Radio buttons of Site selection in Create Window screen
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
  And : Click on Qualification Performance Tile of Create window
  And : Click on Create Window Tile
	And : Click on radio button of All option in Site selection
	And : Click on radio button of Selection option in Site selection
	Then : Verify that at time only one radio button should be selectable when user select radio button
	
@chrome 
Scenario: Verify Required validation message when user select radio button of selection option
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
  And : Click on Qualification Performance Tile of Create window
  And : Click on Create Window Tile
	And : Click on radio button of Selection option in Site selection
	Then : Verify that Required validation message should be desplayed as At least one site is required from each level.
	
@chrome 
Scenario: Verify the color of Required validation message when user select radio button of selection option
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
  And : Click on Qualification Performance Tile of Create window
  And : Click on Create Window Tile
	And : Click on radio button of Selection option in Site selection
	Then : Verify that color of Required validation message for Site Radio should be Red 	
	
@chrome 
Scenario: Verify Site required validation message from each Department for Region 
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
  And : Click on Qualification Performance Tile of Create window
  And : Click on Create Window Tile
	And : Click on Module dropdown of Create Window
	And : Click on App dropdown of Create Window
	And : Click on radio button of Selection option in Site selection
	And : Select check box of All option of Region section
	Then : Verify that Site required validation message should be desplayed as At least one site from Department is required for Regin MD Anderson Cancer Center - Updated.
	
@chrome 
Scenario: Verify the color of Site required validation message from each Department
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
  And : Click on Qualification Performance Tile of Create window
  And : Click on Create Window Tile
	And : Click on Module dropdown of Create Window
	And : Click on App dropdown of Create Window
	And : Click on radio button of Selection option in Site selection
	And : Select check box of All option of Region section
	Then : Verify that color of Site required message should be Red
	
@chrome 
Scenario: Verify Required validation message from  Site for Department
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
  And : Click on Qualification Performance Tile of Create window
  And : Click on Create Window Tile
	And : Click on Module dropdown of Create Window
	And : Click on App dropdown of Create Window
	And : Click on radio button of Selection option in Site selection
	And : Select check box of All option of Region section
	And : Select First checkbox of Department section
	Then : Verify that Required validation message should be desplayed as At least one site from Site is required for Department Donor Operations.

@chrome 
Scenario: Verify No validation for site selection when selecting checkbox of all sections
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
  And : Click on Qualification Performance Tile of Create window
  And : Click on Create Window Tile
	And : Click on Module dropdown of Create Window
	And : Click on App dropdown of Create Window
	And : Click on radio button of Selection option in Site selection
	And : Select check box of All option of Region section
	And : Select First checkbox of Department section
	And : Select First checkbox of Site section
	Then : Verify that system should not displayed any validation message when user select checkbox of all the sections

@chrome 
Scenario: Verify Required validation message for Population Size textbox in Create Window screen
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
  And : Click on Qualification Performance Tile of Create window
  And : Click on Create Window Tile
	And : Select Hypergeometric Distribution option
	And : Click on Population Size textbox
	Then : Verify that Required validation message for Population Size textbox should be displayed as Population Size is required.
	
@chrome 
Scenario: Verify the color of Required validation message for Population Size textbox in Create Window screen
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
  And : Click on Qualification Performance Tile of Create window
  And : Click on Create Window Tile
	And : Select Hypergeometric Distribution option
	And : Click on Population Size textbox
	Then : Verify that color of Required validation message for Population Size textbox should be Red
	
@chrome 
Scenario: Verify Minimum validation message for Population Size textbox in Create Window screen
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
  And : Click on Qualification Performance Tile of Create window
  And : Click on Create Window Tile
	And : Select Hypergeometric Distribution option
	And : Click on Population Size textbox
	And : Enter numeric value 0 in the textbox of Population Size
	Then : Verify that Minimum validation message for Population Size textbox should be displayed as The Population Size should be greater than 0.
	
@chrome 
Scenario: Verify the color of Minimum validation message for Population Size textbox in Create Window screen
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
  And : Click on Qualification Performance Tile of Create window
  And : Click on Create Window Tile
	And : Select Hypergeometric Distribution option
	And : Click on Population Size textbox
	And : Enter numeric value 0 in the textbox of Population Size
	Then : Verify that color of Minimum validation message for Population Size textbox should be Red
	
@chrome 
Scenario: Verify Maximum validation message for Population Size textbox in Create Window screen
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
  And : Click on Qualification Performance Tile of Create window
  And : Click on Create Window Tile
	And : Select Hypergeometric Distribution option
	And : Click on Population Size textbox
	And : Enter numeric value which is greater than 10000
	Then : Verify that Maximum validation message for Population Size textbox should be displayed as Population Size must not be more than 10000.
	
@chrome 
Scenario: Verify the color of Maximum validation message for Population Size textbox in Create Window screen
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
  And : Click on Qualification Performance Tile of Create window
  And : Click on Create Window Tile
	And : Select Hypergeometric Distribution option
	And : Click on Population Size textbox
	And : Enter numeric value which is greater than 10000
	Then : Verify that color of Maximum validation message for Population Size textbox should be Red
	
@chrome 
Scenario: Verify No validation message for Population Size textbox in Create Window screen
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
  And : Click on Qualification Performance Tile of Create window
  And : Click on Create Window Tile
	And : Select Hypergeometric Distribution option
	And : Click on Population Size textbox
	And : Enter valid data in textbox of Population Size
	Then : Verify that system should not displayed any validation message when user enter valid data in textbox of Population Size

@chrome 
Scenario: Verify Minimum validation message for Sample Size textbox in Create Window screen
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
  And : Click on Qualification Performance Tile of Create window
  And : Click on Create Window Tile
	And : Click on dropdown of Window Type field
	And : Select Customized Windows in the dropdown of Window Type field
	And : Click on textbox of Sample Size field
	And : Enter numeric value 0 in the textbox of Sample Size
	Then : Verify that Minimum validation message for Sample Size textbox should be displayed as Sample Size should be at least 1.
	
@chrome 
Scenario: Verify the color of Minimum validation message for Sample Size textbox in Create Window screen
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
  And : Click on Qualification Performance Tile of Create window
  And : Click on Create Window Tile
	And : Click on dropdown of Window Type field
	And : Select Customized Windows in the dropdown of Window Type field
	And : Click on textbox of Sample Size field
	And : Enter numeric value 0 in the textbox of Sample Size
	Then : Verify that color of Minimum validation message for Sample Size textbox should be Red
	
@chrome 
Scenario: Verify Maximum validation message for Sample Size textbox in Create Window screen
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
  And : Click on Qualification Performance Tile of Create window
  And : Click on Create Window Tile
	And : Click on dropdown of Window Type field
	And : Select Customized Windows in the dropdown of Window Type field
	And : Click on textbox of Sample Size field
	And : Enter numeric value in Sample Size which is greater than 1000
	Then : Verify that Maximum validation message for Sample Size textbox should be displayed as Sample Size must not be more than 999.
	
@chrome 
Scenario: Verify the color of Minimum validation message for Sample Size textbox in Create Window screen
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
  And : Click on Qualification Performance Tile of Create window
  And : Click on Create Window Tile
	And : Click on dropdown of Window Type field
	And : Select Customized Windows in the dropdown of Window Type field
	And : Click on textbox of Sample Size field
	And : Enter numeric value in Sample Size which is greater than 1000
	Then : Verify that color of Maximum validation message for Sample Size textbox should be Red

@chrome 
Scenario: Verify No validation message for Sample Size textbox in Create Window screen
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
  And : Click on Qualification Performance Tile of Create window
  And : Click on Create Window Tile
	And : Click on dropdown of Window Type field
	And : Select Customized Windows in the dropdown of Window Type field
	And : Click on textbox of Sample Size field
	And : Enter valid data in textbox of Sample Size
	Then : Verify that system should not displayed any validation message when user enter valid data in textbox of Sample Size	

@chrome 
Scenario: Verify Required validation message for Maximum Allowed Process Failure in Period textbox in Create Window screen
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
  And : Click on Qualification Performance Tile of Create window
  And : Click on Create Window Tile
	And : Click on dropdown of Window Type field
	And : Select Customized Windows in the dropdown of Window Type field
	And : Click on textbox of Maximum Allowed Process Failure in Period
	Then : Verify that Required validation message for Maximum Allowed Process Failure in Period textbox should be displayed as Maximum Allowed Process Failure in Period is required.

@chrome 
Scenario: Verify color of Required validation message for Maximum Allowed Process Failure in Period textbox in Create Window screen
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
  And : Click on Qualification Performance Tile of Create window
  And : Click on Create Window Tile
	And : Click on dropdown of Window Type field
	And : Select Customized Windows in the dropdown of Window Type field
	And : Click on textbox of Maximum Allowed Process Failure in Period
	Then : Verify that the colorof  Required validation message for Maximum Allowed Process Failure in Period textbox should be Red

@chrome 
Scenario: Verify Minimum validation message for Maximum Allowed Process Failure in Period textbox in Create Window screen
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
  And : Click on Qualification Performance Tile of Create window
  And : Click on Create Window Tile
	And : Click on dropdown of Window Type field
	And : Select Customized Windows in the dropdown of Window Type field
	And : Click on textbox of Maximum Allowed Process Failure in Period
	And : Enter numeric value 0 in the textbox of Maximum Allowed Process Failure in Period
	Then : Verify that Minimum validation message for Population Size textbox should be displayed as Maximum Allowed Process Failure in Period should be at least 1.

@chrome 
Scenario: Verify color of  Minimum validation message for Maximum Allowed Process Failure in Period textbox in Create Window screen
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
  And : Click on Qualification Performance Tile of Create window
  And : Click on Create Window Tile
	And : Click on dropdown of Window Type field
	And : Select Customized Windows in the dropdown of Window Type field
	And : Click on textbox of Maximum Allowed Process Failure in Period
	And : Enter numeric value 0 in the textbox of Maximum Allowed Process Failure in Period
	Then : Verify that the color of Minimum validation message for Population Size textbox should be Red
	
@chrome 
Scenario: Verify Maximum validation message for Maximum Allowed Process Failure in Period textbox in Create Window screen
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
  And : Click on Qualification Performance Tile of Create window
  And : Click on Create Window Tile
	And : Click on dropdown of Window Type field
	And : Select Customized Windows in the dropdown of Window Type field
	And : Click on textbox of Maximum Allowed Process Failure in Period
	And : Enter numeric value which is greater than 100 in the textbox of Maximum Allowed Process Failure in Period
	Then : Verify that Maximum validation message for Population Size textbox should be displayed as Maximum Allowed Process Failure in Period must not be more than 99.
	
@chrome 
Scenario: Verify color of  Maximum validation message for Maximum Allowed Process Failure in Period textbox in Create Window screen
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
  And : Click on Qualification Performance Tile of Create window
  And : Click on Create Window Tile
	And : Click on dropdown of Window Type field
	And : Select Customized Windows in the dropdown of Window Type field
	And : Click on textbox of Maximum Allowed Process Failure in Period
	And : Enter numeric value which is greater than 100 in the textbox of Maximum Allowed Process Failure in Period
	Then : Verify that the color of Maximum validation message for Population Size textbox should be Red

@chrome 
Scenario: Verify Validation message for invalid data of Maximum Allowed Process Failure in Period textbox in Create Window screen
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
  And : Click on Qualification Performance Tile of Create window
  And : Click on Create Window Tile
	And : Click on dropdown of Window Type field
	And : Select Customized Windows in the dropdown of Window Type field
	And : Click on textbox of Sample Size field
	And : Enter numeric value in the textbox of Sample Size
	And : Click on textbox of Maximum Allowed Process Failure in Period
	And : Enter numeric value which is greater than value of Sample Size
	Then : Verify that Validation message for invalid data of Maximum Allowed Process Failure in Period textbox should be displayed as Maximum Allowed Process Failure in Period must not be more than Sample Size.
	
@chrome 
Scenario: Verify the color of Validation message for invalid data of Maximum Allowed Process Failure in Period textbox in Create Window screen
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
  And : Click on Qualification Performance Tile of Create window
  And : Click on Create Window Tile
	And : Click on dropdown of Window Type field
	And : Select Customized Windows in the dropdown of Window Type field
	And : Click on textbox of Sample Size field
	And : Enter numeric value in the textbox of Sample Size
	And : Click on textbox of Maximum Allowed Process Failure in Period
	And : Enter numeric value which is greater than value of Sample Size
	Then : Verify that the color of Validation message for invalid data of Maximum Allowed Process Failure in Period textbox should be Red
	
@chrome 
Scenario: Verify No validation message for Maximum Allowed Process Failure in Period textbox in Create Window screen
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
  And : Click on Qualification Performance Tile of Create window
  And : Click on Create Window Tile
	And : Click on dropdown of Window Type field
	And : Select Customized Windows in the dropdown of Window Type field
	And : Click on textbox of Sample Size field
	And : Enter numeric value in the textbox of Sample Size
	And : Click on textbox of Maximum Allowed Process Failure in Period
	And : Enter valid numeric value in the textbox
	Then : Verify that system should not displayed any validation message when user enter valid data in textbox of Maximum Allowed Process Failure in Period
	
@chrome 
Scenario: Verify functionality of Hourly radio button 
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
  And : Click on Qualification Performance Tile of Create window
  And : Click on Create Window Tile
	And : Click on radio button of Hourly option
	Then : Verify that radio button of Hourly option should be clickable when user click on Hourly radio button
	
@chrome 
Scenario: Verify functionality of Weekly radio button 
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
  And : Click on Qualification Performance Tile of Create window
  And : Click on Create Window Tile
	And : Click on radio button of Weekly option
	Then : Verify that radio button of Hourly option should be clickable when user click on Weekly radio button

@chrome 
Scenario: Verify functionality of Monthly radio button 
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
  And : Click on Qualification Performance Tile of Create window
  And : Click on Create Window Tile
	And : Click on radio button of Monthly option
	Then : Verify that radio button of Hourly option should be clickable when user click on Monthly radio button

@chrome @ignore
Scenario: Verify the color of Clear link in Create Window screen
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
  And : Click on Qualification Performance Tile of Create window
  And : Click on Create Window Tile
	Then : Verify that the color of Clear link should be Blue
	
@chrome 
Scenario: Verify functionality of Clear link for Hourly radio button
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
  And : Click on Qualification Performance Tile of Create window
  And : Click on Create Window Tile
	And : Click on radio button of Hourly option
	And : Click on link of Clear in the Create Window screen
	Then : Verify that radio button of Hourly option should be deselected when user click on Clear link
	
@chrome 
Scenario: Verify functionality of Clear link for Weekly radio button
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
  And : Click on Qualification Performance Tile of Create window
  And : Click on Create Window Tile
	And : Click on radio button of Weekly option
	And : Click on link of Clear in the Create Window screen
	Then : Verify that radio button of Weekly option should be deselected when user click on Clear link
	
@chrome 
Scenario: Verify functionality of Clear link for Monthly radio button
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
  And : Click on Qualification Performance Tile of Create window
  And : Click on Create Window Tile
	And : Click on radio button of Monthly option
	And : Click on link of Clear in the Create Window screen
	Then : Verify that radio button of Monthly option should be deselected when user click on Clear link
	
@chrome 
Scenario: Verify label of Hourly textbox in Create Window screen
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
  And : Click on Qualification Performance Tile of Create window
  And : Click on Create Window Tile
	And : Click on radio button of Hourly option
	Then : Verify that label of Hourly textbox should be displayed as "Every\n" + "hour(s)"

@chrome 
Scenario: Verify Required validation message for Hourly textbox in Create Window screen
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
  And : Click on Qualification Performance Tile of Create window
  And : Click on Create Window Tile
	And : Click on radio button of Hourly option
	Then : Verify that Required validation message for Hourly textbox should be displayed as Every Hour(s) is required.
	
@chrome 
Scenario: Verify the color of  Required validation message for Hourly textbox in Create Window screen
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
  And : Click on Qualification Performance Tile of Create window
  And : Click on Create Window Tile
	And : Click on radio button of Hourly option
	Then : Verify that the color of Required validation message for Hourly textbox should be Red
		
@chrome 
Scenario: Verify Minimum validation message for Hourly textbox in Create Window screen
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
  And : Click on Qualification Performance Tile of Create window
  And : Click on Create Window Tile
	And : Click on radio button of Hourly option
	And : Click on textbox of Hourly
	And : Enter numeric value 0 in the textbox of Hourly
	Then : Verify that Minimum validation message for Hourly textbox should be displayed as Invalid Number.It must be at least 1.
	
@chrome 
Scenario: Verify the color of  Minimum validation message for Hourly textbox in Create Window screen
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
  And : Click on Qualification Performance Tile of Create window
  And : Click on Create Window Tile
	And : Click on radio button of Hourly option
	And : Click on textbox of Hourly
	And : Enter numeric value 0 in the textbox of Hourly
	Then : Verify that the color of Minimum validation message for Hourly textbox should be Red
	
@chrome 
Scenario: Verify Maximum validation message for Hourly textbox in Create Window screen
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
  And : Click on Qualification Performance Tile of Create window
  And : Click on Create Window Tile
	And : Click on radio button of Hourly option
	And : Click on textbox of Hourly
	And : Enter numeric value which is greater than 24 hours
	Then : Verify that Maximum validation message for Hourly textbox should be displayed as Invalid Number. You cannot add more than 24 Hours.
	
@chrome 
Scenario: Verify the color of  Maximum validation message for Hourly textbox in Create Window screen
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
  And : Click on Qualification Performance Tile of Create window
  And : Click on Create Window Tile
	And : Click on radio button of Hourly option
	And : Click on textbox of Hourly
	And : Enter numeric value which is greater than 24 hours
	Then : Verify that the color of Maximum validation message for Hourly textbox should be Red
	
@chrome 
Scenario: Verify No validation message for Hourly textbox in Create Window screen
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
  And : Click on Qualification Performance Tile of Create window
  And : Click on Create Window Tile
	And : Click on radio button of Hourly option
	And : Click on textbox of Hourly
	And : Enter valid data in the textbox of Hourly
	Then : Verify that system should not displayed any validation message when user enter valid data in the textbox of Hourly
	
@chrome 
Scenario: Verify default selection of  Weekly checkbox in Create Window screen
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
  And : Click on Qualification Performance Tile of Create window
  And : Click on Create Window Tile
	And : Click on radio button of Weekly option
	Then : Verify that all checkboxs should be selected by default when user click on Weekly radio button

@chrome 
Scenario: Verify functionalityof checkboxs for Weekly radio button in Create Window screen
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
  And : Click on Qualification Performance Tile of Create window
  And : Click on Create Window Tile
	And : Click on radio button of Weekly option
	And : Click on checkboxs of weekly radio button
	Then : Verify that all checkboxs should be clickable when user checked or unchecked the checkboxs

@chrome 
Scenario: Verify Required validation message for Weekly checkbox in Create Window screen
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
  And : Click on Qualification Performance Tile of Create window
  And : Click on Create Window Tile
	And : Click on radio button of Weekly option
	And : Unchecked all the checkbox of Weekly
	Then : Verify that Required validation message for Weekly checkbox should be displayed as Please select at least one day.
	
@chrome 
Scenario: Verify the color of Required validation message for Weekly checkbox in Create Window screen
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
  And : Click on Qualification Performance Tile of Create window
  And : Click on Create Window Tile
	And : Click on radio button of Weekly option
	And : Unchecked all the checkbox of Weekly
	Then : Verify that the color of Required validation message for Weekly checkbox should be Red

@chrome 
Scenario: Verify No validation message for Weekly checkbox in Create Window screen
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
  And : Click on Qualification Performance Tile of Create window
  And : Click on Create Window Tile
	And : Click on radio button of Weekly option
	Then : Verify that system should not displayed any validation message when user check at least one day
	
@chrome 
Scenario: Verify Required validation message for Email Address textbox in Create Window screen
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
  And : Click on Qualification Performance Tile of Create window
  And : Click on Create Window Tile
	And : Click on textbox of Email Address
	Then : Verify that Required validation message for Email Address textbox should be displayed as Email Address is required.
		
@chrome 
Scenario: Verify the color of Required validation message for Email Address textbox in Create Window screen
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
  And : Click on Qualification Performance Tile of Create window
  And : Click on Create Window Tile
	And : Click on textbox of Email Address
	Then : Verify that the color of Required validation message for Email Address textbox should be Red
	
@chrome 
Scenario: Verify validation message for invalid Email Address in Create Window screen
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
  And : Click on Qualification Performance Tile of Create window
  And : Click on Create Window Tile
	And : Click on textbox of Email Address
	And : Enter invalid format of Email Address
	Then : Verify that validation message for invalid Email Address should be displayed as Invalid Email Address.
	
@chrome 
Scenario: Verify the color of validation message for invalid Email Address in Create Window screen
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
  And : Click on Qualification Performance Tile of Create window
  And : Click on Create Window Tile
	And : Click on textbox of Email Address
	And : Enter invalid format of Email Address
	Then : Verify that the color of validation message for invalid Email Address should be Red
	
@chrome 
Scenario: Verify validation message for Duplicate Email Address in Create Window screen
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
  And : Click on Qualification Performance Tile of Create window
  And : Click on Create Window Tile
	And : Click on textbox of Email Address
	And : Enter Email Address which is already added in Create Window
	Then : Verify that validation message for Duplicate Email Address should be displayed as Duplicate Email Address(es) found.

@chrome 
Scenario: Verify the color of validation message for Duplicate Email Address in Create Window screen
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
  And : Click on Qualification Performance Tile of Create window
  And : Click on Create Window Tile
	And : Click on textbox of Email Address
	And : Enter Email Address which is already added in Create Window
	Then : Verify that the color of  validation message for Duplicate Email Address should be Red
	
@chrome 
Scenario: Verify No validation message for Email Address textbox in Create Window screen
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
  And : Click on Qualification Performance Tile of Create window
  And : Click on Create Window Tile
	And : Click on textbox of Email Address
	And : Enter valid Emai Address in the textbox
	Then : Verify that system should not displayed any validation message when user enter valid Email Address in the textbox of Email Address
	
@chrome 
Scenario: Verify Information message for Email Address in Create Window
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
  And : Click on Qualification Performance Tile of Create window
  And : Click on Create Window Tile
	And : Verify that Information message for Email Address should be displayed as Separate multiple Email Addresses with a comma(,).
	
@chrome 
Scenario: Verify the color of Information message for Email Address in Create Window
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
  And : Click on Qualification Performance Tile of Create window
  And : Click on Create Window Tile
	And : Verify that the color of Information message for Email Address should be Blue
	
@chrome 
Scenario: Verify text of save button in Create Window
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
  And : Click on Qualification Performance Tile of Create window
  And : Click on Create Window Tile
	And : Verify that text of save button should be displayed as Save
	
@chrome 
Scenario: Verify the color of save button in Create Window
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
  And : Click on Qualification Performance Tile of Create window
  And : Click on Create Window Tile
	And : Verify that the color of save button should be Green

@chrome @production
Scenario: Verify text of cancel button in Create Window
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
  And : Click on Qualification Performance Tile of Create window
  And : Click on Create Window Tile
	And : Verify that text of cancel button should be displayed as Cancel
	
@chrome 
Scenario: Verify the color of cancel button in Create Window
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
  And : Click on Qualification Performance Tile of Create window
  And : Click on Create Window Tile
	And : Verify that the color of cancel button should be Black
	

	
	
	
	
	
	
	
	
	
	
	
	
	