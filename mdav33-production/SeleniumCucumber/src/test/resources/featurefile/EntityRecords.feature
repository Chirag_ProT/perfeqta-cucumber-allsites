Feature: Module Name: Entity Records
Executed script on "https://test1.beperfeqta.com/mdav33"

  @chrome @production
  Scenario: Verify the module title when user click on "Entity Records" screen
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Entity Records Tile
    Then : Verify the module name as Entity Records

  @chrome 
  Scenario: Verify breadcrumb functionality of Entity Records screen
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Entity Records Tile	
    And : Click on bread crumb of Entity Records screen
    Then : Verify that the page should be redirected to previous page when click on bread crumb

  @chrome 
  Scenario: Verify Record Search type label of Entity Records screen
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Entity Records Tile
    Then : Verify that Record Search type label of Entity Record module should be displayed as "Entity Record Search"

  @chrome 
  Scenario: Verify the color of label for Entity Records screen
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Entity Records Tile
    Then : Verify that color of label Entity Record Search for Entity records module should be "Blue"

  @chrome 
  Scenario: Verify the all module name for Entity Records screen
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Entity Records Tile
    Then : Verify that All Module Name for Entity records module should be displayed as "Blood\n" + "Equipment\n" + "Reagent QC" 

  @chrome 
  Scenario: Verify all modules are selected by default in Entity Records screen
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Entity Records Tile
    Then : Verify that all module in Entity Records sould be selected bydefault

@chrome 
  Scenario: Verify functionality of Uncheck the Check box of module in Entity Records screen
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Entity Records Tile
    And : Uncheck the check box of modules
    Then : Verify that system should allow to Uncheck all modules in Entity Records screen
    
  @chrome @production
  Scenario: Verify functionality of Check the Check box of module in Entity Records screen
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Entity Records Tile
    And : Uncheck the check box of modules
    And : Check the check box of modules
    Then : Verify that system should allow to Check all modules in Entity Records screen

  @chrome 
  Scenario: Verify no entity found message for Entity Records screen
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Entity Records Tile
    And : Click on serchbox of Enity
    And : Enter Enity name which is not added
    Then : Verify that "  No entity found." message should be displayed when user enter entity name which is not added

  @chrome
  Scenario: Verify No Record Found message when add Entity which does not have any record 
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Entity Records Tile
    And : Click on serchbox of Enity
    And : Enter Entity name which does not have any Record
    And : Click on go button
    Then : Verify that system should displayed message as "No Records Found."
    
  @chrome 
  Scenario: Verify No Record Found message when add Entity which does not have any record in My Favorites
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Entity Records Tile
    Then : Verify that system should displayed message in my favorite as "No Records Found."  


# in this scenario @After method will throw exception which will be not an issue.  

  @chrome 
  Scenario: Verify Required validation message of Enter favorite name textbox for Add to favorite popup
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Entity Records Tile
    And : Click on serchbox of Enity
    And : Enter Entity name
        And : Click on Add To Favorite button of Entity Records
    And : Click on Save Button
    Then : Verify that Required validation message of Enter favorite name textbox for Add to favorite popup should be displayed as "Favorite Name is required."

  # in this scenario @After method will throw exception which will be not an issue.
  
  @chrome 
  Scenario: Verify Minimum Validation message of Enter favorite name textbox for Add to favorite popup
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Entity Records Tile
    And : Click on serchbox of Enity
    And : Enter Entity name
    And : Click on Add To Favorite button of Entity Records
    And : Enter less than 2 characters in Enter favorite name textbox
    Then : Verify that Minimum validation message of Enter favorite name textbox for Add to favorite popup should be displayed as "Favorite Name must be at least 2 characters."

	# in this scenario @After method will throw exception which will be not an issue.
	
  @chrome 
  Scenario: Verify the color of Minimum Validation message for Enter favorite name textbox of Add to favorite popup
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Entity Records Tile
    And : Click on serchbox of Enity
    And : Enter Entity name
    And : Click on Add To Favorite button of Entity Records
    And : Enter less than 2 characters in Enter favorite name textbox
    And : Click on Save Button
    Then : Verify that color of Minimum validation message of Enter favorite name textbox should be "Red"

  # in this scenario @After method will throw exception which will be not an issue.
  
  @chrome 
  Scenario: Verify Maximum Validation message of Enter favorite name textbox for Add to favorite popup
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Entity Records Tile
    And : Click on serchbox of Enity
    And : Enter Entity name
    And : Click on Add To Favorite button of Entity Records
    And : Enter more than 50 characters in Enter favorite name textbox
    Then : Verify that Maximum validation message of Enter favorite name textbox for Add to favorite popup should be displayed as "Favorite Name cannot be more than 50 characters."

	# in this scenario @After method will throw exception which will be not an issue.
	
  @chrome 
  Scenario: Verify the color of Maximum Validation message for Enter favorite name textbox of Add to favorite popup
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Entity Records Tile
    And : Click on serchbox of Enity
    And : Enter Entity name
    And : Click on Add To Favorite button of Entity Records
    And : Enter more than 50 characters in Enter favorite name textbox
    Then : Verify that color of Maximum validation message of Enter favorite name textbox should be "Red"
	
  @chrome 
  Scenario: Verify No validation of Enter favorite name textbox when enter valid data for Add to favorite popup
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Entity Records Tile
    And : Click on serchbox of Enity
    And : Enter Entity name
    And : Click on Add To Favorite button of Entity Records
    And : Enter name in textbox of Enter Favorite Name
    Then : Verify that system should allow valid data and no validation should be displayed for Enter favorite name textbox

	# in this scenario @After method will throw exception which will be not an issue.
	
  @chrome @production
  Scenario: Verify label of Add to favorite in Entity Records screen pop up
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Entity Records Tile
    And : Click on serchbox of Enity
    And : Enter Entity name
    And : Click on Add To Favorite button of Entity Records
    Then : Verify that the label of Add to favorite  in Entity Records screen should be displayed as "Add To Favorite"

  @chrome 
  Scenario: Verify the functionality of Cross Button of add to favorite popup
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Entity Records Tile
    And : Click on serchbox of Enity
    And : Enter Entity name
        And : Click on Add To Favorite button of Entity Records
    And : Click on Cross button
    Then : Verify that System should close the popup of Add to Favorite and redirected to the Entity screen when  click on Cross Button

		# in this scenario @After method will throw exception which will be not an issue.
  @chrome 
  Scenario: Verify the functionality of Cancel Button of add to favorite popup
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Entity Records Tile
    And : Click on serchbox of Enity
    And : Enter Entity name
        And : Click on Add To Favorite button of Entity Records
    And : Click on Cancel button of Add to Favorite
    Then : Verify that System should close the popup of Add to Favorite and redirected to the Entity screen when  click on Cancel Button

	# in this scenario @After method will throw exception which will be not an issue.
  @chrome 
  Scenario: Verify My favorite Radio button is selected bydefault in Add to Favorite popup
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Entity Records Tile
    And : Click on serchbox of Enity
    And : Enter Entity name
        And : Click on Add To Favorite button of Entity Records
    Then : Verify that My favorite Radio Button should be selected bydefault in Add to Favorite popup

	# in this scenario @After method will throw exception which will be not an issue.
  @chrome 
  Scenario: Verify color of Save button of add to favorite popup
  Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Entity Records Tile
    And : Click on serchbox of Enity
    And : Enter Entity name
    And : Click on Add To Favorite button of Entity Records
    Then : Verify that color of Save button of add to favoritepop should be "Green"

	# in this scenario @After method will throw exception which will be not an issue.
  @chrome 
  Scenario: Verify color of Cancel button of add to favorite popup
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Entity Records Tile
    And : Click on serchbox of Enity
    And : Enter Entity name
        And : Click on Add To Favorite button of Entity Records
    Then : Verify that color of Cancel button of add favorite popup should be "Black"

  @chrome 
  Scenario: Verify color of Go button of Enity Records screen
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Entity Records Tile
    Then : Verify that color of Go button of Enity Records screen should be "Green"

  @chrome 
  Scenario: Verify functionality of Save Button in Add favorite popup
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Entity Records Tile
    And : Click on serchbox of Enity
    And : Enter Entity name
        And : Click on Add To Favorite button of Entity Records
    And : Enter valid data in Enter favorite name textbox
    And : Click on Save Button
    Then : Verify that system should save the data and redirected to the Enity Records screen when Click on Save Button

	# in this scenario @After method will throw exception which will be not an issue.
  @chrome 
  Scenario: Verify Unique validation message for Enter favorite name textbox of Add to favorite popup
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Entity Records Tile
    And : Click on serchbox of Enity
    And : Enter Entity name
        And : Click on Add To Favorite button of Entity Records
    And : Enter name in Enter favorite name textbox which is already added
    Then : Verify that Unique validation message for  Enter favorite name textbox of Add to favorite popupshouls be displayed as "Favorite Name must be unique."

	# in this scenario @After method will throw exception which will be not an issue.
  @chrome 
  Scenario: Verify label of Remove popup for my favorite
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Entity Records Tile
    And : Click on Remove button of first record under My favorite listing
    Then : Verify that label of Remove popup for my favorite should be displayed as "Are you sure you want to remove " + SavedName + "?"

	# in this scenario @After method will throw exception which will be not an issue.
  @chrome 
  Scenario: Verify color of Yes button under Remove popup
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Entity Records Tile
    And : Click on Remove button of first record under My favorite listing
    Then : Verify that color of Yes button under Remove popup should be "Green"

	# in this scenario @After method will throw exception which will be not an issue.
  @chrome 
  Scenario: Verify color of No button under Remove popup
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Entity Records Tile
    And : Click on Remove button of first record under My favorite listing
    Then : Verify that color of No button under Remove popup should be "Red"

  @chrome 
  Scenario: Verify functionality of No button under Remove popup
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Entity Records Tile
    And : Click on Remove button of first record under My favorite listing
    And : Click on No Button in Remove popup
    Then : Verify that system close the Remove popup and redirected to the Entity Records screen when click on No Button

  @chrome 
  Scenario: Verify functionality of Yes button under Remove popup
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Entity Records Tile
    And : Click on Remove button of first record under My favorite listing
    And : Click on Yes Button in Remove popup
    Then : Verify that system delete selected my favorite record and redirected to the Entity Records screen when click on Yes Button

  @chrome 
  Scenario: Verify the color of Clear All Button in Entity Records screen
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Entity Records Tile
    Then : Verify that color of Clear All Button in Entity Records screen should be "Black"

  @chrome 
  Scenario: Verify the color of Add To Favorite Button in Entity Records screen
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Entity Records Tile
    Then : Verify that color of Add To Favorite Button in Entity Records screen should be "Blue"

  @chrome @production
  Scenario: Verify functionality of Clear All Button in Entity screen
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Entity Records Tile
    And : Click on Clear All Button
    Then : Verify system should clear all the data when click on Clear All button

  @chrome @production
  Scenario: Verify color of grid add Button in Entity screen
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Entity Records Tile
    And : Click on serchbox of Enity
    And : Enter Entity name which have records
    And : Click on go button
    Then : Verify that color of grid add Button should be "Blue"

  @chrome 
  Scenario: Verify color of grid delete selected Button in Entity screen
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Entity Records Tile
    And : Click on serchbox of Enity
    And : Enter Entity name which have records
    And : Click on go button
    Then : Verify that color of grid delete Button should be "Red"

  @chrome
  Scenario: Verify functionality of Entity Attribute Search in Entity Records screen
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Entity Records Tile
    And : Click on serchbox of Enity
    And : Enter Entity name which have text attributes
    And : Click on Filter by Dropdown
    And : Click on Filter by Dropdown
    And : Select Filter By Entity Attribute
    And : Click on textbox which is beside the Dropdown
    And : Enter the value in textbox
    And : Click on go button
    Then : Verify that System should displayed data of Entity according to the Searched attribute Value
