Feature: Module Name: General Settings
Executed script on "https://test1.beperfeqta.com/mdav33"

  @chrome @production
  Scenario: Verify the module title when user click on "General Settings" module
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on General Settings Tile
    Then : Verify the module name as General Settings

  @chrome
  Scenario: Verify breadcrumb functionality of General Settings screen
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on General Settings Tile
    And : Click on breadcrumb of General Settings screen
    Then : Verify that the page should be redirected to previous page

  @chrome
  Scenario: Verify mandatory validation message of Standard Report Title textbox for General Settings screen
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on General Settings Tile
    And : Click on Standard Report Title textbox
    And : Remove Previous data from the textbox
    Then : Verify that mandatory validation message for Report Tile textbox should be displayed as "Standard Report Title is required."

  @chrome
  Scenario: Verify Minimum validation message of Standard Report Title textbox for General Settings screen
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on General Settings Tile
    And : Click on Standard Report Title textbox
    And : Remove Previous data from the textbox
    And : Enter less than 2 characters in Standard Report Title textbox
    Then : Verify that Minimum validation message for Report Tile textbox should be displayed as "Standard Report Title should be at least 2 characters long."

  @chrome
  Scenario: Verify Maximum validation message of Standard Report Title textbox for General Settings screen
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on General Settings Tile
    And : Click on Standard Report Title textbox
    And : Remove Previous data from the textbox
    And : Enter more than 100 characters in Standard Report Title textbox
    Then : Verify that Maximum validation message for Report Tile textbox should be displayed as "Standard Report Title cannot be more than 100 characters."

  @chrome
  Scenario: Enter valid data into the Standard Report Title textbox and verify that system should not display any validation message
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on General Settings Tile
    And : Click on Standard Report Title textbox
    And : Remove Previous data from the textbox
    And : Enter valid data
    Then : Verify that system should allow valid data and no validation should be displayed for Standard Report Tile textbox

  @chrome 
  Scenario: Verify mandatory validation message of Standard Report Sub Title textbox for General Settings screen
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on General Settings Tile
    And : Click on Standard Report Sub Tile textbox
    And : Remove Previous data from the subtitle textbox
    Then : Verify that mandatory validation message for Standard Report Sub Tile textbox should be displayed as "Standard Report Sub Title is required."

  @chrome 
  Scenario: Verify Minimum validation message of Standard Report Sub Title textbox for General Settings screen
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on General Settings Tile
    And : Click on Standard Report Sub Tile textbox
    And : Remove Previous data from the subtitle textbox
    And : Enter less than 2 characters for subtitle textbox
    Then : Verify that Minimum validation message for Standard Report Sub Tile textbox should be displayed as "Standard Report Sub Title should be at least 2 characters long."

  @chrome 
  Scenario: Verify Maximum validation message of Standard Report Sub Title textbox for General Settings screen
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on General Settings Tile
    And : Click on Standard Report Sub Tile textbox
   And : Remove Previous data from the subtitle textbox
    And : Enter more than 100 characters for subtitle textbox
    Then : Verify that Maximum validation message for Standard Report Sub Tile textbox should be displayed as "Standard Report Sub Title must not be more than 100 characters."

  @chrome 
  Scenario: Enter valid data into the Standard Report Sub Title textbox and verify that system should not display any validation message
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on General Settings Tile
    And : Click on Standard Report Sub Tile textbox
    And : Enter valid data in subtitle textbox
    Then : Verify that system should allow valid data and no validation should be displayed for Standard Report Sub Tile textbox

  @chrome
  Scenario: Verify logo is uploaded.
		Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile

  @chrome
  Scenario: Verify Minimum validation message of Support Contact Details textarea for General Settings screen
  Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on General Settings Tile
    And : Click on Support Contact Details textarea
    And : Remove Previous data from the textarea
    And : Enter less than 2 characters for textarea
    Then : Verify that Minimum validation message for Support Contact Details textarea should be displayed as "Support Contact Details should be at least 2 characters long."

  @chrome  
  Scenario: Verify Maximum validation message of Support Contact Details textarea for General Settings screen
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on General Settings Tile
    And : Click on Support Contact Details textarea
    And : Remove Previous data from the textarea
    And : Enter more than 300 characters in textarea
    Then : Verify that Maximum validation message for Support Contact Details textarea should be displayed as "Support Contact Details must not be more than 300 characters."

  @chrome 
  Scenario: Enter valid data into the Support Contact Details textarea and verify that system should not display any validation message
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on General Settings Tile
    And : Click on Support Contact Details textarea
    And : Remove Previous data from the textarea
    And : Enter valid data for textarea
    Then : Verify that system should allow valid data and no validation should be displayed for Support Contact Details textarea

  @chrome 
  Scenario: Verify Minimum validation message of Password Aging Limit textbox for General Settings
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on General Settings Tile
    And : Click on Password Aging Limit textbox
    And : Remove Previous data from password Aging
    And : Enter value as 0 in textbox
    Then : Verify that Minimum validation message for Password Aging Limit textbox should be displayed as "Password Aging Limit must be at least 1."

  @chrome   
  Scenario: Verify Maximum validation message of Password Aging Limit textbox for General Settings
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on General Settings Tile
    And : Click on Password Aging Limit textbox
    And : Remove Previous data from password Aging
    And : Enter more than 999 numeric value
    Then : Verify that Maximum validation message for Password Aging Limit textbox should be displayed as "Password Aging Limit should not be more than 999."

  @chrome 
  Scenario: Enter valid data into the Password Aging Limit textbox and verify that system should not display any validation message
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on General Settings Tile
    And : Click on Password Aging Limit textbox
    And : Remove Previous data from password Aging
    And : Enter valid data in password Aging
    Then : Verify that system should allow valid data and no validation should be displayed for Password Aging Limit textbox

  @chrome 
  Scenario: Verify Minimum validation messaeg of Password Aging Limit Alert Message textbox for General Settings
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on General Settings Tile
    And : Click on Password Aging Limit Alert Message textbox
     And : Remove Previous data from password Aging msg
    And : Enter less than 2 characters password Aging msg
    Then : Verify that Minimum validation message for Password Aging Limit Alert Message textbox should be displayed  as "Alert Message must be at least 2 characters."

  @chrome 
  Scenario: Verify Maximum validation messaeg of Password Aging Limit Alert Message textbox for General Settings
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on General Settings Tile
    And : Click on Password Aging Limit Alert Message textbox
    And : Remove Previous data from password Aging msg
    And : Enter more than 200 characters password Aging msg
    Then : Verify that Maximum validation message for Password Aging Limit Alert Message textbox should be displayed  as  "Alert Message cannot be more than 200 characters."

  @chrome 
  Scenario: Enter valid data into the Password Aging Limit Alert Message textbox and verify that system should not display any validation message
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on General Settings Tile
    And : Click on Password Aging Limit Alert Message textbox
    And : Remove Previous data from password Aging msg
    And : Enter valid data in password Aging msg box
    Then : Verify that system should allow valid data and no validation should be displayed for Password Aging Limit Alert Message textbox
    
  @chrome @production
  Scenario: Verify Minimum validation message of Number of Previous Used Passwords to Disallow textbox
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on General Settings Tile
    And : Click on Number of Previous Used Passwords to Disallow textbox
    And : Remove Number of Previous Used Passwords to Disallow textbox
    And : Enter 0 in textbox in Previous Used Passwords field
    Then : Verify Minimum validation message for Number of Previous Used Passwords to Disallow textbox should be displayed as "Prevent Number of Last reused Passwords should be at least 1."

  @chrome 
  Scenario: Verify Minimum Regulation validation message of Number of Previous Used Passwords to Disallow textbox
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on General Settings Tile
    And : Click on Number of Previous Used Passwords to Disallow textbox
    And : Remove Number of Previous Used Passwords to Disallow textbox
    And : Enter less than 4 number in Number of Previous Used Passwords to Disallow textbox
    Then : Verify Minimum Regulation validation message for Number of Previous Used Passwords to Disallow should be displayed as "Regulation CFR 21 Part 11 requires that this number be set to a minimum of 4. Are you sure you want to proceed with a value less than 4?"

  @chrome 
  Scenario: Verify Maximum validation message of Number of Previous Used Passwords to Disallow textbox
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on General Settings Tile
    And : Click on Number of Previous Used Passwords to Disallow textbox
    And : Remove Number of Previous Used Passwords to Disallow textbox
    And : Enter more than 99 in Number of Previous Used Passwords to Disallow textbox
    Then : Verify Maximum validation message for Number of Previous Used Passwords to Disallow should be displayed as "Prevent Number of Last reused Passwords cannot be more than 99."

  @chrome 
  Scenario: Enter valid data into the Number of Previous Used Passwords to Disallow textbox and verify that system should not display any validation message
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on General Settings Tile
    And : Click on Number of Previous Used Passwords to Disallow textbox
    And : Remove Number of Previous Used Passwords to Disallow textbox
    And : Enter valid data Previous Used Passwords to Disallow textbox
    Then : Verify that system should allow valid data and no validation should be displayed for Number of Previous Used Passwords to Disallow textbox

  @chrome 
  Scenario: Verify Minimum validation message of Number of Previous Used Passwords to Disallow Alert Message textbox
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on General Settings Tile
    And : Click on Number of Previous Used Passwords to Disallow Alert Message textbox
    And : Remove previous data of  Number of Previous Used Passwords to Disallow Alert Message textbox
    And : Enter less than 2 characters in Previous Used Passwords to Disallow Alert Message textbox
    Then : Verify Minimum validation message for Number of Previous Used Passwords to Disallow Alert Message should be displayed as "Alert Message must be at least 2 characters."

  @chrome 
  Scenario: Verify Maximum validation message of Number of Previous Used Passwords to Disallow Alert Message textbox
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on General Settings Tile
    And : Click on Number of Previous Used Passwords to Disallow Alert Message textbox
    And : Remove previous data of  Number of Previous Used Passwords to Disallow Alert Message textbox
    And : Enter more than 200 characters in Previous Used Passwords to Disallow Alert Message textbox
    Then : Verify Maximum validation message for Number of Previous Used Passwords to Disallow Alert Message should be displayed as "Alert Message cannot be more than 200 characters."

  @chrome
  Scenario: Enter valid data into the Number of Previous Used Passwords to Disallow Alert Message textbox and verify that system should not display any validation message
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on General Settings Tile
    And : Click on Number of Previous Used Passwords to Disallow Alert Message textbox
    And : Remove previous data of  Number of Previous Used Passwords to Disallow Alert Message textbox
    And : Enter valid data in Previous Used Passwords to Disallow Alert Message textbox
    Then : Verify that system should allow valid data and no validation mssage should be displayed for Previous Used Passwords to Disallow Alert Message textbox

  @chrome 
  Scenario: Verify Minimum validation message of Maximum Number of Failed Login Attempts textbox
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on General Settings Tile
    And : Click on Maximum Number of Failed Login Attempts textbox
    And : Remove previous data Maximum Number of Failed Login Attempts textbox
    And : Enter 0 in textbox Maximum Number of Failed Login Attempts textbox
    Then : Verify Minimum validation message for Maximum Number of Failed Login Attempts textbox should be displayed as "Number of Failed Login Attempts should be at least 1."

  @chrome 
  Scenario: Verify Maximum validation message of Maximum Number of Failed Login Attempts textbox
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on General Settings Tile
    And : Click on Maximum Number of Failed Login Attempts textbox
    And : Remove previous data Maximum Number of Failed Login Attempts textbox
    And : Enter more than 999 numeric value Maximum Number of Failed Login Attempts textbox
    Then : Verify Maximum validation message for Maximum Number of Failed Login Attempts textbox should be displayed as "Number of Failed Login Attempts must not be more than 999."

  @chrome  
  Scenario: Verify No validation of Maximum Number of Failed Login Attempts textbox textbox when enter valid data
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on General Settings Tile
    And : Click on Maximum Number of Failed Login Attempts textbox
    And : Remove previous data Maximum Number of Failed Login Attempts textbox
    And : Enter valid data Maximum Number of Failed Login Attempts textbox
    Then : Verify that system should allow valid data and no validation should be displayed for Maximum Number of Failed Login Attempts textbox
    
  @chrome 
  Scenario: Verify Minimum validation message of Maximum Number of Failed Login Attempts Alert Message textbox
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on General Settings Tile
    And : Click on Maximum Number of Failed Login Attempts Alert Message textbox
    And : Remove data of Failed Login Attempts Alert Message textbox
    And : Enter less than 2 characters in Failed Login Attempts Alert Message textbox
    Then : Verify Minimum validation message for Maximum Number of Failed Login Attempts Alert Message textbox should be displayed as "Alert Message must be at least 2 characters."

  @chrome 
  Scenario: Verify Maximum validation message of Maximum Number of Failed Login Attempts Alert Message textbox
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on General Settings Tile
    And : Click on Maximum Number of Failed Login Attempts Alert Message textbox
    And : Remove data of Failed Login Attempts Alert Message textbox
    And : Enter more than 200 characters in Failed Login Attempts Alert Message textbox
    Then : Verify Maximum validation message for Maximum Number of Failed Login Attempts Alert Message textbox should be displayed as "Alert Message cannot be more than 200 characters."

  @chrome  
  Scenario: Verify No validation of Maximum Number of Failed Login Attempts Alert Message textbox when enter valid data
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on General Settings Tile
    And : Click on Maximum Number of Failed Login Attempts Alert Message textbox
    And : Remove data of Failed Login Attempts Alert Message textbox
    And : Enter valid data in Failed Login Attempts Alert Message textbox
    Then : Verify that system should allow valid data and no validation should be displayed for Maximum Number of Failed Login Attempts Alert Message textbox

  @chrome 
  Scenario: Verify Minimum validation message of Day to auto-close monthly windows
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on General Settings Tile
    And : Click on textbox of Day to auto-close monthly windows
    And : Remove data from textbox of Day to auto-close monthly windows
    And : Enter 0 in textbox auto-close monthly windows
    Then : Verify Minimum validation message for Day to auto-close monthly windows textbox should be displayed as "Day should be at least 1."

  @chrome  
  Scenario: Verify Maximum validation message of Day to auto-close monthly windows
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on General Settings Tile
    And : Click on textbox of Day to auto-close monthly windows
    And : Remove data from textbox of Day to auto-close monthly windows
    And : Enter more than 31 number auto-close monthly windows
    Then : Verify Maximum validation message for Day to auto-close monthly windows textbox should be displayed as "Day cannot be more than 31."

  @chrome 
  Scenario: Verify No validation message of Day to auto-close monthly windows
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on General Settings Tile
    And : Click on textbox of Day to auto-close monthly windows
    And : Remove data from textbox of Day to auto-close monthly windows
    And : Enter valid data auto-close monthly windows
    Then : Verify that system should allow valid data and no validation should be displayed for Day to auto-close monthly windows

  @chrome 
  Scenario: Verify the functionality of checkbox of Show Sequence number of Entity, Procedure and Questions
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on General Settings Tile
    And : Click on checkbox of Show Sequence number of Entity, Procedure and Questions
    Then : Verify that checkbox of Show Sequence number of Entity, Procedure and Questions should be selectable

  @chrome 
  Scenario: Verify the functionality of checkbox of Show Acceptance Criteria Icon in the App
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on General Settings Tile
    And : Click on checkbox of Show Acceptance Criteria Icon in the App
    Then : Verify that checkbox of Show Acceptance Criteria Icon in the App

  @chrome 
  Scenario: Verify the functionality of checkbox of Do not allow to print blank fields
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on General Settings Tile
    And : Click on checkbox of Do not allow to print blank fields
    Then : Verify that checkbox of Do not allow to print blank fields

  @chrome 
  Scenario: Verify the functionality of checkbox of Allow Sites to Print
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on General Settings Tile
    And : Click on checkbox of  Allow Sites to Print
    Then : Verify that checkbox of Allow Sites to Print

  @chrome  
  Scenario: Verify the functionality of Save button
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on General Settings Tile
    And : Click on Save Button of General Setting
    Then : Verify that System should allow to save the data on General Settings screen
