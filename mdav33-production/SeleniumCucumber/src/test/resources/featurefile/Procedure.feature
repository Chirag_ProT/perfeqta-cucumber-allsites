Feature: Module Name: Procedure
Executed script on "https://test1.beperfeqta.com/mdav33"    

@chrome @production
Scenario: Verify breadcrumb functionality of Procedure Screen
    Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Administration Tile
    And : Click on Procedure Tile 
    And : Click on bread crumb of Procedure screen
    Then : Verify that the page should be redirected to the Administration Page

@chrome
Scenario: Verify pagination of Procedure listing screen when user click on "First"
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Procedure Tile
    And : Click on the Last button of Pagination 
	  And : Click on the First button of Pagination 
    Then : Verify that system should display the First page of the Procedure listing screen
    
@chrome 
Scenario: Verify pagination of Procedure listing screen when user click on "Last"
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Procedure Tile
    And : Click on the Last button of Pagination
    Then : Verify that system should display the Last page of the Procedure listing screen

@chrome
Scenario: Verify pagination of Procedure listing screen when user click on "Next"
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Procedure Tile
    And : Click on Next button of Pagination
    Then : Verify that system should display the Next page of the Procedure listing screen    
    

@chrome
Scenario: Verify pagination of Procedure listing screen when user click on "Previous"
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Procedure Tile
    And : Click on Next button of Pagination
    And : Click on the Previous button of Pagination 
    Then : Verify that system should display the Previous page of the Procedure listing screen    

@chrome
Scenario: Verify Edit Procedure Page load 
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Procedure Tile
    And : Click on First record from Procedure listing screen
    Then : Verify that system should be redirected to the Edit Procedure page 
    
@chrome 
Scenario: Verify Breadcrumb functionality of Edit Procedure screen
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Procedure Tile
    And : Click on First record from Procedure listing screen
    Then : Verify that Breadcrumb for Edit Procedure screen should be displayed as Home > Administration > Procedures > Add / Edit Procedure
    

@chrome
Scenario: Verify functionality of Save Button for Edit Procedure screen
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Procedure Tile
    And : Click on First record from Procedure listing screen
    And : Click on Save Button of Edit Procedure screen
    Then : Verify that system should save the record and redirected to the Procedure listing screen when user click on Save Button

@chrome @production
Scenario: Verify functionality of Cancel Button for Edit Procedure screen
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Procedure Tile
    And : Click on First record from Procedure listing screen
    And : Click on Cancel Button Edit Procedure
    Then : Verify that system should be redirected to the Procedure listing screen when user click on Cancel Button
    
@chrome
Scenario: Verify the color of Save button of Edit Procedure screen
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Procedure Tile
    And : Click on First record from Procedure listing screen
    Then : Verify the color of Save Button of Edit Procedure screen should be Green
    
@chrome
Scenario: Verify the color of Cancel button of Edit Procedure screen
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Procedure Tile
    And : Click on First record from Procedure listing screen
    Then : Verify the color of Cancel Button of Edit Procedure screen should be Black
    
@chrome @ignore
Scenario: Verify the Search functionality of Procedure listing screen
    Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Administration Tile
    And : Click on Procedure Tile
    And : Enter the Procedure name into the search box of Procedure listing screen
    Then : Verify the search result of Procedure Listin Screen


@chrome @ignore
Scenario: Verify the Ascending order for the Procedure column under Procedure listing screen
    Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Administration Tile
    And : Click on Procedure Tile
    Then : Verify that all the records of Procedure column display in ascending order
@chrome @ignore
Scenario: Verify the Descending order for the Procedure column under Procedure listing screen
    Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Administration Tile
    And : Click on Procedure Tile
    And : Click on Sorting icon of the Procedures column
    Then : Verify that all the records of Procedure column display in descending order
    
@chrome
Scenario: Verify View Audit Trail Page redirection and Breadcrumbs of Site Level tab
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Procedure Tile
    And : Click on View Audit Trail link of first record in Procedure listing screen
    Then : Verify that system should be redirected to the Audit Trail page of the first record when user click on View Audit Trail link
    
@chrome
Scenario: Verify Breadcrumb functionality of Audit Trail screen 
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Procedure Tile
     And : Click on View Audit Trail link of first record in Procedure listing screen
    Then : Verify that Breadcrumb for Edit Procedure screen should be displayed as Home > Administration > Procedures > Audit Trail

@chrome @ignore
Scenario: Verify Pagination of Audit Trail screen 
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Procedure Tile
    And : Click on View Audit Trail link of first record in Procedure listing screen
    Then : Verify that total number of entries should be matched with pagination of Audit Trail screen
    
@chrome
Scenario: Verify the color of Back Button of Audit Trail screen
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Procedure Tile
    And : Click on View Audit Trail link of first record in Procedure listing screen
    And : Verify that the color of Back Button of Audit Trail screen should be Black

@chrome
Scenario: Verify functionality of Back Button of Audit Trail screen
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Procedure Tile
    And : Click on View Audit Trail link of first record in Procedure listing screen
    And : Click on Back Button of Audit Trail screen
    Then : Verify that system should be redirected to the Procedure listing screen when user click on Back Button
    
@chrome
Scenario: Verify Required validation message for Procedure Name in Add Procedure screen
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Procedure Tile
    And : Click on Add New Button procedure
    And : Press "TAB" key in add procedure name input box
    Then : Verify that Required validation message for Procedure Name should be displayed as Procedure Name is required.
    
@chrome 
Scenario: Verify Minimum validation message for Procedure Name in Add Procedure screen
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Procedure Tile
    And : Click on Add New Button procedure
    And : Enter less than 2 characters in the textbox of Procedure Name
    Then : Verify that Minimum validation message for Procedure Name should be displayed as Procedure name must be at least 2 characters.
    
@chrome
Scenario: Verify Maximum validation message for Procedure Name in Add Procedure screen
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Procedure Tile
    And : Click on Add New Button procedure
    And : Enter more than 200 characters in the textbox of Procedure Name
    Then : Verify that Maximum validation message for Procedure Name should be displayed as Procedure name cannot be more than 200 characters.

    
@chrome
Scenario: Verify No validation message for Procedure Name in Add Procedure screen
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Procedure Tile
    And : Click on Add New Button procedure
    And : Enter valid data in the textbox of Procedure Name
    Then : Verify that system should not display any validation message when user enter valid data in the textbox of Procedure Name
    
@chrome
Scenario: Verify Unique validation message for Procedure Name in Add Procedure screen
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Procedure Tile
    And : Click on Add New Button procedure
    And : Enter Procedure Name of textbox which is already added 
    Then : Verify that Unique validation message for Procedure Name should be displayed as Procedure Name must be unique.
    
 @chrome 
Scenario: Verify functionality of Save Button for Add Procedure screen
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Procedure Tile
   And : Click on Add New Button procedure
   And : Enter valid data in the textbox of Procedure Name
   And : Enter valid data in the textbox of Procedure Tag
    And : Enter valid data of all mandatory field in Add Procedure screen
    And : Click on Save Button in Add Procedure screen
    Then : Verify that New Procedure should be added into the system when user click on Save Button in Add Procedure screen
    
@chrome 
Scenario: Verify functionality of Edit Existing record of Procedure module
    Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Administration Tile
    And : Click on Procedure Tile
    And : Click on the first record of the Procedures column
    And : Click on Procedure Name textbox
    And : Edit the Procedure Name
    And : Click on Save Button procedure record
    Then : Verify that system should save the Editable data into the Existing record of Procedure module
    
    
@chrome
Scenario: Verify text of Label of Copy Procedure pop-up in Procedure Screen    
    Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Administration Tile
    And : Click on Procedure Tile
    And : Click on Copy icon of first record of Action column
    Then : Verify that Label of Copy Procedure pop-up should be displayed as Procedure Name *
    

@chrome
Scenario: Verify default value '- Copy' is displayed with Procedure name in Copy Procedure pop-up 
    Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Administration Tile
    And : Click on Procedure Tile
    And : Click on Copy icon of first record of Action column for checking -copy 
    Then : Verify that system should display '- Copy' value by default with Procedure Name in Copy Procedure pop-up
    
@chrome
Scenario: Verify functionality of Create Copy of Procedure
    Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Administration Tile
    And : Click on Procedure Tile
    And : Click on Copy icon of first record of Action column
    And : Click on Ok button of pop up window 
    Then : Verify that system should Create Copy of Procedure when user click on OK Button
    
@chrome @ignore
Scenario: Verify the Ascending order for the Last Updated column under Procedure listing screen
    Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Administration Tile
    And : Click on Procedure Tile
    And : Click on Sorting icon of the Last Updated column
    Then : Verify that all the records should be displayed in ascending order
@chrome @ignore
Scenario: Verify the Descending order for the Last Updated column under Procedure listing screen
    Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Administration Tile
    And : Click on Procedure Tile
    And : Click on Sorting icon of the Last Updated column
    Then : Verify that all the records  should be displayed in descending order Verify  functionality of Save Button for Add Procedure screen
    
    @chrome
Scenario: Verify Required validation message for Procedure Tag in Add Procedure screen
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Procedure Tile
    And : Click on Add New Button procedure
    And : Press "TAB" key in add Procedure Tag input box
    Then : Verify that Required validation message for Procedure Tag should be displayed as Procedure Name is required.
    
@chrome
Scenario: Verify Minimum validation message for Procedure Tag in Add Procedure screen
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Procedure Tile
    And : Click on Add New Button procedure
    And : Enter less than 2 characters in the textbox of Procedure Tag
    Then : Verify that Minimum validation message for Procedure Tag should be displayed as Procedure name must be at least 2 characters.
    
@chrome
Scenario: Verify Maximum validation message for Procedure Tag in Add Procedure screen
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Procedure Tile
    And : Click on Add New Button procedure
    And : Enter more than 200 characters in the textbox of Procedure Tag
    Then : Verify that Maximum validation message for Procedure Tag should be displayed as Procedure name cannot be more than 200 characters.

    
@chrome 
Scenario: Verify No validation message for Procedure Name in Add Procedure screen
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Procedure Tile
    And : Click on Add New Button procedure
    And : Enter valid data in the textbox of Procedure Tag
    Then : Verify that system should not display any validation message when user enter valid data in the textbox of Procedure Tag
    
    
  @chrome
Scenario: Verify checkbox of Instruction for user in Add Procedure screen
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Procedure Tile
    And : Click on Add New Button procedure
    And : Enter valid data in the textbox of Procedure Name
    And : Click on Instruction for user checkbox
    Then : Verify that system should allow toclick on checkbox  
    
 @chrome
Scenario: Verify Required validation message for Instructions in Add Procedure screen
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Procedure Tile
    And : Click on Add New Button procedure
    And : Enter valid data in the textbox of Procedure Name
    And : Click on Instruction for user checkbox
    And : Click on Instructions input box
    And : Press Tab key Instruction Input box
    Then : Verify that Required validation message for instructions should be displayed as Instructions is required.  
    
    @chrome
Scenario: Verify Minimum validation message for Instructions in Add Procedure screen
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Procedure Tile
    And : Click on Add New Button procedure
    And : Enter valid data in the textbox of Procedure Name
    And : Click on Instruction for user checkbox
    And : Click on Instructions input box
    And : Enter less than 2 characters in the textbox of Instruction Input box
    Then : Verify that Minimum validation message for instructions should be displayed as Instructions for user must be at least 2 characters. 
    
    @chrome
Scenario: Verify Maximum validation message for Instructions in Add Procedure screen
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Procedure Tile
    And : Click on Add New Button procedure
    And : Enter valid data in the textbox of Procedure Name
    And : Click on Instruction for user checkbox
    And : Click on Instructions input box
    And : Enter more than 200 characters in the textbox of Instruction Input box
    Then : Verify that Maximum validation message for instructions should be displayed as Instructions for user must be at least 2 characters. 
    
    
      @chrome
Scenario: Verify valid data for Instructions in Add Procedure screen
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Procedure Tile
    And : Click on Add New Button procedure
    And : Enter valid data in the textbox of Procedure Name
    And : Click on Instruction for user checkbox
    And : Click on Instructions input box
    And : Enter Valid data Instruction Input box
    Then : Verify that no validation message for instructions   
    
    @chrome
Scenario: Verify Url checkbox in Add Procedure screen
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Procedure Tile
    And : Click on Add New Button procedure
    And : Enter valid data in the textbox of Procedure Name
    And : Click on Instruction for user checkbox
    And : Click on Instructions input box
    And : Enter Valid data Instruction Input box
    And : Click on Url checkbox
    Then : Verify Url checkbox Should be clickable. 
    
   @chrome
Scenario: Verify Required Url checkbox in Add Procedure screen
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Procedure Tile
    And : Click on Add New Button procedure
    And : Enter valid data in the textbox of Procedure Name
    And : Click on Instruction for user checkbox
    And : Click on Instructions input box
    And : Enter Valid data Instruction Input box
    And : Click on Url checkbox
    And : press Tab in URl box
    Then : Verify Required msg Should be displayed.   
    
    @chrome
Scenario: Verify Invalid Url checkbox in Add Procedure screen
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Procedure Tile
    And : Click on Add New Button procedure
    And : Enter valid data in the textbox of Procedure Name
    And : Click on Instruction for user checkbox
    And : Click on Instructions input box
    And : Enter Valid data Instruction Input box
    And : Click on Url checkbox
    And : Enter invalid data url
    Then : Verify invalid  msg Should be displayed as Not a valid URL, don't forget to use http:// or https:// 
      
  @chrome
Scenario: Verify default status of Toggle button in Add Procedure screen  
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
     And : Click on Administration Tile
   And : Click on Procedure Tile
    And : Click on Add New Button procedure
   Then : Verify that Toggle button should be Active by default new Procedure screen 
    
 
       