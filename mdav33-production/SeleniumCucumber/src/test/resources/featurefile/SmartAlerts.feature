Feature: Module Name: Smart Alerts 
Executed script on "https://test1.beperfeqta.com/mdav33" 

  @chrome @production
  Scenario: Verify the module title when user click on "Smart Alerts" module
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Smart Alerts Tile
    Then : Verify module name as Smart Alerts

  @chrome @ignore
  Scenario: Verify Pagination for Smart Alerts Screen
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Smart Alerts Tile
   Then : Verify the record count 

  @chrome 
  Scenario: Verify pagination when user click on Last
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Smart Alerts Tile
     And : Click on the Last button of Pagination 
   Then : Verify that the system should display the last page of the listing screen 
   
  @chrome 
  Scenario: Verify pagination when user click on First
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Smart Alerts Tile
    And : Click on the Last button of Pagination
    And : Click on the First button of Pagination
    Then : Verify that the system should display the first page of the listing screen

  @chrome @production
  Scenario: Verify the navigation of Audit trail page of Smart Alert Screen
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Smart Alerts Tile
    And : Click on "View Audit Trail" link of the first record of the Smart Alerts listing screen
    Then : Verify that system should displayed Audit Trail page when user click on "View Audit Trail" link of the Smart Alert listing screen


  @chrome @ignore
  Scenario: Verify the Ascending order functionality for Smart Alert column of Smart Alerts screen
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Smart Alerts Tile
    Then : Verify that all the records of Smart alert column display in ascending order when click on sorting icon

  @chrome @ignore
  Scenario: Verify the Descending order functionality for Smart Alert column of Smart Alerts screen
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Smart Alerts Tile
    And : Click on Sorting icon of the Smart Alert column
    Then : Verify that all the records of Smart Alert column display in descending order when you click on smart alert
    Then : Verify that all the records of Entities column display in ascending order when click on sorting icon


  @chrome  @ignore
  Scenario: Verify Search Functionality for Smart Alerts screen
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Smart Alerts Tile
    And : Enter the data into search box, which user want to search
	 Then : Verify the search results of Smart alert listing page

  @chrome @production
  Scenario: Verify Click of Add New Button of Smart Alerts
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Smart Alerts Tile
    And : Click on Add New button of Smart Alert
    Then : Verify that system should redirect to the Add / Edit Smart Alert Screen when a user click on the Add New button

  @chrome 
  Scenario: Verify Current Version of Smart Alert in Add / Edit Smart Alert Screen
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Smart Alerts Tile
    And : Click on Add New button of Smart Alert
    Then : Verify that a system should display the Current Version of the Smart Alert

  @chrome 
  Scenario: Verify Required Validation message for Smart Alert Name Field
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Smart Alerts Tile
    And : Click on Add New button of Smart Alert
    And : Click on Smart Alert Name textbox and Press "TAB" Key
    Then : Verify that system should display required validation message as Smart Alert Name is required.

  @chrome 
  Scenario: Verify Unique Validation message for Smart Alert Name Field
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Smart Alerts Tile
    And : Click on Add New button of Smart Alert
    And : Enter Existing Smart Alert Name into Smart Alert Name textbox
    Then : Verify that system should display unique validation message as Smart Alert Name must be unique.

  @chrome 
  Scenario: Verify Minimum Validation message for Smart Alert Name Field
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Smart Alerts Tile
    And : Click on Add New button of Smart Alert
    And : Enter less than 2 characters in Smart Alert Name textbox
    Then : Verify that system should display minimum validation message as Smart Alert Name must be at least 2 characters.

  @chrome 
  Scenario: Verify Maximum Validation message for Smart Alert Name Field
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Smart Alerts Tile
    And : Click on Add New button of Smart Alert
    And : Enter more than 50 characters in Smart Alert Name textbox
    Then : Verify that system should display maximum validation message as Smart Alert Name cannot be more than 50 characters long.


  @chrome 
  Scenario: Verify Minimum Validation message for Smart Alert Description Field
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Smart Alerts Tile
    And : Click on Add New button of Smart Alert
    And : Enter less than 2 characters in Smart Alert Description textbox
    Then : Verify that system should display Minimum validation message as Smart Alert Description must be at least 2 characters.

  @chrome 
  Scenario: Verify Maximum Validation message for Smart Alert Description Field
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Smart Alerts Tile
    And : Click on Add New button of Smart Alert
    And : Enter more than 800 characters in Smart Alert Description textbox
    Then : Verify that system should display Maximum validation message as Smart Alert Description cannot be more than 800 characters.

  @chrome 
  Scenario: Verify No Validation message for Smart Alert Description Field
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Smart Alerts Tile
    And : Click on Add New button of Smart Alert
    And : Enter valid data in Smart Alert Description textbox
    Then : Verify that system should not display any validation message when user do not enter value in Smart Alert Description Field

  @chrome 
  Scenario: Verify Required Validation message for "Message and Instruction to send in Email" field
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Smart Alerts Tile
    And : Click on Add New button of Smart Alert
    And : Click on Message and Instruction to send in Email textarea Press "TAB" Key
    Then : Verify that system should display required validation message as Message and Instructions to send in Email is required.

  @chrome 
  Scenario: Verify Minimum Validation message for "Message and Instruction to send in Email" field
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Smart Alerts Tile
    And : Click on Add New button of Smart Alert
    And : Enter less than 2 characters in Message and Instruction to send in Email textarea
    Then : Verify that system should display Minimum validation message as Message and Instruction to send in Email should be at least 2 characters long.

  @chrome 
  Scenario: Verify Maximum Validation message for "Message and Instruction to send in Email" field
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Smart Alerts Tile
    And : Click on Add New button of Smart Alert
    And : Enter more than 800 characters in Message and Instruction to send in Email textareax
    Then : Verify that system should display Maximum validation message as Message and Instructions to send in Email cannot be more than 800 characters.

  @chrome 
  Scenario: Verify No Validation message for "Message and Instruction to send in Email" field
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Smart Alerts Tile
    And : Click on Add New button of Smart Alert
    And : Enter valid data in Message and Instruction to send in Email
    Then : Verify that system should not display any validation message when a user enter valid value

  @chrome 
  Scenario: Verify Required Validation message for "Email Address(es) field"
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Smart Alerts Tile
    And : Click on Add New button of Smart Alert
    And : Click on Email Address(es) textbox Press "TAB" Key
    Then : Verify that system should display required validation message as Please enter at least one Email Address.

  @chrome 
  Scenario: Verify Invalid Validation message for "Email Address(es) field"
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Smart Alerts Tile
    And : Click on Add New button of Smart Alert
    And : Enter less than 2 characters in Email Address(es) textbox
    Then : Verify that system should display validation message as Invalid Email Address.

  @chrome 
  Scenario: Verify Module Selection Functionality
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Smart Alerts Tile
    And : Click on Add New button of Smart Alert
    And : Select Module Name from the dropdown
    Then : Verify that a user is allowed to select any module from Module dropdown

  @chrome 
  Scenario: Verify Required Validation message for Module field
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Smart Alerts Tile
    And : Click on Add New button of Smart Alert
    And : Select Module Name from the dropdown then deselet module
    Then : Verify that system should display required validation message as "Module is required."

  @chrome 
  Scenario: Verify Required Validation message for App field
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Smart Alerts Tile
    And : Click on Add New button of Smart Alert
    And : Select Module Name from the dropdown
    And : Click on App dropdown
    And : Click on Uncheck All option
    Then : Verify that system should display required validation message as App is required.

  @chrome 
  Scenario: Verify Check All option from App dropdown
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Smart Alerts Tile
    And : Click on Add New button of Smart Alert
    And : Select Module Name from the dropdown
    And : Click on App dropdown
    And : Click on Check All option
    Then : Verify that system should select All the listed App from App dropdown

  @chrome 
  Scenario: Verify Uncheck All option from App dropdown
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Smart Alerts Tile
    And : Click on Add New button of Smart Alert
    And : Select Module Name from the dropdown    
    And : Click on App dropdown
    And : Click on Check All option
    And : Click on Uncheck All option
    Then : Verify that system should deselect All the listed App from App dropdown

  @chrome 
  Scenario: Verify Required Validation message for Every Textbox of Daily Frequencey Type
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Smart Alerts Tile
    And : Click on Add New button of Smart Alert
    And : Select Daily radio button of Smart Alert Frequencey Type
    And : Click on Every day textbox and Press "TAB" Key
    Then : Verify that system should display required message as Every Day(s) is required.

  @chrome 
  Scenario: Verify Maximum Validation message for Every Textbox of Daily Frequencey Type
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Smart Alerts Tile
    And : Click on Add New button of Smart Alert
    And : Select Daily radio button of Smart Alert Frequencey Type
    And : Click on Every textbox and Enter Every day textbox Value as 32
    Then : Verify that system should display maximum validation message for Every day textbox as Invalid Number. You cannot add more than 31 days.

  @chrome 
  Scenario: Verify Invalid Validation message for Every Textbox of Daily Frequencey Type
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Smart Alerts Tile
    And : Click on Add New button of Smart Alert
    And : Select Daily radio button of Smart Alert Frequencey Type
    And : Click on Every textbox and Enter Every day textbox Value as e
    Then : Verify that system should display invalid validation message for Every day textbox as Invalid Number.

  @chrome 
  Scenario: Verify No Validation message for Every Textbox of Daily Frequencey Type
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Smart Alerts Tile
    And : Click on Add New button of Smart Alert
    And : Select Daily radio button of Smart Alert Frequencey Type
    And : Click on Every textbox and Enter Valid Value in Every day textbox
    Then : Verify that system should not display any validation message when a user enter Valid value into Every day textbox

  @chrome 
  Scenario: Verify Rneequired Validation message for Every Textbox of Weekly Frequencey Type
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Smart Alerts Tile
    And : Click on Add New button of Smart Alert
    And : Select Weekly radio button of Smart Alert Frequencey Type
    And : Click on Every weekly textbox and Press "TAB" Key
    Then : Verify that system should display required message as Every Week(s) is required.

  @chrome  
  Scenario: Verify Maximum Validation message for Every Textbox of Weekly Frequencey Type
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Smart Alerts Tile
    And : Click on Add New button of Smart Alert
    And : Select Weekly radio button of Smart Alert Frequencey Type
    And : Click on Every textbox and Enter Every weekly textbox Value as 32
    Then : Verify that system should display maximum validation message as Invalid Number. You cannot add more than 10 weeks.

  @chrome  
  Scenario: Verify Invalid Validation message for Every Textbox of Weekly Frequencey Type
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Smart Alerts Tile
    And : Click on Add New button of Smart Alert
    And : Select Weekly radio button of Smart Alert Frequencey Type
    And : Click on Every textbox and Enter Every weekly textbox Value as e
    Then : Verify that system should display invalid validation message of every weekly textbox as Invalid Number.

  @chrome  
  Scenario: Verify No Validation message for Every Textbox of Weekly Frequencey Type
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Smart Alerts Tile
    And : Click on Add New button of Smart Alert
    And : Select Weekly radio button of Smart Alert Frequencey Type
    And : Click on Every textbox and Enter Valid Value in Every weekly textbox
    Then : Verify that system should not display any validation message when a user enter Valid value into weekly textbox

  @chrome 
  Scenario: Verify Start Date Picker functionality for Hourly Frequencey Type
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Smart Alerts Tile
    And : Click on Add New button of Smart Alert
    And : Select the Date from the houlry Start Date Picker
    Then : Verify that user should able to select Date for Start Date of hourly

  @chrome 
  Scenario: Verify End date picker functionality for Hourly Frequencey Type
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Smart Alerts Tile
    And : Click on Add New button of Smart Alert
    And : Select the Date from the End Date Picker
    Then : Verify that user should able to select Date for End Date

  